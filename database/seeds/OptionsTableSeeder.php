<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            [
                'key'   => 'mon_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'tue_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'wed_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'thu_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'fri_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'sat_schedule',
                'value' => '[11,22]',
            ],
            [
                'key'   => 'sun_schedule',
                'value' => '[11,17]',
            ],
            [
                'key'   => 'close_store',
                'value' => 'false',
            ],
        ]);
    }
}
