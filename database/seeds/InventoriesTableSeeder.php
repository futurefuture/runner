<?php

use App\Inventory;
use Illuminate\Database\Seeder;

class InventoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inventory::create([
            'title'       => 'Yonge & Summerhill',
            'vendor'      => 'LCBO',
            'store_id'    => 1,
            'location_id' => 10,
            'options'     => json_encode([
                'hours'       => ['11,22', '11,22', '11,22', '11,22', '11,22', '11,22', '11,17'],
                'address'     => '10 Scrivener Square, Toronto Ontario, M4W3Y9',
                'phoneNumber' => '416-922-0403',
            ]),
        ]);

        Inventory::create([
            'title'       => 'Queens Quay & Yonge',
            'vendor'      => 'LCBO',
            'store_id'    => 1,
            'location_id' => 217,
            'options'     => json_encode([
                'hours'       => ['11,22', '11,22', '11,22', '11,22', '11,22', '11,22', '11,17'],
                'address'     => '2 Cooper Stree, Toronto Ontario, M5E0B8',
                'phoneNumber' => '416 864-6777',
            ]),
        ]);

        Inventory::create([
            'title'       => 'The Wine Rack',
            'vendor'      => 'The Wine Rack',
            'store_id'    => 5,
            'location_id' => null,
            'options'     => json_encode([
                'hours'       => ['11,22', '11,22', '11,22', '11,22', '11,22', '11,22', '11,17'],
                'address'     => '',
                'phoneNumber' => '',
            ]),
        ]);
    }
}
