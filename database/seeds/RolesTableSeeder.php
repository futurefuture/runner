<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = new Role();
        $customer->name = 'Customer';
        $customer->description = 'A normal customer';
        $customer->save();

        $runner = new Role();
        $runner->name = 'Runner';
        $runner->description = 'A Runner';
        $runner->save();

        $dispatch = new Role();
        $dispatch->name = 'Dispatch';
        $dispatch->description = 'A dispatch';
        $dispatch->save();

        $admin = new Role();
        $admin->name = 'Admin';
        $admin->description = 'An admin';
        $admin->save();
    }
}
