<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'email'    => 'jarek@wearefuturefuture.com',
            'password' => bcrypt('yourmom'),
        ]);

        factory(App\User::class, 50)->create();
    }
}
