<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Refund::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween($min = 1, $max = 99999),
        'refund_id' => Str::random(60),
        'charge_id' => Str::random(60),
        'amount' => $faker->numberBetween($min = 1, $max = 200),
        'reason' => 'faker test',
    ];
});
