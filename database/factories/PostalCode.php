<?php

use Faker\Generator as Faker;

$factory->define(App\PostalCode::class, function (Faker $faker) {
    return [
        'postal_code' => 'M5H',
    ];
});
