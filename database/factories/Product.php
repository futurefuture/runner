<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Product::class, function (Faker $faker) {
    $title = $faker->word;
    $retailPrice = $faker->numberBetween($min = 0, $max = 10000);
    $runnerPrice = $retailPrice * 1.12;
    $markupPercentage = 0.12;
    $markup = $retailPrice * $markupPercentage;

    return [
        'id'                => $faker->numberBetween($min = 0, $max = 999999),
        'sku'               => $faker->numberBetween($min = 0, $max = 999999),
        'upc'               => $faker->numberBetween($min = 0, $max = 999999),
        'is_active'         => 1,
        'is_runner_owned'   => 0,
        'title'             => $title,
        'long_description'  => $faker->sentence,
        'short_description' => $faker->sentence,
        'retail_price'      => $retailPrice,
        'wholesale_price'   => null,
        'markup_percentage' => $markupPercentage,
        'markup'            => $markup,
        'runner_price'      => $runnerPrice,
        'image'             => $faker->imageUrl($width = 319, $height = 319, 'cats'),
        'image_thumbnail'   => $faker->imageUrl($width = 319, $height = 319, 'cats'),
        'packaging'         => '750mL bottle',
        'alcohol_content'   => $faker->numberBetween($min = 0, $max = 10000),
        'sugar_content'     => $faker->numberBetween($min = 0, $max = 10),
        'slug'              => Str::slug($title),
        'category_id'       => $faker->numberBetween($min = 0, $max = 100),
        'producing_country' => $faker->country,
        'producer'          => $faker->word,
    ];
});
