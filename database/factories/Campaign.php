<?php

use Faker\Generator as Faker;

$factory->define(App\Campaign::class, function (Faker $faker) {
    return [
        'partner_id' => $faker->randomDigit,
        'title'      => 'Awesome Campaign',
        'state'      => 'active',
        'start_date' => $faker->dateTime($max = 'now', $timezone = null),
        'end_date'   => $faker->dateTime($max = 'now', $timezone = null)
    ];
});
