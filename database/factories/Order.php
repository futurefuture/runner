<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    // create user to assign order to
    $user = factory(App\User::class)->create();

    // create customer our of user
    $customer = json_encode([
        'first_name'   => $user->first_name,
        'last_name'    => $user->last_name,
        'phone_number' => $user->phone_number,
        'email'        => $user->email,
    ]);

    // create product for order
    $product = factory(App\Product::class)->create();
    $productQuantity = $faker->numberBetween($min = 1, $max = 10);

    // transform product into order content
    $content = json_encode([
        [
            'id'               => $product->id,
            'title'            => $product->title,
            'rowId'            => $faker->sha1,
            'quantity'         => $productQuantity,
            'runnerPrice'      => round($product->runner_price_in_price / 100, 2),
            'retailPrice'      => round($product->runner_price_in_price / 100, 2),
            'packaging'        => $product->packaging,
            'primary_category' => $product->primary_category,
            'thumbnail'        => $product->image_thumb_url,
            'isVintages'       => $product->stock_type === 'VINTAGES' ? 1 : 0,
        ],
    ]);

    // create address
    $address = json_encode([
        'address'               => '263 Adelaide St W, Toronto M5H 1Y2, Canada',
        'lat'                   => 43.6477782,
        'lng'                   => 79.38956289999999,
    ]);

    $activity_log = json_encode([
        [
            'log'  => 'processing',
            'time' => [
                'date'          => '2017-03-03 14:14:45.000000',
                'timezone_type' => 3,
                'timezone'      => 'America\/Toronto',
            ],
        ],
    ]);

    // calulate markup
    $markup = round(($product->price_in_cents * 1.13) - $product->price_in_cents, 2);

    return [
        'id'                     => $faker->numberBetween($min = 100001, $max = 999999),
        'user_id'                => $user->id,
        'status'                 => 0,
        'content'                => $content,
        'card_last_four'         => 4242,
        'activity_log'           => $activity_log,
        'admin_notes'            => null,
        'charge'                 => 'ch_1F4w9VByumHgsxlUPnyG1YWd',
        'runner_1'               => null,
        'runner_2'               => null,
        'device'                 => $faker->randomElement($array = [1, 2, 3]),
        'deleted_at'             => null,
        'customer'               => $customer,
        'unit_number'            => $faker->numberBetween($min = 0, $max = 10000),
        'address'                => $address,
        'instructions'           => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'coupon_id'              => null,
        'subtotal'               => (int) ($productQuantity * $product->runner_price),
        'cogs'                   => round($productQuantity * $product->retail_price / 100, 2),
        'discount'               => 0,
        'delivery'               => 1000,
        'delivery_tax'           => 1000 * 0.13,
        'markup'                 => $markup / 100,
        'markup_tax'             => round($markup * 0.13 / 100, 2),
        'display_tax_total'      => round(9.99 * 0.13 + $markup / 100 * 0.13, 2),
        'tip'                    => 1000,
        'total'                  => (int) ($productQuantity * $product->runner_price + 1000 + 130),
        'stripe_fee'             => (int) ($productQuantity * $product->runner_price * 0.023 + 30),
        'signature'              => null,
        'delivered_at'           => null,
        'schedule_time'          => null,
        'additional_info'        => null,
        'first_time'             => 1,
        'service_fee'            => 0.00,
        'created_at'             => Carbon::now(),
        'updated_at'             => Carbon::now(),
    ];
});

$factory->state(App\Order::class, 'new', function (Faker $faker) {
    $category = factory(\App\Category::class)->create();
    $product = factory(App\Product::class)->create([
        'category_id'  => $category->id,
        'runner_price' => 1000,
    ]);

    $newContent = json_encode([
        [
            'id'           => $product->id,
            'title'        => $product->title,
            'runnerPrice'  => (int) $product->runner_price,
            'image'        => $product->image,
            'quantity'     => 3,
            'packaging'    => $product->packaging,
            'rewardPoints' => null,
            'allowSub'     => false,
            'subTotal'     => (int) $product->runner_price * 3,
        ],
    ]);

    return [
        'content' => $newContent,
        'device'  => 1,
    ];
});
