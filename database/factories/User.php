<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'id'                 => $faker->numberBetween($min = 1, $max = 99999),
        'first_name'         => $faker->name,
        'last_name'          => $faker->name,
        'date_of_birth'      => $faker->date('1979-06-09'),
        'phone_number'       => $faker->unique()->numberBetween($min = 1000000000, $max = 9999999999),
        'email'              => $faker->unique()->freeEmail,
        'password'           => bcrypt('secret'),
        'facebook_id'        => null,
        'google_id'          => null,
        'apple_id'           => null,
        'address'            => null,
        'unit'               => null,
        'stripe_id'          => Str::random(10),
        'is_19'              => 1,
        'invite_code'        => Str::random(6),
        'role'               => 3,
        'gender'             => $faker->randomElement($array = ['male', 'female']),
        'fcm_token'          => null,
        'age_range'          => null,
        'avatar_image'       => null,
        'promo_notification' => 1,
        'order_notification' => 1,
        'cart_notification'  => 1,
        'source'             => null,
        'deleted_at'         => null,
    ];
});
