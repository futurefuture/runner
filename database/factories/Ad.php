<?php

use Faker\Generator as Faker;

$factory->define(App\Ad::class, function (Faker $faker) {
    return [
        'ad_type_id'  => $faker->randomDigit,
        'partner_id'  => $faker->randomDigit,
        'campaign_id' => $faker->randomDigit,
        'content'     => $faker->paragraph($nbSentences = 3, $variableSentences = true),
        'products'    => $faker->paragraph($nbSentences = 3, $variableSentences = true),
        'product_id'  => $faker->randomDigit,
        'category_id' => $faker->randomDigit,
        'tag_id'      => $faker->randomDigit,
        'bid_type'    => 'absolute',
        'bid_amount'  => $faker->randomDigit,
        'state'       => 'active',
        'index'       => $faker->randomDigit,
        'start_date'  => $faker->dateTime($max = 'now', $timezone = null),
        'end_date'    => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
