<?php

use Faker\Generator as Faker;

$factory->define(App\Gift::class, function (Faker $faker) {
    return [
        'order_id' => 1,
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'address' => '{"route":"Dundas Street West","locality":"Toronto","administrative_area_level_1":"Ontario","country":"Canada","postal_code":"M5G 2H1","printable_address":"20 Dundas St W, Toronto, ON M5G 2H1, Canada","lat":43.6563911,"lng":-79.38195089999999}',
        'unit_number' => 121,
        'instructions' => $faker->sentence,
        'is_19' => 1,
        'email' => $faker->safeEmail,
        'phone_number' => $faker->phoneNumber,
        'message' => $faker->sentence,
        'gift_options' => $faker->sentence,
    ];
});
