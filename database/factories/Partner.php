<?php

use Faker\Generator as Faker;

$factory->define(App\Partner::class, function (Faker $faker) {
    return [
        'title'      => $faker->word,
        'user_id'    => 18,
        'image'      => $faker->imageUrl($width = 319, $height = 319, 'cats'),
        'store_id'   => 1,
        'ga_view_id' => 1345,
    ];
});
