<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Survey::class, function (Faker $faker) {
    $title = $faker->word;
    $results = $faker->word;
    $startDate = $faker->date;
    $endDate = $faker->date;

    return [
        'id'                => $faker->numberBetween($min = 0, $max = 999999),
        'title'             => $title,
        'start_date'        => $startDate,
        'end_date'          => $endDate,
        'results'           => $results,
        'segment'           => $faker->numberBetween($min = 0, $max = 999999),
    ];
});
