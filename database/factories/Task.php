<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'state'        => 0,
        'notes'        => $faker->sentence,
        'phone_number' => $faker->phoneNumber,
        'courier_id'   => 131,
        'container_id' => 1,
        'name'         => $faker->name,
        'organization' => null,
        'address_id'   => 202,
        'order_id'     => null,
        'type'         => null,
    ];
});
