<?php

use App\RewardPoint;
use Faker\Generator as Faker;

$factory->define(RewardPoint::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'value'   => $faker->numberBetween($min = 0, $max = 5),
    ];
});
