<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Schedule::class, function (Faker $faker) {
    return [
        'job_type' => 2,
        'user_id'  => 3,
        'start'    => Carbon::now()->toDateTimeString(),
        'end'      => Carbon::now()->addMinutes(120)->toDateTimeString(),
        'color'    => '#ffffff',
    ];
});
