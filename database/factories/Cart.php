<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(App\Cart::class, function (Faker $faker) {
    $product = factory(Product::class)->create([
        'id' => 8168
    ]);

    $content = json_encode([
        'products' => [
            [
                'id'           => $product->id,
                'title'        => $product->title,
                'runnerPrice'  => $product->runner_price,
                'image'        => $product->image,
                'quantity'     => 1,
                'packaging'    => $product->packaging,
                'rewardPoints' => null,
                'allowSub'     => true,
                'subTotal'     => $product->runner_price * 1,
            ]
        ],
        'subTotal'     => $product->runner_price * 1,
        'deliveryFee'  => 999,
        'tax'          => $product->runner_price - ($product->runner_price / 1.12) * 0.13,
        'rewardPoints' => 0,
        'discount'     => 0,
        'total'        => ($product->runner_price * 1) + 999 + ($product->runner_price - ($product->runner_price / 1.12) * 0.13),
        'serviceFee'   => 0,
        'isGift'       => false
    ]);

    return [
        'user_id'          => 1,
        'inventory_id'     => 1,
        'content'          => $content,
        'marketing_status' => 2,
    ];
});
