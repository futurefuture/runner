<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\RunnerPosition::class, function (Faker $faker) {
    return [
        'title'           => 'Janitor',
        'description'     => 'Cleaning shit.',
        'rate'            => 12.26,
        'backgroundColor' => 'red',
    ];
});
