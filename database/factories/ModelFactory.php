<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Coupon::class, function (Faker\Generator $faker) {
    return [
        'value'        => 10,
        'code'         => 'FAKER',
        'active_until' => '2018-12-31',
        'is_active'    => 1,
    ];
});
