<?php

use Faker\Generator as Faker;

$factory->define(App\OrderItem::class, function (Faker $faker) {
    return [
        'order_id'     => $faker->randomDigitNotNull,
        'user_id'      => $faker->randomDigitNotNull,
        'product_id'   => $faker->randomDigitNotNull,
        'title'        => $faker->word,
        'quantity'     => 3,
        'retail_price' => 10000,
        'runner_price' => 23.36,
        'packaging'    => '750mL bottle',
        'producer'     => $faker->word,
        'category_id'  => 1,
    ];
});
