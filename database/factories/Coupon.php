<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Coupon::class, function (Faker $faker) {
    return [
        'value'        => $faker->numberBetween($min = 5, $max = 10),
        'code'         => strtoupper($faker->word),
        'active_until' => new Carbon('tomorrow'),
        'is_active'    => 1,
    ];
});
