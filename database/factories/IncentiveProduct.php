<?php

use Faker\Generator as Faker;

$factory->define(App\IncentiveProduct::class, function (Faker $faker) {
    return [
        'incentive_id'       => 1,
        'product_id'         => 1,
        'start_date'         => '2018-12-01',
        'end_date'           => '2018-12-02',
        'savings'            => 100,
        'is_active'          => 1,
        'reward_points'      => null,
        'minimum_quantity'   => null,
        'coupon_code'        => null,
        'minimum_cart_value' => null,
    ];
});
