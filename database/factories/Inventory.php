<?php

use Faker\Generator as Faker;

$factory->define(App\Inventory::class, function (Faker $faker) {
    return [
        'title'        => $faker->word,
        'vendor'       => 'LCBO',
        'store_id'     => 1,
        'location_id'  => 1,
        'options'      => [
            'hours' => [
                '11:00-21:30',
                '11:00-21:30',
                '11:00-21:30',
                '11:00-21:30',
                '11:00-21:30',
                '11:00-21:30',
                '11:00-17:00',
            ],
            'address'     => '10 Scrivener Square, Toronto Ontario, M4W3Y9',
            'phoneNumber' => '416-922-0403',
        ],
        'is_active'    => 1,
        'address_id'   => 1,
    ];
});
