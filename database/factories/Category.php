<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Category::class, function (Faker $faker) {
    $title = $faker->word;

    return [
        'title'       => $title,
        'parent_id'   => 1,
        'slug'        => Str::slug($title),
    ];
});
