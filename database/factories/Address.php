<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'user_id'           => 1,
        'formatted_address' => '263 Adelaide St W, Toronto, ON M5H 3G2, Canada',
        'lat'               => '43.6477782',
        'lng'               => '43.6477782',
        'place_id'          => 'ChIJdz2LWtA0K4gRUmurrIEGwzg',
        'postal_code'       => 'M5H 3G2',
        'unit_number'       => null,
        'business_name'     => null,
        'instructions'      => '60 brock ave. call: 6479155109',
        'selected'          => 1,
    ];
});
