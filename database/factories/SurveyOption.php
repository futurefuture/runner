<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\SurveyOption::class, function (Faker $faker) {
    $title = $faker->word;
    $results = $faker->word;
    $startDate = $faker->date;
    $endDate = $faker->date;

    return [
        'id'                => $faker->numberBetween($min = 0, $max = 999999),
        'title'             => $title,
    ];
});
