<?php

use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'title'      => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'value'      => $faker->numberBetween($min = 1, $max = 5),
        'product_id' => function () {
            return factory(App\ProductFinal::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'is_approved'   => 1,
        'description'   => $faker->sentence($nbWords = 12, $variableNbWords = true),
    ];
});
