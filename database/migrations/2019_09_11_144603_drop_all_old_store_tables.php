<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class DropAllOldStoreTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stores');
        Schema::dropIfExists('stores_active');
        Schema::dropIfExists('stores_final');
        Schema::dropIfExists('products');
        Schema::dropIfExists('products_additional');
        Schema::dropIfExists('products_custom');
        Schema::dropIfExists('products_final');
        Schema::dropIfExists('products_inactive');
        Schema::dropIfExists('inventories_final');
        Schema::dropIfExists('inventories');
    }
}
