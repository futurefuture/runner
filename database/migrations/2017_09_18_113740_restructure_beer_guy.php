<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RestructureBeerGuy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('the_beer_guy');
        Schema::create('the_beer_guy', function (Blueprint $table) {
            $table->integer('id');
            $table->primary('id'); //purchase_id
            $table->integer('user_id')->nullable();
            $table->integer('status')->nullable(); //status
            $table->longText('content'); //products
            $table->integer('card_last_four')->nullable(); //null
            $table->longText('activity_log');
            $table->longText('admin_notes')->nullable();
            $table->string('charge')->nullable();
            $table->integer('runner_1')->nullable();
            $table->integer('runner_2')->nullable();
            $table->integer('device')->default(1); //always 1
            $table->longText('customer'); //customer_id,name,email,phone,cell
            $table->string('address'); //address TODO
            $table->string('unit_number'); //addr2
            $table->integer('coupon')->nullable(); //null
            $table->longText('instructions'); //buzzer,location_extra,
            $table->decimal('subtotal', 8, 2); //product_amount
            $table->decimal('cogs', 8, 2); //product_amount
            $table->decimal('discount', 8, 2)->default(0);
            $table->decimal('delivery', 8, 2); //deliver_fee
            $table->decimal('delivery_tax', 8, 2); //hst
            $table->decimal('markup', 8, 2)->default(0);
            $table->decimal('markup_tax', 8, 2)->default(0);
            $table->decimal('display_tax_total', 8, 2); //hst
            $table->decimal('tip', 8, 2); //drv_tip_amount
            $table->decimal('total', 8, 2); //total
            $table->decimal('stripe_fee', 8, 2)->default(0);
            $table->longText('signature')->nullable();

            $table->dateTime('schedule_time')->nullable(); //schedule_date, schedule_time
            $table->longText('additional_info')->nullable();
            //purchase_extra,location_id,

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('the_beer_guy');
        Schema::create('the_beer_guy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id');
            $table->softDeletes();
            $table->string('status')->default('received');
            $table->longText('content');
            $table->timestamps();
        });
    }
}
