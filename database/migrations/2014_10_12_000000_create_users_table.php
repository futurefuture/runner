<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth')->nullable();
            $table->string('phone_number')->unique();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('facebook_id')->nullable();
            $table->longText('additional_info')->nullable();
            $table->longText('address')->nullable();
            $table->string('unit')->nullable();
            $table->string('stripe_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->string('api_token', 60)->unique();
            $table->boolean('is_19')->default(0);
            $table->integer('credit')->default(0);
            $table->string('invite_code', 6)->unique();
            $table->integer('role')->default(3);
            $table->string('gender')->nullable();
            $table->string('fcm_token')->nullable();
            $table->string('age_range')->nullable();
            $table->string('avatar_image')->nullable();
            $table->boolean('promo_notification')->default(1);
            $table->boolean('order_notification')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
