<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsFromOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn([
                'discount',
                'rowId',
                'primary_category',
                'secondary_category',
                'tertiary_category',
                'thumbnail',
                'origin',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('discount');
            $table->string('rowId');
            $table->string('primary_category');
            $table->string('secondary_category');
            $table->string('tertiary_category');
            $table->string('thumbnail');
            $table->string('origin');
        });
    }
}
