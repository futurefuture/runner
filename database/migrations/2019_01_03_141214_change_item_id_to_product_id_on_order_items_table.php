<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeItemIdToProductIdOnOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('item_id', 'product_id');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('name', 'title');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('qty', 'quantity');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('true_price', 'retail_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('product_id', 'item_id');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('title', 'name');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('quantity', 'qty');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('retail_price', 'true_price');
        });
    }
}
