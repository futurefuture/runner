<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLimitedTimeOfferAndSavingsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('limited_time_offer_price')->after('runner_price')->nullable();
            $table->integer('limited_time_offer_savings')->after('runner_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('limited_time_offer_price');
            $table->dropColumn('limited_time_offer_savings');
        });
    }
}
