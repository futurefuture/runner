<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePostalCodeIdInventoryPostalCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_postal_code', function (Blueprint $table) {
            $table->renameColumn('postal_code_id', 'postal_code');
        });
        Schema::table('inventory_postal_code', function (Blueprint $table) {
            $table->string('postal_code')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_postal_code', function (Blueprint $table) {
            $table->integer('postal_code_id')->change();
            $table->renameColumn('postal_code', 'postal_code_id');
        });
    }
}
