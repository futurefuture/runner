<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ChangeCategoryProductsIndexTableToCategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('category_product_index', 'category_product');
    }

    public function down()
    {
        Schema::rename('category_product', 'category_product_index');
    }
}
