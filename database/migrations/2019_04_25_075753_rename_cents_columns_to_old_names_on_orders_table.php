<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameCentsColumnsToOldNamesOnOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('subtotalcents', 'subtotal');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('cogscents', 'cogs');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('discountcents', 'discount');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('deliverycents', 'delivery');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('deliverytaxcents', 'delivery_tax');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('markupcents', 'markup');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('markuptaxcents', 'markup_tax');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('displaytaxtotalcents', 'display_tax_total');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('tipcents', 'tip');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('totalcents', 'total');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('stripefeecents', 'stripe_fee');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('servicefeecents', 'service_fee');
        });
    }
}
