<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsAdditional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_additional', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->longText('description')->nullable();
            $table->string('image_thumb_url')->nullable();
            $table->string('image_url')->nullable();
            $table->string('primary_category')->nullable();
            $table->string('secondary_category')->nullable();
            $table->integer('wine_rating')->nullable();
            $table->longText('details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_additional');
    }
}
