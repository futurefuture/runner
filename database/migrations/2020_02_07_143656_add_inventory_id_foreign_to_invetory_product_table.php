<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryIdForeignToInvetoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->unsignedInteger('inventory_id')->change();
        });
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->change();
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->integer('inventory_id')->change();
            $table->integer('product_id')->change();
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table->dropForeign('inventory_product_inventory_id_foreign');
            $table->dropForeign('inventory_product_product_id_foreign');
        });
    }
}
