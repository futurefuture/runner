<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturedProductAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_product_analytics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sku');
            $table->integer('client_id');
            $table->date('start');
            $table->date('end');
            $table->integer('page_views');
            $table->integer('atc');
            $table->integer('quantity_sold');
            $table->integer('total_sales');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_product_analytics');
    }
}
