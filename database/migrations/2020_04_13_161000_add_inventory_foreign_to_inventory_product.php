<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryForeignToInventoryProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->integer('inventory_id')->unsigned()->change();
            $table->foreign('inventory_id')
                ->references('id')->on('inventory_product')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->dropForeign('inventory_product_inventory_id_foreign');
        });
    }
}
