<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNamesOnPartnerProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_product', function (Blueprint $table) {
            $table->renameColumn('client_id', 'partner_id');
        });
        Schema::table('partner_product', function (Blueprint $table) {
            $table->renameColumn('name', 'title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_product', function (Blueprint $table) {
            $table->renameColumn('partner_id', 'client_id');
        });
        Schema::table('partner_product', function (Blueprint $table) {
            $table->renameColumn('title', 'name');
        });
    }
}
