<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->string('upc')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('title');
            $table->longText('long_description')->nullable();
            $table->longText('short_description')->nullable();
            $table->integer('retail_price')->nullable();
            $table->integer('wholesale_price')->nullable();
            $table->float('markup_percentage', 8, 2);
            $table->integer('markup');
            $table->integer('runner_price');
            $table->string('image')->nullable();
            $table->string('image_thumbnail')->nullable();
            $table->string('packaging')->nullable();
            $table->integer('alcohol_content')->nullable();
            $table->integer('sugar_content')->nullable();
            $table->string('slug');
            $table->integer('category_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_products');
    }
}
