<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayoutStoreTerritory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layout_store_territory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('layout_id')->unsigned();
            $table->integer('store_id')->unsigned();
            $table->bigInteger('territory_id')->unsigned();
            $table->foreign('layout_id')->references('id')->on('layouts');
            $table->foreign('store_Id')->references('id')->on('stores');
            $table->foreign('territory_id')->references('id')->on('territories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layout_store_territory');
    }
}
