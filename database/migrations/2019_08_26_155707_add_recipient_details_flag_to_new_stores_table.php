<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecipientDetailsFlagToNewStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_stores', function (Blueprint $table) {
            $table->boolean('is_for_recipient')->default(false)->after('cart_extras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_stores', function (Blueprint $table) {
            $table->dropColumn('is_for_recipient');
        });
    }
}
