<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->float('discount')->default(null)->nullable();
            $table->integer('item_id')->default(null)->nullable();
            $table->string('name')->default(null)->nullable();
            $table->string('rowId')->default(null)->nullable();
            $table->integer('qty')->default(null)->nullable();
            $table->float('true_price')->default(null)->nullable();
            $table->float('runner_price')->default(null)->nullable();
            $table->string('package')->default(null)->nullable();
            $table->string('brand')->default(null)->nullable();
            $table->string('primary_category')->default(null)->nullable();
            $table->string('secondary_category')->default(null)->nullable();
            $table->string('origin')->default(null)->nullable();
            $table->string('thumbnail', 1024)->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
