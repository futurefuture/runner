<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCentsColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('subtotalcents')->after('subtotal')->default(0);
            $table->integer('cogscents')->after('cogs')->default(0);
            $table->integer('discountcents')->after('discount')->default(0);
            $table->integer('deliverycents')->after('delivery')->default(0);
            $table->integer('deliverytaxcents')->after('delivery_tax')->default(0);
            $table->integer('markupcents')->after('markup')->default(0);
            $table->integer('markuptaxcents')->after('markup_tax')->default(0);
            $table->integer('displaytaxtotalcents')->after('display_tax_total')->default(0);
            $table->integer('tipcents')->after('tip')->default(0);
            $table->integer('totalcents')->after('total')->default(0);
            $table->integer('stripefeecents')->after('stripe_fee')->default(0);
            $table->integer('servicefeecents')->after('service_fee')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('subtotalcents');
            $table->dropColumn('cogscents');
            $table->dropColumn('discountcents');
            $table->dropColumn('deliverycents');
            $table->dropColumn('deliverytaxcents');
            $table->dropColumn('markupcents');
            $table->dropColumn('markuptaxcents');
            $table->dropColumn('displaytaxtotalcents');
            $table->dropColumn('tipcents');
            $table->dropColumn('totalcents');
            $table->dropColumn('stripefeecents');
            $table->dropColumn('servicefeecents');
        });
    }
}
