<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropProductsFinalPreviousInventories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_final_previous_inventories', function (Blueprint $table) {
            Schema::dropIfExists('products_final_previous_inventories');
        });
    }
}
