<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponentLayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_layout', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('component_id')->unsigned();
            $table->bigInteger('layout_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components');
            $table->foreign('layout_id')->references('id')->on('layouts');
            $table->integer('index')->nullable()->default('300');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('component_layout');
    }
}
