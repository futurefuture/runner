<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncentiveProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incentive_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('incentive_id');
            $table->integer('product_id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('savings')->nullable();
            $table->integer('is_active')->nullable();
            $table->integer('reward_points')->nullable();
            $table->integer('minimum_quantity')->nullable();
            $table->string('coupon_code')->nullable();
            $table->integer('minimum_cart_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incentive_product');
    }
}
