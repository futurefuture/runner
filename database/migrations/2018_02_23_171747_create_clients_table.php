<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name')->default(null)->nullable();
            $table->string('business_operating_as')->default(null)->nullable();
            $table->string('business_address')->default(null)->nullable();
            $table->string('business_address2')->default(null)->nullable();
            $table->string('business_city')->default(null)->nullable();
            $table->string('business_postcode')->default(null)->nullable();
            $table->string('business_country')->default(null)->nullable();
            $table->string('contact_name')->default(null)->nullable();
            $table->string('contact_email')->default(null)->nullable();
            $table->string('contact_phone1')->default(null)->nullable();
            $table->string('contact_phone2')->default(null)->nullable();
            $table->string('contact_fax')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
