<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state')->default(0);
            $table->integer('index')->default(0);
            $table->longText('notes')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('courier_id');
            $table->integer('container_id');
            $table->string('name')->nullable();
            $table->string('organization')->nullable();
            $table->integer('address_id');
            $table->integer('order_id')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
