<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ad_type_id');
            $table->integer('partner_id');
            $table->integer('campaign_id');
            $table->longText('content')->nullable();
            $table->longText('products')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('tag_id')->nullable();
            $table->string('bid_type');
            $table->integer('bid_amount');
            $table->string('state');
            $table->integer('index')->nullable();
            $table->timeStamp('start_date')->useCurrent();
            $table->timeStamp('end_date')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
