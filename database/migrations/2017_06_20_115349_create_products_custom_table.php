<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCustomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_custom', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('primary_category')->nullable();
            $table->string('package')->nullable();
            $table->integer('price_in_cents')->default(0);
            $table->string('secondary_category')->nullable();
            $table->string('origin')->nullable();
            $table->string('image_thumb_url')->nullable();
            $table->string('image_url')->nullable();
            $table->string('description')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->integer('product_final_id')->nullable();
            $table->integer('pre_tax_price_in_cents')->default(0);
            $table->integer('tax_in_cents')->default(0);
            $table->integer('regular_price_in_cents')->default(0);
            $table->integer('pre_tax_regular_price_in_cents')->default(0);
            $table->integer('regular_price_tax_in_cents')->default(0);
            $table->decimal('markup_percentage', 3, 2)->default(0.00);
            $table->integer('markup_in_cents')->default(0);
            $table->integer('runner_price_in_cents')->default(0);
            $table->integer('limited_time_offer_savings_in_cents')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_custom');
    }
}
