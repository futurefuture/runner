<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesFinalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories_final', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->default(null)->nullable();
            $table->integer('store_id')->default(null)->nullable();
            $table->string('is_dead')->default(null)->nullable();
            $table->integer('quantity')->default(null)->nullable();
            $table->dateTime('reported_on')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories_final');
    }
}
