<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCraftProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('craft_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('is_dead')->nullable();
            $table->string('name')->nullable();
            $table->text('tags')->nullable();
            $table->string('is_discontinued')->nullable();
            $table->integer('price_in_cents')->nullable();
            $table->integer('regular_price_in_cents')->nullable();
            $table->integer('limited_time_offer_savings_in_cents')->nullable();
            $table->string('limited_time_offer_ends_on')->nullable();
            $table->integer('bonus_reward_miles')->nullable();
            $table->string('bonus_reward_miles_ends_on')->nullable();
            $table->string('stock_type')->nullable();
            $table->string('primary_category')->nullable();
            $table->string('secondary_category')->nullable();
            $table->string('origin')->nullable();
            $table->string('package')->nullable();
            $table->string('package_unit_type')->nullable();
            $table->integer('package_unit_volume_in_milliliters')->nullable();
            $table->integer('total_package_units')->nullable();
            $table->integer('volume_in_milliliters')->nullable();
            $table->integer('alcohol_content')->nullable();
            $table->integer('price_per_liter_of_alcohol_in_cents')->nullable();
            $table->integer('price_per_liter_in_cents')->nullable();
            $table->integer('inventory_count')->nullable();
            $table->integer('inventory_volume_in_milliliters')->nullable();
            $table->integer('inventory_price_in_cents')->nullable();
            $table->string('sugar_content')->nullable();
            $table->string('producer_name')->nullable();
            $table->string('released_on')->nullable();
            $table->string('has_value_added_promotion')->nullable();
            $table->string('has_limited_time_offer')->nullable();
            $table->string('has_bonus_reward_miles')->nullable();
            $table->string('is_seasonal')->nullable();
            $table->string('is_vqa')->nullable();
            $table->string('is_kosher')->nullable();
            $table->text('value_added_promotion_description')->nullable();
            $table->text('description')->nullable();
            $table->text('serving_suggestion')->nullable();
            $table->text('tasting_note')->nullable();
            $table->string('updated_at')->nullable();
            $table->string('image_thumb_url')->nullable();
            $table->string('image_url')->nullable();
            $table->string('varietal')->nullable();
            $table->string('style')->nullable();
            $table->string('tertiary_category')->nullable();
            $table->string('sugar_in_grams_per_liter')->nullable();
            $table->string('slug')->nullable();
            $table->longText('stores')->nullable();
            $table->integer('wine_rating')->nullable();
            $table->integer('pre_tax_price_in_cents')->nullable();
            $table->integer('tax_in_cents')->nullable();
            $table->integer('pre_tax_regular_price_in_cents')->nullable();
            $table->integer('regular_price_tax_in_cents')->nullable();
            $table->decimal('markup_percentage', 5, 2)->nullable();
            $table->integer('markup_in_cents')->nullable();
            $table->integer('runner_price_in_cents')->nullable();
            $table->longText('details')->nullable();
            $table->double('rating', 15, 8)->nullable();
            $table->string('producing_country')->nullable();
            $table->string('producing_region')->nullable();
            $table->string('producing_subregion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('craft_products');
    }
}
