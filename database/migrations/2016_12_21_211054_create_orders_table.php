<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->integer('status');
            $table->longText('content');
            $table->string('card_last_four');
            $table->longText('activity_log')->nullable();
            $table->longText('admin_notes')->nullable();
            $table->string('charge');
            $table->integer('runner_1')->nullable();
            $table->integer('runner_2')->nullable();
            $table->integer('device')->default(1);
            $table->longText('customer')->nullable();
            $table->longText('address')->change();
            $table->string('unit_number')->nullable();
            $table->longText('address')->nullable();
            $table->longText('instructions')->nullable();
            $table->integer('coupon')->nullable();
            $table->longText('instructions')->change();
            $table->decimal('subtotal', 8, 2)->default(0);
            $table->decimal('cogs', 8, 2)->default(0);
            $table->decimal('discount', 8, 2)->default(0);
            $table->decimal('delivery', 8, 2)->default(0);
            $table->decimal('delivery_tax', 8, 2)->default(0);
            $table->decimal('markup', 8, 2)->default(0);
            $table->decimal('markup_tax', 8, 2)->default(0);
            $table->decimal('display_tax_total', 8, 2)->default(0);
            $table->decimal('tip', 8, 2)->default(0);
            $table->decimal('total', 8, 2)->default(0);
            $table->decimal('stripe_fee', 8, 2)->default(0);
            $table->longText('signature')->nullable();
            $table->dateTime('delivered_at')->nullable();
            $table->dateTime('schedule_time')->nullable();
            $table->longText('additional_info')->nullable();
            $table->boolean('first_time')->default(0);
            $table->decimal('service_fee', 8, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        if (App::environment('testing')) {
            $statement = "UPDATE SQLITE_SEQUENCE SET seq = 100001 WHERE name = 'orders'";
        } else {
            $statement = 'ALTER TABLE orders AUTO_INCREMENT = 100001;';
        }

        DB::update($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
