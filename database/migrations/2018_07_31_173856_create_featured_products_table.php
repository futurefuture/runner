<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sku');
            $table->string('package');
            $table->string('vendor');
            $table->integer('client_id')->nullable();
            $table->string('primary_category')->nullable();
            $table->string('secondary_category')->nullable();
            $table->date('base_start_date');
            $table->date('base_end_date');
            $table->date('active_start_date');
            $table->date('active_end_date');
            $table->integer('featured_position');
            $table->json('analytics');
            $table->integer('is_active');
            $table->integer('limited_time_offer');
            $table->integer('limited_time_offer_regular_price_in_cents')->nullable();
            $table->string('image_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_products');
    }
}
