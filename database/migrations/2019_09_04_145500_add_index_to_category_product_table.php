<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToCategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tags', function (Blueprint $table) {
            $table->dropIndex('tags_title_index');
            $table->dropIndex('tags_type_index');
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->change();
            $table->integer('product_id')->unsigned()->change();
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->index('category_id');
            $table->index('product_id');
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table
                ->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');

            $table
                ->foreign('product_id')
                ->references('id')
                ->on('new_products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_product', function (Blueprint $table) {
            $table->integer('category_id')->unsigned(false)->change();
            $table->integer('product_id')->unsigned(false)->change();
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->dropIndex('category_id');
            $table->dropIndex('product_id');
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->dropForeign('category_id');
            $table->dropForeign('product_id');
        });
    }
}
