<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndexOnProductTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_tag', function (Blueprint $table) {
            $table->integer('product_id')->unsigned()->nullable()->change();
            $table->integer('tag_id')->unsigned()->nullable()->change();
        });

        Schema::table('product_tag', function (Blueprint $table) {
            $table->index('product_id');
            $table->index('tag_id');
        });

        Schema::table('product_tag', function (Blueprint $table) {
            $table
                ->foreign('product_id')
                ->references('id')
                ->on('new_products')
                ->onDelete('cascade');

            // $table
            //     ->foreign('tag_id')
            //     ->references('id')
            //     ->on('tags')
            //     ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_tag', function (Blueprint $table) {
            $table->integer('product_id')->unsigned(false)->change();
            $table->integer('tag_id')->unsigned(false)->change();
        });

        Schema::table('product_tag', function (Blueprint $table) {
            $table->dropIndex('product_id');
            $table->dropIndex('tag_id');
        });

        Schema::table('product_tag', function (Blueprint $table) {
            $table->dropForeign('product_id');
            // $table->dropForeign('tag_id');
        });
    }
}
