<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresFinalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores_final', function (Blueprint $table) {
            $table->increments('id');
            $table->string('is_dead')->default(null)->nullable();
            $table->string('name')->default(null)->nullable();
            $table->string('tags')->default(null)->nullable();
            $table->string('address_line_1')->default(null)->nullable();
            $table->string('address_line_2')->default(null)->nullable();
            $table->string('city')->default(null)->nullable();
            $table->string('postal_code')->default(null)->nullable();
            $table->string('telephone')->default(null)->nullable();
            $table->string('fax')->default(null)->nullable();
            $table->string('latitude')->default(null)->nullable();
            $table->string('longitude')->default(null)->nullable();
            $table->integer('products_count')->default(null)->nullable();
            $table->integer('inventory_count')->default(null)->nullable();
            $table->bigInteger('inventory_price_in_cents')->default(null)->nullable();
            $table->bigInteger('inventory_volume_in_milliliters')->default(null)->nullable();
            $table->string('has_wheelchair_accessability')->default(null)->nullable();
            $table->string('has_bilingual_services')->default(null)->nullable();
            $table->string('has_product_consultant')->default(null)->nullable();
            $table->string('has_tasting_bar')->default(null)->nullable();
            $table->string('has_beer_cold_room')->default(null)->nullable();
            $table->string('has_special_occasion_permits')->default(null)->nullable();
            $table->string('has_vintages_corner')->default(null)->nullable();
            $table->string('has_parking')->default(null)->nullable();
            $table->string('has_transit_access')->default(null)->nullable();
            $table->string('sunday_open')->default(null)->nullable();
            $table->string('sunday_close')->default(null)->nullable();
            $table->string('monday_open')->default(null)->nullable();
            $table->string('monday_close')->default(null)->nullable();
            $table->string('tuesday_open')->default(null)->nullable();
            $table->string('tuesday_close')->default(null)->nullable();
            $table->string('wednesday_open')->default(null)->nullable();
            $table->string('wednesday_close')->default(null)->nullable();
            $table->string('thursday_open')->default(null)->nullable();
            $table->string('thursday_close')->default(null)->nullable();
            $table->string('friday_open')->default(null)->nullable();
            $table->string('friday_close')->default(null)->nullable();
            $table->string('saturday_open')->default(null)->nullable();
            $table->string('saturday_close')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores_final');
    }
}
