<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveAnalyticsColumnFromFeaturedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('featured_products', 'analytics')) {
            Schema::table('featured_products', function (Blueprint $table) {
                $table->dropColumn('analytics');
            });
        }
    }
}
