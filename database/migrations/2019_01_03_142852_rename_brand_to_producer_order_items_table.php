<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameBrandToProducerOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('brand', 'producer');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('package', 'packaging');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('producer', 'brand');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('packaging', 'package');
        });
    }
}
