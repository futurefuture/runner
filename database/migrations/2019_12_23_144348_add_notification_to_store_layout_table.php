<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotificationToStoreLayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_layouts', function (Blueprint $table) {
            $table->longText('notification')->after('layout')->nullable();
            $table->boolean('can_take_payment')->after('notification')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_layouts', function (Blueprint $table) {
            $table->dropColumn('notification');
            $table->dropColumn('can_take_payment');
        });
    }
}
