<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropNonCentColumnsFromOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'subtotal',
                'cogs',
                'discount',
                'delivery',
                'delivery_tax',
                'markup',
                'markup_tax',
                'display_tax_total',
                'tip',
                'total',
                'stripe_fee',
                'service_fee',
            ]);
            // $table->dropColumn('subtotal');
            // $table->dropColumn('cogs');
            // $table->dropColumn('discount');
            // $table->dropColumn('delivery');
            // $table->dropColumn('delivery_tax');
            // $table->dropColumn('markup');
            // $table->dropColumn('markup_tax');
            // $table->dropColumn('display_tax_total');
            // $table->dropColumn('tip');
            // $table->dropColumn('total');
            // $table->dropColumn('stripe_fee');
            // $table->dropColumn('service_fee');
        });
    }
}
