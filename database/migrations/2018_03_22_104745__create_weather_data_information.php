<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherDataInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_data', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time')->nullable();
            $table->string('summary')->nullable();
            $table->string('icon')->nullable();
            $table->dateTime('sunriseTime')->nullable();
            $table->dateTime('sunsetTime')->nullable();
            $table->decimal('moonPhase')->nullable();
            $table->decimal('precipIntensity')->nullable();
            $table->decimal('precipIntensityMax')->nullable();
            $table->dateTime('precipIntensityMaxTime')->nullable();
            $table->decimal('precipProbability')->nullable();
            $table->decimal('precipAccumulation')->nullable();
            $table->string('precipType')->nullable();
            $table->decimal('temperatureHigh')->nullable();
            $table->dateTime('temperatureHighTime')->nullable();
            $table->decimal('temperatureLow')->nullable();
            $table->dateTime('temperatureLowTime')->nullable();
            $table->decimal('apparentTemperatureHigh')->nullable();
            $table->dateTime('apparentTemperatureHighTime')->nullable();
            $table->decimal('apparentTemperatureLow')->nullable();
            $table->dateTime('apparentTemperatureLowTime')->nullable();
            $table->decimal('dewPoint')->nullable();
            $table->decimal('humidity')->nullable();
            $table->decimal('pressure')->nullable();
            $table->decimal('windSpeed')->nullable();
            $table->decimal('windGust')->nullable();
            $table->dateTime('windGustTime')->nullable();
            $table->decimal('windBearing')->nullable();
            $table->decimal('cloudCover')->nullable();
            $table->decimal('uvIndex')->nullable();
            $table->dateTime('uvIndexTime')->nullable();
            $table->decimal('visibility')->nullable();
            $table->decimal('ozone')->nullable();
            $table->decimal('temperatureMin')->nullable();
            $table->dateTime('temperatureMinTime')->nullable();
            $table->decimal('temperatureMax')->nullable();
            $table->dateTime('temperatureMaxTime')->nullable();
            $table->decimal('apparentTemperatureMin')->nullable();
            $table->dateTime('apparentTemperatureMinTime')->nullable();
            $table->decimal('apparentTemperatureMax')->nullable();
            $table->dateTime('apparentTemperatureMaxTime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_data');
    }
}
