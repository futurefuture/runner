<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameNewInventoriesToInventories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('new_inventories', 'inventories');
        Schema::rename('new_products', 'products');
        Schema::rename('new_stores', 'stores');
    }

    public function down()
    {
        Schema::rename('inventories', 'new_inventories');
        Schema::rename('products', 'new_products');
        Schema::rename('stores', 'new_stores');
    }
}
