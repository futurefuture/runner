<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToInventoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->integer('inventory_id')->unsigned()->change();
            $table->integer('product_id')->unsigned()->change();
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table->index('inventory_id');
            $table->index('product_id');
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table
                ->foreign('inventory_id')
                ->references('id')
                ->on('new_inventories')
                ->onDelete('cascade');

            $table
                ->foreign('product_id')
                ->references('id')
                ->on('new_products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_product', function (Blueprint $table) {
            $table->integer('inventory_id')->unsigned(false)->change();
            $table->integer('product_id')->unsigned(false)->change();
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table->dropIndex('inventory_id');
            $table->dropIndex('product_id');
        });

        Schema::table('inventory_product', function (Blueprint $table) {
            $table->dropForeign('inventory_id');
            $table->dropForeign('product_id');
        });
    }
}
