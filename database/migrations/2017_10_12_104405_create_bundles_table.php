<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->longText('secondary_description')->nullable();
            $table->integer('price_in_cents')->default(0);
            $table->integer('pre_tax_price_in_cents')->default(0);
            $table->integer('tax_in_cents')->default(0);
            $table->integer('regular_price_in_cents')->default(0);
            $table->integer('pre_tax_regular_price_in_cents')->default(0);
            $table->integer('regular_price_tax_in_cents')->default(0);
            $table->decimal('markup_percentage', 3, 2)->default(0.00);
            $table->integer('markup_in_cents')->default(0);
            $table->integer('runner_price_in_cents')->default(0);
            $table->integer('limited_time_offer_savings_in_cents')->default(0);
            $table->string('slug')->unique()->nullable();
            $table->integer('inventory')->default(0);
            $table->string('banner_url')->nullable();
            $table->string('image_url')->nullable();
            $table->integer('order')->nullable();
            $table->boolean('is_active')->default(0);
            $table->longText('carousel_images')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundles');
    }
}
