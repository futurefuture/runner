<?php

// carousel
Route::get('/config/ui/main-carousel/', function () {
    return response()->json([
        'data' => [
            [
                'type'       => 'main carousel images',
                'id'         => '1',
                'attributes' => [
                    'imageDesktop'     => 'https://res.cloudinary.com/dcnts7jzv/image/upload/f_auto,q_auto/v1538000229/rush/mainCarousel/1/imageDesktop.jpg',
                    'imageMobile'      => 'https://res.cloudinary.com/dcnts7jzv/image/upload/f_auto,q_auto/v1537824087/rush/mainCarousel/1/imageMobile.jpg',
                    'image1x'          => '',
                    'image2x'          => '',
                    'image3x'          => '',
                    'title'            => 'Black Spiced Rum',
                    'subtitle'         => 'Kraken',
                    'altText'          => '$10 Off Delivery',
                    'buttonText'       => 'Buy Now',
                    'buttonLink'       => '/query?product=kraken&',
                    'deepLinkType'     => 'appPage',
                    'deepLink'         => 'kraken',
                    'isVideo'          => false,
                ],
            ],
            [
                'type'       => 'main carousel images',
                'id'         => '2',
                'attributes' => [
                    'imageDesktop'     => 'https://res.cloudinary.com/dcnts7jzv/image/upload/v1542639249/runner/stores/runner/mainCarousel/referAFriendDesktop.jpg',
                    'imageMobile'      => 'https://res.cloudinary.com/dcnts7jzv/image/upload/v1542740019/runner/stores/runner/mainCarousel/referAFriendMobile.jpg',
                    'image1x'          => '',
                    'image2x'          => '',
                    'image3x'          => '',
                    'title'            => 'Refer A Friend',
                    'subtitle'         => 'Give $10, Get $10',
                    'altText'          => '',
                    'buttonText'       => 'Click Here',
                    'buttonLink'       => '/refer-a-friend',
                    'deepLinkType'     => 'appPage',
                    'deepLink'         => 'refer-a-friend',
                    'isVideo'          => false,
                ],
            ],
            [
                'type'       => 'main carousel images',
                'id'         => '3',
                'attributes' => [
                    'imageDesktop' => 'https://res.cloudinary.com/dcnts7jzv/image/upload/f_auto,q_auto/v1537819719/rush/mainCarousel/3/imageDesktop.jpg',
                    'imageMobile'  => 'https://res.cloudinary.com/dcnts7jzv/image/upload/f_auto,q_auto/v1537819719/rush/mainCarousel/3/imageMobile.jpg',
                    'image1x'      => '',
                    'image2x'      => '',
                    'image3x'      => '',
                    'title'        => 'Staff Picks',
                    'subtitle'     => 'Spirits',
                    'altText'      => '',
                    'buttonText'   => 'Click Here',
                    'buttonLink'   => '/spirits',
                    'deepLinkType' => 'categoryPage',
                    'deepLink'     => 'spirits',
                    'isVideo'      => false,
                ],
            ],
        ],
    ]);
});
