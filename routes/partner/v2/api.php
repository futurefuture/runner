<?php

Route::post('/login', 'AuthController@login')->name('partnerLogin');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('redshift/partners/{partner}/products/{product}/total-revenue', 'ProductController@getProductRevenue');
    Route::get('redshift/partners/{partner}/products/{product}/broad-category-rank', 'ProductController@getProductBroadRank');
    Route::get('redshift/partners/{partner}/products/{product}/sub-category-rank', 'ProductController@getProductSubRank');

    //product optimised route
    // Route::get('redshift/partners/{partnerId}/products/{productId}/page-views', 'ProductController@getPageViews');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/unique-page-views', 'ProductController@getUniquePageViews');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/add-to-cart', 'ProductController@getAddToCart');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/revenue', 'ProductController@getStoreRevenue');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/favourite', 'ProductController@getStoreFavourite');
    Route::get('redshift/partners/{partner}/products/{product}/total-page-views', 'ProductController@getTotalPageViews');
    Route::get('redshift/partners/{partner}/products/{product}/total-unique-page-views', 'ProductController@getTotalUniquePageViews');
    Route::get('redshift/partners/{partner}/products/{product}/total-add-to-cart', 'ProductController@getTotalAddToCart');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/store-purchases', 'ProductController@getStorePurchases');
    // Route::get('redshift/partners/{partnerId}/products/{productId}/store-quantity', 'ProductController@getStoreQuantity');
    Route::get('redshift/partners/{partner}/products/{product}/total-purchases', 'ProductController@getTotalPurchases');
    Route::get('redshift/partners/{partner}/products/{product}/total-quantity', 'ProductController@getTotalQuantity');

    // auth
    Route::post('/logout', 'AuthController@logout');

    // product sales
    // Route::get('/partners/{partnerId}/products/search', 'ProductController@index');
    // Route::get('/partners/{partnerId}/products/sales/export', 'ProductController@exportSales');
    // Route::get('/partners/{partnerId}/overview', 'PartnerOverviewController@index');
    // Route::get('/partners/{partnerId}/products/{productId}/', 'ProductController@show');
    // Route::get('/partners/{partnerId}/products', 'ProductController@index');
    // Route::get('/partners/{partnerId}/top/postalcode', 'DemographicController@index');
    // Route::get('/partners/{partnerId}/locations', 'DemographicController@getAllOrderLocation');
    // Route::get('/partners/{partnerId}/sales-by-gender', 'DemographicController@getSalesByGender');
    // Route::get('/partners/{partnerId}/sales-by-age', 'DemographicController@getSalesByAge');

    // product reviews
    Route::get('/partners/{partner}/products/{product}/reviews', 'ProductReviewController@index');

    // partner reviews
    Route::get('/partners/{partner}/reviews', 'PartnerReviewController@index');
    Route::get('/partners/{partner}/reviews/export', 'PartnerReviewController@reviewExport');
    // Route::get('/partners/{partner}/reviews/new', 'PartnerReviewController@getNewReviewsCount');

    // redshift
    Route::get('/partners/{partner}/products/sales/export', 'ProductController@exportSalesRedshift');
    Route::get('redshift/partners/{partner}/products/{product}', 'ProductController@getProductAnalytics');
    Route::get('redshift/partners/{partner}/products/{product}/sales-by-medium', 'ProductController@getSalesByMedium');
    Route::get('redshift/partners/{partner}/products/{product}/sales-by-campaign', 'ProductController@getSalesByCampaign');
    Route::get('redshift/partners/{partner}/products/{product}/sales-by-campaign-back-track', 'ProductController@getSalesByCampaignBackTrack');
    Route::get('redshift/partners/{partner}/products/{product}/session-by-source', 'ProductController@getSessionBySource');
    Route::get('redshift/partners/{partner}/products/{product}/impressions', 'ProductController@getProductImpressions');
    Route::get('redshift/partners/{partner}/products/{product}/top/postalcode', 'ProductController@getTopPostalCode');
    Route::get('redshift/partners/{partner}/products/{product}/sales-by-gender', 'ProductController@getSalesByGender');
    Route::get('redshift/partners/{partner}/products/{product}/sales-by-age', 'ProductController@getSalesByAge');
    // Route::get('redshift/partners/{partnerId}/overview', 'PartnerOverviewController@redShiftIndex');
    Route::get('/partners/{partner}/overview/product-sales', 'PartnerOverviewController@show');
    Route::get('redshift/partners/{partner}/products', 'ProductController@redshiftIndex');
    Route::get('redshift/partners/{partner}/categories', 'CategoryController@redShiftIndex');
    Route::get('/redshift/partners/{partner}/categories/sales/export', 'CategoryController@exportSales');
    Route::get('/redshift/partners/{partner}/top/postalcode', 'DemographicRedshiftController@redshiftIndex');
    // Route::get('/redshift/partners/{partnerId}/locations', 'DemographicRedshiftController@getAllRedshiftOrderLocation');
    Route::get('/redshift/partners/{partner}/sales-by-gender', 'DemographicRedshiftController@getRedshiftSalesByGender');
    Route::get('/redshift/partners/{partner}/sales-by-age', 'DemographicRedshiftController@getSalesByAge');

    // categories
    // Route::get('/partners/{partnerId}/categories/', 'CategoryController@index');

    // auth
    Route::post('/logout', 'AuthController@logout');

    //overview url
    Route::get('redshift/partners/{partner}/overview/total-sales', 'PartnerOverviewController@getTotalSales');
    Route::get('redshift/partners/{partner}/overview/total-order', 'PartnerOverviewController@getTotalOrder');
    Route::get('redshift/partners/{partner}/overview/total-customer', 'PartnerOverviewController@getTotalCustomer');
    // Route::get('redshift/partners/{partnerId}/overview/new-customers', 'PartnerOverviewController@getNewCustomers');
    Route::get('redshift/partners/{partner}/overview/top-products', 'PartnerOverviewController@getTopProducts');
    Route::get('redshift/partners/{partner}/overview/order-by-device', 'PartnerOverviewController@getOrderByDevice');
    Route::get('redshift/partners/{partner}/overview/sessions', 'PartnerOverviewController@getSessions');
    Route::get('redshift/partners/{partner}/overview/add-to-cart', 'PartnerOverviewController@getAddToCart');
    // Route::get('redshift/partners/{partnerId}/overview/checkout', 'PartnerOverviewController@getCheckout');
    Route::get('redshift/partners/{partner}/overview/transactions', 'PartnerOverviewController@getTransactions');
    Route::get('redshift/partners/{partner}/overview/sales-by-medium', 'PartnerOverviewController@getSalesByMedium');
    Route::get('redshift/partners/{partner}/overview/sales-by-campaign', 'PartnerOverviewController@getSalesByCampaign');
    Route::get('redshift/partners/{partner}/overview/sales-by-campaign-back-track', 'PartnerOverviewController@getSalesByCampaignBackTrack');
    Route::get('redshift/partners/{partner}/overview/top-product-pages', 'PartnerOverviewController@getTopProductPages');
    Route::get('redshift/partners/{partner}/overview/session-by-source', 'PartnerOverviewController@getSessionBySource');
    Route::get('redshift/partners/{partner}/overview/product-views', 'PartnerOverviewController@getProductViews');
    Route::get('redshift/partners/{partner}/overview/product-impressions', 'PartnerOverviewController@getTotalProductImpressions');
    Route::get('redshift/partners/{partner}/overview/order-by-hour', 'PartnerOverviewController@getOrderByHour');

    // partner campaigns
    Route::get('/partners/{partner}/campaigns', 'PartnerCampaignController@index');
    Route::get('/partners/{partner}/campaigns/{campaign}', 'PartnerCampaignController@show');
    Route::get('/partners/{partner}/campaigns/{campaign}/ads', 'PartnerCampaignAdController@index');
    Route::delete('/partners/{partner}/campaigns/{campaign}', 'PartnerCampaignController@delete');

    // ad types
    Route::get('/ad-types', 'AdTypeController@index');
    Route::get('/ad-types/{adType}', 'AdTypeController@show');

    // partner ads
    Route::get('/partners/{partner}/ads', 'PartnerAdController@index');
    Route::get('/partners/{partner}/ads/{ad}', 'PartnerAdController@show');
    Route::delete('/partners/{partner}/ads/{ad}', 'PartnerAdController@delete');
    Route::get('redshift/partners/{partner}/campaigns/{campaign}/ads', 'PartnerAdController@getAdsInformation');
    Route::get('redshift/partners/{partner}/campaigns/{campaign}/ads-total', 'PartnerAdController@getAdsTotalInformation');
});
