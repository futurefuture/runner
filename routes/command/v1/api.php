<?php

// command
Route::group(['middleware' => 'auth:api'], function () {
    // config
    Route::get('/config/92002993827773499200982773', 'ConfigController@index');
    Route::put('/config/92002993827773499200982773', 'ConfigController@update');

    // products
    Route::get('/products', 'ProductController@index');
    Route::get('/products/search/{query}', 'ProductController@search');
    Route::get('/products/{productId}', 'ProductController@show');
    Route::put('/products/{productId}', 'ProductController@update');

    // stores
    Route::get('/stores', 'StoreController@index');
    Route::get('/stores/{storeId}', 'StoreController@show');
    Route::put('/stores/{storeId}', 'StoreController@update');

    // overview
    Route::get('/temp-dash-numbers', 'OverviewController@getTempDashNumbers');

    // tags
    Route::get('/tags', 'TagController@index');
    Route::post('/tags', 'TagController@store');
    Route::delete('/tags/{tagId}', 'TagController@delete');

    // product tags
    Route::post('/products/{productId}/tags/{tagId}', 'ProductTagController@create');
    Route::put('/products/{productId}/tags/{tagId}', 'ProductTagController@update');
    Route::get('/products/{productId}/tags', 'ProductTagController@index');
    Route::delete('/products/{productId}/tags/{tagId}', 'ProductTagController@delete');

    // incentives
    Route::get('/incentives', 'IncentiveController@index');

    // product incentives
    Route::post('/products/{productId}/incentives', 'ProductIncentiveController@create');
    Route::get('/products/{productId}/incentives', 'ProductIncentiveController@index');
    Route::put('/products/{productId}/incentives/{productIncentiveId}', 'ProductIncentiveController@update');
    Route::delete('/products/{productId}/incentives/{productIncentiveId}', 'ProductIncentiveController@delete');

    // fcm test
    Route::post('/fcm', 'FCMController@store');

    // categories
    Route::get('/categories', 'CategoryController@index');

    // partners
    // Route::get('/partners', 'PartnerController@index');

    // product partners
    Route::post('/products/{productId}/partners', 'ProductPartnerController@create');
    Route::get('/products/{productId}/partners', 'ProductPartnerController@index');
    Route::delete('/products/{productId}/partners/{productPartnerId}', 'ProductPartnerController@delete');

    Route::post('/stores/{storeId}', 'CommandController@setStore')->middleware('auth:api');
    Route::post('/orders', 'CommandController@getOrders');
    Route::get('/orders/export', 'CommandController@exportOrders');
    Route::post('/pageviews', 'CommandController@pageviews');
    Route::post('/revenue', 'CommandController@revenue_profit');
    Route::post('/costs', 'CommandController@getCosts');
    Route::post('/dashboardcustomers', 'CommandController@dashboardCustomers');
    Route::post('/avgsession', 'CommandController@avgsession');
    Route::post('/bouncerate', 'CommandController@bouncerate');
    Route::post('/demographics', 'CommandController@demographics');
    Route::post('/customers', 'CommandController@getCustomers');
    Route::put('/customers/{id}', 'CommandController@updateCustomer');
    Route::post('/report/{category?}', 'CommandController@getReport');
    Route::post('/segment', 'SegmentController@getResult');
    Route::get('/segment/export', 'SegmentController@exportSegmentResults');
    Route::get('/employees', 'CommandController@getEmployees');
    Route::post('/employees/{id}', 'CommandController@getEmployee');
    Route::get('/invited', 'CommandController@getInvitedUsers');
    Route::post('/clients/{id}', 'OldPartnerController@getClientReport');
    Route::get('/clients/{id?}', 'OldPartnerController@getClients');
    Route::post('/ordersbydevice', 'CommandController@getOrdersbydevice');
    Route::delete('/products/{product}', 'CommandController@deleteProduct');
    Route::delete('/product_extend/{id}', 'CommandController@resetProduct');
    Route::post('/products/{product}/sale', 'OldProductController@GetProductSales');
    Route::post('/products/promotion', 'ProductController@newPromotion');
    Route::get('/products/{id}/promotions', 'OldProductController@getPromotions');
    Route::delete('/promotions/{id}', 'ProductController@removePromotion');
    Route::post('/products/{id}', 'CommandController@updateProduct');
    Route::delete('/runner-positions/{id}', 'CommandController@removeRunnerPositions');
    Route::post('/runner-positions', 'CommandController@newRunnerPosition');
    Route::put('/runner-positions/{id}', 'CommandController@updateRunnerPositions');
    Route::get('/runner-positions/{id?}', 'CommandController@getRunnerPositions');
    Route::put('/options/featured-products', 'OptionsController@updateFeaturedProducts');
    Route::post('/options/category-banners', 'OptionsController@updateCategoryBanners');
    Route::get('/options/category-banners', 'OptionsController@getCategoryBanners');
    Route::get('/options/featured-products', 'OptionsController@getFeaturedProductsCommand');
    Route::get('/options/categories-order', 'OptionsController@getCategoriesOrder');
    Route::put('/options/categories-order', 'OptionsController@updateCategoriesOrder');
    Route::post('/options/featured-products-report', 'OptionsController@saveFeatureProductReport');
    Route::post('/reviews/{reviewId}', 'ReviewController@verifyReview');
    Route::get('/reviews/{type?}', 'ReviewController@getReviews');
    Route::post('/coupons', 'CouponController@newCoupon');
    Route::get('/coupons/{id?}', 'CouponController@getCoupons');
    Route::put('/coupons/{coupon}', 'CouponController@updateCoupon');
});
