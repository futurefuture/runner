<?php

Route::group(['middleware' => 'isDispatch'], function () {
    // overview
    Route::get('/', 'PageController@stores');

    // products
    Route::get('/products', 'PageController@products');
    Route::get('/products/{id}', 'PageController@product');

    // stores
    Route::get('/stores', 'PageController@stores');
    Route::get('/stores/{storeId}', 'PageController@store');

    // tags
    Route::get('/tags', 'PageController@tags');

    // config
    Route::get('/config', 'PageController@config');

    Route::get('/numbers', 'CommandController@numbers');
    Route::get('/orders', 'CommandController@ordersIndex');
    Route::get('/invited-users', 'CommandController@invitedUsers');
    Route::get('/featured', 'CommandController@featuredProducts');
    Route::get('/employees', 'CommandController@employees');
    Route::get('/customers', 'CommandController@customers');
    Route::get('/employees/{id}', 'CommandController@employee');
    Route::get('/schedule', 'CommandController@schedule');
    Route::get('/reviews', 'CommandController@reviews');
    Route::get('/clients', 'CommandController@clients');
    Route::get('/clients/{id}', 'CommandController@client');
    Route::get('/links', 'CommandController@links');
    Route::get('/reports', 'CommandController@reports');
    Route::get('/segments', 'CommandController@segments');
});
