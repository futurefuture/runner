<?php

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::group(['middleware' => 'auth:api'], function () {
    // tasks
    Route::get('/couriers/{courierId}/tasks', 'TaskController@index');
    Route::get('/couriers/{courierId}/tasks/{taskId}', 'TaskController@show');
    Route::put('/couriers/{courier}/tasks/{task}', 'TaskController@update');
    Route::delete('/couriers/{courierId}/tasks/{taskId}', 'TaskController@delete');

    // order items
    Route::get('/couriers/{courierId}/order-items', 'OrderItemController@index');
    Route::get('/products/{id}', 'OrderController@getProduct');

    // courier
    Route::get('/couriers/{courierId}', 'CourierController@show');
    Route::put('/couriers/{courierId}', 'CourierController@update');

    // courier location
    Route::post('/couriers/{courierId}/coordinates', 'CourierCoordinateController@store');

    // users
    Route::get('/couriers/{courierId}/orders', 'CourierOrderController@index');
    Route::get('/couriers/{courierId}/order/{orderId}', 'CourierOrderController@show');
});
