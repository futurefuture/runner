<?php

Route::post('/login', 'CourierController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', 'CourierController@getAuthenticatedUser');
    Route::get('/schedules', 'CourierController@getSchedules');
    Route::get('/orders', 'CourierController@currentOrders');
    Route::get('/items', 'CourierController@getOrderItems');
    Route::get('/orders/past', 'CourierController@pastOrders');
    Route::get('/orders/past/{id}', 'CourierController@pastOrder');
    Route::put('/orders/{id}', 'CourierController@updateOrder');
    Route::get('/orders/{id}', 'CourierController@currentOrder');
    Route::post('/user/location', 'CourierController@updateLocation');
});
