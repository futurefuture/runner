<?php

// search
Route::get('/search', 'SearchController@index');

// auth
Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/logout', 'AuthController@logout');
});

// users
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{user}', 'UserController@show');
    Route::put('/users/{user}', 'UserController@update');
    Route::put('/users/{user}/update-password', 'UserController@updatePassword');
    Route::post('/users/{user}/refer-a-friend', 'UserController@referAFriend');
});

// user stripe
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{user}/stripe/customer', 'UserStripeController@getCustomer');
    Route::post('/users/{user}/stripe/customer/sources', 'UserStripeController@createSource');
    Route::put('/users/{user}/stripe/customer/sources/{sourceId}', 'UserStripeController@updateDefaultSource');
    Route::delete('/users/{user}/stripe/customer/sources/{sourceId}', 'UserStripeController@deleteSource');
});

// product reviews
Route::get('/products/{product}/reviews', 'ProductReviewController@index');
Route::group(['middleware' => 'auth:api'], function () {
    Route::put('/products/{product}/reviews/{review}', 'ProductReviewController@update');
    Route::post('/products/{product}/reviews', 'ProductReviewController@create');
});

// stores
Route::get('/stores/{store}', 'StoreController@show');

// inventory delivery times
// TODO - this should be reafactored into another controller
Route::get('/inventories/{inventory}/available-delivery-times', 'StoreController@getAvailableDeliveryTimesByInventory');

// storefronts
Route::get('/store-fronts/{inventory}', 'StoreFrontController@show');

// products
Route::get('/products/{product}', 'ProductController@show');

// inventory category products
Route::get('/inventories/{inventory}/categories/{category}/products', 'InventoryCategoryProductController@index');
Route::get('/inventories/{inventory}/categories/{category}/products/{product}', 'InventoryCategoryProductController@show');
Route::get('/inventories/{inventory}/categories/{category}/products/{product}/related-products', 'InventoryCategoryProductController@relatedProducts');

// categories
Route::get('/categories', 'CategoryController@index');
Route::get('/categories/{category}', 'CategoryController@show');
Route::get('categories/{category}/childCategories', 'CategoryController@getChildCategory');
Route::get('categories/{category}/parentCategories', 'CategoryController@getParentCategory');

// category postal code validation
Route::get('/address/validate', 'AddressController@validatePostalCode');

// postal codes
Route::post('/postal-codes/out-of-bounds', 'PostalCodeController@storeOutOfBoundsPostalCode');

// user inventory carts
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{user}/inventories/{inventory}/carts', 'UserInventoryCartController@index');
    Route::get('/users/{user}/inventories/{inventory}/carts/{cart}', 'UserInventoryCartController@show');
    Route::post('/users/{user}/inventories/{inventory}/carts', 'UserInventoryCartController@create');
    Route::put('/users/{user}/inventories/{inventory}/carts/{cart}', 'UserInventoryCartController@update');
});

// user orders
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{user}/orders', 'UserOrderController@index');
    Route::get('/users/{user}/orders/{order}', 'UserOrderController@show');
    Route::post('/users/{user}/orders/', 'UserOrderController@create');
    Route::post('/users/{user}/orders/{order}/re-order/', 'UserOrderController@reOrder');
});

// category stores validate
Route::get('/categories/{category}/stores', 'CategoryStoreController@index');

// user addresses
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/users/{user}/addresses', 'UserAddressController@create');
    Route::get('/users/{user}/addresses', 'UserAddressController@index');
    Route::put('/users/{user}/addresses/{address}', 'UserAddressController@update');
    Route::delete('/users/{user}/addresses/{address}', 'UserAddressController@delete');
});

// user favourites
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{user}/favourites', 'UserFavouriteController@index');
    Route::get('/users/{user}/favourites/by-store', 'UserFavouriteController@indexByStore');
    Route::post('/users/{user}/favourites', 'UserFavouriteController@create');
    Route::delete('/users/{user}/favourites/{favourite}', 'UserFavouriteController@delete');
});

// user reward points
Route::get('/users/{user}/reward-points', 'UserRewardPointController@index');

// category tags
Route::get('/category/{categoryId}/tags', 'CategoryTagController@index');

// fcm
Route::post('/fcm', '\App\Http\Controllers\Command\V1\FCMController@store');

// config
// TODO - this needs to be created for notifications and config v4
Route::get('/config/92002993827773499200982773', 'ConfigController@index');
Route::get('/config/92002993827773499200982773/global-notification', 'ConfigController@getGlobalNotification');

// brand commerce
Route::post('/client/login', 'AuthController@tokenGrant');

// refund
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/refund', 'RefundController@index');
    Route::get('/refund/{refund}', 'RefundController@show');
    Route::post('/refund/{refund}', 'RefundController@update');
    Route::delete('/refund/{refund}', 'RefundController@delete');
});

// survey
Route::get('/surveys', 'SurveyController@index');
Route::get('/surveys/{survey}', 'SurveyController@show');

// survey option
Route::get('/survey-options', 'SurveyOptionController@index');
Route::get('/surveys-options/{surveyOption}', 'SurveyOptionController@show');

// survey responses
Route::get('/surveys/{survey}/responses', 'SurveyResponseController@index');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/surveys/{survey}/responses', 'SurveyResponseController@create');
});

// adwords
Route::group(['middleware' => ['web']], function () {
    Route::get('/adwords-campaigns', 'AdWordsApiController@getCampaignsAction');
});
