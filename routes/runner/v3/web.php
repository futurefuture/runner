<?php

// redirects
Route::get('/explore', function () {
    return redirect('/');
});

// products
Route::get('/product/{productSlug}', 'PageController@product');

// orders
Route::get('/orders', 'PageController@orders');
Route::get('/orders/{id}', 'PageController@order');

// rewards
Route::get('/rewards', 'PageController@rewards');

// auth
Route::get('/login', 'PageController@newLogin');
Route::get('/register', 'PageController@newRegister');

// explore
Route::get('/', 'PageController@explore');

// checkout
Route::get('/checkout', 'PageController@checkout');

// confirmation
Route::get('/confirmation', 'PageController@confirmation')->name('confirmation');

// categories
Route::get('/category/{categories}', 'PageController@category')->where('categories', '^[a-zA-Z0-9-_\/]+$');

// tags
Route::get('/tag/{tagSlug}', 'PageController@tag')->where('tags', '^[a-zA-Z0-9-_\/]+$');

// search
// Route::get('/search', 'PageController@searchResults');
Route::get('/search', 'PageController@newSearchResults');
Route::get('/new-search', 'PageController@newSearchResults');

// about
Route::get('/about', 'PageController@about');

// terms and conditions
Route::get('/terms-conditions', 'PageController@termsConditions');

// privacy policy
Route::get('/privacy-policy', 'PageController@privacyPolicy');

// press and media
Route::get('/press-media', 'PageController@pressMedia');

// faq
Route::get('/faq', 'PageController@faq');

// account details
Route::get('/account-details', 'PageController@accountDetails');

// account settings
Route::get('/account-settings', 'PageController@accountSettings');

// refer a friend
Route::get('/refer-a-friend', 'PageController@referAFriend');

// payment information
Route::get('/payment-information', 'PageController@paymentInformation');

// brand pages
Route::get('/usual', 'PageController@usual');

// socialite
Route::get('login/{socialPlatform}', '\App\Http\Controllers\Runner\V4\AuthController@redirectToProvider');
Route::get('login/{socialPlatform}/callback', '\App\Http\Controllers\Runner\V4\AuthController@handleProviderCallback');

// out of box password
Route::get('/password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('/password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset');
Route::get('/password/reset/{token?}', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');

// sitemap
Route::get('/sitemap.xml', function () {
    return \File::get(public_path() . '/sitemap.xml');
});

// v4
// account details
Route::get('/account/addresses', 'PageController@addresses');
Route::get('/account/settings', 'PageController@newAccountSettings');
Route::get('/account/payment-methods', 'PageController@paymentMethods');
Route::get('/account/orders', 'PageController@newOrders');
Route::get('/account/orders/{order}', 'PageController@newOrder');
Route::get('/account/reward-points', 'PageController@newRewardPoints');
Route::get('/account/invite-friends', 'PageController@newInviteFriends');
// Route::get('/new-login', 'PageController@newLogin');
// Route::get('/new-register', 'PageController@newRegister');

// sop
Route::get('/sop', 'PageController@sop');

// landing pages
Route::get('/pizza-roulette', 'PageController@pizzaRoulette');
