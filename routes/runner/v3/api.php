<?php

Route::get('/users/{userId}/reward-points', 'RewardPointsController@getRewardPointsHistory')->middleware('auth:api');

// stores
Route::get('/stores/{storeId?}', 'StoreController@show');
Route::get('/stores/{storedId}/inventories/{inventoryId?}', 'StoreController@getInventories');
Route::get('/stores/{storeId}/featured-products', 'StoreController@getFeaturedProducts');
Route::get('/stores/{storeId}/available-delivery-times', 'StoreController@getAvailableDeliveryTimes');
Route::get('/inventories/{inventoryId}/available-delivery-times', 'StoreController@getAvailableDeliveryTimesByInventory');

// products
Route::get('/products/{productId}', 'ProductController@getProduct');

// inventories
Route::get('/inventories/{inventoryId?}', 'InventoryController@show');
Route::get('/inventories/{inventoryId?}/categories/{categoryId?}/products/{productId?}', 'InventoryController@getProducts');

// cart
Route::get('/users/{userId}/inventories/{inventoryId}/carts/{cartId?}', 'CartController@index');
Route::post('/users/{userId}/inventories/{inventoryId}/carts', 'CartController@create');
Route::put('/users/{userId}/inventories/{inventoryId}/carts/{cartId}', 'CartController@update');

// search
Route::get('/inventories/{inventoryId}/search/{searchString}', 'InventoryController@getSearch');

// auth
// TODO - JAREK none are being used client side yet
Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('/login/facebook', 'AuthController@facebookLogin');

// product reviews
// TODO - JAREK set to V3
Route::post('/products/{productId}/reviews', 'ProductReviewController@create')->middleware('auth:api');
Route::get('/products/{productId}/reviews', 'ProductReviewController@index');
Route::put('/products/{productId}/reviews/{reviewId}', 'ProductReviewController@update');

// payment
Route::post('/payment/create', 'PaymentController@create');

// categories
Route::get('/categories/', 'CategoryController@show');
Route::get('/categories/{categoryId?}/filters', 'CategoryController@getFilters');

// user
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users/{userId}', 'UserController@show');
    Route::put('/users/{userId}', 'UserController@update');
    Route::post('/logout', 'AuthController@logout');
});

// user favourites
Route::get('/users/{userId}/inventories/{inventoryId}/favourites', 'UserFavouriteController@index')->middleware('auth:api');
Route::post('/users/{userId}/inventories/{inventoryId}/favourites/{favouriteId}', 'UserFavouriteController@create')->middleware('auth:api');
Route::delete('/users/{userId}/inventories/{inventoryId}/favourites/{favouriteId}', 'UserFavouriteController@delete')->middleware('auth:api');

// user orders
Route::get('/users/{userId}/orders/', 'UserOrderController@index')->middleware('auth:api');
Route::get('/users/{userId}/orders/{orderId?}', 'UserOrderController@show')->middleware('auth:api');
Route::post('/users/{userId}/orders/{orderId}/re-order/', 'UserOrderController@reOrder')->middleware('auth:api');

// user stripe
Route::get('/users/{userId}/stripe/customer', 'UserStripeController@getCustomer')->middleware('auth:api');

// TODO -  JAREK create wrapper because all need token from stripe to create source
Route::post('/users/{userId}/stripe/customer/sources', 'UserStripeController@createSource')->middleware('auth:api');
Route::put('/users/{userId}/stripe/customer/sources/{sourceId}', 'UserStripeController@updateSource')->middleware('auth:api');
Route::delete('/users/{userId}/stripe/customer/sources/{sourceId}', 'UserStripeController@deleteSource')->middleware('auth:api');

// address postal codes
Route::get('/address/validate', 'PostalCodeController@getStores');
Route::post('/postal-codes/out-of-bounds', 'PostalCodeController@storeOutOfBoundsPostalCode');

// addresses
Route::post('/users/{userId}/addresses', 'AddressController@create');
Route::get('/users/{userId}/addresses', 'AddressController@index');
Route::put('/users/{userId}/addresses/{addressId}', 'AddressController@update');
Route::delete('/users/{userId}/addresses/{addressId}', 'AddressController@delete');

// config
Route::get('/config/92002993827773499200982773', 'ConfigController@index');

// refer a friend
Route::post('/users/{userId}/refer-a-friend', 'ReferAFriendController@create')->middleware('auth:api');

Route::get('/config/92002993827773499200982773/global-notification', 'ConfigController@getGlobalNotification');

Route::get('/order-report', 'ReportController@getTopFiveCategory');
