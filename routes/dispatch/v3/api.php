<?php

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::group(['middleware' => 'cors', 'dispatch.api'], function () {
    // orders
    Route::get('/orders', 'OrderController@index');
    Route::get('/orders/{order}', 'OrderController@show');
    Route::put('/orders/{order}', 'OrderController@update');
    Route::delete('/orders/{order}', 'OrderController@delete');

    Route::put('/order/{order}/cancel', 'OrderController@cancelOrder');
    Route::put('/orders/{order}/items', 'OrderController@updateItems');
    Route::post('/order/{order}/receipt', 'OrderController@sendReceipt');

    // users orders
    Route::get('/users/{user}/orders', 'UserOrderController@index');
    Route::get('/users/{user}/orders/{order}', 'UserOrderController@show');

    // tasks
    Route::get('/tasks', 'TaskController@index');
    Route::get('/tasks/{task}', 'TaskController@show');
    Route::post('/tasks', 'TaskController@create');
    Route::put('/tasks/{task}', 'TaskController@update');
    Route::delete('/tasks/{task}', 'TaskController@delete');
});
