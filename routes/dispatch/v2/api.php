<?php

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::group(['middleware' => 'cors', 'dispatch.api'], function () {
    // users
    Route::get('/users', 'UserController@index');
    Route::get('/users/{user}', 'UserController@show');
    Route::put('/users/{user}', 'UserController@update');
    Route::delete('/users/{UserId}', 'UserController@delete');
    Route::post('/login/{UserId}', 'UserController@loginAsUser');

    // orders
    Route::get('/orders', 'OrderController@index');
    Route::get('/orders/{order}', 'OrderController@show');
    Route::put('/orders/{order}', 'OrderController@update');
    Route::delete('/orders/{order}', 'OrderController@delete');

    Route::put('/order/{order}/cancel', 'OrderController@cancelOrder');
    Route::put('/orders/{order}/items', 'OrderController@updateItems');
    Route::post('/order/{order}/receipt', 'OrderController@sendReceipt');

    // order inventories
    Route::get('/orders/{order}/inventories', 'OrderInventoryController@index');

    // orders stripe
    Route::get('/orders/{order}/stripe/charge', 'OrderStripeController@show');

    // users orders
    Route::get('/users/{user}/orders', 'UserOrderController@index');
    Route::get('/users/{user}/orders/{order}', 'UserOrderController@show');

    // users addresses
    Route::get('/addresses', 'UserAddressController@index');
    Route::put('/users/{userId}/addresses/{addressId}', 'UserAddressController@update');

    // couriers
    Route::get('/couriers', 'CourierController@index');

    // stores
    Route::get('/stores', 'StoreController@index');

    // tasks
    Route::get('/tasks', 'TaskController@index');
    Route::get('/tasks/{task}', 'TaskController@show');
    Route::post('/tasks', 'TaskController@create');
    Route::put('/tasks/{task}', 'TaskController@update');
    Route::delete('/tasks/{task}', 'TaskController@delete');

    // schedules
    Route::get('/schedules', 'ScheduleController@index');
    Route::get('/schedules/{scheduleId}', 'ScheduleController@show');
    Route::post('/schedules', 'ScheduleController@store');
    Route::put('/schedules/{scheduleId}', 'ScheduleController@update');
    Route::delete('/schedules/{scheduleId}', 'ScheduleController@delete');
    Route::get('/employees', 'ScheduleController@getEmployees');
    Route::get('/runner-positions', 'ScheduleController@getCourierPositions');

    // courier locations
    Route::get('/courier-location', 'CourierLocationController@index');

    // customer notes
    Route::get('/user/{userId}/notes', 'UserNoteController@index');
    Route::get('/user/{userId}/notes/{noteId}', 'UserNoteController@show');
    Route::post('/user/{userId}/notes', 'UserNoteController@create');
    Route::put('/user/{userId}/notes/{noteId}', 'UserNoteController@update');
    Route::delete('/user/{userId}/notes/{noteId}', 'UserNoteController@delete');

    // customer tags
    Route::get('/user/{userId}/tags', 'UserTagController@index');
    Route::get('/user/{userId}/tags/{tagId}', 'UserTagController@show');
    Route::post('/user/{userId}/tags', 'UserTagController@create');
    Route::put('/user/{userId}/tags/{tagId}', 'UserTagController@update');
    Route::delete('/user/{userId}/tags/{tagId}', 'UserTagController@delete');

    // tags
    Route::get('/tags', 'TagController@index');
});
