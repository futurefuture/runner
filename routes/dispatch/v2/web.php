<?php

Route::get('/login', 'PageController@login');
Route::get('/manage', 'PageController@manage')->name('dispatch.v2.manage');
Route::get('/past-orders', 'PageController@pastOrders')->name('dispatch.v2.past-orders');
Route::get('/past-orders/{orderId}', 'PageController@pastOrder')->name('dispatch.v2.past-order');
Route::get('/orders/{$order}', 'PageController@orders')->name('dispatch.v2.order');
Route::get('/schedule', 'PageController@schedule')->name('dispatch.v2.schedule');
Route::get('/customers', 'PageController@customers')->name('dispatch.v2.customers');
Route::get('/customers/{customerId}', 'PageController@customer')->name('dispatch.v2.customer');

Route::group(['middleware' => 'isDispatch'], function () {
    Route::get('/orders', 'PageController@orders');
});
