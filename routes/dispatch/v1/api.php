<?php

Route::group([], function () {
    Route::get('/customers', 'CustomerController@getCustomers');
    Route::post('/login/{customerId}', 'CustomerController@loginAsCustomer');
    Route::delete('/customers/{customerId}', 'CustomerController@deleteCustomer');
    Route::get('/customers/search/{query}', 'CustomerController@getCustomers');
    Route::get('/customers/{customer}', 'CustomerController@getCustomer');
    Route::post('/customers/{customer}/addresses', 'CustomerController@saveAddress');
    Route::put('/customers/{customer}/password', 'CustomerController@changePassword');
    Route::put('/customers/{customer}', 'CustomerController@updateCustomer');
    Route::get('/customers/{customer}/orders', 'CustomerController@getCustomerOrders');
    Route::delete('/orders/{order}', 'AdminOrderController@deleteOrder');
    Route::get('/ordersloc', 'AdminOrderApiController@getCompleteOrdersLocation');
    Route::get('/orders/{query?}', 'AdminOrderApiController@getOrders');
    Route::post('/order/{order}/admin_notes', 'AdminOrderApiController@addNotes');
    Route::put('/order/{order}/cancel', 'AdminOrderApiController@cancelOrder');
    Route::put('/order/{orderId}/items', 'AdminOrderApiController@updateItems');
    Route::post('/order/{order}/receipt', 'AdminOrderApiController@sendReceipt');
    Route::put('/order/{order}', 'AdminOrderApiController@updateOrder');
    Route::get('/runners/{id?}', 'AdminOrderApiController@getRunner');
    Route::put('/assign_runners', 'AdminUserApiController@assignRunners');
    Route::delete('/schedules/{scheduleId}', 'ScheduleController@removeSchedule');
    Route::put('/schedules/{scheduleId}', 'ScheduleController@updateSchedule');
    Route::post('/schedules', 'ScheduleController@newSchedule');
    Route::get('/schedules/orders', 'ScheduleController@getScheduledOrders');
    Route::get('/schedules/{scheduleId?}', 'ScheduleController@getSchedules');
    Route::get('/employees', 'ScheduleController@getEmployees');
    Route::get('/runner-positions', 'ScheduleController@getRunnerPositions');
    Route::get('/runnners_onduty', 'AdminUserApiController@runnersOnDuty');
});
