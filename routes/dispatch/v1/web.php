<?php

Route::get('/login', 'AdminOrderController@login');
Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');
Route::get('/orders', 'AdminOrderController@index')->name('dispatch.orders');
Route::get('/schedule', 'ScheduleController@dispatchView');

Route::group(['middleware' => 'isDispatch'], function () {
    Route::get('/customers/{customer?}', 'AdminUserController@getCustomers');
    Route::post('/customers/{customerId}', 'AdminUserController@updateCustomer');
    Route::get('/customers/{customer}/{orderId?}', 'AdminOrderController@getCustomerOrders');
    Route::post('/assign_runners', 'AdminUserController@assignRunners');
    Route::get('/runners/{runner?}', 'AdminUserController@getRunners');
    Route::get('/runners/{runner}/{orderId}', 'AdminOrderController@getRunnerOrders');
    Route::get('/orders/{order}', 'AdminOrderController@order');
    Route::get('/orders/{order}/{status}', 'AdminOrderController@changeStatus');
});
