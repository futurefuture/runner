@extends('runner.app', [
  	'storeInfo' => [
    	'image' => $image ?? null,
    	'title' => null,
    	'description' => $description ?? null
  	]
])

@section('content')
<about page-attributes="{{ $pageAttributes }}"></about>
@endsection