<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    // check if is browser to allow page tests to pass
    if (isset($_SERVER['SERVER_NAME']) && ! empty($_SERVER['SERVER_NAME'])) {
        $domain = $_SERVER['SERVER_NAME'];
    } else {
        $domain = 'runner.test';
    }

    // check if is browser to allow page tests to pass
    if (isset($_SERVER['HTTP_HOST']) && ! empty($_SERVER['HTTP_HOST'])) {
        $subDomain = explode('.', $_SERVER['HTTP_HOST'])[0];
    } else {
        $subDomain = 'www';
    }

    $userIp = '';

    if (getenv('HTTP_CLIENT_IP')) {
        $userIp = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $userIp = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('HTTP_X_FORWARDED')) {
        $userIp = getenv('HTTP_X_FORWARDED');
    } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $userIp = getenv('HTTP_FORWARDED_FOR');
    } elseif (getenv('HTTP_FORWARDED')) {
        $userIp = getenv('HTTP_FORWARDED');
    } elseif (getenv('REMOTE_ADDR')) {
        $userIp = getenv('REMOTE_ADDR');
    } else {
        $userIp = 'UNKNOWN';
    }
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Account Page">
    <meta name="fragment" content="!">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Runner | Account</title>

    <!-- Facebook Open Graph -->
    <meta property="og:url" content="https://www.{{ $domain }}/{{ Request::path() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Runner Account">
    <meta property="og:description" content="Account">
    <meta property="og:image" content="">
    <meta property="fb:app_id" content="3799646ff09051589">

    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@getrunnerio">
    <meta name="twitter:title" content="Runner Account">
    <meta name="twitter:description" content="Account">
    <meta name="twitter:image" content="">

    <!-- Styles -->
    <link href="{{ mix('/css/v3/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ asset('assets/icons/Runner_Favicon.png') }}}">

    <!-- Scripts -->
    <script>
        window.domain = <?php echo json_encode($domain) ?>;
        window.userIp = <?php echo json_encode([
                            $userIp
                        ]) ?>;
        window.Laravel = <?php echo json_encode([
                                'csrfToken' => csrf_token(),
                            ]); ?>;
    </script>

    <!-- Google Data Layer -->
    <script>
        window.dataLayer = [];
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WBT4MHK');
    </script>
    <!-- End Google Tag Manager -->

    @if ((App::environment('production')))
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1820994367914109');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1820994367914109&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    @endif
    <!-- Start of runnerhelp Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=b105aa69-078f-46c4-b3d9-c8e2f8370a1a"> </script>
    <!-- End of runnerhelp Zendesk Widget script -->

    @if ((App::environment('production')))
    <script>
        !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
        analytics.load("gEj2tuuyTyzdm0SrWaaijyDkTYSJPKPT");
        analytics.page();
        }}();
    </script>
    @endif
    @if ((App::environment('local')) || (App::environment('staging')))
   <script>
        !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
        analytics.load("JcHCnvq605Nn6fmWvU1koPaoYudI5AJS");
        analytics.page();
        }}();
    </script>
    @endif
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WBT4MHK" height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Branch App Link -->
    <div class="branch-journeys-top"></div>
    <div id="app">
        @yield('content')
    </div>

    <!-- Stripe -->
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>

    <!-- Adobe TypeKit Fonts -->
    <script src="https://use.typekit.net/bgl2llc.js"></script>
    <script>
        try {
            Typekit.load({
                async: true
            });
        } catch (e) {}
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>​

    <script src="{{ mix('/runner/js/app.js') }}"></script>
</body>

</html>