@extends('runner.layouts.account')

@section('content')
<new-payment-methods page-attributes="{{ $pageAttributes ?? '' }}" />
@endsection