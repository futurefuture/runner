@extends('runner.app', [
	'storeInfo' => [
		'image' => $storeInfo['image'],
		'title' => 'Runner | Rewards',
		'description' => 'Reward points are converted in $$ and you can earn them in so many ways!'
	]
])

@section('content')
<postal-code-modal domain="{{ $domain }}"></postal-code-modal>
<cart-V3 domain="{{ $domain }}" sub-domain="www"></cart-v3>
<main-menu-v3 domain="{{ $domain }}" sub-domain="www"></main-menu-v3>
<store-menu domain="{{ $domain }}" sub-domain="www"></store-menu>
<favourites></favourites>
<search-v3 sub-domain="www"></search-v3>
<fixed-header-v3 domain="{{ $domain }}" sub-domain="www" store-id="1"></fixed-header-v3>
<rewards domain="{{ $domain }}"></rewards>
<main-footer />
@endsection