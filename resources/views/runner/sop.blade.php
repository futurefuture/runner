@extends('runner.app', [
'storeInfo' => [
'image' => $storeInfo['image'] ?? null,
'title' => 'Runner | SOP',
'description' => 'Standard operating procedures for Couriers'
]
])

@section('content')
<sop page-attributes="{{ $pageAttributes }}"></sop>
@endsection