@extends('runner.app', [
    'storeInfo' => [
        'image' => $storeInfo['image'] ?? null,
        'title' => 'Runner | Checkout',
        'description' => 'Register for Runner'
    ]
])

@section('content')
<checkout-v3 page-attributes="{{ $pageAttributes }}"></checkout-v3>
@endsection