@extends('runner.app', [
	'storeInfo' => [
		'image' => $storeInfo['image'] ?? null,
		'title' => 'Runner | Terms & Conditions',
		'description' => 'Yea yea, boring....But it has to be here'
	]
])

@section('content')
<terms-conditions page-attributes="{{ $pageAttributes }}" />
@endsection