@extends('runner.app', [
'storeInfo' => [
'image' => '',
'title' => json_decode($pageAttributes)->store->title . ' | ' . json_decode($pageAttributes)->tag->title,
'description' => json_decode($pageAttributes)->tag->title ?? null
]
])

@section('content')
<tags page-attributes="{{ $pageAttributes }}" />
@endsection