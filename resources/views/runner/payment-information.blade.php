@extends('runner.app')

@section('title', 'Payment Information')

@section('description', 'User Payment Information')

@section('content')
	<postal-code-modal domain="{{ $domain }}"></postal-code-modal>
	<cart-V3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></cart-v3>
	<main-menu-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></main-menu-v3>
	<store-menu domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></store-menu>
	<favourites></favourites>
	<search-v3 sub-domain="{{ $subDomain }}"></search-v3>
	<fixed-header-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}" store-id="{{ $storeId }}"></fixed-header-v3>
	<payment-information domain="{{ $domain }}" sub-domain="{{ $subDomain }}" store-id="{{ $storeId }}"></payment-information>
  <main-footer/>
@endsection