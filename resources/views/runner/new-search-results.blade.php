@extends('runner.app', [
  	'storeInfo' => [
    	'image' => $image ?? null,
    	'title' => null,
    	'description' => $description ?? null
  	]
])

@section('content')
<new-search-results page-attributes="{{ $pageAttributes }}"></new-search-results>
@endsection