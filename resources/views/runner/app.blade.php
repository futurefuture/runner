<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    // check if is browser to allow page tests to pass
    if (isset($_SERVER['SERVER_NAME']) && ! empty($_SERVER['SERVER_NAME'])) {
        $domain = $_SERVER['SERVER_NAME'];
    } else {
        $domain = 'runner.test';
    }

    // check if is browser to allow page tests to pass
    if (isset($_SERVER['HTTP_HOST']) && ! empty($_SERVER['HTTP_HOST'])) {
        $subDomain = explode('.', $_SERVER['HTTP_HOST'])[0];
    } else {
        $subDomain = 'www';
    }

    $userIp = '';

    if (getenv('HTTP_CLIENT_IP')) {
        $userIp = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $userIp = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('HTTP_X_FORWARDED')) {
        $userIp = getenv('HTTP_X_FORWARDED');
    } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $userIp = getenv('HTTP_FORWARDED_FOR');
    } elseif (getenv('HTTP_FORWARDED')) {
        $userIp = getenv('HTTP_FORWARDED');
    } elseif (getenv('REMOTE_ADDR')) {
        $userIp = getenv('REMOTE_ADDR');
    } else {
        $userIp = 'UNKNOWN';
    }
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{ $storeInfo['description'] }}">
    <meta name="fragment" content="!">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $storeInfo['title'] }}</title>

    <!-- Facebook Open Graph -->
    <meta property="og:url" content="https://{{ $subDomain }}.{{ $domain }}/{{ Request::path() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $storeInfo['title'] }}">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:image" content="{{ $storeInfo['image'] }}">
    <meta property="fb:app_id" content="3799646ff09051589">

    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@getrunnerio">
    <meta name="twitter:title" content="{{ $storeInfo['title'] }}">
    <meta name="twitter:description" content="@yield('description')">
    <meta name="twitter:image" content="{{ $storeInfo['image'] }}">

    <!-- Styles -->
    <link href="{{ mix('/css/v3/app.css') }}" rel="stylesheet">

    @if ((App::environment('production')))
    <link rel="icon" type="image/png" href="{{{ asset('assets/icons/RunnerLogoSquare-06.png') }}}">
    @else
      <link rel="icon" type="image/png" href="{{{ asset('assets/icons/RunnerLogoSquareBlack.png') }}}">
    @endif

    <!-- Scripts -->
    <script>
        window.domain = <?php echo json_encode($domain) ?>;
        window.userIp = <?php echo json_encode([
                            $userIp
                        ]) ?>;
        window.Laravel = <?php echo json_encode([
                                'csrfToken' => csrf_token(),
                            ]); ?>;
    </script>
    @if ((App::environment('production')))
    <script>
        !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
        analytics.load("gEj2tuuyTyzdm0SrWaaijyDkTYSJPKPT");
        analytics.page();
        }}();
    </script>
    @endif
    @if ((App::environment('local')) || (App::environment('staging')))
   <script>
        !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
        analytics.load("JcHCnvq605Nn6fmWvU1koPaoYudI5AJS");
        analytics.debug(false);
        analytics.page();
        }}();
    </script>
    @endif

    <!-- Branch -->
    <script type="text/javascript">
        // load the Branch SDK file
        (function(b, r, a, n, c, h, _, s, d, k) {
            if (!b[n] || !b[n]._q) {
                for (; s < _.length;) c(h, _[s++]);
                d = r.createElement(a);
                d.async = 1;
                d.src = "https://cdn.branch.io/branch-latest.min.js";
                k = r.getElementsByTagName(a)[0];
                k.parentNode.insertBefore(d, k);
                b[n] = h
            }
        })(window, document, "script", "branch", function(b, r) {
            b[r] = function() {
                b._q.push([r, arguments])
            }
        }, {
            _q: [],
            _v: 1
        }, "addListener applyCode banner closeBanner creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode".split(" "), 0);
        branch.init('key_live_fmztIKaob3DC3uqKtVwi4oaptFpVoozO');
        // define the deepview structure
        branch.deepview({
            'channel': 'mobile_web',
            'feature': 'deepview'
        }, {
            'open_app': true // If true, Branch attempts to open your app immediately when the page loads. If false, users will need to press a button. Defaults to true
        });
    </script>

    <!-- Structured Data -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "url": "https://wwww.getrunner.io",
            "name": "Runner",
            "dateCreated": "",
            "creator": "",
            "keywords": "Liquor Delivery, Toronto, Alcohol, Wine, Beer, Spirits, On-Demand, Uber, LCBO Delivery, LCBO"
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "LiquorStore",
            "@id": "https://www.getrunner.io",
            "email": "support@getrunner.io",
            "priceRange": "$0000 - $1000",
            "Description": "Toronto online alcohol delivery. Toronto beer, wine and spirits delivered to your door. Get your alcohol delivered today.",
            "areaServed": "Toronto",
            "name": "Runner",
            "legalName": "Runner Inc.",
            "telephone": "16473608262",
            "logo": "https://www.getrunner.io/assets/icons/Logo_Square_Beta.svg",
            "openingHoursSpecification": [{
                    "@type": "OpeningHoursSpecification",
                    "closes": "05:00:00",
                    "dayOfWeek": "http://schema.org/Sunday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Saturday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Friday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Thursday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Wednesday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Tuesday",
                    "opens": "11:00:00"
                },
                {
                    "@type": "OpeningHoursSpecification",
                    "closes": "22:00:00",
                    "dayOfWeek": "http://schema.org/Monday",
                    "opens": "11:00:00"
                }
            ],
            "image": "https://www.getrunner.io/assets/icons/Logo_Square_Beta.svg",
            "url": "https://www.getrunner.io",
            "address": "Toronto"
        }
    </script>

    <!-- Google Data Layer -->
    <script>
        window.dataLayer = [];
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WBT4MHK');
    </script>
    <!-- End Google Tag Manager -->

    @if ((App::environment('production')))
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1820994367914109');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1820994367914109&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    @endif
    <!-- Start of runnerhelp Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=b105aa69-078f-46c4-b3d9-c8e2f8370a1a"> </script>
    <!-- End of runnerhelp Zendesk Widget script -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WBT4MHK" height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Branch App Link -->
    <div class="branch-journeys-top"></div>
    <div id="app">
        @yield('content')
    </div>

    <!-- Stripe -->
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>

    <!-- Adobe TypeKit Fonts -->
    <script src="https://use.typekit.net/bgl2llc.js"></script>
    <script>
        try {
            Typekit.load({
                async: true
            });
        } catch (e) {}
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>​

    <script src="{{ mix('/runner/js/app.js') }}"></script>
</body>

</html>