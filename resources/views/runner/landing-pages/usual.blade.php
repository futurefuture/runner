@extends('runner.app', [
	'storeInfo' => [
		'image' => $storeInfo['image'] ?? null,
		'title' => 'Runner | Usual',
		'description' => 'Usual'
	]
])

@section('content')
<usual page-attributes="{{ $pageAttributes }}"/>
@endsection