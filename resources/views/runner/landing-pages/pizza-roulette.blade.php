@extends('runner.app', [
'storeInfo' => [
'image' => $storeInfo['image'] ?? null,
'title' => 'Runner | Pizza Roulette',
'description' => 'Pizza Roulette'
]
])

@section('content')
<pizza-roulette page-attributes="{{ $pageAttributes }}" />
@endsection