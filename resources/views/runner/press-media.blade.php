@extends('runner.app', [
	'storeInfo' => [
		'image' => $storeInfo['image'],
		'title' => $storeId == 1 ? 'Runner | Press & Media' : $storeInfo['title'] . ' | ' . 'Runner | Press & Media',
		'description' => 'Check out some of the cool press that we\'ve recieved from some amazing outlets!'
	]
])

@section('content')
<postal-code-modal domain="{{ $domain }}"></postal-code-modal>
<cart-V3 domain="{{ $domain }}" sub-domain="www"></cart-v3>
<main-menu-v3 domain="{{ $domain }}" sub-domain="www"></main-menu-v3>
<store-menu domain="{{ $domain }}" sub-domain="www"></store-menu>
<favourites></favourites>
<search-v3 sub-domain="www"></search-v3>
<fixed-header-v3 domain="{{ $domain }}" sub-domain="www" store-id="1"></fixed-header-v3>
<press-media domain="{{ $domain }}"></press-media>
<main-footer />
@endsection