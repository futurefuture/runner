@extends('runner.app', [
    'storeInfo' => [
        'image' => $storeInfo['image'],
        'title' => 'Runner | Register',
        'description' => 'Register for Runner'
    ]
])

@section('content')
<register domain="{{ $domain }}" user="{{ $user }}" inviting-user="{{ $invitingUser }}"></register>
<main-footer></main-footer>
@endsection