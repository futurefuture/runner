@extends('runner.app', [
	'storeInfo' => [
		'image' => $storeInfo['image'],
		'title' => $storeId == 1 ? 'Runner | Refer A Friend' : $storeInfo['title'] . ' | ' . 'Runner | Refer A Friend',
		'description' => 'Recommand Runner to your friend and earn Reward Points!'
	]
])

@section('content')
<postal-code-modal domain="{{ $domain }}"></postal-code-modal>
<cart-V3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></cart-v3>
<main-menu-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></main-menu-v3>
<store-menu domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></store-menu>
<favourites></favourites>
<search-v3 sub-domain="{{ $subDomain }}"></search-v3>
<fixed-header-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}" store-id="{{ $storeId }}"></fixed-header-v3>
<account-referral domain="{{ $domain }}" sub-domain="{{ $subDomain }}" store-id="{{ $storeId }}"></account-referral>
<main-footer />
@endsection