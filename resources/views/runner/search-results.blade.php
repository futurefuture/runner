@extends('runner.app', [
    'storeInfo' => [
        'image' => $storeInfo['image'],
        'title' => $storeId == 1 ? 'Runner | Search' : $storeInfo['title'] . ' | ' . 'Runner | Search',
        'description' => 'Select from thousands of alcohol products!'
    ]
])

@section('content')
<cart-V3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></cart-v3>
<main-menu-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></main-menu-v3>
<store-menu domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></store-menu>
<favourites></favourites>
<search-v3></search-v3>
<fixed-header-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}" store-id="{{ $storeId }}"></fixed-header-v3>
<search-results-v3 domain="{{ $domain }}" sub-domain="{{ $subDomain }}"></search-results-v3>
<main-footer></main-footer>
@endsection