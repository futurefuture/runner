@extends('runner.app', [
  	'storeInfo' => [
    	'image' => json_decode($pageAttributes)->store->blockImage ?? null,
    	'title' => 'Runner | ' . json_decode($pageAttributes)->store->title ?? 'Runner',
    	'description' => json_decode($pageAttributes)->store->description ?? null
  	]
])

@section('content')
<explore page-attributes="{{ $pageAttributes }}"/>
@endsection