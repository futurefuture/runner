@extends('runner.app', [
	'storeInfo' => [
		'image' => json_decode($pageAttributes)->store->blockImage ?? null,
		'title' => json_decode($pageAttributes)->store->title . ' | ' . 'Runner | Confirmation',
		'description' => 'Order confirmation.'
	]
])

@section('content')
<confirmation page-attributes="{{ $pageAttributes }}" />
@endsection