@extends('runner.app', [
    'storeInfo' => [
        'image' => '',
        'title' => 'Runner | Register',
        'description' => 'Login to your Runner account.'
    ]
])

@section('content')
<register page-attributes="{{ $pageAttributes }}" />
@endsection