@extends('runner.app', [
  	'storeInfo' => [
    	'image' => $image ?? null,
    	'title' => null,
    	'description' => $description ?? null
  	]
])

@section('content')
<addresses page-attributes="{{ $pageAttributes }}" />
@endsection