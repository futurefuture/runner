@extends('runner.app', [
  	'storeInfo' => [
    	'image' => $image ?? null,
    	'title' => null,
    	'description' => $description ?? null
  	]
])

@section('content')
<account-settings page-attributes="{{ $pageAttributes }}" />
@endsection