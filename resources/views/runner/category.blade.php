@extends('runner.app', [
  	'storeInfo' => [
    	'image' => '',
    	'title' => json_decode($pageAttributes)->store->title . ' | ' . json_decode($pageAttributes)->category->title,
    	'description' => json_decode($pageAttributes)->category->description ?? null
  	]
])

@section('content')
<category page-attributes="{{ $pageAttributes }}"/>
@endsection