@extends('runner.app', [
  	'storeInfo' => [
    	'image' => json_decode($pageAttributes)->product->image ?? null,
    	'title' => json_decode($pageAttributes)->store->title . ' | ' . json_decode($pageAttributes)->product->title . ' | ' . json_decode($pageAttributes)->product->packaging,
    	'description' => json_decode($pageAttributes)->product->long_description
  	]
])

@section('content')
<product page-attributes="{{ $pageAttributes }}" />
@endsection