@extends('runner.app', [
    'storeInfo' => [
        'image' => '',
        'title' => 'Runner | Login',
        'description' => 'Login to your Runner account.'
    ]
])

@section('content')
<login page-attributes="{{ $pageAttributes }}" />
@endsection