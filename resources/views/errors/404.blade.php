@extends('runner.app', [
  	'storeInfo' => [
    	'image' => $image ?? null,
    	'title' => null,
    	'description' => $description ?? null
  	]
])

@section('content')
<error-not-found page-attributes="{{ $pageAttributes }}"/>
@endsection

