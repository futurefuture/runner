@extends('runner.app')

@section('title')
    Reset Password
@endsection

@section('content')
    <div id="password-reset">
        <div style="max-width: 800px; margin: 0 auto;" class="py-30">
            <div>
                <h1 class="text-xl">
                    Reset Password
                </h1>
            </div>
            <form class="flex flex-col" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <input class="h-50 rounded px-15 appearance-none border mb-30" id="email" type="email" placeholder="Email" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="r-reset__text">
                        {{ $errors->first('email') }}
                    </span>
                @endif
                <input class="h-50 rounded px-15 appearance-none border mb-30" id="password" type="password" placeholder="Password" name="password" required>
                @if ($errors->has('password'))
                    <div class="notification notification--warning">
                        <span>
                            {{ $errors->first('password') }}
                        </span>
                    </div>
                @endif
                <input class="h-50 rounded px-15 appearance-none border mb-30" id="password-confirm" type="password" placeholder="Confirm Password" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <div class="notification notification--warning">
                        <span>
                            {{ $errors->first('password_confirmation') }}
                        </span>
                    </div>
                @endif
                <input type="submit" class="h-50 rounded px-15 bg-green text-white text-lg font-bold" value="Reset Password" />
            </form>
        </div>
    </div>
    <main-footer />
@endsection