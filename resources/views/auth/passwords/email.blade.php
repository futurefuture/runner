@extends('runner.app', [
    'storeInfo' => [
    'image' => '',
    'title' => 'Password Reset',
    'description' => ''
    ]
])

@section('content')
<div id="send-password-reset">
    <div style="max-width: 800px; margin: 0 auto;" class="py-30">
        <div>
            <h1 class="text-xl">
                Send Reset Password Link
            </h1>
        </div>
        @if (session('status'))
        <p>
            {{ session('status') }}
        </p>
        😁
        @endif

        @if (!session('status'))
        <form class="flex flex-col" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}
            <input class="h-50 rounded px-15 appearance-none border mb-30" id="email" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
            <div class="notification notification--warning">
                <span>
                    {{ $errors->first('email') }}
                </span>
            </div>
            @endif
            <input type="submit" class="h-50 rounded px-15 bg-green text-white text-lg font-bold" value="Send Password Reset" />
        </form>
        @endif
    </div>
</div>
<main-footer />
@endsection