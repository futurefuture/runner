@extends('dispatch.app')

@section('content')
    <dispatch-header></dispatch-header>
    <dispatch-nav></dispatch-nav>
    <dispatch-customers></dispatch-customers>
@endsection