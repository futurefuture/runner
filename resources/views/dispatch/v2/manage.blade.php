@extends('dispatch.v2.app')

@section('content')
<manage orders="{{ $orders }}" scheduled_orders="{{ $scheduled_orders }}" tasks="{{ $tasks }}" domain="{{ $domain }}"></manage>
@endsection