@extends('dispatch.v2.app')

@section('content')
<order-page order-id="{{ $orderId }}" domain="{{ $domain }}"></order-page>

@endsection