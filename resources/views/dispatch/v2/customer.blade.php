@extends('dispatch.v2.app')

@section('content')
<customer customer-id="{{ $customerId }}" domain="{{ $domain }}"></customer>
@endsection