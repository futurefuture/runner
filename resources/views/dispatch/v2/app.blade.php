<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="@yield('description')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ mix('/css/dispatchv2.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ asset('assets/icons/Runner_Favicon.png') }}}">
    <?php
        // check if is browser to allow page tests to pass
        if (isset($_SERVER['SERVER_NAME']) && ! empty($_SERVER['SERVER_NAME'])) {
            $domain = $_SERVER['SERVER_NAME'];
        } else {
            $domain = 'runner.test';
        }

        // check if is browser to allow page tests to pass
        if (isset($_SERVER['HTTP_HOST']) && ! empty($_SERVER['HTTP_HOST'])) {
            $subDomain = $_SERVER['HTTP_HOST'];
        } else {
            $subDomain = '';
        }

    ?>
    <!-- Scripts -->
    <script>
        window.domain = <?php echo json_encode($domain) ?>;
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;

        // window.dataLayer = window.dataLayer || [];
    </script>

    <!-- Stripe -->
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Adobe TypeKit Fonts -->
    <script src="https://use.typekit.net/bgl2llc.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <script src="{{ mix('/dispatchV2/js/app.js') }}"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>​
</body>
</html>
