@extends('dispatch.app')

@section('content')
	@if(gettype($order) === 'string')
		{{ $order }}
	@else
		<dispatch-customers-order></dispatch-customers-order>
	@endif
@endsection