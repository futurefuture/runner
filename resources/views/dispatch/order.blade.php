@extends('dispatch.app')

@section('content')
	<dispatch-header></dispatch-header>
	<dispatch-nav></dispatch-nav>
	<dispatch-order id={{ $order }}></dispatch-order>
@endsection