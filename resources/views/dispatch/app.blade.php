<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="@yield('description')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ mix('/css/bulma.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/dispatch.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/v3/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ asset('assets/icons/Runner_Favicon.png') }}}">
    <?php
        // check if is browser to allow page tests to pass
        if (isset($_SERVER['SERVER_NAME']) && ! empty($_SERVER['SERVER_NAME'])) {
            $domain = $_SERVER['SERVER_NAME'];
        } else {
            $domain = 'runner.test';
        }
    ?>
    <!-- Scripts -->
    <script>
        window.domain = <?php echo json_encode($domain) ?>;
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;

        // window.dataLayer = window.dataLayer || [];
    </script>

    <!-- Stripe -->
    <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{ mix('/dispatchV1/js/app.js') }}"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
</body>
</html>
