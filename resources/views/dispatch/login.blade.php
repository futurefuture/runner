@extends('dispatch.app')

@section('title', 'Dispatch Login')

@section('content')
    <div class="bg-grey-lighter w-full grid justify-center content-center" style="height: 100vh;">
        <form method="post" class="bg-white p-30" style="width: 400px;" action="{{ url('/dispatch/login') }}">
        {{ csrf_field() }}
            <div class="mb-15">
                <input type="email" placeholder="email" name="email" class="h-50 px-15 appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" />
            </div>
            <div class="mb-15">
                <input placeholder="password" type="password" name="password" class="h-50 px-15 appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" />
            </div>
            <input type="submit" name="login" class="h-50 bg-red text-white font-bold py-2 px-4 rounded w-full cursor-pointer" value="Login" />
        </form>
    </div>
@endsection