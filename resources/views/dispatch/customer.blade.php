@extends('dispatch.app')

@section('content')
    <dispatch-header></dispatch-header>
    <dispatch-nav></dispatch-nav>
    <dispatch-customer domain="{{ $domain }}" customerid={{ $customer }}></dispatch-customer>
@endsection