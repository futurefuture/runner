@component('mail::message')
# Order Receipt #{{ $order->id }} @if($type) - Revised @endif

@component('mail::panel')
Delivery Details
@endcomponent

@component('mail::table')
|           | 
| --------- |
| **Delivery Time**<br />{{ $order->delivered_at }} |
| **Customer Name**<br />@if (isset($order->customer->firstName)) {{ $order->customer->firstName }} @else {{ $order->customer->first_name }} @endif @if (isset($order->customer->lastName)) {{ $order->customer->lastName }} @else {{ $order->customer->last_name }} @endif |
| **Address**<br />@if (isset($order->unit_number)) {{ $order->unit_number }} - {{ $order->address->address }} @else {{ $order->address->address }} @endif                                   |
| **Email**<br />{{ $order->customer->email }}                                      |
| **Phone Number**<br />@if (isset($order->customer->phoneNumber)) {{ $order->customer->phoneNumber }} @else {{ $order->customer->phone_number }} @endif                              |
| **Instructions**<br />@if (isset($order->instructions)) {{ $order->instructions }} @else None @endif |
| **Delivery Time**<br />@if(!$order->schedule_time) ASAP @else {{ $order->schedule_time }} @endif |
| **Your Runner**<br />{{ $runners }} |
@if($order->gift)
| **Recipient**<br />{{ $order->gift->first_name }} {{ $order->gift->last_name }} |
| **Recipient Address**<br />{{ isset($order->gift->unit_number) ? $order->gift->unit_number .' - '. $order->gift->address: $order->gift->address }} |
| **Recipient Email**<br />{{ $order->gift->email }} |
| **Recipient Phone Number**<br />{{ $order->gift->phone_number }} |
| **Recipient Instructions**<br />{{ $order->gift->instructions }} |
| **Personal Note**<br />{{ $order->gift->message }} |
@endif

@endcomponent

@component('mail::panel')
Order Items
@endcomponent

@component('mail::table')
|               |          |           | 
| ------------- | -------- | --------: |
@foreach($order->content as $item)
| @if (isset($item->qty)) {{ $item->qty }} @else {{ $item->quantity }} @endif | @if (isset($item->name)) {{ $item->name }} @else {{ $item->title }} @endif<br /> @if (isset($item->package)) {{ $item->package }} @else {{ $item->packaging }} @endif | ${{ property_exists($item, 'runner_price') ? number_format($item->runner_price, 2) : number_format($item->runnerPrice / 100, 2) }} |
@endforeach
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| Subtotal      | ${{ number_format(($order->subtotal / 100), 2) }} |
| Delivery      | ${{ number_format(($order->delivery / 100), 2) }} |
@if($order->serviceFee)
| Service Fee      | ${{ number_format(($order->serviceFee / 100), 2) }} |
@endif
| Tax           | ${{ number_format(($order->display_tax_total / 100), 2) }} |
@if($order->discount > 0)
| Discount      | ${{ number_format(($order->discount / 100), 2) }} |
@endif
| Tip           | ${{ number_format(($order->tip / 100), 2) }} |
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| **Total**         | **${{ number_format(($order->total / 100), 2) }}** |           |
@endcomponent

@component('mail::panel')
**Your Signature**<br />
@if( $order->signature )<img src="{{ 'https://s3.amazonaws.com/order-signatures/'.$order->id.'.png' }}" alt=""> @else No Signature @endif
@endcomponent

@component('mail::button', ['url' => 'https://www.getrunner.io/explore'])
Continue Shopping
@endcomponent

@component('mail::button', ['url' => 'https://www.getrunner.io/orders/'.$hashId.'/receipt'])
Get Receipt
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[<img src="https://res.cloudinary.com/dcnts7jzv/image/upload/v1542731573/runner/stores/confirmationFooter.jpg
">](https://www.getrunner.io/refer-a-friend)
@endcomponent
