<!DOCTYPE html>
<!-- email send to courier with order -->
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <style type="text/css" rel="stylesheet" media="all">
            /* Media Queries */
            @media only screen and (max-width: 500px) {
                .button {
                    width: 100% !important;
                }
            }
        </style>
    </head>

<?php
    $style = [
        /* Layout ------------------------------ */

        'body' => 'margin: 0; padding: 0; width: 100%; background-color: #FFFFFF;',
        'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #FFFFFF;',

        /* Masthead ----------------------- */

        'email-masthead' => 'padding: 25px 0; text-align: center;',
        'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #323C46; text-decoration: none; text-shadow: 0 1px 0 white;',

        'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
        'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
        'email-body_cell' => 'padding: 35px;',

        'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
        'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

        /* Body ------------------------------ */

        'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
        'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

        /* Type ------------------------------ */

        'anchor' => 'color: #F83E40; font-weight: 600;',
        'header-1' => 'margin-top: 0; color: #323C46; font-size: 19px; font-weight: bold; text-align: left;',
        'paragraph' => 'margin-top: 0; color: #9B9B9B; font-size: 16px; line-height: 1.5em;',
        'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
        'paragraph-center' => 'text-align: center;',

        /* Buttons ------------------------------ */

        'button' => 'box-sizing: border-box; display: block; display: inline-block; width: 200px; min-height: 60px; background-color: #3869D4; color: #ffffff; font-size: 15px; line-height: 60px; text-align: center; text-decoration: none; text-transform: uppercase; -webkit-text-size-adjust: none;',

        'button--green' => 'background-color: #22BC66;',
        'button--red' => 'background-color: #F83E40;',
        'button--blue' => 'background-color: #323C46;',
    ];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

    <body style="{{ $style['body'] }}">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="{{ $style['email-wrapper'] }}" align="center">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <!-- Logo -->
                        <tr>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 25px;">
                                        <a style="{{ $fontFamily }} {{ $style['email-masthead_name'] }}" href="{{ url('/') }}" target="_blank">
                                            <img style="width: 100%; max-width: 150px;" src="https://www.getrunner.io/assets/icons/Runner_Red_Long.png" alt="">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </tr>

                        <!-- Email Body -->
                        <tr>
                            <td style="{{ $style['email-body'] }}" width="100%">
                                <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                            <h1 style="{{ $style['header-1'] }}">
                                                Order #: {{ $order->id }}
                                            </h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">

                                            <!-- Intro -->
                                            <h1 style="{{ $style['header-1'] }}">
                                               Order Details
                                            </h1>
                                            <p style="{{ $style['paragraph'] }}">
                                                {{ $order->content->customer->first_name }} {{ $order->content->customer->last_name }}
                                            </p>
                                            <p style="{{ $style['paragraph'] }}">
                                                @if(isset($order->content->instructions->unitno))
                                                    {{ $order->content->instructions->unitno }}
                                                @endif
                                                    {{ $order->content->address }}
                                            </p>
                              
											<h1 style="{{ $style['header-1'] }}">
                                               Contact Information
                                            </h1>
                                            <p style="{{ $style['paragraph'] }}">
                                            	<a href="tel:{{ $order->content->customer->phone_number }}">   {{ $order->content->customer->phone_number }}
                                                </a>
                                            </p>
                                            <p style="{{ $style['paragraph'] }}">
                                            	<a href="mailto:{{ $order->content->customer->email }}">
                                                    {{ $order->content->customer->email }}
                                                </a>
                                            </p>

                                            <h1 style="{{ $style['header-1'] }}">
                                               Delivery Details
                                            </h1>

											@if(isset($order->content->instructions->instruction))
												<p style="{{ $style['paragraph'] }}">
                                                    {{ $order->content->instructions->instruction }}
                                                </p>
											@else
												<p style="{{ $style['paragraph'] }}">
                                                    No Delivery Instructions
                                                </p>
											@endif

                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <p style="{{ $style['paragraph'] }}">
                                                            Summary
                                                        </p>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @foreach($order->content->items as $item)
                                                    <tr>
                                                        <td>
                                                            <p style="{{ $style['paragraph'] }}">
                                                                {{ $item->qty }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p style="{{ $style['paragraph'] }}">
                                                                <img src="{{ $item->thumbnail }}" alt="" height="70px" width="auto">
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p style="{{ $style['paragraph'] }}">
                                                                <a href="https://www.getrunner.io/products/{{ $item->primary_category }}/{{ $item->id }}" target="_blank">
                                                                    {{ $item->name }}<br />
                                                                    {{ $item->package }}
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p style="{{ $style['paragraph'] }}">
                                                                ${{ $item->true_price }}
                                                            </p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- Footer -->
                        <tr>
                            <td>
                                <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                            <p style="{{ $style['paragraph-sub'] }}">
                                                &copy; {{ date('Y') }}
                                                <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">
                                                    {{ config('app.name') }}
                                                </a>.
                                                All rights reserved.
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>






