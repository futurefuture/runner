@component('mail::message')
Hi {{ $recentUser->first_name }}!

We noticed that you signed up for Runner and didn't end up using the service. If there is any particular reason why, we would love to know! We are a young company, and any insight is extremely valuable to us.

Please shoot us a note at [support@getrunner.io](support@getrunner.io)

If you DO decide to use our service in the future, please feel free to use coupon code:

#HKDIIKAF1

This will waive our $10 delivery fee. Thank you for taking a look at our service, and hope to be delivering to you soon!

Love, the Runner Team

{{ config('app.name') }} - [https://www.getrunner.io](https://www.getrunner.io)

@endcomponent
