@component('mail::message')
# {{ $user->first_name }} Thinks Runner Is For You!

Hey it's your good friend {{ $user->first_name }}!<br /><br /> 

I've been using Runner to have alcohol delivered to my door - thought you'd love it too.<br /><br /> 

Collect your **$10** off by clicking the button below:

@component('mail::button', ['url' => $invite_url])
Register Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
