@component('mail::message')
# Hi {{ $user->first_name }}!

Thanks for registering for Runner :)<br /><br />
Head on over to our store by clicking the button below!

@component('mail::button', ['url' => 'https://www.getrunner.io'])
Shop Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[<img src="{{ asset('/assets/v2/Give-10-Get-10.jpg') }}">](https://www.getrunner.io/refer-a-friend)
@endcomponent
