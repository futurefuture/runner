@component('mail::message')
# Order Confirmation #{{ $order->id }}
{{ $order->created_at }}

Thanks for choosing Runner, **{{ $order->customer->first_name }}**<br />

Your order has been placed and should be on its way soon.<br />

@component('mail::panel')
Delivery Details
@endcomponent

@component('mail::table')
|           | 
| --------- |
| **Customer Name**<br />{{ $order->customer->first_name }} {{ $order->customer->last_name }} |
| **Email**<br />{{ $order->customer->email }}                                      |
| **Phone Number**<br />{{ $order->customer->phone_number }}                               |
| **Instructions**<br />@if (isset($order->instructions)) {{ $order->instructions }} @else None @endif |
| **Delivery Time**<br />@if(!$order->schedule_time) ASAP @else {{ $order->schedule_time }} @endif |
@if($order->gift)
| **Recipient**<br />{{ $order->gift->first_name }} {{ $order->gift->last_name }} |
| **Recipient Address**<br />{{ isset($order->gift->unit_number) ? $order->gift->unit_number .' - '. $order->gift->address->printable_address: $order->gift->address->printable_address }} |
| **Recipient Email**<br />{{ $order->gift->email }} |
| **Recipient Phone Number**<br />{{ $order->gift->phone_number }} |
| **Recipient Instructions**<br />{{ $order->gift->instructions }} |
| **Personal Note**<br />{{ $order->gift->message }} |
@endif

@endcomponent

@component('mail::panel')
Order Items
@endcomponent

@component('mail::table')
|               |          |           |          | 
| ------------- | -------- | --------: | -------- |
@foreach($order->content as $item)
| {{ $item->qty }} | {{ $item->name }}<br /> {{ $item->package }} | ${{ property_exists($item, 'runner_price') ? number_format($item->runner_price, 2) : number_format($item->price, 2) }} | {{ $item->allowSub ? 'Allow Substitution' : 'Don\'t Allow Substitution' }}
@endforeach
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| Subtotal      | ${{ number_format(($order->subtotal / 100), 2) }} |
| Delivery      | ${{ number_format(($order->delivery /100), 2) }} |
@if($order->serviceFee)
| Service Fee      | ${{ number_format(($order->serviceFee / 100), 2) }} |
@endif
| Tax           | ${{ number_format(($order->display_tax_total / 100), 2) }} |
@if($order->discount > 0)
| Discount      | ${{ number_format(($order->discount / 100), 2) }} |
@endif
| Tip           | ${{ number_format(($order->tip / 100), 2) }} |
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| **Total**         | **${{ number_format(($order->total / 100), 2) }}** |           |
@endcomponent

Though we generally only take about 1 hour or so for delivery, please be advised that depending on the time and weather conditions, orders can sometimes take up to about 2 hours.<br />

If you need to make a change, or have noticed something wrong with the order, please just contact us by clicking the button below:
@component('mail::button', ['url' => 'mailto:service@getrunner.io'])
Contact Us
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[![refer](https://res.cloudinary.com/dcnts7jzv/image/upload/v1542731573/runner/stores/confirmationFooter.jpg
)](https://www.getrunner.io/.refer-a-friend)
@endcomponent
