@component('mail::message')
# Order Cancellation #{{ $order->id }}

Thanks for **ALMOST** ordering with Runner, **{{ $user->first_name }}**<br />

**:)**

Your order has been cancelled. We hope that you have a great day, and that you think of choosing Runner again in the future!<br />

@component('mail::panel')
Delivery Details
@endcomponent

@component('mail::table')
|           | 
| --------- |
| **Customer Name**<br />{{ $user->first_name }} {{ $user->last_name }} |
| **Address**<br />@if (isset($address->unit_number)) {{ $address->unit_number }} - {{ $address->formatted_address }} @else {{ $address->formatted_address }} @endif                                   |
| **Email**<br />{{ $user->email }}                                      |
| **Phone Number**<br />{{ $user->phoneNumber }}                               |
| **Instructions**<br />@if (isset($order->instructions)) {{ $order->instructions }} @else None @endif |
| **Delivery Time**<br />@if(!$order->schedule_time) ASAP @else {{ $order->schedule_time }} @endif |
@endcomponent

@component('mail::panel')
Order Items
@endcomponent

@component('mail::table')
|               |          |           | 
| ------------- | -------- | --------: |
@foreach($order->content as $item)
| {{ $item->quantity }} | {{ $item->title }}<br /> {{ $item->packaging }} | ${{ number_format(($item->runnerPrice / 100), 2) }} |
@endforeach
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| Subtotal      | ${{ number_format(($order->subtotal / 100), 2) }} |
| Delivery      | ${{ number_format(($order->delivery / 100), 2) }} |
@if($order->service_fee != 0.00)
| Gift Fee      | ${{ number_format(($order->service_fee / 100), 2) }} |
@endif
| Tax           | ${{ number_format(($order->display_tax_total / 100), 2) }} |
@if($order->discount > 0)
| Discount      | ${{ number_format(($order->discount / 100), 2) }} |
@endif
| Tip           | ${{ number_format(($order->tip / 100), 2) }} |
@endcomponent

@component('mail::table')
|               |           | 
| ------------- | --------: |
| **Total**         | **${{ number_format(($order->total / 100), 2) }}** |           |
@endcomponent

If this cancellation was an accident or you need to make a change, please just contact us by clicking the button below:
@component('mail::button', ['url' => 'mailto:service@getrunner.io'])
Contact Us
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[<img src="{{ asset('/assets/v2/Give-10-Get-10.jpg') }}">](https://www.getrunner.io/refer-a-friend)
@endcomponent

