@component('mail::message')
Hi {{ $user->first_name }}!

We still have you covered! Your cart items are safe and sound. To make your decision THAT MUCH EASIER, take $5 OFF your next purchase. Use the coupon code below at checkout, it's your ticket to the savings fast lane: 

#CARTSAVE

Love the Runner team,

@component('mail::table')
|               |               |           | |
| ------------- | --------: | --------: |   --------: |
@foreach($products as $p)
| {{ $p->quantity }} | <img src="{{ $p->image }}" width="50" /> | {{ $p->title }} | ${{ number_format(($p->runnerPrice / 100), 2) }} |
@endforeach
@endcomponent

@component('mail::button', ['url' => 'https://www.getrunner.io'])
Take Me Back
@endcomponent


[https://www.getrunner.io](https://www.getrunner.io)

{{ config('app.name') }}
@endcomponent
