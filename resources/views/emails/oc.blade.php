@component('mail::message')
# Scheduled Order Order Confirmation #{{ $order->id }}
{{ $order->schedule_time }}

Thanks for choosing Runner, **{{ $order->customer->firstName }}**<br />

Your order has been placed and should be on its way soon.<br />

@component('mail::panel')
Delivery Details
@endcomponent

@component('mail::table')
| |
| --------- |
| **Customer Name**<br />{{ $order->customer->firstName }} {{ $order->customer->lastName }} |
| **Email**<br />{{ $order->customer->email }} |
| **Phone Number**<br />{{ $order->customer->phoneNumber }}
@if (!$order->gift)
| **Address**<br />{{ json_decode($order->address)->address }} |
| **Unit Number**<br />@if (isset($order->unit_number)) {{ $order->unit_number }} @else None @endif |
| **Instructions**<br />@if (isset($order->instructions)) {{ $order->instructions }} @else None @endif |
@endif
| **Delivery Time**<br />@if(!$order->schedule_time) ASAP @else {{ $order->schedule_time }} @endif |
| **Card Type**<br />@if(!$order->card_type) Unknown @else {{ $order->card_type }} @endif |
| **Card Last Four**<br />@if(!$order->card_last_four) **** @else {{ $order->card_last_four }} @endif |
@endcomponent

@if ($order->gift)
@component('mail::table')
| |
| --------- |
| **Recipient Name**<br />{{ $order->gift->first_name }} {{ $order->gift->last_name }} |
| **Email**<br />{{ $order->gift->email }} |
| **Phone Number**<br />{{ $order->gift->phone_number }} |
| **Address**<br />{{ json_decode($order->address)->address }} |
| **Unit Number**<br />@if (isset($order->unit_number)) {{ $order->unit_number }} @else None @endif |
| **Instructions**<br />@if (isset($order->instructions)) {{ $order->instructions }} @else None @endif |
| **Message**<br />{!! nl2br(e($order->gift->message)) !!} |
@endcomponent
@endif

@component('mail::panel')
Order Items
@endcomponent

@component('mail::table')
| | | | |
| ------------- | -------- | --------: | -------- |
@foreach($order->content as $item)
| {{ $item->quantity }} | {{ $item->title }}<br /> {{ $item->packaging }} | ${{ number_format(($item->runnerPrice / 100), 2) }} | {{ $item->allowSub ? 'Allow Substitution' : 'Don\'t Allow Substitution' }}
@endforeach
@endcomponent

@component('mail::table')
| | |
| ------------- | --------: |
| Subtotal | ${{ number_format(($order->subtotal / 100), 2) }} |
| Delivery | ${{ number_format(($order->delivery / 100), 2) }} |
| Tax | ${{ number_format(($order->display_tax_total / 100), 2) }} |
@if($order->discount > 0)
| Discount | ${{ number_format(($order->discount / 100), 2) }} |
@endif
@if($order->service_fee > 0)
| Service Fee | ${{ number_format(($order->service_fee / 100), 2) }} |
@endif
| Tip | ${{ number_format(($order->tip / 100), 2) }} |
@endcomponent

@component('mail::table')
| | |
| ------------- | --------: |
| **Total** | **${{ number_format(($order->total / 100), 2) }}** | |
@endcomponent

Please keep your phone close by, we will be calling you if any of your products do not exist. Because the LCBO is asking our couriers to leave quickly, we will be calling once.

Though we generally only take about 1 hour or so for delivery, please be advised that given the current circumstances, orders can sometimes take up to about 3 hours.<br />

If you need to make a change, or have noticed something wrong with the order, please just contact us by clicking the button below:
@component('mail::button', ['url' => 'mailto:service@getrunner.io'])
Contact Us
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[![refer](https://res.cloudinary.com/dcnts7jzv/image/upload/v1542731573/runner/stores/confirmationFooter.jpg
)](https://www.getrunner.io/.refer-a-friend)
@endcomponent