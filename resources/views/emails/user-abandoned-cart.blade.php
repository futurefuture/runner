@component('mail::message')
Hi {{ $user->first_name }}!

You spent time carefully selecting your cart items, with precision... and care - what taste! We certainly don't want any of that effort to go to waste. 

We have your cart saved, it's listed below! Click the button beneath your items to get right back to where you left off. You keep doing you.

Love the Runner team,

@component('mail::table')
|               |               |           | |
| ------------- | --------: | --------: |   --------: |
@foreach($products as $p)
| {{ $p->quantity }} | <img src="{{ $p->image }}" width="50" /> | {{ $p->title }} | ${{ number_format(($p->runnerPrice / 100), 2) }} |
@endforeach
@endcomponent

@component('mail::button', ['url' => 'https://www.getrunner.io'])
Take Me Back
@endcomponent


[https://www.getrunner.io](https://www.getrunner.io)

{{ config('app.name') }}
@endcomponent
