@component('mail::message')
#Daily Status For {{ $date }}

### Hours
@component('mail::table')
| **Runner** | **Start** | **Finish** |
| :------------: | :------------: | :-------------: |
@foreach($scheduledRunners as $runner)
| {{ $runner->name }} | {{ $runner->start }} | {{ $runner->end }} |
@endforeach
@endcomponent

### Order Counts
@component('mail::table')
| | |
| :------------ | :------------ |
| Orders Created | {{ $general['ordersCreated'] }} |
| Orders Delivered | {{ $general['ordersDelivered'] }} |
| Orders Cancelled | {{ $general['ordersCancelled'] }} |
@endcomponent

### Finances
@component('mail::table')
| | |
| :------------ | :------------ |
| Cost Of Goods Sold | {{ $finances['costOfGoodsSold'] }} |
| Markup | {{ $finances['markup'] }} |
| Service Fees | {{ $finances['serviceFees'] }} |
| Delivery Fees | {{ $finances['deliveryFees'] }} |
| Tips | {{ $finances['tips'] }} |
| **Revenue** | **{{ $finances['revenue'] }}** |
@endcomponent

@component('mail::table')
| | |
| :------------ | :------------ |
| Cost Of Goods Sold | {{ $finances['costOfGoodsSold'] }} |
| Runner Costs | {{ $finances['runnerCosts'] }} |
| Dispatch Costs | {{ $finances['dispatchCosts'] }} |
| Discounts | {{ $finances['discounts'] }} |
| Stripe Fees | {{ $finances['stripeFees'] }} |
| Tips | {{ $finances['tips'] }} |
| **Expenses** | **{{ $finances['costOfGoodsSold'] + $finances['runnerCosts'] + $finances['dispatchCosts']  + $finances['discounts'] + $finances['stripeFees'] + $finances['tips'] }}** |
@endcomponent

@component('mail::table')
| | |
| :------------ | :------------ |
| Revenue | {{ $finances['revenue'] }} |
| Expenses | {{ $finances['costOfGoodsSold'] + $finances['runnerCosts'] + $finances['dispatchCosts'] + $finances['discounts'] + $finances['stripeFees'] + $finances['tips'] }} |
| Net Revenue | **{{ $finances['revenue'] - ($finances['costOfGoodsSold'] + $finances['runnerCosts'] + + $finances['dispatchCosts'] + $finances['discounts'] + $finances['stripeFees'] + $finances['tips']) }}** |
@endcomponent

### Order Item Discrepancies
@component('mail::table')
| **Order ID** | **Order Content** | **Order Items Created** |
| :------------: | :------------: | :-------------: |
@foreach($ordersWithOrderItemDiscrepancies as $o)
| {{ $o->id }} | {{ $o->c }} | {{ $o->oi }} |
@endforeach
@endcomponent

@endcomponent
