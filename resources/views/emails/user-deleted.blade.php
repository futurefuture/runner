@component('mail::message')
# Hi {{ $user->first_name }}!

We're extremely sorry to see you leave :(<br /><br />

Of course we'd love to know why you are choosing to leave, so please feel free to let us know if you feel like it.

Otherwise, we wish you all the best!

Thanks,<br>
{{ config('app.name') }}
@endcomponent
