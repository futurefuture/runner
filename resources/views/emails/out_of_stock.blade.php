@component('mail::message')
# {{ $product->name . ' - ' . $product->package }} is back in stock!

<img src="{{ $product->image_url }}" alt="">

@component('mail::button', ['url' => env('APP_URL').'/'.$product->primary_category.'/'.$product->slug ])
See Product
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
