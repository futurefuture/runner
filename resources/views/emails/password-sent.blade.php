@component('mail::message')
# Hi {{ $user->first_name }}!

Your password has been reset to:<br />

** {{ $password }} **

@component('mail::button', ['url' => '/explore'])
Shop Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
[<img src="{{ asset('/assets/v2/Give-10-Get-10.jpg') }}">](https://www.getrunner.io/refer-a-friend)
@endcomponent
