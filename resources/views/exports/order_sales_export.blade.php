<table>

    <thead>
        <tr>
            <th>id</th>
            <th>user_id</th>
            <th>charge</th>
            <th>runner_1</th>
            <th>subtotal</th>
            <th>cogs</th>
            <th>discount</th>
            <th>delivery</th>
            <th>markup</th>
            <th>display_tax_total</th>
            <th>tip</th>
            <th>total</th>
            <th>stripe_fee</th>
            <th>created_at</th>
            <th>delivered_at</th>
            <th>address</th>
        </tr>

    </thead>
    <tbody>

        @foreach($orderSales as $sales)

        <tr>

            <td>
                {{ $sales['id'] }}
            </td>
            <td>
                {{ $sales['user_id']}}
            </td>
            <td>
                {{ $sales['charge']}}
            </td>

            <td>
                {{ $sales['runner_1']}}
            </td>
            <td>
                {{ $sales['subtotal']}}
            </td>
            <td>
                {{ $sales['cogs']}}
            </td>
            <td>
                {{ $sales['discount']}}
            </td>
            <td>
                {{ $sales['delivery']}}
            </td>
            <td>
                {{ $sales['markup']}}
            </td>
            <td>
                {{ $sales['display_tax_total']}}
            </td>
            <td>
                {{ $sales['tip']}}
            </td>
            <td>
                {{ $sales['total']}}
            </td>
            <td>
                {{ $sales['stripe_fee']}}
            </td>
            <td>
                {{ $sales['created_at']}}
            </td>
            <td>
                {{ $sales['delivered_at']}}
            </td>
            <td>
                {{ $sales['address_id']}}
            </td>
        </tr>

        @endforeach
    </tbody>
</table>