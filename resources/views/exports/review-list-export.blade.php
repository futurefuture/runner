<table>
    <thead>
        <tr>
            <th>Ratings</th>
            <th>Product</th>
            <th>Date </th>
            <th>Review</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reviews as $review)
        <tr>
            <td>
                {{ $review['rating'] }}
            </td>
            <td>
                {{ $review['product']}}
            </td>
            <td>
                {{ $review['date']}}
            </td>
            <td>
                {{ $review['review']}}
            </td>
            <td>
                {{ $review['name']}}
            </td>

        </tr>
        @endforeach
    </tbody>
</table>