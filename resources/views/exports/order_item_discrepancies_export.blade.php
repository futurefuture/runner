<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Items</th>
            <th> Order Items</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orderItemDiscrepancies as $discrepancy)
        <tr>
            <td>
                {{ $discrepancy->id }}
            </td>
            <td>
                {{ $discrepancy->items }}
            </td>
            <td>
                {{ $discrepancy->orderItem }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>