<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Packaging</th>
            <th>Producer Name</th>
            <th>Price</th>
            <th>Units Sold</th>
            <th>Total Sold</th>
        </tr>
    </thead>
    <tbody>
        @foreach($productsWithTotalSold as $product)
        <tr>
            <td>
                {{ $product['id'] }}
            </td>
            <td>
                {{ $product['title'] }}
            </td>
            <td>
                {{ $product['packaging'] }}
            </td>
            <td>
                {{ $product['producer'] }}
            </td>
            <td>
                {{ $product['retailPrice'] / 100 }}
            </td>
            <td>
                {{ $product['unitsSold'] }}
            </td>
            <td>
                {{ $product['totalSold'] / 100 }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
