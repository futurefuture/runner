<table>
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Status
            </th>
            <th>
                Discount
            </th>
            <th>
                Actual Total
            </th>
            <th>
                Total
            </th>
            <th>
                Actual Cogs
            </th>
            <th>
                Cogs
            </th>
            <th>
                Difference
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $o)
        <tr>
            <td>
                {{ $o['id'] }}
            </td>
            <td>
                {{ $o['status'] }}
            </td>
            <td>
                {{ $o['discount'] }}
            </td>
            <td>
                {{ $o['actualTotal'] }}
            </td>
            <td>
                {{ $o['total'] }}
            </td>
            <td>
                {{ $o['actualCogs'] }}
            </td>
            <td>
                {{ $o['cogs'] }}
            </td>
            <td>
                {{ $o['difference'] }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>