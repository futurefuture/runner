<table>
    <thead>
        <tr>
            <th>
                All Sales
            </th>
            <th>
                {{ $timeRange }}
            </th>
        </tr>
    </thead>
</table>
<table>
    <thead>
        <tr>
            <th>Primary Category</th>
            <th>Secondary Category</th>
            <th>Price Band</th>
            <th>Sales</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Tequila
            </td>
            <td></td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Reposado
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['reposado']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['reposado']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['reposado']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['reposado']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Mixto
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $15.00 - $19.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['1500-1995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mixto']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Mezcal
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mezcal']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mezcal']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $100+
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['mezcal']['100+'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Blanco
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['blanco']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['blanco']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['blanco']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['blanco']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $100+
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['blanco']['100+'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Anejo
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['anejo']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['anejo']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['anejo']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $100+
            </td>
            <td>
                {{ $partnerSalesByPriceBand['tequila']['anejo']['100+'] }}
            </td>
        </tr>
        <tr>
            <td>
                Whiskey
            </td>
            <td></td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Bourbon/American
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $15.00 - $19.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['1500-1995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $59.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['bourbonAmericanWhiskey']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Canadian
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $15.00 - $19.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['1500-1995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['whiskey']['canadianWhisky']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td>
                Vodka
            </td>
            <td></td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                Unflavoured
            </td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $15.00 - $19.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['1500-1995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['vodka']['unflavouredVodka']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td>
                Cognac/Armagnac
            </td>
            <td></td>
            <td></td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                VS
            </td>
            <td></td>
            <td>
               {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $100+
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['vs']['100+'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                XO
            </td>
            <td></td>
            <td>
               {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['totalSales'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $20.00 - $29.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['2000-2995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $30.00 - $39.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['3000-3995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $40.00 - $49.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['4000-4995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $50.00 - $99.95
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['5000-9995'] }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                $100+
            </td>
            <td>
                {{ $partnerSalesByPriceBand['cognacArmagnac']['xo']['100+'] }}
            </td>
        </tr>
    </tbody>
</table>
