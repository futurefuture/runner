<table>

    <thead>
        <tr>
            <th>Day</th>
            <th>Total Order</th>
            <th>Retail Price</th>
            <th>Markup</th>
            <th>Delivery</th>
            <th>tip</th>
            <th>Discount</th>
        </tr>

    </thead>
    <tbody>

        @foreach($torontoSouthSales as $sales)

        <tr>

            <td>
                {{ $sales['day'] }}
            </td>
            <td>
                {{ $sales['totalOrder']}}
            </td>
            <td>
                {{ $sales['retailPrice']}}
            </td>

            <td>
                {{ $sales['markup']}}
            </td>
            <td>
                {{ $sales['delivery']}}
            </td>
            <td>
                {{ $sales['tip']}}
            </td>
            <td>
                {{ $sales['discount']}}
            </td>
        </tr>

        @endforeach
    </tbody>
</table>