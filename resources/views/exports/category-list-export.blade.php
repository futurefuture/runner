<table>
    <thead>
        <tr>
            <th>Category</th>
            <th>Partner Sales($)</th>
            <th>Runner Sales($)</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categoryWithTotalSold as $category)
        <tr>
            <td>
                {{ $category['title'] }}
            </td>
            <td>
                {{ $category['partnerSales']}}
            </td>
            <td>
                {{ $category['runnerSales']}}
            </td>

        </tr>
        @endforeach
    </tbody>
</table>