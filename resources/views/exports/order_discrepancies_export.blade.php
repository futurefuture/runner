<table>
    <thead>
        <tr>
            <th>Order ID</th>
            <th>COGS</th>
            <th>Content Total</th>
            <th>SubTotal</th>
            <th>Created At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orderDiscrepancies as $discrepancy)
        <tr>
            <td>
                {{ $discrepancy['order_id'] }}
            </td>
            <td>
                {{ $discrepancy['cogs']}}
            </td>
            <td>
                {{ $discrepancy['contentTotal']}}
            </td>
            <td>
                {{ $discrepancy['subtotal']}}
            </td>
            <td>
                {{ $discrepancy['created_at']}}
            </td>

        </tr>
        @endforeach
    </tbody>
</table>