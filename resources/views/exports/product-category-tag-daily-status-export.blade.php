<table>
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Order ID
            </th>
            <th>
                SKU
            </th>
            <th>
                slug
            </th>
            <th>
                Title
            </th>
            <th>
                Category 1
            </th>
            <th>
                Category 2
            </th>
            <th>
                Category 3
            </th>
            <th>
                Tag 1
            </th>
            <th>
                Tag 2
            </th>
            <th>
                Tag 3
            </th>
            <th>
                Tag 4 (vintage/craft)
            </th>
            <th>
                Tag 5 (organic)
            </th>
            <th>
                Country Tag (france)
            </th>
            <th>
                Varietal (pinot-grigio)
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $item)
        <tr>
            <td>
                {{ $item->id }}
            </td>
            <td>
                {{ $item->orderId }}
            </td>
            <td>
                {{ $item->sku }}
            </td>
            <td>
                {{ $item->slug }}
            </td>
            <td>
                {{ $item->title }}
            </td>
            <td>
                {{ $item->categories[0] ?? ''}}
            </td>
            <td>
                {{ $item->categories[1] ?? ''}}
            </td>
            <td>
                {{ $item->categories[2] ?? ''}}
            </td>
            <td>
                {{ $item->tags[0] ?? ''}}
            </td>
            <td>
                {{ $item->tags[1] ?? ''}}
            </td>
            <td>
                {{ $item->tags[2] ?? ''}}
            </td>
            <td>
                {{ $item->tags[3] ?? ''}}
            </td>
            <td>
                {{ $item->tags[4] ?? ''}}
            </td>
            <td>
                {{ $item->countryTag ?? ''}}
            </td>
            <td>
                {{ $item->varietalTag ?? ''}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>