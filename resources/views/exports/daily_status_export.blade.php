<table>
    <thead>
        <tr>
            <th>Daily Status For {{ $date }}</th>
        </tr>
    </thead>

</table>

<table>
    <thead>
        <tr>
            <th><b>Hours</b></th>
        </tr>
        <tr> </tr>
        <tr>
            <th>Runner</th>
            <th>Start</th>
            <th>Finish</th>
        </tr>
    </thead>
    <tbody>
        @foreach($scheduledRunners as $runner)
        <tr>
            <td>
                {{ $runner->name }}
            </td>
            <td>
                {{ $runner->start }}
            </td>
            <td>
                {{ $runner->end }}
            </td>

        </tr>
        @endforeach
    </tbody>

</table>


<table>
    <thead>

        <tr>
            <th><b>Finances</b></th>
        </tr>
        <tr></tr>
    </thead>
    <tbody>

        <tr>
            <td>
                Cost of Goods
            </td>
            <td>
                {{ $finances['costOfGoodsSold'] }}
            </td>
        </tr>
        <tr>
            <td>
                Markup
            </td>
            <td>
                {{ $finances['markup'] }}
            </td>
        </tr>
        <tr>
            <td>
                Service Fees
            </td>
            <td>
                {{ $finances['serviceFees'] }}
            </td>
        </tr>

        <tr>
            <td>
                Delivery Fees
            </td>
            <td>
                {{ $finances['deliveryFees'] }}
            </td>
        </tr>

        <tr>
            <td>
                Tips
            </td>
            <td>
                {{ $finances['tips'] }}
            </td>
        </tr>

        <tr>
            <td>
                <b>Revenue</b>
            </td>
            <td align="right">
                <b>{{ $finances['revenue'] }}</b>
            </td>
        </tr>
        <tr></tr>
        <tr>
            <td>
                Cost of Goods
            </td>
            <td>
                {{ $finances['costOfGoodsSold'] }}
            </td>
        </tr>
        <tr>
            <td>
                Runner Cost
            </td>
            <td>
                {{ $finances['runnerCosts'] }}
            </td>

        </tr>
        <tr>
            <td>
                Dispatch Cost
            </td>
            <td>
                {{ $finances['dispatchCosts'] }}
            </td>

        </tr>
        <tr>
            <td>
                Discounts
            </td>
            <td>
                {{ $finances['discounts'] }}
            </td>
        </tr>
        <tr>
            <td>
                Stripe Fees
            </td>
            <td>
                {{ $finances['stripeFees'] }}
            </td>
        </tr>
        <tr>
            <td>
                Tips
            </td>
            <td>
                {{ $finances['tips'] }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Expenses</b>
            </td>
            <td align="right">
                <b>{{ $finances['costOfGoodsSold'] + $finances['runnerCosts'] + $finances['dispatchCosts'] + $finances['discounts'] + $finances['stripeFees'] + $finances['tips'] }}</b>
            </td>
        </tr>
        <tr></tr>
        <tr>
            <td>
                Revenue
            </td>
            <td>
                {{ $finances['revenue'] }}
            </td>
        </tr>
        <tr>
            <td>
                Expenses
            </td>
            <td>
                {{ $finances['costOfGoodsSold'] + $finances['runnerCosts'] + $finances['dispatchCosts'] + $finances['discounts'] + $finances['stripeFees'] + $finances['tips'] }}
            </td>
        </tr>
        <tr>
            <td>
                Net Revenue
            </td>
            <td align="right">
                <b>{{ $finances['revenue'] - ($finances['costOfGoodsSold'] + $finances['runnerCosts'] +  $finances['dispatchCosts'] + $finances['discounts'] + $finances['stripeFees'] + $finances['tips']) }}</b>
            </td>
        </tr>
    </tbody>
</table>