<table>
    <thead>
        <tr>
            <th>Product Id</th>
            <th>Created At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($productUpdate as $update)
        <tr>
            <td>
                {{ $update->id }}
            </td>
            <td>
                {{ $update->created_at}}
            </td>

        </tr>
        @endforeach
    </tbody>
</table>