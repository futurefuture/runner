<table>

    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>No of Orders</th>
            <th>COGS</th>
        </tr>
        <tr></tr>
    </thead>
    <tbody>

        @foreach($runnerReport as $sales)

        <tr>
            <td>
                {{ $sales->runner_1}}
            </td>
            <td>
                {{ $sales->first_name }}
            </td>
            <td>
                {{ $sales->totalOrders }}
            </td>
            <td>
                {{ $sales->cogs}}
            </td>

        </tr>

        @endforeach
    </tbody>
</table>