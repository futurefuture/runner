<table>

    <thead>
        <tr>
            <th>User</th>
            <th>Day</th>
            <th>Total </th>
        </tr>
        <tr></tr>
    </thead>
    <tbody>

        @foreach($torontoTerritoryCustomer as $sales)

        <tr>
            <td>
                {{ $sales['user'] }}
            </td>
            <td>
                {{ $sales['day'] }}
            </td>
            <td>
                {{ $sales['total']}}
            </td>

        </tr>

        @endforeach
    </tbody>
</table>