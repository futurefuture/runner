<table>

    <thead>
        <tr>
            <th>Day</th>
            <th>Total Order</th>
            <th>Retail Price</th>
            <th>Markup</th>
            <th>Delivery</th>
            <th>tip</th>
            <th>Discount</th>
        </tr>

    </thead>
    <tbody>

        @foreach($torontoRushSales as $rushsales)

        <tr>

            <td>
                {{ $rushsales['day'] }}
            </td>
            <td>
                {{ $rushsales['totalOrder']}}
            </td>
            <td>
                {{ $rushsales['retailPrice']}}
            </td>

            <td>
                {{ $rushsales['markup']}}
            </td>
            <td>
                {{ $rushsales['delivery']}}
            </td>
            <td>
                {{ $rushsales['tip']}}
            </td>
            <td>
                {{ $rushsales['discount']}}
            </td>
        </tr>

        @endforeach
    </tbody>
</table>