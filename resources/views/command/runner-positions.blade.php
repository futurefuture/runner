@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="jobs"></command-nav>
				<command-jobs></command-jobs>
			</div>
		</section>
	</div>
@endsection