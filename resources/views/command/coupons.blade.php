@extends ('command.app')

@section('title', 'Runner Command Cart Starters')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="coupons"></command-nav>
				<command-coupons></command-coupons>
			</div>
		</section>
	</div>
@endsection