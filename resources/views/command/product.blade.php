@extends ('command.app')

@section('title', 'Runner Command Product')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav></command-nav>
				<command-product product-id={{ $productId }}></command-product>
			</div>
		</section>
	</div>
@endsection