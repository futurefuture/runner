@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="numbers"></command-nav>
				<command-numbers></command-numbers>
			</div>
		</section>
	</div>
@endsection