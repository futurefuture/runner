@extends ('command.app')

@section('title', 'Runner Command Custom Product')

@section ('content')
    <command-header></command-header>
    <div class="container is-fluid">
        <section class="section">
            <div class="columns">
                <command-nav active="customproducts"></command-nav>
                <command-custom-product id={{ $id }}></command-custom-product>
            </div>
        </section>
    </div>
@endsection