@extends ('command.app')

@section('title', 'Runner Command Notifications')

@section ('content')
    <command-header></command-header>

    <div class="container is-fluid">
        <section class="section">
            <div class="columns">
                <command-nav active="fcm"></command-nav>
                <command-fcm></command-fcm>
            </div>
        </section>
    </div>
@endsection