@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="client"></command-nav>
				<command-client id="{{ $id }}"></command-client>
			</div>
		</section>
	</div>
@endsection