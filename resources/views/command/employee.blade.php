@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="employees"></command-nav>
				<command-employee id="{{ $id }}"></command-employee>
			</div>
		</section>
	</div>
@endsection