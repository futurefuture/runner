@extends ('command.app')

@section('title', 'Runner Command Notifications')

@section ('content')
	<command-header></command-header>

	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
                <command-nav active="links"></command-nav>
                <div class="column">
                    <div class="columns">
                        <div class="column">
                            <a href="/api/v1/command/user_with_fb_id_has_order" target="_blank">user_with_fb_id_has_order</a>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <a href="/api/v1/command/user_has_ordered_wine" target="_blank">user_has_ordered_wine</a>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <a href="/api/v1/command/users_who_make_2_or_more_purchases_per_month" target="_blank">users_who_make_2_or_more_purchases_per_month</a>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <a href="/api/v1/command/user_who_have_carts_over_65" target="_blank">user_who_have_carts_over_65</a>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <a href="/api/v1/command/average_cart_size_of_beer_only_orders" target="_blank">average_cart_size_of_beer_only_orders</a>
                        </div>
                    </div>
                </div>       
			</div>
		</section>
	</div>
@endsection