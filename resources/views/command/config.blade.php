@extends ('command.app')

@section('title', 'Runner Command Config')

@section ('content')
<command-header></command-header>

<div class="container is-fluid">
	<section class="section">
		<div class="columns">
			<command-nav active="config"></command-nav>
			<command-config></command-config>
		</div>
	</section>
</div>
@endsection