@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
    <command-header></command-header>

    <div class="container is-fluid">
        <section class="section">
            <div class="columns">
                <command-nav active="invited_users"></command-nav>
                <command-invited-users></command-invited-users>
            </div>
        </section>
    </div>
@endsection