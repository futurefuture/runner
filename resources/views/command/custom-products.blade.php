@extends ('command.app')

@section('title', 'Runner Command Cart Starters')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="customproducts"></command-nav>
				<command-custom-products></command-custom-products>
			</div>
		</section>
	</div>
@endsection