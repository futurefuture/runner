@extends ('command.app')

@section('title', 'Runner Command Products')

@section ('content')
    <command-header></command-header>

    <div class="container is-fluid">
        <section class="section">
            <div class="columns">
                <command-nav active="reviews"></command-nav>
                <command-reviews></command-reviews>
            </div>
        </section>
    </div>
@endsection