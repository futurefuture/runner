@extends ('command.app')

@section('title', 'Runner Command Products')

@section ('content')
<command-header></command-header>

<div class="container is-fluid">
	<section class="section">
		<div class="columns">
			<command-nav active="tags"></command-nav>
			<command-tags></command-tags>
		</div>
	</section>
</div>
@endsection