@extends ('command.app')

@section('title', 'Runner Command Cart Starters')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="cartstarters"></command-nav>
				<command-cart-starters></command-cart-starters>
			</div>
		</section>
	</div>
@endsection