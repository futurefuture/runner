@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
    <command-header></command-header>

    <div class="container is-fluid">
        <section class="section">
            <div class="columns">
                <command-nav active="clients"></command-nav>
                <command-clients></command-clients>
            </div>
        </section>
    </div>
@endsection