@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
<command-header></command-header>
<div class="container is-fluid">
	<section class="section">
		<div class="columns">
			<command-nav></command-nav>
			<command-store store-id="{{ $storeId }}"></command-store>
		</div>
	</section>
</div>
@endsection