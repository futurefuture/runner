@extends ('command.app')

@section('title', 'Runner Command Products')

@section ('content')
<command-header></command-header>

<div class="container is-fluid">
	<section class="section">
		<div class="columns">
			<command-nav></command-nav>
			<command-products></command-products>
		</div>
	</section>
</div>
@endsection