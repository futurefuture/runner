@extends ('command.app')

@section('title', 'Runner Command Bitch')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="schedule"></command-nav>
				<command-schedule></command-schedule>
			</div>
		</section>
	</div>
@endsection