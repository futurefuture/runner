@extends ('command.app')

@section('title', 'Runner Command Cart Starter')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="bundles"></command-nav>
				<command-bundle id={{ $id }}></command-bundle>
			</div>
		</section>
	</div>
@endsection