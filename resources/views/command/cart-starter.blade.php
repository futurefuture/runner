@extends ('command.app')

@section('title', 'Runner Command Cart Starter')

@section ('content')
	<command-header></command-header>
	
	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="cartstarters"></command-nav>
				<command-cart-starter id={{ $id }}></command-cart-starter>
			</div>
		</section>
	</div>
@endsection