@extends ('command.app')

@section('title', 'Runner Command Notifications')

@section ('content')
	<command-header></command-header>

	<div class="container is-fluid">
		<section class="section">
			<div class="columns">
				<command-nav active="notifications"></command-nav>
				<command-notifications></command-notifications>
			</div>
		</section>
	</div>
@endsection