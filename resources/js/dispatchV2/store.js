import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import api from './mixins/api.js';

const VueCookie = require('vue-cookie');

Vue.use(VueCookie);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    domain: window.domain,
    accessToken: Vue.cookie.get('token', { domain }) || '',
    updated: false,
    zoomLevel: 13,
    storeArr: [],
    token: null,
    user: null,
    isUserLoggedIn: false,
    activePage: '',
    searchResult: {},
    isLoading: false,
    orders: [],
    scheduled_orders: [],
    tasks: [],
  },
  getters: {
    getActivePage(state) {
      return state.activePage;
    },
    getUserById(state) {
      if (state.isUserLoggedIn) {
        return state.user.id;
      }
      return 'N/A';
    },
    getUser(state) {
      if (state.isUserLoggedIn) {
        return state.user;
      }
      return 'N/A';
    },
    getUpdated(state) {
      return state.updated;
    },
    getZoomLevel(state) {
      return state.zoomLevel;
    },
    storeArr(state) {
      return state.storeArr;
    },
    getSearchResult(state) {
      return state.searchResult;
    },
    getLoading(state) {
      return state.isLoading;
    },
    getTasks(state) {
      return state.tasks;
    },
    getOrders(state) {
      return state.orders;
    },
    getScheduledOrders(state) {
      return state.scheduled_orders;
    },
  },
  mutations: {
    SET_ACTIVE_PAGE: (state, activePage) => {
      Vue.set(state, 'activePage', activePage);
    },
    SET_TOKEN(state, token) {
      state.token = token;
      state.isUserLoggedIn = !!(token);
    },
    SET_USER(state, user) {
      state.user = user;
    },
    SET_STORE_ARR: (state, storeArr) => {
      Vue.set(state, 'storeArr', storeArr);
    },
    SET_ORDER(state, value) {
      state.order = value;
    },
    // CHRISTIAN - what are we using this SET_UPDATED for? (answer: any reactivity for updating something. ex: assign)
    SET_UPDATED(state, bool) {
      state.updated = bool;
    },
    SET_ZOOM_LEVEL(state, value) {
      state.zoomLevel = value;
    },
    SET_SEARCH_RESULT(state, value) {
      state.searchResult = value;
    },
    SET_LOADING(state, value) {
      state.isLoading = value;
    },
    SET_ORDERS(state, value) {
      state.orders = value;
    },
    SET_SCHEDULED_ORDERS(state, value) {
      state.scheduled_orders = value;
    },
    SET_TASKS(state, value) {
      state.tasks = value;
    },
  },
  actions: {
    setToken({ commit }, token) {
      // do in here any async logic
      commit('setToken', token);
    },
    setUser({ commit }, user) {
      // do in here any async logic
      commit('setUser', user);
    },
    loadActivePage: (context, activePage) => {
      context.commit('SET_ACTIVE_PAGE', activePage);
    },
    loadStoreArr: (context, storeArr) => {
      context.commit('SET_STORE_ARR', storeArr);
    },
    SET_UPDATED({ commit }, bool) {
      commit('SET_UPDATED', bool);
    },
    SET_ZOOM_LEVEL({ commit }, value) {
      commit('SET_ZOOM_LEVEL', value);
    },
    SET_SEARCH_RESULT({ commit }, value) {
      commit('SET_SEARCH_RESULT', value);
    },
    SET_LOADING({ commit }, value) {
      commit('SET_LOADING', value);
    },
    async getOrders(context) {
      try {
        const response = await axios({
          method: 'get',
          url: `https://api.${context.state.domain}/dispatch/v3/orders?status=0`,
          headers: {
            Authorization: `Bearer ${Vue.cookie.get('token', {
              domain: window.domain,
            })}`,
            Accept: 'application/json',
            ContentType: 'application/json',
          },
        });

        const orders = response.data.data;
        context.commit('SET_ORDERS', orders);
      } catch (error) {
        console.log(error);
      }
    },
    async getScheduledOrders(context) {
      try {
        const response = await axios({
          method: 'get',
          url: `https://api.${context.state.domain}/dispatch/v3/orders?status=7`,
          headers: {
            Authorization: `Bearer ${Vue.cookie.get('token', {
              domain: window.domain,
            })}`,
            Accept: 'application/json',
            ContentType: 'application/json',
          },
        });

        const orders = response.data.data;
        context.commit('SET_SCHEDULED_ORDERS', orders);
      } catch (error) {
        console.log(error);
      }
    },
    getTasks(context) {
      axios({
        method: 'get',
        url: `https://api.${context.state.domain}/dispatch/v3/tasks?state=1,2`,
        headers: {
          Authorization: `Bearer ${Vue.cookie.get('token', {
            domain: window.domain,
          })}`,
          Accept: 'application/json',
          ContentType: 'application/json',
        },
      })
        .then((response) => {
          const sortedTasks = _.sortBy(
            response.data.data,
            'index',
          );

          context.commit('SET_TASKS', sortedTasks);
        })
        .catch((error) => {
          console.log(error);
        });
    },
  },
});

export default store;
