
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from 'vue';
import VueTheMask from 'vue-the-mask';
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';
import FullCalendar from 'vue-full-calendar';
import Notifications from 'vue-notification';
import store from './store';

require('./bootstrap');

window.moment = require('moment');
const VueCookie = require('vue-cookie');

if (window.location.host.indexOf('getrunner.io') > -1) {
  Sentry.init({
    dsn: 'https://93174daa797a423c8e95efd0e13dad8d@sentry.io/1506163',
    integrations: [
      new Integrations.Vue({
        Vue, attachProps: true,
      }),
    ],
  });
}
Vue.use(VueTheMask);
Vue.use(VueCookie);
Vue.use(Notifications);
Vue.use(FullCalendar);
Vue.use(require('vue-moment'));

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDrPkhS9nd8nO-V0rXBuDBi7pPQCepKfes',
    v: '3',
    libraries: 'places,geometry',
  },
});

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
  'X-CSRF-TOKEN': window.Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest',
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// utils
Vue.component('moon-loader', require('vue-spinner/src/MoonLoader.vue'));

// pages
Vue.component('PastOrders', require('./pages/PastOrders.vue'));
Vue.component('OrderPage', require('./pages/OrderPage.vue'));
Vue.component('Manage', require('./pages/Manage.vue'));
Vue.component('Schedule', require('./pages/Schedule.vue'));
Vue.component('Login', require('./pages/Login.vue'));
Vue.component('Customers', require('./pages/Customers.vue'));
Vue.component('Customer', require('./pages/Customer.vue'));

// components
Vue.component('FixedHeader', require('./components/FixedHeader.vue'));
Vue.component('DispatchMap', require('./components/DispatchMap.vue'));
Vue.component('Order', require('./components/Order.vue'));
Vue.component('Orders', require('./components/Orders.vue'));
Vue.component('OrderDetails', require('./components/OrderDetails.vue'));
Vue.component('OrderInformation', require('./components/OrderInformation.vue'));
Vue.component('OrderStripeCharge', require('./components/OrderStripeCharge.vue'));
Vue.component('OrderInventories', require('./components/OrderInventories.vue'));
Vue.component('Loader', require('./components/Loader.vue'));
Vue.component('Runners', require('./components/Runners.vue'));
Vue.component('MapControls', require('./components/MapControls.vue'));
Vue.component('PastOrdersTable', require('./components/PastOrdersTable.vue'));
Vue.component('Calendar', require('./components/Calendar.vue'));
Vue.component('Pagination', require('./components/Pagination.vue'));
Vue.component('Assignees', require('./components/Assignees.vue'));
Vue.component('Tooltip', require('./components/Tooltip.vue'));
Vue.component('CustomerTable', require('./components/CustomerTable.vue'));
Vue.component('CustomerInformation', require('./components/CustomerInformation.vue'));
Vue.component('Search', require('./components/Search.vue'));
Vue.component('TaskBox', require('./components/TaskBox.vue'));


// order details


/**
 * Debug
 */

if (window.location.host.indexOf('getrunner.io') > -1) {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

/**
 *
 * App Initialization
 */
const app = new Vue({
  store,
}).$mount('#app');
