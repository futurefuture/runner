export function identify(data) {
  const {
    userId, address, age, avatar, birthday, createdAt, email, firstName, gender, lastName, phone, inventoryId, storeId,
  } = data;

  if (userId) {
    window.analytics.identify(userId, {
      address,
      age,
      avatar,
      birthday,
      createdAt,
      email,
      firstName,
      gender,
      id: String(userId),
      lastName,
      name: `${firstName} ${lastName}`,
      phone,
      username: email,
      storeId: String(storeId),
      inventoryId: String(inventoryId),
    });
  } else {
    window.analytics.identify({
      storeId: String(storeId),
      inventoryId: String(inventoryId),
      address,
    });
  }
}
export function productsSearched(data) {
  const { query, storeId, inventoryId } = data;

  window.analytics.track('Products Searched', {
    query,
    storeId: String(storeId),
    inventoryId: String(inventoryId),
  });
}
export function productViewed(data) {
  const {
    product, subDomain, domain, storeId, inventoryId,
  } = data;
  let partnerIds = null;

  if (product.partners) {
    partnerIds = product.partners.map(partner => String(partner.id));
  }

  window.analytics.track('Product Viewed', {
    productId: String(product.id),
    sku: product.sku,
    name: product.title,
    brand: product.producer,
    variant: product.packaging,
    price: Number((product.runner_price / 100).toFixed(2)),
    quantity: Number(1),
    currency: 'cad',
    value: Number((product.runner_price / 100).toFixed(2)),
    url: `https://${subDomain}.${domain}/products/${product.slug}`,
    imageUrl: product.image,
    storeId: String(storeId),
    inventoryId: String(inventoryId),
    partnerIds,
  });
}
export function productListView(data) {
  const {
    category, products, subDomain, domain, storeId, inventoryId,
  } = data;
  const analyticProducts = products.map((product, index) => {
    const analyticProduct = {};
    let partnerIds = [];
    let partnerArr = {};
    partnerArr = product.attributes.partners.map((partner) => {
      partnerIds.push(partner.id);
    });

    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }

    analyticProduct.productId = String(product.id);
    analyticProduct.sku = product.attributes.sku;
    analyticProduct.name = product.attributes.title;
    analyticProduct.brand = product.attributes.producer;
    analyticProduct.variant = product.attributes.packaging;
    analyticProduct.price = Number((product.attributes.runnerPrice / 100).toFixed(2));
    analyticProduct.quantity = Number(1);
    analyticProduct.position = Number(index + 1);
    analyticProduct.url = `https://${subDomain}.${domain}/products/${product.attributes.slug}`;
    analyticProduct.imageUrl = product.attributes.image;
    analyticProduct.partnerIds = partnerArr;

    return analyticProduct;
  });

  window.analytics.track('Product List Viewed', {
    categoryId: String(category.id),
    listId: category.slug,
    category: category.title,
    products: analyticProducts,
    storeId: String(storeId),
    inventoryId: String(inventoryId),
  });
}
export function productListFiltered(data) {
  const {
    category, products, subDomain, domain, storeId, inventoryId, filters,
  } = data;
  const analyticProducts = products.map((product, index) => {
    const analyticProduct = {};
    let partnerIds = null;

    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }

    analyticProduct.productId = String(product.id);
    analyticProduct.sku = product.attributes.sku;
    analyticProduct.category = category.title;
    analyticProduct.name = product.attributes.title;
    analyticProduct.brand = product.attributes.producer;
    analyticProduct.variant = product.attributes.packaging;
    analyticProduct.price = Number((product.attributes.runnerPrice / 100).toFixed(2));
    analyticProduct.quantity = Number(1);
    analyticProduct.position = Number(index + 1);
    analyticProduct.url = `https://${subDomain}.${domain}/products/${product.attributes.slug}`;
    analyticProduct.imageUrl = product.attributes.image;
    analyticProduct.partnerIds = partnerIds;

    return analyticProduct;
  });

  const filtersArr = [];
  let filtersObj = {};

  for (let i = 0; i < Object.keys(filters).length; i++) {
    if ((Object.keys(filters)[i]) !== 'sort' && (Object.keys(filters)[i]) !== 'page') {
      filtersObj.type = (Object.keys(filters)[i]).split('[')[1].replace(']', '');
    } else {
      filtersObj.type = (Object.keys(filters)[i]);
    }
    filtersObj.value = Object.values(filters)[i];
    filtersArr.push(filtersObj);
    filtersObj = {};
  }

  let sortObj = {};

  for (let j = 0; j < filtersArr.length; j++) {
    if (filtersArr[j].type === 'sort') {
      sortObj = filtersArr[j];
      filtersArr.splice(filtersArr.indexOf(filtersArr[j]), 1);
    }
  }

  if (Object.keys(filters).length > 0) {
    window.analytics.track('Product List Filtered', {
      categoryId: String(category.id),
      listId: category.slug,
      filters: filtersArr,
      sorts: sortObj,
      products: analyticProducts,
      storeId: String(storeId),
      inventoryId: String(inventoryId),
    });
  }
}
export function productClicked(data) {
  const {
    product, index, subDomain, domain, storeId, inventoryId,
  } = data;
  let partnerIds = null;

  if (product.attributes.partners) {
    partnerIds = product.attributes.partners.map(partner => String(partner.id));
  }

  window.analytics.track('Product Clicked', {
    productId: String(product.id),
    sku: product.attributes.sku,
    name: product.attributes.title,
    brand: product.attributes.producer,
    variant: product.attributes.packaging,
    price: Number((product.attributes.runnerPrice / 100).toFixed(2)),
    quantity: Number(1),
    currency: 'cad',
    position: index,
    value: Number((product.attributes.runnerPrice / 100).toFixed(2)),
    url: `https://${subDomain}.${domain}/products/${product.attributes.slug}`,
    imageUrl: product.attributes.image,
    storeId: String(storeId),
    inventoryId: String(inventoryId),
    partnerIds,
  });
}
export function cartViewed(data) {
  const { cart, storeId, inventoryId } = data;
  if (cart) {
    const analyticProducts = cart.attributes.products.map((product, index) => {
      const analyticProduct = {};

      analyticProduct.productId = String(product.id);
      analyticProduct.name = product.title;
      analyticProduct.variant = product.packaging;
      analyticProduct.price = Number((product.runnerPrice / 100).toFixed(2));
      analyticProduct.quantity = Number(1);
      analyticProduct.position = Number(index + 1);
      analyticProduct.imageUrl = product.image;
      analyticProduct.partnerIds = product.partnerIds;

      return analyticProduct;
    });

    window.analytics.track('Cart Viewed', {
      cartId: String(cart.id),
      products: analyticProducts,
      storeId: String(storeId),
      inventoryId: String(inventoryId),
    });
  }
}
export function checkoutStarted(data) {
  const {
    cart, products, storeId, subDomain, inventoryId,
  } = data;

  if (cart) {
    const analyticProducts = products.map((product, index) => {
      const analyticProduct = {};

      analyticProduct.productId = String(product.id);
      analyticProduct.allowSub = product.allowSub;
      analyticProduct.title = product.title;
      analyticProduct.productType = product.type;
      analyticProduct.packaging = product.packaging;
      analyticProduct.quantity = product.quantity;
      analyticProduct.retailPrice = Number((product.retailPrice / 100).toFixed(2));
      analyticProduct.runnerPrice = Number((product.runnerPrice / 100).toFixed(2));
      analyticProduct.position = Number(index + 1);
      analyticProduct.imageUrl = product.image;
      analyticProduct.partnerIds = product.partnerIds;

      return analyticProduct;
    });

    window.analytics.track('Checkout Started', {
      orderId: String(cart.id),
      storeId: String(storeId),
      affiliation: subDomain,
      total: Number((cart.attributes.total / 100).toFixed(2)),
      subTotal: Number((cart.attributes.subTotal / 100).toFixed(2)),
      tax: Number((cart.attributes.tax / 100).toFixed(2)),
      deliveryFee: Number((cart.attributes.deliveryFee / 100).toFixed(2)),
      discount: Number((cart.attributes.discount / 100).toFixed(2)),
      products: analyticProducts,
      inventoryId: String(inventoryId),
    });
  }
}
export function storeClicked(data) {
  const { store, userId, storeId } = data;
  if (typeof store !== 'undefined') {
    window.analytics.track('Store Clicked', {
      title: store.attributes.title,
      inventoryId: String(store.attributes.inventoryId),
      currentStoreId: storeId ? String(storeId) : '',
      storeId: String(store.id),
      userId: userId ? String(userId) : '',
    });
  }
}
export function promotionViewed(data) {
  const {
    products, promotionId, creative, title, index, categoryId, promotionSubDomain, subDomain, domain, promotionStoreId, storeId, promotionInventoryId, inventoryId,
  } = data;
  const analyticProducts = products.map((product, index) => {
    const analyticProduct = {};
    let partnerIds = null;

    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }

    analyticProduct.productId = String(product.id);
    analyticProduct.sku = product.attributes.sku;
    analyticProduct.categoryId = categoryId;
    analyticProduct.name = product.attributes.title;
    analyticProduct.brand = product.attributes.producer;
    analyticProduct.variant = product.attributes.packaging;
    analyticProduct.price = Number((product.attributes.runnerPrice / 100).toFixed(2));
    analyticProduct.quantity = Number(1);
    analyticProduct.position = Number(index + 1);
    analyticProduct.url = `https://${subDomain}.${domain}/products/${product.attributes.slug}`;
    analyticProduct.imageUrl = product.attributes.image;
    analyticProduct.partnerIds = partnerIds;

    return analyticProduct;
  });

  window.analytics.track('Promotion Viewed', {
    promotionId,
    creative,
    name: title,
    position: index,
    products: analyticProducts,
    storeId: String(storeId),
    promotionStoreId: String(promotionStoreId),
    inventoryId: String(inventoryId),
    promotionInventoryId: String(promotionInventoryId),
    promotionSubDomain: String(promotionSubDomain),
    subDomain: String(subDomain),
    domain: String(domain),
    categoryId: String(categoryId),
  });
}
export function relatedProductsViewed(data) {
  const {
    productId, products, subDomain, domain, storeId, inventoryId,
  } = data;
  const analyticProducts = products.map((product, index) => {
    const analyticProduct = {};
    let partnerIds = null;

    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }

    analyticProduct.productId = String(product.id);
    analyticProduct.sku = product.attributes.sku;
    analyticProduct.name = product.attributes.title;
    analyticProduct.categoryId = product.attributes.categoryId;
    analyticProduct.brand = product.attributes.producer;
    analyticProduct.variant = product.attributes.packaging;
    analyticProduct.price = Number((product.attributes.runnerPrice / 100).toFixed(2));
    analyticProduct.quantity = Number(1);
    analyticProduct.position = Number(index + 1);
    analyticProduct.url = `https://${subDomain}.${domain}/products/${product.attributes.slug}`;
    analyticProduct.imageUrl = product.attributes.imageThumbnail;
    analyticProduct.partnerIds = partnerIds;

    return analyticProduct;
  });

  window.analytics.track('Related Products Viewed', {
    domain,
    subDomain,
    productId,
    products: analyticProducts,
    storeId: String(storeId),
    inventoryId: String(inventoryId),
  });
}

export function adClicked(data) {
  const {
    ads, userId, storeId, inventoryId, product,
  } = data;

  let partnerIds = null;

  if (product) {
    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }
  }

  if (ads) {
    if (product) {
      ads.map((ad) => {
        window.analytics.track('Ad Clicked', {
          adId: ad.id,
          adTypeId: ad.ad_type_id,
          campaignId: ad.campaign_id,
          content: ad.content,
          products: ad.products,
          categoryId: ad.category_id,
          tagId: ad.tag_id,
          carouselId: ad.carousel_id,
          carouselComponentId: ad.carousel_component_id,
          bidType: ad.bid_type,
          bidAmount: ad.bid_amount,
          state: ad.state,
          index: ad.index,
          startDate: ad.start_date,
          endDate: ad.end_date,
          createdAt: ad.created_at,
          updatedAt: ad.updated_at,
          userId,
          productId: product.id,
          partnerIds,
          storeId,
          inventoryId,
        });
        return null;
      });
    } else {
      ads.map((ad) => {
        window.analytics.track('Ad Clicked', {
          adId: ad.id,
          adTypeId: ad.ad_type_id,
          campaignId: ad.campaign_id,
          content: ad.content,
          products: ad.products,
          categoryId: ad.category_id,
          tagId: ad.tag_id,
          carouselId: ad.carousel_id,
          carouselComponentId: ad.carousel_component_id,
          bidType: ad.bid_type,
          bidAmount: ad.bid_amount,
          state: ad.state,
          index: ad.index,
          startDate: ad.start_date,
          endDate: ad.end_date,
          createdAt: ad.created_at,
          updatedAt: ad.updated_at,
          userId,
          partnerIds,
          storeId,
          inventoryId,
        });
        return null;
      });
    }
  }
}


export function adViewed(data) {
  const {
    ads, userId, storeId, inventoryId, product,
  } = data;

  let partnerIds = null;
  if (product) {
    if (product.attributes.partners) {
      partnerIds = product.attributes.partners.map(partner => String(partner.id));
    }
  }

  if (ads) {
    if (product) {
      ads.map((ad) => {
        window.analytics.track('Ad Impression', {
          adId: ad.id,
          adTypeId: ad.ad_type_id,
          campaignId: ad.campaign_id,
          content: ad.content,
          products: ad.products,
          categoryId: ad.category_id,
          tagId: ad.tag_id,
          carouselId: ad.carousel_id,
          carouselComponentId: ad.carousel_component_id,
          bidType: ad.bid_type,
          bidAmount: ad.bid_amount,
          state: ad.state,
          index: ad.index,
          startDate: ad.start_date,
          endDate: ad.end_date,
          createdAt: ad.created_at,
          updatedAt: ad.updated_at,
          userId,
          productId: product.id,
          partnerIds,
          storeId,
          inventoryId,
        });
        return null;
      });
    } else {
      ads.map((ad) => {
        window.analytics.track('Ad Impression', {
          adId: ad.id,
          adTypeId: ad.ad_type_id,
          campaignId: ad.campaign_id,
          content: ad.content,
          products: ad.products,
          categoryId: ad.category_id,
          tagId: ad.tag_id,
          carouselId: ad.carousel_id,
          carouselComponentId: ad.carousel_component_id,
          bidType: ad.bid_type,
          bidAmount: ad.bid_amount,
          state: ad.state,
          index: ad.index,
          startDate: ad.start_date,
          endDate: ad.end_date,
          createdAt: ad.created_at,
          updatedAt: ad.updated_at,
          userId,
          partnerIds,
          storeId,
          inventoryId,
        });
        return null;
      });
    }
  }
}
