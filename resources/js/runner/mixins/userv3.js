export default {
  created() {
    this.$store.dispatch('getUser');
  },
  computed: {
    user() {
      return this.$store.getters.user;
    },
    isAuthenticated() {
      return this.$store.getters.isAuthenticated;
    },
  },
};
