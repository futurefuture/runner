import Vue from 'vue';

export default {
  created() {
    this.$store.dispatch('getCart', Vue.cookie.get('userId', { domain: window.domain }));
  },
  computed: {
    cart() {
      return this.$store.getters.cart;
    },
  },
};
