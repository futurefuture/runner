export default {
  computed: {
    domain() {
      return this.$store.getters.domain;
    },
    subDomain() {
      return this.$store.getters.subDomain;
    },
  },
};
