import Vue from 'vue';
import axios from 'axios';

export default () => axios.create({
  baseURL: `https://api.${window.domain}/runner/v4`,
  headers: {
    Authorization: `Bearer ${Vue.cookie.get('token', { domain: window.domain })}`,
    Accept: 'application/json',
    ContentType: 'application/json',
  },
});
