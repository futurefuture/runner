export default {
  created() {
    if (this.token) {
      this.$cookie.set('token', this.token);
    }

    if (this.$cookie.get('token')) {
      this.getUser();
    }
  },
  computed: {
    user() {
    },
  },
  props: [
    'token',
  ],
  methods: {
    getUser() {
    },
  },
};
