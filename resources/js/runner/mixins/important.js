import { mapGetters, mapState, mapMutations } from 'vuex';

export default {
  props: {
    pageAttributes: {
      type: String,
      required: true,
      default: null,
    },
  },
  computed: {
    domain() {
      return JSON.parse(this.pageAttributes).domain;
    },
    subDomain() {
      return JSON.parse(this.pageAttributes).subDomain;
    },
    storeId() {
      return JSON.parse(this.pageAttributes).storeId;
    },
    secret() {
      return JSON.parse(this.pageAttributes).secret || null;
    },
    accessToken() {
      return JSON.parse(this.pageAttributes).accessToken || null;
    },
    userId() {
      return JSON.parse(this.pageAttributes).userId || null;
    },
    orderId() {
      return JSON.parse(this.pageAttributes).orderId || null;
    },
    ...mapGetters([
      'isAuthenticated',
      'selectedAddress',
      'postalCode',
      'user',
    ]),
    ...mapState([
      'cart',
      'inventoryId',
      'stores',
      'store',
      'activeModal',
      'locationValidated',
    ]),
  },
  data() {
    return {
      addresses: [],
      isLoading: false,
    };
  },
  async mounted() {
    await this.validate();
  },
  methods: {
    ...mapMutations([
      'SET_ACTIVE_MODAL',
    ]),
    setActiveModal(activeModal) {
      this.SET_ACTIVE_MODAL(activeModal);
    },
    async validatePostalCode(domain, postalCode) {
      await this.$store.dispatch('validatePostalCode', {
        domain,
        postalCode,
      });
    },
    async getUser() {
      await this.$store.dispatch('getUser');
    },
    async getUserAddresses() {
      await axios
        .get(`https://api.${this.domain}/runner/v4/users/${this.user.id}/addresses`)
        .then((response) => {
          this.addresses = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });
    },
    async getCart() {
      this.$store.dispatch('getCart');
    },
    validate() {
      if (this.isAuthenticated) {
        // this.isLoading = true;
        this.getUser()
          .then((response) => {
            if (!this.selectedAddress) {
              // this.setActiveModal('addressInput');
            } else {
              this.validatePostalCode(this.domain, this.selectedAddress.attributes.postalCode)
                .then((response) => {
                  const store = this.stores.find(s => s.id == this.storeId);

                  if (store) {
                    this.$store.dispatch('setStore', {
                      storeId: this.storeId,
                      postalCode: this.selectedAddress.attributes.postalCode,
                    });
                    this.$store.commit('SET_INVENTORY_ID', store.attributes.inventoryId);
                    this.isLoading = false;
                  } else {
                    this.$store.dispatch('setStore', {
                      storeId: this.storeId,
                      postalCode: this.selectedAddress.attributes.postalCode,
                    });
                  }
                });
            }

            this.getCart();

            if (this.user) {
              window.analytics.identify(this.user.id, {
                avatar: this.user.attributes.avatarImage || null,
                birthday: this.user.attributes.dateOfBirth || null,
                email: this.user.attributes.email,
                firstName: this.user.attributes.firstName,
                lastName: this.user.attributes.lastName,
                phone: this.user.attributes.phoneNumber,
              });
            }
          });
      } else if (this.selectedAddress) {
        this.validatePostalCode(this.domain, this.selectedAddress.attributes.postalCode)
          .then((response) => {
            const store = this.stores.find(s => s.id == this.storeId);
            if (store) {
              this.$store.dispatch('setStore', {
                storeId: this.storeId,
                postalCode: this.selectedAddress.attributes.postalCode,
              });
              this.$store.commit('SET_INVENTORY_ID', store.attributes.inventoryId);
              this.$store.commit('SET_TEST', true);
            }
          });
      } else {
        this.$store.dispatch('setStore', {
          storeId: this.storeId,
          postalCode: this.selectedAddress.attributes.postalCode,
        });
        // this.setActiveModal('addressInput');
      }
    },
  },
};
