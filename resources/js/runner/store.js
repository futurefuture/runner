import Vue from 'vue';
import Vuex from 'vuex';
import LogRocket from 'logrocket';
import createPlugin from 'logrocket-vuex';
import api from './mixins/api.js';

const logrocketPlugin = createPlugin(LogRocket);

require('es6-promise').polyfill();

const VueCookie = require('vue-cookie');

Vue.use(VueCookie);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    chosenPill: {},
    lastSubDomain: Vue.cookie.get('lastSubDomain', { domain }) || null,
    activeModal: 'none',
    selectedAddress: Vue.cookie.get('selectedAddress', { domain }) || null,
    addressIsValid: true,
    domain: window.domain,
    subDomain: '',
    user: null,
    customer: {
      attributes: {
        sources: {
          total_count: null,
          data: [{
            brand: '',
          }],
        },
      },
    },
    stores: [],
    store: {},
    menuStatus: false,
    cartStatus: false,
    storeMenuStatus: false,
    accountMenuStatus: false,
    searchStatus: false,
    loginModalStatus: 'closed',
    globalNotificationModalStatus: false,
    cart: null,
    giftOptions: {
      personalMessage: Vue.cookie.get('giftMessage', { domain }) || null,
      card: {
        id: 0,
      },
      tag: {
        id: 0,
      },
    },
    accessToken: Vue.cookie.get('token', { domain }) || Vue.cookie.get('accessToken', { domain }) || null,
    postalCode: Vue.cookie.get('postalCode', { domain }) || null,
    postalCodeIsValid: true,
    inventoryId: null,
    storeId: Vue.cookie.get('storeId', { domain }) || 1,
    userId: Vue.cookie.get('userId', { domain }),
    addAddressIsShown: false,

    storeLayout: null,
  },
  mutations: {
    SET_TEST(state, value) {
      state.test = value;
    },
    SET_CHOSEN_PILL(state, value) {
      state.chosenPill = value;
    },
    SET_ACTIVE_MODAL(state, value) {
      state.activeModal = value;
    },
    SET_NEW_STORES(state, value) {
      state.newStores = value;
    },
    SET_USER(state, value) {
      state.user = value;
      Vue.cookie.set('userId', value.id, { domain: this.domain });
    },
    TOGGLE_CLOSING_SOON(state) {
      state.closingSoon = !state.closingSoon;
    },
    TOGGLE_STORE_IS_CLOSED(state) {
      state.isClosed = !state.isClosed;
    },
    TOGGLE_API_IS_LOADING(state) {
      state.apiIsLoading = !state.apiIsLoading;
    },
    TOGGLE_MENU_STATUS(state) {
      state.menuStatus = !state.menuStatus;
    },
    TOGGLE_CART_STATUS(state) {
      state.cartStatus = !state.cartStatus;
    },
    TOGGLE_STORE_MENU_STATUS(state) {
      state.storeMenuStatus = !state.storeMenuStatus;
    },
    TOGGLE_ACCOUNT_MENU_STATUS(state) {
      state.accountMenuStatus = !state.accountMenuStatus;
    },
    TOGGLE_SEARCH_STATUS(state) {
      state.searchStatus = !state.searchStatus;
    },
    SET_STORE(state, value) {
      state.store = value;
      Vue.cookie.set('storeId', value.id, { domain });
    },
    SET_CART(state, value) {
      state.cart = value;
    },
    SET_DOMAIN(state, value) {
      state.domain = value;
    },
    SET_SUB_DOMAIN(state, value) {
      state.subDomain = value;
    },
    SET_ACCESS_TOKEN(state, value) {
      state.accessToken = value;
      Vue.cookie.set('token', value, {
        domain,
      });
    },
    SET_POSTAL_CODE(state, value) {
      state.postalCode = value;
      Vue.cookie.set('postalCode', value, { domain });
    },
    SET_INVENTORY_ID(state, value) {
      state.inventoryId = value;
      Vue.cookie.set('inventoryId', value, { domain });
    },
    SET_CUSTOMER(state, value) {
      state.customer = value;
    },
    TOGGLE_LOGIN_MODAL_STATUS(state, value) {
      state.loginModalStatus = value;
    },
    TOGGLE_POSTAL_CODE_IS_VALID(state) {
      state.postalCodeIsValid = !state.postalCodeIsValid;
    },
    TOGGLE_GLOBAL_NOTIFICATION_MODAL_STATUS(state) {
      state.globalNotificationModalStatus = !state.globalNotificationModalStatus;
    },
    SET_ADDRESSES(state, value) {
      state.addresses = value;
    },
    SET_STORES(state, value) {
      state.stores = value;
    },
    SET_SELECTED_ADDRESS(state, value) {
      state.selectedAddress = JSON.stringify(value);
      Vue.cookie.set('selectedAddress', JSON.stringify(value), { domain });
    },
    SET_GIFT_CARD(state, value) {
      state.giftOptions.card = value;
    },
    SET_GIFT_BOTTLE_TAG(state, value) {
      state.giftOptions.tag = value;
    },
    SET_GIFT_MESSAGE(state, value) {
      state.giftOptions.personalMessage = value;
      Vue.cookie.set('giftMessage', value, { domain });
    },
    SET_ADDRESS_IS_VALID(state, value) {
      state.addressIsValid = value;
    },
    SET_STORE(state, value) {
      state.store = value;
    },
  },
  actions: {
    async validatePostalCode(context, data) {
      const { domain, postalCode } = data;

      try {
        const response = await axios({
          method: 'get',
          url: `https://api.${domain}/runner/v4/address/validate?postalCode=${postalCode}&isWeb=true`,
        });

        const stores = response.data.data;

        if (stores.length > 0) {
          context.commit('SET_STORES', stores);
        } else {
          context.commit('SET_ADDRESS_IS_VALID', false);

          await context.dispatch('validatePostalCode', {
            domain,
            postalCode: 'M6J 2M5',
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    async createCart(context, data) {
      const { accessToken, userId, inventoryId } = context.state;
      const { addressIsValid } = context.getters;

      if (!addressIsValid) {
        alert('Your address is not within our delivery range. Please change your address to add to cart.');

        return;
      }


      const payload = {
        data: {
          type: 'carts',
          attributes: {
            anonymousId: data.anonymousId,
            products: data.payload,
          },
        },
      };

      await axios({
        method: 'post',
        url: `https://api.${window.domain}/runner/v4/users/${userId}/inventories/${inventoryId}/carts`,
        data: payload,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      context.dispatch('getCart', {
        userId,
        inventoryId,
        accessToken,
      });
    },
    async updateCart(context, data) {
      const { accessToken, userId, inventoryId } = context.state;

      const payload = {
        data: {
          type: 'carts',
          id: data.cartId,
          attributes: {
            anonymousId: data.anonymousId,
            products: data.payload.products,
            couponCode: data.payload.couponCode,
          },
        },
      };

      await axios({
        method: 'put',
        url: `https://api.${window.domain}/runner/v4/users/${userId}/inventories/${inventoryId}/carts/${data.cartId}`,
        data: payload,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      context.dispatch('getCart', {
        userId,
        inventoryId,
        accessToken,
      });
    },
    async getCart(context, data) {
      const { userId, inventoryId, accessToken } = data;
      let response;

      if (!userId || !inventoryId) {
        return false;
      }

      try {
        response = await axios({
          method: 'get',
          url: `https://api.${window.domain}/runner/v4/users/${userId}/inventories/${inventoryId}/carts/`,
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        if (response.data.data.length > 0) {
          context.commit('SET_CART', response.data.data[0]);
          context.commit('SET_GIFT_CARD', { id: 0 });
          context.commit('SET_GIFT_BOTTLE_TAG', { id: 0 });

          for (let i = 0; i < response.data.data[0].attributes.products.length; i++) {
            if (response.data.data[0].attributes.products[i].type === 'giftCard') {
              context.commit('SET_GIFT_CARD', response.data.data[0].attributes.products[i]);
            }

            if (response.data.data[0].attributes.products[i].type === 'bottleTag') {
              context.commit('SET_GIFT_BOTTLE_TAG', response.data.data[0].attributes.products[i]);
            }
          }
        } else {
          context.commit('SET_CART', null);
        }
      } catch (error) {
        console.error(error);
      }
    },
    async getUser(context, data) {
      const { accessToken, userId } = data;

      if (!userId || !accessToken) {
        return false;
      }

      try {
        const response = await axios({
          method: 'get',
          url: `https://api.${window.domain}/runner/v4/users/${userId}?include=addresses`,
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        const selectedAddress = response.data.data.attributes.addresses.find(address => address.attributes.selected == 1) || null;
        context.commit('SET_SELECTED_ADDRESS', selectedAddress);
        context.commit('SET_USER', response.data.data);

        LogRocket.identify(context.state.user.attributes.email);
      } catch (error) {
        console.error(error);
      }
    },
    async getStore(context, data) {
      const { storeId, postalCode } = data;

      try {
        const response = await api().get(`/stores/${storeId}?postalCode=${postalCode}`)
          .catch((error) => {
            console.log(error);
          });

        context.commit('SET_STORE', response.data.data);
      } catch (error) {
        console.error(error);
      }
    },
    async getCustomer(context) {
      try {
        const response = await api().get(`users/${context.state.user.id}/stripe/customer`);

        context.commit('SET_CUSTOMER', response.data.data);
      } catch (error) {
        console.error(error);
      }
    },
    async validate(context, data) {
      const { domain, storeId, secret } = data;
      let userId;
      let accessToken;

      // check if secret param after social login
      if (secret) {
        userId = data.userId;
        accessToken = data.accessToken;
      } else {
        userId = context.getters.userId;
        accessToken = context.getters.accessToken;
      }

      let postalCode;

      if (accessToken && userId) {
        await context.dispatch('getUser', {
          accessToken,
          userId,
        });
      }

      const { selectedAddress } = context.getters;

      if (!selectedAddress) {
        postalCode = 'M6J 2M5';

        await context.dispatch('validatePostalCode', {
          domain,
          postalCode,
        });
      } else {
        postalCode = selectedAddress.attributes.postalCode;

        await context.dispatch('validatePostalCode', {
          domain,
          postalCode,
        });
      }

      const store = context.getters.stores.find(s => s.id == storeId);
      const inventoryId = typeof store !== 'undefined' ? store.attributes.inventoryId : 0;

      if (typeof (store) === 'undefined' && (window.location.pathname === '/' || window.location.pathname.includes('product') || window.location.pathname.includes('category'))) {
        window.location.href = `https://www.${domain}/`;
      }

      context.commit('SET_INVENTORY_ID', inventoryId);

      const { addressIsValid } = context.getters;

      if (addressIsValid) {
        await context.dispatch('getStore', {
          storeId,
          postalCode,
        });
      } else {
        await context.dispatch('getStore', {
          storeId,
          postalCode: 'M6J 2M5',
        });
      }

      await context.dispatch('getCart', {
        userId,
        inventoryId,
        accessToken,
      });
    },
  },
  getters: {
    menuStatus: state => state.menuStatus,
    cartStatus: state => state.cartStatus,
    storeMenuStatus: state => state.storeMenuStatus,
    searchStatus: state => state.searchStatus,
    accountMenuStatus: state => state.accountMenuStatus,
    loginModalStatus: state => state.loginModalStatus,
    globalNotificationModalStatus: state => state.globalNotificationModalStatus,
    cart: state => state.cart,
    environment: state => state.environment,
    subDomain: state => state.subDomain,
    postalCode: state => (JSON.parse(state.selectedAddress) ? JSON.parse(state.selectedAddress).attributes.postalCode : ''),
    domain: state => state.domain,
    address: state => state.address,
    customer: state => state.customer,
    giftOptions: state => state.giftOptions,
    postalCodeIsValid: state => state.postalCodeIsValid,
    selectedAddress: state => JSON.parse(state.selectedAddress),
    test: state => state.test,

    accessToken: state => state.accessToken,
    isAuthenticated: state => !!state.accessToken && !!state.userId,
    userId: state => state.userId,
    user: state => state.user,
    stores: state => state.stores,
    storeLayout: state => state.formattedStore,
    activeModal: state => state.activeModal,
    store: state => state.store,
    inventoryId: state => state.inventoryId,
    addressIsValid: state => state.addressIsValid,
  },

  plugins: [
    logrocketPlugin,
  ],
});

export default store;
