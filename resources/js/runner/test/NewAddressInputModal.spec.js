import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import NewAddressInputModal from '../components/NewAddressInputModal.vue';
import '@babel/polyfill';
import 'babel-polyfill';
import './app';

describe('NewAddressInputModal.vue', () => {
  let getters;
  let store;

  beforeEach(() => {
    getters = {
      clicks: () => 2,
      inputValue: () => 'input',
    };

    store = new Vuex.Store({
      getters,
    });
  });

  it('Should not appear if postal code exist', () => {
    const wrapper = shallowMount(NewAddressInputModal, {
      propsData: {
        subDomain: 'www',
        postalCode: 'L4Z 3P4',
        stores: [],
      },
    });
    const modal = wrapper.find('#addressInputModal');
    expect(modal.exists()).toBe(false);
  });
  it('Should appear if postal code does not exist', () => {
    const wrapper = shallowMount(NewAddressInputModal, {
      propsData: {
        subDomain: 'www',
        postalCode: null,
        stores: [],
      },
    });
    const modal = wrapper.find('#addressInputModal');
    expect(modal.exists()).toBe(true);
  });
});
