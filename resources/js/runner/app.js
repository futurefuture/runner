
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from 'vue';
import DatePicker from 'vue2-datepicker';
import VueTheMask from 'vue-the-mask';
import VueLazyload from 'vue-lazyload';
import LogRocket from 'logrocket';
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';
import Notifications from 'vue-notification';
import ToggleButton from 'vue-js-toggle-button';
import Vuelidate from 'vuelidate';
import store from './store';
import 'url-search-params-polyfill';

require('es6-promise').polyfill();
require('./bootstrap');

window.moment = require('moment');
const SocialSharing = require('vue-social-sharing');
const VueCookie = require('vue-cookie');

let SENTRY_ENV = 'production';

if (window.location.host.includes('staging')) {
  SENTRY_ENV = 'staging';
} else if (window.location.host.includes('test')) {
  SENTRY_ENV = 'development';
}

Sentry.init({
  environment: SENTRY_ENV,
  dsn: 'https://41fd203ccc6743d49bad4672368bc784@sentry.io/1518719',
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true,
    }),
  ],
});
Vue.use(VueTheMask);
Vue.use(SocialSharing);
Vue.use(VueCookie);
Vue.use(Notifications);
Vue.use(require('vue-moment'));

Vue.use(Vuelidate);

Vue.use(ToggleButton);
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'https://runner-file-manager.s3.amazonaws.com/products/ProductGeneralNoImage.jpg',
  loading: 'https://runner-file-manager.s3.amazonaws.com/products/ProductGeneralNoImage.jpg',
  attempt: 1,
});
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDrPkhS9nd8nO-V0rXBuDBi7pPQCepKfes',
    v: '3',
    libraries: 'places,geometry',
  },
});
if (window.location.host.indexOf('getrunner.io') > -1) {
  LogRocket.init('du0spc/runner');

  LogRocket.getSessionURL((sessionURL) => {
    Sentry.configureScope((scope) => {
      scope.setExtra('sessionURL', sessionURL);
    });
  });
}

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
  'X-CSRF-TOKEN': window.Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest',
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// pages
Vue.component('ErrorNotFound', require('./pages/ErrorNotFound.vue'));
Vue.component('Product', require('./pages/Product.vue'));
Vue.component('Category', require('./pages/Category.vue'));
Vue.component('Tags', require('./pages/Tags.vue'));
Vue.component('Explore', require('./pages/Explore.vue'));
Vue.component('Addresses', require('./pages/Addresses.vue'));
Vue.component('NewPaymentMethods', require('./pages/NewPaymentMethods.vue'));
Vue.component('Order', require('./pages/Order.vue'));
Vue.component('Orders', require('./pages/Orders.vue'));
Vue.component('RewardPointsPage', require('./pages/RewardPointsPage.vue'));
Vue.component('InviteFriends', require('./pages/InviteFriends.vue'));
Vue.component('NewSearchResults', require('./pages/NewSearchResults.vue'));
Vue.component('Login', require('./pages/Login.vue'));
Vue.component('Register', require('./pages/Register.vue'));
Vue.component('Careers', require('./pages/Careers.vue'));
Vue.component('RunnerApplication', require('./pages/RunnerApplication.vue'));
Vue.component('AccountSettings', require('./pages/AccountSettings.vue'));
Vue.component('About', require('./pages/About.vue'));
Vue.component('Confirmation', require('./pages/Confirmation.vue'));
Vue.component('PrivacyPolicy', require('./pages/PrivacyPolicy.vue'));
Vue.component('TermsConditions', require('./pages/TermsConditions.vue'));
Vue.component('CheckoutV3', require('./pages/Checkout.vue'));
// Vue.component('CheckoutV3', require('./pages/NewCheckout.vue'));

// components
Vue.component('BreadCrumbs', require('./components/BreadCrumbs.vue'));
Vue.component('RelatedProducts', require('./components/RelatedProducts.vue'));
Vue.component('Reviews', require('./components/Reviews.vue'));
Vue.component('AddressInputModal', require('./components/AddressInputModal.vue'));
Vue.component('ProductCard', require('./components/ProductCard.vue'));
Vue.component('AddToCart', require('./components/AddToCart.vue'));
Vue.component('FixedHeader', require('./components/FixedHeader.vue'));
Vue.component('ReviewModal', require('./components/ReviewModal.vue'));
Vue.component('NewSideNav', require('./components/NewSideNav.vue'));
Vue.component('NewStoresModal', require('./components/NewStoresModal.vue'));
Vue.component('NewStoreLayout', require('./components/NewStoreLayout.vue'));
Vue.component('NewSkeletonBox', require('./components/NewSkeletonBox.vue'));
Vue.component('MainFooter', require('./components/MainFooter.vue'));
Vue.component('RewardPoints', require('./components/RewardPoints.vue'));
Vue.component('Pagination', require('./components/Pagination.vue'));
Vue.component('UsualAddToCart', require('./components/UsualAddToCart.vue'));
Vue.component('cart', require('./components/Cart.vue'));
Vue.component('Favourites', require('./components/Favourites.vue'));
Vue.component('NewSearch', require('./components/NewSearch.vue'));
Vue.component('FilterSideNav', require('./components/FilterSideNav.vue'));
Vue.component('NewScrollToTop', require('./components/NewScrollToTop.vue'));
Vue.component('NewSecondaryCopy', require('./components/NewSecondaryCopy.vue'));
Vue.component('CartAddOns', require('./components/CartAddOns.vue'));
Vue.component('AddressChangedModal', require('./components/AddressChangedModal.vue'));
Vue.component('AccountMenu', require('./components/AccountMenu.vue'));
Vue.component('NotificationModal', require('./components/NotificationModal.vue'));

// layouts
Vue.component('MasterLayout', require('./layouts/MasterLayout.vue'));

// utilities
Vue.component('ButtonComponent', require('./utilities/ButtonComponent.vue'));

// V3
Vue.component('CartV3', require('./components/cart/Cart.vue'));
Vue.component('ToolsV3', require('./components/Tools.vue'));
Vue.component('SearchV3', require('./components/Search.vue'));
Vue.component('LoginModal', require('./components/LoginModal.vue'));
Vue.component('GlobalNotificationModal', require('./components/GlobalNotificationModal.vue'));
Vue.component('MoonLoaderWrapper', require('./components/MoonLoaderWrapper.vue'));
Vue.component('Avatar', require('./components/Avatar.vue'));

// V3 Gift
Vue.component('GiftCart', require('./components/GiftCart.vue'));

// V3 Checkout
Vue.component('PaymentMethod', require('./components/PaymentMethod.vue'));
Vue.component('CheckoutAddress', require('./components/checkout/Address.vue'));
Vue.component('CheckoutCart', require('./components/checkout/CheckoutCart.vue'));

// V4 Checkout
Vue.component('NewCheckoutAddress', require('./components/new-checkout/NewAddress.vue'));
Vue.component('NewPaymentMethod', require('./components/NewPaymentMethod.vue'));
Vue.component('NewCheckoutCart', require('./components/new-checkout/NewCheckoutCart.vue'));
Vue.component('NewRecipientDetails', require('./components/new-checkout/NewRecipientDetails.vue'));
Vue.component('NewPersonalMessage', require('./components/new-checkout/NewPersonalMessage.vue'));


// Loaders
Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('clip-loader', require('vue-spinner/src/ClipLoader.vue'));
Vue.component('moon-loader', require('vue-spinner/src/MoonLoader.vue'));

// Landing Pages
Vue.component('Usual', require('./components/landing-pages/Usual.vue'));
Vue.component('PizzaRoulette', require('./components/landing-pages/PizzaRoulette.vue'));

// Explore Components
Vue.component('PaymentInformation', require('./components/PaymentInformation.vue'));
Vue.component('PaymentInfo', require('./components/PaymentInfo.vue'));
Vue.component('Search', require('./components/Search.vue'));
Vue.component('Tools', require('./components/Tools.vue'));
Vue.component('SearchBar', require('./components/SearchBar.vue'));
Vue.component('Faq', require('./pages/Faq.vue'));
Vue.component('Rewards', require('./components/Rewards.vue'));
Vue.component('RewardsChart', require('./components/RewardsChart.vue'));
Vue.component('SecondaryCopy', require('./components/SecondaryCopy.vue'));
Vue.component('Sop', require('./pages/Sop.vue'));

Vue.component('DatePicker', DatePicker);

/**
 * Debug
 */

if (window.location.host.indexOf('getrunner.io') > -1) {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

/**
 *
 * App Initialization
 */
const app = new Vue({
  store,
}).$mount('#app');
