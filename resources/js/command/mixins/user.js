export default {
  created() {
    if (this.token) {
      this.$cookie.set('token', this.token);
    }

    if (this.$cookie.get('token')) {
      this.getUser();
    }
  },
  computed: {
    user() {
      return this.$store.state.v2.user;
    },
  },
  props: [
    'token',
  ],
  methods: {
    getUser() {
      this.$store.dispatch('v2/getUser', {
        token: this.$cookie.get('token'),
      });
    },
  },
};
