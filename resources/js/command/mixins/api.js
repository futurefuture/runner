import Vue from 'vue';
import axios from 'axios';

export default () => axios.create({
  baseURL: '/api/v1/command/',
  headers: {
    Authorization: `Bearer ${Vue.cookie.get('token', { domain: window.domain })}`,
    Accept: 'application/json',
    ContentType: 'application/json',
  },
});
