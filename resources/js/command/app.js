
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

import Vue from 'vue';
import DatePicker from 'vue2-datepicker';
import VueTheMask from 'vue-the-mask';
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';
import Notifications from 'vue-notification';
import VJsoneditor from 'v-jsoneditor';

require('./bootstrap');

window.moment = require('moment');
const VueCookie = require('vue-cookie');

if (window.location.host.indexOf('getrunner.io') > -1) {
  Sentry.init({
    dsn: 'https://eed031f26818458f94591a2539063261@sentry.io/1518722',
    integrations: [
      new Integrations.Vue({
        Vue,
        attachProps: true,
      }),
    ],
  });
}
Vue.use(VueTheMask);
Vue.use(VueCookie);
Vue.use(Notifications);
Vue.use(require('vue-moment'));

Vue.use(VJsoneditor);


/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
  'X-CSRF-TOKEN': window.Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest',
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// Utils
Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('clip-loader', require('vue-spinner/src/ClipLoader.vue'));

// Command
Vue.component('CommandHeader', require('./components/Header.vue'));
Vue.component('CommandNav', require('./components/Nav.vue'));
Vue.component('CommandReports', require('./components/Reports.vue'));
Vue.component('CommandSegments', require('./components/Segments.vue'));
Vue.component('CommandOrders', require('./components/Orders.vue'));
Vue.component('CommandFeaturedProducts', require('./components/FeaturedProducts.vue'));
Vue.component('CommandProducts', require('./components/Products.vue'));
Vue.component('CommandProduct', require('./components/Product.vue'));
Vue.component('CommandCartStarters', require('./components/CartStarters.vue'));
Vue.component('CommandCartStarter', require('./components/CartStarter.vue'));
Vue.component('CommandBundles', require('./components/Bundles.vue'));
Vue.component('CommandBundle', require('./components/Bundle.vue'));
Vue.component('CommandCustomProducts', require('./components/CustomProducts.vue'));
Vue.component('CommandCustomProduct', require('./components/CustomProduct.vue'));
Vue.component('CommandEmployees', require('./components/Employees.vue'));
Vue.component('CommandEmployee', require('./components/Employee.vue'));
Vue.component('CommandClients', require('./components/Clients.vue'));
Vue.component('CommandClient', require('./components/Client.vue'));
Vue.component('CommandSchedule', require('./components/Schedule.vue'));
Vue.component('CommandRunnerPositions', require('./components//RunnerPositions.vue'));
Vue.component('CommandReviews', require('./components/Reviews.vue'));
Vue.component('CommandCustomers', require('./components/Customers.vue'));
Vue.component('CommandNotifications', require('./components/Notifications.vue'));
Vue.component('CommandFcm', require('./components/Fcm.vue'));
Vue.component('CommandFcmAddress', require('./components/AddressSearch.vue'));
Vue.component('CommandCoupons', require('./components/Coupons.vue'));
Vue.component('CommandInvitedUsers', require('./components/InvitedUsers.vue'));
Vue.component('CommandStore', require('./components/Store.vue'));
Vue.component('CommandStores', require('./components/Stores.vue'));
Vue.component('CommandNumbers', require('./components/Numbers.vue'));
Vue.component('CommandOverview', require('./components/Overview.vue'));
Vue.component('CommandTags', require('./components/Tags.vue'));
Vue.component('CommandConfig', require('./components/Config.vue'));

Vue.component('DatePicker', DatePicker);

/**
 * Debug
 */

if (location.host === 'www.getrunner.io') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

/**
 *
 * App Initialization
 */
const app = new Vue().$mount('#app');
