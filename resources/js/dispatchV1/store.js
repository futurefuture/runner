import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import api from './mixins/api.js';

const VueCookie = require('vue-cookie');

Vue.use(VueCookie);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    domain: window.domain,
    accessToken: Vue.cookie.get('token', { domain }) || '',
    user: null,
    userId: Vue.cookie.get('userId', { domain }) || 131,
    cart: {
      attributes: {
        coupon: {},
        products: [],
      },
    },
    inventoryId: Vue.cookie.get('inventoryId', { domain }) || 1,
  },
  getters: {
    cart: state => state.cart,
    user: state => state.user,
  },
  mutations: {
    SET_USER(state, value) {
      state.user = value;
    },
    SET_CART(state, cart) {
      state.cart = cart;
    },
  },
  actions: {
    getUser(context, data) {
      if (context.state.accessToken) {
        return new Promise((resolve, reject) => {
          api().get('/users')
            .then((response) => {
              context.commit('SET_USER', response.data.data);
              resolve(response);
            })
            .catch((error) => {
              console.log(error);
              reject(error);
            });
        });
      }
    },
    getAuth(context, data) {
      axios({
        method: 'get',
        url: '/api/v1/user',
      })
        .then((response) => {
          context.commit('setUser', response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    getCart(context) {
      const inventoryId = context.state.inventoryId;
      const userId = context.state.userId;

      return new Promise((resolve, reject) => {
        axios({
          method: 'get',
          url: `https://api.${window.domain}/v3/runner/users/${userId}/inventories/${inventoryId}/carts`,
          headers: {
            Authorization: `Bearer ${Vue.cookie.get('token', { domain: window.domain })}`,
          },
        })
          .then((response) => {
            if (response.data.data.length > 0) {
              context.commit('SET_CART', response.data.data[0]);
            } else {
              const cart = {
                attributes: {
                  coupon: {},
                  products: [],
                },
              };
              context.commit('SET_CART', cart);
            }
            resolve(response);
          })
          .catch((error) => {
            reject(error);
            console.log(error);
          });
      });
    },
  },
});

export default store;
