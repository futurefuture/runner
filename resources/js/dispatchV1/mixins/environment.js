export default {
	methods: {
		getAppEnvironment: function() {
			this.$store.dispatch('v2/getAppEnvironment');
		}
	}
};