export default {
  methods: {
    getTodayHours() {
      const day = moment().day();
      let schedule;
      let tomorrowSchedule;
      const vm = this;

      if (day === 1) {
        schedule = 'monday_schedule';
        tomorrowSchedule = 'tuesday_schedule';
      } else if (day === 2) {
        schedule = 'tuesday_schedule';
        tomorrowSchedule = 'wednesday_schedule';
      } else if (day === 3) {
        schedule = 'wednesday_schedule';
        tomorrowSchedule = 'thursday_schedule';
      } else if (day === 4) {
        schedule = 'thursday_schedule';
        tomorrowSchedule = 'friday_schedule';
      } else if (day === 5) {
        schedule = 'friday_schedule';
        tomorrowSchedule = 'saturday_schedule';
      } else if (day === 6) {
        schedule = 'saturday_schedule';
        tomorrowSchedule = 'sunday_schedule';
      } else {
        schedule = 'sunday_schedule';
        tomorrowSchedule = 'monday_schedule';
      }

      axios.get('/api/v1/options/hours')
        .then((response) => {
          vm.todayHours.close = response.data[schedule].close;
          vm.todayHours.open = response.data[schedule].open;
          vm.tomorrowOpenAt = response.data[tomorrowSchedule].open;
          vm.getTodayHoursMessage();
          vm.storeIsOpen();
        })
        .catch((error) => {
          console.log(error);
        });
    },
    getHours() {
      const vm = this;

      axios.get('/api/v1/options/hours')
        .then((response) => {
          vm.hours = response.data;
        })
        .catch((error) => {
          console.log(error);
        });
    },
    toTwelveHour(value) {
      return moment(value, [
        'HH',
      ]).format('hA');
    },
    storeIsOpen() {
      const hour = moment().hour();

      if (this.todayHours.open <= hour && hour < this.todayHours.close) {
        this.storeOpen = true;
      } else {
        this.storeOpen = false;
        this.time = 'scheduled';
      }
    },
    getTodayHoursMessage() {
      const hour = moment().hour();

      if (hour < this.todayHours.open) {
        this.todayHoursMessage = ' Open Today At ';
        this.todayHoursMessageHour = this.toTwelveHour(this.todayHours.open);
      } else if (this.todayHours.open <= hour && hour < this.todayHours.close) {
        this.todayHoursMessage = ' Open Today Until ';
        this.todayHoursMessageHour = this.toTwelveHour(this.todayHours.close);
      } else {
        this.todayHoursMessage = ' Open Tomorrow Morning At ';
        this.todayHoursMessageHour = this.toTwelveHour(this.tomorrowOpenAt);
      }
    },
  },
};
