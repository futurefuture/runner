
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from 'vue';
import moment from 'moment-timezone';
import DatePicker from 'vue2-datepicker';
import VueTheMask from 'vue-the-mask';
import Raven from 'raven-js';
import VTooltip from 'v-tooltip';
import FullCalendar from 'vue-full-calendar';
import Vuetify from 'vuetify';
import Notifications from 'vue-notification';
import store from './store';

require('./bootstrap');

window.moment = require('moment');
const VueCookie = require('vue-cookie');

Raven
  .config('https://9e8eab099a2b4e3e85906002d18207c4@sentry.io/1005887')
  .install();
Vue.use(VueTheMask);
Vue.use(VueCookie);
Vue.use(Notifications);
Vue.use(FullCalendar);
Vue.use(VTooltip);
Vue.use(require('vue-moment'));

Vue.use(Vuetify);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDrPkhS9nd8nO-V0rXBuDBi7pPQCepKfes',
    v: '3',
    libraries: 'places,geometry',
  },
});
if (window.userIp && !window.userIp.indexOf('99.230.165.50') > -1) {
  Raven.setDataCallback((data) => {
    data.extra.sessionURL = LogRocket.sessionURL;

    return data;
  });

  LogRocket.init('du0spc/runner');
}

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
  'X-CSRF-TOKEN': window.Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest',
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// Loaders
Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('clip-loader', require('vue-spinner/src/ClipLoader.vue'));

// Dispatch
Vue.component('DispatchCustomersOrder', require('./components/DispatchCustomersOrder.vue'));
Vue.component('DispatchOrders', require('./components/Orders.vue'));
Vue.component('DispatchCouriers', require('./components/Couriers.vue'));
Vue.component('DispatchOrder', require('./components/Order.vue'));
Vue.component('DispatchOrderAddress', require('./components/OrderAddress.vue'));
Vue.component('DispatchOptions', require('./components/Options.vue'));
Vue.component('DispatchHeader', require('./components/Header.vue'));
Vue.component('DispatchMap', require('./components/Map.vue'));
Vue.component('DispatchNav', require('./components/Nav.vue'));
Vue.component('DispatchCustomers', require('./components/Customers.vue'));
Vue.component('DispatchCustomer', require('./components/Customer.vue'));
Vue.component('DispatchCustomerAddress', require('./components/CustomerAddress.vue'));
Vue.component('DispatchSchedule', require('./components/Schedule.vue'));
Vue.component('Staff', require('./components/Staff.vue'));

Vue.component('DatePicker', DatePicker);

/**
 * Debug
 */

if (location.host === 'www.getrunner.io') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

/**
 *
 * App Initialization
 */
const app = new Vue({
  store,
}).$mount('#app');
