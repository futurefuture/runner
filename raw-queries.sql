SELECT
	*
FROM
	php_production.order_completed
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND device = 2;


SELECT
	COUNT(DISTINCT javascript_production.pages.anonymous_id)
FROM
	php_production.order_completed
	JOIN javascript_production.pages ON php_production.order_completed.anonymous_id = javascript_production.pages.anonymous_id
WHERE
	php_production.order_completed.received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND device = 2
	AND path LIKE '%/checkout%';


SELECT
	*
FROM
	javascript_production.pages
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND path LIKE '%/checkout%';





-- total add to carts
SELECT
	COUNT(*) AS total_add_to_carts
FROM (
	SELECT
		COUNT(*),
		anonymous_id
	FROM ( SELECT DISTINCT
			anonymous_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (partner_ids,
				seq.i) AS partner,
			product_id,
			user_id,
			received_at
		FROM
			php_production.product_added,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(partner_ids))
	WHERE
		partner = 11
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00'
	GROUP BY
		anonymous_id);		
		
-- orders by device
SELECT
	COUNT(*), device, order_id
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products,
				seq.i,
				TRUE) AS product,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN (844)
GROUP BY
	device, order_id;

-- orders by hour
SELECT
	hour,
	COUNT(*)
FROM (
	SELECT
		EXTRACT(hour FROM received_at) AS hour,
		order_id,
		COUNT(*)
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			received_at,
			order_id
		FROM (
			SELECT
				order_id,
				received_at,
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product
			FROM
				php_production.order_completed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-13 00:00:00'
				AND '2020-02-14 00:00:00')
		WHERE
			product_id IN(844))
	GROUP BY
		order_id, hour)
GROUP BY
	hour
ORDER BY
	hour;

-- get web product list filtered
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.product_list_filtered,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844);
	
-- get web product list viewed
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.product_list_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844);

-- get ios product list filtered
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.product_list_filtered,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844);

-- get ios product list viewed
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.product_list_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(' . $this->ps . ');

-- get web promotion
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.promotion_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844);

-- get ios promotion
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.promotion_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844);

-- show
SELECT
	SUM(quantity * retail_price) AS revenue,
	day,
	month,
	year
FROM (
	SELECT
		order_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity,
		JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price,
		day,
		month,
		year
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			EXTRACT(day FROM timestamp) AS day,
			EXTRACT(month FROM timestamp) AS month,
			EXTRACT(year FROM timestamp) AS year
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND timestamp::date BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00')
	WHERE
		product_id IN(844))
GROUP BY
	month, day, year
ORDER BY
	month,
	day ASC;

-- web product views
SELECT
	SUM(click)
FROM (
	SELECT
		COUNT(*) AS click,
		product_id,
		store_id
	FROM (
		SELECT
			product_id,
			store_id,
			received_at
		FROM
			javascript_production.product_viewed
		WHERE
			received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00')
	WHERE
		product_id IN(844)
	GROUP BY
		product_id, store_id);

-- ios product views
SELECT
	SUM(click)
FROM (
	SELECT
		COUNT(*) AS click,
		params_product_id,
		params_store_id
	FROM (
		SELECT
			params_product_id,
			params_store_id,
			received_at
		FROM
			ios_production.product_viewed
		WHERE
			received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00')
	WHERE
		params_product_id IN(844)
	GROUP BY
		params_product_id, params_store_id);

-- anonymous_id of campaigns for backtracks 
SELECT DISTINCT
	context_campaign_name,
	anonymous_id,
	context_campaign_source
FROM
	javascript_production.pages
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					SUM(JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id, anonymous_id FROM (
						SELECT
							order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i, TRUE) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products, TRUE)
							AND received_at BETWEEN '2020-02-13 00:00:00'
							AND '2020-02-14 00:00:00')
					WHERE
						product_id IN(844)
					GROUP BY
						anonymous_id, product_id))
	AND received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00'
	AND context_campaign_name IS NOT NULL
GROUP BY
	context_campaign_name, anonymous_id, context_campaign_source;

-- order sales of anonymous id of campaign backtracks
SELECT
	SUM(JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) * JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE)) AS retail_price,
	JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
	anonymous_id
FROM (
	SELECT
		order_id,
		JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
			seq.i,
			TRUE) AS product,
		store_id,
		received_at,
		anonymous_id
	FROM
		php_production.order_completed,
		seq_0_to_20 AS seq
	WHERE
		seq.i < JSON_ARRAY_LENGTH(products, TRUE)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00')
WHERE
	product_id IN(844)
	AND anonymous_id = '12312312'
GROUP BY
	anonymous_id, product_id;

-- order transactions of anonymous id of campaign backtracks
SELECT
	COUNT(*),
	JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
	anonymous_id
FROM (
	SELECT
		order_id,
		JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
			seq.i,
			TRUE) AS product,
		store_id,
		received_at,
		anonymous_id
	FROM
		php_production.order_completed,
		seq_0_to_20 AS seq
	WHERE
		seq.i < JSON_ARRAY_LENGTH(products, TRUE)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00')
WHERE
	product_id IN(844)
	AND anonymous_id = '23123'
GROUP BY
	anonymous_id, product_id;

-- add to carts of anonymous id of campaign backtracks
SELECT
	COUNT(*)
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00'
	AND anonymous_id = '2345'
	AND product_id IN(844);

-- page views of anonymous id of campaign backtracks
SELECT
	COUNT(*)
FROM
	javascript_production.pages
WHERE
	received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00'
	AND context_campaign_name = 'EMAIL_TEST';

-- get sales by campaign top campaigns
SELECT
	context_campaign_name,
	COUNT(*),
	SPLIT_PART(context_page_path, '/', 3) AS slug
FROM
	javascript_production.pages
WHERE
	context_campaign_name IS NOT NULL
	AND slug IN('TEST')
	AND received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00'
GROUP BY
	context_campaign_name,
	slug
ORDER BY
	COUNT(*)
	DESC;

-- get sales by campaign order query
SELECT
	SUM(total_sales) AS email_sales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) * JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE)) AS total_sales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'
			AND anonymous_id IN( SELECT DISTINCT
					anonymous_id FROM javascript_production.pages
				WHERE
					anonymous_id IN(
						SELECT
							anonymous_id FROM javascript_production.pages
						WHERE
							context_campaign_name = 'EMAIL_TEST'
							AND received_at BETWEEN '2020-02-13 00:00:00'
							AND '2020-02-14 00:00:00')
						AND received_at BETWEEN '2020-02-13 00:00:00'
						AND '2020-02-14 00:00:00'))
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00'
	GROUP BY
		product_id, received_at);

-- get sales by campaign add to carts
SELECT
	COUNT(*)
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-14 00:00:00'
	AND anonymous_id IN( SELECT DISTINCT
			anonymous_id FROM javascript_production.pages
		WHERE
			anonymous_id IN(
				SELECT
					anonymous_id FROM javascript_production.pages
				WHERE
					context_campaign_name = 'EMAIL_TEST'
					AND received_at BETWEEN '2020-02-13 00:00:00'
					AND '2020-02-14 00:00:00'))
	AND product_id IN(844);

-- get sales by campaign transactions
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales,
		order_id
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			timestamp::date
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND timestamp::date BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'
			AND anonymous_id IN( SELECT DISTINCT
					anonymous_id FROM javascript_production.pages
				WHERE
					anonymous_id IN(
						SELECT
							anonymous_id FROM javascript_production.pages
						WHERE
							context_campaign_name = 'EMAIL_TEST'
							AND timestamp::date BETWEEN '2020-02-13 00:00:00'
							AND '2020-02-14 00:00:00')
						AND timestamp::date BETWEEN '2020-02-13 00:00:00'
						AND '2020-02-14 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, order_id);

-- get sales by campaign sources 
SELECT DISTINCT
	context_campaign_source
FROM
	javascript_production.pages
WHERE
	context_campaign_name = 'EMAIL_TEST'
	AND received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00';

-- get sales by medium email
SELECT
	SUM(totalSales) AS emailSales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) * JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE)) AS totalSales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			cogs,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'
			AND anonymous_id IN(
				SELECT
					anonymous_id FROM (
						SELECT
							path, anonymous_id, context_page_search FROM javascript_production.pages
						WHERE
							received_at BETWEEN '2020-02-13 00:00:00'
							AND '2020-02-14 00:00:00'
							AND anonymous_id IN(
								SELECT
									anonymous_id FROM javascript_production.pages
								WHERE
									context_campaign_medium = 'email'
									AND received_at BETWEEN '2020-02-13 00:00:00'
									AND '2020-02-14 00:00:00'
								GROUP BY
									anonymous_id))
						WHERE
							path = '/confirmation'
						GROUP BY
							anonymous_id, path))
	WHERE
		product_id IN(844)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00'
	GROUP BY
		product_id, received_at);

-- get sales by medium digital
SELECT
	SUM(totalSales) AS digitialSales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) * JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE)) AS totalSales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'
			AND anonymous_id IN(
				SELECT
					anonymous_id FROM (
						SELECT
							path, anonymous_id, context_page_search FROM javascript_production.pages
						WHERE
							received_at BETWEEN '2020-02-13 00:00:00'
							AND '2020-02-14 00:00:00'
							AND anonymous_id IN(
								SELECT
									anonymous_id FROM javascript_production.pages
								WHERE
									context_campaign_medium = 'digital'
									AND received_at BETWEEN '2020-02-13 00:00:00'
									AND '2020-02-14 00:00:00'
								GROUP BY
									anonymous_id))
						WHERE
							path = '/confirmation'
						GROUP BY
							anonymous_id, path))
	WHERE
		product_id IN(844)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00'
	GROUP BY
		product_id, received_at);

-- get session by source
SELECT
	SUM(totalClick),
	context_campaign_source
FROM (
	SELECT
		context_campaign_source,
		COUNT(*) AS totalClick,
		split_part(context_page_path, '/', 3) AS slug
	FROM
		javascript_production.pages
	WHERE
		slug IN('/test')
		AND context_campaign_source IS NOT NULL
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00'
	GROUP BY
		context_campaign_source,
		slug)
GROUP BY
	context_campaign_source
ORDER BY
	sum DESC
LIMIT 5;

-- get sessions web
SELECT
	COUNT(*) AS session
FROM ( SELECT DISTINCT
		anonymous_id
	FROM
		javascript_production.product_viewed
	WHERE
		product_id IN(844)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00');

-- get sessions ios
SELECT
	COUNT(*) AS session
FROM ( SELECT DISTINCT
		anonymous_id
	FROM
		ios_production.product_viewed
	WHERE
		params_product_id IN(844)
		AND received_at BETWEEN '2020-02-13 00:00:00'
		AND '2020-02-14 00:00:00');

-- top product pages
SELECT
	path,
	COUNT(*),
	SPLIT_PART(context_page_path, '/', 3) AS slug
FROM
	javascript_production.pages
WHERE
	slug IN('\test')
	AND received_at BETWEEN '2020-02-13 00:00:00'
	AND '2020-02-14 00:00:00'
GROUP BY
	path,
	slug
ORDER BY
	COUNT(*)
	DESC
LIMIT 5;

-- top products
SELECT
	product_id,
	SUM(quantity * retail_price) AS revenue
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
		JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) AS quantity,
		JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) AS retail_price,
		order_id,
		store_id
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			store_id,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00'))
WHERE
	product_id IN(844)
GROUP BY
	product_id
ORDER BY
	revenue DESC
LIMIT 5;

-- get total customers
SELECT
	COUNT(DISTINCT user_id) AS totalCustomer
FROM (
	SELECT
		user_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM ( SELECT DISTINCT
			user_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00')
	WHERE
		product_id IN(844));

-- get total orders
SELECT
	SUM(revenue) AS revenue,
	COUNT(*) AS
ORDER
FROM (
	SELECT
		SUM(retail_price * quantity) AS revenue,
		order_id,
		COUNT(*) AS totalOrders
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) AS quantity,
			JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) AS retail_price,
			order_id,
			store_id,
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
		FROM (
			SELECT
				order_id,
				cogs,
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				store_id,
				received_at
			FROM
				php_production.order_completed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-13 00:00:00'
				AND '2020-02-14 00:00:00'))
	WHERE
		product_id IN(844)
	GROUP BY
		order_id);

-- get total sales
SELECT
	SUM(revenue) AS revenue,
	SUM(totalOrders) AS
ORDER
FROM (
	SELECT
		SUM(retail_price * quantity) AS revenue,
		store_id,
		order_id,
		COUNT(*) AS totalOrders
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) AS quantity,
			JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) AS retail_price,
			order_id,
			store_id,
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
		FROM (
			SELECT
				order_id,
				cogs,
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				store_id,
				received_at
			FROM
				php_production.order_completed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-13 00:00:00'
				AND '2020-02-14 00:00:00'))
	WHERE
		product_id IN(844)
	GROUP BY
		store_id, order_id);

-- get transactions
SELECT
	COUNT(*),
	anonymous_id
FROM ( SELECT DISTINCT
		anonymous_id,
		order_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM ( SELECT DISTINCT
			anonymous_id,
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-13 00:00:00'
			AND '2020-02-14 00:00:00')
	WHERE
		product_id IN(844))
GROUP BY
	anonymous_id;




--- PRODUCTS
-- product analytics page views web
SELECT
	COUNT(*),
	product_id,
	store_id
FROM
	javascript_production.product_viewed
WHERE
	product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	product_id,
	store_id
ORDER BY
	store_id ASC;


-- product analytics page views ios
SELECT
	COUNT(*),
	params_product_id AS product_id,
	params_store_id AS store_id
FROM
	ios_production.product_viewed
WHERE
	params_product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND params_product_id = ' . $this->product . '
GROUP BY
	params_product_id,
	params_store_id
ORDER BY
	params_store_id ASC;

-- unique product page views web
SELECT
	COUNT(*),
	store_id
FROM (
	SELECT
		COUNT(*),
		product_id,
		store_id,
		anonymous_id
	FROM
		javascript_production.product_viewed
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
	GROUP BY
		product_id,
		store_id,
		anonymous_id)
GROUP BY
	store_id
ORDER BY
	store_id ASC;

-- unique product page views ios
SELECT
	COUNT(*),
	params_store_id AS store_id
FROM (
	SELECT
		COUNT(*),
		params_product_id,
		params_store_id,
		anonymous_id
	FROM
		ios_production.product_viewed
	WHERE
		params_product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
		AND params_product_id = ' . $this->product . '
	GROUP BY
		params_product_id,
		params_store_id,
		anonymous_id)
GROUP BY
	params_store_id
ORDER BY
	params_store_id;

-- analytics add to carts web
SELECT
	anonymous_id
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND product_id = 844
	AND anonymous_id IS NOT NULL;

-- analytics add to carts web - product viewed
SELECT
	COUNT(*),
	store_id
FROM
	javascript_production.product_viewed
WHERE
	anonymous_id IN('12312')
	AND product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:0'
	AND '2020-02-15 00:00:00'
GROUP BY
	store_id
ORDER BY
	store_id;

-- analytics add to carts ios - product viewed
SELECT
	COUNT(*),
	params_store_id AS store_id
FROM
	ios_production.product_viewed
WHERE
	lower(anonymous_id)
	IN('123')
	AND params_product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:0'
	AND '2020-02-15 00:00:00'
GROUP BY
	params_store_id
ORDER BY
	params_store_id ASC;

-- analytics store purchases
SELECT
	COUNT(*),
	store_id,
	device
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:0'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844
GROUP BY
	store_id, device
ORDER BY
	store_id ASC;


-- analytics store quantity
SELECT
	SUM(quantity) AS quantity,
	store_id,
	device
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:0'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844
GROUP BY
	store_id, device
ORDER BY
	store_id ASC;

-- analytics store revenue
SELECT
	SUM(quantity * retail_price) AS revenue,
	store_id,
	device
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity,
		JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			cogs,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:0'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844
GROUP BY
	product_id, store_id, device
ORDER BY
	store_id ASC;

-- broad category rank
SELECT product, rank FROM(SELECT product, totRevenue, @curRank := @curRank + 1 AS rank FROM(SELECT product_id AS product, ROUND(SUM(quantity * retail_price) / 100, 2) AS totRevenue FROM order_items WHERE DATE(created_at) BETWEEN '2020-02-14 00:00:00' AND '2020-02-15 00:00:00' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 ON e1.id = e.parent_id WHERE 2 IN(r.parent_id, e.parent_id, e1.parent_id) UNION SELECT id FROM categories WHERE id = 2) GROUP BY product_id ORDER BY totRevenue DESC) AS test, (SELECT @curRank := 0) r) AS rank WHERE product = 844;

-- get product impressions -- web list filtered
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id,
		store_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			store_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				store_id,
				received_at
			FROM
				javascript_production.product_list_filtered,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, store_id);

-- get product impressions -- web list filtered -- double
SELECT
	count(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.product_list_filtered,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get product impressions -- web list viewed
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id,
		store_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			store_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				store_id,
				received_at
			FROM
				javascript_production.product_list_viewed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, store_id);

-- get product impressions -- web list viewed -- double
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.product_list_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get product impressions -- ios list filtered
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id,
		params_store_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			params_store_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
					seq.i,
					TRUE) AS product,
				params_store_id,
				received_at
			FROM
				ios_production.product_list_filtered,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, params_store_id);

-- get product impressions -- ios list filtered -- double
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.product_list_filtered,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get product impressions -- ios list viewed
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id,
		params_store_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			params_store_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
					seq.i,
					TRUE) AS product,
				params_store_id,
				received_at
			FROM
				ios_production.product_list_viewed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, params_store_id);

-- get product impressions -- ios list viewed -- double
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.product_list_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get product impressions -- web promotion
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id,
		promotion_store_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
			promotion_store_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				promotion_store_id,
				received_at
			FROM
				javascript_production.promotion_viewed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, promotion_store_id);

-- get product impressions -- web promotion -- double
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			javascript_production.promotion_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get product impressions -- ios promotion
SELECT
	SUM(count)
FROM (
	SELECT
		COUNT(*),
		product_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
		FROM (
			SELECT
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
					seq.i,
					TRUE) AS product,
				received_at
			FROM
				ios_production.promotion_viewed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		product_id);

-- get product impresssions -- ios promotion -- double
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (params_products,
				seq.i,
				TRUE) AS product,
			received_at
		FROM
			ios_production.promotion_viewed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(params_products, TRUE)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get top postal codes
SELECT
	extractCode,
	COUNT(DISTINCT order_id) AS totalOrderCount
FROM (
	SELECT
		SUBSTRING(postal_code, 1, 3) AS extractCode,
		order_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			order_id,
			postal_code
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00')
	WHERE
		product_id = 844)
GROUP BY
	extractCode
ORDER BY
	totalOrderCount DESC
LIMIT 5;

-- get top postal codes -- double
SELECT
	extractCode,
	COUNT(DISTINCT order_id) AS totalOrderCount
FROM (
	SELECT
		SUBSTRING(postal_code, 1, 3) AS extractCode,
		order_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			order_id,
			postal_code
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00')
	WHERE
		product_id = 844)
GROUP BY
	extractCode
ORDER BY
	totalOrderCount DESC
LIMIT 5;

-- sales by age web
SELECT
	age,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id = 844
					AND device = 1)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, age
HAVING
	AGE IS NOT NULL;

-- sales by age ios
SELECT
	age,
	LOWER(anonymous_id),
	COUNT(*)
FROM
	ios_production.identifies
WHERE
	LOWER(anonymous_id)
	IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id = 844
					AND device = 3)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	LOWER(anonymous_id), age
HAVING
	age IS NOT NULL;

-- get sales by campaign backtrack --anonymous ids 
SELECT DISTINCT
	context_campaign_name,
	anonymous_id,
	context_campaign_source
FROM
	javascript_production.pages
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					SUM(JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id, anonymous_id FROM (
						SELECT
							order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i, TRUE) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products, TRUE)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00')
					WHERE
						product_id = 844
					GROUP BY
						anonymous_id, product_id))
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND context_campaign_name IS NOT NULL
GROUP BY
	context_campaign_name, anonymous_id, context_campaign_source;

-- get sales by campaign backtrack --order sales
SELECT
	SUM(JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) * JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE)) AS retail_price,
	JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
	anonymous_id
FROM (
	SELECT
		order_id,
		JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
			seq.i,
			TRUE) AS product,
		store_id,
		received_at,
		anonymous_id
	FROM
		php_production.order_completed,
		seq_0_to_20 AS seq
	WHERE
		seq.i < JSON_ARRAY_LENGTH(products, TRUE)
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00')
WHERE
	product_id = 844
	AND anonymous_id = '123'
GROUP BY
	anonymous_id, product_id;

 
-- get sales by campaign backtrack --transactions
SELECT
	COUNT(*),
	JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id,
	anonymous_id
FROM (
	SELECT
		order_id,
		JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
			seq.i,
			TRUE) AS product,
		store_id,
		received_at,
		anonymous_id
	FROM
		php_production.order_completed,
		seq_0_to_20 AS seq
	WHERE
		seq.i < JSON_ARRAY_LENGTH(products, TRUE)
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00')
WHERE
	product_id = 844
	AND anonymous_id = '1234'
GROUP BY
	anonymous_id, product_id;

-- get sales by campaign backtrack --add to carts
SELECT
	COUNT(*)
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND anonymous_id = '1234'
	AND product_id = 844;

-- get sales by campaign backtrack --page views
SELECT
	COUNT(*)
FROM
	javascript_production.pages
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND context_campaign_name = 'EMAIL_TEST';

-- get sales by campaign -- top click campaigns query
SELECT
	context_campaign_name,
	COUNT(*),
	SPLIT_PART(context_page_path, '/', 3) AS slug
FROM
	javascript_production.pages
WHERE
	context_campaign_name IS NOT NULL
	AND slug = 'test'
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	context_campaign_name,
	slug
ORDER BY
	COUNT(*)
	DESC;

-- get sales by campaign -- orders
SELECT
	JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
	SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales
FROM (
	SELECT
		anonymous_id,
		JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
			seq.i) AS product,
		received_at
	FROM
		php_production.order_completed,
		seq_0_to_20 AS seq
	WHERE
		seq.i < JSON_ARRAY_LENGTH(products)
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
		AND anonymous_id IN( SELECT DISTINCT
				anonymous_id FROM javascript_production.pages
			WHERE
				anonymous_id IN(
					SELECT
						anonymous_id FROM javascript_production.pages
					WHERE
						context_campaign_name = 'test'
						AND received_at BETWEEN '2020-02-14 00:00:00'
						AND '2020-02-15 00:00:00')
					AND received_at BETWEEN '2020-02-14 00:00:00'
					AND '2020-02-15 00:00:00'
					AND context_page_path = '/product/test'))
WHERE
	product_id = 844
GROUP BY
	product_id;

-- get sales by campaign -- add to carts
SELECT
	COUNT(*)
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND anonymous_id IN( SELECT DISTINCT
			anonymous_id FROM javascript_production.pages
		WHERE
			anonymous_id IN(
				SELECT
					anonymous_id FROM javascript_production.pages
				WHERE
					context_campaign_name = 'test'
					AND received_at BETWEEN '2020-02-14 00:00:00'
					AND '2020-02-15 00:00:00'
					AND path = '/product/test'))
	AND product_id = 844;

-- get sales by campaign -- transactions
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales,
		order_id
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'
			AND anonymous_id IN( SELECT DISTINCT
					anonymous_id FROM javascript_production.pages
				WHERE
					anonymous_id IN(
						SELECT
							anonymous_id FROM javascript_production.pages
						WHERE
							context_campaign_name = 'EMAIL_TEST'
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00')
						AND received_at BETWEEN '2020-02-14 00:00:00'
						AND '2020-02-15 00:00:00'
						AND context_page_path = '/product/test'))
	WHERE
		product_id = 844
	GROUP BY
		product_id, order_id);

-- get sales by campaign -- sources 
SELECT DISTINCT
	context_campaign_source
FROM
	javascript_production.pages
WHERE
	context_campaign_name = 'EMAIL_TEST'
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00';

-- sales by gender -- web
SELECT
	gender,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id = 844
					AND device = 1)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, gender
HAVING
	gender IS NOT NULL;

-- sales by gender -- ios
SELECT
	gender,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id = 844
					AND device = 3)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, gender
HAVING
	gender IS NOT NULL;

-- sales by medium -- email
SELECT
	SUM(totalSales) AS emailSales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			cogs,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'
			AND anonymous_id IN(
				SELECT
					anonymous_id FROM (
						SELECT
							path, anonymous_id, context_page_search FROM javascript_production.pages
						WHERE
							received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'
							AND anonymous_id IN(
								SELECT
									anonymous_id FROM javascript_production.pages
								WHERE
									context_campaign_medium = 'email'
									AND received_at BETWEEN '2020-02-14 00:00:00'
									AND '2020-02-15 00:00:00'
								GROUP BY
									anonymous_id))
						WHERE
							path = '/confirmation'
						GROUP BY
							anonymous_id, path))
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
	GROUP BY
		product_id, received_at);

-- sales by medium -- digital
SELECT
	SUM(totalSales) AS digitialSales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			cogs,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'
			AND anonymous_id IN(
				SELECT
					anonymous_id FROM (
						SELECT
							path, anonymous_id, context_page_search FROM javascript_production.pages
						WHERE
							received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'
							AND anonymous_id IN(
								SELECT
									anonymous_id FROM javascript_production.pages
								WHERE
									context_campaign_medium = 'Digital'
									AND received_at BETWEEN '2020-02-14 00:00:00'
									AND '2020-02-15 00:00:00'
								GROUP BY
									anonymous_id))
						WHERE
							path = '/confirmation'
						GROUP BY
							anonymous_id, path))
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
	GROUP BY
		product_id, received_at);

-- sales by medium -- none
SELECT
	SUM(totalSales) AS noneSales
FROM (
	SELECT
		SUM(JSON_EXTRACT_PATH_TEXT(product, 'quantity') * JSON_EXTRACT_PATH_TEXT(product, 'price')) AS totalSales,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		received_at
	FROM (
		SELECT
			cogs,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'
			AND anonymous_id IN(
				SELECT
					anonymous_id FROM (
						SELECT
							path, anonymous_id, context_page_search FROM javascript_production.pages
						WHERE
							received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'
							AND anonymous_id IN(
								SELECT
									anonymous_id FROM javascript_production.pages
								WHERE
									context_campaign_medium IS NULL
									AND received_at BETWEEN '2020-02-14 00:00:00'
									AND '2020-02-15 00:00:00'
								GROUP BY
									anonymous_id))
						WHERE
							path LIKE '%/product/test%'
						GROUP BY
							anonymous_id, path))
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
	GROUP BY
		product_id, received_at);

-- sessions by source
SELECT
	context_campaign_source,
	COUNT(*)
FROM (
	SELECT
		context_campaign_source,
		context_page_path,
		COUNT(*),
		anonymous_id
	FROM
		javascript_production.pages
	WHERE
		received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
		AND context_page_path LIKE '%/product/test%'
	GROUP BY
		context_campaign_source,
		context_page_path,
		anonymous_id)
GROUP BY
	context_campaign_source;

-- total add to carts
SELECT
	COUNT(*) AS totalAddToCart
FROM
	php_production.product_added
WHERE
	received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND product_id = 844
	AND anonymous_id IS NOT NULL;

-- total page views -- web
SELECT
	COUNT(*) AS totalPageViews
FROM
	javascript_production.product_viewed
WHERE
	product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00';

-- total page views -- ios
SELECT
	COUNT(*) AS totalPageViews
FROM
	ios_production.product_viewed
WHERE
	params_product_id = 844
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
	AND params_product_id = 844;

-- get total purchases
SELECT
	COUNT(*)
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- get total quantity -- web
SELECT
	SUM(quantity) AS quantity
FROM (
	SELECT
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id,
		JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity,
		order_id,
		store_id,
		device
	FROM (
		SELECT
			order_id,
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			store_id,
			device,
			received_at
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00'))
WHERE
	product_id = 844;

-- total unique page views -- web
SELECT
	COUNT(*) AS uniqueView
FROM (
	SELECT
		COUNT(*),
		product_id,
		store_id,
		anonymous_id
	FROM
		javascript_production.product_viewed
	WHERE
		product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
	GROUP BY
		product_id,
		store_id,
		anonymous_id);

-- total unique page views -- ios
SELECT
	COUNT(*) AS uniqueView
FROM (
	SELECT
		COUNT(*),
		params_product_id,
		params_store_id,
		anonymous_id
	FROM
		ios_production.product_viewed
	WHERE
		params_product_id = 844
		AND received_at BETWEEN '2020-02-14 00:00:00'
		AND '2020-02-15 00:00:00'
		AND params_product_id = 844
	GROUP BY
		params_product_id,
		params_store_id,
		anonymous_id);

-- total revenue
SELECT
	SUM(revenue) AS revenue
FROM (
	SELECT
		SUM(retail_price * quantity) AS revenue,
		order_id
	FROM (
		SELECT
			JSON_EXTRACT_PATH_TEXT(product, 'quantity', TRUE) AS quantity,
			JSON_EXTRACT_PATH_TEXT(product, 'price', TRUE) AS retail_price,
			order_id,
			JSON_EXTRACT_PATH_TEXT(product, 'productId', TRUE) AS product_id
		FROM (
			SELECT
				order_id,
				JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
					seq.i,
					TRUE) AS product,
				store_id,
				received_at
			FROM
				php_production.order_completed,
				seq_0_to_20 AS seq
			WHERE
				seq.i < JSON_ARRAY_LENGTH(products, TRUE)
				AND received_at BETWEEN '2020-02-14 00:00:00'
				AND '2020-02-15 00:00:00'))
	WHERE
		product_id = 844
	GROUP BY
		order_id);





-- demographics
-- top postal code

SELECT
	extractCode,
	COUNT(DISTINCT order_id) AS totalOrderCount
FROM (
	SELECT
		SUBSTRING(postal_code, 1, 3) AS extractCode,
		order_id,
		JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id
	FROM (
		SELECT
			JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products,
				seq.i) AS product,
			order_id,
			postal_code
		FROM
			php_production.order_completed,
			seq_0_to_20 AS seq
		WHERE
			seq.i < JSON_ARRAY_LENGTH(products)
			AND received_at BETWEEN '2020-02-14 00:00:00'
			AND '2020-02-15 00:00:00')
	WHERE
		product_id IN(844))
GROUP BY
	extractCode
ORDER BY
	totalOrderCount DESC
LIMIT 5;

-- sales by age -- web
SELECT
	age,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, json_extract_path_text(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id IN(844)
					AND device = 1)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, age
HAVING
	age IS NOT NULL;

-- sales by age -- ios
SELECT
	age,
	LOWER(anonymous_id),
	COUNT(*)
FROM
	ios_production.identifies
WHERE
	LOWER(anonymous_id)
	IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id IN(844)
					AND device = 3)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	LOWER(anonymous_id), age
HAVING
	age IS NOT NULL;

-- sales by gender -- web
SELECT
	gender,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id IN(844)
					AND device = 1)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, gender
HAVING
	gender IS NOT NULL;

-- sales by gender -- ios
SELECT
	gender,
	anonymous_id,
	COUNT(*)
FROM
	javascript_production.identifies
WHERE
	anonymous_id IN(
		SELECT
			anonymous_id FROM (
				SELECT
					device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, 'productId') AS product_id, JSON_EXTRACT_PATH_TEXT(product, 'quantity') AS quantity, JSON_EXTRACT_PATH_TEXT(product, 'price') AS retail_price FROM (
						SELECT
							device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT (products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq
						WHERE
							seq.i < JSON_ARRAY_LENGTH(products)
							AND received_at BETWEEN '2020-02-14 00:00:00'
							AND '2020-02-15 00:00:00'))
				WHERE
					product_id IN(844)
					AND device = 3)
	AND received_at BETWEEN '2020-02-14 00:00:00'
	AND '2020-02-15 00:00:00'
GROUP BY
	anonymous_id, gender
HAVING
	gender IS NOT NULL;