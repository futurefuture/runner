<?php

namespace App\Services;

use Auth;
use App\User;
use App\Order;
use App\Address;
use Stripe\Charge;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Http\Request;
use App\Services\StripeService;
use App\Http\Controllers\Controller;

class StripeServiceFake extends StripeService
{
    protected $charge;

    public function __construct()
    {
        Stripe::setApiKey(getenv('STRIPE_SECRET'));

        $this->charge = (object) [
            'id'                     => 'ch_19yLqMByumHgsxlUp6yGe5Fu',
            'object'                 => 'charge',
            'amount'                 => 2000,
            'amount_refunded'        => 0,
            'application'            => null,
            'application_fee'        => null,
            'application_fee_amount' => null,
            'balance_transaction'    => 'txn_18fBQlByumHgsxlUDptFJNhb',
            'billing_details'        => [
                'address' => [
                    'city'        => null,
                    'country'     => 'Canada',
                    'line1'       => null,
                    'line2'       => null,
                    'postal_code' => 'M5V3Y2',
                    'state'       => null
                ],
                'email' => null,
                'name'  => 'Yafan Zhang',
                'phone' => null
            ],
            'captured'        => true,
            'created'         => 1489760054,
            'currency'        => 'cad',
            'customer'        => 'cus_AHsi5delVECmdU',
            'description'     => 'Charge for jenny.rosen@example.com',
            'dispute'         => null,
            'disputed'        => false,
            'failure_code'    => null,
            'failure_message' => null,
            'fraud_details'   => [],
            'invoice'         => null,
            'livemode'        => false,
            'metadata'        => [],
            'on_behalf_of'    => null,
            'order'           => null,
            'outcome'         => [
                'network_status' => 'approved_by_network',
                'reason'         => null,
                'risk_level'     => 'normal',
                'seller_message' => 'Payment complete.',
                'type'           => 'authorized'
            ],
            'paid'                   => true,
            'payment_intent'         => null,
            'payment_method'         => 'card_19xIxCByumHgsxlUcYoS4cHj',
            'payment_method_details' => [
                'card' => [
                    'brand'  => 'visa',
                    'checks' => [
                        'address_line1_check'       => null,
                        'address_postal_code_check' => 'pass',
                        'cvc_check'                 => null
                    ],
                    'country'        => 'US',
                    'exp_month'      => 5,
                    'exp_year'       => 2020,
                    'fingerprint'    => 'RfPBtEVTA4bPeu8S',
                    'funding'        => 'credit',
                    'installments'   => null,
                    'last4'          => '4242',
                    'network'        => 'visa',
                    'three_d_secure' => null,
                    'wallet'         => null
                ],
                'type' => 'card'
            ],
            'receipt_email'  => null,
            'receipt_number' => null,
            'receipt_url'    => 'https://pay.stripe.com/receipts/acct_18e5C8ByumHgsxlU/ch_19yLqMByumHgsxlUp6yGe5Fu/rcpt_EHVSdwMViSL595lT0kM06oFHPXvuaDx',
            'refunded'       => false,
            'refunds'        => [
                'object'   => 'list',
                'data'     => [],
                'has_more' => false,
                'url'      => '/v1/charges/ch_19yLqMByumHgsxlUp6yGe5Fu/refunds'
            ],
            'review'                      => null,
            'shipping'                    => null,
            'source_transfer'             => null,
            'statement_descriptor'        => null,
            'statement_descriptor_suffix' => null,
            'status'                      => 'succeeded',
            'transfer_data'               => null,
            'transfer_group'              => null,
            'source'                      => 'tok_visa'
        ];
    }

    // public function createCustomer($data)
    // {
    //     try {
    //         $customer = Customer::create([
    //             'description' => ucfirst(strtolower($data['firstName'])),
    //             'email'       => strtolower($data['email']),
    //         ]);

    //         return $customer;
    //     } catch (\Stripe\Error\Base $e) {
    //         return response()->json([
    //             'errors'      => [
    //                 'error' => 'There has been an error in registering your account.',
    //             ],
    //         ], 401);
    //     }
    // }

    public function getCustomer(User $user)
    {
        $customer = (object) [
            'id'               => $user->stripe_id,
            'object'           => 'customer',
            'account_balance'  => 0,
            'address'          => null,
            'balance'          => 0,
            'created'          => $user->created_at,
            'currency'         => null,
            'default_source'   => 'test_card',
            'delinquent'       => false,
            'description'      => 'Test',
            'discount'         => null,
            'email'            => $user->email,
            'invoice_prefix'   => '698C304',
            'invoice_settings' => [
                'custom_fields'          => null,
                'default_payment_method' => null,
                'footer'                 => null
            ],
            'livemode'          => false,
            'metadata'          => [],
            'name'              => $user->first_name . ' ' . $user->last_name,
            'phone'             => $user->phone_number,
            'preferred_locales' => [],
            'shipping'          => null,
            'sources'           => collect([
                'object' => 'list',
                'data'   => [
                    [
                        'id'                  => 'test_card',
                        'object'              => 'card',
                        'address_city'        => null,
                        'address_country'     => null,
                        'address_line1'       => null,
                        'address_line1_check' => null,
                        'address_line2'       => null,
                        'address_state'       => null,
                        'address_zip'         => '42424',
                        'address_zip_check'   => 'pass',
                        'brand'               => 'Visa',
                        'country'             => 'US',
                        'customer'            => $user->stripe_id,
                        'cvc_check'           => 'pass',
                        'dynamic_last4'       => null,
                        'exp_month'           => 4,
                        'exp_year'            => 2024,
                        'fingerprint'         => 'RfPBtEVTA4bPeu8S',
                        'funding'             => 'credit',
                        'last4'               => '4242',
                        'metadata'            => [],
                        'name'                => $user->first_name . ' ' . $user->last_name,
                        'tokenization_method' => null
                    ]
                ],
                'has_more'    => false,
                'total_count' => 1,
                'url'         => '/v1/customers/test_customer/sources'
            ]),
            'subscriptions' => [
                'object'      => 'list',
                'data'        => [],
                'has_more'    => false,
                'total_count' => 0,
                'url'         => '/v1/customers/test_customer/subscriptions'
            ],
            'tax_exempt' => 'none',
            'tax_ids'    => [
                'object'      => 'list',
                'data'        => [],
                'has_more'    => false,
                'total_count' => 0,
                'url'         => '/v1/customers/' . $user->stripe_id . '/tax_ids'
            ],
            'tax_info'              => null,
            'tax_info_verification' => null
        ];

        return $customer;
    }

    public function createSource(User $user, $token)
    {
        $card = [
            'id'                  => 'card_1FjTCMByumHgsxlUwFP0H6uu',
            'object'              => 'card',
            'address_city'        => null,
            'address_country'     => null,
            'address_line1'       => null,
            'address_line1_check' => null,
            'address_line2'       => null,
            'address_state'       => null,
            'address_zip'         => null,
            'address_zip_check'   => null,
            'brand'               => 'Visa',
            'country'             => 'US',
            'customer'            => 'cus_GFzAFFM46a6ewl',
            'cvc_check'           => null,
            'dynamic_last4'       => null,
            'exp_month'           => 8,
            'exp_year'            => 2020,
            'fingerprint'         => 'RfPBtEVTA4bPeu8S',
            'funding'             => 'credit',
            'last4'               => '4242',
            'metadata'            => [],
            'name'                => null,
            'tokenization_method' => null
        ];

        return $card;
    }

    public function deleteSource(User $user, $sourceId)
    {
        $card = [
            'id'                  => 'card_1FjTCMByumHgsxlUwFP0H6uu',
            'object'              => 'card',
            'address_city'        => null,
            'address_country'     => null,
            'address_line1'       => null,
            'address_line1_check' => null,
            'address_line2'       => null,
            'address_state'       => null,
            'address_zip'         => null,
            'address_zip_check'   => null,
            'brand'               => 'Visa',
            'country'             => 'US',
            'customer'            => 'cus_GFzAFFM46a6ewl',
            'cvc_check'           => null,
            'dynamic_last4'       => null,
            'exp_month'           => 8,
            'exp_year'            => 2020,
            'fingerprint'         => 'RfPBtEVTA4bPeu8S',
            'funding'             => 'credit',
            'last4'               => '4242',
            'metadata'            => [],
            'name'                => null,
            'tokenization_method' => null
        ];

        return $card;
    }

    public function updateDefaultSource(User $user, $sourceId)
    {
        $customer = (object) [
            'id'               => $user->stripe_id,
            'object'           => 'customer',
            'account_balance'  => 0,
            'address'          => null,
            'balance'          => 0,
            'created'          => $user->created_at,
            'currency'         => null,
            'default_source'   => 'test_card',
            'delinquent'       => false,
            'description'      => 'Test',
            'discount'         => null,
            'email'            => $user->email,
            'invoice_prefix'   => '698C304',
            'invoice_settings' => [
                'custom_fields'          => null,
                'default_payment_method' => null,
                'footer'                 => null
            ],
            'livemode'          => false,
            'metadata'          => [],
            'name'              => $user->first_name . ' ' . $user->last_name,
            'phone'             => $user->phone_number,
            'preferred_locales' => [],
            'shipping'          => null,
            'sources'           => collect([
                'object' => 'list',
                'data'   => [
                    [
                        'id'                  => 'test_card',
                        'object'              => 'card',
                        'address_city'        => null,
                        'address_country'     => null,
                        'address_line1'       => null,
                        'address_line1_check' => null,
                        'address_line2'       => null,
                        'address_state'       => null,
                        'address_zip'         => '42424',
                        'address_zip_check'   => 'pass',
                        'brand'               => 'Visa',
                        'country'             => 'US',
                        'customer'            => $user->stripe_id,
                        'cvc_check'           => 'pass',
                        'dynamic_last4'       => null,
                        'exp_month'           => 4,
                        'exp_year'            => 2024,
                        'fingerprint'         => 'RfPBtEVTA4bPeu8S',
                        'funding'             => 'credit',
                        'last4'               => '4242',
                        'metadata'            => [],
                        'name'                => $user->first_name . ' ' . $user->last_name,
                        'tokenization_method' => null
                    ]
                ],
                'has_more'    => false,
                'total_count' => 1,
                'url'         => '/v1/customers/test_customer/sources'
            ]),
            'subscriptions' => [
                'object'      => 'list',
                'data'        => [],
                'has_more'    => false,
                'total_count' => 0,
                'url'         => '/v1/customers/test_customer/subscriptions'
            ],
            'tax_exempt' => 'none',
            'tax_ids'    => [
                'object'      => 'list',
                'data'        => [],
                'has_more'    => false,
                'total_count' => 0,
                'url'         => '/v1/customers/' . $user->stripe_id . '/tax_ids'
            ],
            'tax_info'              => null,
            'tax_info_verification' => null
        ];

        return $customer;
    }

    public function createCharge(Order $order, User $user, array $data)
    {
        return $this->charge;
    }

    public function getCharge(Order $order)
    {
        return $this->charge;
    }

    public function captureCharge(Order $order)
    {
        return $this->charge;
    }
}
