<?php

namespace App\Services;

use App\User;
use App\Order;
use App\Address;
use Stripe\Charge;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Customer;

class StripeService
{
    public function __construct()
    {
        Stripe::setApiKey(getenv('STRIPE_SECRET'));
    }

    // public function createCustomer($data)
    // {
    //     try {
    //         $customer = Customer::create([
    //             'description' => ucfirst(strtolower($data['firstName'])),
    //             'email'       => strtolower($data['email']),
    //         ]);

    //         return $customer;
    //     } catch (\Stripe\Error\Base $e) {
    //         return response()->json([
    //             'errors'      => [
    //                 'error' => 'There has been an error in registering your account.',
    //             ],
    //         ], 401);
    //     }
    // }

    public function getCustomer(User $user)
    {
        $customer = Customer::retrieve($user->stripe_id);

        return $customer;
    }

    public function createSource(User $user, $token)
    {
        $card = Customer::createSource($user->stripe_id, [
            'source' => $token
        ]);

        return $card;
    }

    public function deleteSource(User $user, $sourceId)
    {
        $card = Customer::deleteSource($user->stripe_id, $sourceId);

        return $card;
    }

    public function updateDefaultSource(User $user, $sourceId)
    {
        $customer = Customer::update($user->stripe_id, [
            'default_source' => $sourceId
        ]);

        return $customer;
    }

    public function createCharge(Order $order, User $user, array $data)
    {
        $address = Address::where('user_id', $user->id)
            ->where('selected', 1)
            ->first();

        if (($user->email == 'jake@wearefuturefuture.com' || $user->email == 'jarek@wearefuturefuture.com' || $user->email == 'dummy@getrunner.io' && env('APP_ENV') == 'production')) {
            $chargeAmount = 51;
        } else {
            if (! isset($data['chargeAmount'])) {
                $chargeAmount = $order->total;
            } else {
                $chargeAmount = $data['chargeAmount'];
            }
        }

        $charge = Charge::create([
            'amount'            => $chargeAmount,
            'currency'          => 'cad',
            'source'            => isset($data['stripeToken']) ? $data['stripeToken'] : null,
            'customer'          => ! isset($data['stripeToken']) ? $user->stripe_id : null,
            'description'       => 'Order',
            'metadata'          => [
                'customer'     => $user->first_name . ' ' . $user->last_name,
                'email'        => $user->email,
                'phone'        => $user->phone_number,
                'address'      => $address->formatted_address,
                'instructions' => $address->instructions,
                'time'         => $data['orderDateTime'] ?? null,
                'total'        => number_format($chargeAmount / 100, 2, '.', ','),
                'tax'          => number_format($order->display_tax_total / 100, 2, '.', ','),
                'delivery'     => number_format($order->delivery / 100, 2, '.', ','),
                'discount'     => number_format($order->discount / 100, 2, '.', ','),
                'tip'          => number_format($order->tip / 100, 2, '.', ','),
                'subtotal'     => number_format($order->subtotal / 100, 2, '.', ','),
                'appVersion'   => $data['appVersion'] ?? null,
            ],
            'capture'     => false,
        ]);

        return $charge;
    }

    public function getCharge(Order $order)
    {
        $charge = Charge::retrieve($order->charge);

        return $charge;
    }

    public function captureCharge(Order $order)
    {
        $charge = Charge::retrieve($order->charge);

        // check that charge has not been captured already
        if ($charge['captured'] === false) {
            $charge->capture();
        }

        return $charge;
    }

    public function createRefund(Order $order, int $amount)
    {
        $refund = Refund::create($order->charge, [
            'amount' => $amount
        ]);

        return $refund;
    }
}
