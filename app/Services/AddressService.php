<?php

namespace App\Services;

use App\Address;
use Validator;

class AddressService
{
    public function create($address)
    {
        $address = Address::create($address);

        return $address;
    }
}
