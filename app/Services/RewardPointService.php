<?php

namespace App\Services;

use App\RewardPoint;

class RewardPointService
{
    public function addPoints($points, $type, $values, $userId)
    {
        RewardPoint::create([
            'user_id' => $userId,
            'value'   => $points,
            'type'    => $type,
            'details' => json_encode($values),
        ]);
    }
}
