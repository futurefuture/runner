<?php

namespace App\Services;

use App\Partner;
use App\Product;
use App\Store;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ProductRedShiftAnalyticsService
{
    protected $partnerId;
    protected $partner;
    protected $productInfo;
    protected $productSKU;
    protected $product;
    protected $slug;
    protected $slugImp;
    protected $productTitle;
    protected $productCategory;
    protected $broadCategory;
    protected $subCategory;
    protected $totalProductClick           = 0;
    protected $totalUniqueClick            = 0;
    protected $totalProductAddToCart       = 0;
    protected $totalAddToCartDetail        = 0;
    protected $totalBuyToDetail            = 0;
    protected $totalProductUniquePurchases = 0;
    protected $totalQuantity               = 0;
    protected $totalRevenue                = 0;
    protected $totalShares                 = 0;
    protected $totalFavourites             = 0;
    protected $totalProductRevenue         = 0;
    protected $totalProductQuantitySold    = 0;
    protected $totalProductPurchases       = 0;
    protected $storearr                    = [];
    public $phpdatabase                    = 0;
    public $iosdatabase                    = 0;
    public $javascriptdb                   = 0;

    public function __construct($partnerId, $productId)
    {
        $this->partner         = Partner::find($partnerId);
        $this->product         = $productId;
        $this->productInfo     = Product::find($this->product);
        $this->productSKU      = $this->productInfo->sku;
        $this->slug            = $this->productInfo->slug;
        $this->productTitle    = $this->productInfo->title;
        $this->productCategory = Product::where('id', '=', $this->product)
            ->get()
            ->pluck('category_id')
            ->toArray();
        $this->getBroadCategoryAndSubCategory();
        $this->totalProductClick     = 0;
        $this->totalProductAddToCart = 0;
        $this->phpdatabase           = 'php_production';
        $this->iosdatabase           = 'ios_production';
        $this->javascriptdb          = 'javascript_production';
    }

    public function getProductAnalytics($startDate, $endDate)
    {
        $to   = Carbon::createFromFormat('Y-m-d', $startDate);
        $from = Carbon::createFromFormat('Y-m-d', $endDate);
        // Get Product Clic for both Ios and Mobile
        $diffInDays          = $to->diffInDays($from);
        $broadRank           = $this->getRank($startDate, $endDate, $this->broadCategory);
        $subCategoryRank     = $this->getRank($startDate, $endDate, $this->subCategory);
        $webPageViews        = $this->getWebPageViews($to, $from);
        $analyticsByMedium   = $this->getSalesByMedium($startDate, $endDate);
        $analyticsByCampaign = $this->getSalesByCampaign($startDate, $endDate);
        $analyticsBySource   = $this->getSessionsBySource($startDate, $endDate);

        $productOverallAnalytics = [
            'runnerAnalytics'  => [
                'totalRevenue'          => $this->totalProductRevenue,
                'broadCategory'         => $broadRank,
                'subCategory'           => $subCategoryRank,
                'totalPurchases'        => $this->totalProductPurchases,
                'totalQuantity'         => $this->totalProductQuantitySold,
                'totalAveragePurchases' => ceil($this->totalProductQuantitySold / $diffInDays),
            ],
            'googleAnalytics' => [
                'totalPageViews'        => $this->totalProductClick,
                'totalUniquePageViews'  => $this->totalUniqueClick,
                'totalAddToCarts'       => $this->totalProductAddToCart,
                'totalCartToDetails'    => $this->totalAddToCartDetail,
                'totalBuyToDetails'     => $this->totalBuyToDetail,
                'totalShares'           => $this->totalShares,
                'totalFavourites'       => $this->totalFavourites,
            ],
            'storeFronts'              => $webPageViews,
            'medium'                   => $analyticsByMedium,
            'campaign'                 => $analyticsByCampaign,
            'traffic'                  => $analyticsBySource,
        ];

        return response()->json([
            'data' => [
                'type'       => 'products',
                'id'         => (string) $this->product,
                'SKU'        => $this->productSKU,
                'title'      => $this->productTitle,
                'attributes' => $productOverallAnalytics,
            ],
        ]);
    }

    public function getWebPageViews($startDate, $endDate)
    {
        $productAnalytics       = [];
        $productJavascriptClick = $this->getProductClick($startDate, $endDate);

        $productIosData   = $this->getProductIosClick($startDate, $endDate);
        $productPurchases = $this->getPurchases($startDate, $endDate);
        $productQuantity  = $this->getQuantity($startDate, $endDate);
        $productRevenue   = $this->getRevenue($startDate, $endDate);

        $store = Store::where('is_active', 1)->pluck('id')->toArray();

        $storefront = array_intersect(array_unique($this->storearr), $store);

        foreach ($storefront as $key => $val) {
            $webStore                    = [];
            $iosStore                    = [];
            $storeInfo                   = Store::find($val);
            $webStore['pageViews']       = $productJavascriptClick[$val]['pageView'] ?? 0;
            $webStore['uniquePageViews'] = $productJavascriptClick[$val]['uniquePageViews'] ?? 0;
            $webStore['addToCart']       = $productJavascriptClick[$val]['addToCart'] ?? 0;

            if (isset($productPurchases)) {
                foreach ($productPurchases as $purchases) {
                    if ($purchases->store_id == $val && $purchases->device == 1) {
                        $webStore['purchases'] = $purchases->count;
                    }
                }
            }

            if (isset($productQuantity)) {
                foreach ($productQuantity as $quantity) {
                    if ($quantity->store_id == $val && $quantity->device == 1) {
                        $webStore['quantitySold'] = $quantity->quantity;
                    }
                }
            }

            if (isset($productRevenue)) {
                foreach ($productRevenue as $rev) {
                    if ($rev->store_id == $val && $rev->device == 1) {
                        $webStore['revenue'] = $rev->revenue;
                    }
                }
            }

            $webStore['cartToDetailRate'] = $webStore['pageViews'] > 0 && $webStore['addToCart'] > 0 ? $webStore['pageViews'] / $webStore['addToCart'] : 0;

            if (isset($webStore['uniquePageViews']) && isset($webStore['purchases'])) {
                $webStore['buyToDetailRate'] = $webStore['uniquePageViews'] > 0 && $webStore['purchases'] > 0 ? $webStore['uniquePageViews'] / $webStore['purchases'] : 0;
            } else {
                $webStore['buyToDetailRate'] = 0;
            }

            $webStore['shares']     = 10;
            $webStore['favourites'] = $productJavascriptClick[$val]['favuorite'] ?? 0;

            $iosStore['pageViews']        = $productIosData[$val]['pageView'] ?? 0;
            $iosStore['uniquePageViews']  = $productIosData[$val]['uniquePageViews'] ?? 0;
            $iosStore['addToCart']        = $productIosData[$val]['addToCart'] ?? 0;
            $iosStore['cartToDetailRate'] = $iosStore['pageViews'] > 0 && $iosStore['addToCart'] > 0 ? $iosStore['pageViews'] / $iosStore['addToCart'] : 0;
            if (isset($iosStore['uniquePageViews']) && isset($webStore['purchases'])) {
                $iosStore['buyToDetailRate'] = $iosStore['uniquePageViews'] > 0 && $webStore['purchases'] > 0 ? $iosStore['uniquePageViews'] / $webStore['purchases'] : 0;
            } else {
                $iosStore['buyToDetailRate'] = 0;
            }
            $iosStore['shares'] = 10;

            // if (isset($productJavascriptClick[$val]['device'])) {

            //     $iosStore['purchases']        =   $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['purchases'] : 0;
            //     $iosStore['quantitySold']     =  $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['quantitySold'] : 0;
            //     $iosStore['revenue']          =  $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['revenue'] : 0;
            // }
            if (isset($productPurchases)) {
                foreach ($productPurchases as $purchases) {
                    if ($purchases->store_id == $val && $purchases->device == 3) {
                        $iosStore['purchases'] = $purchases->count;
                    }
                }
            }

            if (isset($productQuantity)) {
                foreach ($productQuantity as $quantity) {
                    if ($quantity->store_id == $val && $quantity->device == 3) {
                        $iosStore['quantitySold'] = $quantity->quantity;
                    }
                }
            }

            if (isset($productRevenue)) {
                foreach ($productRevenue as $rev) {
                    if ($rev->store_id == $val && $rev->device == 3) {
                        $iosStore['revenue'] = $rev->revenue;
                    }
                }
            }

            $this->totalProductClick += $webStore['pageViews'] + $iosStore['pageViews'];
            $this->totalUniqueClick += $webStore['uniquePageViews'] + $iosStore['uniquePageViews'];
            $this->totalProductAddToCart += $webStore['addToCart'] + $iosStore['addToCart'];
            $this->totalAddToCartDetail += $webStore['cartToDetailRate'] + $iosStore['cartToDetailRate'];
            $this->totalBuyToDetail += $webStore['buyToDetailRate'] + $iosStore['buyToDetailRate'];
            $this->totalShares += $webStore['shares'] + $iosStore['shares'];
            $this->totalFavourites += $webStore['favourites'];

            if (isset($webStore['revenue']) || isset($iosStore['revenue'])) {
                $this->totalProductRevenue += $webStore['revenue'] ?? 0;
                $this->totalProductRevenue += $iosStore['revenue'] ?? 0;
            }
            if (isset($webStore['purchases']) || isset($iosStore['purchases'])) {
                $this->totalProductPurchases += $webStore['purchases'] ?? 0;
                $this->totalProductPurchases += $iosStore['purchases'] ?? 0;
            }
            if (isset($webStore['quantitySold']) || isset($iosStore['quantitySold'])) {
                $this->totalProductQuantitySold += $webStore['quantitySold'] ?? 0;
                $this->totalProductQuantitySold += $iosStore['quantitySold'] ?? 0;
            }

            $productAnalytics[$val] = [
                'store'  => [
                    'title' => $storeInfo->title,
                    'logo'  => $storeInfo->options['storeLogo'],
                ],
                'pageViews' => [
                    'web' => $webStore['pageViews'] ?? 0,
                    'ios' => $iosStore['pageViews'] ?? 0,
                ],
                'uniquePageViews' => [
                    'web'   => $webStore['uniquePageViews'] ?? 0,
                    'ios'   => $iosStore['uniquePageViews'] ?? 0,
                ],
                'addToCarts' => [
                    'web' => $webStore['addToCart'] ?? 0,
                    'ios' => $iosStore['addToCart'] ?? 0,
                ],
                'cartToDetailRate' => [
                    'web'  => $webStore['cartToDetailRate'] ?? 0,
                    'ios'  => $iosStore['cartToDetailRate'] ?? 0,
                ],
                'buyToDetailRate'  => [
                    'web'  => $webStore['buyToDetailRate'] ?? 0,
                    'ios'  => $iosStore['buyToDetailRate'] ?? 0,
                ],
                'shares' => [
                    'web'  => $webStore['shares'] ?? 0,
                    'ios'  => $iosStore['shares'] ?? 0,
                ],
                'favourites' => [
                    'web'  => $webStore['favourites'] ?? 0,
                    'ios'  => $iosStore['favourites'] ?? 0,
                ],
                'purchases' => [
                    'web'  => $webStore['purchases'] ?? 0,
                    'ios'  => $iosStore['purchases'] ?? 0,
                ],
                'quantitySold' => [
                    'web'  => $webStore['quantitySold'] ?? 0,
                    'ios'  => $iosStore['quantitySold'] ?? 0,
                ],
                'revenue' => [
                    'web'  => $webStore['revenue'] ?? 0,
                    'ios'  => $iosStore['revenue'] ?? 0,
                ],
            ];
        }

        return $productAnalytics;
    }

    protected function getProductClick($startDate, $endDate)
    {
        $query = 'SELECT count(*),product_id,store_id FROM ' . $this->javascriptdb . '.product_viewed where product_id =' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id =' . $this->product . ' group by product_id,store_id';

        $product_list_clicked_frontend = DB::connection('pgsql')->select($query);

        $javascriptClick = [];
        foreach ($product_list_clicked_frontend as $row) {
            $this->storearr[]                = $row->store_id;
            $javascriptClick[$row->store_id] = [
                'pageView' => $row->count,
            ];
        }

        return $this->getProductUniqueClick($startDate, $endDate, $javascriptClick);
    }

    protected function getProductIosClick($startDate, $endDate)
    {
        $query = 'SELECT count(*),params_product_id,params_store_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id =' . $this->product . ' and date(timestamp) between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and params_product_id =' . $this->product . ' group by params_product_id,params_store_id';

        $product_list_clicked_ios = DB::connection('pgsql')->select($query);

        $iosClick = [];
        foreach ($product_list_clicked_ios as $row) {
            $this->storearr[]                = $row->params_store_id;
            $iosClick[$row->params_store_id] = [
                'pageView' => $row->count,
            ];
        }

        return $this->getProductUniqueIosClick($startDate, $endDate, $iosClick);
    }

    protected function getProductUniqueClick($startDate, $endDate, $javascriptClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_frontend = DB::connection('pgsql')->select('Select count(*) as uniqueView,store_id from(SELECT count(*),product_id,store_id,anonymous_id FROM ' . $this->javascriptdb . '.product_viewed where product_id = ' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id =' . $this->product . 'group by product_id,store_id,anonymous_id ) group by store_id');

        foreach ($product_unique_clicked_frontend as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $javascriptClick[$arr->store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        return $this->getAddToCart($startDate, $endDate, $javascriptClick);
    }

    protected function getProductUniqueIosClick($startDate, $endDate, $iosClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_ios = DB::connection('pgsql')->select('Select count(*) as uniqueView,params_store_id from(SELECT count(*),params_product_id,params_store_id,anonymous_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id = ' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and params_product_id =' . $this->product . 'group by params_product_id,params_store_id,anonymous_id ) group by params_store_id');

        foreach ($product_unique_clicked_ios as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->params_store_id;
                if ($arr->params_store_id == $key) {
                    $iosClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $iosClick[$arr->params_store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        //return $this->getAddToCartIos($startDate, $endDate, $iosClick);
    }

    protected function getAddToCart($startDate, $endDate, $javascriptClick)
    {
        $addtoCartQuery = 'SELECT count(*),product_id,store_id FROM ' . $this->phpdatabase . '.product_added where product_id =' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and  product_id = ' . $this->product . '    group by product_id,store_id';

        // $addtoCartQuery = "SELECT anonymous_id,product_id,store_id FROM " . $this->phpdatabase . ".product_added where product_id =" . $this->product . " and timestamp::date between "  . "'" . $startDate . "'" . " and "  . "'" . $endDate . "'" . " group by product_id,store_id";

        // TODO once IOS table is ready add that value into it
        $productAddToCart = DB::connection('pgsql')->select('SELECT count(*),product_id,store_id FROM ' . $this->phpdatabase . '.product_added where product_id =' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,store_id');

        foreach ($productAddToCart as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['addToCart'] = $arr->count;
                } else {
                    $javascriptClick[$arr->store_id]['addToCart'] = $arr->count;
                }
            }
        }

        return $this->getFavourite($startDate, $endDate, $javascriptClick);
    }

    protected function getAddToCartIos($startDate, $endDate, $iosClick)
    {
        $query = 'SELECT count(*),params_product_id,params_store_id FROM ' . $this->iosdatabase . '.product_added where params_product_id =' . $this->product . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and  params_product_id = ' . $this->product . '    group by params_product_id,params_store_id';

        $productAddToCartIOS = DB::connection('pgsql')->select($query);

        foreach ($productAddToCartIOS as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->params_store_id;
                if ($arr->params_store_id == $key) {
                    $iosClick[$key]['addToCart'] = $arr->count;
                } else {
                    $iosClick[$arr->params_store_id]['addToCart'] = $arr->count;
                }
            }
        }

        return $iosClick;
    }

    protected function getPurchases($startDate, $endDate)
    {
        $query = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,timestamp::date FROM  " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $productPurchases = DB::connection('pgsql')->select($query);

        foreach ($productPurchases as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $productPurchases;
    }

    protected function getQuantity($startDate, $endDate)
    {
        // TODO once IOS table is ready add that value into it
        $query = "SELECT count(*),product_id,SUM(quantity) as quantity,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,timestamp FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $quantitySold = DB::connection('pgsql')->select($query);

        foreach ($quantitySold as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $quantitySold;
    }

    protected function getFavourite($startDate, $endDate, $javascriptClick)
    {
        $query = 'Select count(*),product_id,store_id from ' . $this->javascriptdb . '.product_favourited where product_id = ' . $this->product . ' group by product_id,store_id';

        $favourite = DB::connection('pgsql')->select($query);

        foreach ($favourite as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['favuorite'] = $arr->count;
                } else {
                    $javascriptClick[$arr->store_id]['favuorite'] = $arr->count;
                }
            }
        }

        return $javascriptClick;
    }

    protected function getRevenue($startDate, $endDate)
    {
        $query = "SELECT count(*),product_id,SUM(quantity*retail_price) as revenue ,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,device,timestamp FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $revenue = DB::connection('pgsql')->select($query);

        foreach ($revenue as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return  $revenue;
    }

    protected function getRank($startDate, $endDate, $categoryId)
    {
        // TODO once there is table on rds
        $query = 'SELECT product,rank from(Select  product,totRevenue,@curRank := @curRank + 1 AS rank from(SELECT product_id as product,ROUND(SUM(quantity*retail_price)/100,2) as totRevenue FROM order_items WHERE created_at between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 on e1.id = e.parent_id WHERE ' . $categoryId . ' IN(r.parent_id,e.parent_id,e1.parent_id) UNION SELECT id FROM categories WHERE id =' . $categoryId . ') group by product_id order by totRevenue desc) as test,(SELECT @curRank := 0) r ) as rank where product=' . $this->product;

        $rank = DB::select($query);

        return  $rank ? $rank[0]->rank : 0;
    }

    protected function getBroadCategoryAndSubCategory()
    {
        // TODO once there is table on rds
        $cat = DB::select('select  c1.id as category_id,c1.title,c1.parent_id as subCategory ,c2.parent_id as broadCategory from   categories c1
        left join categories  c2 on c2.id = c1.parent_id 
        left join  categories c3 on c3.id = c2.parent_id where c1.id =' . $this->productCategory[0]);

        $this->broadCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->subCategory : $cat[0]->broadCategory;
        $this->subCategory   = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->category_id : $cat[0]->subCategory;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param string $startDate
     * @param string $endId
     * @return array
     */
    protected function getSalesByMedium($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $emailQuery = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'email' and timestamp between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp )';

        $salesByEmail = DB::connection('pgsql')->select($emailQuery);

        $digitialQuery = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'Digital' and timestamp between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp )';

        $noneQuery = "select SUM(totalSales) as noneSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . '.pages where context_campaign_medium is null  and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " group by anonymous_id)) where path like '%/product/" . $this->slug . "%' group by anonymous_id,path)) where product_id = " . $this->product . ' and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp )';

        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);
        $salesByNone     = DB::connection('pgsql')->select($noneQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
            'none'     => $salesByNone[0]->nonesales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getSalesByCampaign($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $topClickCampaignQuery = "select context_campaign_name,count(*),split_part(context_page_path, '/', 3) as slug from javascript_production.pages  where context_campaign_name is not null and slug in(" . $this->slugImp . ') and timestamp::date  between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by context_campaign_name,slug order by count(*) desc ';

        $topCampaignName        = DB::connection('pgsql')->select($topClickCampaignQuery);
        $formattedCampaignSales = [];

        foreach ($topCampaignName as $campaign) {
            // get orders  by each campaign name
            $orderQuery = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select DISTINCT anonymous_id from  ' . $this->javascriptdb . '.pages where anonymous_id in (select anonymous_id from ' . $this->javascriptdb . '.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') and  timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " and path = '/confirmation')) where product_id = " . $this->product . ' and timestamp between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp )';

            $getSalesByCampaign     = DB::connection('pgsql')->select($orderQuery);
            $campaignSales['name']  = $campaign->context_campaign_name;
            $campaignSales['sales'] = $getSalesByCampaign[0]->emailsales ?? 0;

            array_push($formattedCampaignSales, $campaignSales);
        }

        return $formattedCampaignSales;
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param string $startDate
     * @param string $endDate
     */
    protected function getSessionsBySource($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $query = 'select context_campaign_source,context_page_path,count(*) from ' . $this->javascriptdb . '.pages where timestamp::date  between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " and context_page_path like '%/product/" . $this->slug . "%' group by context_campaign_source,context_page_path";

        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                if ($medium->context_campaign_source == null) {
                    $salesSource['none'] = $medium->count;
                } else {
                    $salesSource[$medium->context_campaign_source] = $medium->count;
                }
            }

            return $salesSource;
        } else {
            return;
        }
    }
}
