<?php

namespace App\Services;

use Analytics;
use App\Order;
use App\OrderItem;
use App\Partner;
use App\Product;
use Carbon\Carbon;
use DB;
use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class PartnerOverviewService
{
    protected $partnerId;
    public $totalRevenue   = 0;
    public $totalOrders    = 0;
    public $totalCustomers = 0;
    public $diffInDays     = 0;
    public $topProducts    = [];
    public $ids            = [];
    public $orderByDevice  = [];
    public $gaViewId;

    public function __construct($partnerId, $filter)
    {
        $this->getservicekey = base_path('service-account-credentials.json');
        $this->cloud         = new ServiceBuilder([
            'keyFilePath'                   => $this->getservicekey,
        ]);

        $to               = Carbon::createFromFormat('Y-m-d', $filter->all()['startDate']);
        $from             = Carbon::createFromFormat('Y-m-d', $filter->all()['endDate']);
        $this->diffInDays = $to->diffInDays($from);
        $this->bigQuery   = $this->cloud->bigQuery();
        $this->partner    = Partner::find($partnerId);
        $this->store      = $this->partner->store_id;
        $this->gaViewId   = $this->partner->ga_view_id;

        $this->ids = DB::table('partner_product')->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id');

        $this->getTotalSales($filter);
        $this->getTotalCustomers($filter);
        $this->topProducts   = $this->getTopProducts($filter);
        $this->orderByDevice = $this->getOrdersByDevice($filter);
    }

    /**
     * Returns overall sales, total orders for given period.
     * @param string startDate,endDate
     * @return void
     */
    protected function getTotalSales($filter)
    {
        $orderItems = OrderItem::filter($filter)->select(DB::raw('SUM(quantity*retail_price) as totalRevenue'), DB::raw('COUNT(*) as totalOrders'))->whereIn('product_id', $this->ids)->get();

        $this->totalRevenue = $orderItems[0]->totalRevenue;
        $this->totalOrders  = $orderItems[0]->totalOrders;
    }

    /**
     * Returns total customers.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getTotalCustomers($filter)
    {
        $users = OrderItem::filter($filter)
            ->select('user_id')
            ->whereIn('product_id', $this->ids)
            ->groupBy('user_id')
            ->get()->count();

        $this->totalCustomers = $users;
    }

    /**
     * Returns total customers.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getTopProducts($filter)
    {
        $products = OrderItem::filter($filter)
            ->select('product_id', DB::raw('SUM(quantity*retail_price) as totalRevenue'))
            ->whereIn('product_id', $this->ids)
            ->groupBy('product_id')
            ->orderBy('totalRevenue', 'desc')
            ->get()->take(5);
        $topProducts = [];
        foreach ($products as $product) {
            $newProduct          = Product::find($product->product_id);
            $topProduct['title'] = $newProduct->title;
            $topProduct['price'] = $product->totalRevenue;
            array_push($topProducts, $topProduct);
        }

        return $topProducts;
    }

    /**
     * Returns all analytics data for transactions module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getTransactions($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        // set google analytics view

        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions, ga:transactions, ga:productAddsToCart, ga:productCheckouts',
            ]
        );
        $analyticsData                       = $analyticsData->rows;
        $conversionRate['sessions']          = $analyticsData[0][0];
        $conversionRate['transactions']      = $analyticsData[0][1];
        $conversionRate['productAddsToCart'] = $analyticsData[0][2];
        $conversionRate['productCheckouts']  = $analyticsData[0][3];

        return $conversionRate;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getSalesByMedium($startDate, $endDate)
    {
        $startDate         = Carbon::parse($startDate);
        $endDate           = Carbon::parse($endDate);
        $analyticsByMedium = [];

        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:itemRevenue',
            [
                'metrics'     => 'ga:itemRevenue',
                'dimensions'  => 'ga:medium',
                'sort'        => '-ga:itemRevenue',
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getSalesByCampaign($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:itemRevenue',
            [
                'metrics'     => 'ga:itemRevenue',
                'dimensions'  => 'ga:campaign',
                'sort'        => '-ga:itemRevenue',
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getSessionsBySource($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics'     => 'ga:sessions',
                'dimensions'  => 'ga:source',
                'sort'        => '-ga:sessions',
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data order by device.
     * @param OrderItem $filter
     * @return void
     */
    public function getOrdersByDevice($filter)
    {
        $orderIds = OrderItem::filter($filter)
            ->whereIn('product_id', $this->ids)
            ->distinct('order_id')
            ->pluck('order_id');

        $iosCount        = 0;
        $webDesktopCount = 0;

        foreach ($orderIds as $o) {
            $device = Order::find($o)->device;

            if ($device == 3) {
                $iosCount++;
            } else {
                $webDesktopCount++;
            }
        }
        $orderByDevice = [];

        return $orderByDevice = [
            'ios'        => $iosCount,
            'webDesktop' => $webDesktopCount,
        ];
    }

    /**
     * Returns all analytics data for top pages module.
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getTopPages($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:pageviews',
            [
                'metrics'     => 'ga:pageviews',
                'dimensions'  => 'ga:pagePath',
                'sort'        => '-ga:pageviews',
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data for page views module.
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getUsers($startDate, $endDate)
    {
        $totalUsers = 0;
        $startDate  = Carbon::parse($startDate);
        $endDate    = Carbon::parse($endDate);

        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:users',
            [
                'metrics'    => 'ga:users',
            ]
        );
        $totalUsers += $analyticsData->rows[0][0];
        /** ios users */
        $totalIosUsers = 'Select count(*) as users from (
            SELECT count(*) as totalUser ,app_instance FROM `runner-221bc.analytics_151500901.users` 
            where event_timestamp between' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and storeId =' . $this->store . '
            group by app_instance )';

        $queryTotalUsers   = $this->bigQuery->query($totalIosUsers);
        $queryUsersResults = $this->bigQuery->runQuery($queryTotalUsers);
        foreach ($queryUsersResults as $users) {
            $totalUsers += $users['users'];
        }

        return $totalUsers;
    }

    /**
     * Returns all analytics data for page views module.
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getPageViews($startDate, $endDate)
    {
        $startDate      = Carbon::parse($startDate);
        $endDate        = Carbon::parse($endDate);
        $totalPageViews = 0;
        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:pageviews',
            [
                'metrics'    => 'ga:pageviews',
            ]
        );
        $totalPageViews += $analyticsData[0][0];

        $totalIosPageViews = 'Select count(*) as pageviews from (
            SELECT count(*) as pageView ,app_instance FROM `runner-221bc.analytics_151500901.users`  
            where event_timestamp between' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and storeId =' . $this->store . ' and event_name = "screen_view"
            group by app_instance )';

        $queryTotalPageViews = $this->bigQuery->query($totalIosPageViews);
        $queryPageResults    = $this->bigQuery->runQuery($queryTotalPageViews);
        foreach ($queryPageResults as $pages) {
            $totalPageViews += $pages['pageviews'];
        }

        return $totalPageViews;
    }

    /**
     * Returns all analytics data for sessions module.
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function getSessions($startDate, $endDate)
    {
        $startDate        = Carbon::parse($startDate);
        $endDate          = Carbon::parse($endDate);
        $totalStoreVisits = 0;
        // set google analytics view
        Analytics::setViewId($this->gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics'    => 'ga:sessions',
            ]
        );
        $totalStoreVisits += $analyticsData[0][0];

        $totalSessions = 'select count(distinct session) as sessions FROM `runner-221bc.analytics_151500901.sessions`  
            where event_timestamp between' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and storeId =' . $this->store;

        $querySessions       = $this->bigQuery->query($totalSessions);
        $querySessionResults = $this->bigQuery->runQuery($querySessions);
        foreach ($querySessionResults as $sessions) {
            $totalStoreVisits += $sessions['sessions'];
        }

        return $totalStoreVisits;
    }

    public function all($startDate, $endDate)
    {
        $conversionRate   = $this->getTransactions($startDate, $endDate);
        $salesByMedium    = $this->getSalesByMedium($startDate, $endDate);
        $salesByCampaign  = $this->getSalesByCampaign($startDate, $endDate);
        $topPages         = $this->getTopPages($startDate, $endDate);
        $sessionBySource  = $this->getSessionsBySource($startDate, $endDate);
        $allUsers         = $this->getUsers($startDate, $endDate);
        $allPageViews     = $this->getPageViews($startDate, $endDate);
        $totalStoreVisits = $this->getSessions($startDate, $endDate);

        $partnerOverallAnalytics = [
            'totalAnalytics'  => [
                'totalSales'      => $this->totalRevenue,
                'totalOrders'     => $this->totalOrders,
                'totalCustomers'  => $this->totalCustomers,
                'avgOrderValue'   => ceil($this->totalRevenue / $this->totalOrders),
                'customerLTV'     => 0,
            ],
            'conversionRate'      => $conversionRate,
            'salesByMedium'       => $salesByMedium,
            'salesByCampaign'     => $salesByCampaign,
            'orderByDevice'       => $this->orderByDevice,
            'products'            => $this->topProducts,
            'totalStoreVisits'    => $totalStoreVisits,
            'totalPageViews'      => $allPageViews,
            'totalUsers'          => $allUsers,
            'sessionBySource'     => $sessionBySource,
            'topPages'            => $topPages,
        ];

        return response()->json([
            'data'   => [
                'type'       => 'partnerOverview',
                'id'         => $this->partner->id,
                'attributes' => $partnerOverallAnalytics,
            ],
        ]);
    }

    public function getOverviewProductSales($filter)
    {
        $productSales = OrderItem::filter($filter)
            ->select(DB::raw('day(created_at) as day'), DB::raw('month(created_at) as month'), DB::raw('year(created_at) as year'), DB::raw('SUM(quantity*retail_price) as totalRevenue'))
            ->whereIn('product_id', $this->ids)->groupBy('month', 'day', 'year')
            ->get();

        return $productSales;
    }
}
