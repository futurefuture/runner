<?php

namespace App\Services;

use Analytics;
use App\Exports\PartnerReviewExport;
use App\OrderItem;
use App\Partner;
use App\Product;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class PartnerRedShiftOverviewService
{
    protected $partnerId;
    protected $partner;
    protected $store;
    public $totalRevenue   = 0;
    public $totalOrders    = 0;
    public $totalCustomers = 0;
    public $diffInDays     = 0;
    public $topProducts    = [];
    public $ids            = [];
    public $slug           = [];
    public $slugImp        = [];
    public $ps             = [];
    public $orderByDevice  = [];
    public $gaViewId;
    public $phpdatabase  = 'php_production';
    public $iosdatabase  = 'ios_production';
    public $javascriptdb = 'javascript_production';

    public function __construct($partnerId, $startDate, $endDate)
    {
        $from             = Carbon::createFromFormat('Y-m-d', $startDate);
        $to               = Carbon::createFromFormat('Y-m-d', $endDate);
        $this->diffInDays = $to->diffInDays($from);
        $this->partner    = Partner::find($partnerId);
        $this->store      = $this->partner->store_id;
        $this->gaViewId   = 138227695;

        $this->ids = DB::table('partner_product')->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id')->toArray();
        $ps         = implode("','", $this->ids);
        $this->ps   = "'" . $ps . "'";
        $this->slug = DB::table('partner_product')->where('partner_id', $partnerId)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.slug')->toArray();
        $impSlug       = implode("','", $this->slug);
        $this->slugImp = "'" . $impSlug . "'";

        $this->getTotalSales($from, $to);
        $this->getTotalCustomers($from, $to);
        $this->topProducts   = $this->getTopProducts($from, $to);
        $this->orderByDevice = $this->getOrdersByDevice($from, $to);
        $this->phpdatabase   = 'php_production';
        $this->iosdatabase   = 'ios_production';
        $this->javascriptdb  = 'javascript_production';
    }

    /**
     * Returns overall sales, total orders for given period.
     * @param string startDate,endDate
     * @return void
     */
    protected function getTotalSales($from, $to)
    {
        $query = "Select SUM(revenue) as revenue , SUM(totalOrders) as order from (SELECT SUM(retail_price * quantity) as revenue,store_id,order_id,count(*) as totalOrders from(select json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,order_id,store_id,json_extract_path_text(product, 'productId',true) as product_id from(SELECT order_id,cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,timestamp FROM " . $this->phpdatabase . '.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  timestamp::date between ' . "'" . $from . "'" . ' and ' . "'" . $to . "'" . ')) where product_id IN( ' . $this->ps . ') group by store_id,order_id)';

        $orderItems = DB::connection('pgsql')->select($query);

        $this->totalRevenue = $orderItems[0]->revenue ?? 0;
        $this->totalOrders  = $orderItems[0]->order ?? 0;
    }

    /**
     * Returns total customers.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    protected function getTotalCustomers($from, $to)
    {
        $query = "select count(DISTINCT user_id) as totalCustomer from(select user_id,json_extract_path_text(product, 'productId',true) as product_id from (SELECT DISTINCT user_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,timestamp FROM " . $this->phpdatabase . '.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between' . "'" . $from . "'" . ' and ' . "'" . $to . "'" . ' ) where product_id IN( ' . $this->ps . '))';

        $users = DB::connection('pgsql')->select($query);

        $this->totalCustomers = $users[0]->totalcustomer;
    }

    /**
     * Returns total customers.
     * @param string $from
     * @param string $to
     * @return array
     */
    protected function getTopProducts($from, $to)
    {
        $query = "SELECT product_id,store_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,order_id,store_id from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,timestamp FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between' . "'" . $from . "'" . ' and ' . "'" . $to . "'" . ')) where  product_id IN( ' . $this->ps . ') group by product_id,store_id order By revenue desc limit 5';

        $products    = DB::connection('pgsql')->select($query);
        $topProducts = [];

        foreach ($products as $product) {
            $newProduct          = Product::find($product->product_id);
            $topProduct['title'] = $newProduct->title ?? 0;
            $topProduct['price'] = $product->revenue ?? 0;

            array_push($topProducts, $topProduct);
        }

        return $topProducts;
    }

    /**
     * Returns all analytics data for transactions module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getTransactions($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        // set google analytics view
        //sessions
        $sessionQuery    = 'select count(*) as session from (select distinct anonymous_id from ' . $this->javascriptdb . '.product_viewed where  product_id IN( ' . $this->ps . ') and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')';
        $sessionWebQuery = DB::connection('pgsql')->select($sessionQuery);
        $iosSession      = 'select count(*) as session from (select distinct anonymous_id from ' . $this->iosdatabase . '.product_viewed where  params_product_id IN( ' . $this->ps . ') and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')';
        $iosSessionQuery = DB::connection('pgsql')->select($iosSession);

        //checkout
        $checkoutProduct  = 0;
        $checkoutQuery    = "Select count(*),json_extract_path_text(product, 'id',true) as product_id from (Select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id from " . $this->iosdatabase . '.checkout_started, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' ) where product_id IN( ' . $this->ps . ') group by params_store_id,product_id';
        $checkoutIos      = DB::connection('pgsql')->select($checkoutQuery);
        $checkoutWebQuery = "Select count(*),json_extract_path_text(product, 'productId',true) as product_id from (Select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id from " . $this->javascriptdb . '.checkout_started, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' ) where product_id IN( ' . $this->ps . ') group by store_id,product_id';
        $checkoutWeb      = DB::connection('pgsql')->select($checkoutWebQuery);

        foreach ($checkoutWeb as $checkoutWeb) {
            $checkoutProduct += $checkoutWeb->count;
            $checkoutProduct += $checkoutWeb->count;
        }

        //Add to Cart
        $addToCart = 0;
        // $query = "SELECT count(*),product_id,store_id FROM " . $this->phpdatabase . ".product_added where timestamp::date between "  . "'" . $startDate . "'" . ' and '  . "'" . $endDate . "'" . " and  product_id IN( " . $this->ps . ")  group by product_id,store_id";

        // $query = "select anonymous_id,product_id,user_id,timestamp::date from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,timestamp::date from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =" . $this->partner->id . "and anonymous_id is not null";

        // $productAddToCart = DB::connection('pgsql')->select($query);

        // foreach ($productAddToCart as $cart) {
        //     $productViewQuery = "select count(*),product_id from javascript_production.product_viewed where anonymous_id =" . "'" . $cart->anonymous_id . "'" . " and product_id = " . $cart->product_id . " and timestamp::date =" . "'" . $cart->timestamp . "'" . " group by product_id";

        //     $productViewed = DB::connection('pgsql')->select($productViewQuery);
        //     if ($productViewed) {
        //         $addToCart = $addToCart + $productViewed[0]->count;
        //     }
        // }
        // //TODO when anonymous id in product added table
        // $iosAddToCart = "SELECT count(*),params_product_id,params_store_id FROM ios_development.product_added where  timestamp::date between "  . "'" . $startDate . "'" . ' and '  . "'" . $endDate . "'" . " and  params_product_id IN( " . $this->ps . ") group by params_product_id,params_store_id";

        // $productAddToCartIOS = DB::connection('pgsql')->select($iosAddToCart);

        // foreach ($productAddToCartIOS as $cart) {
        //     $addToCart += $cart->count;
        // }

        $query            = 'select SUM(count) as totalAddToCart from (select product_id,count(*) from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,timestamp::date from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =' . $this->partner->id . '  and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id )';
        $productAddToCart = DB::connection('pgsql')->select($query);
        $addToCart        = $productAddToCart[0]->totaladdtocart;

        //transaction
        $transactionProduct = 0;
        $transactionQuery   = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId',true) as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,device,timestamp::date FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')) where product_id IN( ' . $this->ps . ') group by product_id,store_id,device';
        $productTransaction = DB::connection('pgsql')->select($transactionQuery);

        foreach ($productTransaction as $transaction) {
            $transactionProduct += $transaction->count;
        }

        $conversionRate['sessions']          = $sessionWebQuery[0]->session + $iosSessionQuery[0]->session;
        $conversionRate['productAddsToCart'] = $addToCart;
        $conversionRate['productCheckouts']  = $checkoutProduct;
        $conversionRate['transactions']      = $transactionProduct;

        return $conversionRate;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getSalesByMedium($startDate, $endDate)
    {
        $startDate       = Carbon::parse($startDate);
        $endDate         = Carbon::parse($endDate);
        $emailQuery      = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity',true) * json_extract_path_text(product, 'price',true)) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp::date from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,timestamp::date from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from javascript_production.pages where  timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " and anonymous_id IN (select anonymous_id from javascript_production.pages where context_campaign_medium = 'email' and timestamp::date between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id IN( " . $this->ps . ') and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp )';
        $salesByEmail    = DB::connection('pgsql')->select($emailQuery);
        $digitialQuery   = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity',true) * json_extract_path_text(product, 'price',true)) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp::date from (select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,timestamp::date from php_production.order_completed, seq_0_to_20 AS seq where  seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from javascript_production.pages where  timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " and anonymous_id IN (select anonymous_id from javascript_production.pages where context_campaign_medium = 'digital' and timestamp::date between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id IN( " . $this->ps . ') and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp::date )';
        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param string $startDate
     * @param string $endDate
     * @return array||void
     */
    protected function getSalesByCampaign($startDate, $endDate)
    {
        $startDate             = Carbon::parse($startDate);
        $endDate               = Carbon::parse($endDate);
        $topClickCampaignQuery = "select context_campaign_name,count(*),split_part(context_page_path, '/', 3) as slug from javascript_production.pages  where context_campaign_name is not null and slug in(" . $this->slugImp . ') and timestamp::date  between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by context_campaign_name,slug order by count(*) desc ';
        $topCampaignName       = DB::connection('pgsql')->select($topClickCampaignQuery);

        if ($topCampaignName) {
            $formattedCampaignSales = [];

            foreach ($topCampaignName as $campaign) {
                // get orders  by each campaign name
                $orderQuery             = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity',true) * json_extract_path_text(product, 'price',true)) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp::date from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,timestamp::date from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products,true)and timestamp::date between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and anonymous_id IN (select DISTINCT anonymous_id from javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') and  timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . " and path = '/confirmation')) where product_id IN(  " . $this->ps . ') and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by product_id,timestamp::date )';
                $getSalesByCampaign     = DB::connection('pgsql')->select($orderQuery);
                $campaignSales['name']  = $campaign->context_campaign_name;
                $campaignSales['sales'] = $getSalesByCampaign[0]->emailsales ?? 0;

                array_push($formattedCampaignSales, $campaignSales);
            }

            return $formattedCampaignSales;
        } else {
            return;
        }
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param string $startDate
     * @param string $endDate
     * @return array||void
     */
    protected function getSessionsBySource($startDate, $endDate)
    {
        $startDate     = Carbon::parse($startDate);
        $endDate       = Carbon::parse($endDate);
        $query         = "select SUM(totalClick),context_campaign_source from(select context_campaign_source,count(*) as totalClick,split_part(context_page_path, '/', 3) as slug from javascript_production.pages where slug in (" . $this->slugImp . ') and context_campaign_source is not null and timestamp::date  between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by context_campaign_source,slug) group by context_campaign_source order by sum desc limit 5 ';
        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                $salesSource[$medium->context_campaign_source] = $medium->sum;
            }

            return $salesSource;
        } else {
            return;
        }
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getTopProductsPages($startDate, $endDate)
    {
        $startDate    = Carbon::parse($startDate);
        $endDate      = Carbon::parse($endDate);
        $query        = "select path,count(*),split_part(context_page_path, '/', 3) as slug from javascript_production.pages where  slug IN( " . $this->slugImp . ') and timestamp::date  between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' group by path, slug order by count(*) desc limit 5';
        $salesByPages = DB::connection('pgsql')->select($query);

        foreach ($salesByPages as $medium) {
            $salesByPages[$medium->path] = $medium->count;
        }

        return $salesByPages;
    }

    /**
     * Returns all analytics data order by device.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getOrdersByDevice($startDate, $endDate)
    {
        $query = "SELECT count(*),device,order_id from(select json_extract_path_text(product, 'productId',true) as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,device,timestamp::date FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and timestamp::date between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ')) where  product_id IN( ' . $this->ps . ') group by device,order_id';

        $orderByDevice   = DB::connection('pgsql')->select($query);
        $iosCount        = 0;
        $webDesktopCount = 0;

        foreach ($orderByDevice as $device) {
            if ($device->device == 1) {
                $webDesktopCount = $webDesktopCount + $device->count;
            } else {
                $iosCount = $iosCount + $device->count;
            }
        }

        return $orderByDevice = [
            'ios'        => $iosCount ?? 0,
            'webDesktop' => $webDesktopCount ?? 0,
        ];
    }

    /**
     * Returns all analytics data for page views module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getProductViews($startDate, $endDate)
    {
        $startDate         = Carbon::parse($startDate);
        $endDate           = Carbon::parse($endDate);
        $totalProductViews = 0;
        $webProductViews   = 'Select SUM(click) from(SELECT count(*) as click,product_id,store_id from(SELECT product_id,store_id,timestamp::date FROM ' . $this->javascriptdb . '.product_viewed WHERE timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') where  product_id IN( ' . $this->ps . ') group by product_id,store_id)';
        $webProductView    = DB::connection('pgsql')->select($webProductViews);
        $iosProductViews   = 'Select SUM(click) from(SELECT count(*) as click,params_product_id,params_store_id from(SELECT params_product_id,params_store_id,timestamp::date FROM ' . $this->iosdatabase . '.product_viewed WHERE timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') where  params_product_id IN( ' . $this->ps . ') group by params_product_id,params_store_id )';
        $iosProductView    = DB::connection('pgsql')->select($iosProductViews);

        $totalProductViews += $webProductView[0]->sum + $iosProductView[0]->sum;

        return $totalProductViews;
    }

    /**
     * Returns all analytics data for page views module.
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function getProductImpression($startDate, $endDate)
    {
        $partnerProducts        = implode("','", $this->ids);
        $partnerProducts        = "'" . $partnerProducts . "'";
        $startDate              = Carbon::parse($startDate);
        $endDate                = Carbon::parse($endDate);
        $totalProductImpression = 0;
        $webProductListfiltered = "select SUM(count) from(select count(*), product_id,store_id from (select json_extract_path_text(product, 'productId',true) as product_id,store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,timestamp::date FROM " . $this->javascriptdb . '.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . '))  where product_id IN( ' . $this->ps . ') group by product_id,store_id )';
        $webProductListFilter   = DB::connection('pgsql')->select($webProductListfiltered);
        $webProductListViewed   = "Select SUM(count) from (select count(*), product_id,store_id from (select json_extract_path_text(product, 'productId',true) as product_id,store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,timestamp::date FROM " . $this->javascriptdb . '.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . '))  where  product_id IN( ' . $this->ps . ') group by product_id,store_id)';
        $webProductListView     = DB::connection('pgsql')->select($webProductListViewed);
        $iosProductListfiltered = "select SUM(count) from(select count(*), product_id,params_store_id from (select json_extract_path_text(product, 'id',true) as product_id,params_store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id,timestamp::date FROM " . $this->iosdatabase . '.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and  timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . '))  where  product_id IN( ' . $this->ps . ') group by product_id,params_store_id )';
        $iosProductListFilter   = DB::connection('pgsql')->select($iosProductListfiltered);
        $iosProductListViewed   = "Select SUM(count) from (select count(*), product_id,params_store_id from (select json_extract_path_text(product, 'id',true) as product_id,params_store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id,timestamp::date FROM " . $this->iosdatabase . '.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and  timestamp::date between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . '))  where  product_id IN( ' . $this->ps . ') group by product_id,params_store_id)';
        $iosProductListView     = DB::connection('pgsql')->select($iosProductListViewed);

        $totalProductImpression += $webProductListFilter[0]->sum + $webProductListView[0]->sum + $iosProductListFilter[0]->sum + $iosProductListView[0]->sum;

        return $totalProductImpression;
    }

    public function all($startDate, $endDate)
    {
        $startDate         = Carbon::createFromFormat('Y-m-d', $startDate);
        $endDate           = Carbon::createFromFormat('Y-m-d', $endDate);
        $conversionRate    = $this->getTransactions($startDate, $endDate);
        $salesByMedium     = $this->getSalesByMedium($startDate, $endDate);
        $salesByCampaign   = $this->getSalesByCampaign($startDate, $endDate);
        $topPages          = $this->getTopProductsPages($startDate, $endDate);
        $sessionBySource   = $this->getSessionsBySource($startDate, $endDate);
        $allProductViews   = $this->getProductViews($startDate, $endDate);
        $productImpression = $this->getProductImpression($startDate, $endDate);

        $partnerOverallAnalytics = [
            'totalAnalytics'  => [
                'totalSales'      => $this->totalRevenue,
                'totalOrders'     => $this->totalOrders,
                'totalCustomers'  => $this->totalCustomers,
                'avgOrderValue'   => $this->totalOrders > 0 ? ceil($this->totalRevenue / $this->totalOrders) : 0,
                'customerLTV'     => 0,
            ],
            'conversionRate'         => $conversionRate,
            'salesByMedium'          => $salesByMedium,
            'salesByCampaign'        => $salesByCampaign,
            'orderByDevice'          => $this->orderByDevice,
            'products'               => $this->topProducts,
            'totalProductImpression' => $productImpression,
            'totalProductViews'      => $allProductViews,
            'sessionBySource'        => $sessionBySource,
            'topPages'               => $topPages,
        ];

        return response()->json([
            'data'   => [
                'type'       => 'partnerOverview',
                'id'         => $this->partner->id,
                'attributes' => $partnerOverallAnalytics,
            ],
        ]);
    }
}
