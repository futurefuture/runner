<?php

namespace App\Services\V5;

use App\Category;
use App\Inventory;
use App\Http\Resources\Runner\V5\Category as CategoryResource;
use App\Http\Resources\Runner\V5\CategoryCollection;
use App\OrderItem;
use App\Product;
use DB;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryService
{
    protected $tree = [];

    protected $count = 0;

    protected $link;

    public function index()
    {
        $categories = QueryBuilder::for(Category::class)
            ->allowedIncludes(['children', 'tags'])
            ->get();

        return new CategoryCollection($categories);
    }

    public function show($category)
    {
        $category = QueryBuilder::for(Category::where('id', $category->id))
            ->allowedIncludes(['children', 'tags'])
            ->first();

        return new CategoryResource($category);
    }
}
