<?php

namespace App\Services;

use App\Category;
use App\Inventory;
use App\Http\Resources\Runner\Category as CategoryResource;
use App\Http\Resources\Runner\CategoryCollection;
use App\OrderItem;
use App\Product;
use DB;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryService
{
    protected $tree = [];

    protected $count = 0;

    protected $link;

    public function index()
    {
        $categories = QueryBuilder::for(Category::class)
            ->allowedIncludes(['children', 'tags'])
            ->get();

        return new CategoryCollection($categories);
    }

    public function show($category)
    {
        $category = QueryBuilder::for(Category::where('id', $category->id))
            ->allowedIncludes(['children', 'tags'])
            ->first();

        return new CategoryResource($category);
    }

    public function getParentCategory($categories)
    {
        $parentCategoryIds = [];

        foreach ($categories as $cat) {
            if ($cat['parent_id']) {
                array_push($parentCategoryIds, $cat['parent_id']);
            }
        }

        return array_values(array_unique($parentCategoryIds));
    }

    public function getPartnerProductCategory($categories)
    {
        $productCategoryIds = [];
        foreach ($categories as $cat) {
            if ($cat['productCategoryId']) {
                array_push($productCategoryIds, $cat['productCategoryId']);
            }
        }

        return array_values(array_unique($productCategoryIds));
    }

    public function getFormattedCategory($formattedParentCategoryIds, $formattedProductCategoryIds, $partnerProducts, $filter)
    {
        $allCategorySales    = [];
        $formattedCategories = [];
        foreach ($formattedParentCategoryIds as $f) {
            $parentCategory                 = Category::find($f);
            $parentCategory['partnerSales'] = 0;
            $parentCategory['partnerUnits'] = 0;
            $parentCategory['runnerSales']  = 0;

            foreach ($formattedProductCategoryIds as $p) {
                $category = Category::find($p);

                if ($category->parent_id == $f || $category->id == $f) {
                    $category['partnerSales'] = 0;
                    $category['partnerUnits'] = 0;
                    $category['runnerSales']  = 0;

                    $orderItems = OrderItem::filter($filter)
                        ->select(DB::raw('SUM(quantity*retail_price)/100 as totalSold'), DB::raw('SUM(quantity) as totalQuantity'))
                        ->where('category_id', $p)
                        ->get()->toArray();

                    $partnerOrderItems = OrderItem::filter($filter)
                        ->whereIn('product_id', $partnerProducts)
                        ->where('category_id', $p)
                        ->get();

                    $category['runnerSales'] += $orderItems[0]['totalSold'];
                    $parentCategory['runnerSales'] += $orderItems[0]['totalSold'];

                    /**  foreach ($orderItems as $o) {
                        $category['runnerSales'] += ($o->retail_price * $o->quantity) / 100;
                        $parentCategory['runnerSales'] += ($o->retail_price * $o->quantity) / 100;.
                    }
                     */
                    foreach ($partnerOrderItems as $p) {
                        $category['partnerSales'] += ($p->retail_price * $p->quantity) / 100;
                        $category['partnerUnits'] += $p->quantity;
                        $parentCategory['partnerSales'] += ($p->retail_price * $p->quantity) / 100;
                        $parentCategory['partnerUnits'] += $p->quantity;
                    }

                    if (in_array($category->id, $formattedParentCategoryIds, true)) {
                    } else {
                        array_push($allCategorySales, $category);
                    }
                }
            }

            array_push($allCategorySales, $parentCategory);
        }

        foreach ($allCategorySales as $a) {
            // skip alcohol as category
            if ($a->id === 1) {
                continue;
            }

            $formattedCategory = (object) [
                'type'       => 'category percentages',
                'attributes' => [
                    'title'        => $a->title,
                    'slug'         => $a->slug,
                    'categoryId'   => $a->id,
                    'parentId'     => $a->parent_id,
                    'partnerSales' => $a->partnerSales,
                    'runnerSales'  => $a->runnerSales,
                    'partnerUnits' => $a->partnerUnits,
                ],
            ];

            array_push($formattedCategories, $formattedCategory);
        }

        return response()->json([
            'data' => $formattedCategories,
        ]);
    }

    public function getRedshiftFormattedCategory($formattedParentCategoryIds, $formattedProductCategoryIds, $partnerProducts, $startDate, $endDate)
    {
        $ps                  = implode("','", $partnerProducts);
        $ps                  = "'" . $ps . "'";
        $allCategorySales    = [];
        $formattedCategories = [];
        $parentCategory      = [2, 3, 4, 5];

        $parent = array_intersect($formattedParentCategoryIds, $parentCategory);
        foreach ($formattedParentCategoryIds as $f) {
            $parentCategory                 = Category::find($f);
            $parentCategory['partnerSales'] = 0;
            $parentCategory['partnerUnits'] = 0;
            $parentCategory['runnerSales']  = 0;

            $getAllChildQuery = 'select c1.id as child_id from categories c1 left join categories c2 on c2.id = c1.parent_id left join categories c3 on c3.id = c2.parent_id left join categories c4 on c4.id = c3.parent_id where ' . $f . ' in (c1.parent_id, c2.parent_id, c3.parent_id) union select id from categories where id =' . $f;

            $allChildCat = DB::select($getAllChildQuery);
            $catArr      = [];
            foreach ($allChildCat as $cat) {
                array_push($catArr, $cat->child_id);
            }
            $cat_implode = implode("','", $catArr);
            $allCat      = "'" . $cat_implode . "'";
            if (in_array($f, $parent)) {
                $parentRunnerQuery = "Select SUM(quantity) as unitSold,SUM(retail_price * quantity)/100 as totalSold from (SELECT quantity,retail_price,category,received_at from(select json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and category In(' . $allCat . '))';

                $parentOrderItems = DB::connection('pgsql')->select($parentRunnerQuery);
                $parentCategory['runnerSales'] += $parentOrderItems[0]->totalsold;

                $partnerParentQuery = "Select SUM(quantity) as unitSold,SUM(retail_price * quantity)/100 as totalSold from (SELECT quantity,retail_price,category,product_id,received_at from(select json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id In(' . $ps . ') and category In(' . $allCat . '))';

                $partnerParentItems = DB::connection('pgsql')->select($partnerParentQuery);

                $parentCategory['partnerSales'] += $partnerParentItems ? $partnerParentItems[0]->totalsold : 0;
                $parentCategory['partnerUnits'] += $partnerParentItems ? $partnerParentItems[0]->unitsold : 0;
                array_push($allCategorySales, $parentCategory);
            }
            foreach ($formattedProductCategoryIds as $p) {
                $category = Category::find($p);
                if ($category->parent_id == $f || $category->id == $f) {
                    $category['partnerSales'] = 0;
                    $category['partnerUnits'] = 0;
                    $category['runnerSales']  = 0;

                    $runnerQuery = "Select SUM(quantity) as unitSold,SUM(retail_price * quantity)/100 as totalSold from (SELECT quantity,retail_price,category,received_at from(select json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and category =' . $p . ')';

                    $orderItems = DB::connection('pgsql')->select($runnerQuery);

                    $partnerQuery = "SELECT quantity,retail_price,category,received_at from(select json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id In(' . $ps . ') and category =' . $p . '';

                    $partnerOrderItems = DB::connection('pgsql')->select($partnerQuery);

                    $category['runnerSales'] += $orderItems[0]->totalsold;

                    foreach ($partnerOrderItems as $partner) {
                        $category['partnerSales'] += $partner ? ($partner->retail_price * $partner->quantity) / 100 : 0;
                        $category['partnerUnits'] += $partner ? $partner->quantity : 0;
                    }

                    if (in_array($category->id, $parent, true) || $category->id == $f) {
                    } else {
                        array_push($allCategorySales, $category);
                    }
                }
            }
        }

        foreach ($allCategorySales as $a) {
            // skip alcohol as category
            if ($a->id === 1) {
                continue;
            }

            $formattedCategory = (object) [
                'type'       => 'category percentages',
                'attributes' => [
                    'title'        => $a->title,
                    'slug'         => $a->slug,
                    'categoryId'   => $a->id,
                    'parentId'     => $a->parent_id,
                    'partnerSales' => $a->partnerSales,
                    'runnerSales'  => $a->runnerSales,
                    'partnerUnits' => $a->partnerUnits,
                ],
            ];

            array_push($formattedCategories, $formattedCategory);
        }

        return $formattedCategories;
        // return response()->json([
        //     'data' => $formattedCategories,
        // ]);
    }

    public function getRedshiftCategoryOptimised($partnerProducts, $partner, $startDate, $endDate)
    {
        $ps = implode("','", $partnerProducts);
        $ps = "'" . $ps . "'";

        $getAllWineQuery = 'select c1.id as child_id from categories c1 left join categories c2 on c2.id = c1.parent_id left join categories c3 on c3.id = c2.parent_id left join categories c4 on c4.id = c3.parent_id where 4 in (c1.parent_id, c2.parent_id, c3.parent_id)';

        $allWineChildCat = DB::select($getAllWineQuery);

        $catArr = [];
        foreach ($allWineChildCat as $cat) {
            array_push($catArr, $cat->child_id);
        }
        $wine_cat_implode = implode("','", $catArr);
        $allWineCat       = "'" . $wine_cat_implode . "'";

        $getAllBeerQuery = 'select c1.id as child_id from categories c1 left join categories c2 on c2.id = c1.parent_id left join categories c3 on c3.id = c2.parent_id left join categories c4 on c4.id = c3.parent_id where 2 in (c1.parent_id, c2.parent_id, c3.parent_id)';

        $allBeerChildCat = DB::select($getAllBeerQuery);

        $catArr = [];
        foreach ($allBeerChildCat as $cat) {
            array_push($catArr, $cat->child_id);
        }
        $beer_cat_implode   = implode("','", $catArr);
        $allBeerCat         = "'" . $beer_cat_implode . "'";
        $allPartnerCategory = DB::table('category_partner')->where('partner_id', $partner->id)
            ->pluck('category_id')->toArray();

        $partnerParentQuery = "Select product_id,SUM(retail_price * quantity)/100 as totalSold from (SELECT quantity,retail_price,category,product_id,received_at from(select json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id In(' . $ps . ') ) group by product_id order by totalSold desc';

        $runnerProductQuery =  "Select product_id,SUM(retail_price * quantity)/100 as totalSold from (SELECT quantity,retail_price,category,product_id,received_at from(select json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'category',true) as category,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between" . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' ) group by product_id order by totalSold desc';

        $runnerQuery = DB::connection('pgsql')->select($runnerProductQuery);

        $partnerQuery = DB::connection('pgsql')->select($partnerParentQuery);

        $formattedRunnerCategory = [];

        foreach ($runnerQuery as $sales) {
            $categories = DB::table('category_product')->where('product_id', $sales->product_id)
                ->pluck('category_id')->toArray();

            foreach ($categories as $cat) {
                $productRunnerCategory = [];
                $category              = Category::find($cat);
                if (in_array($cat, $allPartnerCategory)) {
                    $productRunnerCategory['title']     = $category->title;
                    $productRunnerCategory['parentId']  = $category->parent_id;
                    $productRunnerCategory['productId'] = $sales->product_id;
                    $productRunnerCategory['id']        = $cat;
                    $productRunnerCategory['sales']     = $sales->totalsold;
                    array_push($formattedRunnerCategory, $productRunnerCategory);
                }
            }
        }
        $runnerdata = [];
        foreach ($formattedRunnerCategory as $c) {
            if (isset($runnerdata[$c['id']])) {
                $runnerdata[$c['id']]['sales'] = $runnerdata[$c['id']]['sales'] + $c['sales'];
            } else {
                $runnerdata[$c['id']]['sales'] = $c['sales'];
            }
            $runnerdata[$c['id']]['title']      = $c['title'];
            $runnerdata[$c['id']]['parentId']   = $c['parentId'];
            $runnerdata[$c['id']]['categoryId'] = $c['id'];
        }
        $price = array_column($runnerdata, 'sales');

        $productsWithTotalSold = array_multisort($price, SORT_DESC, $runnerdata);

        $formattedCategory = [];

        $partnerQuery = DB::connection('pgsql')->select($partnerParentQuery);
        foreach ($partnerQuery as $sales) {
            $categories = DB::table('category_product')->where('product_id', $sales->product_id)
                ->pluck('category_id')->toArray();
            foreach ($categories as $cat) {
                $productCategory = [];
                $category        = Category::find($cat);
                if (in_array($cat, $allPartnerCategory)) {
                    $productCategory['title']     = $category->title;
                    $productCategory['parentId']  = $category->parent_id;
                    $productCategory['productId'] = $sales->product_id;
                    $productCategory['id']        = $cat;
                    $productCategory['sales']     = $sales->totalsold;
                    array_push($formattedCategory, $productCategory);
                }
            }
        }
        $partnerdata = [];
        foreach ($formattedCategory as $c) {
            if (isset($partnerdata[$c['id']])) {
                $partnerdata[$c['id']]['sales'] = $partnerdata[$c['id']]['sales'] + $c['sales'];
            } else {
                $partnerdata[$c['id']]['sales'] = $c['sales'];
            }
            $partnerdata[$c['id']]['title']      = $c['title'];
            $partnerdata[$c['id']]['parentId']   = $c['parentId'];
            $partnerdata[$c['id']]['categoryId'] = $c['id'];
        }

        $structuredCat = [];
        foreach ($runnerdata as $data) {
            foreach ($partnerdata as $partner) {
                $patnerCategory     = array_column($partnerdata, 'categoryId');
                $alreadyExistingCat = array_column($structuredCat, 'categoryId');
                $mergeCat           = [];
                if ($data['categoryId'] == $partner['categoryId']) {
                    $mergeCat['title']        = $data['title'];
                    $mergeCat['parentId']     = $data['parentId'];
                    $mergeCat['categoryId']   = $data['categoryId'];
                    $mergeCat['partnerSales'] = $partner['sales'];
                    $mergeCat['runnerSales']  = $data['sales'];
                    array_push($structuredCat, $mergeCat);
                } elseif (! in_array($data['categoryId'], $patnerCategory) && ! in_array($data['categoryId'], $alreadyExistingCat)) {
                    $mergeCat['title']        = $data['title'];
                    $mergeCat['parentId']     = $data['parentId'];
                    $mergeCat['categoryId']   = $data['categoryId'];
                    $mergeCat['partnerSales'] = 0;
                    $mergeCat['runnerSales']  = $data['sales'];
                    array_push($structuredCat, $mergeCat);
                }
            }
        }
        $seqCat = [];
        for ($i = 0; $i < count($structuredCat); $i++) {
            for ($j = $i + 1; $j < count($structuredCat); $j++) {
                if ($structuredCat[$i]['parentId'] == 1 && $j == $i + 1) {
                    array_push($seqCat, $structuredCat[$i]);
                }
                if ($structuredCat[$j]['parentId'] == $structuredCat[$i]['categoryId']) {
                    array_push($seqCat, $structuredCat[$j]);
                } else {
                }
            }
        }

        return $seqCat;
    }

    public function getAllCategories($categoryId)
    {
        $categoryIds = [];

        if ($categoryId == 0) {
            $categories = Category::all();

            foreach ($categories as $category) {
                array_push($categoryIds, $category->id);
            }
        } else {
            $categories = $this->getCategoriesByParentId($categoryId);

            foreach ($categories as $category) {
                array_push($categoryIds, $category['id']);
            }
        }

        return  $categoryIds;
    }

    /**
     * Function to get all Categories by parent Id.
     *
     * @param [type] $categoryId
     *
     * @return array
     */
    public function getCategoriesByParentId($categoryId)
    {
        $category_data             = [];
        $parentCategory            = Category::find($categoryId);
        $formattedParentCategory[] = [
            'id'        => $parentCategory->id,
            'title'     => $parentCategory->title,
            'slug'      => $parentCategory->slug,
            'parent_id' => $parentCategory->parent_id,
        ];
        $category_query = Category::where('parent_id', $categoryId)->get();

        foreach ($category_query as $category) {
            $category_data[] = [
                'id'        => $category->id,
                'title'     => $category->title,
                'slug'      => $category->slug,
                'parent_id' => $category->parent_id,
            ];
            $children = $this->getCategoriesByParentId($category->id);

            if ($children) {
                $category_data = array_merge($children, $category_data);
            }
        }

        return array_merge($category_data, $formattedParentCategory);
    }

    /**
     * function to get Top Products for Category.
     *
     * @param [type] $id
     *
     * @return array
     */
    public function getTopProducts($id)
    {
        $topProductIds = '';
        switch ($id) {
            case 2:
                $topProductIds = '1597, 9729, 8272, 8716, 12039, 3283, 10249, 8438, 296, 9044, 1808, 3210, 3282, 7784, 844, 6604, 9415, 2421, 6849, 10601, 10949, 8241, 7197, 7752, 226, 4209, 11840, 11924, 14138, 2787, 3210, 3841, 912, 17069, 5773';

                return $topProductIds;

                break;
            case 3:
                $topProductIds = '5349, 4526, 5326, 3711, 1855, 272, 6476, 2502, 8278, 2777, 2715, 2131, 7498, 4688, 9807, 291, 3098, 10096, 1579, 304';

                return $topProductIds;

                break;
            case 4:
                $topProductIds = '3188, 3745, 2353, 3128, 3249, 128, 13947, 2199, 12860, 508, 7226, 2802, 2180, 6151, 2189, 6132, 393, 528, 1284, 419, 191, 23, 8760, 12966, 681, 2180, 6151, 1078, 218, 506, 1078, 4020, 691, 3768, 7358, 9594, 18018, 16719, 3102, 12776';

                return $topProductIds;

                break;
            case 5:
                $topProductIds = '10889, 6833, 8207, 5594, 5581, 4713, 1510, 10871, 10772, 10798, 3870, 11534, 6174, 11534, 3633, 1902, 8198, 10891, 1904';

                return $topProductIds;

                break;
            case 6:
                $topProductIds = '3188, 2353, 13947, 7226, 2180, 6151, 393, 1284, 419, 239, 8760, 12966, 218, 1078, 3768, 7358, 12930, 1520,  4020, 506, 2244, 1916, 1173, 2800, 127, 812, 1216, 3551, 7345, 14418, 671, 2263, 8588, 3295, 4388, 2492, 8640, 621, 413, 139';

                return $topProductIds;

                break;
            case 7:
                $topProductIds = '3128, 3249, 128, 241, 2199, 508, 681, 2189, 528, 191, 1314, 1011, 12381, 664, 4040, 12030, 13601, 508, 7844, 2177, 8040, 3120, 2576, 3766, 12940, 4683, 14234, 12934, 5839, 14920, 1274, 744, 5896, 4882, 5545, 12319, 2580, 8449, 3966';

                return $topProductIds;

                break;
            case 9:
                $topProductIds = '705, 1091, 687, 571, 2979, 1180, 1602, 1637, 11992, 7718, 2137, 2911, 1140, 374, 297, 249, 282, 244, 240, 291';

                return $topProductIds;

                break;
            case 10:
                $topProductIds = '6866, 1157, 1815, 1509, 352, 10763, 11117, 4785, 5660, 6867, 14929, 12911, 1142, 3031,4680, 3689, 3185, 3625, 2311, 8302';

                return $topProductIds;

                break;
            case 11:
                $topProductIds = '7280, 5003, 1444, 8719, 408, 8735, 6932, 7863, 3389, 11950, 4557, 3692, 11325, 1714, 4989, 1015, 2066';

                return $topProductIds;

                break;
            case 12:
                $topProductIds = '2005, 3014, 13715, 7901, 2510, 8091, 2012, 319, 1177, 5222, 13912, 2524, 1963, 2035, 5542, 922, 12397, 1991, 11935, 14094';

                return $topProductIds;

                break;
            case 18:
                $topProductIds = '11389, 11255, 10156, 8921, 8455, 5681, 5019, 4998, 4318, 1629, 769, 590, 536, 310, 268, 245';

                return $topProductIds;

                break;
            case 19:
                $topProductIds = '579, 1982, 7617, 990, 336, 3463, 231, 238, 346, 404, 331, 304, 225, 7813, 1026, 2974, 11185, 673, 5661, 5793';

                return $topProductIds;

                break;
            case 20:
                $topProductIds = '4169, 7719, 2325, 1265, 2161, 5379, 940, 280, 8027,9996, 416, 1986, 2715, 13119, 12438, 197, 2502, 4515, 6456, 10661, 8338, 10236, 5879, 1881, 1237, 496, 4941, 4662, 5669, 201, 6732, 3854, 3853, 5883, 994, 1079, 2119';

                return $topProductIds;

                break;
            case 21:
                $topProductIds = '316, 2565, 3921,4688, 247, 272, 652, 888, 725, 5464, 5465, 1040, 1439, 1611, 202, 228, 220, 2019, 2133, 248';

                return $topProductIds;

                break;
            case 23:
                $topProductIds = '2160, 2711, 1581, 251, 463, 3682, 2785, 2163, 1579, 6403, 1577, 1576, 1116, 1189, 5032, 516, 517, 611, 633, 8278';

                return $topProductIds;

                break;
            case 28:
                $topProductIds = '579, 1982, 7617, 990, 336, 3463, 231, 238, 346, 404, 331, 304, 225, 7813, 1026, 2974, 11185, 673, 5661, 5793';

                return $topProductIds;

                break;
            case 34:
                $topProductIds = '17322';

                return $topProductIds;

                break;
        }
    }

    /**
     * Function to get all children of given Category.
     *
     * @return array
     */
    public function getAllChildren($categoryId)
    {
        $childCategoryIds = [];
        $childrenIds      = [];
        $categoryChildren = $this->getAllChild($categoryId);

        foreach ($categoryChildren as $child) {
            $parentTitle  = $child->parentTitle;
            $parentSlug   = $child->parentSlug;
            $parentId     = $child->category_id;
            $childOfChild = $this->getAllChild($child->child);
            if ($childOfChild) {
                foreach ($childOfChild as $c) {
                    $childrenIds[] = [
                        'id'    => $c->child,
                        'title' => $c->childTitle,
                        'slug'  => $c->childSlug,
                    ];
                }
                $childCategoryIds[] = [
                    'id'       => $child->child,
                    'title'    => $child->childTitle,
                    'slug'     => $child->childSlug,
                    'children' => $childrenIds,
                ];
                $childrenIds = [];
            } else {
                $childCategoryIds[] = [
                    'id'    => $child->child,
                    'title' => $child->childTitle,
                    'slug'  => $child->childSlug,
                ];
            }
        }

        return response()->json([
            'data' => [
                'type'       => 'category children',
                'title'      => $parentTitle,
                'slug'       => $parentSlug,
                'attributes' => [
                    'children' => $childCategoryIds,
                ],
            ],
        ]);
    }

    /**
     * Function to get all parent of given Category.
     *
     * @return array
     */
    public function getAllParents($categoryId)
    {
        $parentCategoriesIds = [];

        $parentCategories = DB::select('SELECT c1.id as category_id,c1.title,IF(c1.parent_id && c2.parent_id,CONCAT(c1.parent_id, "," ,c2.parent_id),CONCAT(c1.parent_id)) AS parent FROM categories c1 LEFT JOIN categories  c2 on c2.id = c1.parent_id LEFT JOIN categories c3 on c3.id = c2.parent_id where c1.id =' . $categoryId . ' GROUP BY category_id');

        $formattedCategories = [];

        foreach ($parentCategories as $parent) {
            $categoryId    = $parent->category_id;
            $categoryTitle = $parent->title;
            $parents       = explode(',', $parent->parent);

            foreach ($parents as $key => $value) {
                $cat                   = Category::find($value);
                $parentCategoriesIds[] = [
                    'id'    => $value,
                    'title' => $cat->title,
                    'slug'  => $cat->slug,
                ];
            }
            $formattedCategories[] = [
                'parent' => $parentCategoriesIds,
            ];
            $parentCategoriesIds = [];

            return [
                'type'       => 'category parent',
                'id'         => $categoryId,
                'title'      => $categoryTitle,
                'attributes' => $formattedCategories,
            ];
        }
    }

    /**
     * Function to get all children of given Category.
     *
     * @return array
     */
    public function getAllChild($categoryId)
    {
        $childCategories = DB::select('SELECT c.id as category_id,c.title as parentTitle,c.slug as parentSlug,c1.id child,c1.title as childTitle,c1.slug as childSlug FROM categories as c INNER JOIN categories c1 ON  c.id = c1.parent_id where c.id = ' . $categoryId . ' ORDER BY category_id');

        return $childCategories;
    }

    public function getBreadCrumbs(Category $category)
    {
        $parentCategory = Category::find($category->parent_id);

        if ($this->count == 0) {
            array_push($this->tree, [
                'id'    => $category->id,
                'title' => $category->title,
                'slug'  => $category->slug,
            ]);
        }

        $this->count++;

        if ($parentCategory) {
            $category = [
                'id'    => $parentCategory->id,
                'title' => $parentCategory->title,
                'slug'  => $parentCategory->slug,
            ];

            array_push($this->tree, $category);

            $this->getBreadCrumbs($parentCategory);
        } else {
            array_push($this->tree, [
                'id'    => null,
                'title' => 'Home',
                'slug'  => '/',
            ]);
        }

        return array_reverse($this->tree);
    }

    public function categories(Category $category)
    {
        $children          = Category::where('parent_id', $category->id)->get();
        $formattedChildren = [];

        if (! $children->count()) {
            $parentCategory = Category::find($category->parent_id);

            if ($parentCategory) {
                $parentChildren = Category::where('parent_id', $parentCategory->id)->get();

                foreach ($parentChildren as $c) {
                    $category = [
                        'id'    => $c->id,
                        'title' => $c->title,
                        'slug'  => $c->slug,
                    ];

                    array_push($formattedChildren, $category);
                }
            }
        } else {
            foreach ($children as $c) {
                $category = [
                    'id'         => $c->id,
                    'title'      => $c->title,
                    'slug'       => $c->slug,
                    'pill_image' => $c->pill_image,
                ];

                array_push($formattedChildren, $category);
            }
        }

        return $formattedChildren;
    }

    public function categoriesWithCount(Inventory $inventory, Category $category)
    {
        $children          = Category::where('parent_id', $category->id)->get();
        $formattedChildren = [];

        if (! $children->count()) {
            $parentCategory = Category::find($category->parent_id);
            $parentChildren = Category::where('parent_id', $parentCategory->id)->get();

            foreach ($parentChildren as $c) {
                $category = [
                    'id'           => $c->id,
                    'title'        => $c->title,
                    'slug'         => $c->slug,
                    'pillImage'    => $c->pill_image,
                    'productCount' => $inventory
                        ->productsAll()
                        ->join('category_product', 'products.id', '=', 'category_product.product_id')
                        ->join('categories', 'categories.id', '=', 'category_product.category_id')
                        ->where('category_product.category_id', '=', $c->id)
                        ->select('products.*', 'category_product.index', 'quantity')
                        ->count()
                ];

                array_push($formattedChildren, $category);
            }
        } else {
            foreach ($children as $c) {
                $category = [
                    'id'           => $c->id,
                    'title'        => $c->title,
                    'slug'         => $c->slug,
                    'pillImage'    => $c->pill_image,
                    'productCount' => $inventory
                        ->productsAll()
                        ->join('category_product', 'products.id', '=', 'category_product.product_id')
                        ->join('categories', 'categories.id', '=', 'category_product.category_id')
                        ->where('category_product.category_id', '=', $c->id)
                        ->select('products.*', 'category_product.index', 'quantity')
                        ->count()
                ];

                array_push($formattedChildren, $category);
            }
        }

        return $formattedChildren;
    }
}
