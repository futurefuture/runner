<?php

namespace App\Services;

use Mailgun\Mailgun;

class ValidateEmailService
{
    protected $email;
    protected $blackList = [
        'qq.com',
        'huawei.com',
    ];

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function validate()
    {
        if ($this->validateAgainstMailgun($this->email) && $this->validateAgainstBlackList($this->email)) {
            return true;
        } else {
            return false;
        }
    }

    private function validateAgainstMailgun($email)
    {
        $mailgun = Mailgun::create(env('MAILGUN_SECRET'));

        $response = $mailgun->get('address/private/validate', [
            'address' => $email,
        ]);

        if (! $response->http_response_body->is_valid) {
            return false;
        } else {
            return true;
        }
    }

    private function validateAgainstBlackList($email)
    {
        $parts = explode('@', $email);
        $domain = array_pop($parts);

        if (in_array($domain, $this->blackList)) {
            return false;
        } else {
            return true;
        }
    }
}
