<?php

namespace App\Services;

use App\Tag;

class TagService
{
    protected $tree = [];

    protected $count = 0;

    protected $link;

    public function tags(Tag $tag)
    {
        $children          = Tag::where('id', $tag->id)->get();
        $formattedChildren = [];

        if (!$children->count()) {
            $parentCategory = Tag::find($tag->id);

            if ($parentCategory) {
                $parentChildren = Tag::where('id', $parentCategory->id)->get();

                foreach ($parentChildren as $c) {
                    $category = [
                        'id'    => $c->id,
                        'title' => $c->title,
                        'slug'  => $c->slug,
                    ];

                    array_push($formattedChildren, $category);
                }
            }
        } else {
            foreach ($children as $c) {
                $category = [
                    'id'    => $c->id,
                    'title' => $c->title,
                    'slug'  => $c->slug,
                ];

                array_push($formattedChildren, $category);
            }
        }

        return $formattedChildren;
    }
}
