<?php

namespace App\Services;

use FCM;
use App\Task;
use App\User;
use App\Order;
use Carbon\Carbon;
use Nexmo\Laravel\Facade\Nexmo;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use GuzzleHttp;

class NotificationService
{
    public function orderPlaced(Order $order)
    {
        if ($order->schedule_time) {
            $text = 'Thank you for scheduling an ordering with Runner! Close to your scheduled time, please keep your phone close by, we will be calling you if any of your products do not exist. Because the LCBO is asking our couriers to leave quickly, we will be calling once.';
        // $text = $order->user->first_name . ', thanks for ordering with Runner! Your order should be delivered on '
            //     . Carbon::parse($order->schedule_time)->format('M j') . ', ' . Carbon::parse($order->schedule_time)->format('ha') . ' - ' . Carbon::parse($order->schedule_time)->copy()->addHours(3)->format('ha');
        } else {
            $text = 'Thank you for scheduling an ordering with Runner! Close to your scheduled time, please keep your phone close by, we will be calling you if any of your products do not exist. Because the LCBO is asking our couriers to leave quickly, we will be calling once.';
            // $text = $order->user->first_name . ', thanks for ordering with Runner! Your order will be on it\'s way shortly.';
        }

        if ($order->total > 20000) {
            $text_200 = ' Your order is above $200, please have your credit card ready to show the runner on arrival.';
        } else {
            $text_200 = ' ';
        }

        $data = $text . $text_200;

        $this->sendStatusNotification($order, $data);
    }

    public function orderDispatched(Order $order)
    {
        $data = 'Your Runner is on route to pick up your product(s). Please keep your phone close by, we will be calling you if any of your products do not exist. Because the LCBO is asking our couriers to leave quickly, we will be calling once.';
        // $data = 'Your Runner is on route to pick up your product(s).  We will send you a notification when they are on their way.';

        $this->sendStatusNotification($order, $data);
    }

    public function orderOnRoute(Order $order)
    {
        $data = 'Your Runner has left the building!! They now have your order and are on their way to deliver it. Please be ready with your ID at the door';

        $this->sendStatusNotification($order, $data);
    }

    public function orderDelivered(Order $order)
    {
        $data = 'Thank you for ordering with Runner. Remember to tell your friends and get $10 OFF your next purchase!' . env('APP_URL') . '/refer-a-friend';

        $this->sendStatusNotification($order, $data);
    }

    private function sendStatusNotification(Order $order, $text)
    {
        if ($order->device === 3 && $order->user->fcm_token) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('Order ' . $order->id);
            $notificationBuilder
                ->setBody($text)
                ->setSound('default');

            // test
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'data' => [
                    'orderId' => (string) $order->id,
                    'type'    => 'order',
                ],
            ]);

            $option       = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $FCMdata      = $dataBuilder->build();
            $token        = $order->user->fcm_token;

            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $FCMdata);
            } catch (Exception $e) {
                return $e->getMessage();
            }

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
        } else {
            if (env('APP_ENV') == 'production') {
                try {
                    Nexmo::message()->send([
                        'to'    => '1' . $order->user->phone_number,
                        'from'  => '12493155516',
                        'text'  => $text,
                    ]);
                } catch (Exception $e) {
                    Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());

                    return $e->getMessage();
                }
            }
        }
    }

    public function sendSMS($mobile, $text)
    {
        if (env('APP_ENV') == 'production') {
            try {
                Nexmo::message()->send([
                'to'    => '1' . $mobile,
                'from'  => '12493155516',
                'text'  => $text,
            ]);
            } catch (Exception $e) {
                Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());
            }
        }
    }

    public function sendReferrerNotification(Order $order, $referrer)
    {
        $referrerLastOrder = Order::where('user_id', $referrer->id)
            ->orderBy('id', 'desc')
            ->first();
        $pushText = 'Congrats! You\'ve just earned 1000 Reward Points or $10 OFF your next purchase. Swipe right!';
        $smsText  = 'Congrats! Your friend ' . $order->user->first_name . ' made a purchase on Runner! You\'ve just earned 1000 Reward Points or $10 OFF your next purchase!';

        if ($referrerLastOrder->device === 3 && $referrerLastOrder->user->fcm_token) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder($order->user->first_name . ' made a purchase');
            $notificationBuilder
                ->setBody($pushText)
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'orderId' => $order->id,
            ]);

            $option       = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $FCMdata      = $dataBuilder->build();
            $token        = $referrerLastOrder->user->fcm_token;

            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $FCMdata);
            } catch (Exception $e) {
                return $e->getMessage();
            }

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
        } else {
            if (env('APP_ENV') == 'production') {
                try {
                    Nexmo::message()->send([
                    'to'    => '1' . $referrerLastOrder->user->phone_number,
                    'from'  => '12493155516',
                    'text'  => $smsText,
                ]);
                } catch (Exception $e) {
                    Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());

                    return $e->getMessage();
                }
            }
        }
    }

    public function sendReviewAcceptedNotification($review, $product, $isFirstReview)
    {
        $lastOrder = Order::where('user_id', $review->user->id)
            ->orderBy('id', 'desc')
            ->first();

        if ($isFirstReview === true) {
            $rewardPointsValue = 500;
        } else {
            $rewardPointsValue = 100;
        }

        $pushText = 'Congrats. You\'ve just earned ' . $rewardPointsValue . ' Reward Points for the awesome product review you gave. Swipe right to check it out!';
        $smsText  = 'Congrats! You\'ve just earned ' . $rewardPointsValue . ' Reward Points on Runner for the awesome product review you gave. Check it out at https://www.getrunner.io!';

        if ($lastOrder && $lastOrder->device === 3 && $review->user->fcm_token) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('Review Accepted!');
            $notificationBuilder
                ->setBody($pushText)
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'productName' => $product->name,
                'productId'   => $product->id,
            ]);

            $option       = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $FCMdata      = $dataBuilder->build();
            $token        = $review->user->fcm_token;

            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $FCMdata);
            } catch (Exception $e) {
                return $e->getMessage();
            }

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
        } else {
            if (env('APP_ENV') == 'production') {
                try {
                    Nexmo::message()->send([
                        'to'    => '1' . $review->user->phone_number,
                        'from'  => '12493155516',
                        'text'  => $smsText,
                    ]);
                } catch (Exception $e) {
                    Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());

                    return $e->getMessage();
                }
            }
        }
    }

    public function sendManualNotification($value, $reason, $userId)
    {
        $user      = User::find($userId);
        $lastOrder = Order::where('user_id', $user->id)->latest()->first();

        if ($lastOrder && $lastOrder->device === 3 && $user->fcm_token) {
            $text          = $reason;
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('You have earned ' . $value . ' Reward Points!');
            $notificationBuilder
                ->setBody('Because ' . strtolower($text))
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();

            $option       = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $FCMdata      = $dataBuilder->build();
            $token        = $user->fcm_token;

            try {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $FCMdata);
            } catch (Exception $e) {
                return $e->getMessage();
            }

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
        } else {
            $text = 'Congrats! You have earned ' . $value . ' Reward Points from Runner because ' . $reason;

            if (env('APP_ENV') == 'production') {
                try {
                    Nexmo::message()->send([
                        'to'    => '1' . $user->phone_number,
                        'from'  => '12493155516',
                        'text'  => strtolower($text),
                    ]);
                } catch (Exception $e) {
                    Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());

                    return $e->getMessage();
                }
            }
        }
    }

    // new
    public function sendCourierPushNotification(Task $task, User $courier)
    {
        $access_token = 'AAAAWkEXnDA:APA91bFT5A7wdnaqDNX3ZqpF2iqFq2Jt1KcEQsmp8Y707cx6JVNtP1a1hMTTbQ4pFRNnXgHvejJorxvM-Pl5n16DPeYgsqEI8mqdppjQM-w8SkgZZ4LCg8X40ml570lSwzQiNwAbVe0b';
        $reg_id       = $courier->fcm_token;
        $message      = [
                'notification' => [
                    'title'              => 'Hey ' . $courier->first_name . ', you have a new order!',
                    'android_channel_id' => '501'
                    // 'body'  => 'This is a test!'
                ],
                'to' => $reg_id
            ];
        $client = new GuzzleHttp\Client([
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => 'key=' . $access_token,
                ]
            ]);
        $response = $client->post(
            'https://fcm.googleapis.com/fcm/send',
            [
                'body' => json_encode($message)
            ]
        );

        echo $response->getBody();
    }
}
