<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GenderizeApi
{
    private $baseUrl = 'https://api.genderize.io';
    private $apiKey = '87f095b218dcd160655e93e5e85da24c';

    public function getGender(String $firstName)
    {
        $client = new Client();

        try {
            $response = $client->get($this->baseUrl.'?name='.$firstName.'&apikey='.$this->apiKey);
            $gender = json_decode($response->getBody())->gender;

            return $gender;
        } catch (GuzzleException $exception) {
            $responseBody = $exception->getResponse()->getBody(true);
        }
    }
}
