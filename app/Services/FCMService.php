<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;

class FCMService
{
    protected $application = 'runner';
    protected $accessToken = 'AAAA7vA_Hyk:APA91bEW62YMukvwPy-9RDYkuXzQIv_97tLW7silUGowV88OQN4v2dACEIxOah58reRt_G-xo_9_mbbB8-99UEmJ5DBM7sJh24IeYIY9LfFDwYePWiEjoUKk_6o1UqMTcr0FV9oNGUV2';

    /**
     * @param mixed $users
     */
    public function sendPushNotification(string $application, $users)
    {
        $this->application = $application;

        if ($this->application === 'runnerCourier') {
            $this->accessToken = 'AAAAWkEXnDA:APA91bFT5A7wdnaqDNX3ZqpF2iqFq2Jt1KcEQsmp8Y707cx6JVNtP1a1hMTTbQ4pFRNnXgHvejJorxvM-Pl5n16DPeYgsqEI8mqdppjQM-w8SkgZZ4LCg8X40ml570lSwzQiNwAbVe0b';
        }

        if (! is_array($users)) {
            $users = [$users];
        }

        $fcmTokens = [];

        foreach ($users as $u) {
            if ($this->application === 'runnerCourier') {
                if ($u->role != 1 && $u->role != 2) {
                    throw new Exception('user is not a runner or admin');
                }
            }

            array_push($fcmTokens, $u->fcm_token);
        }

        $message  = [
                'notification' => [
                    'title'              => 'Hey, you have a new order!',
                    'android_channel_id' => '501',
                    'body'               => 'This is a test!',
                    'click_action'       => 'RunnerProductNotification',
                    'mutable_content'    => true,
                    'icon'               => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                    'imageUrl'           => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                ],
                'data' => [
                    'imageUrl'         => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                    'data'             => [
                        'imageUrl'         => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                        'surveyId'         => '1',
                        'title'            => 'Hello',
                        'type'             => 'survey',
                    ],
                ],
                'imageUrl'           => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                'registration_ids'   => $fcmTokens
            ];
        $client = new Client([
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'key=' . $this->accessToken,
            ]
        ]);
        $response = $client->post(
            'https://fcm.googleapis.com/fcm/send',
            [
                'imageUrl'         => 'https://www.lcbo.com/content/dam/lcbo/products/000570.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg',
                'body'             => json_encode($message)
            ]
        );

        return $response;
    }
}
