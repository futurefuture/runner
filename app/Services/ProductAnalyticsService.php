<?php

namespace App\Services;

use Analytics;
use App\Partner;
use App\Product;
use App\Store;
use Carbon\Carbon;
use DB;
use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class ProductAnalyticsService
{
    protected $partnerId;
    protected $viewId;
    protected $getServiceKey;
    protected $cloud;
    protected $bigQuery;
    protected $productInfo;
    protected $productSKU;
    protected $productTitle;
    protected $productCategory;
    protected $broadCategory;
    protected $subCategory;
    protected $partner;
    protected $product;
    protected $totalProductClick           = 0;
    protected $totalUniqueClick            = 0;
    protected $totalProductAddToCart       = 0;
    protected $totalAddToCartDetail        = 0;
    protected $totalBuyToDetail            = 0;
    protected $totalProductUniquePurchases = 0;
    protected $totalQuantity               = 0;
    protected $totalRevenue                = 0;
    protected $totalShares                 = 0;
    protected $totalFavourites             = 0;
    protected $totalProductRevenue         = 0;
    protected $totalProductQuantitySold    = 0;
    protected $totalProductPurchases       = 0;

    // TODO - JAREK formating and get more paremeters dynamic which are currently static

    public function __construct($partnerId, $productId)
    {
        $this->viewId        = 138227695;
        $this->getServiceKey = base_path('service-account-credentials.json');
        $this->cloud         = new ServiceBuilder([
            'keyFilePath'                   => $this->getServiceKey,
        ]);
        $this->bigQuery        = $this->cloud->bigQuery();
        $this->partner         = Partner::find($partnerId);
        $this->product         = $productId;
        $this->productInfo     = Product::find($this->product);
        $this->productSKU      = $this->productInfo->sku;
        $this->productTitle    = $this->productInfo->title;
        $this->productCategory = Product::where('id', '=', $this->product)
            ->get()
            ->pluck('category_id')
            ->toArray();
        $this->getBroadCategoryAndSubCategory();
    }

    public function getProductAnalytics($startDate, $endDate)
    {
        $productOverallAnalytics = [];
        $broadRank               = $this->getRank($startDate, $endDate, $this->broadCategory);
        $subCategoryRank         = $this->getRank($startDate, $endDate, $this->subCategory);
        $iosStore                = $this->getMobileAnalyticData($startDate, $endDate);
        $webPageViews            = $this->getWebPageView($startDate, $endDate, $iosStore);
        $overallSales            = $this->getOverallSales($startDate, $endDate);
        $to                      = Carbon::createFromFormat('Y-m-d', $startDate);
        $from                    = Carbon::createFromFormat('Y-m-d', $endDate);
        $diffInDays              = $to->diffInDays($from);

        $analyticsByMedium   = $this->getSalesByMedium($startDate, $endDate);
        $analyticsByCampaign = $this->getSalesByCampaign($startDate, $endDate);
        $analyticsBySource   = $this->getSessionsBySource($startDate, $endDate);

        $productOverallAnalytics = [
            'runnerAnalytics'  => [
                'totalRevenue'          => $this->totalProductRevenue,
                'broadCategory'         => $broadRank,
                'subCategory'           => $subCategoryRank,
                'totalPurchases'        => $this->totalProductPurchases,
                'totalQuantity'         => $this->totalProductQuantitySold,
                'totalAveragePurchases' => ceil($this->totalProductQuantitySold / $diffInDays),
            ],
            'googleAnalytics' => [
                'totalPageViews'        => $this->totalProductClick,
                'totalUniquePageViews'  => $this->totalUniqueClick,
                'totalAddToCarts'       => $this->totalProductAddToCart,
                'totalCartToDetails'    => $this->totalAddToCartDetail,
                'totalBuyToDetails'     => $this->totalBuyToDetail,
                'totalShares'           => $this->totalShares,
                'totalFavourites'       => $this->totalFavourites,
                'totalUniquePurchases'  => $this->totalProductUniquePurchases,
                'totalQuantitySold'     => $this->totalQuantity,
                'totalRevenue'          => $this->totalRevenue,
            ],
            'storeFronts'              => $webPageViews,
            'medium'                   => $analyticsByMedium,
            'campaign'                 => $analyticsByCampaign,
            'traffic'                  => $analyticsBySource,
        ];

        return response()->json([
            'data' => [
                'type'       => 'products',
                'id'         => (string) $this->product,
                'SKU'        => $this->productSKU,
                'title'      => $this->productTitle,
                'attributes' => $productOverallAnalytics,
            ],
        ]);
    }

    /**
     * Function to return web product clicks.
     *
     * @param string $startDate
     * @param string $endDate
     * @param array $iosData
     * @return array
     */
    protected function getWebPageView($startDate, $endDate, $iosData)
    {
        $productAnalytics = [];
        $startDate        = Carbon::parse($startDate);
        $endDate          = Carbon::parse($endDate);

        Analytics::setViewId($this->viewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:Ecommerce',
            [
                'metrics'     => 'ga:ProductListClicks,ga:ProductAddsToCart',
                'dimensions'  => 'ga:productSKU,ga:dimension1',
                'filters'     => 'ga:productSKU==' . $this->product,
            ]
        );

        $this->totalProductClick += $analyticsData->totalsForAllResults['ga:ProductListClicks'];
        $this->totalUniqueClick += 20;
        $this->totalProductAddToCart += $analyticsData->totalsForAllResults['ga:ProductAddsToCart'];
        // var to add total of each ends here
        foreach ($analyticsData as $row) {
            // var to add total of each value for web
            if ($row[1] == 'undefined') {
                break;
            }
            $storeInfo                    = Store::find($row[1]);
            $webStore['pageViews']        = $row[2];
            $webStore['uniquePageViews']  = 20;
            $webStore['addToCart']        = $row[3];
            $webStore['cartToDetailRate'] = $row[2] > 0 ? ($row[3] / $row[2]) * 100 : 0;
            $webStore['shares']           = 10;
            $webStore['favourites']       = 10;
            $this->totalAddToCartDetail += $webStore['cartToDetailRate'];

            /** Get all data from database */
            $localDatabaseValues = $this->getTotalRevenue($startDate, $endDate, $row[1]);

            if (count($localDatabaseValues) > 0) {
                foreach ($localDatabaseValues as $db) {
                    if ($db->device == 1) {
                        $webStore['purchases']    = $db->purchase;
                        $webStore['quantitySold'] = $db->totalQuantity;
                        $webStore['revenue']      = $db->revenue;
                    } elseif ($db->device == 3) {
                        $iosStore['purchases']    = $db->purchase;
                        $iosStore['quantitySold'] = $db->totalQuantity;
                        $iosStore['revenue']      = $db->revenue;
                    } else {
                        $webStore['purchases']    = 0;
                        $webStore['quantitySold'] = 0;
                        $webStore['revenue']      = 0;
                        $iosStore['purchases']    = 0;
                        $iosStore['quantitySold'] = 0;
                        $iosStore['revenue']      = 0;
                    }
                }
            } else {
                $webStore['purchases']    = 0;
                $webStore['quantitySold'] = 0;
                $webStore['revenue']      = 0;
                $iosStore['purchases']    = 0;
                $iosStore['quantitySold'] = 0;
                $iosStore['revenue']      = 0;
            }

            $webStore['buyToDetailRate'] = isset($webStore['uniquePageViews']) && isset($webStore['purchases']) ? ($webStore['purchases'] / $webStore['uniquePageViews']) * 100 : 0;
            $this->totalBuyToDetail += $webStore['buyToDetailRate'];
            /* Call to function to get NoOfClicks and UniqueClicks and merge in array for a product and store */

            $iosStore['pageViews']       = $iosData[$row[1]]['pageView'] ?? 0;
            $iosStore['uniquePageViews'] = $iosData[$row[1]]['UniqueView'] ?? 0;
            $iosStore['addToCart']       = $iosData[$row[1]]['addToCart'] ?? 0;

            $this->totalProductClick += $iosStore['pageViews'];
            $this->totalUniqueClick += $iosStore['uniquePageViews'];
            $this->totalProductAddToCart += $iosStore['addToCart'];

            $iosStore['shares']     = 10;
            $iosStore['favourites'] = 10;

            $iosStore['cartToDetailRate'] = $iosStore['pageViews'] ? ($iosStore['addToCart'] / $iosStore['pageViews']) * 100 : 0;
            $this->totalAddToCartDetail += $iosStore['cartToDetailRate'];

            $iosStore['buyToDetailRate'] = $iosStore['uniquePageViews'] > 0 && isset($iosStore['purchases']) ? ($iosStore['purchases'] / $iosStore['uniquePageViews']) * 100 : 0;
            $this->totalBuyToDetail += $iosStore['buyToDetailRate'];

            $productAnalytics[$row[1]] = [
                'store'  => [
                    'title' => $storeInfo->title,
                    'logo'  => $storeInfo->options['storeLogo'],
                ],
                'pageViews' => [
                    'web'  => $webStore['pageViews'] ?? 0,
                    'ios'  => $iosStore['pageViews'],
                ],
                'uniquePageViews' => [
                    'web'   => 20,
                    'ios'   => $iosStore['uniquePageViews'],
                ],
                'addToCarts' => [
                    'web'   => $webStore['addToCart'] ?? 0,
                    'ios'   => $iosStore['addToCart'],
                ],
                'cartToDetailRate' => [
                    'web'  => $webStore['cartToDetailRate'] ?? 0,
                    'ios'  => $iosStore['cartToDetailRate'] ?? 0,
                ],
                'buyToDetailRate'  => [
                    'web'  => $webStore['buyToDetailRate'] ?? 0,
                    'ios'  => $iosStore['buyToDetailRate'] ?? 0,
                ],
                'shares' => [
                    'web'  => $webStore['shares'] ?? 0,
                    'ios'  => $iosStore['shares'] ?? 0,
                ],
                'favourites' => [
                    'web'  => $webStore['favourites'] ?? 0,
                    'ios'  => $iosStore['favourites'] ?? 0,
                ],
                'purchases' => [
                    'web'  => $webStore['purchases'] ?? 0,
                    'ios'  => $iosStore['purchases'] ?? 0,
                ],
                'quantitySold' => [
                    'web'  => $webStore['quantitySold'] ?? 0,
                    'ios'  => $iosStore['quantitySold'] ?? 0,
                ],
                'revenue' => [
                    'web'  => $webStore['revenue'] ?? 0,
                    'ios'  => $iosStore['revenue'] ?? 0,
                ],
            ];
        }

        return $productAnalytics;
    }

    protected function getAllViewId()
    {
        $viewId = Partner::where('ga_view_id', '!=', null)->get()->pluck('store_id', 'ga_view_id')->toArray();

        return $viewId;
    }

    // TODO - JAREK find a way where I can query both event in one and get clicks
    protected function getMobileAnalyticData($startDate, $endDate)
    {
        $featureQuery = 'SELECT SUM(clicks) as totalClicks,count(*) as uniqueClick,storeId from (Select count(*) as clicks,app_instance,storeId  from `runner-221bc.analytics_151500901.cluster_productClick` where productId =' . $this->product . ' and event_name IN("product_in_category_tapped","featured_product_tapped") and event_timestamp between ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' group by app_instance,storeId) group by storeId order by storeId';

        $queryJobConfig = $this->bigQuery->query($featureQuery);

        $queryResults = $this->bigQuery->runQuery($queryJobConfig);

        $iosData = [];
        foreach ($queryResults as $row) {
            $iosData[$row['storeId']] = [
                'pageView'   => $row['totalClicks'],
                'UniqueView' => $row['uniqueClick'],
            ];
        }

        return $this->getMobileAddToCart($startDate, $endDate, $iosData);
    }

    // TODO - JAREK find a way to merge all three query in one
    protected function getMobileAddToCart($startDate, $endDate, $iosData)
    {
        $productAddToCartQuery = 'Select storeId,count(*) as addToCart  from `runner-221bc.analytics_151500901.cluster_productCart` where productId =' . $this->product . ' and event_name IN("product_added_to_cart","add_to_cart_from_product_tapped","featured_product_added_to_cart") and event_timestamp between ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' group by storeId order by storeId';

        $queryProductAddToCartQuery = $this->bigQuery->query($productAddToCartQuery);
        $queryProductResults        = $this->bigQuery->runQuery($queryProductAddToCartQuery);
        $cartStore                  = [];
        foreach ($queryProductResults as $cart) {
            $cartStore[] = $cart;
        }

        $cartStore;
        foreach ($cartStore as $arr) {
            foreach ($iosData as $key => $row[1]) {
                if ($arr['storeId'] == $key) {
                    $iosData[$key]['addToCart'] = $arr['addToCart'];
                }
            }
        }

        return $iosData;
    }

    protected function getTotalRevenue($startDate, $endDate, $storeId)
    {
        $startDate = $startDate->format('Y-m-d');
        $endDate   = $endDate->format('Y-m-d');

        $orders = DB::select('SELECT order_items.product_id,o.device,SUM(order_items.quantity) as totalQuantity, SUM(order_items.quantity*order_items.retail_price) as revenue,COUNT(*) as purchase from order_items as order_items inner join orders as o on o.id = order_items.order_id where order_items.product_id =' . $this->product . ' and order_items.created_at between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and o.store_id =' . $storeId . ' group by o.device');

        $totOrders = DB::select('SELECT SUM(order_items.quantity) as totalQuantity, SUM(order_items.quantity*order_items.retail_price) as revenue,COUNT(*) as purchase from order_items as order_items inner join orders as o on o.id = order_items.order_id where order_items.product_id =' . $this->product . ' and order_items.created_at between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and o.device !=0 and o.store_id =' . $storeId);

        if ($totOrders) {
            $this->totalProductUniquePurchases += $totOrders[0]->purchase;
            $this->totalQuantity += $totOrders[0]->totalQuantity;
            $this->totalRevenue += $totOrders[0]->revenue;
        }

        return $orders;
    }

    protected function getOverallSales($startDate, $endDate)
    {
        /** DB raw query instead of eloquent */
        $orderItems = DB::select('Select SUM(quantity) as totQuantity,SUM(quantity * retail_price) as totRevenue,count(*) as purchases from order_items where product_id =' . $this->product . ' and created_at between' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'");

        $this->totalProductRevenue += $orderItems[0]->totRevenue;
        $this->totalProductQuantitySold += $orderItems[0]->totQuantity;
        $this->totalProductPurchases = $orderItems[0]->purchases;
    }

    protected function getRank($startDate, $endDate, $categoryId)
    {
        $rank = DB::select('SELECT product,rank from(Select  product,totRevenue,@curRank := @curRank + 1 AS rank from(SELECT product_id as product,ROUND(SUM(quantity*retail_price)/100,2) as totRevenue FROM order_items WHERE created_at between ' . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 on e1.id = e.parent_id WHERE ' . $categoryId . ' IN(r.parent_id,e.parent_id,e1.parent_id) UNION SELECT id FROM categories WHERE id =' . $categoryId . ') group by product_id order by totRevenue desc) as test,(SELECT @curRank := 0) r ) as rank where product=' . $this->product);

        return  $rank ? $rank[0]->rank : 0;
    }

    protected function getBroadCategoryAndSubCategory()
    {
        $cat = DB::select('select  c1.id as category_id,c1.title,c1.parent_id as subCategory ,c2.parent_id as broadCategory from   categories c1
        left join categories  c2 on c2.id = c1.parent_id 
        left join  categories c3 on c3.id = c2.parent_id where c1.id =' . $this->productCategory[0]);

        $this->broadCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->subCategory : $cat[0]->broadCategory;
        $this->subCategory   = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->category_id : $cat[0]->subCategory;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getSalesByMedium($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $partner  = Partner::find($this->partner->id);
        $gaViewId = $partner->ga_view_id;

        // set google analytics view
        Analytics::setViewId($gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:itemRevenue',
            [
                'metrics'     => 'ga:itemRevenue',
                'dimensions'  => 'ga:medium',
                'sort'        => '-ga:itemRevenue',
                'filters'     => 'ga:productSKU==' . $this->product,
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getSalesByCampaign($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $partner  = Partner::find($this->partner->id);
        $gaViewId = $partner->ga_view_id;

        // set google analytics view
        Analytics::setViewId($gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:itemRevenue',
            [
                'metrics'     => 'ga:itemRevenue',
                'dimensions'  => 'ga:campaign',
                'sort'        => '-ga:itemRevenue',
                'filters'     => 'ga:productSKU==' . $this->product,
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    protected function getSessionsBySource($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);
        $endDate   = Carbon::parse($endDate);

        $partner  = Partner::find($this->partner->id);
        $gaViewId = $partner->ga_view_id;

        // set google analytics view
        Analytics::setViewId($gaViewId);

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics'     => 'ga:sessions',
                'dimensions'  => 'ga:source',
                'sort'        => '-ga:sessions',
                'filters'     => 'ga:productSKU==' . $this->product,
                'max-results' => 5,
            ]
        );

        return $analyticsData->rows;
    }
}
