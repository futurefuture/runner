<?php

namespace App\Services;

use App\Ad;
use Segment;
use Exception;
use Illuminate\Support\Facades\Log;

class SegmentService
{
    public function __construct()
    {
        Segment::init(env('SEGMENT_KEY'));
    }

    public function trackLoggedIn($user, $anonymousId)
    {
        try {
            Segment::track([
                'userId'      => (string) $user->id,
                'event'       => 'Logged In',
                'properties'  => [
                    'anonymousId' => $anonymousId,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());
        }
    }

    public function trackSignedUp($user, $anonymousId)
    {
        try {
            Segment::track([
                'userId'     => (string) $user->id,
                'event'      => 'Registered',
                'properties' => [
                    'anonymousId' => $anonymousId,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());
        }
    }

    public function trackOrderCompleted($user, $products, $order, $store, $quantity, $device, $anonymousId, $inventoryId)
    {
        $splitPostalCode = substr($order->postal_code, 0, 3);

        try {
            Segment::track([
                'userId'     => (string) $user->id,
                'event'      => 'Order Completed',
                'properties' => [
                    'anonymousId' => strtolower($anonymousId),
                    'checkoutId'  => (string) rand(10, 21),
                    'orderId'     => (string) $order->id,
                    'affiliation' => $store->title,
                    'total'       => $order->total,
                    'quantity'    => $quantity,
                    'revenue'     => $order->subtotal,
                    'cogs'        => $order->cogs,
                    'shipping'    => $order->delivery,
                    'discount'    => $order->discount,
                    'products'    => $products,
                    'storeId'     => (string) $store->id,
                    'inventoryId' => (string) $inventoryId,
                    'device'      => $device,
                    'addressId'   => (string) $order->address_id,
                    'postalCode'  => $splitPostalCode,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());

            return $e->getMessage();
        }
    }

    public function trackProductAdded($user, $product, $cart, $category, $quantity, $couponCode, $storeId, $inventoryId, $anonymousId)
    {
        $partnerIds = [];

        foreach ($product->partners as $p) {
            array_push($partnerIds, (string) $p->id);
        }

        try {
            Segment::track([
                'userId'     => (string) $user->id,
                'event'      => 'Product Added',
                'properties' => [
                    'cartId'      => (string) $cart->id,
                    'anonymousId' => $anonymousId,
                    'productId'   => (string) $product->id,
                    'sku'         => $product->sku,
                    'category'    => $category->title,
                    'name'        => $product->title,
                    'brand'       => $product->producer,
                    'variant'     => $product->packaging,
                    'price'       => (float) number_format($product->runner_price / 100, 2, '.', ''),
                    'quantity'    => $quantity,
                    'coupon'      => $couponCode ?? null,
                    'storeId'     => (string) $storeId,
                    'inventoryId' => (string) $inventoryId,
                    'partnerIds'  => $partnerIds,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());
        }
    }

    public function trackProductRemoved($user, $product, $cart, $category, $quantity, $couponCode, $storeId, $inventoryId, $anonymousId)
    {
        $partnerIds = [];

        foreach ($product->partners as $p) {
            array_push($partnerIds, (string) $p->id);
        }

        try {
            Segment::track([
                'userId'     => (string) $user->id,
                'event'      => 'Product Removed',
                'properties' => [
                    'cartId'      => (string) $cart->id,
                    'anonymousId' => $anonymousId,
                    'productId'   => (string) $product->id,
                    'sku'         => $product->sku,
                    'category'    => $category->title,
                    'name'        => $product->title,
                    'brand'       => $product->producer,
                    'variant'     => $product->packaging,
                    'price'       => (float) number_format($product->runner_price / 100, 2, '.', ''),
                    'quantity'    => $quantity,
                    'coupon'      => $couponCode ?? null,
                    'storeId'     => (string) $storeId,
                    'inventoryId' => (string) $inventoryId,
                    'partnerIds'  => $partnerIds,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());
        }
    }

    // ad hits
    public function trackAdImpression(Ad $ad, $anonymousId = null)
    {
        try {
            Segment::track([
                'userId'     => 4,
                'event'      => 'Ad Impression',
                'properties' => [
                    'anonymousId'         => $anonymousId,
                    'adId'                => (string) $ad->id,
                    'adTypeId'            => (string) $ad->ad_type_id,
                    'products'            => $ad->products,
                    'partnerId'           => (string) $ad->partner_id,
                    'campaignId'          => (string) $ad->campaign_id,
                    'content'             => $ad->content,
                    'productId'           => $ad->product_id,
                    'categoryId'          => $ad->category_id,
                    'tagId'               => $ad->tag_id,
                    'carouselId'          => $ad->carousel_id,
                    'carouselComponentId' => $ad->carousel_component_id,
                    'bidType'             => $ad->bid_type,
                    'bidAmount'           => (float) number_format($ad->bid_amount / 100, 2, '.', ''),
                    'index'               => $ad->index,
                    'startDate'           => $ad->start_date,
                    'endDate'             => $ad->end_date,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Event track doesnt work because of ' . $e->getMessage());
        }
    }
}
