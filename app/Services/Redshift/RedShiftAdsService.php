<?php

namespace App\Services\Redshift;

use App\Partner;
use App\Campaign;
use App\Ad;
use App\Http\Resources\Partner\Ad as AdResource;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RedShiftAdsService
{
    protected $from;
    protected $to;
    protected $diffInDays;
    protected $partner;
    protected $campaignId;

    public function __construct($partner, $campaign, $startDate, $endDate)
    {
        $this->from       = Carbon::parse($startDate)->addHour(5);
        $this->to         = Carbon::parse($endDate)->addHour(6);
        $this->diffInDays = $this->to->diffInDays($this->from);
        $this->partner    = $partner;
        $this->partnerId  = $partner->id;
        $this->campaign   = $campaign;
        $this->campaignId = $campaign->id;
    }

    /**
     * Returns all ads data for given campaign.
     */
    public function getAdsInformation()
    {
        $query      = 'SELECT ad_id, sum(ad_clicks) AS ad_clicks, ad_impressions FROM(SELECT jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, TO_CHAR(jsac.start_date, \'YYYY-MM-DD HH:MM:SS\') AS start_date, TO_CHAR(jsac.end_date, \'YYYY-MM-DD HH:MM:SS\') AS end_date, jsac.index, jsac.bid_amount, COUNT(phpai.uuid) AS ad_impressions FROM(SELECT ad_id, COUNT(uuid) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, index, bid_amount FROM javascript_production.ad_clicked WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND campaign_id = ' . $this->campaignId . ' GROUP BY ad_id, ad_type_id, start_date, end_date, INDEX, bid_amount, carousel_id) AS jsac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id = jsac.ad_Id GROUP BY jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, jsac.start_date, jsac.end_date, jsac.bid_amount, jsac.index UNION SELECT STRTOL(iosac.ad_id,10) AS ad_id, ad_clicks, STRTOL(iosac.carousel_id, 10) AS carousel_id, STRTOL(iosac.ad_type_id, 10) AS ad_type_id, iosac.start_date, iosac.end_date, STRTOL(iosac.index,10) AS INDEX, STRTOL(iosac.bid_amount, 10) AS bid_amount, COUNT(phpai.uuid) AS ad_impressions FROM(SELECT ad_id, COUNT(campaign_id) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, INDEX, bid_amount FROM(SELECT JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'id\', TRUE) AS ad_id,JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'campaign_id\', TRUE) AS campaign_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'carousel_id\', TRUE) AS carousel_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'ad_type_id\', TRUE) AS ad_type_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'start_date\', TRUE) AS start_date,JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'end_date\', TRUE) AS end_date, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'index\', TRUE) AS INDEX, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'bid_amount\', TRUE) AS bid_amount FROM seq_0_to_20 AS seq, ios_production.ad_clicked WHERE seq.i < JSON_ARRAY_LENGTH(params_ads) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND campaign_id = ' . $this->campaignId . ') GROUP BY ad_id, ad_type_id, carousel_id, start_date, end_date, INDEX, bid_amount) AS iosac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id = iosac.ad_id GROUP BY iosac.ad_id, iosac.ad_clicks, iosac.carousel_id, iosac.ad_type_id, iosac.start_date, iosac.end_date, iosac.bid_amount, iosac.index) GROUP BY ad_id, ad_impressions';

        $adInfo  = DB::connection('pgsql')->select($query);

        $formattedAdInfo = [];
        $returnedArray = [];

        foreach ($adInfo as $ad) {
            $adModel = Ad::where('id', $ad->ad_id)->first()->toArray() ?? null;

            $atcQuery = 'SELECT sum(adds_to_cart) FROM(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND campaign_id = ' . $this->campaignId . ' AND ad_id = ' . $ad->ad_id . ' AND product_id IS NOT NULL) AS jsai INNER JOIN(SELECT product_id, adds_to_cart FROM(SELECT product_id, COUNT(*) AS adds_to_cart FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner, product_id, uuid, anonymous_id, user_id, timestamp::date FROM php_production.product_added, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids)) WHERE partner = ' . $this->partnerId . '  AND timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id)) AS total_atc ON total_atc.product_id = jsai.product_id';

            $atc  = DB::connection('pgsql')->select($atcQuery);

            $conversionQuery = 'SELECT count(*) as total_conversion FROM(SELECT product_id FROM(SELECT JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products,seq2.i), \'productId\') AS product_id, timestamp::date FROM php_production.order_completed, seq_0_to_20 AS seq2 WHERE seq2.i < JSON_ARRAY_LENGTH(products))WHERE timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') AS completed_orders INNER JOIN(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND campaign_id = ' . $this->campaignId . ' AND ad_id = ' . $ad->ad_id . ' AND product_id IS NOT NULL) AS partner_products ON completed_orders.product_id=partner_products.product_id';

            $conversion  = DB::connection('pgsql')->select($conversionQuery);

            $revenueQuery = 'SELECT SUM(priceTotal) FROM(SELECT product_id, price * quantity AS priceTotal FROM(SELECT JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq2.i), \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq2.i),\'price\') AS price, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq2.i), \'quantity\') AS quantity, timestamp::date FROM php_production.order_completed, seq_0_to_20 AS seq2 WHERE seq2.i < JSON_ARRAY_LENGTH(products)) WHERE timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' ) AS completed_orders INNER JOIN(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND campaign_id = ' . $this->campaignId . ' AND ad_id = ' . $ad->ad_id . ' AND product_id IS NOT NULL) AS partner_products ON completed_orders.product_id = partner_products.product_id';

            $revenue = DB::connection('pgsql')->select($revenueQuery);

            if ($adModel['partner_id'] == $this->partnerId) {
                $formattedAdInfo['adClicks'] = $ad->ad_clicks;
                $formattedAdInfo['adImpressions'] = $ad->ad_impressions;
                $formattedAdInfo['adATC'] = $atc[0]->sum ?? 0;
                $formattedAdInfo['adConversion'] = $conversion[0]->total_conversion ?? 0;
                $formattedAdInfo['adCPM'] =  $ad->ad_impressions ? ((($adModel['budget'] / 100) * 1000) / $ad->ad_impressions) : 0;
                $formattedAdInfo['adCPC'] =  $atc[0]->sum  > 0 ? ((($adModel['budget'] / 100)) / $atc[0]->sum ?? 0) : 0;
                $formattedAdInfo['adRevenue'] = ((int) ($revenue[0]->sum)) / 100 ?? 0;
                $formattedAdInfo['ROAS'] = $adModel['budget'] / 100 ? ((int) ($revenue[0]->sum)) / 100 ?? 0 / $adModel['budget']  : 0;
                $formattedAdInfo['ad'] = new AdResource(Ad::where('id', $ad->ad_id)->first()) ?? null;

                array_push($returnedArray, $formattedAdInfo);
            }
        }
        if ($returnedArray) {
            return [
                'type'       => 'ads',
                'attributes' => $returnedArray
            ];
        } else {
            return [
                'type'       => 'ads',
                'attributes' => null
            ];
        }
    }


    /**
     * Returns all ads data for given campaign.
     */
    public function getAdsTotalInformation()
    {

        $query      = 'SELECT ad_id, sum(ad_clicks) AS ad_clicks, ad_impressions FROM(SELECT jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, to_char(jsac.start_date,\'YYYY-MM-DD HH:MM:SS\')AS start_date, to_char(jsac.end_date,\'YYYY-MM-DD HH:MM:SS\') AS end_date,jsac.index,jsac.bid_amount,count(phpai.uuid) AS ad_impressions FROM(SELECT ad_id,count(uuid) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, index, bid_amount FROM javascript_production.ad_clicked WHERE received_at BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND campaign_id=' . $this->campaignId . ' GROUP BY ad_id, ad_type_id, start_date, end_date, INDEX, bid_amount, carousel_id) AS jsac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id=jsac.ad_id GROUP BY jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, jsac.start_date, jsac.end_date, jsac.bid_amount, jsac.index UNION SELECT strtol(iosac.ad_id,10)AS ad_id, ad_clicks, strtol(iosac.carousel_id,10) AS carousel_id, strtol(iosac.ad_type_id, 10) AS ad_type_id,iosac.start_date, iosac.end_date, strtol(iosac.index,10)AS INDEX, strtol(iosac.bid_amount, 10)AS bid_amount,count(phpai.uuid)AS ad_impressions FROM(SELECT ad_id, count(campaign_id) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, INDEX, bid_amount FROM(SELECT json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'id\',TRUE) AS ad_id,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'campaign_id\',TRUE) AS campaign_id ,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i), \'carousel_id\',TRUE) AS carousel_id ,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i), \'ad_type_id\',TRUE) AS ad_type_id ,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'start_date\',TRUE) AS start_date ,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'end_date\',TRUE) AS end_date ,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'index\',TRUE) AS INDEX,json_extract_path_text(json_extract_array_element_text(params_ads, seq.i),\'bid_amount\',TRUE) AS bid_amount FROM seq_0_to_20 AS seq,ios_production.ad_clicked WHERE seq.i<JSON_ARRAY_LENGTH(params_ads) AND received_at BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND campaign_id=' . $this->campaignId . ')GROUP BY ad_id, ad_type_id, carousel_id, start_date, end_date, INDEX, bid_amount) AS iosac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id=iosac.ad_id GROUP BY iosac.ad_id, iosac.ad_clicks, iosac.carousel_id, iosac.ad_type_id, iosac.start_date, iosac.end_date, iosac.bid_amount, iosac.index)GROUP BY ad_id, ad_impressions';
        $adInfo  = DB::connection('pgsql')->select($query);

        $formattedAdInfo = [
            'totalAdClicks'         => 0,
            'totalBudget'           => 0,
            'totalAdImpressions'    => 0,
            'totalATC'              => 0,
            'totalConversion'       => 0,
            'totalRevenue'          => 0,
        ];

        foreach ($adInfo as $ad) {
            $adModel = Ad::where('id', $ad->ad_id)->first()->toArray() ?? null;
            $atcQuery = 'SELECT sum(adds_to_cart) FROM(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND campaign_id=' . $this->campaignId . ' AND ad_id=' . $ad->ad_id . ' AND product_id IS NOT NULL)AS jsai INNER JOIN(SELECT product_id,adds_to_cart FROM(SELECT product_id,count(*)AS adds_to_cart FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids,seq.i)AS partner,product_id,uuid,anonymous_id,user_id,timestamp::date FROM php_production.product_added,seq_0_to_20 AS seq WHERE seq.i<JSON_ARRAY_LENGTH(partner_ids))WHERE partner=' . $this->partnerId . '  AND timestamp::date BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . 'GROUP BY product_id))AS total_atc ON total_atc.product_id=jsai.product_id';
            $atc  = DB::connection('pgsql')->select($atcQuery);
            $conversionQuery = 'SELECT count(*) as total_conversion FROM(SELECT product_id FROM(SELECT json_extract_path_text(json_extract_array_element_text(products,seq2.i), \'productId\')AS product_id,timestamp::date FROM php_production.order_completed,seq_0_to_20 AS seq2 WHERE seq2.i<JSON_ARRAY_LENGTH(products))WHERE timestamp::date BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')as completed_orders INNER JOIN(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND campaign_id=' . $this->campaignId . ' AND ad_id=' . $ad->ad_id . ' AND product_id IS NOT NULL)as partner_products ON completed_orders.product_id=partner_products.product_id';
            $conversion  = DB::connection('pgsql')->select($conversionQuery);
            $revenueQuery = 'SELECT sum(priceTotal)FROM(SELECT product_id,price * quantity as priceTotal FROM(SELECT json_extract_path_text(json_extract_array_element_text(products,seq2.i),\'productId\')AS product_id,json_extract_path_text(json_extract_array_element_text(products,seq2.i),\'price\')AS price, json_extract_path_text(json_extract_array_element_text(products,seq2.i),\'quantity\')AS quantity, timestamp::date FROM php_production.order_completed,seq_0_to_20 AS seq2 WHERE seq2.i<JSON_ARRAY_LENGTH(products))WHERE timestamp::date BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' )as completed_orders INNER JOIN(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '  AND campaign_id=' . $this->campaignId . ' AND ad_id=' . $ad->ad_id . ' AND product_id IS NOT NULL)as partner_products ON completed_orders.product_id=partner_products.product_id';
            $revenue = DB::connection('pgsql')->select($revenueQuery);
            if ($adModel['partner_id'] == $this->partnerId) {
                $formattedAdInfo['totalAdClicks'] += $ad->ad_clicks ?? 0;
                $formattedAdInfo['totalBudget'] += $adModel ? $adModel['budget'] : 0;
                $formattedAdInfo['totalAdImpressions'] += $ad->ad_impressions ?? 0;
                $formattedAdInfo['totalATC']        += $atc[0]->sum ?? 0;
                $formattedAdInfo['totalConversion'] += $conversion[0]->total_conversion;
                $formattedAdInfo['totalRevenue']    += $revenue[0]->sum / 100;
            }
        }
        $formattedAdInfo['totalBudget']     = $formattedAdInfo['totalBudget'] / 100;
        $formattedAdInfo['ROAS']            = $formattedAdInfo['totalBudget'] ? $formattedAdInfo['totalRevenue'] / $formattedAdInfo['totalBudget'] : 0;
        return [
            'type'       => 'ads',
            'attributes' => $formattedAdInfo
        ];
    }
}
