<?php

namespace App\Services\Redshift;

use Analytics;
use App\Category;
use App\Partner;
use App\Product;
use App\Store;
use Carbon\Carbon;
use DB;
use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class RedShiftProductInfoOptimisedService
{
    protected $partnerId;
    protected $from;
    protected $to;
    protected $partner;
    protected $product;
    protected $productInfo;
    protected $slug;
    protected $productTitle;
    protected $productCategory;
    protected $subCategory;
    protected $subCategoryTitle;
    protected $broadCategory;
    protected $broadCategoryTitle;
    protected $productSKU;
    protected $totalProductClick           = 0;
    protected $totalUniqueClick            = 0;
    protected $totalProductAddToCart       = 0;
    protected $totalAddToCartDetail        = 0;
    protected $totalBuyToDetail            = 0;
    protected $totalProductUniquePurchases = 0;
    protected $totalQuantity               = 0;
    protected $totalRevenue                = 0;
    protected $totalShares                 = 0;
    protected $totalFavourites             = 0;
    protected $totalProductRevenue         = 0;
    protected $totalProductQuantitySold    = 0;
    protected $totalProductPurchases       = 0;
    protected $storearr                    = [];
    public $phpdatabase                    = 0;
    public $iosdatabase                    = 0;
    public $javascriptdb                   = 0;
    protected $age0;
    protected $age18                       = 0;
    protected $age25                       = 0;
    protected $age35                       = 0;
    protected $age45                       = 0;
    protected $age55                       = 0;
    protected $age65                       = 0;

    public function __construct($partnerId, $productId, $startDate, $endDate)
    {
        $this->from            = Carbon::parse($startDate)->addHour(5);
        $this->to              = Carbon::parse($endDate)->addHour(6);
        $this->partner         = Partner::find($partnerId);
        $this->product         = $productId;
        $this->productInfo     = Product::find($this->product);
        $this->productSKU      = $this->productInfo->sku;
        $this->slug            = $this->productInfo->slug;
        $this->productTitle    = $this->productInfo->title;
        $this->productCategory = Product::where('id', '=', $this->product)
            ->get()
            ->pluck('category_id')
            ->toArray();
        $this->getBroadCategoryAndSubCategory();
        $this->totalProductClick     = 0;
        $this->totalProductAddToCart = 0;
        $this->phpdatabase           = 'php_production';
        $this->iosdatabase           = 'ios_production';
        $this->javascriptdb          = 'javascript_production';
    }

    public function getSubRank()
    {
        $subRankArray = [];

        $subRank                  = $this->getRank($this->subCategory);
        $subRankArray['subTitle'] = $this->subCategoryTitle;
        $subRankArray['rank']     = $subRank;

        return $subRankArray;
    }

    public function getBroadRank()
    {
        $broadRankArray = [];
        $broadRank      = $this->getRank($this->broadCategory);

        $broadRankArray['broadTitle'] = $this->broadCategoryTitle;
        $broadRankArray['rank']       = $broadRank;

        return $broadRankArray;
    }

    protected function getBroadCategoryAndSubCategory()
    {
        // TODO once there is table on rds
        $cat = DB::select('select  c1.id as category_id,c1.title,c1.parent_id as subCategory ,c2.parent_id as broadCategory from   categories c1
        left join categories  c2 on c2.id = c1.parent_id 
        left join  categories c3 on c3.id = c2.parent_id where c1.id =' . $this->productCategory[0]);

        $this->broadCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->subCategory : $cat[0]->broadCategory;

        if ($this->broadCategory != null) {
            $this->broadCategoryTitle = Category::find($this->broadCategory)->title;
        } else {
            $this->broadCategoryTitle = 0;
        }
        $this->subCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->category_id : $cat[0]->subCategory;

        if ($this->subCategory != null) {
            $this->subCategoryTitle = Category::find($this->subCategory)->title;
        } else {
            $this->subCategoryTitle = 0;
        }
    }

    protected function getRank($categoryId)
    {
        if ($categoryId != null) {
            $query = 'SELECT product,rank from(Select  product,totRevenue,@curRank := @curRank + 1 AS rank from(SELECT product_id as product,ROUND(SUM(quantity*retail_price)/100,2) as totRevenue FROM order_items WHERE date(created_at) between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 on e1.id = e.parent_id WHERE ' . $categoryId . ' IN(r.parent_id,e.parent_id,e1.parent_id) UNION SELECT id FROM categories WHERE id =' . $categoryId . ') group by product_id order by totRevenue desc) as test,(SELECT @curRank := 0) r ) as rank where product=' . $this->product;

            $rank = DB::select($query);

            return  $rank ? $rank[0]->rank : 0;
        } else {
            return 0;
        }
    }

    public function getTotalPurchases()
    {
        $query = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM  php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $productPurchases = DB::connection('pgsql')->select($query);

        foreach ($productPurchases as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $productPurchases;
    }

    public function getTotalQuantity()
    {
        // TODO once IOS table is ready add that value into it
        $query = "SELECT count(*),product_id,SUM(quantity) as quantity,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $quantitySold = DB::connection('pgsql')->select($query);

        foreach ($quantitySold as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $quantitySold;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByMedium()
    {
        $emailQuery  = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'email' and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $salesByEmail = DB::connection('pgsql')->select($emailQuery);

        $digitialQuery = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'Digital' and timestamp between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,timestamp )';

        $digitialQuery  = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'Digital' and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $noneQuery = "select SUM(totalSales) as noneSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . '.pages where context_campaign_medium is null  and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path like '%/product/" . $this->slug . "%' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);
        $salesByNone     = DB::connection('pgsql')->select($noneQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
            'none'     => $salesByNone[0]->nonesales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByCampaign()
    {
        $productSlug = Product::find($this->product)->slug;

        $topClickCampaignQuery = "select context_campaign_name,count(*),split_part(context_page_path, '/', 3) as slug from javascript_production.pages  where context_campaign_name is not null and slug = " . "'" . $productSlug . "'" . ' and received_at  between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by context_campaign_name,slug order by count(*) desc ';

        $topCampaignName        = DB::connection('pgsql')->select($topClickCampaignQuery);
        $formattedCampaignSales = [];
        foreach ($topCampaignName as $campaign) {
            // get orders  by each campaign name
            $orderQuery = "select json_extract_path_text(product, 'productId') as product_id,SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales from (select anonymous_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in (select DISTINCT anonymous_id from javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') and  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and context_page_path =' . "'/product/" . $productSlug . "'" . ') ) where product_id = ' . $this->product . ' group by product_id';

            $addTocartQuery = 'select count(*) from php_production.product_added where received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in(select DISTINCT anonymous_id from  javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and path =' . "'/product/" . $productSlug . "'" . ')) and product_id = ' . $this->product . '';

            $addToCart = DB::connection('pgsql')->select($addTocartQuery);

            $getSalesByCampaign = DB::connection('pgsql')->select($orderQuery);

            $transactionQuery = "select count(*) from(select json_extract_path_text(product, 'productId') as product_id,SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,order_id from (select order_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in (select DISTINCT anonymous_id from javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') and  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and context_page_path =' . "'/product/" . $productSlug . "'" . ') ) where product_id = ' . $this->product . ' group by product_id,order_id)';

            $sourceQuery = 'select Distinct context_campaign_source from javascript_production.pages where  context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '';

            $sourceQuery = DB::connection('pgsql')->select($sourceQuery);

            $totalTransactionQuery        = DB::connection('pgsql')->select($transactionQuery);
            $campaignSales['source']      = $sourceQuery[0]->context_campaign_source;
            $campaignSales['name']        = $campaign->context_campaign_name;
            $campaignSales['sales']       = $getSalesByCampaign[0]->totalsales ?? 0;
            $campaignSales['addToCart']   = $addToCart[0]->count ?? 0;
            $campaignSales['transaction'] = $totalTransactionQuery[0]->count ?? 0;
            $campaignSales['pageViews']   = $campaign->count ?? 0;
            array_push($formattedCampaignSales, $campaignSales);
        }

        return $formattedCampaignSales;
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSessionsBySource()
    {
        $query = 'select context_campaign_source,count(*) from(select context_campaign_source,context_page_path,count(*),anonymous_id from ' . $this->javascriptdb . '.pages where received_at  between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " and context_page_path like '%/product/" . $this->slug . "%' group by context_campaign_source,context_page_path,anonymous_id) group by context_campaign_source";

        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                if ($medium->context_campaign_source == null) {
                    $salesSource['none'] = $medium->count;
                } else {
                    $salesSource[$medium->context_campaign_source] = $medium->count;
                }
            }

            return $salesSource;
        } else {
            return;
        }
    }

    public function getProductPageViews()
    {
        $webQuery = 'SELECT count(*),product_id,store_id FROM ' . $this->javascriptdb . '.product_viewed where product_id =' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,store_id';

        $product_list_clicked_frontend_web = DB::connection('pgsql')->select($webQuery);

        $iosQuery = 'SELECT count(*),params_product_id as product_id,params_store_id as store_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id =' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and params_product_id =' . $this->product . ' group by params_product_id,params_store_id';

        $product_list_clicked_ios = DB::connection('pgsql')->select($iosQuery);

        return response()->json([
            'data'   => [
                'type' => 'pageviews',
                'web'  => $product_list_clicked_frontend_web,
                'ios'  => $product_list_clicked_ios
            ],
        ]);
    }

    protected function getProductUniqueClick($javascriptClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_frontend =  DB::connection('pgsql')->select('Select count(*) as uniqueView,store_id from(SELECT count(*),product_id,store_id,anonymous_id FROM ' . $this->javascriptdb . '.product_viewed where product_id = ' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . 'group by product_id,store_id,anonymous_id ) group by store_id');

        foreach ($product_unique_clicked_frontend as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $javascriptClick[$arr->store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        return $this->getAddToCart($javascriptClick);
    }

    protected function getProductUniqueIosClick($iosClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_ios =  DB::connection('pgsql')->select('Select count(*) as uniqueView,params_store_id from(SELECT count(*),params_product_id,params_store_id,anonymous_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id = ' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and params_product_id =' . $this->product . 'group by params_product_id,params_store_id,anonymous_id ) group by params_store_id');

        foreach ($product_unique_clicked_ios as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->params_store_id;
                if ($arr->params_store_id == $key) {
                    $iosClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $iosClick[$arr->params_store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        return $this->getAddToCartIos($iosClick);
    }

    protected function getAddToCart($javascriptClick)
    {
        $query = 'select anonymous_id,product_id,user_id,store_id,received_at from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,store_id,received_at from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =' . $this->partner->id . 'and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and product_id =' . $this->product . ' and anonymous_id is not null';

        $productAddToCart = DB::connection('pgsql')->select($query);
        $cartCount        = 0;
        $cartCount1       = 0;
        foreach ($productAddToCart as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                $productViewQuery = 'select count(*),product_id from javascript_production.product_viewed where anonymous_id =' . "'" . $arr->anonymous_id . "'" . ' and product_id = ' . $arr->product_id . ' and received_at =' . "'" . $arr->received_at . "'" . ' group by product_id';

                $productViewWebQuery = DB::connection('pgsql')->select($productViewQuery);

                if ($productViewWebQuery) {
                    if ($arr->store_id == $key) {
                        $cartCount                          = $cartCount + $productViewWebQuery[0]->count;
                        $javascriptClick[$key]['addToCart'] = $cartCount;
                    } else {
                        $cartCount1                                   = $cartCount1 + $productViewWebQuery[0]->count;
                        $javascriptClick[$arr->store_id]['addToCart'] = $cartCount1;
                    }
                }
            }
        }

        return $this->getFavourite($javascriptClick);
    }

    protected function getAddToCartIos($iosClick)
    {
        $query = 'select anonymous_id,product_id,user_id,store_id,received_at from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,store_id,received_at from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =' . $this->partner->id . 'and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and product_id =' . $this->product . ' and anonymous_id is not null';

        $productAddToCart = DB::connection('pgsql')->select($query);
        $cartCount        = 0;
        $cartCount1       = 0;
        foreach ($productAddToCart as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;

                $productViewQuery = 'select count(*),params_product_id from ios_production.product_viewed where anonymous_id =' . "'" . $arr->anonymous_id . "'" . ' and params_product_id = ' . $arr->product_id . ' and received_at =' . "'" . $arr->received_at . "'" . ' group by params_product_id';

                $productViewIosQuery = DB::connection('pgsql')->select($productViewQuery);

                if ($productViewIosQuery) {
                    if ($arr->store_id == $key) {
                        $cartCount                   = $cartCount + $productViewIosQuery[0]->count;
                        $iosClick[$key]['addToCart'] = $cartCount;
                    } else {
                        $cartCount1                            = $cartCount1 + $productViewIosQuery[0]->count;
                        $iosClick[$arr->store_id]['addToCart'] = $productViewIosQuery[0]->count;
                    }
                }
            }
        }

        return $iosClick;
    }

    protected function getPurchases()
    {
        $query = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM  " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $productPurchases = DB::connection('pgsql')->select($query);

        foreach ($productPurchases as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $productPurchases;
    }

    protected function getQuantity()
    {
        // TODO once IOS table is ready add that value into it
        $query = "SELECT count(*),product_id,SUM(quantity) as quantity,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $quantitySold = DB::connection('pgsql')->select($query);

        foreach ($quantitySold as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $quantitySold;
    }

    protected function getFavourite($javascriptClick)
    {
        $query = 'Select count(*),product_id,store_id from ' . $this->javascriptdb . '.product_favourited where product_id = ' . $this->product . ' group by product_id,store_id';

        $favourite = DB::connection('pgsql')->select($query);

        foreach ($favourite as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['favuorite'] = $arr->count;
                } else {
                    $javascriptClick[$arr->store_id]['favuorite'] = $arr->count;
                }
            }
        }

        return $javascriptClick;
    }

    protected function getRevenue()
    {
        $query = "SELECT count(*),product_id,SUM(quantity*retail_price) as revenue ,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,device,received_at FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $revenue = DB::connection('pgsql')->select($query);

        foreach ($revenue as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return  $revenue;
    }

    public function getTopPostalCode()
    {
        $query = "select extractCode,count(Distinct order_id) as totalOrderCount  from(select SUBSTRING(postal_code, 1, 3) as extractCode,order_id,json_extract_path_text(product, 'productId') as product_id from (select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,order_id,postal_code from php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') where product_id = ' . $this->product . ') GROUP BY extractCode order by totalOrderCount desc limit 5';

        $topPostalCode = DB::connection('pgsql')->select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    /**
     * Returns all analytics data by gender.
     * @param Request $request
     * @param int $partnerId
     * @return json

    public function getSalesByGender($this->from, $this->to)
    {

        $maleSale   = 0;
        $femaleSale = 0;

        $productSlug = Product::find($this->product)->slug;

        $webQuery = "select count(*),gender,anonymous_id from javascript_production.identifies  where gender is not null and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . " and split_part(context_page_path, '/', 3) = " . "'" . $productSlug . "'" . " group by gender,anonymous_id";

        $genderWebQuery = DB::connection('pgsql')->select($webQuery);

        foreach ($genderWebQuery as $gender) {

            $revQuery = "Select SUM(revenue) totalSales from (SELECT product_id,store_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,received_at FROM php_production.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . "  and anonymous_id =" . "'" . $gender->anonymous_id . "'" . ")) where product_id = " . $this->product . " group by product_id,store_id)";


            $revenueOrder = DB::connection('pgsql')->select($revQuery);

            if ($gender->gender == 'male') {
                $maleSale = $maleSale + $revenueOrder[0]->totalsales;
            } else if ($gender->gender == 'female') {
                $femaleSale = $femaleSale + $revenueOrder[0]->totalsales;
            }
        }
        $iosQuery = "select count(*),anonymous_id,gender from(
            select c.params_product_sku,c.params_product_title,lower(c.anonymous_id) as anonymous_id,i.gender from ios_production.product_add_to_cart_tapped as c inner join ios_production.identifies as i on LOWER(i.anonymous_id) = LOWER(c.anonymous_id) where i.received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . " and c.params_product_id= " . $this->product . " and i.gender is not null) group by anonymous_id,gender";

        $genderIosQuery = DB::connection('pgsql')->select($iosQuery);

        foreach ($genderIosQuery as $gender) {

            $revIosQuery = "Select SUM(revenue) totalSales from (SELECT product_id,store_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,received_at FROM php_production.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . "  and anonymous_id =" . "'" . $gender->anonymous_id . "'" . ")) where product_id = " . $this->product . " group by product_id,store_id)";


            $revenueIosOrder = DB::connection('pgsql')->select($revIosQuery);

            if ($gender->gender == 'male') {
                $maleSale = $maleSale + $revenueIosOrder[0]->totalsales;
            } else if ($gender->gender == 'female') {
                $femaleSale = $femaleSale + $revenueIosOrder[0]->totalsales;
            }
        }
        return response()->json([
            'data'   => [
                'type'       => 'Gender',
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale
                ],
            ],
        ]);
    }
     */
    public function getSalesByGender()
    {
        $maleSale   = 0;
        $femaleSale = 0;

        $genderWebQuery = "select gender,anonymous_id,count(*) from javascript_production.identifies where anonymous_id in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where  product_id = ' . $this->product . ' and device = 1) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by anonymous_id,gender having gender is not null ';

        $genderDbWebQuery = DB::connection('pgsql')->select($genderWebQuery);

        foreach ($genderDbWebQuery as $genderWeb) {
            if ($genderWeb->gender == 'male') {
                $maleSale++;
            } elseif ($genderWeb->gender == 'female') {
                $femaleSale++;
            }
        }

        $genderIosQuery = "select gender,anonymous_id,count(*) from javascript_production.identifies where anonymous_id in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where  product_id = ' . $this->product . ' and device = 3) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by anonymous_id,gender having gender is not null ';

        $genderDbIosQuery = DB::connection('pgsql')->select($genderIosQuery);

        foreach ($genderDbIosQuery as $genderIos) {
            if ($genderIos->gender == 'male') {
                $maleSale++;
            } elseif ($genderIos->gender == 'female') {
                $femaleSale++;
            }
        }

        return response()->json([
            'data'   => [
                'type'       => 'Gender',
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale,
                ],
            ],
        ]);
    }

    /**
     * Returns all analytics data by age.
     * @param Request $request
     * @param int $partnerId
     * @return json

    public function getSalesByAge($this->from, $this->to)
    {

        $productSlug = Product::find($this->product)->slug;

        $query = "select count(*),i.age,split_part(i.context_page_path, '/', 3) as slug ,i.anonymous_id,o.order_id from javascript_production.identifies as i inner join php_production.order_completed as o on o.anonymous_id = i.anonymous_id where i.age is not null and i.received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . " and slug =  " . "'" . $productSlug . "'" . " group by i.age,i.context_page_path,i.anonymous_id,o.order_id";

        $ageQuery = DB::connection('pgsql')->select($query);

        foreach ($ageQuery as $age) {

            $revQuery = "Select SUM(revenue)/100 totalSales from (SELECT product_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and order_id =" . $age->order_id . ")) where product_id =" . $this->product . " group by product_id)";

            // $revQuery = "select SUM(quantity * retail_price) as totalSales from order_completed_view where order_id =" . $age->order_id . " and product_id = " . $this->product . "";



            $revenueOrder = DB::connection('pgsql')->select($revQuery);

            $this->ageDistribution($age->age ?? 0, $revenueOrder[0]->totalsales);
        }


        return response()->json([
            'data'   => [
                'type'       => 'demographic age distribution',
                'attributes' =>  [
                    '18-24' => $this->age18 ?? 0,
                    '25-34' => $this->age25 ?? 0,
                    '35-44' => $this->age35 ?? 0,
                    '45-54' => $this->age45 ?? 0,
                    '55-64' => $this->age55 ?? 0,
                    '65+'   => $this->age65 ?? 0
                ],
            ],
        ]);
    }
     */
    public function getSalesByAge()
    {
        $ageWebQuery = "select age,anonymous_id,count(*) from javascript_production.identifies where anonymous_id in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where product_id =' . $this->product . ' and device = 1) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by anonymous_id,age having age is not null ';

        $ageWebDbQuery = DB::connection('pgsql')->select($ageWebQuery);

        foreach ($ageWebDbQuery as $webage) {
            $this->ageDistribution($webage->age ?? 0);
        }

        $ageIosQuery = "select age,lower(anonymous_id),count(*) from ios_production.identifies where lower(anonymous_id) in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where product_id =' . $this->product . ' and device = 3) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by lower(anonymous_id),age having age is not null ';

        $ageIosDbQuery = DB::connection('pgsql')->select($ageIosQuery);

        foreach ($ageIosDbQuery as $iosage) {
            $this->ageDistribution($iosage->age ?? 0);
        }

        return response()->json([
            'data'   => [
                'type'       => 'demographic age distribution',
                'attributes' => [
                    '18-24' => $this->age18 ?? 0,
                    '25-34' => $this->age25 ?? 0,
                    '35-44' => $this->age35 ?? 0,
                    '45-54' => $this->age45 ?? 0,
                    '55-64' => $this->age55 ?? 0,
                    '65+'   => $this->age65 ?? 0,
                ],
            ],
        ]);
    }

    protected function ageDistribution($age)
    {
        switch ($age) {
            case $age > 18 && $age <= 24:
                $this->age18++;
                break;
            case $age > 24 && $age <= 34:
                $this->age25++;
                break;
            case $age > 35 && $age <= 44:
                $this->age35++;
                break;
            case $age > 45 && $age <= 54:
                $this->age45++;
                break;
            case $age > 54 && $age <= 64:
                $this->age55++;
                break;
            case $age > 65:
                $this->age65++;
                break;
            default:
                $this->age0 = 0;
        }
    }

    public function getProductImpressions()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        //TODO after json rds issue will be fixed
        $totalProductImpression = 0;

        $webProductListfiltered   = "select SUM(count) from(select count(*), product_id,store_id from (select json_extract_path_text(product, 'productId',true) as product_id,store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM javascript_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where product_id =' . $this->product . '  group by product_id,store_id )';

        $webProductListFilter = DB::connection('pgsql')->select($webProductListfiltered);

        $webProductListViewed = "Select SUM(count) from (select count(*), product_id,store_id from (select json_extract_path_text(product, 'productId',true) as product_id,store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM javascript_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where product_id =' . $this->product . '  group by product_id,store_id)';

        $webProductListView = DB::connection('pgsql')->select($webProductListViewed);

        $iosProductListfiltered   = "select SUM(count) from(select count(*), product_id,params_store_id from (select json_extract_path_text(product, 'productId',true) as product_id,params_store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id,received_at FROM ios_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where  product_id =' . $this->product . '  group by product_id,params_store_id )';

        $iosProductListFilter = DB::connection('pgsql')->select($iosProductListfiltered);

        $iosProductListViewed = "Select SUM(count) from (select count(*), product_id,params_store_id from (select json_extract_path_text(product, 'productId',true) as product_id,params_store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id,received_at FROM ios_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where  product_id =' . $this->product . '  group by product_id,params_store_id)';

        $iosProductListView = DB::connection('pgsql')->select($iosProductListViewed);

        // javascript promotions table impression

        $webPromotion = "Select SUM(count) from (select count(*), product_id,promotion_store_id from (select json_extract_path_text(product, 'productId',true) as product_id,promotion_store_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,promotion_store_id,received_at FROM javascript_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where  product_id =' . $this->product . '  group by product_id,promotion_store_id)';

        $iosPromotion = "Select SUM(count) from (select count(*), product_id from (select json_extract_path_text(product, 'productId',true) as product_id from(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,received_at FROM ios_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and  received_at between" . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '))  where  product_id =' . $this->product . ' group by product_id)';

        $iosPromotionViewed = DB::connection('pgsql')->select($iosPromotion);

        $webPromotionViewed = DB::connection('pgsql')->select($webPromotion);

        $totalProductImpression += $webProductListFilter[0]->sum + $webProductListView[0]->sum + $iosProductListFilter[0]->sum + $iosProductListView[0]->sum + $webPromotionViewed[0]->sum + $iosPromotionViewed[0]->sum;

        //dd($totalProductImpression);
        return $totalProductImpression;
    }
}
