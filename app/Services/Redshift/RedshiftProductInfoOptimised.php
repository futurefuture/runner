<?php

namespace App\Services\Redshift;

use Analytics;
use App\Category;
use App\Partner;
use App\Product;
use App\Store;
use Carbon\Carbon;
use DB;
use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class RedshiftProductInfoOptimised
{
    protected $partnerId;
    protected $totalProductClick           = 0;
    protected $totalUniqueClick            = 0;
    protected $totalProductAddToCart       = 0;
    protected $totalAddToCartDetail        = 0;
    protected $totalBuyToDetail            = 0;
    protected $totalProductUniquePurchases = 0;
    protected $totalQuantity               = 0;
    protected $totalRevenue                = 0;
    protected $totalShares                 = 0;
    protected $totalFavourites             = 0;
    protected $totalProductRevenue         = 0;
    protected $totalProductQuantitySold    = 0;
    protected $totalProductPurchases       = 0;
    protected $storearr                    = [];
    public $phpdatabase                    = 0;
    public $iosdatabase                    = 0;
    public $javascriptdb                   = 0;
    protected $age0                        = 0;
    protected $age18                       = 0;
    protected $age25                       = 0;
    protected $age35                       = 0;
    protected $age45                       = 0;
    protected $age55                       = 0;
    protected $age65                       = 0;
    protected $from;
    protected $to;
    protected $partner;
    protected $product;
    protected $productInfo;
    protected $productSKU;
    protected $slug;
    protected $productTitle;
    protected $productCategory;
    protected $subCategory;
    protected $subCategoryTitle;
    protected $broadCategory;
    protected $broadCategoryTitle;

    public function __construct(Partner $partner, Product $product, $startDate, $endDate)
    {
        $this->from            = Carbon::parse($startDate)->addHour(5);
        $this->to              = Carbon::parse($endDate)->addHour(6);
        $this->partner         = $partner;
        $this->product         = $product;
        $this->productInfo     = $product;
        $this->productSKU      = $product->sku;
        $this->slug            = $product->slug;
        $this->productTitle    = $product->title;
        $this->productCategory = Product::where('id', '=', $this->product->id)
            ->get()
            ->pluck('category_id')
            ->toArray();
        $this->getBroadCategoryAndSubCategory();
        $this->totalProductClick     = 0;
        $this->totalProductAddToCart = 0;
        $this->phpdatabase           = 'php_production';
        $this->iosdatabase           = 'ios_production';
        $this->javascriptdb          = 'javascript_production';
    }

    public function getSubRank()
    {
        $subRankArray = [];

        $subRank                  = $this->getRank($this->subCategory);
        $subRankArray['subTitle'] = $this->subCategoryTitle;
        $subRankArray['rank']     = $subRank;

        return $subRankArray;
    }

    public function getBroadRank()
    {
        $broadRankArray = [];
        $broadRank      = $this->getRank($this->broadCategory);

        $broadRankArray['broadTitle'] = $this->broadCategoryTitle;
        $broadRankArray['rank']       = $broadRank;

        return $broadRankArray;
    }

    protected function getBroadCategoryAndSubCategory()
    {
        // TODO once there is table on rds
        $cat = DB::select('select  c1.id as category_id,c1.title,c1.parent_id as subCategory ,c2.parent_id as broadCategory from   categories c1
        left join categories  c2 on c2.id = c1.parent_id 
        left join  categories c3 on c3.id = c2.parent_id where c1.id =' . $this->productCategory[0]);

        $this->broadCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->subCategory : $cat[0]->broadCategory;

        if ($this->broadCategory != null) {
            $this->broadCategoryTitle = Category::find($this->broadCategory)->title;
        } else {
            $this->broadCategoryTitle = 0;
        }
        $this->subCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->category_id : $cat[0]->subCategory;

        if ($this->subCategory != null) {
            $this->subCategoryTitle = Category::find($this->subCategory)->title;
        } else {
            $this->subCategoryTitle = 0;
        }
    }

    protected function getRank($categoryId)
    {
        if ($categoryId != null) {
            $query = 'SELECT product, rank FROM(SELECT product, totRevenue, @curRank := @curRank + 1 AS rank FROM(SELECT product_id AS product, ROUND(SUM(quantity * retail_price) / 100, 2) AS totRevenue FROM order_items WHERE DATE(created_at) BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 ON e1.id = e.parent_id WHERE ' . $categoryId . ' IN(r.parent_id, e.parent_id, e1.parent_id) UNION SELECT id FROM categories WHERE id = ' . $categoryId . ') GROUP BY product_id ORDER BY totRevenue DESC) AS test, (SELECT @curRank := 0) r) AS rank WHERE product = ' . $this->product;

            $rank = DB::select($query);

            return  $rank ? $rank[0]->rank : 0;
        } else {
            return 0;
        }
    }

    public function getTotalPurchases()
    {
        $query = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM  php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $productPurchases = DB::connection('pgsql')->select($query);

        foreach ($productPurchases as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $productPurchases;
    }

    public function getTotalQuantity()
    {
        // TODO once IOS table is ready add that value into it
        $query = "SELECT count(*),product_id,SUM(quantity) as quantity,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $quantitySold = DB::connection('pgsql')->select($query);

        foreach ($quantitySold as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $quantitySold;
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByMedium()
    {
        $emailQuery = "select SUM(totalSales) as emailSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'email' and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $salesByEmail = DB::connection('pgsql')->select($emailQuery);

        $digitialQuery = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,timestamp from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,timestamp from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'Digital' and timestamp between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and timestamp between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,timestamp )';

        $digitialQuery = "select SUM(totalSales) as digitialSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . ".pages where context_campaign_medium = 'Digital' and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path = '/confirmation' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $noneQuery = "select SUM(totalSales) as noneSales from(select SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,json_extract_path_text(product, 'productId') as product_id,received_at from (select cogs,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from(select path, anonymous_id,context_page_search from ' . $this->javascriptdb . '.pages where  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id IN (select anonymous_id from ' . $this->javascriptdb . '.pages where context_campaign_medium is null  and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " group by anonymous_id)) where path like '%/product/" . $this->slug . "%' group by anonymous_id,path)) where product_id = " . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,received_at )';

        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);
        $salesByNone     = DB::connection('pgsql')->select($noneQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
            'none'     => $salesByNone[0]->nonesales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByCampaign()
    {
        $productSlug = Product::find($this->product)->slug;

        $topClickCampaignQuery = "select context_campaign_name,count(*),split_part(context_page_path, '/', 3) as slug from javascript_production.pages  where context_campaign_name is not null and slug = " . "'" . $productSlug . "'" . ' and received_at  between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by context_campaign_name,slug order by count(*) desc ';

        $topCampaignName        = DB::connection('pgsql')->select($topClickCampaignQuery);
        $formattedCampaignSales = [];
        foreach ($topCampaignName as $campaign) {
            // get orders  by each campaign name
            $orderQuery = "select json_extract_path_text(product, 'productId') as product_id,SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales from (select anonymous_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in (select DISTINCT anonymous_id from javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') and  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and context_page_path =' . "'/product/" . $productSlug . "'" . ') ) where product_id = ' . $this->product . ' group by product_id';

            $addTocartQuery = 'select count(*) from php_production.product_added where received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in(select DISTINCT anonymous_id from  javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and path =' . "'/product/" . $productSlug . "'" . ')) and product_id = ' . $this->product . '';

            $addToCart = DB::connection('pgsql')->select($addTocartQuery);

            $getSalesByCampaign = DB::connection('pgsql')->select($orderQuery);

            $transactionQuery = "select count(*) from(select json_extract_path_text(product, 'productId') as product_id,SUM(json_extract_path_text(product, 'quantity') * json_extract_path_text(product, 'price')) as totalSales,order_id from (select order_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,received_at from php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and anonymous_id in (select DISTINCT anonymous_id from javascript_production.pages where anonymous_id in (select anonymous_id from javascript_production.pages  where context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') and  received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and context_page_path =' . "'/product/" . $productSlug . "'" . ') ) where product_id = ' . $this->product . ' group by product_id,order_id)';

            $sourceQuery = 'select Distinct context_campaign_source from javascript_production.pages where  context_campaign_name =' . "'" . $campaign->context_campaign_name . "'" . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . '';

            $sourceQuery = DB::connection('pgsql')->select($sourceQuery);

            $totalTransactionQuery        = DB::connection('pgsql')->select($transactionQuery);
            $campaignSales['source']      = $sourceQuery[0]->context_campaign_source;
            $campaignSales['name']        = $campaign->context_campaign_name;
            $campaignSales['sales']       = $getSalesByCampaign[0]->totalsales ?? 0;
            $campaignSales['addToCart']   = $addToCart[0]->count ?? 0;
            $campaignSales['transaction'] = $totalTransactionQuery[0]->count ?? 0;
            $campaignSales['pageViews']   = $campaign->count ?? 0;
            array_push($formattedCampaignSales, $campaignSales);
        }

        return $formattedCampaignSales;
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSessionsBySource()
    {
        $query = 'select context_campaign_source,count(*) from(select context_campaign_source,context_page_path,count(*),anonymous_id from ' . $this->javascriptdb . '.pages where received_at  between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . " and context_page_path like '%/product/" . $this->slug . "%' group by context_campaign_source,context_page_path,anonymous_id) group by context_campaign_source";

        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                if ($medium->context_campaign_source == null) {
                    $salesSource['none'] = $medium->count;
                } else {
                    $salesSource[$medium->context_campaign_source] = $medium->count;
                }
            }

            return $salesSource;
        } else {
            return;
        }
    }

    public function getTotalPageViews()
    {
        $webQuery = 'SELECT COUNT(*) AS totalPageViews FROM ' . $this->javascriptdb . '.product_viewed where product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'';

        $product_total_page_views = DB::connection('pgsql')->select($webQuery);

        $iosQuery = 'SELECT COUNT(*) AS totalPageViews FROM ' . $this->iosdatabase . '.product_viewed where params_product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND params_product_id = ' . $this->product->id;

        $product_total_page_views_ios = DB::connection('pgsql')->select($iosQuery);

        return $product_total_page_views[0]->totalpageviews + $product_total_page_views_ios[0]->totalpageviews;
    }

    public function getTotalUniquePageViews()
    {
        $product_unique_clicked_frontend = DB::connection('pgsql')->select('SELECT COUNT(*) AS uniqueView FROM(SELECT COUNT(*),product_id, store_id, anonymous_id FROM ' . $this->javascriptdb . '.product_viewed WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, store_id,anonymous_id)');

        $product_unique_clicked_ios = DB::connection('pgsql')->select('SELECT COUNT(*) AS uniqueView FROM(SELECT COUNT(*), params_product_id, params_store_id, anonymous_id FROM ' . $this->iosdatabase . '.product_viewed WHERE params_product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND params_product_id = ' . $this->product->id . ' GROUP BY params_product_id, params_store_id, anonymous_id)');

        return $product_unique_clicked_frontend[0]->uniqueview + $product_unique_clicked_ios[0]->uniqueview;
    }

    public function getTotalAddToCart()
    {
        $webQuery = 'SELECT COUNT(*) AS totalAddToCart FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND product_id = ' . $this->product->id . ' AND anonymous_id IS NOT NULL';

        $productAddToCartWeb = DB::connection('pgsql')->select($webQuery);

        return $productAddToCartWeb[0]->totaladdtocart;
    }

    public function getWebPageViews()
    {
        $productAnalytics = [];
        $pageViews        = $this->getPageViews();
        $uniquePageViews  = $this->getUniquePageViews();
        $addToCart        = $this->getAddToCart();
        $purchases        = $this->getStorePurchases();
        $quantity         = $this->getStoreQuantity();
        $revenue          = $this->getStoreRevenue();
        $store            = Store::where('is_active', 1)->pluck('id')->toArray();
        $storefront       = array_intersect(array_unique($this->storearr), $store);

        foreach ($storefront as $key => $val) {
            $webStore                    = [];
            $iosStore                    = [];
            $storeInfo                   = Store::find($val);
            $webStore['pageViews']       = $pageViews[$val]['pageViews']['web'] ?? 0;
            $webStore['uniquePageViews'] = $uniquePageViews[$val]['uniquePageViews']['web'] ?? 0;
            $webStore['addToCart']       = $addToCart[$val]['addToCarts']['web'] ?? 0;
            $webStore['purchases']       = $purchases[$val]['purchases']['web'] ?? 0;
            $webStore['quantity']        = $quantity[$val]['quantitySold']['web'] ?? 0;
            $webStore['revenue']         = $revenue[$val]['revenue']['web'] ?? 0;
            $iosStore['pageViews']       = $pageViews[$val]['pageViews']['ios'] ?? 0;
            $iosStore['uniquePageViews'] = $uniquePageViews[$val]['uniquePageViews']['ios'] ?? 0;
            $iosStore['addToCart']       = $addToCart[$val]['addToCarts']['ios'] ?? 0;
            $iosStore['purchases']       = $purchases[$val]['purchases']['ios'] ?? 0;
            $iosStore['quantity']        = $quantity[$val]['quantitySold']['ios'] ?? 0;
            $iosStore['revenue']         = $revenue[$val]['revenue']['ios'] ?? 0;

            $productAnalytics[$val] = [
                'store'  => [
                    'title' => $storeInfo->title,
                    'logo'  => $storeInfo->options['storeLogo'],
                ],
                'pageViews' => [
                    'web' => $webStore['pageViews'] ?? 0,
                    'ios' => $iosStore['pageViews'] ?? 0,
                ],
                'uniquePageViews' => [
                    'web'   => $webStore['uniquePageViews'] ?? 0,
                    'ios'   => $iosStore['uniquePageViews'] ?? 0,
                ],
                'addToCarts' => [
                    'web' => $webStore['addToCart'] ?? 0,
                    'ios' => $iosStore['addToCart'] ?? 0,
                ],
                'cartToDetailRate' => [],
                'buyToDetailRate'  => [],
                'purchases'        => [
                    'web'  => $webStore['purchases'] ?? 0,
                    'ios'  => $iosStore['purchases'] ?? 0,
                ],
                'quantitySold' => [
                    'web'  => $webStore['quantity'] ?? 0,
                    'ios'  => $iosStore['quantity'] ?? 0,
                ],
                'revenue' => [
                    'web'  => $webStore['revenue'] ?? 0,
                    'ios'  => $iosStore['revenue'] ?? 0,
                ],
            ];
        }

        return response()->json([
            'data' => [
                'type'       => 'products',
                'id'         => (string) $this->product,
                'SKU'        => $this->productSKU,
                'title'      => $this->productTitle,
                'attributes' => $productAnalytics,
            ],
        ]);
    }

    public function getPageViews()
    {
        $pageViews = [];
        $webQuery  = 'SELECT COUNT(*), product_id, store_id FROM ' . $this->javascriptdb . '.product_viewed WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, store_id ORDER BY store_id ASC';

        $product_list_clicked_frontend = DB::connection('pgsql')->select($webQuery);

        foreach ($product_list_clicked_frontend as $view) {
            $store            = Store::find($view->store_id);
            $this->storearr[] = $view->store_id;
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $query = 'SELECT COUNT(*), params_product_id as product_id, params_store_id as store_id FROM ' . $this->iosdatabase . '.product_viewed WHERE params_product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND params_product_id = ' . $this->product->id . ' GROUP BY params_product_id, params_store_id ORDER BY params_store_id ASC';

        $product_list_clicked_ios = DB::connection('pgsql')->select($query);

        foreach ($product_list_clicked_ios as $view) {
            $this->storearr[] = $view->store_id;
            $store            = Store::find($view->store_id);
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $storefront = false;

        if (count($product_list_clicked_ios) > count($product_list_clicked_frontend)) {
            $storefront = true;
        }

        if ($storefront) {
            return $this->pageViewsStructure($product_list_clicked_ios, $product_list_clicked_frontend, 'pageViews');
        } else {
            return  $this->pageViewsStructure($product_list_clicked_frontend, $product_list_clicked_ios, 'pageViews');
        }
    }

    public function getUniquePageViews()
    {
        $product_unique_clicked_frontend = DB::connection('pgsql')->select('SELECT COUNT(*), store_id FROM (SELECT COUNT(*), product_id,store_id, anonymous_id FROM ' . $this->javascriptdb . '.product_viewed WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, store_id, anonymous_id) GROUP BY store_id ORDER BY store_id ASC');

        foreach ($product_unique_clicked_frontend as $view) {
            $store            = Store::find($view->store_id);
            $this->storearr[] = $view->store_id;
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $product_unique_clicked_ios = DB::connection('pgsql')->select('SELECT COUNT(*), params_store_id AS store_id FROM (SELECT COUNT(*), params_product_id, params_store_id, anonymous_id FROM ' . $this->iosdatabase . '.product_viewed WHERE params_product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND params_product_id = ' . $this->product->id . ' GROUP BY params_product_id, params_store_id, anonymous_id) GROUP BY params_store_id ORDER BY params_store_id');

        foreach ($product_unique_clicked_ios as $view) {
            $store            = Store::find($view->store_id);
            $this->storearr[] = $view->store_id;
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $storefront = false;

        if (count($product_unique_clicked_ios) > count($product_unique_clicked_frontend)) {
            $storefront = true;
        }

        if ($storefront) {
            return $this->pageViewsStructure($product_unique_clicked_ios, $product_unique_clicked_frontend, 'uniquePageViews');
        } else {
            return  $this->pageViewsStructure($product_unique_clicked_frontend, $product_unique_clicked_ios, 'uniquePageViews');
        }
    }

    public function getAddToCart()
    {
        $webQuery = 'SELECT anonymous_id FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND product_id = ' . $this->product->id . ' AND anonymous_id IS NOT NULL';

        $productViewQueryWeb = 'SELECT COUNT(*), store_id from javascript_production.product_viewed WHERE anonymous_id IN (' . $webQuery . ') AND product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY store_id ORDER BY store_id';

        $productAddToCartWeb = DB::connection('pgsql')->select($productViewQueryWeb);

        foreach ($productAddToCartWeb as $view) {
            $store            = Store::find($view->store_id);
            $this->storearr[] = $view->store_id;
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $productViewQueryIos = 'SELECT COUNT(*), params_store_id AS store_id FROM ios_production.product_viewed WHERE lower(anonymous_id) IN (' . $webQuery . ') AND params_product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY params_store_id ORDER BY params_store_id ASC';

        $productAddToCartIos = DB::connection('pgsql')->select($productViewQueryIos);

        foreach ($productAddToCartIos as $view) {
            $store            = Store::find($view->store_id);
            $this->storearr[] = $view->store_id;
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        $storefront = false;

        if (count($productAddToCartIos) > count($productAddToCartWeb)) {
            $storefront = true;
        }

        if ($storefront) {
            return $this->pageViewsStructure($productAddToCartIos, $productAddToCartWeb, 'addToCarts');
        } else {
            return  $this->pageViewsStructure($productAddToCartWeb, $productAddToCartIos, 'addToCarts');
        }
    }

    public function getStorePurchases()
    {
        $query = 'SELECT COUNT(*), store_id, device FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, order_id, store_id, device FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, store_id, device, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY store_id, device ORDER BY store_id ASC';

        $productPurchases = DB::connection('pgsql')->select($query);

        $webPurchases = [];

        foreach ($productPurchases as $arr) {
            $storeInfo        = Store::find($arr->store_id);
            $this->storearr[] = $arr->store_id;

            if ($arr->device == 1) {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['purchases']['web'] = $arr->count;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'purchases' => [
                            'web' => $arr->count,
                            'ios' => 0
                        ],
                    ];
                }
            } else {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['purchases']['ios'] = $arr->count;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'purchases' => [
                            'ios' => $arr->count,
                            'web' => 0
                        ],
                    ];
                }
            }
        }

        return $webPurchases;
    }

    public function getStoreQuantity()
    {
        $query = 'SELECT SUM(quantity) AS quantity, store_id, device FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, order_id, store_id, device FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, store_id, device, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY store_id, device ORDER BY store_id ASC';

        $quantitySold = DB::connection('pgsql')->select($query);

        $formattedWebQuantity = [];
        $formattedIosQuantity = [];
        $webPurchases         = [];

        foreach ($quantitySold as $arr) {
            $storeInfo        = Store::find($arr->store_id);
            $this->storearr[] = $arr->store_id;
            if ($arr->device == 1) {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['quantitySold']['web'] = $arr->quantity;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'quantitySold' => [
                            'web' => $arr->quantity,
                            'ios' => 0
                        ],
                    ];
                }
            } else {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['quantitySold']['ios'] = $arr->quantity;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'quantitySold' => [
                            'ios' => $arr->quantity,
                            'web' => 0
                        ],
                    ];
                }
            }
        }

        return $webPurchases;
    }

    public function getStoreRevenue()
    {
        $query = 'SELECT SUM(quantity * retail_price) AS revenue, store_id, device FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price, order_id, store_id, device FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, cogs, store_id, device, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY product_id, store_id, device ORDER BY store_id ASC';

        $revenue = DB::connection('pgsql')->select($query);

        $webPurchases = [];

        foreach ($revenue as $arr) {
            $storeInfo        = Store::find($arr->store_id);
            $this->storearr[] = $arr->store_id;
            if ($arr->device == 1) {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['revenue']['web'] = $arr->revenue;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'revenue' => [
                            'web' => $arr->revenue,
                            'ios' => 0
                        ],
                    ];
                }
            } else {
                if (isset($webPurchases[$arr->store_id])) {
                    $webPurchases[$arr->store_id]['revenue']['ios'] = $arr->revenue;
                } else {
                    $webPurchases[$arr->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        'revenue' => [
                            'ios' => $arr->revenue,
                            'web' => 0
                        ],
                    ];
                }
            }
        }

        return $webPurchases;
    }

    public function getStoreFavourite()
    {
        $query = 'Select count(*),store_id from ' . $this->javascriptdb . '.product_favourited where product_id = ' . $this->product . ' group by store_id order by store_id asc';

        $favourite = DB::connection('pgsql')->select($query);

        foreach ($favourite as $view) {
            $store            = Store::find($view->store_id);
            $view->storetitle = $store->title ?? '';
            $view->storelogo  = isset($store->options['storeLogo']) ? $store : null;
        }

        return [
            'Web' => $favourite ?? 0,
        ];
    }

    public function getTopPostalCode()
    {
        $query = 'SELECT extractCode, COUNT(DISTINCT order_id) AS totalOrderCount FROM(SELECT SUBSTRING(postal_code, 1, 3) AS extractCode, order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, order_id, postal_code FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ') WHERE product_id = ' . $this->product . ') GROUP BY extractCode ORDER BY totalOrderCount DESC LIMIT 5';

        $topPostalCode = DB::connection('pgsql')->select($query);

        return response()->json([
            'data' => [
                'type'       => 'demographic postal code',
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    /**
     * Returns all analytics data by gender.
     * @param Request $request
     * @param int $partnerId
     * @return json

    public function getSalesByGender($this->from, $this->to)
    {

        $maleSale   = 0;
        $femaleSale = 0;

        $productSlug = Product::find($this->product)->slug;

        $webQuery = "select count(*),gender,anonymous_id from javascript_production.identifies  where gender is not null and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . " and split_part(context_page_path, '/', 3) = " . "'" . $productSlug . "'" . " group by gender,anonymous_id";

        $genderWebQuery = DB::connection('pgsql')->select($webQuery);

        foreach ($genderWebQuery as $gender) {

            $revQuery = "Select SUM(revenue) totalSales from (SELECT product_id,store_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,received_at FROM php_production.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . "  and anonymous_id =" . "'" . $gender->anonymous_id . "'" . ")) where product_id = " . $this->product . " group by product_id,store_id)";


            $revenueOrder = DB::connection('pgsql')->select($revQuery);

            if ($gender->gender == 'male') {
                $maleSale = $maleSale + $revenueOrder[0]->totalsales;
            } else if ($gender->gender == 'female') {
                $femaleSale = $femaleSale + $revenueOrder[0]->totalsales;
            }
        }
        $iosQuery = "select count(*),anonymous_id,gender from(
            select c.params_product_sku,c.params_product_title,lower(c.anonymous_id) as anonymous_id,i.gender from ios_production.product_add_to_cart_tapped as c inner join ios_production.identifies as i on LOWER(i.anonymous_id) = LOWER(c.anonymous_id) where i.received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . " and c.params_product_id= " . $this->product . " and i.gender is not null) group by anonymous_id,gender";

        $genderIosQuery = DB::connection('pgsql')->select($iosQuery);

        foreach ($genderIosQuery as $gender) {

            $revIosQuery = "Select SUM(revenue) totalSales from (SELECT product_id,store_id,SUM(quantity*retail_price) as revenue from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,received_at FROM php_production.order_completed , seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between "  . "'" . $this->from . "'" . ' and '  . "'" . $this->to . "'" . "  and anonymous_id =" . "'" . $gender->anonymous_id . "'" . ")) where product_id = " . $this->product . " group by product_id,store_id)";


            $revenueIosOrder = DB::connection('pgsql')->select($revIosQuery);

            if ($gender->gender == 'male') {
                $maleSale = $maleSale + $revenueIosOrder[0]->totalsales;
            } else if ($gender->gender == 'female') {
                $femaleSale = $femaleSale + $revenueIosOrder[0]->totalsales;
            }
        }
        return response()->json([
            'data'   => [
                'type'       => 'Gender',
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale
                ],
            ],
        ]);
    }
     */
    public function getSalesByGender()
    {
        $maleSale   = 0;
        $femaleSale = 0;

        $genderWebQuery = "select gender,anonymous_id,count(*) from javascript_production.identifies where anonymous_id in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where  product_id = ' . $this->product . ' and device = 1) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by anonymous_id,gender having gender is not null ';

        $genderDbWebQuery = DB::connection('pgsql')->select($genderWebQuery);

        foreach ($genderDbWebQuery as $genderWeb) {
            if ($genderWeb->gender == 'male') {
                $maleSale++;
            } elseif ($genderWeb->gender == 'female') {
                $femaleSale++;
            }
        }

        $genderIosQuery = "select gender,anonymous_id,count(*) from javascript_production.identifies where anonymous_id in(Select anonymous_id from (select device,anonymous_id,json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price from(SELECT device,anonymous_id,order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ') ) where  product_id = ' . $this->product . ' and device = 3) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by anonymous_id,gender having gender is not null ';

        $genderDbIosQuery = DB::connection('pgsql')->select($genderIosQuery);

        foreach ($genderDbIosQuery as $genderIos) {
            if ($genderIos->gender == 'male') {
                $maleSale++;
            } elseif ($genderIos->gender == 'female') {
                $femaleSale++;
            }
        }

        return response()->json([
            'data' => [
                'type'       => 'Gender',
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale,
                ],
            ],
        ]);
    }

    public function getSalesByAge()
    {
        $ageWebQuery = 'SELECT age, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM(SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ')) WHERE product_id = ' . $this->product . ' AND device = 1) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' GROUP BY anonymous_id, age HAVING AGE IS NOT NULL';

        $ageWebDbQuery = DB::connection('pgsql')->select($ageWebQuery);

        foreach ($ageWebDbQuery as $webage) {
            $this->ageDistribution($webage->age ?? 0);
        }

        $ageIosQuery = 'SELECT age, LOWER(anonymous_id), COUNT(*) FROM ios_production.identifies WHERE LOWER(anonymous_id) IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ')) WHERE product_id = ' . $this->product . ' AND device = 3) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' GROUP BY LOWER(anonymous_id), age HAVING age IS NOT NULL';

        $ageIosDbQuery = DB::connection('pgsql')->select($ageIosQuery);

        foreach ($ageIosDbQuery as $iosage) {
            $this->ageDistribution($iosage->age ?? 0);
        }

        return response()->json([
            'data' => [
                'type'       => 'demographic age distribution',
                'attributes' => [
                    '18-24' => $this->age18 ?? 0,
                    '25-34' => $this->age25 ?? 0,
                    '35-44' => $this->age35 ?? 0,
                    '45-54' => $this->age45 ?? 0,
                    '55-64' => $this->age55 ?? 0,
                    '65+'   => $this->age65 ?? 0,
                ],
            ],
        ]);
    }

    protected function ageDistribution($age)
    {
        switch ($age) {
            case $age > 18 && $age <= 24:
                $this->age18++;
                break;
            case $age > 24 && $age <= 34:
                $this->age25++;
                break;
            case $age > 35 && $age <= 44:
                $this->age35++;
                break;
            case $age > 45 && $age <= 54:
                $this->age45++;
                break;
            case $age > 54 && $age <= 64:
                $this->age55++;
                break;
            case $age > 65:
                $this->age65++;
                break;
            default:
                $this->age0 = 0;
        }
    }

    public function getProductImpressions()
    {
        $this->from             = Carbon::parse($this->from);
        $this->to               = Carbon::parse($this->to);
        $totalProductImpression = 0;
        $webProductListfiltered = 'SELECT SUM(count) FROM(SELECT COUNT(*), product_id, store_id FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, store_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM javascript_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . '))  WHERE product_id = ' . $this->product . ' GROUP BY product_id, store_id)';

        $webProductListFilter = DB::connection('pgsql')->select($webProductListfiltered);

        $webProductListViewed = 'SELECT SUM(count) FROM(SELECT COUNT(*), product_id, store_id FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, store_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM javascript_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) and received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . '))   WHERE product_id = ' . $this->product . ' GROUP BY product_id, store_id)';

        $webProductListView = DB::connection('pgsql')->select($webProductListViewed);

        $iosProductListfiltered = 'SELECT SUM(count) FROM(SELECT COUNT(*), product_id, params_store_id FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, params_store_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, params_store_id, received_at FROM ios_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ')) WHERE product_id = ' . $this->product . ' GROUP BY product_id, params_store_id)';

        $iosProductListFilter = DB::connection('pgsql')->select($iosProductListfiltered);

        $iosProductListViewed = 'SELECT SUM(count) FROM(SELECT COUNT(*), product_id, params_store_id FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, params_store_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, params_store_id, received_at FROM ios_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ')) WHERE product_id = ' . $this->product . ' GROUP BY product_id, params_store_id)';

        $iosProductListView = DB::connection('pgsql')->select($iosProductListViewed);

        $webPromotion = 'SELECT SUM(count) FROM (SELECT COUNT(*), product_id, promotion_store_id FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, promotion_store_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product, promotion_store_id, received_at FROM javascript_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . '))  WHERE product_id = ' . $this->product . ' GROUP BY product_id, promotion_store_id)';

        $iosPromotion = 'SELECT SUM(count) FROM (SELECT COUNT(*), product_id FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ')) WHERE product_id = ' . $this->product . ' GROUP BY product_id)';

        $iosPromotionViewed = DB::connection('pgsql')->select($iosPromotion);

        $webPromotionViewed = DB::connection('pgsql')->select($webPromotion);

        $totalProductImpression += $webProductListFilter[0]->sum + $webProductListView[0]->sum + $iosProductListFilter[0]->sum + $iosProductListView[0]->sum + $webPromotionViewed[0]->sum + $iosPromotionViewed[0]->sum;

        return $totalProductImpression;
    }

    protected function pageViewsStructure(array $large, array $small, $type)
    {
        $pageViews = [];

        foreach ($large as $ios) {
            $storeInfo = Store::find($ios->store_id);

            foreach ($small as $web) {
                if ($web->store_id == $ios->store_id) {
                    $pageViews[$ios->store_id] = [
                        'store'  => [
                            'title' => $storeInfo->title,
                            'logo'  => $storeInfo->options['storeLogo'],
                        ],
                        $type => [
                            'web' => $web->count,
                            'ios' => $ios->count,
                        ],
                    ];
                } else {
                    if (isset($pageViews[$ios->store_id])) {
                    } else {
                        $pageViews[$ios->store_id] = [
                            'store'  => [
                                'title' => $storeInfo->title ?? '',
                                'logo'  => isset($storeInfo->options['storeLogo']) ? $storeInfo : null,
                            ],
                            $type => [
                                'web' => 0,
                                'ios' => $ios->count,
                            ],
                        ];
                    }
                }
            }
        }

        return $pageViews;
    }
}
