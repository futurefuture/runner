<?php

namespace App\Services\Redshift;

use App\OrderItem;
use App\Partner;
use App\Product;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RedshiftService
{
    protected $from;
    protected $to;
    protected $diffInDays;
    protected $partner;
    protected $store;
    protected $ids;
    protected $ps;
    protected $slug;
    protected $slugImp;

    public function __construct(Partner $partner, $startDate, $endDate)
    {
        $this->from       = Carbon::parse($startDate)->addHour(5);
        $this->to         = Carbon::parse($endDate)->addHour(6);
        $this->diffInDays = $this->to->diffInDays($this->from);
        $this->partner    = $partner;
        $this->store      = $this->partner->store_id;
        $this->ids        = DB::table('partner_product')->where('partner_id', $partner->id)
            ->where('product_id', '<>', 0)
            ->pluck('product_id')->toArray();
        $ps         = implode("','", $this->ids);
        $this->ps   = "'" . $ps . "'";
        $this->slug = DB::table('partner_product')->where('partner_id', $partner->id)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.slug')->toArray();
        $impSlug       = implode("','", $this->slug);
        $this->slugImp = "'" . $impSlug . "'";
    }

    /**
     * Returns overall sales, total orders for given period.
     */
    public function getTotalSales()
    {
        $query = 'SELECT SUM(revenue) as revenue, SUM(totalOrders) AS order FROM (SELECT SUM(retail_price * quantity) AS revenue, store_id, order_id, COUNT(*) AS totalOrders FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\', true) AS retail_price, order_id, store_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT order_id, cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ') GROUP BY store_id, order_id)';

        $orderItems = DB::connection('pgsql')->select($query);

        return $orderItems[0]->revenue ?? 0;
    }

    /**
     * Returns overall sales, total orders for given period.
     */
    public function getTotalOrder()
    {
        $query = 'SELECT SUM(revenue) as revenue, COUNT(*) AS order FROM (SELECT SUM(retail_price * quantity) AS revenue, order_id, COUNT(*) AS totalOrders FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\', true) AS retail_price, order_id, store_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT order_id, cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND  received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ') GROUP BY order_id)';

        $orderItems = DB::connection('pgsql')->select($query);

        return $orderItems[0]->order ?? 0;
    }

    /**
     * Returns total customers.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getTotalCustomers()
    {
        $totalCustomers = 0;
        $query          = 'SELECT COUNT(DISTINCT user_id) AS totalCustomer FROM(SELECT user_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT DISTINCT user_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN(' . $this->ps . '))';

        $users = DB::connection('pgsql')->select($query);

        $totalCustomers = $users[0]->totalcustomer;

        return $totalCustomers;
    }

    /**
     * Returns total new customers.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getTotalNewCustomers()
    {
        $totalCustomers = 0;
        $query          = 'SELECT count(new_customers.user_id)FROM(SELECT user_id AS recent_users,json_extract_path_text(json_extract_array_element_text(oc.products,seq.i),\'productId\')AS productId FROM seq_0_to_100 AS seq,php_production.order_completed AS oc WHERE seq.i<JSON_ARRAY_LENGTH(oc.products)AND received_at between' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND productId IN( ' . $this->ps . '))AS recent_users_table INNER JOIN(SELECT user_id FROM(SELECT user_id,json_extract_path_text(json_extract_array_element_text(oc.products,seq.i),\'productId\')AS productId FROM seq_0_to_100 AS seq,php_production.order_completed AS oc WHERE seq.i<JSON_ARRAY_LENGTH(oc.products)AND productId IN( ' . $this->ps . '))GROUP BY user_id HAVING count(user_id)=1)AS new_customers ON recent_users=new_customers.user_id';

        $users = DB::connection('pgsql')->select($query);

        $totalCustomers = $users[0]->count ?? 0;

        return $totalCustomers;
    }

    /**
     * Returns total customers.
     */
    public function getTopProducts()
    {
        $query = 'SELECT product_id, SUM(quantity * retail_price) AS revenue FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\',true) AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\',true) AS retail_price, order_id, store_id FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ') GROUP BY product_id ORDER BY revenue DESC LIMIT 5';

        $products    = DB::connection('pgsql')->select($query);
        $topProducts = [];
        foreach ($products as $product) {
            $newProduct          = Product::find($product->product_id);
            $topProduct['title'] = $newProduct->title ?? 0;
            $topProduct['price'] = $product->revenue ?? 0;
            array_push($topProducts, $topProduct);
        }

        return $topProducts;
    }

    /**
     * Returns all analytics data order by device.
     * @param OrderItem $filter
     * @return void
     */
    public function getOrdersByDevice()
    {
        $query = 'SELECT COUNT(*), device, order_id FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, order_id, store_id, device FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, device, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN( ' . $this->ps . ') GROUP BY device, order_id';

        $orderByDevice   = DB::connection('pgsql')->select($query);
        $iosCount        = 0;
        $webDesktopCount = 0;
        $webMobileCount  = 0;

        foreach ($orderByDevice as $device) {
            if ($device->device == 1) {
                $webDesktopCount = $webDesktopCount + $device->count;
            } elseif ($device->device == 2) {
                $webMobileCount = $webMobileCount + $device->count;
            } else {
                $iosCount = $iosCount + $device->count;
            }
        }

        return $orderByDevice = [
            'ios'        => $iosCount ?? 0,
            'webDesktop' => $webDesktopCount ?? 0,
            'webMobile'  => $webMobileCount ?? 0,
        ];
    }

    /**
     * Returns all analytics data order by device.
     */
    public function getOrderByHour()
    {
        $query = 'SELECT hour, COUNT(*) FROM (SELECT EXTRACT(hour FROM received_at) AS hour, order_id, COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product,  \'productId\', true) AS product_id, received_at, order_id FROM (SELECT order_id, received_at, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN( ' . $this->ps . ')) GROUP BY order_id, hour) GROUP BY hour ORDER BY hour';

        $orderByHour = DB::connection('pgsql')->select($query);

        return $orderByHour;
    }

    /**
     * Returns all analytics data for transactions module.
     */
    public function getSessions()
    {
        $this->from   = Carbon::parse($this->from);
        $this->to     = Carbon::parse($this->to);
        $sessions     = 0;

        $sessionQuery = 'SELECT COUNT(*) AS session FROM (SELECT DISTINCT anonymous_id FROM javascript_production.product_viewed WHERE product_id IN(' . $this->ps . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')';

        $sessionWebQuery = DB::connection('pgsql')->select($sessionQuery);

        $iosSession = 'SELECT COUNT(*) AS session FROM (SELECT DISTINCT anonymous_id FROM ios_production.product_viewed WHERE  params_product_id IN (' . $this->ps . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')';

        $iosSessionQuery = DB::connection('pgsql')->select($iosSession);

        $sessions = $sessionWebQuery[0]->session + $iosSessionQuery[0]->session;

        return $sessions;
    }

    /**
     * Returns all analytics data for transactions module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getCheckout()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        //checkout

        $checkoutProduct = 0;
        $checkoutQuery   = "Select count(*),json_extract_path_text(product, 'id',true) as product_id from (Select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i,true) AS product,params_store_id from ios_production.checkout_started, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' ) where product_id IN( ' . $this->ps . ') group by params_store_id,product_id';

        $checkoutIos = DB::connection('pgsql')->select($checkoutQuery);

        $checkoutWebQuery =  "Select count(*),json_extract_path_text(product, 'productId',true) as product_id from (Select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id from javascript_production.checkout_started, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) and received_at between " . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' ) where product_id IN( ' . $this->ps . ') group by store_id,product_id';

        $checkoutWeb = DB::connection('pgsql')->select($checkoutWebQuery);

        foreach ($checkoutWeb as $checkoutWeb) {
            $checkoutProduct += $checkoutWeb->count;
            $checkoutProduct += $checkoutWeb->count;
        }

        return $checkoutProduct;
    }

    /**
     * Returns all analytics data for transactions module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getAddToCart()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);
        $addToCart  = 0;

        $query = 'SELECT COUNT(*) AS total_add_to_carts FROM (SELECT COUNT(*), anonymous_id FROM (SELECT DISTINCT anonymous_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner, product_id, user_id, received_at FROM php_production.product_added, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids)) WHERE partner = ' . $this->partner->id . ' AND received_at BETWEEN ' . "'" . $this->from . "'" . ' AND ' . "'" . $this->to . "'" . ' GROUP BY anonymous_id)';

        $productAddToCart = DB::connection('pgsql')->select($query);

        return $productAddToCart[0]->total_add_to_carts;
    }

    /**
     * Returns all analytics data for transactions module.
     */
    public function getTransactions()
    {
        $this->from         = Carbon::parse($this->from);
        $this->to           = Carbon::parse($this->to);
        $transactionProduct = 0;
        $transactionQuery   = 'SELECT COUNT(*), anonymous_id FROM (SELECT DISTINCT anonymous_id, order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT DISTINCT anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN(' . $this->ps . ')) GROUP BY anonymous_id';

        $productTransaction = DB::connection('pgsql')->select($transactionQuery);

        foreach ($productTransaction as $transaction) {
            $transactionProduct++;
        }

        return $transactionProduct;
    }

    /**
     * Returns all analytics data for sales by medium module.
     */
    public function getSalesByMedium()
    {
        $this->from          = Carbon::parse($this->from);
        $this->to            = Carbon::parse($this->to);

        $emailQuery  = 'SELECT SUM(totalSales) as emailSales FROM (SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) * JSON_EXTRACT_PATH_TEXT(product, \'price\', true)) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,received_at FROM (SELECT cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM(SELECT path, anonymous_id, context_page_search FROM javascript_production.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_medium = \'email\' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id)) WHERE path = \'/confirmation\' GROUP BY anonymous_id, path)) WHERE product_id IN(' . $this->ps . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

        $salesByEmail = DB::connection('pgsql')->select($emailQuery);

        $digitialQuery  = 'SELECT SUM(totalSales) as digitialSales FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) * JSON_EXTRACT_PATH_TEXT(product, \'price\', true)) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,received_at FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM(SELECT path, anonymous_id,context_page_search FROM javascript_production.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_medium = \'digital\' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id)) WHERE path = \'/confirmation\' GROUP BY anonymous_id, path)) WHERE product_id IN(' . $this->ps . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     */
    public function getSalesByCampaign()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        $topClickCampaignQuery = 'SELECT context_campaign_name, COUNT(*), SPLIT_PART(context_page_path, \'/\', 3) AS slug FROM javascript_production.pages  WHERE context_campaign_name IS NOT NULL AND slug IN (' . $this->slugImp . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY context_campaign_name, slug ORDER BY COUNT(*) DESC';

        $topCampaignName = DB::connection('pgsql')->select($topClickCampaignQuery);

        if ($topCampaignName) {
            $formattedCampaignSales = [];

            foreach ($topCampaignName as $campaign) {
                $product = Product::where('slug', '=', $campaign->slug)->first()->id;

                // get orders by each campaign name
                $orderQuery = 'SELECT SUM(total_sales) AS email_sales FROM (SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) * JSON_EXTRACT_PATH_TEXT(product, \'price\', true)) AS total_sales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, received_at FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq where seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT DISTINCT anonymous_id FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name =' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $product . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

                $addTocartQuery = 'SELECT COUNT(*) FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT DISTINCT anonymous_id FROM  javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) AND product_id IN (' . $this->ps . ')';

                $transactionQuery = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, order_id FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, timestamp::date FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT DISTINCT anonymous_id FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') AND timestamp::date BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $product . ' GROUP BY product_id, order_id)';

                $sourceQuery = 'SELECT DISTINCT context_campaign_source FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'';

                $sourceQuery                   = DB::connection('pgsql')->select($sourceQuery);
                $addToCart                     = DB::connection('pgsql')->select($addTocartQuery);
                $totalTransactionQuery         = DB::connection('pgsql')->select($transactionQuery);
                $getSalesByCampaign            = DB::connection('pgsql')->select($orderQuery);
                $campaignSales['source']       = $sourceQuery[0]->context_campaign_source;
                $campaignSales['name']         = $campaign->context_campaign_name;
                $campaignSales['sales']        = $getSalesByCampaign[0]->email_sales ?? 0;
                $campaignSales['addToCarts']   = $addToCart[0]->count ?? 0;
                $campaignSales['transactions'] = $totalTransactionQuery[0]->count ?? 0;
                $campaignSales['pageViews']    = $campaign->count ?? 0;

                array_push($formattedCampaignSales, $campaignSales);
            }

            return array_slice($formattedCampaignSales, 0, 5);
        } else {
            return;
        }
    }

    /**
     * Returns all analytics data for sales by campaign module.
     */
    public function getSalesByCampaignBackTrack()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        $query = 'SELECT DISTINCT context_campaign_name, anonymous_id, context_campaign_source FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM (SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'price\', true)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at,anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN (' . $this->ps . ') GROUP BY anonymous_id, product_id)) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_campaign_name IS NOT NULL GROUP BY context_campaign_name, anonymous_id, context_campaign_source';

        $anonymous_id  = DB::connection('pgsql')->select($query);

        $campaignFormatted = [];

        foreach ($anonymous_id as $id) {
            $rev             = [];

            $orderSalesQuery = 'SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'price\', true) * JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM (SELECT order_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN (' . $this->ps . ') AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . 'GROUP BY anonymous_id, product_id';

            $orderSales      =  DB::connection('pgsql')->select($orderSalesQuery);
            $rev['name']     = $id->context_campaign_name;
            $rev['sales']    = $orderSales[0]->retail_price;

            $transactionQuery = 'SELECT COUNT(*), JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id IN (' . $this->ps . ') AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . 'GROUP BY anonymous_id,product_id';

            $transactions        = DB::connection('pgsql')->select($transactionQuery);
            $rev['transactions'] = $transactions[0]->count;

            $addTocartQuery = 'SELECT COUNT(*) FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . ' AND product_id IN (' . $this->ps . ')';

            $addtocarts        = DB::connection('pgsql')->select($addTocartQuery);
            $rev['addToCarts'] = $addtocarts[0]->count;
            $rev['source']     = $id->context_campaign_source;

            $pageViews  = 'SELECT COUNT(*) FROM javascript_production.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_campaign_name = ' . '\'' . $id->context_campaign_name . '\'';

            $views            = DB::connection('pgsql')->select($pageViews);
            $rev['pageViews'] = $views[0]->count;

            array_push($campaignFormatted, $rev);
        }

        return array_slice($campaignFormatted, 0, 5);
    }

    /**
     * Returns all analytics data for sessions by source module.
     */
    public function getProductPage()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        $query     = 'SELECT path, COUNT(*), SPLIT_PART(context_page_path, \'/\', 3) AS slug FROM javascript_production.pages WHERE slug IN (' . $this->slugImp . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY path, slug ORDER BY COUNT(*) DESC LIMIT 5';

        $salesByPages = DB::connection('pgsql')->select($query);

        foreach ($salesByPages as $medium) {
            $salesByPages[$medium->path] = $medium->count;
        }

        return $salesByPages;
    }

    /**
     * Returns all analytics data for sessions by source module.
     */
    public function getSessionsBySource()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        $query = 'SELECT SUM(totalClick), context_campaign_source FROM(SELECT context_campaign_source, COUNT(*) AS totalClick, split_part(context_page_path, \'/\', 3) AS slug FROM javascript_production.pages WHERE slug IN (' . $this->slugImp . ') AND context_campaign_source IS NOT NULL AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY context_campaign_source, slug) GROUP BY context_campaign_source ORDER BY sum DESC LIMIT 5';

        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                $salesSource[$medium->context_campaign_source] = $medium->sum;
            }
        } else {
            return;
        }
    }

    /**
     * Returns all analytics data for page views module.
     */
    public function getProductImpressions()
    {
        $partnerProducts = implode("','", $this->ids);
        $partnerProducts = "'" . $partnerProducts . "'";
        $totalProductImpression = 0;
        $webProductListFiltered = $this->getWebProductListFiltered();
        $webProductListViewed   = $this->getWebProductListViewed();
        $webPromotion           = $this->getWebPromotion();
        $iosProductListFiltered = $this->getIosProductListFiltered();
        $iosProductListViewed   = $this->getIosProductListViewed();
        $iosPromotion           = $this->getIosPromotion();
        $totalProductImpression = $webProductListFiltered + $webProductListViewed + $webPromotion + $iosProductListFiltered + $iosProductListViewed + $iosPromotion;

        return $totalProductImpression;
    }

    public function getWebProductListFiltered()
    {
        $webProductListfiltered = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM javascript_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $webProductListFilter = DB::connection('pgsql')->select($webProductListfiltered);

        return $webProductListFilter[0]->count;
    }

    public function getWebProductListViewed()
    {
        $webProductListViewed = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product,received_at FROM javascript_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $webProductListView = DB::connection('pgsql')->select($webProductListViewed);

        return $webProductListView[0]->count;
    }

    public function getIosProductListFiltered()
    {
        $iosProductListfiltered = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $iosProductListFilter = DB::connection('pgsql')->select($iosProductListfiltered);

        return $iosProductListFilter[0]->count;
    }

    public function getIosProductListViewed()
    {
        $iosProductListViewed = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $iosProductListView = DB::connection('pgsql')->select($iosProductListViewed);

        return $iosProductListView[0]->count;
    }

    public function getWebPromotion()
    {
        $webPromotion = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM javascript_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $webPromotionViewed = DB::connection('pgsql')->select($webPromotion);

        return $webPromotionViewed[0]->count;
    }

    public function getIosPromotion()
    {
        $iosPromotion = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id IN(' . $this->ps . ')';

        $iosPromotionViewed = DB::connection('pgsql')->select($iosPromotion);

        return $iosPromotionViewed[0]->count;
    }

    /**
     * Returns all analytics data for page views module.
     * @param string $this->from
     * @param string $this->to
     * @return void
     */
    public function getProductViews()
    {
        $this->from        = Carbon::parse($this->from);
        $this->to          = Carbon::parse($this->to);
        $totalProductViews = 0;
        $webProductViews   = 'SELECT SUM(click) FROM (SELECT COUNT(*) AS click, product_id, store_id FROM (SELECT product_id, store_id, received_at FROM javascript_production.product_viewed WHERE received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ') where product_id IN(' . $this->ps . ') GROUP BY product_id, store_id)';

        $webProductView = DB::connection('pgsql')->select($webProductViews);

        $iosProductViews = 'SELECT SUM(click) FROM (SELECT COUNT(*) AS click, params_product_id, params_store_id FROM (SELECT params_product_id, params_store_id, received_at FROM ios_production.product_viewed WHERE received_at BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ') WHERE params_product_id IN(' . $this->ps . ') GROUP BY params_product_id,params_store_id )';

        $iosProductView = DB::connection('pgsql')->select($iosProductViews);

        $totalProductViews += $webProductView[0]->sum + $iosProductView[0]->sum;

        return $totalProductViews;
    }
}
