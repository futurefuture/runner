<?php

namespace App\Services\Redshift;

use Analytics;
use App\Category;
use App\Partner;
use App\Product;
use App\Store;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RedShiftProductInfoService
{
    protected $partnerId;
    protected $partner;
    protected $product;
    protected $productInfo;
    protected $productSKU;
    protected $slug;
    protected $productTitle;
    protected $subCategory;
    protected $subCategoryTitle;
    protected $broadCategory;
    protected $broadCategoryTitle;
    protected $productCategory;
    protected $to;
    protected $from;
    protected $totalProductClick           = 0;
    protected $totalUniqueClick            = 0;
    protected $totalProductAddToCart       = 0;
    protected $totalAddToCartDetail        = 0;
    protected $totalBuyToDetail            = 0;
    protected $totalProductUniquePurchases = 0;
    protected $totalQuantity               = 0;
    protected $totalRevenue                = 0;
    protected $totalShares                 = 0;
    protected $totalFavourites             = 0;
    protected $totalProductRevenue         = 0;
    protected $totalProductQuantitySold    = 0;
    protected $totalProductPurchases       = 0;
    protected $storearr                    = [];
    public $phpdatabase                    = 0;
    public $iosdatabase                    = 0;
    public $javascriptdb                   = 0;
    protected $age0;
    protected $age18                       = 0;
    protected $age25                       = 0;
    protected $age35                       = 0;
    protected $age45                       = 0;
    protected $age55                       = 0;
    protected $age65                       = 0;

    public function __construct(Partner $partner, Product $product, $startDate, $endDate)
    {
        $this->from            = Carbon::parse($startDate)->addHour(5);
        $this->to              = Carbon::parse($endDate)->addHour(6);
        $this->partner         = $partner;
        $this->product         = $product;
        $this->productInfo     = $product;
        $this->productSKU      = $product->sku;
        $this->slug            = $product->slug;
        $this->productTitle    = $product->title;
        $this->productCategory = Product::where('id', '=', $this->product->id)
            ->get()
            ->pluck('category_id')
            ->toArray();
        $this->getBroadCategoryAndSubCategory();
        $this->totalProductClick     = 0;
        $this->totalProductAddToCart = 0;
        $this->phpdatabase           = 'php_production';
        $this->iosdatabase           = 'ios_production';
        $this->javascriptdb          = 'javascript_production';
    }

    public function getSubRank()
    {
        $subRankArray = [];

        $subRank                  = $this->getRank($this->subCategory);
        $subRankArray['subTitle'] = $this->subCategoryTitle;
        $subRankArray['rank']     = $subRank;

        return $subRankArray;
    }

    public function getBroadRank()
    {
        $broadRankArray = [];
        $broadRank      = $this->getRank($this->broadCategory);

        $broadRankArray['broadTitle'] = $this->broadCategoryTitle;
        $broadRankArray['rank']       = $broadRank;

        return $broadRankArray;
    }

    protected function getBroadCategoryAndSubCategory()
    {
        // TODO once there is table on rds
        $cat = DB::select('select  c1.id as category_id,c1.title,c1.parent_id as subCategory ,c2.parent_id as broadCategory from   categories c1
        left join categories  c2 on c2.id = c1.parent_id 
        left join  categories c3 on c3.id = c2.parent_id where c1.id =' . $this->productCategory[0]);

        $this->broadCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->subCategory : $cat[0]->broadCategory;

        if ($this->broadCategory != null) {
            $this->broadCategoryTitle = Category::find($this->broadCategory)->title;
        } else {
            $this->broadCategoryTitle = 0;
        }
        $this->subCategory = $cat[0]->broadCategory == 1 || $cat[0]->broadCategory == null ? $cat[0]->category_id : $cat[0]->subCategory;

        if ($this->subCategory != null) {
            $this->subCategoryTitle = Category::find($this->subCategory)->title;
        } else {
            $this->subCategoryTitle = 0;
        }
    }

    protected function getRank($categoryId)
    {
        if ($categoryId != null) {
            $query = 'SELECT product,rank from(Select  product,totRevenue,@curRank := @curRank + 1 AS rank from(SELECT product_id as product,ROUND(SUM(quantity*retail_price)/100,2) as totRevenue FROM order_items WHERE date(created_at) between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' AND category_id IN(SELECT r.id AS child_id FROM categories r LEFT JOIN categories e ON e.id = r.parent_id LEFT JOIN categories e1 on e1.id = e.parent_id WHERE ' . $categoryId . ' IN(r.parent_id,e.parent_id,e1.parent_id) UNION SELECT id FROM categories WHERE id =' . $categoryId . ') group by product_id order by totRevenue desc) as test,(SELECT @curRank := 0) r ) as rank where product=' . $this->product->id;

            $rank = DB::select($query);

            return  $rank ? $rank[0]->rank : 0;
        } else {
            return 0;
        }
    }

    /**
     * Returns overall sales, total orders for given period.
     */
    public function getProductRevenue()
    {
        $query = 'SELECT SUM(revenue) AS revenue FROM (SELECT SUM(retail_price * quantity) AS revenue, order_id FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true) AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\', true) AS retail_price, order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY order_id)';

        $orderItems = DB::connection('pgsql')->select($query);

        return $orderItems[0]->revenue ?? 0;
    }

    public function getTotalPurchases()
    {
        $query = 'SELECT COUNT(*) FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, order_id, store_id, device FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, store_id, device, received_at FROM  php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id;

        $productPurchases = DB::connection('pgsql')->select($query);

        return $productPurchases[0]->count;
    }

    public function getTotalQuantity()
    {
        $query = 'SELECT SUM(quantity) AS quantity FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, order_id, store_id, device FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, store_id, device, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id;

        $quantitySold = DB::connection('pgsql')->select($query);

        return $quantitySold[0]->quantity;
    }

    /**
     * Returns all analytics data for sales by medium module.
     */
    public function getSalesByMedium()
    {
        $emailQuery  = 'SELECT SUM(totalSales) AS emailSales FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,received_at FROM (SELECT cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (select anonymous_id FROM(SELECT path, anonymous_id, context_page_search FROM ' . $this->javascriptdb . '.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM ' . $this->javascriptdb . '.pages WHERE context_campaign_medium = \'email\' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id)) WHERE path = \'/confirmation\' GROUP BY anonymous_id, path)) WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

        $salesByEmail = DB::connection('pgsql')->select($emailQuery);

        // $digitialQuery = 'SELECT SUM(totalSales) as digitialSales FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,timestamp FROM (SELECT cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, timestamp FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND timestamp BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' AND anonymous_id IN (SELECT anonymous_id FROM(SELECT path, anonymous_id,context_page_search FROM ' . $this->javascriptdb . '.pages WHERE timestamp BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' AND anonymous_id IN (SELECT anonymous_id FROM ' . $this->javascriptdb . '.pages WHERE context_campaign_medium = \'Digital\' AND timestamp BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' GROUP BY anonymous_id)) WHERE path = \'/confirmation\' GROUP BY anonymous_id, path)) WHERE product_id = ' . $this->product . ' AND timestamp BETWEEN ' . '"' . $this->from . '"' . ' AND ' . '"' . $this->to . '"' . ' GROUP BY product_id, timestamp)';

        $digitialQuery  = 'SELECT SUM(totalSales) AS digitialSales FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,received_at FROM (SELECT cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM(SELECT path, anonymous_id, context_page_search FROM ' . $this->javascriptdb . '.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM ' . $this->javascriptdb . '.pages WHERE context_campaign_medium = \'Digital\' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id)) WHERE path = \'/confirmation\' GROUP BY anonymous_id, path)) WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

        $noneQuery = 'SELECT SUM(totalSales) AS noneSales FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,received_at FROM (SELECT cogs, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, received_at FROM ' . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM(SELECT path, anonymous_id, context_page_search FROM ' . $this->javascriptdb . '.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT anonymous_id FROM ' . $this->javascriptdb . '.pages WHERE context_campaign_medium IS NULL AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id)) WHERE path LIKE \'%/product/' . $this->slug . '\%\' GROUP BY anonymous_id, path)) WHERE product_id = ' . $this->product->id . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY product_id, received_at)';

        $salesByDigitial = DB::connection('pgsql')->select($digitialQuery);
        $salesByNone     = DB::connection('pgsql')->select($noneQuery);

        return [
            'email'    => $salesByEmail[0]->emailsales,
            'digitial' => $salesByDigitial[0]->digitialsales,
            'none'     => $salesByNone[0]->nonesales,
        ];
    }

    /**
     * Returns all analytics data for sales by campaign module.
     */
    public function getSalesByCampaignBackTrack()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        $query = 'SELECT DISTINCT context_campaign_name, anonymous_id, context_campaign_source FROM javascript_production.pages WHERE anonymous_id IN(SELECT anonymous_id FROM(SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'price\', true)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM(SELECT order_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id = ' . $this->product->id . ' GROUP BY anonymous_id,product_id)) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_campaign_name IS NOT NULl GROUP BY context_campaign_name, anonymous_id, context_campaign_source';

        $anonymous_id  = DB::connection('pgsql')->select($query);

        $campaignFormatted = [];

        foreach ($anonymous_id as $id) {
            $rev             = [];
            $orderSalesQuery = 'SELECT SUM(JSON_EXTRACT_PATH_TEXT(product, \'price\', true) * JSON_EXTRACT_PATH_TEXT(product, \'quantity\', true)) AS retail_price, JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id = ' . $this->product->id . ' AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . ' GROUP BY anonymous_id, product_id';

            $orderSales   =  DB::connection('pgsql')->select($orderSalesQuery);
            $rev['name']  = $id->context_campaign_name;
            $rev['sales'] = $orderSales[0]->retail_price;

            $transactionQuery = 'SELECT COUNT(*), JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id, anonymous_id FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, store_id, received_at, anonymous_id FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id = ' . $this->product->id . ' AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . ' GROUP BY anonymous_id, product_id';

            $transactions       = DB::connection('pgsql')->select($transactionQuery);
            $rev['transaction'] = $transactions[0]->count;

            $addTocartQuery = 'SELECT COUNT(*) FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id = ' . '\'' . $id->anonymous_id . '\'' . ' AND product_id = ' . $this->product->id;

            $addtocarts        = DB::connection('pgsql')->select($addTocartQuery);
            $rev['addToCarts'] = $addtocarts[0]->count;
            $rev['source']     = $id->context_campaign_source;

            $pageViews = 'SELECT COUNT(*) FROM javascript_production.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_campaign_name = ' . '\'' . $id->context_campaign_name . '\'';

            $views            = DB::connection('pgsql')->select($pageViews);
            $rev['pageViews'] = $views[0]->count;

            array_push($campaignFormatted, $rev);
        }

        return array_slice($campaignFormatted, 0, 5);
    }

    /**
     * Returns all analytics data for sales by campaign module.
     */
    public function getSalesByCampaign()
    {
        $topClickCampaignQuery = 'SELECT context_campaign_name, COUNT(*), SPLIT_PART(context_page_path, \'/\', 3) AS slug FROM javascript_production.pages WHERE context_campaign_name IS NOT NULL AND slug = ' . '\'' . $this->product->slug . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY context_campaign_name, slug ORDER BY COUNT(*) DESC';

        $topCampaignName        = DB::connection('pgsql')->select($topClickCampaignQuery);
        $formattedCampaignSales = [];

        foreach ($topCampaignName as $campaign) {
            // get orders by each campaign name
            $orderQuery = 'SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales FROM (SELECT anonymous_id,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT DISTINCT anonymous_id FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_page_path = ' . '\'/product/\'' . $this->product->slug . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY product_id';

            $addTocartQuery = 'SELECT COUNT(*) FROM php_production.product_added WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN(SELECT DISTINCT anonymous_id FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND path = ' . '\'/product/' . $this->product->slug . '\'' . ')) AND product_id = ' . $this->product->id;

            $addToCart          = DB::connection('pgsql')->select($addTocartQuery);
            $getSalesByCampaign = DB::connection('pgsql')->select($orderQuery);

            $transactionQuery = 'SELECT COUNT(*) FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, SUM(JSON_EXTRACT_PATH_TEXT(product, \'quantity\') * JSON_EXTRACT_PATH_TEXT(product, \'price\')) AS totalSales, order_id FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND anonymous_id IN (SELECT DISTINCT anonymous_id FROM javascript_production.pages WHERE anonymous_id IN (SELECT anonymous_id FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_page_path = ' . '\'/product/' . $this->product->slug . '\'' . ')) WHERE product_id = ' . $this->product->id . ' GROUP BY product_id, order_id)';

            $sourceQuery = 'SELECT DISTINCT context_campaign_source FROM javascript_production.pages WHERE context_campaign_name = ' . '\'' . $campaign->context_campaign_name . '\'' . ' AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'';

            $sourceQuery = DB::connection('pgsql')->select($sourceQuery);

            $totalTransactionQuery         = DB::connection('pgsql')->select($transactionQuery);
            $campaignSales['source']       = $sourceQuery[0]->context_campaign_source;
            $campaignSales['name']         = $campaign->context_campaign_name;
            $campaignSales['sales']        = $getSalesByCampaign[0]->totalsales ?? 0;
            $campaignSales['addToCarts']   = $addToCart[0]->count ?? 0;
            $campaignSales['transactions'] = $totalTransactionQuery[0]->count ?? 0;
            $campaignSales['pageViews']    = $campaign->count ?? 0;

            array_push($formattedCampaignSales, $campaignSales);
        }

        return array_slice($formattedCampaignSales, 0, 5);
    }

    /**
     * Returns all analytics data for sessions by source module.
     */
    public function getSessionsBySource()
    {
        $query = 'SELECT context_campaign_source, COUNT(*) FROM(SELECT context_campaign_source, context_page_path, COUNT(*), anonymous_id FROM ' . $this->javascriptdb . '.pages WHERE received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' AND context_page_path LIKE \'%/product/' . $this->slug . '%\' GROUP BY context_campaign_source,context_page_path, anonymous_id) GROUP BY context_campaign_source';

        $salesBySource = DB::connection('pgsql')->select($query);

        if ($salesBySource) {
            foreach ($salesBySource as $medium) {
                if ($medium->context_campaign_source == null) {
                    $salesSource['none'] = $medium->count;
                } else {
                    $salesSource[$medium->context_campaign_source] = $medium->count;
                }
            }

            return $salesSource;
        } else {
            return;
        }
    }

    public function getProductAnalytics()
    {
        // Get Product Clic for both Ios and Mobile
        $webPageViews = $this->getWebPageViews();

        $productOverallAnalytics = [
            'runnerAnalytics'  => [
                'totalRevenue'          => $this->totalProductRevenue,
                'totalPurchases'        => $this->totalProductPurchases,
                'totalQuantity'         => $this->totalProductQuantitySold,
            ],
            'googleAnalytics' => [
                'totalPageViews'        => $this->totalProductClick,
                'totalUniquePageViews'  => $this->totalUniqueClick,
                'totalAddToCarts'       => $this->totalProductAddToCart,
                'totalFavourites'       => $this->totalFavourites,
            ],
            'storeFronts'              => $webPageViews,
        ];

        return response()->json([
            'data' => [
                'type'       => 'products',
                'id'         => (string) $this->product,
                'SKU'        => $this->productSKU,
                'title'      => $this->productTitle,
                'attributes' => $productOverallAnalytics,
            ],
        ]);
    }

    public function getWebPageViews()
    {
        $productAnalytics       = [];
        $productJavascriptClick = $this->getProductClick();

        $productIosData   = $this->getProductIosClick();
        $productPurchases = $this->getPurchases();
        $productQuantity  = $this->getQuantity();
        $productRevenue   = $this->getRevenue();

        $store = Store::where('is_active', 1)->pluck('id')->toArray();

        $storefront = array_intersect(array_unique($this->storearr), $store);

        foreach ($storefront as $key => $val) {
            $webStore                    = [];
            $iosStore                    = [];
            $storeInfo                   = Store::find($val);
            $webStore['pageViews']       = $productJavascriptClick[$val]['pageView'] ?? 0;
            $webStore['uniquePageViews'] = $productJavascriptClick[$val]['uniquePageViews'] ?? 0;
            $webStore['addToCart']       = $productJavascriptClick[$val]['addToCart'] ?? 0;

            if (isset($productPurchases)) {
                foreach ($productPurchases as $purchases) {
                    if ($purchases->store_id == $val && $purchases->device == 1) {
                        $webStore['purchases'] = $purchases->count;
                    }
                }
            }

            if (isset($productQuantity)) {
                foreach ($productQuantity as $quantity) {
                    if ($quantity->store_id == $val && $quantity->device == 1) {
                        $webStore['quantitySold'] = $quantity->quantity;
                    }
                }
            }

            if (isset($productRevenue)) {
                foreach ($productRevenue as $rev) {
                    if ($rev->store_id == $val && $rev->device == 1) {
                        $webStore['revenue'] = $rev->revenue;
                    }
                }
            }

            $webStore['cartToDetailRate'] = $webStore['pageViews'] > 0 && $webStore['addToCart'] > 0 ? $webStore['addToCart'] / $webStore['pageViews'] : 0;

            if (isset($webStore['uniquePageViews']) && isset($webStore['purchases'])) {
                $webStore['buyToDetailRate'] = $webStore['uniquePageViews'] > 0 && $webStore['purchases'] > 0 ? $webStore['purchases'] / $webStore['uniquePageViews'] : 0;
            } else {
                $webStore['buyToDetailRate'] = 0;
            }

            $webStore['shares']     = 10;
            $webStore['favourites'] = $productJavascriptClick[$val]['favuorite'] ?? 0;

            $iosStore['pageViews']        = $productIosData[$val]['pageView'] ?? 0;
            $iosStore['uniquePageViews']  = $productIosData[$val]['uniquePageViews'] ?? 0;
            $iosStore['addToCart']        = $productIosData[$val]['addToCart'] ?? 0;
            $iosStore['cartToDetailRate'] = $iosStore['pageViews'] > 0 && $iosStore['addToCart'] > 0 ? $iosStore['addToCart'] / $iosStore['pageViews'] : 0;

            if (isset($iosStore['uniquePageViews']) && isset($iosStore['purchases'])) {
                $iosStore['buyToDetailRate'] = $iosStore['uniquePageViews'] > 0 && $iosStore['purchases'] > 0 ? $iosStore['purchases'] / $iosStore['uniquePageViews'] : 0;
            } else {
                $iosStore['buyToDetailRate'] = 0;
            }
            $iosStore['shares'] = 10;

            // if (isset($productJavascriptClick[$val]['device'])) {

            //     $iosStore['purchases']        =   $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['purchases'] : 0;
            //     $iosStore['quantitySold']     =  $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['quantitySold'] : 0;
            //     $iosStore['revenue']          =  $productJavascriptClick[$val]['device'] == 3 ? $productJavascriptClick[$val]['revenue'] : 0;
            // }
            if (isset($productPurchases)) {
                foreach ($productPurchases as $purchases) {
                    if ($purchases->store_id == $val && $purchases->device == 3) {
                        $iosStore['purchases'] = $purchases->count;
                    }
                }
            }

            if (isset($productQuantity)) {
                foreach ($productQuantity as $quantity) {
                    if ($quantity->store_id == $val && $quantity->device == 3) {
                        $iosStore['quantitySold'] = $quantity->quantity;
                    }
                }
            }

            if (isset($productRevenue)) {
                foreach ($productRevenue as $rev) {
                    if ($rev->store_id == $val && $rev->device == 3) {
                        $iosStore['revenue'] = $rev->revenue;
                    }
                }
            }

            $this->totalProductClick += $webStore['pageViews'] + $iosStore['pageViews'];
            $this->totalUniqueClick += $webStore['uniquePageViews'] + $iosStore['uniquePageViews'];
            $this->totalProductAddToCart += $webStore['addToCart'] + $iosStore['addToCart'];
            $this->totalShares += $webStore['shares'] + $iosStore['shares'];
            $this->totalFavourites += $webStore['favourites'];

            if (isset($webStore['revenue']) || isset($iosStore['revenue'])) {
                $this->totalProductRevenue += $webStore['revenue'] ?? 0;
                $this->totalProductRevenue += $iosStore['revenue'] ?? 0;
            }
            if (isset($webStore['purchases']) || isset($iosStore['purchases'])) {
                $this->totalProductPurchases += $webStore['purchases'] ?? 0;
                $this->totalProductPurchases += $iosStore['purchases'] ?? 0;
            }
            if (isset($webStore['quantitySold']) || isset($iosStore['quantitySold'])) {
                $this->totalProductQuantitySold += $webStore['quantitySold'] ?? 0;
                $this->totalProductQuantitySold += $iosStore['quantitySold'] ?? 0;
            }

            $productAnalytics[$val] = [
                'store'  => [
                    'title' => $storeInfo->title,
                    'logo'  => $storeInfo->options['storeLogo'],
                ],
                'pageViews' => [
                    'web' => $webStore['pageViews'] ?? 0,
                    'ios' => $iosStore['pageViews'] ?? 0,
                ],
                'uniquePageViews' => [
                    'web'   => $webStore['uniquePageViews'] ?? 0,
                    'ios'   => $iosStore['uniquePageViews'] ?? 0,
                ],
                'addToCarts' => [
                    'web' => $webStore['addToCart'] ?? 0,
                    'ios' => $iosStore['addToCart'] ?? 0,
                ],
                'cartToDetailRate' => [
                    'web'  => $webStore['cartToDetailRate'] ?? 0,
                    'ios'  => $iosStore['cartToDetailRate'] ?? 0,
                ],
                'buyToDetailRate'  => [
                    'web'  => $webStore['buyToDetailRate'] ?? 0,
                    'ios'  => $iosStore['buyToDetailRate'] ?? 0,
                ],
                'shares' => [
                    'web'  => $webStore['shares'] ?? 0,
                    'ios'  => $iosStore['shares'] ?? 0,
                ],
                'favourites' => [
                    'web'  => $webStore['favourites'] ?? 0,
                    'ios'  => $iosStore['favourites'] ?? 0,
                ],
                'purchases' => [
                    'web'  => $webStore['purchases'] ?? 0,
                    'ios'  => $iosStore['purchases'] ?? 0,
                ],
                'quantitySold' => [
                    'web'  => $webStore['quantitySold'] ?? 0,
                    'ios'  => $iosStore['quantitySold'] ?? 0,
                ],
                'revenue' => [
                    'web'  => $webStore['revenue'] ?? 0,
                    'ios'  => $iosStore['revenue'] ?? 0,
                ],
            ];
        }

        return $productAnalytics;
    }

    protected function getProductClick()
    {
        $query = 'SELECT count(*),product_id,store_id FROM ' . $this->javascriptdb . '.product_viewed where product_id =' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' group by product_id,store_id';

        $product_list_clicked_frontend = DB::connection('pgsql')->select($query);

        $javascriptClick = [];
        foreach ($product_list_clicked_frontend as $row) {
            $this->storearr[]                = $row->store_id;
            $javascriptClick[$row->store_id] = [
                'pageView' => $row->count,
            ];
        }

        return $this->getProductUniqueClick($javascriptClick);
    }

    protected function getProductIosClick()
    {
        $query = 'SELECT count(*),params_product_id,params_store_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id =' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and params_product_id =' . $this->product . ' group by params_product_id,params_store_id';

        $product_list_clicked_ios = DB::connection('pgsql')->select($query);

        $iosClick = [];
        foreach ($product_list_clicked_ios as $row) {
            $this->storearr[]                = $row->params_store_id;
            $iosClick[$row->params_store_id] = [
                'pageView' => $row->count,
            ];
        }

        return $this->getProductUniqueIosClick($iosClick);
    }

    protected function getProductUniqueClick($javascriptClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_frontend =  DB::connection('pgsql')->select('Select count(*) as uniqueView,store_id from(SELECT count(*),product_id,store_id,anonymous_id FROM ' . $this->javascriptdb . '.product_viewed where product_id = ' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . 'group by product_id,store_id,anonymous_id ) group by store_id');

        foreach ($product_unique_clicked_frontend as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $javascriptClick[$arr->store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        return $this->getAddToCart($javascriptClick);
    }

    protected function getProductUniqueIosClick($iosClick)
    {
        // TODO once IOS table is ready add that value into it
        $product_unique_clicked_ios =  DB::connection('pgsql')->select('Select count(*) as uniqueView,params_store_id from(SELECT count(*),params_product_id,params_store_id,anonymous_id FROM ' . $this->iosdatabase . '.product_viewed where params_product_id = ' . $this->product . ' and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and params_product_id =' . $this->product . 'group by params_product_id,params_store_id,anonymous_id ) group by params_store_id');

        foreach ($product_unique_clicked_ios as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->params_store_id;
                if ($arr->params_store_id == $key) {
                    $iosClick[$key]['uniquePageViews'] = $arr->uniqueview;
                } else {
                    $iosClick[$arr->params_store_id]['uniquePageViews'] = $arr->uniqueview;
                }
            }
        }

        return $this->getAddToCartIos($iosClick);
    }

    protected function getAddToCart($javascriptClick)
    {
        $query = 'select anonymous_id,product_id,user_id,store_id,received_at from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,store_id,received_at from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =' . $this->partner->id . 'and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and product_id =' . $this->product . ' and anonymous_id is not null';

        $productAddToCart = DB::connection('pgsql')->select($query);
        $cartCount        = 0;
        $cartCount1       = 0;
        foreach ($productAddToCart as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                $productViewQuery = 'select count(*),product_id from javascript_production.product_viewed where anonymous_id =' . "'" . $arr->anonymous_id . "'" . ' and product_id = ' . $arr->product_id . ' and received_at =' . "'" . $arr->received_at . "'" . ' group by product_id';

                $productViewWebQuery = DB::connection('pgsql')->select($productViewQuery);

                if ($productViewWebQuery) {
                    if ($arr->store_id == $key) {
                        $cartCount                          = $cartCount + $productViewWebQuery[0]->count;
                        $javascriptClick[$key]['addToCart'] = $cartCount;
                    } else {
                        $cartCount1                                   = $cartCount1 + $productViewWebQuery[0]->count;
                        $javascriptClick[$arr->store_id]['addToCart'] = $cartCount1;
                    }
                }
            }
        }

        return $this->getFavourite($javascriptClick);
    }

    protected function getAddToCartIos($iosClick)
    {
        $query = 'select anonymous_id,product_id,user_id,store_id,received_at from(select JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner,product_id,uuid,anonymous_id,user_id,store_id,received_at from php_production.product_added ,seq_0_to_20 AS seq  WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids))  where partner =' . $this->partner->id . 'and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ' and product_id =' . $this->product . ' and anonymous_id is not null';

        $productAddToCart = DB::connection('pgsql')->select($query);
        $cartCount        = 0;
        $cartCount1       = 0;
        foreach ($productAddToCart as $arr) {
            foreach ($iosClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;

                $productViewQuery = 'select count(*),params_product_id from ios_production.product_viewed where anonymous_id =' . "'" . $arr->anonymous_id . "'" . ' and params_product_id = ' . $arr->product_id . ' and received_at =' . "'" . $arr->received_at . "'" . ' group by params_product_id';

                $productViewIosQuery = DB::connection('pgsql')->select($productViewQuery);

                if ($productViewIosQuery) {
                    if ($arr->store_id == $key) {
                        $cartCount = $cartCount + $productViewIosQuery[0]->count;
                    } else {
                        $cartCount1                            = $cartCount1 + $productViewIosQuery[0]->count;
                        $iosClick[$arr->store_id]['addToCart'] = $productViewIosQuery[0]->count;
                    }
                }
            }
        }

        return $iosClick;
    }

    public function getPurchases()
    {
        $query = "SELECT count(*),product_id,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM  " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $productPurchases = DB::connection('pgsql')->select($query);

        foreach ($productPurchases as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $productPurchases;
    }

    public function getQuantity()
    {
        // TODO once IOS table is ready add that value into it
        $query = "SELECT count(*),product_id,SUM(quantity) as quantity,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,store_id,device,received_at FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $quantitySold = DB::connection('pgsql')->select($query);

        foreach ($quantitySold as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return $quantitySold;
    }

    protected function getFavourite($javascriptClick)
    {
        $query = 'Select count(*),product_id,store_id from ' . $this->javascriptdb . '.product_favourited where product_id = ' . $this->product . ' group by product_id,store_id';

        $favourite = DB::connection('pgsql')->select($query);

        foreach ($favourite as $arr) {
            foreach ($javascriptClick as $key => $row[1]) {
                $this->storearr[] = $arr->store_id;
                if ($arr->store_id == $key) {
                    $javascriptClick[$key]['favuorite'] = $arr->count;
                } else {
                    $javascriptClick[$arr->store_id]['favuorite'] = $arr->count;
                }
            }
        }

        return $javascriptClick;
    }

    protected function getRevenue()
    {
        $query = "SELECT count(*),product_id,SUM(quantity*retail_price) as revenue ,store_id,device from(select json_extract_path_text(product, 'productId') as product_id,json_extract_path_text(product, 'quantity') as quantity,json_extract_path_text(product, 'price') as retail_price,order_id,store_id,device from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product,cogs,store_id,device,received_at FROM " . $this->phpdatabase . '.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) and received_at between ' . "'" . $this->from . "'" . ' and ' . "'" . $this->to . "'" . ')) where product_id = ' . $this->product . ' group by product_id,store_id,device';

        $revenue = DB::connection('pgsql')->select($query);

        foreach ($revenue as $arr) {
            $this->storearr[] = $arr->store_id;
        }

        return  $revenue;
    }

    public function getTopPostalCode()
    {
        $query = 'SELECT extractCode, COUNT(DISTINCT order_id) AS totalOrderCount FROM(SELECT SUBSTRING(postal_code, 1, 3) AS extractCode, order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, order_id, postal_code FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ') WHERE product_id = ' . $this->product->id . ') GROUP BY extractCode ORDER BY totalOrderCount DESC LIMIT 5';

        $topPostalCode = DB::connection('pgsql')->select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    public function getSalesByGender()
    {
        $maleSale   = 0;
        $femaleSale = 0;

        $genderWebQuery = 'SELECT gender, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' AND device = 1) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id, gender HAVING gender IS NOT NULL';

        $genderDbWebQuery = DB::connection('pgsql')->select($genderWebQuery);

        foreach ($genderDbWebQuery as $genderWeb) {
            if ($genderWeb->gender == 'male') {
                $maleSale++;
            } elseif ($genderWeb->gender == 'female') {
                $femaleSale++;
            }
        }

        $genderIosQuery = 'SELECT gender, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM(SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' and device = 3) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id, gender HAVING gender IS NOT NULL';

        $genderDbIosQuery = DB::connection('pgsql')->select($genderIosQuery);

        foreach ($genderDbIosQuery as $genderIos) {
            if ($genderIos->gender == 'male') {
                $maleSale++;
            } elseif ($genderIos->gender == 'female') {
                $femaleSale++;
            }
        }

        return response()->json([
            'data'   => [
                'type'       => 'Gender',
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale,
                ],
            ],
        ]);
    }

    public function getSalesByAge()
    {
        $ageWebQuery = 'SELECT age, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM (SELECT device,anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' AND device = 1) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY anonymous_id, age HAVING age IS NOT NULL';

        $ageWebDbQuery = DB::connection('pgsql')->select($ageWebQuery);

        foreach ($ageWebDbQuery as $webage) {
            $this->ageDistribution($webage->age ?? 0);
        }

        $ageIosQuery = 'SELECT age, LOWER(anonymous_id), COUNT(*) FROM ios_production.identifies WHERE LOWER(anonymous_id) IN(SELECT anonymous_id FROM (SELECT device,anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id . ' AND device = 3) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ' GROUP BY LOWER(anonymous_id), age HAVING age IS NOT NULL';

        $ageIosDbQuery = DB::connection('pgsql')->select($ageIosQuery);

        foreach ($ageIosDbQuery as $iosage) {
            $this->ageDistribution($iosage->age ?? 0);
        }

        return response()->json([
            'data'   => [
                'type'       => 'demographic age distribution',
                'attributes' => [
                    '18-24' => $this->age18 ?? 0,
                    '25-34' => $this->age25 ?? 0,
                    '35-44' => $this->age35 ?? 0,
                    '45-54' => $this->age45 ?? 0,
                    '55-64' => $this->age55 ?? 0,
                    '65+'   => $this->age65 ?? 0,
                ],
            ],
        ]);
    }

    protected function ageDistribution($age)
    {
        switch ($age) {
            case $age > 18 && $age <= 24:
                $this->age18++;
                break;
            case $age > 24 && $age <= 34:
                $this->age25++;
                break;
            case $age > 35 && $age <= 44:
                $this->age35++;
                break;
            case $age > 45 && $age <= 54:
                $this->age45++;
                break;
            case $age > 54 && $age <= 64:
                $this->age55++;
                break;
            case $age > 65:
                $this->age65++;
                break;
            default:
                $this->age0 = 0;
        }
    }

    public function getProductImpressions()
    {
        $this->from = Carbon::parse($this->from);
        $this->to   = Carbon::parse($this->to);

        //TODO after json rds issue will be fixed
        $totalProductImpression = 0;

        $webProductListFiltered = $this->getWebProductListFiltered();
        $webProductListViewed   = $this->getWebProductListViewed();
        $webPromotion           = $this->getWebPromotion();

        $iosProductListFiltered = $this->getIosProductListFiltered();
        $iosProductListViewed   = $this->getIosProductListViewed();
        $iosPromotion           = $this->getIosPromotion();

        $totalProductImpression = $webProductListFiltered + $webProductListViewed + $webPromotion + $iosProductListFiltered + $iosProductListViewed + $iosPromotion;

        return $totalProductImpression;
    }

    public function getWebProductListFiltered()
    {
        $webProductListfiltered   = 'SELECT count(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM javascript_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id;

        $webProductListFilter = DB::connection('pgsql')->select($webProductListfiltered);

        return $webProductListFilter[0]->count;
    }

    public function getWebProductListViewed()
    {
        $webProductListViewed = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM javascript_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . '))  WHERE product_id = ' . $this->product->id;

        $webProductListView = DB::connection('pgsql')->select($webProductListViewed);

        return $webProductListView[0]->count;
    }

    public function getIosProductListFiltered()
    {
        $iosProductListfiltered   = 'SELECT COUNT(*) FROM(SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.product_list_filtered, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id;
        $iosProductListFilter     = DB::connection('pgsql')->select($iosProductListfiltered);

        return $iosProductListFilter[0]->count;
    }

    public function getIosProductListViewed()
    {
        $iosProductListViewed = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.product_list_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . '))  WHERE product_id = ' . $this->product->id;

        $iosProductListView = DB::connection('pgsql')->select($iosProductListViewed);

        return $iosProductListView[0]->count;
    }

    public function getWebPromotion()
    {
        $webPromotion = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i, true) AS product, received_at FROM javascript_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products, true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . '))  WHERE product_id = ' . $this->product->id;

        $webPromotionViewed = DB::connection('pgsql')->select($webPromotion);

        return $webPromotionViewed[0]->count;
    }

    public function getIosPromotion()
    {
        $iosPromotion = 'SELECT COUNT(*) FROM (SELECT JSON_EXTRACT_PATH_TEXT(product, \'productId\', true) AS product_id FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_products, seq.i, true) AS product, received_at FROM ios_production.promotion_viewed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(params_products,true) AND received_at BETWEEN ' . '\'' . $this->from . '\'' . ' AND ' . '\'' . $this->to . '\'' . ')) WHERE product_id = ' . $this->product->id;

        $iosPromotionViewed = DB::connection('pgsql')->select($iosPromotion);

        return $iosPromotionViewed[0]->count;
    }
}
