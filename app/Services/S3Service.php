<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class S3Service
{
    public function addSignature($signature, $order)
    {
        $signature = Storage::disk('order_signatures')->put($order->id.'.png', base64_decode(str_replace('data:image/png;base64,', '', $signature)));
        $url = Storage::disk('order_signatures')->url($order->id.'.png');

        return $url;
    }

    public function addTaskSignature($signature, $task)
    {
        $signature = Storage::disk('runner-task-signatures')
            ->put($task->id.'.jpg', base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $signature)), 'public');

        $url = Storage::disk('runner-task-signatures')->url($task->id.'.jpg');

        return $url;
    }
}
