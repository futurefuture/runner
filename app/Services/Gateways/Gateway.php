<?php

namespace App\Services\Gateways;

use App\Services\MailChimpService;

class Gateway
{
    const acceptedGateways = ['stripe', 'mailChimp'];

    public static function mocking()
    {
        return config('mocking') !== 0;
    }

    public static function noMocking()
    {
        return config(['mocking' => 0]);
    }

    public static function via($gateway = 'stripe', $user = null)
    {
        if (($gateway === 'testing') && app()->environment('testing') && self::mocking()) {
            return new TestingGateway;
        }

        if (! in_array($gateway, self::acceptedGateways)) {
            abort(400, 'INVALID_GATEWAY');
        }

        switch ($gateway) {
            case 'stripe':
                return new StripeService;

                break;
            case 'mailChimp':
                return new MailChimpService($user);

                break;
        }
    }
}
