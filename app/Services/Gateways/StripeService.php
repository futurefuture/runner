<?php

namespace App\Services\Gateways;

use App\Address;
use App\Exceptions\StripeCardErrorException;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Refund;
use Stripe\Stripe;

class StripeService extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(getenv('STRIPE_SECRET'));
    }

    public function settle($order, $user, $data)
    {
        $address      = Address::where('user_id', $user->id)->where('selected', 1)->first();
        $chargeAmount = $order->total;
        if (isset($data['stripeToken'])) {
            $stripeToken = $data['stripeToken'];
        }

        if (($user->email == 'jake@wearefuturefuture.com' || $user->email == 'jarek@wearefuturefuture.com' || $user->email == 'dummy@getrunner.io' && env('APP_ENV') == 'production')) {
            $chargeAmount = 51;
        }

        if (! empty($stripeToken)) {
            $charge = \Stripe\Charge::create([
                'amount'      => $chargeAmount,
                'currency'    => 'cad',
                'source'      => $stripeToken,
                'description' => 'Order',
                'metadata'    => [
                    'customer'     => $user->first_name . ' ' . $user->last_name,
                    'email'        => $user->email,
                    'phone'        => $user->phone_number,
                    'address'      => $address->formatted_address,
                    'instructions' => $address->instructions,
                    'time'         => $data['orderDateTime'],
                    'total'        => number_format($chargeAmount / 100, 2, '.', ','),
                    'tax'          => number_format($order->display_tax_total / 100, 2, '.', ','),
                    'delivery'     => number_format($order->delivery / 100, 2, '.', ','),
                    'discount'     => number_format($order->discount / 100, 2, '.', ','),
                    'tip'          => number_format($order->tip / 100, 2, '.', ','),
                    'subtotal'     => number_format($order->subtotal / 100, 2, '.', ','),
                    'appVersion'   => $data['appVersion'],
                ],
                'capture' => false,
            ]);
        } else {
            $charge = Charge::create([
                'amount'        => $chargeAmount,
                'currency'      => 'cad',
                'customer'      => $user->stripe_id,
                'description'   => 'Order',
                'metadata'      => [
                    'customer'     => $user->first_name . ' ' . $user->last_name,
                    'email'        => $user->email,
                    'phone'        => $user->phone_number,
                    'address'      => $address->formatted_address,
                    'instructions' => $address->instructions,
                    'time'         => $data['orderDateTime'],
                    'total'        => number_format($chargeAmount / 100, 2, '.', ','),
                    'tax'          => number_format($order->display_tax_total / 100, 2, '.', ','),
                    'delivery'     => number_format($order->delivery / 100, 2, '.', ','),
                    'discount'     => number_format($order->discount / 100, 2, '.', ','),
                    'tip'          => number_format($order->tip / 100, 2, '.', ','),
                    'subtotal'     => number_format($order->subtotal / 100, 2, '.', ','),
                    'appVersion'   => $data['appVersion'],
                ],
                'capture'     => false,
            ]);
        }

        return (object) [
            'transaction_id' => $charge->id,
            'last4'          => $charge->source->last4,
        ];
    }

    public function dispatchSettle($chargeAmount, $order)
    {
        try {
            $charge = Charge::create([
                'amount'      => $chargeAmount,
                'currency'    => 'cad',
                'customer'    => $order->user->stripe_id,
                'description' => 'extra charge for order number ' . $order->id,
                'capture'     => false,
            ]);

            return (object) [
                'transaction_id' => $charge->id,
                'last4'          => $charge->source->last4,
            ];
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        }
    }

    public function retrieveCharge($charge)
    {
        try {
            $chargeDetail = Charge::retrieve($charge);

            return  $chargeDetail->amount;
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        }
    }

    public function refund($amount, $charge)
    {
        try {
            $refund = Refund::create([
                'charge' => $charge,
                'amount' => $amount,
            ]);

            return (object) [
                'refundId' => $refund->id,
            ];
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $err;
        }
    }

    public function storeCustomer($request)
    {
        try {
            $customer = Customer::create([
                'description' => ucfirst(strtolower($request['first_name'])),
                'email'       => strtolower($request['email']),
            ]);

            return $customer;
        } catch (\Stripe\Error\Base $e) {
            return response()->json([
                'errors'      => [
                    'error' => 'There has been an error in registering your account.',
                ],
            ], 401);
        }
    }

    public function getCustomer()
    {
        $user = Auth::user();

        $stripeCustomer = Customer::retrieve($user->stripe_id);

        return response()->json([
            'data' => [
                'type'       => 'stripe customers',
                'id'         => $user->stripe_id,
                'attributes' => $stripeCustomer,
            ],
        ]);
    }

    public function createSource(Request $request)
    {
        $user       = Auth::user();
        $zipMessage = 'The Zip Code associated with your card is incorrect. If you aleady have a card added, please delete and re-enter your credit card information.';

        try {
            $customer = Customer::retrieve($user->stripe_id);
            $source   = $customer->sources->create([
                'source' => $request->input('data')['id'],
            ]);
            $customer->default_source = $source->id;
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            if (strpos($err['message'], 'zip') == true) {
                return response()->json([
                    'errors' => [
                        'error' => $zipMessage,
                    ],
                ], 400);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => $err['message'],
                    ],
                ], 400);
            }

            return response()->json([
                'errors' => [
                    'error' => $z,
                ],
            ], 400);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'errors' => [
                    'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
                ],
            ]);
        }

        return $this->getCustomer();
    }

    public function updateSource(Request $request, $userId, $sourceId)
    {
        $user = Auth::user();

        try {
            $customer                 = Customer::retrieve($user->stripe_id);
            $customer->default_source = $sourceId;
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return response()->json([
                'errors' => [
                    'error' => $err['message'] . ' { CODE: ' . $err['code'] . ' } ',
                ],
            ], 400);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'errors' => [
                    'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
                ],
            ]);
        }

        return $this->getCustomer();
    }

    public function deleteSource(Request $request, $userId, $sourceId)
    {
        $user = Auth::user();

        $customer = Customer::retrieve($user->stripe_id);
        $customer->sources->retrieve($sourceId)->delete();

        return $this->getCustomer();
    }
}
