<?php

namespace App\Services;

use App\User;
use App\Order;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DrewM\MailChimp\MailChimp;

class MailChimpService
{
    protected $user;
    protected $mailchimp;
    protected $mailchimpApi;
    protected $mailchimpList;

    public function __construct(User $user = null)
    {
        $this->user          = $user;
        $this->mailchimpApi  = env('MAILCHIMP_APIKEY');
        $this->mailchimpList = env('MAILCHIMP_LIST_ID');
        $this->mailchimp     = new Mailchimp($this->mailchimpApi);
    }

    public function addSubscriber()
    {
        $birthday = Carbon::parse($this->user->birthday)->format('m/d');

        $this->mailchimp->post('lists/' . $this->mailchimpList . '/members/', [
                'email_address' => $this->user->email,
                'status'        => 'subscribed',
                'merge_fields'  => [
                    'FNAME'    => $this->user->first_name,
                    'LNAME'    => $this->user->last_name,
                    'PHONE'    => $this->user->phone_number,
                    'BIRTHDAY' => $birthday,
                ],
            ]);

        return $this->mailchimp->getLastResponse();
    }

    public function getSubscriber(User $user)
    {
        try {
            $subscriber_hash = $this->mailchimp->subscriberHash($user->email);
            $this->mailchimp->get('/lists/' . $this->mailchimpList . '/members/' . $subscriber_hash);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function removeSubscriber()
    {
        $subscriber_hash = $this->mailchimp->subscriberHash($this->user->email);

        $this->mailchimp->delete('/lists/' . $this->mailchimpList . '/members/' . $subscriber_hash);

        return $this->mailchimp->getLastResponse();
    }

    public function addCustomer()
    {
        try {
            $this->mailchimp->post('ecommerce/stores/mc001/customers', [
                'id'            => 'cust' . $this->user->id,
                'email_address' => $this->user->email,
                'opt_in_status' => true,
                'first_name'    => $this->user->first_name,
                'last_name'     => $this->user->last_name,
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function removeCustomer($id)
    {
        try {
            $this->mailchimp->delete('ecommerce/stores/mc001/customers/cust' . $id);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function CustomerAddCart()
    {
        $result = $this->getCustomer($this->user);

        if ($result['headers']['http_code'] == 200) {
            $customer = json_decode($result['body']);
        } else {
            $result = $this->addCustomer();

            $customer = json_decode($result['body']);
        }

        try {
            $this->mailchimp->post('/ecommerce/stores/mc001/carts', [
                'id'       => 'cart' . $customer->id . '' . Str::random(6),
                'customer' => [
                    'id' => $customer->id,
                ],
                'currency_code' => 'CAD',
                'order_total'   => '100',
                'lines'         => [
                    [
                        'id'                 => '1',
                        'product_id'         => 'prod001',
                        'product_variant_id' => 'prod001',
                        'quantity'           => 1,
                        'price'              => 1.00,
                    ],
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function getCustomer(User $user)
    {
        $customerId = 'cust' . $user->id;

        try {
            $this->mailchimp->get('ecommerce/stores/mc001/customers/' . $customerId);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function FindorCreateCustomerAndAddOrder(Order $order)
    {
        $result = $this->getCustomer($this->user);

        if ($result['headers']['http_code'] == 200) {
            $customer = json_decode($result['body']);
        } else {
            $this->user = $order->user;
            $result     = $this->addCustomer();

            $customer = json_decode($result['body']);
        }

        try {
            $result = $this->mailchimp->post('ecommerce/stores/mc001/orders', [
                'id'       => (string) $order->id,
                'customer' => [
                    'id' => $customer->id,
                ],
                'currency_code' => 'CAD',
                'order_total'   => $order->total,
                'lines'         => [
                    [
                        'id'                 => '1',
                        'product_id'         => 'prod001',
                        'product_variant_id' => 'prod001',
                        'quantity'           => 1,
                        'price'              => 1.00,
                    ],
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function getOrder(Order $order)
    {
        try {
            $result = $this->mailchimp->get('ecommerce/stores/mc001/orders/' . $order->id);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }

    public function removeOrder(Order $order)
    {
        try {
            $result = $this->mailchimp->delete('ecommerce/stores/mc001/orders/' . $order->id);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $this->mailchimp->getLastResponse();
    }
}
