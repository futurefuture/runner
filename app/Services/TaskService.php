<?php

namespace App\Services;

use App\Task;
use App\User;
use App\Order;
use Exception;
use App\Address;
use App\Container;
use App\Mail\OrderCancelled;
use App\Events\TaskStateUpdated;
use Illuminate\Support\Facades\Log;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Event;

class TaskService
{
    public function create(
        $courierId,
        Order $order = null,
        $type = null,
        $index = 0,
        $notes = null,
        $phoneNumber = null,
        $containerId = null,
        $name = null,
        $organization = null,
        $addressId = null
    ) {
        if ($containerId == null) {
            $containerId = Container::create([
                'courier_id' => $courierId,
            ])->id;
        }

        if ($order != null) {
            $addressId = Address::where('user_id', $order->user_id)
                ->where('selected', 1)
                ->first()
                ->id;
        }

        $task = Task::create([
            'state'        => 1,
            'index'        => $index,
            'notes'        => $notes,
            'phone_number' => $phoneNumber,
            'courier_id'   => $courierId,
            'container_id' => $containerId,
            'name'         => $name,
            'organization' => $organization,
            'order_id'     => $order->id,
            'address_id'   => $addressId,
            'type'         => $type,
        ]);
    }

    public function update($task, $data)
    {
        if (isset($data['state'])) {
            $task->state = $data['state'];

            if ($data['state'] == Task::IS_COMPLETED) {
                $order = Order::find($task->order_id);

                if ($order) {
                    $notificationService = new NotificationService();
                    $order->status       = Order::IS_DELIVERED;
                    $order->save();

                    try {
                        $notificationService->orderDelivered($order);
                    } catch (Exception $e) {
                        Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());
                    }
                }
            }
        }

        if (isset($data['notes'])) {
            $task->notes = $data['notes'];
        }

        if (isset($data['phoneNumber'])) {
            $task->phone_number = $data['phoneNumber'];
        }

        if (isset($data['containerId'])) {
            // code to adjust index within container
            if ($task->container_id == $data['containerId']) {
                $taskAtIndex = Task::where('index', '=', $data['index'])->where('container_id', '=', $data['containerId'])->whereIn('state', [1, 2])->first();

                if ($taskAtIndex) {
                    $taskAtIndex->index = $task->index;
                    $taskAtIndex->save();
                    $task->index = $data['index'];
                }
            } else {
                $taskAtIndex = Task::where('index', '=', $data['index'])->where('container_id', '=', $data['containerId'])->whereIn('state', [1, 2])->first();

                if (! $taskAtIndex) {
                    $task->index = $data['index'];
                } else {
                    $task->index          = $data['index'];
                    $getAllTaskAfterIndex = Task::where('index', '>=', $data['index'])->where('container_id', '=', $data['containerId'])->whereIn('state', [1, 2])->get();

                    if ($getAllTaskAfterIndex) {
                        foreach ($getAllTaskAfterIndex as $taskIndex) {
                            $taskIndex->index = $taskIndex->index + 1;
                            $taskIndex->save();
                        }
                    }
                }
                // Code to rearrange all index if moving from one conatiner to another
                $taskInContainer = Task::where('container_id', '=', $task->container_id)->whereIn('state', [1, 2])->orderBy('index')->where('id', '!=', $task->id)->get();
                $index           = 0;

                foreach ($taskInContainer as $t) {
                    $t->index = $index;
                    $t->save();
                    $index++;
                }

                $task->container_id = $data['containerId'];

                if (isset($data['courierId'])) {
                    $courier          = User::find($data['courierId']);
                    $color            = $courier->schedules()->latest('created_at')->first()->color ?? '#ff00ff';
                    $task->courier_id = $data['courierId'];
                    $task->color      = $color;
                }

                $order = Order::find($task->order_id);

                if ($order) {
                    $order->runner_1 = $data['courierId'];
                    $order->save();
                }
            }
            // code ends here
        }

        if (isset($data['name'])) {
            $task->name = $data['name'];
        }

        if (isset($data['organization'])) {
            $task->organization = $data['organization'];
        }

        if (isset($data['addressId'])) {
            $task->address_id = $data['addressId'];
        }

        if (isset($data['type'])) {
            $task->type = $data['type'];
        }

        if (isset($data['index'])) {
            $task->index = $data['index'];
        }

        if (isset($data['signature'])) {
            $signature       = $data['signature'];
            $url             = (new S3Service())->addSignature($signature, $task);
            $task->signature = $url;
        }

        if (isset($data['scheduleTime'])) {
            $order = Order::find($task->order_id);
            if ($order) {
                $order->schedule_time = $data['scheduleTime'];
                $order->save();
            }
        }

        $task->save();

        return $task;
    }

    public function updateState($state, Order $order = null, $signature = null)
    {
        $task = Task::where('order_id', '=', $order->id)->first();
        if ($state == 3) {
            $allOtherTask = Task::where('container_id', '=', $task->container_id)->whereIn('state', [1, 2])->where('index', '>', $task->index)->get();
            foreach ($allOtherTask as $t) {
                $t->index = ($t->index - 1);
                $t->save();
            }
        }
        $task->state     = $state;
        $url             = (new S3Service())->addSignature($signature, $task);
        $task->signature = $url;
        $task->save();
        event(new TaskStateUpdated($task));
    }
}
