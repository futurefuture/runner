<?php

namespace App\Services;

use App\Services\SegmentService;
use App\User;
use Auth;
use Validator;

class LoginService
{
    /**
     * function to login with social.
     *
     * @param [type] $type
     * @return access token
     */
    public function socialLogin($request, $anonymousId)
    {
        if (isset($request['googleId'])) {
            $validator = Validator::make($request, [
                'googleId' => 'required',
            ]);

            $user = User::where('google_id', '=', $request['googleId'])
                ->first();
        } elseif (isset($request['facebookId'])) {
            $validator = Validator::make($request, [
                'facebookId' => 'required',
            ]);

            $user = User::where('facebook_id', '=', $request['facebookId'])
                ->first();
        } elseif (isset($request['appleId'])) {
            $validator = Validator::make($request, [
                'appleId' => 'required',
            ]);

            $user = User::where('apple_id', '=', $request['appleId'])
                ->first();
        }

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors'      => $errors,
            ], 401);
        }

        if (! $user) {
            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => null,
                        'user'        => null,
                    ],
                ],
            ]);
        }

        Auth::login($user);

        (new SegmentService())->trackLoggedIn($user, $anonymousId);

        $accessToken = $user->createToken('Runner')
            ->accessToken;

        return response()->json([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => $accessToken,
                    'user'        => [
                        'id'        => $user->id,
                        'firstName' => $user->first_name,
                        'lastName'  => $user->last_name,
                    ],
                ],
            ],
        ]);
    }
}
