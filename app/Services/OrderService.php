<?php

namespace App\Services;

use DB;
use App\User;
use App\Order;
use App\Refund;
use App\Address;
use App\Product;
use App\Inventory;
use Carbon\Carbon;
use Stripe\Stripe;
use App\Mail\OrderReceived;
use App\Services\Gateways\Gateway;
use App\Actions\CreateChargeAction;
use App\Actions\CreateRefundAction;
use Illuminate\Support\Facades\Mail;

class OrderService
{
    protected $order;
    protected $subtotal = 0;
    protected $cogs     = 0;
    protected $markup   = 0;
    protected $tax      = 0;
    protected $total    = 0;
    protected $itemsTax = 0;

    public function __construct(Order $order)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->order = $order;
    }

    public function updateStatus($status)
    {
        $activity_log = json_decode($this->order->activity_log);

        if ($this->order->status == 0 || $this->order->status == 7 || $this->order->status == 1) {
            if ($status === 'processing') {
                $this->order->status = 1;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => Carbon::now('America/Toronto'),
                ]);

                $this->order->activity_log = json_encode($activity_log);
                $this->order->save();
            }
        } elseif ($this->order->status === 1) {
            if ($status === 'onroute') {
                $this->order->status = 2;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => Carbon::now('America/Toronto'),
                ]);

                $this->order->activity_log = json_encode($activity_log);
                $this->order->save();
            }
        } elseif ($this->order->status === 2) {
            if ($status === 'delivered') {
                $this->order->status = 3;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => Carbon::now('America/Toronto'),
                ]);

                $this->order->delivered_at = Carbon::now('America/Toronto');
                $this->order->activity_log = json_encode($activity_log);

                $this->order->save();
                $this->order->customer = json_decode($this->order->customer);
                $this->order->content  = json_decode($this->order->content);
                $this->order->customer = json_decode($this->order->customer);

                if (env('APP_ENV') === 'production') {
                    Mail::to($this->order->customer->email)->bcc('service@getrunner.io')
                        ->queue(new OrderReceived($this->order));
                }

                $this->createOrderItems();
            }
        }
    }

    public function getInventories()
    {
        $orderContent          = json_decode($this->order->content);
        $inventories           = Inventory::all();
        $inventoriesCollection = new \Illuminate\Database\Eloquent\Collection();

        foreach ($inventories as $i) {
            $totalItems   = 0;
            $inStockItems = 0;
            $products     = [];
            $hours        = $i->options['hours'];

            foreach ($orderContent as $o) {
                $totalItems++;
                $product     = $i->productsInStock()->find($o->id);
                $fullProduct = Product::find($o->id);

                if ($product && (int) $o->quantity < $product->pivot->quantity) {
                    $inStockItems += 1;
                }

                if ($product) {
                    array_push($products, $fullProduct);
                }
            }

            $location      = Address::withTrashed()->find($i->address_id);
            $i->address    = $location->formatted_address;
            $i->inStock    = $inStockItems;
            $i->products   = $products;
            $i->totalItems = $totalItems;
            $today         = Carbon::now()->dayOfWeek;
            $i->hours      = $i->options['hours'][$today];

            $inventoriesCollection->add($i);
        }

        return collect($inventoriesCollection->sortByDesc('inStock'));
    }

    public function getStoreTime($hour)
    {
        $weekMap = [
            0 => 'SU',
            1 => 'MO',
            2 => 'TU',
            3 => 'WE',
            4 => 'TH',
            5 => 'FR',
            6 => 'SA',
        ];
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekday      = $weekMap[$dayOfTheWeek];

        switch ($weekday) {
            case 'MO':
                return $hour[1];

                break;
            case 'TU':
                return $hour[2];

                break;
            case 'WE':
                return $hour[3];

                break;
            case 'TH':
                return $hour[4];

                break;
            case 'FR':
                return $hour[5];

                break;
            case 'SA':
                return $hour[6];

                break;
            case 'SU':
                return $hour[0];

                break;
            default:
                echo 'its holiday';
        }
    }

    public function updateItem($newOrderItems)
    {
        $newItems = [];

        foreach ($newOrderItems['newItems'] as $key => $newItem) {
            if (isset($newItem['quantity'])) {
                array_push($newItems, [
                    'id'           => $newItem['id'],
                    'type'         => 'product',
                    'title'        => $newItem['title'],
                    'quantity'     => $newItem['quantity'],
                    'runnerPrice'  => $newItem['runnerPrice'],
                    'retailPrice'  => $newItem['retailPrice'],
                    'packaging'    => $newItem['packaging'],
                    'image'        => $newItem['image'],
                    'rewardPoints' => $newItem['rewardPoints'],
                    'allowSub'     => 'true',
                ]);
            }

            if (isset($newItem['productId']) && $newItem['productQuantity'] > 0) {
                $product = Product::find($newItem['productId']);

                array_push($newItems, [
                    'id'           => $product->id,
                    'type'         => 'product',
                    'title'        => $product->title,
                    'quantity'     => $newItem['productQuantity'],
                    'runnerPrice'  => $product->runner_price,
                    'retailPrice'  => $product->retail_price,
                    'packaging'    => $product->packaging,
                    'image'        => $product->image,
                    'rewardPoints' => 0,
                    'allowSub'     => 'true',
                ]);
            }
        }

        $this->subTotal($newItems);
        $this->markUp($newItems);
        $this->itemsTax($newItems);
        $this->totalTax();
        $this->total();
        $this->costOfGoods($newItems);

        if ($this->order->subtotal > $this->subtotal) {
            // refund
            if ($newOrderItems['isVerify'] == true) {
                $refundAmount = $this->refundAmount();
                $message      = 'total decreased: amount to refund is $' . number_format($refundAmount / 100, 2, '.', ',');

                return response()->json([
                    'message' => $message,
                ]);
            } else {
                $status = $this->refund($newItems);
            }
        } elseif ($this->order->subtotal < $this->subtotal) {
            // make another charge
            if ($newOrderItems['isVerify'] == true) {
                $chargeAmount = $this->subtotal - $this->order->subtotal;
                $message      = 'total increased: has made another charge for $' . number_format($chargeAmount / 100, 2, '.', ',');

                return response()->json([
                    'message' => $message,
                ]);
            } else {
                $status = $this->extraCharge($newItems);
            }
        } elseif ($this->order->subtotal === $this->subtotal) {
            // No need to do anything
            if ($newOrderItems['isVerify'] == true) {
                return response()->json([
                    'message' => 'total equal: everything looks fine',
                ]);
            } else {
                $activity_log = json_decode($this->order->activity_log);
                array_push($activity_log, (object) [
                    'log'   => 'total equal: everything looks fine',
                    'time'  => \Carbon\Carbon::now(),
                ]);

                $this->order->activity_log = json_encode($activity_log);
                $status                    = 'everything looks fine';
            }
        }

        $this->order->content           = json_encode($newItems);
        $this->order->subtotal          = (int) $this->subtotal;
        $this->order->cogs              = (int) $this->cogs;
        $this->order->markup            = (int) $this->markup;
        $this->order->display_tax_total = (int) $this->tax;
        $this->order->total             = (int) $this->total;
        $this->order->save();

        return $status;
    }

    private function itemsTax($newItems)
    {
        foreach ($newItems as $n) {
            $product = Product::find($n['id']);

            if (! $product->is_runner_owned) {
                $this->itemsTax += ($product->markup) * 0.13 * $n['quantity'];
            } else {
                $this->itemsTax += $product->runner_price * 0.13 * $n['quantity'];
            }
        }
    }

    /** add subTotal */
    public function subTotal($newItems)
    {
        foreach ($newItems as &$newItem) {
            $this->subtotal += $newItem['quantity'] * ($newItem['runnerPrice']);
        }

        return $this->subtotal;
    }

    public function costOfGoods($newItems)
    {
        foreach ($newItems as &$newItem) {
            $this->cogs += $newItem['quantity'] * ($newItem['retailPrice']);
        }

        return $this->cogs;
    }

    public function markUp($newItems)
    {
        foreach ($newItems as &$newItem) {
            $this->markup += $newItem['quantity'] * (($newItem['runnerPrice'] - $newItem['retailPrice']));
        }

        return $this->markup;
    }

    public function totalTax()
    {
        $this->tax = (int) (($this->order->delivery + $this->order->service_fee) * 0.13) + $this->itemsTax;

        return $this->tax;
    }

    public function total()
    {
        $this->total = (int) ($this->subtotal + $this->order->delivery + $this->order->tip + $this->tax - $this->order->discount);

        return $this->total;
    }

    public function refund($newItems)
    {
        $refundAmount = $this->refundAmount();
        $activity_log = $this->order->activity_log != null ? json_decode($this->order->activity_log) : [];

        array_push($activity_log, (object) [
            'log'   => 'total decreased: amount to refund is $' . number_format($refundAmount / 100, 2, '.', ','),
            'time'  => \Carbon\Carbon::now(),
        ]);

        $this->order->refund       = (int) $refundAmount;
        $this->order->activity_log = json_encode($activity_log);
        $status                    = 'need to refund $' . number_format($refundAmount / 100, 2, '.', ',') . '.';

        Refund::create([
            'order_id'  => $this->order->id,
            'user_id'   => $this->order->user_id,
            'refund_id' => '',
            'charge_id' => $this->order->charge ?? '',
            'amount'    => $this->order->refund ?? 0,
            'reason'    => '',
        ]);

        return $status;
    }

    public function extraCharge($newItems)
    {
        $chargeAmount  = $this->subtotal - $this->order->subtotal;
        $data          = [
            'chargeAmount' => $chargeAmount
        ];
        $user          = User::find($this->order->user_id);
        $action        = new CreateChargeAction();
        $action->execute($this->order, $user, $data);

        $activity_log = json_decode($this->order->activity_log);

        array_push($activity_log, (object) [
            'log'   => 'total increased: has made another charge for $' . number_format($chargeAmount / 100, 2, '.', ','),
            'time'  => \Carbon\Carbon::now(),
        ]);

        $this->order->activity_log = json_encode($activity_log);
        $status                    = 'has made another charge for $' . number_format($chargeAmount / 100, 2, '.', ',');

        return $status;
    }

    public function refundAmount()
    {
        return (int) ($this->order->subtotal - $this->subtotal);
    }

    public function reOrder(User $user, $cartItems)
    {
        $cart              = [];
        $formattedProducts = [];
        $cartSubtotal      = 0;
        $cartTax           = 0;
        $cartDiscount      = 0;
        $deliveryFee       = 999;

        // check for reward points
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 10,
            ];
            $cart['coupon'] = $coupon;
            $cartDiscount   = (int) $coupon->value * 100;
        }

        foreach ($cartItems as $cartItem) {
            $product = Product::find((int) $cartItem->id);
            // check for limited time offer on product
            $runnerPrice                      = 0;
            $retailPrice                      = 0;
            $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
                ->where('product_id', $product->id)
                ->where('incentive_id', 1)
                ->where('is_active', 1)
                ->first();
            if ($productIncentiveLimitedTimeOffer) {
                $runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
                $retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
            } else {
                $runnerPrice = $product->runner_price;
                $retailPrice = $product->retail_price;
            }
            $formattedProduct['id']           = $product->id;
            $formattedProduct['title']        = $product->title;
            $formattedProduct['runnerPrice']  = $runnerPrice;
            $formattedProduct['retailPrice']  = $retailPrice;
            $formattedProduct['image']        = $product->image;
            $formattedProduct['quantity']     = isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty;
            $formattedProduct['packaging']    = $product->packaging;
            $formattedProduct['rewardPoints'] = $product->rewardPoints;
            $formattedProduct['allowSub']     = isset($cartItem->allowSub) ? $cartItem->allowSub : true;
            $formattedProduct['subTotal']     = $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            $cartSubtotal += $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);
            $cartTax += (($runnerPrice - $runnerPrice / 1.12) * 0.13) * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            array_push($formattedProducts, $formattedProduct);
        }

        $cart['products']         = $formattedProducts;
        $cart['subTotal']         = $cartSubtotal;
        $cart['deliveryFee']      = $deliveryFee;
        $cart['tax']              = round($cartTax + ($deliveryFee * 0.13));
        $cart['rewardPointsUsed'] = 0;
        $cart['discount']         = $cartDiscount;
        $cart['total']            = round($cartSubtotal + $cartTax + $deliveryFee + ($deliveryFee * 0.13) - $cartDiscount);

        return $cart;
    }
}
