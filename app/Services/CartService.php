<?php

namespace App\Services;

use App\Actions\CreateGiftAction;
use App\Address;
use App\Cart;
use App\Category;
use App\Coupon;
use App\Events\NewOrder;
use App\Gift;
use App\Inventory;
use App\Invitation;
use App\Mail\OC;
use App\Order;
use App\Product;
use App\Services\Gateways\Gateway;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe\Stripe;
use Validator;

class CartService
{
    public $itemsTax      = 0;
    public $cartTax       = 0;
    public $cartDiscount  = 0;
    public $cartSubtotal  = 0;
    public $deliveryFee   = 999;
    public $serviceFeeTax = 0;
    public $serviceFee    = 0;
    public $cart          = [];
    public $runnerPrice   = 0;
    public $retailPrice   = 0;
    public $isGift        = false;

    public function __construct()
    {
        Stripe::setApiKey(getenv('STRIPE_SECRET'));
    }

    private function checkIfIsGiftCart($inventoryId)
    {
        if ($inventoryId == 11 || $inventoryId == 23) {
            $this->isGift        = true;
            $this->serviceFee    = 500;
            $this->serviceFeeTax = 500 * 0.13;
        }
    }

    /**
     *  function to create cart.
     *
     * @param Request $request
     * @param $userId
     * @param $inventoryId
     *
     * @return array
     */
    public function store(Request $request, User $user, Inventory $inventory)
    {
        $cartItems   = $request->input('data.attributes.products');
        $anonymousId = $request->input('data.attributes.anonymousId');
        $deliveryTax = $this->deliveryFee * 0.13;
        $this->checkIfIsGiftCart($inventory->id);

        if ($request->input('data.attributes.couponCode')) {
            $this->verifyCoupon($request->input('data.attributes.couponCode'), $user);
        }

        $this->ensureRewardPointsExits($user);

        $products                       = $this->CartProducts($cartItems, $request->input('data.attributes.couponCode'), $user);
        $this->cartTax                  = round($this->itemsTax + $deliveryTax + $this->serviceFeeTax);
        $this->cart['products']         = $products;
        $this->cart['subTotal']         = $this->cartSubtotal;
        $this->cart['deliveryFee']      = $this->deliveryFee;
        $this->cart['serviceFee']       = $this->serviceFee;
        $this->cart['tax']              = $this->cartTax;
        $this->cart['rewardPointsUsed'] = 0;
        $this->cart['isGift']           = $this->isGift;
        $this->cart['discount']         = $this->cartDiscount;
        $this->cart['serviceFee']       = $this->serviceFee;
        $this->cart['total']            = round($this->cartSubtotal + $this->cartTax + $this->deliveryFee + $this->serviceFee - $this->cartDiscount);

        $cart = Cart::updateOrCreate(
            [
                'user_id'      => (int) $user->id,
                'inventory_id' => (int) $inventory->id,
            ],
            [
                'content' => json_encode($this->cart),
            ]
        );

        $product  = Product::find($products[0]['id']);
        $category = Category::find($product->category_id);
        $quantity = $products[0]['quantity'];
        $coupon   = $request->input('data.attributes.couponCode') ?? null;
        $storeId  = Inventory::find($inventory->id)->first()->store_id;

        (new SegmentService())->trackProductAdded($user, $product, $cart, $category, $quantity, $coupon, $storeId, $inventory->id, $anonymousId);
    }

    /**
     * function to update cart.
     *
     * @param Request $request
     * @param $userId
     * @param $cartId
     *
     * @return array
     */
    public function update(Request $request, User $user, $cartId)
    {
        $oldCart            = Cart::find($cartId);
        $newCartItems       = $request->input('data.attributes.products');
        $anonymousId        = $request->input('data.attributes.anonymousId');
        $oldCartItems       = json_decode($oldCart->content)->products;
        $oldCartContent     = json_decode($oldCart->content);
        $oldCartItemIds     = [];
        $cart               = [];
        $this->cartSubTotal = 0;

        if (isset($oldCartContent->coupon)) {
            $cart['coupon'] = $oldCartContent->coupon;
        }

        $this->cartDiscount = $oldCartContent->discount;
        $oldCartItemIds     = [];
        $this->checkIfIsGiftCart($oldCart->inventory_id);

        if ($request->input('data.attributes.couponCode')) {
            $couponCode = $request->input('data.attributes.couponCode');
            if ($couponCode === 'CLEARCOUPON') {
                $cart['coupon']     = null;
                $this->cartDiscount = 0;
            } elseif ($couponCode === 'CLEARCART') {
                Cart::destroy($cartId);
            } else {
                $this->verifyCoupon($request->input('data.attributes.couponCode'), $user);
            }
        }

        foreach ($oldCartItems as $oldCartItem) {
            (int) array_push($oldCartItemIds, $oldCartItem->id);
        }

        $quantity     = $newCartItems['0']['quantity'];
        $coupon       = $request->input('data.attributes.couponCode') ?? null;
        $oldCartItems = $this->getCartItemsToUpdate($newCartItems, $oldCartItemIds, $oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId);

        $this->deliveryFee = $oldCartContent->deliveryFee;
        $deliveryTax       = $this->deliveryFee * 0.13;

        foreach ($oldCartItems as $oldCartItem) {
            // check for quantity discount
            $this->ensureProductQuantityDiscountExists($oldCartItem, $oldCartItem->quantity, 'update', $request->input('data.attributes.couponCode'), $user);

            $this->cartSubtotal += $oldCartItem->subTotal;
            $this->itemsTax += (($oldCartItem->runnerPrice - $oldCartItem->runnerPrice / 1.12) * 0.13) * $oldCartItem->quantity;
        }

        $this->cartTax             = round($this->itemsTax + $deliveryTax + $this->serviceFeeTax);
        $this->cart['products']    = $oldCartItems;
        $this->cart['isGift']      = $this->isGift;
        $this->cart['serviceFee']  = $this->serviceFee;
        $this->cart['subTotal']    = $this->cartSubtotal;
        $this->cart['tax']         = round($this->cartTax);
        $this->cart['deliveryFee'] = $this->deliveryFee;
        $this->cart['discount']    = $this->cartDiscount;
        $this->cart['total']       = round($this->cartSubtotal + $this->cartTax + $this->deliveryFee + $this->serviceFee - $this->cartDiscount);

        if (count($oldCartItems) === 0) {
            Cart::destroy($cartId);
        } else {
            $oldCart->update([
                'content' => json_encode($this->cart),
            ]);
        }
    }

    /**
     *  function to create order and payment.
     *
     * @return array
     */
    public function checkout($request)
    {
        $this->verifyStoreOpen();
        $inventoryId = (int) $request->input('data.attributes.inventoryId');
        $anonymousId = $request->input('data.attributes.anonymousId');
        $device      = (int) $request->input('data.attributes.device');
        $inventory   = Inventory::find($inventoryId);
        $store       = $inventory->store;
        $user        = User::find($request->input('data.attributes.userId'));
        $appVersion  = $request->input('data.attributes.appVersion');

        /** order make */
        $dt  = Carbon::parse($request->input('data.attributes.orderDateTime'), 'America/Toronto');
        $now = Carbon::now();

        $cart = Cart::where('user_id', $user->id)
            ->where('inventory_id', $inventoryId)
            ->first();

        $cartContent    = json_decode($cart->content);
        $cartSubtotal   = (int) $cartContent->subTotal;
        $cartTotal      = (int) $cartContent->total;
        $deliveryFee    = (int) $cartContent->deliveryFee;
        $deliveryFeeTax = (int) $deliveryFee * 0.13;
        $costOfGoods    = (int) $cartSubtotal / 1.12;
        $markup         = (int) ($cartSubtotal - $costOfGoods);
        $markupTax      = (int) $markup * 0.13;
        $tip            = (int) $request->input('data.attributes.tip');
        $serviceFee     = array_key_exists('serviceFee', $cartContent) ? (int) $cartContent->serviceFee : 0;
        $serviceTax     = (int) $serviceFee * 0.13;
        $totalTax       = (int) $markupTax + $deliveryFeeTax + $serviceTax;
        $discount       = (int) $cartContent->discount;
        $chargeAmount   = (int) ($cartTotal + $tip);
        $coupon         = null;
        $couponId       = null;
        $couponCode     = null;

        if (isset($cartContent->coupon) && $cartContent->coupon->code != 'Reward Points') {
            $couponId   = Coupon::where('code', $cartContent->coupon->code)->first()->id;
            $coupon     = Coupon::where('code', $cartContent->coupon->code)->first();
            $couponCode = $cartContent->coupon->code;
        } elseif (isset($cartContent->coupon) && $cartContent->coupon->code == 'Reward Points') {
            $couponCode = $cartContent->coupon->code;
        }

        $this->handleProductRewardPoints($cartContent->products, $user);

        $order = new Order();

        // check if gift data and has address
        $address = Address::where('user_id', $user->id)->where('selected', 1)->first();

        $groupedAddress = json_encode((object) [
            'address' => $address->formatted_address,
            'lat'     => (float) $address->lat,
            'lng'     => (float) $address->lng,
        ]);

        if ($request->input('data.attributes.isGift') == true) {
            $giftData = $request->input('data.attributes.giftDetails');

            if (isset($giftData['address'])) {
                $address = (object) [
                    'formatted_address' => $giftData['address']['formattedAddress'],
                    'lat'               => $giftData['address']['lat'],
                    'lng'               => $giftData['address']['lng'],
                    'postal_code'       => $giftData['address']['postalCode'],
                    'instructions'      => isset($giftData['address']['instructions']) ? $giftData['address']['instructions'] : '',
                    'unit_number'       => isset($giftData['address']['unitNumber']) ? $giftData['address']['unitNumber'] : '',
                ];
            }
        }

        $order->user_id       = $user->id;
        $order->inventory_id  = $inventoryId;
        $order->store_id      = $store->id;
        $order->status        = $dt->diffInMinutes($now) > 180 ? 7 : 0;
        $order->schedule_time = $dt->diffInMinutes($now) > 180 ? $dt->toDateTimeString() : null;
        $order->content       = json_encode($cartContent->products);
        $order->activity_log  = json_encode([(object) [
            'log'  => 'received',
            'time' => \Carbon\Carbon::now(),
        ]]);

        $order->device   = $device ?? 3;
        $order->customer = json_encode((object) [
            'firstName'   => $user->first_name,
            'lastName'    => $user->last_name,
            'phoneNumber' => $user->phone_number,
            'email'       => $user->email,
        ]);
        $order->address              = $groupedAddress;
        $order->postal_code          = $address->postal_code;
        $order->unit_number          = $address->unit_number ?? null;
        $order->instructions         = $address->instructions ?? null;
        $order->coupon_id            = $couponId;
        $order->subtotal             = $cartSubtotal;
        $order->cogs                 = $costOfGoods;
        $order->discount             = $discount;
        $order->delivery             = $deliveryFee;
        $order->delivery_tax         = $deliveryFeeTax;
        $order->markup               = $markup;
        $order->markup_tax           = $markupTax;
        $order->display_tax_total    = $totalTax;
        $order->tip                  = $tip;
        $order->total                = $chargeAmount;
        $order->service_fee          = $serviceFee;
        $order->incentive_id         = isset($cartContent->incentive) ? (string) $cartContent->incentive->id : null;
        $order->first_time           = $user->orders()->count() ? 0 : 1;
        $order->app_version          = $appVersion;
        $order->address_id           = $address->id ?? null;
        $order->stripe_fee           = (int) ($chargeAmount * 0.029 + 30);
        $stripePayment               = Gateway::via('stripe')->settle($order, $user, $request);
        $order->card_last_four       = $stripePayment->last4;
        $order->charge               = $stripePayment->transaction_id;

        $order->save();

        if ($request->input('data.attributes.isGift') == true) {
            $giftAction = new CreateGiftAction();
            $giftAction->execute($giftData, $order);
        }

        if ($request->input('data.attributes.isGift') == true) {
            $this->handleGiftOrder($request, $groupedAddress, $order);
        }

        // check for coupon
        if (isset($cartContent->coupon) && $cartContent->coupon->code != 'ADMIN907') {
            $this->updateUserCouponUsedAfterOrder($couponCode, $user, $order);
        }

        $this->rewardReferrerForFirstOrder($user, $order);

        if (env('APP_ENV') === 'production') {
            if ($inventoryId == '23' && $user->id != 131) {
                Mail::to($user->email)
                ->bcc([
                    'voiseykristen@gmail.com',
                    'kristen@cocktailemporium.com',
                    'service@getrunner.io',
                    'jake@getrunner.io',
                    'jarek@getrunner.io',
                ])
                ->queue(new OC($order));
            } else {
                Mail::to($user->email)
                ->bcc('service@getrunner.io')
                ->queue(new OC($order));
            }
        }

        $notificationService = new NotificationService();

        try {
            $notificationService->orderPlaced($order);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e,
                ],
            ], 401);
        }

        // Add 100 reward points to user.
        (new RewardPointService())->addPoints(100, USER::MAKE_PURCHASE, [
            'order_id' => $order->id,
        ], $user->id);

        Cart::destroy($cart->id);

        event(new NewOrder($order));

        $formattedProduct = [];
        $quantity         = 0;

        foreach ($cartContent->products as $product) {
            $partnerIds   = [];
            $orderProduct = [];
            $newProduct   = Product::find($product->id);

            foreach ($newProduct->partners as $p) {
                array_push($partnerIds, (string) $p->id);
            }

            $orderProduct['productId']  = $newProduct->id;
            $orderProduct['sku']        = $newProduct->sku;
            $orderProduct['name']       = $newProduct->title;
            $orderProduct['price']      = $newProduct->retail_price;
            $orderProduct['quantity']   = $product->quantity;
            $orderProduct['packaging']  = $newProduct->packaging;
            $orderProduct['producer']   = $newProduct->producer;
            $orderProduct['category']   = $newProduct->category_id;
            $orderProduct['partnerIds'] = $partnerIds;
            $quantity += $product->quantity;

            array_push($formattedProduct, $orderProduct);
        }

        (new SegmentService())->trackOrderCompleted($user, $formattedProduct, $order, $store, $quantity, $device, $anonymousId, $inventoryId);

        return $order;
    }

    /**
     * function function to get all cart items to update.
     *
     * @param $newCartItems
     * @param $oldCartItemIds
     * @param $oldCartItems
     *
     * @return array
     */
    public function getCartItemsToUpdate($newCartItems, $oldCartItemIds, $oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId)
    {
        collect($newCartItems)->each(function ($newCartItem) use (&$oldCartItemIds, &$oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId) {
            if (in_array($newCartItem['id'], $oldCartItemIds)) {
                foreach ($oldCartItems as $key => $oldCartItem) {
                    if ($newCartItem['id'] == $oldCartItem->id) {
                        $product = Product::find($newCartItem['id']);
                        $category = Category::find($product->category_id);
                        $inventory = Inventory::find($oldCart->inventory_id);
                        $storeId = $inventory->store_id;

                        if ($newCartItem['quantity'] == 0) {
                            unset($oldCartItems[$key]);
                        }

                        // disallow negative quantity
                        if ((int) $newCartItem['quantity'] < 0) {
                            continue;
                        }

                        $newQuantity = (int) $newCartItem['quantity'] - $oldCartItem->quantity;

                        if ((int) $newCartItem['quantity'] > 0) {
                            if (! $this->verifyInventoryHasQuantity((int) $newCartItem['quantity'], $product, $inventory->id)) {
                                throw new Exception('Sorry, but the quantity that you are trying to add is larger than what is in stock.');
                            }
                        }

                        // check if product removed or added
                        if ($newQuantity > 0) {
                            (new SegmentService())->trackProductAdded($user, $product, $oldCart, $category, $newQuantity, $coupon, $storeId, $inventory->id, $anonymousId);
                        } else {
                            (new SegmentService())->trackProductRemoved($user, $product, $oldCart, $category, abs($newQuantity), $coupon, $storeId, $inventory->id, $anonymousId);
                        }

                        $oldCartItem->quantity = (int) $newCartItem['quantity'];
                        $oldCartItem->allowSub = $newCartItem['allowSub'];
                        $oldCartItem->subTotal = $newCartItem['quantity'] * $oldCartItem->runnerPrice;
                    }
                }

                $oldCartItems = (array) array_values($oldCartItems);

                return $oldCartItems;
            } else {
                $product = Product::find($newCartItem['id']);
                $partnerIds = [];

                foreach ($product->partners as $p) {
                    array_push($partnerIds, (string) $p->id);
                }

                $category = Category::find($product->category_id);

                // check for limited time offer on product
                $this->ensureProductLimitedOfferExists($product);

                $newProduct['id'] = $product->id;
                $newProduct['title'] = $product->title;
                $newProduct['type'] = $newCartItem['type'];
                $newProduct['runnerPrice'] = $this->runnerPrice;
                $newProduct['retailPrice'] = $this->retailPrice;
                $newProduct['quantity'] = (int) $newCartItem['quantity'];
                $newProduct['packaging'] = $product->packaging;
                $newProduct['producer'] = $product->producer;
                $newProduct['image'] = $product->image;
                $newProduct['rewardPoints'] = $product->rewardPoints;
                $newProduct['allowSub'] = $newCartItem['allowSub'];
                $newProduct['subTotal'] = $newCartItem['quantity'] * $this->runnerPrice;
                $newProduct['partnerIds'] = $partnerIds;

                $this->cartSubTotal += $newProduct['subTotal'];
                $this->cartTax += (($this->runnerPrice - $this->runnerPrice / 1.12) * 0.13) * $newCartItem['quantity'];

                array_push($oldCartItems, (object) $newProduct);

                $inventory = Inventory::find($oldCart->inventory_id);
                $storeId = $inventory->store_id;

                (new SegmentService())->trackProductAdded($user, $product, $oldCart, $category, $quantity, $coupon, $storeId, $inventory->id, $anonymousId);
            }
        });

        return $oldCartItems;
    }

    /**
     * function to verify coupon.
     *
     * @param $couponCode
     * @param User $user
     *
     * @return array
     */
    public function verifyCoupon($couponCode, $user)
    {
        $coupon = Coupon::where('code', $couponCode)
            ->where('is_active', 1)
            ->first();
        if ($coupon) {
            if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $user->id)->where('used', 1)->doesntExist()) {
                $this->cart['coupon'] = (object) [
                    'code'  => $coupon->code,
                    'value' => (int) $coupon->value,
                ];
                $this->cartDiscount = (int) $coupon->value;
            } else {
                return response()->json([
                    'errors' => [
                        [
                            'status' => 404,
                            'title'  => 'Invalid Coupon Code',
                            'detail' => 'The coupon code given has already been used.',
                        ],
                    ],
                ], 404);
            }
        } else {
            return response()->json([
                'errors' => [
                    [
                        'status' => 404,
                        'title'  => 'Invalid Coupon Code',
                        'detail' => 'The coupon code given is not a valid coupon code.',
                    ],
                ],
            ], 404);
        }
        // check for reward points
    }

    /**
     * function to check reward points.
     *
     * @param User $user
     *
     * @return array
     */
    public function ensureRewardPointsExits($user)
    {
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 1000,
            ];

            $this->cart['coupon'] = $coupon;
            $this->cartDiscount   = (int) $coupon->value;
        }
    }

    public function cartProducts($cartItems, $couponCode, $user)
    {
        $formattedProducts = [];

        foreach ($cartItems as $cartItem) {
            // disallow negative quantity
            if ((int) $cartItem['quantity'] < 0) {
                return false;
            }

            $product = Product::find((int) $cartItem['id']);

            // check for limited time offer on product
            $this->ensureProductLimitedOfferExists($product);

            // check for quantity discount
            $this->ensureProductQuantityDiscountExists($product, $cartItem['quantity'], 'create', $couponCode, $user);
            $partnerIds = [];

            foreach ($product->partners as $p) {
                array_push($partnerIds, (string) $p->id);
            }

            $formattedProduct['id']           = $product->id;
            $formattedProduct['title']        = $product->title;
            $formattedProduct['runnerPrice']  = $this->runnerPrice;
            $formattedProduct['retailPrice']  = $this->retailPrice;
            $formattedProduct['image']        = $product->image;
            $formattedProduct['quantity']     = (int) $cartItem['quantity'];
            $formattedProduct['packaging']    = $product->packaging;
            $formattedProduct['producer']     = $product->producer;
            $formattedProduct['rewardPoints'] = $product->rewardPoints;
            $formattedProduct['allowSub']     = $cartItem['allowSub'];
            $formattedProduct['type']         = $cartItem['type'] ?? 'regular';
            $formattedProduct['subTotal']     = $this->runnerPrice * $cartItem['quantity'];
            $formattedProduct['partnerIds']   = $partnerIds;

            $this->cartSubtotal += $this->runnerPrice * $cartItem['quantity'];
            $this->itemsTax += (($this->runnerPrice - $this->runnerPrice / 1.12) * 0.13) * $cartItem['quantity'];
            array_push($formattedProducts, $formattedProduct);
        }

        return $formattedProducts;
    }

    /**
     * function to check for limited time offer on product.
     *
     * @param User $user
     */
    public function ensureProductLimitedOfferExists($product)
    {
        $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
            ->where('product_id', $product->id)
            ->where('incentive_id', 1)
            ->where('is_active', 1)
            ->first();

        if ($productIncentiveLimitedTimeOffer && Carbon::parse($productIncentiveLimitedTimeOffer->start_date)->isPast() && Carbon::parse($productIncentiveLimitedTimeOffer->end_date)->isFuture()) {
            $this->runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
            $this->retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
        } else {
            $this->runnerPrice = $product->runner_price;
            $this->retailPrice = $product->retail_price;
        }
    }

    /**
     * function to check for limited time offer on product.
     *
     * @param User $user
     */
    public function ensureProductQuantityDiscountExists($product, $quantity, $type, $couponCode, $user)
    {
        $productQuantityDiscount = DB::table('incentive_product')
            ->where('product_id', $product->id)
            ->where('incentive_id', 3)
            ->first();

        if ($productQuantityDiscount && $quantity > $productQuantityDiscount->minimum_quantity) {
            if (Carbon::parse($productQuantityDiscount->start_date)->isPast() && Carbon::parse($productQuantityDiscount->end_date)->isFuture()) {
                $incentive = (object) [
                    'id'    => (string) $productQuantityDiscount->id,
                    'value' => (int) $productQuantityDiscount->savings,
                ];

                $this->cart['coupon'] = null;

                $this->cart['incentive'] = $incentive;
                $this->cartDiscount      = (int) $productQuantityDiscount->savings;
            }
        } elseif ($productQuantityDiscount && $quantity < $productQuantityDiscount->minimum_quantity) {
            unset($this->cart['incentive']);
            if ($couponCode || $user->rewardPointsBalance() >= 1000) {
            } else {
                $this->cartDiscount = 0;
            }
        }
    }

    public function verifyStoreOpen()
    {
        // check if global store is closed
        $globalStoreStatus = DB::table('config')
            ->where('key', 'close_store')
            ->first();

        if ($globalStoreStatus->value == 'true') {
            return response()->json([
                'errors' => [
                    'error' => 'Sorry, but the store is temporarily closed. Please check back later!',
                ],
            ], 500);
        }
    }

    public function handleProductRewardPoints($products, $user)
    {
        foreach ($products as $p) {
            $product = Product::find($p->id);
            // reward points
            if ($product->incentives()->where('incentive_id', 7)->pluck('reward_points')->first()) {
                $q = $product->incentives()->where('incentive_id', 7)->first();

                if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                    $rewardPoints = $q->reward_points;
                } else {
                    $rewardPoints = 0;
                }
            } else {
                $rewardPoints = 0;
            }

            if ($rewardPoints > 0) {
                (new RewardPointService())->addPoints($rewardPoints, USER::PROMO_PURCHASE, [
                    'productId'       => $rewardPoints,
                    'productTitle'    => $product->title,
                    'productPackaing' => $product->packaging,
                ], $user->id);
            }
        }
    }

    public function handleGiftOrder($request, $groupAddress, $order)
    {
        foreach ($request->input('data.attributes.giftDetails') as $key => $r) {
            $giftDetails[$key] = $r;
        }

        if (array_key_exists('address', $giftDetails)) {
            $address = json_encode((object) [
                'address' => $giftDetails['address']['formattedAddress'],
                'lat'     => $giftDetails['address']['lat'],
                'lng'     => $giftDetails['address']['lng'],
            ]);
            $unitNumber   = isset($giftDetails['address']['unitNumber']) ? $giftDetails['address']['unitNumber'] : '';
            $instructions = isset($giftDetails['address']['instructions']) ? $giftDetails['address']['instructions'] : '';
        } else {
            $address      = $groupAddress;
            $unitNumber   = $address->unit_number ?? null;
            $instructions = $address->instructions ?? null;
        }

        Gift::create([
            'order_id'     => $order->id,
            'first_name'   => $giftDetails['firstName'],
            'last_name'    => $giftDetails['lastName'],
            'address'      => $address,
            'unit_number'  => $unitNumber,
            'email'        => $giftDetails['email'],
            'instructions' => $instructions,
            'is_19'        => true,
            'phone_number' => $giftDetails['phoneNumber'],
            'message'      => $giftDetails['personalMessage'],
        ]);

        $giftDetails = [];
    }

    public function updateUserCouponUsedAfterOrder($code, $user, $order)
    {
        $coupon = \App\Coupon::where('code', $code)->first();

        if ($coupon) {
            $user->coupons()->attach($coupon->id, [
                'used'     => 1,
                'order_id' => $order->id,
            ]);
        } elseif ($code === 'Reward Points') {
            if ($user->rewardPointsBalance() < 1000) {
                return response()->json([
                    'errors' => [
                        'error' => 'You do not have enough reward points.',
                    ],
                ], 401);
            } else {
                (new RewardPointService())->addPoints(-1000, USER::REWARDS_POINTS_DISCOUNT, [
                    'order_id' => $order->id,
                ], $user->id);
            }
        }
    }

    public function rewardReferrerForFirstOrder($user, $order)
    {
        $invitation = Invitation::where('invited_user_id', $user->id)->first();

        if ($invitation && $user->orders()->count() === 1) {
            $referrer = User::find($invitation->user_id);

            (new RewardPointService())->addPoints(1000, USER::REFERRER_AWARDED, [
                'invited_id'    => $user->id,
                'invited_name'  => $user->first_name . ' ' . $user->last_name,
                'invited_email' => $user->email,
            ], $user->id);

            $notificationService = new NotificationService();
            $notificationService->sendReferrerNotification($order, $referrer);
        }
    }

    private function verifyInventoryHasQuantity($quantity, $product, $inventoryId)
    {
        $inventoryProduct = DB::table('inventory_product')
            ->where('product_id', $product->id)
            ->where('inventory_id', $inventoryId)
            ->first();

        if ($quantity > $inventoryProduct->quantity) {
            return false;
        } else {
            return true;
        }
    }
}
