<?php

namespace App\Services;

use App\User;
use Validator;
use App\Events\UserCreated;
use App\Services\UserService;
use App\Services\SegmentService;
use App\Services\Gateways\Gateway;
use App\Services\MailChimpService;
use Illuminate\Support\Facades\Mail;
use App\Services\ValidateEmailService;

class AuthService
{
    private function validateIsLegalAge($data)
    {
        if ($data['isLegalAge'] == false) {
            return response()->json([
                    'errors' => [
                        'error' => 'Please check that you are above 19 years of age to register.',
                    ],
                ], 401);
        }
    }

    public function register($data)
    {
        $anonymousId          = $data['anonymousId'];
        $validateEmailService = new ValidateEmailService($data['email']);
        $emailIsValidated     = $validateEmailService->validate();

        if ($emailIsValidated) {
            $this->validateIsLegalAge($data);

            // create stripe user
            $customer = Gateway::via('stripe')->storeCustomer($data);

            // create user
            $user = (new UserService())->create($data, $customer);

            $accessToken = $user->createToken('Runner')
                ->accessToken;

            event(new UserCreated($user));

            if (env('APP_ENV') === 'production') {
                // add user to mailchimp
                $mailChimpService = new MailChimpService($user);
                $mailChimpService->addSubscriber();
            }

            (new SegmentService())->trackSignedUp($user, $anonymousId);

            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => $accessToken,
                        'user'        => [
                            'id'        => (string) $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                    ],
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'invalid credentials',
                ],
            ], 401);
        }
    }

    public function socialLogin($data)
    {
        if (isset($data['googleId'])) {
            $user = User::where('google_id', '=', $data['googleId'])
                ->first();
        } elseif (isset($data['facebookId'])) {
            $user = User::where('facebook_id', '=', $data['facebookId'])
                ->first();
        } elseif (isset($data['appleId'])) {
            $user = User::where('apple_id', '=', $data['appleId'])
                ->first();
        }

        if (! $user) {
            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => null,
                        'user'        => null,
                    ],
                ],
            ]);
        }

        (new SegmentService())->trackLoggedIn($user, $data['anonymousId']);

        $accessToken = $user->createToken('Runner')
            ->accessToken;

        return response()->json([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => $accessToken,
                    'user'        => [
                        'id'        => (string) $user->id,
                        'firstName' => $user->first_name,
                        'lastName'  => $user->last_name,
                    ],
                ],
            ],
        ]);
    }
}
