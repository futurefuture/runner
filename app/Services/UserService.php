<?php

namespace App\Services;

use App\Actions\GetUserGenderAction;
use App\Services\AddressService;
use App\Services\RewardPointService;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class UserService
{
    private function createAddress($address)
    {
        (new AddressService())->create($address);
    }

    private function checkIfWasReferred($user)
    {
        $userWasReferred = DB::table('invite_user')
            ->where('email_sent_to', $user->email)
            ->orWhere('phone_number_sent_to', $user->phone_number)
            ->first();

        // add 1000 reward points to account if referrer exists.
        if ($userWasReferred) {
            // dd($userWasReferred);
            $referrer = User::find($userWasReferred->user_id);

            (new RewardPointService())->addPoints(1000, USER::REGISTER_REFERRER, [
                'referrer_id'     => $referrer->id,
                'referrer_name'   => $referrer->first_name . $referrer->last_name,
                'referrer_email'  => $referrer->email,
            ], $referrer->id);

            $referrer->invites()
                ->create([
                    'invited_user_id' => $user->id,
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now(),
                ]);
        }
    }

    public function create($data, $customer)
    {
        $user = User::create([
            'first_name'    => ucfirst(strtolower($data['firstName'])),
            'last_name'     => ucfirst(strtolower($data['lastName'])),
            'phone_number'  => preg_replace('/\D/', '', $data['phoneNumber']),
            'date_of_birth' => new Carbon($data['dateOfBirth'], 'America/Toronto'),
            'email'         => strtolower($data['email']),
            'password'      => isset($data['password']) ? bcrypt($data['password']) : null,
            'facebook_id'   => isset($data['facebookId']) ? $data['facebookId'] : null,
            'google_id'     => isset($data['googleId']) ? $data['googleId'] : null,
            'apple_id'      => isset($data['appleId']) ? $data['appleId'] : null,
            'stripe_id'     => $customer->id,
            'is_19'         => 1,
            'invite_code'   => strtoupper(Str::random(6)),
            'gender'        => isset($data['gender']) ? $data['gender'] : null,
            'age_range'     => isset($data['ageRange']) ? $data['ageRange'] : null,
            'avatar_image'  => isset($data['avatarImage']) ? $data['avatarImage'] : null,
            'address'       => $data['address']['postalCode'],
        ]);

        // grab gender with genderize api
        $getUserGenderAction = new GetUserGenderAction;
        $getUserGenderAction->execute($user);

        // create address for user
        $address = [
            'user_id'           => $user->id,
            'formatted_address' => $data['address']['formattedAddress'],
            'lat'               => $data['address']['lat'],
            'lng'               => $data['address']['lng'],
            'place_id'          => $data['address']['placeId'],
            'postal_code'       => $data['address']['postalCode'],
            'selected'          => 1,
        ];

        $this->createAddress($address);
        $this->checkIfWasReferred($user);

        return $user;
    }
}
