<?php

namespace App;

use App\Category;
use App\Inventory;
use App\ProductImage;
use App\ProductIncentive;
use App\Review;
use App\Tag;
use App\Utilities\Filters\QueryFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use SoftDeletes, Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'new_products';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku',
        'upc',
        'is_active',
        'title',
        'long_description',
        'short_description',
        'retail_price',
        'wholesale_price',
        'markup_percentage',
        'markup',
        'runner_price',
        'limited_time_offer_savings',
        'limited_time_offer_price',
        'image',
        'image_thumbnail',
        'packaging',
        'alcohol_content',
        'sugar_content',
        'slug',
        'category_id',
        'producing_country',
        'producer',
        'is_alcohol',
        'is_runner_owned'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function inventories()
    {
        return $this
            ->belongsToMany(Inventory::class, 'inventory_product', 'product_id', 'inventory_id')
            ->withPivot('quantity');
    }

    public function reviews()
    {
        return $this
            ->hasMany(Review::class, 'product_id')
            ->where('is_approved', 1);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'product_tag', 'product_id', 'tag_id')
            ->withPivot('index')
            ->orderBy('index', 'asc')
            ->withTimestamps();
    }

    public function incentives()
    {
        return $this->hasMany(ProductIncentive::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class)->active()->where('ad_type_id', 1);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function scopePrice($query, $value)
    {
        if (strpos($value, '-') !== false) {
            $value = substr($value, strpos($value, '-') + 1);
        }

        return $query->where('runner_price', '<=', $value);
    }

    /**
     * Get the query limit.
     */
    private function limit(Request $request)
    {
        return min($request->input('limit'), 50);
    }

    public function getCategoryAttribute()
    {
        return Category::find($this->category_id);
    }

    // public function getStyleAttribute()
    // {
    //     return $this->tags->where('type', 'style')->first()->title ?? null;
    // }

    public function getRewardPointsAttribute()
    {
        // if ($this->incentives()->where('incentive_id', 7)->pluck('reward_points')->first()) {
        //     $q = $this->incentives()->where('incentive_id', 7)->first();

        //     if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
        //         return $q->reward_points;
        //     } else {
        //         return 0;
        //     }
        // } else {
        //     return 0;
        // }
        return 0;
    }

    public function getCaseDealAttribute()
    {
        return null;
        // return $this->incentives()->where('incentive_id', 2)->pluck('savings')->first() ?? null;
    }

    public function getQuantityDiscountAttribute()
    {
        if ($this->incentives()->where('incentive_id', 3)->pluck('savings', 'minimum_quantity')->first()) {
            $q = $this->incentives()->where('incentive_id', 3)->first();

            if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                $quantityDiscount = (object) [
                    'savings'  => $q->savings,
                    'quantity' => $q->minimum_quantity,
                ];

                return $quantityDiscount;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    public function scopeAverageRating(Builder $query, $value): Builder
    {
        if (strpos($value, '-') !== false) {
            $value = substr($value, strpos($value, '-') + 1);
        }

        return $query->where('average_rating', '>=', $value);
    }

    public function getIsGiftAttribute()
    {
        if (DB::table('inventory_product')->where('inventory_id', 11)->where('product_id', $this->id)->first()) {
            return true;
        } else {
            return false;
        }
    }

    public function categories()
    {
        return $this
            ->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id')
            ->withPivot('index');
    }

    public function relatedProducts()
    {
        return $this
            ->belongsToMany(self::class, 'related_products', 'product_id', 'related_product_id')
            ->withPivot('index')
            ->orderBy('index');
    }

    public function productImages()
    {
        return $this
            ->hasMany(ProductImage::class)
            ->orderBy('index');
    }

    public function partners()
    {
        return $this->belongsToMany(Partner::class);
    }
}
