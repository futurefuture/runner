<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyStatusSent extends Mailable
{
    use Queueable, SerializesModels;

    protected $date;
    protected $scheduledRunners;
    protected $finances;
    protected $attachment;
    protected $emails;
    protected $ordersWithOrderItemDiscrepancies;
    protected $formattedLowProducts;
    protected $general;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formattedRunners, $finances, $date, $emails, $attachment, $ordersWithOrderItemDiscrepancies, $general)
    {
        $this->attachment                       = $attachment;
        $this->scheduledRunners                 = $formattedRunners;
        $this->date                             = $date;
        $this->finances                         = $finances;
        $this->emails                           = $emails;
        $this->ordersWithOrderItemDiscrepancies = $ordersWithOrderItemDiscrepancies;
        $this->general                          = $general;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date = $this->date;

        if (app()->environment() === 'production') {
            return $this->to($this->emails)
                ->markdown('emails.daily-status-sent')
                ->subject('Daily Status')
                ->with([
                    'date'                      => $date,
                    'scheduledRunners'          => $this->scheduledRunners,
                    'finances'                  => [
                        'revenue'           => $this->finances['revenue'],
                        'costOfGoodsSold'   => $this->finances['costOfGoodsSold'],
                        'deliveryFees'      => $this->finances['deliveryFees'],
                        'serviceFees'       => $this->finances['serviceFees'],
                        'discounts'         => $this->finances['discounts'],
                        'stripeFees'        => $this->finances['stripeFees'],
                        'tips'              => $this->finances['tips'],
                        'markup'            => $this->finances['markup'],
                        'runnerCosts'       => $this->finances['runnerCosts'],
                        'dispatchCosts'     => $this->finances['dispatchCosts'],
                    ],
                    'ordersWithOrderItemDiscrepancies' => $this->ordersWithOrderItemDiscrepancies,
                    'general'                          => $this->general
                ])
                ->attach($this->attachment, ['as' => 'daily-status-' . $this->date . '.xlsx']);
        }
        if (app()->environment() === 'staging') {
            return $this->to($this->emails)
                ->markdown('emails.daily-status-sent')
                ->subject('Staging | Daily Status')
                ->with([
                    'date'                      => $date,
                    'scheduledRunners'          => $this->scheduledRunners,
                    'finances'                  => [
                        'revenue'           => $this->finances['revenue'],
                        'costOfGoodsSold'   => $this->finances['costOfGoodsSold'],
                        'deliveryFees'      => $this->finances['deliveryFees'],
                        'serviceFees'       => $this->finances['serviceFees'],
                        'discounts'         => $this->finances['discounts'],
                        'stripeFees'        => $this->finances['stripeFees'],
                        'tips'              => $this->finances['tips'],
                        'markup'            => $this->finances['markup'],
                        'runnerCosts'       => $this->finances['runnerCosts'],
                        'dispatchCosts'     => $this->finances['dispatchCosts'],
                    ],
                    'ordersWithOrderItemDiscrepancies' => $this->ordersWithOrderItemDiscrepancies,
                    'general'                          => $this->general
                ])
                ->attach($this->attachment, ['as' => 'daily-status-' . $this->date . '.xlsx']);
        } else {
            return $this->to($this->emails)
                ->markdown('emails.daily-status-sent')
                ->subject('Local | Daily Status')
                ->with([
                    'date'                      => $date,
                    'scheduledRunners'          => $this->scheduledRunners,
                    'finances'                  => [
                        'revenue'           => $this->finances['revenue'],
                        'costOfGoodsSold'   => $this->finances['costOfGoodsSold'],
                        'deliveryFees'      => $this->finances['deliveryFees'],
                        'serviceFees'       => $this->finances['serviceFees'],
                        'discounts'         => $this->finances['discounts'],
                        'stripeFees'        => $this->finances['stripeFees'],
                        'tips'              => $this->finances['tips'],
                        'markup'            => $this->finances['markup'],
                        'runnerCosts'       => $this->finances['runnerCosts'],
                        'dispatchCosts'     => $this->finances['dispatchCosts'],
                    ],
                    'ordersWithOrderItemDiscrepancies' => $this->ordersWithOrderItemDiscrepancies,
                    'general'                          => $this->general
                ])
                ->attach($this->attachment, ['as' => 'daily-status-' . $this->date . '.xlsx']);
        }
    }
}
