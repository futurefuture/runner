<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Mailable
{
    use SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;

        if (app()->environment() === 'production') {
            return $this->markdown('emails.user-registered')
                ->subject('Thanks For Registering ' . $user->first_name)
                ->with([
                    'user' => $user,
                ]);
        } elseif (app()->environment() === 'staging') {
            return $this->markdown('emails.user-registered')
                ->subject('Staging | Thanks For Registering ' . $user->first_name)
                ->with([
                    'user' => $user,
                ]);
        } else {
            return $this->markdown('emails.user-registered')
                ->subject('Local | Thanks For Registering ' . $user->first_name)
                ->with([
                    'user' => $user,
                ]);
        }
    }
}
