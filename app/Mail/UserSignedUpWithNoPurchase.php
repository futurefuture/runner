<?php

namespace App\Mail;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Vinkla\Hashids\Facades\Hashids;

class UserSignedUpWithNoPurchase extends Mailable
{
    use Queueable, SerializesModels;

    protected $recentUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $recentUser)
    {
        $this->recentUser = $recentUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (app()->environment() == 'production') {
            return $this
                    ->markdown('emails.user-signed-up-with-no-purchase')
                    ->subject('Howdy :)')
                    ->with([
                        'recentUser' => $this->recentUser,
                    ]);
        } elseif (app()->environment() == 'staging') {
            return $this
                    ->markdown('emails.user-signed-up-with-no-purchase')
                    ->subject('Howdy :)')
                    ->with([
                        'recentUser' => $this->recentUser,
                    ]);
        } else {
            return $this
                    ->markdown('emails.user-signed-up-with-no-purchase')
                    ->subject('Howdy :)')
                    ->with([
                        'recentUser' => $this->recentUser,
                    ]);
        }
    }
}
