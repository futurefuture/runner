<?php

namespace App\Mail;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OC extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order           = $this->order;
        $order->content  = json_decode($order->content);
        $order->customer = json_decode($order->customer);

        if (app()->environment() === 'production') {
            return $this->markdown('emails.oc')
                ->subject('Scheduled Order Confirmation #' . $order->id)
                ->with([
                    'order' => $order,
                ]);
        } elseif (app()->environment() === 'staging') {
            return $this->markdown('emails.oc')
                ->subject('Staging | Scheduled Order Confirmation #' . $order->id)
                ->with([
                    'order' => $order,
                ]);
        } else {
            return $this->markdown('emails.oc')
                ->subject('Local | Scheduled Order Confirmation #' . $order->id)
                ->with([
                    'order' => $order,
                ]);
        }
    }
}
