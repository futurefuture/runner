<?php

namespace App\Mail;

use App\ProductFinal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OutOfStockNotified extends Mailable
{
    use Queueable, SerializesModels;

    protected $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ProductFinal $product)
    {
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $product = $this->product;

        return $this->markdown('emails.out_of_stock')
                    ->subject(env('APP_ENV').'| Restock Notification :'.$product->name)
                    ->with([
                        'product' => $product,
                    ]);
    }
}
