<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductUpdateMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $attachment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($startDate, $attachment, $emails)
    {
        $this->attachment = $attachment;
        $this->startDate = $startDate;
        $this->emails = $emails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->emails)
            ->subject('Product Update Report-(' . $this->startDate . ')')
            ->markdown('emails.product_update')
            ->attach($this->attachment, ['as' => 'product_update' . $this->startDate . '.xlsx']);

        return $mail;
    }
}
