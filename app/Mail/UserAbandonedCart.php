<?php

namespace App\Mail;

use App\Cart;
use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserAbandonedCart extends Mailable implements shouldQueue
{
    use Queueable, SerializesModels;

    protected $user;

    protected $cart;

    protected $products;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Cart $f)
    {
        $this->user = $user;
        $this->cart = $f;
        $this->products = json_decode($this->cart->content)->products;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->cart->marketing_status == 0) {
            return $this
                ->markdown('emails.user-abandoned-cart')
                ->subject('Your Saved Cart')
                ->with([
                    'user'     => $this->user,
                    'cart'     => $this->cart,
                    'products' => $this->products,
                ]);
        } else {
            return $this
                ->markdown('emails.user-abandoned-cart-with-discount')
                ->subject('Your Saved Cart')
                ->with([
                    'user'     => $this->user,
                    'cart'     => $this->cart,
                    'products' => $this->products,
                ]);
        }
    }
}
