<?php

namespace App\Mail;

use App\Gift;
use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Vinkla\Hashids\Facades\Hashids;

class OrderReceived extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    protected $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, $type = null)
    {
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $type = $this->type;
        $order->content = (gettype($order->content) == 'string') ? json_decode($order->content) : $order->content;
        $order->address = (gettype($order->address) == 'string') ? json_decode($order->address) : $order->address;
        $order->customer = (gettype($order->customer) == 'string') ? json_decode($order->customer) : $order->customer;

        if ($order->runner_1 && $order->runner_2) {
            $runner_1 = User::find($order->runner_1);
            $runner_2 = User::find($order->runner_2);
            $runners = $runner_1->first_name.' '.$runner_1->last_name.' & '.$runner_2->first_name.' '.$runner_2->last_name;
        } elseif ($order->runner_1 && ! $order->runner_2) {
            $runner_1 = User::find($order->runner_1);
            $runners = $runner_1->first_name.' '.$runner_1->last_name;
        } elseif (! $order->runner_1 && $order->runner_2) {
            $runner_2 = User::find($order->runner_2);
            $runners = $runner_2->first_name.' '.$runner_2->last_name;
        } else {
            $runners = 'Unknown';
        }

        $gift = Gift::where('order_id', $this->order->id)->first();

        if ($gift) {
            $order->serviceFee = 500;
        }

        if (app()->environment() == 'production') {
            return $this->markdown('emails.order-received')
                        ->subject('Order Receipt #'.$order->id)
                        ->with([
                                'order'   => $order,
                                'runners' => $runners,
                                'type'    => $type,
                                'hashId'  => Hashids::encode($order->id),
                            ]);
        } elseif (app()->environment() == 'staging') {
            return $this->markdown('emails.order-received')
                        ->subject('Staging | Order Receipt #'.$order->id)
                        ->with([
                                'order'   => $order,
                                'runners' => $runners,
                                'type'    => $type,
                                'hashId'  => Hashids::encode($order->id),
                            ]);
        } else {
            return $this->markdown('emails.order-received')
                        ->subject('Local | Order Receipt #'.$order->id)
                        ->with([
                                'order'   => $order,
                                'runners' => $runners,
                                'type'    => $type,
                                'hashId'  => Hashids::encode($order->id),
                            ]);
        }
    }
}
