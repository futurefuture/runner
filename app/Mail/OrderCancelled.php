<?php

namespace App\Mail;

use App\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCancelled extends Mailable implements ShouldQueue
{
    use SerializesModels;

    protected $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;

        $order->content = json_decode($order->content);

        $user      = $order->user;
        $address   = $order->addressObject;
        $image_url = env('APP_URL') . '/public/assets/v2/Give-10-Get-10.jpg';

        if (app()->environment() === 'production') {
            return $this->markdown('emails.order-cancelled')
                ->subject('Order Cancelled #' . $order->id)
                ->with([
                    'order'     => $order,
                    'user'      => $user,
                    'address'   => $address,
                    'image_url' => $image_url,
                ]);
        } elseif (app()->environment() === 'staging') {
            return $this->markdown('emails.order-cancelled')
                ->subject('Staging | Order Cancelled #' . $order->id)
                ->with([
                    'order'     => $order,
                    'user'      => $user,
                    'address'   => $address,
                    'image_url' => $image_url,
                ]);
        } else {
            return $this->markdown('emails.order-cancelled')
                ->subject('Local | Order Cancelled #' . $order->id)
                ->with([
                    'order'     => $order,
                    'user'      => $user,
                    'address'   => $address,
                    'image_url' => $image_url,
                ]);
        }
    }
}
