<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TorontoLCBOTerritorySalesMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $attachment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($startDate, $endDate, $attachment, $emails)
    {
        $this->attachment = $attachment;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->emails = $emails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->emails)
            ->subject('Territory Order Report-('.$this->startDate.'-'.$this->endDate.')')
            ->markdown('emails.toronto-territory-sales')
            ->attach($this->attachment, ['as' => 'territory_order_sales_export_'.$this->startDate.'-'.$this->endDate.'.xlsx']);

        return $mail;
    }
}
