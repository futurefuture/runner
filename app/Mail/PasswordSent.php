<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordSent extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $password = $this->password;

        if (app()->environment() === 'production') {
            return $this->markdown('emails.password-sent')
                        ->subject('Thank you for registering')
                        ->with([
                            'user'      => $user,
                            'password'  => $password,
                        ]);
        } elseif (app()->environment() === 'staging') {
            return $this->markdown('emails.password-sent')
                        ->subject('Staging | Thank you for registering')
                        ->with([
                            'user'      => $user,
                            'password'  => $password,
                        ]);
        } else {
            return $this->markdown('emails.password-sent')
                        ->subject('Local | Thank you for registering')
                        ->with([
                            'user'      => $user,
                            'password'  => $password,
                        ]);
        }
    }
}
