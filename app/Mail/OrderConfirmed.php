<?php

namespace App\Mail;

use App\Gift;
use App\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $order->content = json_decode($order->content);
        $order->address = json_decode($order->address);
        $order->customer = json_decode($order->customer);

        $gift = Gift::where('order_id', $this->order->id)->first();

        if ($gift) {
            $order->serviceFee = 500;
        }

        if (app()->environment() === 'production') {
            return $this->markdown('emails.order-confirmed')
                ->subject('Order Confirmation #'.$order->id)
                ->with([
                    'order' => $order,
                ]);
        }
        if (app()->environment() === 'staging') {
            return $this->markdown('emails.order-confirmed')
                ->subject('Staging | Order Confirmation #'.$order->id)
                ->with([
                    'order' => $order,
                ]);
        } else {
            return $this->markdown('emails.order-confirmed')
                ->subject('Local | Order Confirmation #'.$order->id)
                ->with([
                    'order' => $order,
                ]);
        }
    }
}
