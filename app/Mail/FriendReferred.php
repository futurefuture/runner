<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FriendReferred extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $invite_url = env('APP_URL').'/register?inviteCode='.$user->invite_code;

        return $this->markdown('emails.friend-referred')
                    ->with([
                        'user'       => $user,
                        'invite_url' => $invite_url,
                    ]);
    }
}
