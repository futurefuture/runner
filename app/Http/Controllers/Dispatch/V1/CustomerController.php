<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Address;
use App\Http\Controllers\Controller;
use App\Invitation;
use App\Order;
use App\Services\NotificationService;
use App\User;
use Auth;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function getCustomers($query = null)
    {
        if ($query) {
            $customers = \App\User::where('last_name', 'LIKE', '%' . $query . '%')
                ->get()
                ->toArray();

            return response()->json([
                'data' => $customers,
            ]);
        } else {
            $customers = User::orderBy('id', 'desc')->paginate(15);

            return response()->json($customers);
        }
    }

    public function getCustomer($customer)
    {
        $customer               = User::find($customer);
        $customer->rewardPoints = $customer->rewardPointsBalance();

        $addressArr = $customer->addresses()->get()->toArray();

        if (count($addressArr) > 0) {
            $customer->address_objects = array_map(function ($ad) {
                $ad['address'] = json_decode($ad['formatted_address']);

                return $ad;
            }, $addressArr);
        } else {
            $customer->address_objects = [];
        }

        // this is for invited by someone
        $invitation           = Invitation::where('invited_user_id', $customer->id)->first();
        $customer->invited_by = null;

        if ($invitation) {
            $inviter = User::find($invitation->user_id);

            if ($inviter) {
                $customer->invited_by = (object) [
                    'id'    => $inviter->id,
                    'name'  => $inviter->first_name . ' ' . $inviter->last_name,
                ];
            }
        }

        // this is for this guy invited users
        $invites = Invitation::where('user_id', $customer->id)->pluck('invited_user_id');

        $customer->invite_users = [];

        if (count($invites) > 0) {
            $invite_users = User::findMany($invites);

            foreach ($invite_users as $key => $invite_user) {
                $invite_user->hasPurcahse = $invite_user->orders()->count() ? true : false;
            }

            $customer->invite_users = $invite_users;
        }

        unset($customer->address);

        return response()->json($customer);
    }

    public function saveAddress($customer, Request $request)
    {
        $user = User::find($customer);

        $data = $request->except('id');

        $address = Address::find($request->input('id'));

        if (! $address) {
            return response()->json([
                'errors' => [
                    'error' => 'address not found',
                ],
            ], 404);
        }

        $address->update($data);

        $addresses = $user->addresses()->update([
            'selected' => 0,
        ]);

        $address->update([
            'selected'  => 1,
        ]);

        return response()->json([
            'message' => 'success',
        ]);
    }

    public function changePassword($customer)
    {
        $user = User::find($customer);

        $user->update([
            'password' => bcrypt('runner12345'),
        ]);
    }

    public function deleteCustomer($customerId)
    {
        $user = User::find($customerId);

        if ($user->delete()) {
            return response()->json([
                    'success'=> 'customer has been removed',
                ]);
        } else {
            return response()->json([
                    'error'=> 'You Shall Not Pass',
                ], 400);
        }
    }

    public function getCustomerOrders($customer)
    {
        $orders = Order::where('user_id', $customer)
                    ->orderBy('id', 'DESC')
                    ->paginate(10);

        return response()->json($orders);
    }

    public function updateCustomer(Request $request, $customerId)
    {
        $data           = $request->input('customer');
        $customer       = User::find($customerId);
        $updateProperty = [
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'rewardPointsTransaction',
            'rewardPointsTransactionReason',
            'date_of_birth',
        ];

        $filteredData = [];

        foreach ($data as $key => $value) {
            if ($key === 'rewardPointsTransaction') {
                $customer->rewardPointsTransaction($value, 6, [
                    'reason' => $data['rewardPointsTransactionReason'],
                ]);

                $notificationService = new NotificationService();
                $notificationService->sendManualNotification($data['rewardPointsTransaction'], $data['rewardPointsTransactionReason'], $customer->id);
            } elseif (in_array($key, $updateProperty)) {
                array_push($filteredData, [
                    $key => $value,
                ]);
            }
        }

        $customer->update($data);
    }

    public function loginAsCustomer($customerId)
    {
        $user        = User::find($customerId);
        $accessToken = $user->createToken('MyApp')
                                ->accessToken;

        Auth::guard()->login($user);

        return response()->json([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => $accessToken,
                    'user'        => [
                        'id'        => $user->id,
                        'firstName' => $user->first_name,
                        'lastName'  => $user->last_name,
                    ],
                ],
            ],
        ]);
    }
}
