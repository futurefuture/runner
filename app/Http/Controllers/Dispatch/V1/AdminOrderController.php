<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Http\Controllers\Controller;
use App\Mail\OrderCancelled;
use App\Mail\OrderReceived;
use App\Mail\OrderReceivedForRunner;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AdminOrderController extends Controller
{
    public function login()
    {
        return view('dispatch.login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dispatch.orders');
    }

    public function order($order)
    {
        return view('dispatch.order', compact('order'));
    }

    public function getCustomerOrders(User $customer, $orderId = null)
    {
        if ($orderId) {
            if (! $order = $customer->orders()->where('id', $orderId)->first()) {
                $order = 'order not found or order maded by another customer';

                return view('dispatch.customer-order', compact('order'));
            }

            $order->content = json_decode($order->content);

            $order->activity_log = json_decode($order->activity_log);

            return view('dispatch.customer-order', compact('order'));
        } else {
            $orders = $customer->orders;

            foreach ($orders as $key => $order) {
                $order->content = json_decode($order->content);

                $order->activity_log = json_decode($order->activity_log);

                $order->content->items_excerpt = '';

                $last_key = count($order->content->items);

                foreach ($order->content->items as $key => $item) {
                    if ($key == $last_key - 1) {
                        $order->content->items_excerpt = $order->content->items_excerpt.$item->qty.' '.$item->name;
                    } else {
                        $order->content->items_excerpt = $order->content->items_excerpt.$item->qty.' '.$item->name.', ';
                    }
                }

                $order->content->items_excerpt = Str::limit($order->content->items_excerpt, 70);
            }
            $runners = User::where('role', 2)
                ->orwhere('role', 1)
                ->get();

            return view('dispatch.customer-orders', compact(['orders', 'runners']));
        }
    }

    public function getRunnerOrders($orderId)
    {
        $order = Order::find($orderId);

        $order->content = json_decode($order->content);

        $order->activity_log = json_decode($order->activity_log);

        return view('dispatch.runners.runner-order', compact('order'));
    }

    public function changeStatus(Order $order, $status)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $order->content = json_decode($order->content);
        $activity_log = json_decode($order->activity_log);

        if ($order->status == 0) {
            if ($status == 'processing') {
                $order->status = 1;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->activity_log = json_encode($activity_log);
                $order->content = json_encode($order->content);
                $order->save();
                $order->content = json_decode($order->content);
            } elseif ($status == 'cancel') {
                $re = \Stripe\Refund::create([
                    'charge' => $order->charge,
                ]);

                $order->content->refund = (object) [
                    'refund_items'      => '',
                    'refund_percentage' => '',
                    'refund_custom'     => '',
                    'refund_amount'     => $order->content->total,
                    'revised_total'     => 0,
                ];

                $order->status = 5;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->content = json_encode($order->content);
                $order->activity_log = json_encode($activity_log);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    Mail::to($order->content->customer->email)->send(new OrderCancelled($order));
                    Mail::to('service@getrunner.io')->send(new OrderCancelled($order));
                }
            } elseif ($status == 'refund') {
                return view('dispatch.order-refund');
            }
        } elseif ($order->status == 1) {
            if ($status == 'onroute') {
                $order->status = 2;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->activity_log = json_encode($activity_log);
                $order->content = json_encode($order->content);
                $order->save();
                $order->content = json_decode($order->content);
            } elseif ($status == 'cancel') {
                $re = \Stripe\Refund::create([
                    'charge' => $order->charge,
                ]);

                $order->content->refund = (object) [
                    'refund_items'      => '',
                    'refund_percentage' => '',
                    'refund_custom'     => '',
                    'refund_amount'     => $order->content->total,
                    'revised_total'     => 0,
                ];

                $order->status = 5;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->content = json_encode($order->content);
                $order->activity_log = json_encode($activity_log);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    Mail::to($order->content->customer->email)->send(new OrderCancelled($order));
                    Mail::to('service@getrunner.io')->send(new OrderCancelled($order));
                }
            } elseif ($status == 'refund') {
                return view('dispatch.order-refund');
            }
        } elseif ($order->status == 2) {
            if ($status == 'delivered') {
                $order->status = 3;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->activity_log = json_encode($activity_log);
                $order->content = json_encode($order->content);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    $runner = User::find($order->runner_1);

                    Mail::to($order->content->customer->email)->send(new OrderReceived($order));
                    Mail::to('service@getrunner.io')->send(new OrderReceived($order));
                    Mail::to($runner->email)->send(new OrderReceivedForRunner($order));

                    try {
                        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                        $charge = \Stripe\Charge::retrieve($order->charge);
                        $charge->capture();
                    } catch (\Stripe\Error\Card $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];

                        if (strpos($err['message'], 'zip') == true) {
                            return response()->json([
                                'errors' => [
                                    'error' => $zipMessage,
                                ],
                            ], 400);
                        } else {
                            return response()->json([
                                'errors' => [
                                    'error' => $err['message'],
                                ],
                            ], 400);
                        }
                    } catch (\Stripe\Error\ApiConnection $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];

                        return response()->json([
                            'errors' => [
                                'error' => $err['message'],
                            ],
                        ], 404);
                    }
                }
            } elseif ($status == 'cancel') {
                $re = \Stripe\Refund::create([
                    'charge' => $order->charge,
                ]);

                $order->content->refund = (object) [
                    'refund_items'      => '',
                    'refund_percentage' => '',
                    'refund_custom'     => '',
                    'refund_amount'     => $order->content->total,
                    'revised_total'     => 0,
                ];

                $order->status = 5;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->content = json_encode($order->content);
                $order->activity_log = json_encode($activity_log);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    Mail::to($order->content->customer->email)->send(new OrderCancelled($order));
                    Mail::to('service@getrunner.io')->send(new OrderCancelled($order));
                }
            } elseif ($status == 'refund') {
                return view('dispatch.order-refund');
            }
        } elseif ($order->status == 3) {
            if ($status == 'cancel') {
                $re = \Stripe\Refund::create([
                    'charge' => $order->charge,
                ]);

                $order->content->refund = (object) [
                    'refund_items'      => '',
                    'refund_percentage' => '',
                    'refund_custom'     => '',
                    'refund_amount'     => $order->content->total,
                    'revised_total'     => 0,
                ];

                $order->status = 5;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->content = json_encode($order->content);
                $order->activity_log = json_encode($activity_log);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    Mail::to($order->content->customer->email)->send(new OrderCancelled($order));
                    Mail::to('service@getrunner.io')->send(new OrderCancelled($order));
                }
            } elseif ($status == 'refund') {
                return view('dispatch.order-refund');
            }
        } elseif ($order->status == 4) {
            if ($status == 'cancel') {
                $re = \Stripe\Refund::create([
                    'charge' => $order->charge,
                ]);

                $order->content->refund = (object) [
                    'refund_items'      => '',
                    'refund_percentage' => '',
                    'refund_custom'     => '',
                    'refund_amount'     => $order->content->total,
                    'revised_total'     => 0,
                ];

                $order->status = 5;

                array_push($activity_log, (object) [
                    'log'  => $status,
                    'time' => $order->updated_at,
                ]);

                $order->content = json_encode($order->content);
                $order->activity_log = json_encode($activity_log);
                $order->save();
                $order->content = json_decode($order->content);
                if (env('APP_ENV') === 'production') {
                    Mail::to($order->content->customer->email)->send(new OrderCancelled($order));
                    Mail::to('service@getrunner.io')->send(new OrderCancelled($order));
                }
            } elseif ($status == 'refund') {
                return view('dispatch.order-refund');
            }
        }

        return redirect()->back();
    }

    public function deleteOrder(Request $request, Order $order)
    {
        $order->delete();

        return response()->json([
            'success' => 'order deleted.',
        ]);
    }
}
