<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Coupon;
use App\Gift;
use App\Http\Controllers\Controller;
use App\Invitation;
use App\Mail\OrderCancelled;
use App\Mail\OrderReceived;
use App\Order;
use App\Product;
use App\StoreFinal;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AdminOrderApiController extends Controller
{
    /**
     * Get all orders.
     *
     * @param string $query
     * @return array
     */
    public function getOrders($query = null)
    {
        if ($query) {
            if ($query === 'ready') {
                $orders = Order::whereIn('status', [0, 7])
                    ->orderBy('created_at', 'desc')
                    ->whereNull('runner_1')
                    ->whereNull('runner_2')
                    ->get();

                foreach ($orders as $key => $order) {
                    $order->content       = json_decode($order->content);
                    $order->customer      = json_decode($order->customer);
                    $order->address       = json_decode($order->address);
                    $order->items_excerpt = '';
                    $last_key             = count($order->content);

                    foreach ($order->content as $key => $item) {
                        if (isset($item->quantity)) {
                            $iq = $item->quantity;
                        } else {
                            $iq = $item->qty;
                        }
                        if (isset($item->title)) {
                            $it = $item->title;
                        } else {
                            $it = $item->name;
                        }

                        if ($key === $last_key - 1) {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it;
                        } else {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it . ', ';
                        }

                        $gift = Gift::where('order_id', $order->id)->first();

                        if ($gift) {
                            $gift->address = json_decode($gift->address);
                            $order->gift   = $gift;
                        }
                    }

                    if (strlen($order->items_excerpt) > 30) {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30) . '...';
                    } else {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30);
                    }
                }
            } elseif ($query === 'received') {
                $orders = Order::where('status', 0)
                    ->orderBy('created_at', 'desc')
                    ->paginate(50);

                foreach ($orders as $key => $order) {
                    $order->content       = json_decode($order->content);
                    $order->customer      = json_decode($order->customer);
                    $order->address       = json_decode($order->address);
                    $order->items_excerpt = '';
                    $last_key             = count($order->content);

                    foreach ($order->content as $key => $item) {
                        if (isset($item->quantity)) {
                            $iq = $item->quantity;
                        } else {
                            $iq = $item->qty;
                        }
                        if (isset($item->title)) {
                            $it = $item->title;
                        } else {
                            $it = $item->name;
                        }

                        if ($key === $last_key - 1) {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it;
                        } else {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it . ', ';
                        }

                        $gift = Gift::where('order_id', $order->id)->first();

                        if ($gift) {
                            $gift->address = json_decode($gift->address);
                            $order->gift   = $gift;
                        }
                    }

                    if (strlen($order->items_excerpt) > 30) {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30) . '...';
                    } else {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30);
                    }

                    if ($order->runner_1 && ! $order->runner_2) {
                        $order->runner = ($runner = User::find($order->runner_1)) ? $runner->first_name : 'Unknown';
                    } elseif (! $order->runner_1 && $order->runner_2) {
                        $order->runner = ($runner = User::find($order->runner_2)) ? $runner->first_name : 'Unknown';
                    } elseif ($order->runner_1 && $order->runner_2) {
                        $order->runner = (($runner = User::find($order->runner_1)) ? $runner->first_name : '') . '+' . (($runner = User::find($order->runner_2)) ? $runner->first_name : '');
                    } else {
                        $order->runner = 'Unassigned';
                    }
                }
            } elseif ($query === 'scheduled') {
                $orders = Order::where('status', 7)
                    ->orderBy('created_at', 'desc')
                    ->paginate(50);

                foreach ($orders as $key => $order) {
                    $order->content       = json_decode($order->content);
                    $order->customer      = json_decode($order->customer);
                    $order->address       = json_decode($order->address);
                    $order->items_excerpt = '';
                    $last_key             = count($order->content);

                    foreach ($order->content as $key => $item) {
                        if (isset($item->quantity)) {
                            $iq = $item->quantity;
                        } else {
                            $iq = $item->qty;
                        }
                        if (isset($item->title)) {
                            $it = $item->title;
                        } else {
                            $it = $item->name;
                        }

                        if ($key == $last_key - 1) {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it;
                        } else {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it . ', ';
                        }

                        $gift = Gift::where('order_id', $order->id)->first();

                        if ($gift) {
                            $gift->address = json_decode($gift->address);
                            $order->gift   = $gift;
                        }

                        if ($order->runner_1 && ! $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_1)) ? $runner->first_name : 'Unknown';
                        } elseif (! $order->runner_1 && $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_2)) ? $runner->first_name : 'Unknown';
                        } elseif ($order->runner_1 && $order->runner_2) {
                            $order->runner = (($runner = User::find($order->runner_1)) ? $runner->first_name : '') . '+' . (($runner = User::find($order->runner_2)) ? $runner->first_name : '');
                        } else {
                            $order->runner = 'Unassigned';
                        }
                    }

                    if (strlen($order->items_excerpt) > 30) {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30) . '...';
                    } else {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30);
                    }
                }
            } elseif ($query === 'in_progress') {
                $orders = Order::whereIn('status', [1, 2, 4])
                    ->orderBy('created_at', 'desc')
                    ->paginate(50);

                foreach ($orders as $key => $order) {
                    $order->content       = json_decode($order->content);
                    $order->customer      = json_decode($order->customer);
                    $order->address       = json_decode($order->address);
                    $order->items_excerpt = '';
                    $last_key             = count($order->content);

                    foreach ($order->content as $key => $item) {
                        if (isset($item->quantity)) {
                            $iq = $item->quantity;
                        } else {
                            $iq = $item->qty;
                        }
                        if (isset($item->title)) {
                            $it = $item->title;
                        } else {
                            $it = $item->name;
                        }

                        if ($key == $last_key - 1) {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it;
                        } else {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it . ', ';
                        }

                        $gift = Gift::where('order_id', $order->id)->first();

                        if ($gift) {
                            $gift->address = json_decode($gift->address);
                            $order->gift   = $gift;
                        }

                        if ($order->runner_1 && ! $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_1)) ? $runner->first_name : 'Unknown';
                        } elseif (! $order->runner_1 && $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_2)) ? $runner->first_name : 'Unknown';
                        } elseif ($order->runner_1 && $order->runner_2) {
                            $order->runner = (($runner = User::find($order->runner_1)) ? $runner->first_name : '') . '+' . (($runner = User::find($order->runner_2)) ? $runner->first_name : '');
                        } else {
                            $order->runner = 'Unassigned';
                        }
                    }

                    if (strlen($order->items_excerpt) > 30) {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30) . '...';
                    } else {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30);
                    }
                }
            } elseif ($query === 'complete') {
                $orders = Order::whereIn('status', [3, 5, 6])
                    ->orderBy('created_at', 'desc')
                    ->paginate(50);

                foreach ($orders as $key => $order) {
                    $order->content       = json_decode($order->content);
                    $order->customer      = json_decode($order->customer);
                    $order->address       = json_decode($order->address);
                    $order->items_excerpt = '';
                    $last_key             = count($order->content);

                    foreach ($order->content as $key => $item) {
                        if (isset($item->quantity)) {
                            $iq = $item->quantity;
                        } else {
                            $iq = $item->qty;
                        }
                        if (isset($item->title)) {
                            $it = $item->title;
                        } else {
                            $it = $item->name;
                        }

                        if ($key == $last_key - 1) {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it;
                        } else {
                            $order->items_excerpt = $order->items_excerpt . $iq . ' ' . $it . ', ';
                        }

                        $gift = Gift::where('order_id', $order->id)->first();

                        if ($gift) {
                            $gift->address = json_decode($gift->address);
                            $order->gift   = $gift;
                        }

                        if ($order->runner_1 && ! $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_1)) ? $runner->first_name : 'Unknown';
                        } elseif (! $order->runner_1 && $order->runner_2) {
                            $order->runner = ($runner = User::find($order->runner_2)) ? $runner->first_name : 'Unknown';
                        } elseif ($order->runner_1 && $order->runner_2) {
                            $order->runner = (($runner = User::find($order->runner_1)) ? $runner->first_name : '') . '+' . (($runner = User::find($order->runner_2)) ? $runner->first_name : '');
                        } else {
                            $order->runner = 'Unassigned';
                        }
                    }

                    if (strlen($order->items_excerpt) > 30) {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30) . '...';
                    } else {
                        $order->items_excerpt = Str::limit($order->items_excerpt, 30);
                    }
                }
            } else {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $order               = Order::find($query);
                $order->content      = json_decode($order->content, true);
                $order->customer     = json_decode($order->customer);
                $order->address      = json_decode($order->address);
                $order->activity_log = json_decode($order->activity_log);
                $products_id         = [];
                $stores_id           = [];

                foreach ($order->content as $item) {
                    array_push($products_id, $item['id']);
                }

                if ($order->activity_log != null) {
                    foreach ($order->activity_log as $log) {
                        $log->time = date('m/d/Y h:i A', strtotime($log->time->date));
                    }
                }

                $isGoogleorApplePay = false;

                if (strpos($order->charge, '[') !== false) {
                    $arrCharge     = json_decode($order->charge);
                    $arrChargeObjs = [];

                    foreach ($arrCharge as $charge) {
                        try {
                            $charge = \Stripe\Charge::retrieve($charge);
                            if ($isGoogleorApplePay == false && $charge->source->tokenization_method != null) {
                                $isGoogleorApplePay = true;
                            }
                            array_push($arrChargeObjs, (object) [
                                'id'     => $charge->id,
                                'amount' => $charge->amount / 100,
                            ]);
                        } catch (\Stripe\Error\Card $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        } catch (\Stripe\Error\Authentication $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        } catch (\Stripe\Error\ApiConnection $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        } catch (\Stripe\Error\Base $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        } catch (Exception $e) {
                            $body = $e->getJsonBody();
                            $err  = $body['error'];

                            return $err;
                        }
                    }

                    $order->charge = $arrChargeObjs;
                } else {
                    $arrChargeObjs = [];

                    try {
                        $charge = \Stripe\Charge::retrieve($order->charge);

                        if ($isGoogleorApplePay == false && $charge->source->tokenization_method != null) {
                            $isGoogleorApplePay = true;
                        }

                        array_push($arrChargeObjs, (object) [
                            'id'     => $charge->id,
                            'amount' => $charge->amount / 100,
                        ]);
                    } catch (\Stripe\Error\Card $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    } catch (\Stripe\Error\Authentication $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    } catch (\Stripe\Error\ApiConnection $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    } catch (\Stripe\Error\Base $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    } catch (Exception $e) {
                        $body = $e->getJsonBody();
                        $err  = $body['error'];

                        return $err;
                    }

                    $order->charge = $arrChargeObjs;
                }

                if ($order->admin_notes) {
                    $order->admin_notes = json_decode($order->admin_notes);
                }

                if ($order->schedule_time) {
                    $order->schedule_time = Carbon::parse($order->schedule_time)
                        ->format('M dS - g A');
                } else {
                    $order->schedule_time = 'ASAP';
                }

                $invite = Invitation::where('invited_user_id', $order->user_id)
                    ->first();

                $userCoupon = DB::table('coupon_user')->where('order_id', $order->id)->first();

                if ($userCoupon) {
                    $discountType = Coupon::find($userCoupon->coupon_id)->code;
                } elseif ($order->coupon_id === null && $order->first_time === 1 && $order->discount > 0 && $invite) {
                    $invitedByUser = User::find($invite->user_id);
                    $discountType  = 'Referred By ' . $invitedByUser->first_name . $invitedByUser->last_name;
                } elseif ($order->coupon_id === null && $order->first_time === 0 && $order->discount > 0) {
                    $discountType = 'Reward Points';
                } else {
                    $discountType = null;
                }

                $order->discountType = $discountType;

                $gift = Gift::where('order_id', $order->id)
                    ->first();

                if ($gift) {
                    $gift->address = json_decode($gift->address);
                    $order->gift   = $gift;
                }

                $order->isGoogleorApplePay = $isGoogleorApplePay;

                return $order;
            }
        } else {
            $orders = Order::all();
        }

        return $orders;
    }

    /**
     * Gets completed orders locations.
     *
     * @return array
     */
    public function getCompleteOrdersLocation()
    {
        $orders = Order::whereIn('status', [3, 5, 6])
            ->orderBy('updated_at', 'desc')
            ->get()
            ->toArray();

        $ordersLoc = array_map(function ($order) {
            $order['content'] = json_decode($order['content']);
            $order['customer'] = json_decode($order['customer']);
            $order['address'] = json_decode($order['address']);

            return (object) [
                'id'           => $order['id'],
                'name'         => $order['customer'] ? $order['customer']->first_name . ' ' . $order['customer']->last_name : 'Unknown',
                'email'        => $order['customer'] ? $order['customer']->email : 'Unknown',
                'phone_number' => $order['customer'] ? $order['customer']->phone_number : 'Unknown',
                'address'      => $order['address'] ? $order['address']->address : 'Unknown',
                'lat'          => $order['address'] ? $order['address']->lat : 'Unknown',
                'lng'          => $order['address'] ? $order['address']->lng : 'Unknown',
            ];
        }, $orders);

        return $ordersLoc;
    }

    /**
     * Cancels order.
     *
     * @param Order $order
     * @return Response
     */
    public function cancelOrder($orderId)
    {
        $order          = Order::find($orderId);
        $order->content = json_decode($order->content);
        $activity_log   = $order->activity_log != null ? json_decode($order->activity_log) : [];
        $order->status  = 5;

        array_push($activity_log, (object) [
            'log'  => 'cancelled',
            'time' => $order->updated_at,
        ]);

        $order->activity_log = json_encode($activity_log);
        $customerEmail       = json_decode($order->customer)->email;
        $order->content      = json_encode($order->content);
        $order->stripe_fee   = 0;

        if ($order->save()) {
            $user = User::find($order->user->id);

            $user->rewardPointsTransaction(-100, 3, [
                'order_id' => $order->id,
            ]);

            DB::table('promotions_used')->where('order_id', $order->id)->update([
                'used'  => 0,
            ]);
            if (env('APP_ENV') === 'production') {
                Mail::to($customerEmail)->send(new OrderCancelled($order));
            }

            return response()->json(['success']);
        } else {
            return response()->json([
                'error' => 'cant update',
            ], 401);
        }
    }

    /**
     * Sends order receipt to customer.
     *
     * @param Order $order
     * @return Mail
     */
    public function sendReceipt($order)
    {
        $o           = Order::where('id', $order)->first();
        $o->customer = json_decode($o->customer);
        if (env('APP_ENV') === 'production') {
            Mail::to($o->customer->email)->send(new OrderReceived($o, 'revised'));
        }
    }

    /**
     * Updates order details.
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function updateOrder($orderId, Request $request)
    {
        $order               = Order::find($orderId);
        $data                = $request->all();
        $order->content      = json_decode($order->content);
        $order->customer     = json_decode($order->customer);
        $order->activity_log = json_decode($order->activity_log);
        $order->address      = json_decode($order->address);
        $order->admin_notes  = json_decode($order->admin_notes);

        if ($request->filled('first_name')) {
            $order->customer->first_name = $request->input('first_name');
        }

        if ($request->filled('last_name')) {
            $order->customer->last_name = $request->input('last_name');
        }

        if ($request->filled('email')) {
            $order->customer->email = $request->input('email');
        }

        if ($request->filled('phone_number')) {
            $order->customer->phone_number = $request->input('phone_number');
        }

        if ($request->filled('address')) {
            $address                 = json_decode($request->input('address'));
            $order->address->address = $address->printable_address;
            $order->address->lat     = $address->lat;
            $order->address->lng     = $address->lng;
        }

        if ($request->filled('unitno')) {
            $order->unit_number = $request->input('unitno');
        }

        if ($request->filled('instructions')) {
            $order->instructions = $request->input('instructions');
        }

        $order->content      = json_encode($order->content);
        $order->customer     = json_encode($order->customer);
        $order->activity_log = json_encode($order->activity_log);
        $order->address      = json_encode($order->address);
        $order->admin_notes  = json_encode($order->admin_notes);

        $order->save();
    }

    /**
     * Update order items.
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function updateItems($orderId, Request $request)
    {
        $order          = Order::find($orderId);
        $newOrderItems  = $request->all();
        $order->content = json_decode($order->content);
        $newItems       = [];

        foreach ($newOrderItems as $key => $newItem) {
            if (isset($newItem['quantity'])) {
                array_push($newItems, [
                    'id'          => $newItem['id'],
                    'type'        => 'product',
                    'title'       => $newItem['title'],
                    'quantity'    => $newItem['quantity'],
                    'runnerPrice' => $newItem['runnerPrice'],
                    'truePrice'   => ($newItem['runnerPrice'] / 1.12),
                    'packaging'   => $newItem['packaging'],
                ]);
            } elseif (isset($newItem['qty'])) {
                array_push($newItems, [
                    'id'           => $newItem['id'],
                    'type'         => isset($newItem['type']),
                    'name'         => $newItem['name'],
                    'qty'          => $newItem['qty'],
                    'runner_price' => $newItem['runner_price'],
                    'true_price'   => $newItem['true_price'],
                    'package'      => $newItem['package'],
                ]);
            }
        }

        $subtotal = 0;
        $cogs     = 0;
        $markup   = 0;

        foreach ($newItems as &$newItem) {
            if (isset($newItem['qty'])) {
                $subtotal += $newItem['qty'] * $newItem['runner_price'];
                $cogs += $newItem['qty'] * $newItem['true_price'];
                $markup += $newItem['qty'] * ($newItem['runner_price'] - $newItem['true_price']);
            } else {
                $subtotal += $newItem['quantity'] * ($newItem['runnerPrice']);
                $cogs += $newItem['quantity'] * ($newItem['truePrice']);
                $markup += $newItem['quantity'] * (($newItem['runnerPrice'] - $newItem['truePrice']));
            }
        }

        $tax   = (int) (($markup + $order->delivery + $order->service_fee) * 0.13);
        $total = (int) ($subtotal + $order->delivery + $order->tip + $tax - $order->discount);

        if ($order->subtotal > $subtotal) {
            // Release less amount
            $refundAmount             = (int) ($order->subtotal - $subtotal);
            $order->total             = (int) $total;
            $order->content           = $newItems;
            $order->subtotal          = (int) $subtotal;
            $order->cogs              = (int) $cogs;
            $order->markup            = (int) $markup;
            $order->display_tax_total = (int) $tax;
            $order->content           = json_encode($order->content);
            $activity_log             = $order->activity_log != null ? json_decode($order->activity_log) : [];

            array_push($activity_log, (object) [
                'log'   => 'TotalDecreased:amount to refund is $' . number_format($refundAmount / 100, 2, '.', ','),
                'time'  => \Carbon\Carbon::now(),
            ]);

            $order->activity_log = json_encode($activity_log);
            $order->save();

            return response()->json([
                'status' => 'Need to refund $' . number_format($refundAmount / 100, 2, '.', ',') . '.',
            ]);
        } elseif ($order->subtotal < $subtotal) {
            // Make another charge
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            $chargeAmount = $subtotal - $order->subtotal;

            try {
                $charge = \Stripe\Charge::create([
                    'amount'      => $chargeAmount,
                    'currency'    => 'cad',
                    'customer'    => $order->user->stripe_id,
                    'description' => 'extra charge for order number ' . $order->id,
                    'capture'     => true,
                ]);
            } catch (\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                return $err;
            } catch (\Stripe\Error\ApiConnection $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                return $err;
            }

            $order->total             = (int) $total;
            $order->content           = $newItems;
            $order->subtotal          = (int) $subtotal;
            $order->cogs              = (int) $cogs;
            $order->markup            = (int) $markup;
            $order->display_tax_total = (int) $tax;
            $order->total             = (int) $total;
            $order->content           = json_encode($order->content);

            if (strpos($order->charge, '[') !== false) {
                $arrCharge = json_decode($order->charge);
                array_push($arrCharge, $charge->id);
                $order->charge = json_encode($arrCharge);
            } else {
                $order->charge = json_encode([$order->charge, $charge->id]);
            }

            $activity_log = json_decode($order->activity_log);

            array_push($activity_log, (object) [
                'log'   => 'TotalIncreased:has made another charge for $' . number_format($chargeAmount / 100, 2, '.', ','),
                'time'  => \Carbon\Carbon::now(),
            ]);

            $order->activity_log = json_encode($activity_log);
            $order->save();

            return response()->json([
                'status' => 'has made another charge for $' . number_format($chargeAmount / 100, 2, '.', ','),
            ]);
        } elseif ($order->subtotal === $subtotal) {
            // No need to do anything
            $order->total             = (int) $total;
            $order->content           = $newItems;
            $order->subtotal          = (int) $subtotal;
            $order->cogs              = (int) $cogs;
            $order->markup            = (int) $markup;
            $order->display_tax_total = (int) $tax;
            $order->content           = json_encode($order->content);
            $activity_log             = json_decode($order->activity_log);

            array_push($activity_log, (object) [
                'log'   => 'TotalEqualed:everything looks fine',
                'time'  => \Carbon\Carbon::now(),
            ]);

            $order->activity_log = json_encode($activity_log);
            $order->save();

            return response()->json([
                'status' => 'everything looks fine',
            ]);
        }
    }

    /**
     * Adds notes to order.
     *
     * @param Order $order
     * @param Request $request
     * @return array
     */
    public function addNotes(Order $order, Request $request)
    {
        $admin_notes = $request->input('admin_notes');

        if ($order->admin_notes === null) {
            $order->admin_notes = [];
            $arrNotes           = $order->admin_notes;
            array_push($arrNotes, $admin_notes);

            $order->update([
                'admin_notes' => json_encode($arrNotes),
            ]);
        } else {
            $order->admin_notes = json_decode($order->admin_notes);
            $arrNotes           = $order->admin_notes;
            array_push($arrNotes, $admin_notes);

            $order->update([
                'admin_notes' => json_encode($arrNotes),
            ]);
        }
    }

    /**
     * Get runner info.
     *
     * @param int $id
     * @return array
     */
    public function getRunner($id = null)
    {
        if ($id === 'onduty') {
            $now     = Carbon::now();
            $runners = User::where('role', 1)
                ->orWhere('role', 2)
                ->get();

            foreach ($runners as $runner) {
                $schedules = $runner->schedules()->get();
                $onduty    = false;

                foreach ($schedules as $schedule) {
                    if ($now <= $schedule->end && $now >= $schedule->start) {
                        $onduty = true;
                    }
                }

                $runner->onduty = $onduty;
                $location       = $runner
                    ->runnerlocations()
                    ->orderBy('created_at', 'desc')
                    ->first();

                if ($location) {
                    $runner->location = (object) [
                        'lat' => $location->latitude,
                        'lng' => $location->longitude,
                    ];
                } else {
                    $runner->location = (object) [
                        'lat' => null,
                        'lng' => null,
                    ];
                }
            }

            return $runners;
        } elseif ($id) {
            if ($runner = User::find($id)) {
                if ($runner->role === 2 || $runner->role === 1) {
                    return $runner;
                }
            } else {
                return 0;
            }
        } else {
            $runner = User::where('role', 1)
                ->orWhere('role', 2)
                ->get();

            foreach ($runner as $r) {
                $location = $r
                    ->runnerlocations()
                    ->orderBy('created_at', 'desc')
                    ->first();

                if ($location) {
                    $r->location = (object) [
                        'lat' => $location->latitude,
                        'lng' => $location->longitude,
                    ];
                } else {
                    $r->location = (object) [
                        'lat' => null,
                        'lng' => null,
                    ];
                }
            }

            return $runner;
        }
    }

    /**
     * Save customer signature.
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function saveSignature(Order $order, Request $request)
    {
        if ($request->filled('signature')) {
            $order->content            = json_decode($order->content);
            $order->content->signature = $request->input('signature');
            $order->content            = json_encode($order->content);
            $order->save();
        }
    }
}
