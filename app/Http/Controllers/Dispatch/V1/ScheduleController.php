<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Http\Controllers\Controller;
use App\Order;
use App\RunnerPosition;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function dispatchView()
    {
        return view('dispatch.schedule');
    }

    public function getScheduledOrders()
    {
        $orders = Order::whereNotNull('schedule_time')
                    ->get()
                    ->toArray();

        $orders = array_map(function ($order) {
            $order['content'] = json_decode($order['content']);
            $start = Carbon::parse($order['schedule_time'])->format('Y-m-d H:i');
            $end = Carbon::parse($order['schedule_time'])->addHour()->format('Y-m-d H:i');

            return (object) [
                'title'  => 'Order #'.$order['id'],
                'start'  => $start,
                'end'    => $end,
                'url'    => '/dispatch/orders/'.$order['id'],
                'allDay' => false,
            ];
        }, $orders);

        return $orders;
    }

    public function newSchedule(Request $request)
    {
        $data = $request->all();
        $user = User::find($data['employee']);

        if ($user) {
            $schedule = new Schedule;
            $schedule->job_type = $data['job_type'];
            $schedule->start = Carbon::parse($data['start']);
            $schedule->end = Carbon::parse($data['end']);
            $user->schedules()->save($schedule);
        } else {
            return response()->json([
                    'error' => 'user not found',
                ], 404);
        }
    }

    public function removeSchedule($id)
    {
        $schedule = Schedule::find($id);
        $schedule->delete();
    }

    public function updateSchedule($scheduleId, Request $request)
    {
        $data = $request->all();
        $schedule = Schedule::find($scheduleId);
        $schedule->user_id = $data['user']['id'];
        $schedule->job_type = $data['job']['id'];
        $schedule->start = Carbon::parse($data['start']);
        $schedule->end = Carbon::parse($data['end']);
        $schedule->save();
    }

    public function getSchedules($scheduleId = null)
    {
        if ($scheduleId != null) {
            $schedule = Schedule::find($scheduleId);
            $user = User::find($schedule['user_id']);
            $runnerPosition = RunnerPosition::find($schedule['job_type']);

            $schedule = (object) [
                'id'                => $schedule['id'],
                'user'              => $user,
                'title'             => $user->first_name.' '.$user->last_name.' / '.$runnerPosition->title,
                'start'             => $schedule['start'],
                'end'               => $schedule['end'],
                'backgroundColor'   => $runnerPosition->backgroundColor,
                'allDay'            => false,
                'job'               => $runnerPosition,
            ];

            return response()->json($schedule);
        }

        $schedules = Schedule::orderBy('start', 'desc')
                        ->limit(100)
                        ->get()
                        ->toArray();

        $schedules = array_map(function ($schedule) {
            $user = User::find($schedule['user_id']);
            $runnerPosition = RunnerPosition::find($schedule['job_type']);

            if ($user && $runnerPosition) {
                return (object) [
                    'id'              => $schedule['id'],
                    'title'           => $user->first_name.' '.$user->last_name.' / '.$runnerPosition->title,
                    'start'           => $schedule['start'],
                    'end'             => $schedule['end'],
                    'backgroundColor' => $runnerPosition->backgroundColor,
                    'allDay'          => false,
                ];
            }
        }, $schedules);

        // trim null values away.
        $cleanedSchedules = array_filter($schedules);

        return array_values($cleanedSchedules);
    }

    public function getEmployees()
    {
        $employees = User::whereIn('role', [1, 2, 5])->get();

        return $employees;
    }

    public function getRunnerPositions()
    {
        $runnerPositions = RunnerPosition::all();

        return $runnerPositions;
    }
}
