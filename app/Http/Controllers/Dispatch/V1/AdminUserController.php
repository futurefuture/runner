<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminUserController extends Controller
{
    public function getCustomers($customer = null)
    {
        $domain = env('APP_DOMAIN');

        if ($customer) {
            return view('dispatch.customer', compact(['customer', 'domain']));
        } else {
            return view('dispatch.customers');
        }
    }

    public function DispatchLogin(Request $request)
    {
        $validator = Validator::make($data, [
            'email'   => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
    }

    public function updateCustomer($customerId, Request $request)
    {
        $customer = User::find($customerId);
        $excepts = [];

        if ($request->input('unit') == 'No Unit') {
            array_push($excepts, 'unit');
        }

        if ($request->input('address') == 'No Address') {
            array_push($excepts, 'address');
        }

        if ($request->input('date_of_birth') == 'No Date Of Birth') {
            array_push($excepts, 'date_of_birth');
        }

        $data = $request->except($excepts);

        $customer->update($data);

        return redirect()->back();
    }

    public function getRunners($runnerId = null)
    {
        if ($runnerId) {
            $runner = User::find($runnerId);

            if ($runner->role == 3) {
                return redirect('/dispatch/customers/'.$runner->id);
            } else {
                $orders = Order::where('runner_1', $runner->id)->orWhere('runner_2', $runner->id)->get();

                foreach ($orders as $key => $order) {
                    $order->content = json_decode($order->content);
                    $order->activity_log = json_decode($order->activity_log);
                    $order->content->items_excerpt = '';
                    $last_key = count($order->content->items);

                    foreach ($order->content->items as $key => $item) {
                        if ($key == $last_key - 1) {
                            $order->content->items_excerpt = $order->content->items_excerpt.$item->qty.' '.$item->name;
                        } else {
                            $order->content->items_excerpt = $order->content->items_excerpt.$item->qty.' '.$item->name.', ';
                        }
                    }

                    if (strlen($order->content->items_excerpt) > 70) {
                        $order->content->items_excerpt = Str::limit($order->content->items_excerpt, 70).'...';
                    } else {
                        $order->content->items_excerpt = Str::limit($order->content->items_excerpt, 70);
                    }
                }

                return view('dispatch.runners.runner', compact(['runner', 'orders']));
            }
        } else {
            $runners = User::where('role', 2)->orWhere('role', 1)->get();

            return view('dispatch.runners.runners', compact('runners'));
        }
    }
}
