<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/orders';

    protected $fromUrl;

    public function logout()
    {
        Auth::logout();

        return response()->json(true);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) {
            $user = Auth::user();
            $accessToken = $user->createToken('Runner Dispatch')
                                        ->accessToken;
            $domain = env('APP_DOMAIN');
            $token = cookie('token', $accessToken, null, null, $domain, true, false);
            $userId = cookie('userId', $user->id, null, null, $domain, true, false);

            return redirect()->route('dispatch.orders')->withCookie($token)->withCookie($userId);
        } else {
            return redirect('/dispatch/login');
        }
    }
}
