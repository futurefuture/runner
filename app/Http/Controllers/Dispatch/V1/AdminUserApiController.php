<?php

namespace App\Http\Controllers\Dispatch\V1;

use App\Http\Controllers\Controller;
use App\Order;
use App\Services\NotificationService;
use App\Services\OrderService;
use App\Services\TaskService;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class AdminUserApiController extends Controller
{
    public function getRunners()
    {
        $runners = User::where('role', 2)->orWhere('role', 1)->get();

        return $runners;
    }

    public function assignRunners(Request $request)
    {
        $data                = $request->only('orderId', 'runner_1', 'runner_2');
        $order               = Order::find($data['orderId']);
        $notificationService = new NotificationService($order);

        if ($data['runner_1']) {
            $order->runner_1 = $data['runner_1'];
            $runner1         = User::find($data['runner_1']);

            if (env('APP_ENV') == 'production') {
                $notificationService->sendSMS($runner1->phone_number, $runner1->first_name . ', you\'ve received an order!');
            }
        } else {
            $order->runner_1 = null;
        }

        if ($data['runner_2']) {
            $order->runner_2 = $data['runner_2'];
            $runner2         = User::find($data['runner_2']);

            if (env('APP_ENV') == 'production') {
                $notificationService->sendSMS($runner2->phone_number, $runner2->first_name . ', you\'ve received an order!');
            }
        } else {
            $order->runner_2 = null;
        }

        try {
            $orderService = new OrderService($order);
            $orderService->updateStatus('processing');

            $task = Task::where('order_id', $order->id)->first();

            if (! $task) {
                $taskService = new TaskService();
                $taskService->create($order->runner_1, $order, 'order');
                $notificationService->orderDispatched($order);

                return 1;
            }
        } catch (\Exception $e) {
            report($e);

            return 0;
        }
    }

    public function runnersOnDuty()
    {
        $runner1_onDuty = Order::whereIn('status', [0, 1, 2, 7])
            ->pluck('runner_1')
            ->toArray();
        $runner2_onDuty = Order::whereIn('status', [0, 1, 2, 7])
            ->pluck('runner_2')
            ->toArray();

        $runners_onDuty = array_unique(array_filter(array_merge($runner1_onDuty, $runner2_onDuty)));

        $runners = User::findMany($runners_onDuty);

        foreach ($runners as &$runner) {
            $location       = $runner->runnerlocations()->orderBy('created_at', 'desc')->first();
            $runner->orders = Order::whereIn('status', [0, 1, 2, 7])->Where(function ($query) use ($runner) {
                $query->where('runner_1', $runner->id)
                    ->orWhere('runner_2', $runner->id);
            })->get();

            foreach ($runner->orders as &$order) {
                $order->address = json_decode($order->address);
            }

            if ($location) {
                $runner->location = (object) [
                    'lat' => $location->latitude,
                    'lng' => $location->longitude,
                ];
            } else {
                $runner->location = (object) [
                    'lat' => null,
                    'lng' => null,
                ];
            }
        }

        return $runners;
    }
}
