<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Address;
use App\Http\Controllers\Controller;
use App\Http\Resources\AddressesCollection;
use App\User;
use App\Utilities\Filters\AddressFilter;
use Auth;
use Illuminate\Http\Request;

class UserAddressController extends Controller
{
    public function index(AddressFilter $filter)
    {
        $addresses = Address::filter($filter)->get();

        return new AddressesCollection($addresses);
    }

    public function update($userId, Request $request, $addressId)
    {
        $address = Address::find($addressId);

        // change selected address
        if ($request->input('data.attributes.selected')) {
            // unselect other addresses
            $addresses = Address::where('user_id', $userId)->get();

            if ($addresses) {
                foreach ($addresses as $a) {
                    $a->selected = 0;
                    $a->update();
                }
            }

            $address->selected = $request->input('data.attributes.selected');
            $address->update();
        }

        // change selected address
        if ($request->input('data.attributes')) {
            if ($request->input('data.attributes.formattedAddress')) {
                $address->formatted_address = $request->input('data.attributes.formattedAddress');
            }
            if ($request->input('data.attributes.lat')) {
                $address->lat = (float) $request->input('data.attributes.lat');
            }
            if ($request->input('data.attributes.lng')) {
                $address->lng = (float) $request->input('data.attributes.lng');
            }
            if ($request->input('data.attributes.placeId')) {
                $address->place_id = $request->input('data.attributes.placeId');
            }
            if ($request->input('data.attributes.postalCode')) {
                $address->postal_code = $request->input('data.attributes.postalCode');
            }
            if ($request->input('data.attributes.unitNumber')) {
                $address->unit_number = $request->input('data.attributes.unitNumber');
            }
            if ($request->input('data.attributes.businessName')) {
                $address->business_name = $request->input('data.attributes.businessName');
            }
            if ($request->input('data.attributes.instructions')) {
                $address->instructions = $request->input('data.attributes.instructions');
            }
            $address->update();
        }
    }
}
