<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Dispatch\User as UserResource;
use App\Invitation;
use App\Services\NotificationService;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->all();

        if ($params && isset($params['search'])) {
            if (strpos($params['search'], ' ')) {
                $names = explode(' ', $params['search']);

                $customers = \App\User::where('first_name', 'LIKE', '%' . $names[0] . '%')
                ->where('last_name', 'LIKE', '%' . $names[1] . '%')
                ->get()
                ->toArray();
            } else {
                $customers = \App\User::where('first_name', 'LIKE', '%' . $params['search'] . '%')
                ->orWhere('last_name', 'LIKE', '%' . $params['search'] . '%')
                ->orWhere('email', 'LIKE', '%' . $params['search'] . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $params['search'] . '%')
                ->get()
                ->toArray();
            }

            return response()->json([
                'data' => $customers,
            ]);
        } else {
            $customers = User::orderBy('id', 'desc')->paginate(15);

            return response()->json($customers);
        }
    }

    public function show($user)
    {
        $user               = User::find($user);
        $user->rewardPoints = $user->rewardPointsBalance();
        $addressArr         = $user->addresses()->get()->toArray();
        $notes              = $user->notes()->get()->toArray();

        $user->notes = (object) [
            $notes,
        ];
        if (count($addressArr) > 0) {
            $user->address_objects = array_map(function ($ad) {
                $ad['address'] = json_decode($ad['formatted_address']);

                return $ad;
            }, $addressArr);
        } else {
            $user->address_objects = [];
        }

        // this is for invited by someone
        $invitation = Invitation::where('invited_user_id', $user->id)->first();

        $user->invited_by = null;
        if ($invitation) {
            $inviter          = User::find($invitation->user_id);
            $user->invited_by = (object) [
                'id'    => $inviter->id,
                'name'  => $inviter->first_name . ' ' . $inviter->last_name,
            ];
        }

        // this is for this guy invited users
        $invites = Invitation::where('user_id', $user->id)->pluck('invited_user_id');

        $user->invite_users = [];

        if (count($invites) > 0) {
            $invite_users = User::findMany($invites);

            foreach ($invite_users as $key => $invite_user) {
                $invite_user->hasPurcahse = $invite_user->orders()->count() ? true : false;
            }

            $user->invite_users = $invite_users;
        }

        unset($user->address);

        return new UserResource($user);
    }

    public function changePassword($user)
    {
        $user = User::find($user);

        $user->update([
            'password' => bcrypt('runner12345'),
        ]);
    }

    public function delete($userId)
    {
        $user = User::find($userId);

        if ($user->delete()) {
            return response()->json([
                'success' => 'user has been removed',
            ]);
        } else {
            return response()->json([
                'error' => 'You Shall Not Pass',
            ], 400);
        }
    }

    public function update(Request $request, User $user)
    {
        $data         = $request->input('user');
        $filteredData = [];

        foreach ($data as $key => $value) {
            if ($key === 'rewardPointsTransaction') {
                $user->rewardPointsTransaction($value, 6, [
                    'reason' => $data['rewardPointsTransactionReason'],
                ]);

                $notificationService = new NotificationService();
                $notificationService->sendManualNotification($data['rewardPointsTransaction'], $data['rewardPointsTransactionReason'], $user->id);
            } elseif ($key == 'password') {
                $data = [$key => bcrypt('runner12345')];
            }
        }
        $utilities     = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);
        $user->update($formattedData);
    }

    public function loginAsUser($userId)
    {
        $user        = User::find($userId);
        $accessToken = $user->createToken('MyApp')
            ->accessToken;

        Auth::guard()->login($user);

        return response()->json([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => $accessToken,
                    'user'        => [
                        'id'        => $user->id,
                        'firstName' => $user->first_name,
                        'lastName'  => $user->last_name,
                    ],
                ],
            ],
        ]);
    }
}
