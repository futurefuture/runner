<?php

App\Http\Controllers\Dispatch\V3;

use App\Http\Controllers\Controller;
use App\Service\PaymentService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function checkout(Request $request)
    {
        try {
            $order = (new PaymentService())->checkout($request->all());
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 422);
        }

        return $order;
    }
}
