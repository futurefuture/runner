<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dispatch\StoreCollection;
use App\Inventory;
use App\StoreActive;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function show($storeId)
    {
        return view('command.stores', compact(['storeId']));
    }

    public function index()
    {
        $inventories = Inventory::all();

        return new StoreCollection($inventories);
    }

    public function removeStore(Request $request, $id)
    {
        $store = StoreActive::where('store_id', $id)->first();

        $store->delete();
    }

    public function addStore(Request $request, $id)
    {
        return $id;
    }
}
