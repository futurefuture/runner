<?php

namespace App\Http\Controllers\Dispatch\v2;

use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\UserNote as UserNoteResource;
use App\Http\Resources\Runner\UserNoteCollection;
use App\User;
use App\UserNote;
use Illuminate\Http\Request;

class UserNoteController extends Controller
{
    public function index($userId)
    {
        $notes = UserNote::where('user_id', $userId)->get();

        return new UserNoteCollection($notes);
    }

    public function show($userId, $noteId)
    {
        $notes = UserNote::find($noteId);

        return $notes;
    }

    public function create(Request $request, $userId)
    {
        $data = $request->input('data.attributes');
        $user = User::find($userId);
        $utilities = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);
        $note = $user->notes()->create([
            'title' => $formattedData['title'] ?? null,
            'body'  => $formattedData['body'] ?? null,
        ]);

        return new UserNoteResource($note);
    }

    public function update(Request $request, $userId, $noteId)
    {
        $note = UserNote::find($noteId);
        $data = $request->input('data.attributes');
        $utilities = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);
        $note->update($formattedData);
        $updatedNote = UserNote::find($noteId);

        return new UserNoteResource($updatedNote);
    }

    public function delete($userId, $noteId)
    {
        $note = UserNote::find($noteId);
        $note->delete();
    }
}
