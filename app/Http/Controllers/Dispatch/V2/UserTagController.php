<?php

namespace App\Http\Controllers\Dispatch\v2;

use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\UserTag as UserTagResource;
use App\Http\Resources\Runner\UserTagCollection;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

class UserTagController extends Controller
{
    public function index($userId)
    {
        $user = User::find($userId)->tags()->paginate(10);

        return new UserTagCollection($user);
    }

    public function show($userId, $noteId)
    {
    }

    public function create(Request $request, $userId)
    {
        $data = $request->input('data.attributes');
        $user = User::find($userId);

        $tag = Tag::create([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'type' => $data['type'],
        ]);

        $user->tags()->save($tag);

        return new UserTagResource($tag);
    }

    public function update(Request $request, $userId, $tagId)
    {
        $data = $request->input('data.attributes');
        $tag = Tag::find($tagId);
        $utilities = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);
        $tag->update($formattedData);
        $tag = Tag::find($tagId);

        return new UserTagResource($tag);
    }

    public function delete($userId, $tagId)
    {
        $tag = Tag::find($tagId);
        $user = User::find($userId);
        // $user->tags()->detach();
        $tag->delete();
    }
}
