<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewOrdersCollection;
use App\Mail\OrderCancelled;
use App\Mail\OrderConfirmation;
use App\Mail\OrderDelivered;
use App\Mail\OrderProcessing;
use App\Mail\OrderReceived;
use App\Mail\PasswordSent;
use App\Order;
use App\Services\V3\OrderService;
use App\User;
use App\Utilities\Filters\OrderFilter;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class OrderStripeController extends Controller
{
    /**
     * Get all orders.
     *
     * @param string $query
     * @return array
     */
    public function show(Request $request, Order $order)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (strpos($order->charge, '[') !== false) {
            $arrCharge = json_decode($order->charge);
            $count = count($arrCharge);
            $index = count($arrCharge) - $count;
            $chargeId = $arrCharge[$index];
        } else {
            $chargeId = $order->charge;
        }
        $charge = \Stripe\Charge::retrieve($chargeId);

        return response()->json([
            'data' => [
                'type'       => 'order stripe charges',
                'id'         => $charge->id,
                'attributes' => [
                    'paymentMethod'         => $charge->source->brand,
                    'paymentLastFourDigits' => $charge->source->last4,
                    'postalCode'            => $charge->source->address_zip,
                    'riskLevel'             => $charge->outcome->risk_level,
                    'riskScore'             => $charge->outcome->risk_score,
                    'applePay'	             => ($charge->source->tokenization_method != null) ? true : false,
                ],
            ],
        ]);
    }
}
