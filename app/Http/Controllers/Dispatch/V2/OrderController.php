<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Actions\Order\CancelOrder;
use App\Task;
use App\Order;
use App\Address;
use App\Events\NewOrder;
use App\Classes\Utilities;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use App\Utilities\Filters\OrderFilter;
use App\Actions\UserAddress\CreateUserAddress;
use App\Http\Resources\Dispatch\OrdersCollection;
use App\Http\Resources\Dispatch\Order as OrderResource;

class OrderController extends Controller
{
    /**
     * Get all orders.
     */
    public function index(OrderFilter $filter): OrdersCollection
    {
        $orders = Order::filter($filter)->with([
            'user',
            'user.tags',
            'store',
            'gift'
        ])
        ->orderBy('schedule_time', 'asc')
        ->paginate(400);

        return new OrdersCollection($orders);
    }

    /**
     * Get a specific order
     */
    public function show(Order $order): OrderResource
    {
        return new OrderResource($order);
    }

    /**
     * Updates order
     */
    public function update(Request $request, Order $order)
    {
        $data          = $request->input('data.attributes');
        $utilities     = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);
        $order         = Order::find($order->id);

        if (isset($formattedData['phone_number']) || isset($formattedData['first_name']) || isset($formattedData['email']) || isset($formattedData['last_name'])) {
            $order->customer              = json_decode($order->customer);
            $order->customer->firstName   = isset($formattedData['first_name']) ? $formattedData['first_name'] : $order->customer->firstName;
            $order->customer->lastName    = isset($formattedData['last_name']) ? $formattedData['last_name'] : $order->customer->lastName;
            $order->customer->phoneNumber = isset($formattedData['phone_number']) ? $formattedData['phone_number'] : $order->customer->phoneNumber;
            $order->customer->email       = isset($formattedData['email']) ? $formattedData['email'] : $order->customer->email;
            $order->customer              = json_encode($order->customer);
            $order->save();
        }

        if (isset($formattedData['formattedAddress']) || isset($formattedData['lat']) || isset($formattedData['lng'])) {
            $action     = new CreateUserAddress();
            $newAddress = $action->execute($order->user, $data);
            $task       = Task::where('order_id', $order->id)->first();

            if ($task) {
                $task->address_id = $newAddress->id;
                $task->save();
            }

            $order->address          = json_decode($order->address);
            $order->address->address = $formattedData['formatted_address'];
            $order->address->lat     = $formattedData['lat'];
            $order->address->lng     = $formattedData['lng'];
            $order->address          = json_encode($order->address);
            $order->address_id       = $newAddress->id;
            $order->save();
        }

        if (isset($formattedData['unit'])) {
            $address              = Address::find($order->address_id);
            $address->unit_number = $formattedData['unit'];
            $address->save();
        }

        if (isset($formattedData['instructions'])) {
            $address               = Address::find($order->address_id);
            $address->instructions = $formattedData['instructions'];
            $address->save();
        }

        if (isset($formattedData['status']) && $formattedData['status'] == 5) {
            (new CancelOrder)->execute($order);

            $message = 'Need to refund $' . number_format($order->total / 100, 2, '.', ',') . '.';

            return response()->json([
                'message' => $message,
            ]);
        } else {
            $order->update($formattedData);

            event(new NewOrder($order));

            return new OrderResource($order);
        }
    }

    /**
     * Deletes order
     */
    public function delete(Order $order)
    {
        $order->delete();

        return response()->json([
            'success' => 'order deleted.',
        ]);
    }

    /**
     * Update order items.
     */
    public function updateItems(int $orderId, Request $request)
    {
        $order         = Order::find($orderId);
        $newOrderItems = $request->all();
        $orderUpdate   = (new OrderService($order))->updateItem($newOrderItems);

        event(new NewOrder($order));

        return $orderUpdate;
    }
}
