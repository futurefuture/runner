<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\RunnerLocation;
use App\Schedule;
use App\User;
use App\Utilities\Filters\ScheduleFilter;

class CourierLocationController extends Controller
{
    /**
     * Get runner locations.
     */
    public function index(ScheduleFilter $filter)
    {
        $schedules          = Schedule::filter($filter)->orderBy('start', 'desc')->get();
        $formattedLocations = [];

        foreach ($schedules as $schedule) {
            $formattedCourier = [];
            $courier          = RunnerLocation::where('runner_id', $schedule->user_id)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($courier) {
                $user             = User::find($courier->runner_id);
                $formattedCourier = [
                    'runner_id'  => $courier->runner_id,
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                    'latitude'   => $courier->latitude,
                    'longitude'  => $courier->longitude,
                    'created_at' => $courier->created_at,
                    'color'      => $schedule->color,
                ];

                array_push($formattedLocations, $formattedCourier);
            }
        }

        return response()->json([
            'data' => $formattedLocations,
        ]);
    }
}
