<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\Schedule;
use App\User;
use App\Utilities\Filters\ScheduleFilter;

class CourierController extends Controller
{
    /**
     * Get couriers.
     */
    public function index(ScheduleFilter $filter)
    {
        $couriers                    = [];
        $runner                      = Schedule::filter($filter)->get();
        $id                          = 13440;
        $couriers['scheduledRunner'] = $runner;
        $couriers['joey']            = User::find($id);

        return response()->json([
            $couriers,
        ]);
    }
}
