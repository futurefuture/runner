<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderInventoryController extends Controller
{
    /**
     * Gets inventories and quantities for given order.
     *
     * @param Order $order
     * @return Response
     */
    public function index(Order $order)
    {
        $orderService = new OrderService($order);
        $inventories = $orderService->getInventories();
        $formattedInventories = [];

        foreach ($inventories as $i) {
            $formattedInventory = [
                'type'       => 'order inventories',
                'id'         => $i->id,
                'attributes' => $i,
            ];

            array_push($formattedInventories, $formattedInventory);
        }

        return response()->json([
                    'data' => $formattedInventories,
                ]);
    }
}
