<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Task;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Http\Resources\Dispatch\V3\TaskCollection;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    protected $subDomain;

    protected $domain;

    private function getDomainDetails()
    {
        // set HTTP_HOST for tests as well
        if (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            $host = 'runner.test';
        }

        $this->subDomain = explode('.', $host)[0];
        $this->domain    = env('APP_DOMAIN');
    }

    public function __construct()
    {
        $this->getDomainDetails();
    }

    public function login()
    {
        $domain = $this->domain;

        return view('dispatch.v2.login', compact('domain'));
    }

    public function manage()
    {
        $domain = $this->domain;
        $now    = Carbon::now()->toDateString();
        $orders = Order::where('status', 0)->with([
            'user',
            'user.tags',
            'store',
            'gift'
        ])
            ->orderBy('schedule_time', 'asc')
            ->get();
        $scheduled_orders = Order::where('status', 7)->with([
            'user',
            'user.tags',
            'store',
            'gift'
        ])
            ->orderBy('schedule_time', 'asc')
            ->get();
        $tasks = Task::with([
            'address',
            'order',
            'order.user',
            'order.store',
            'order',
            'courier',
            'order.courier',
            'order.gift',
            'order.user.tags',
            'courier.schedules'
        ])
            ->where('state', '!=', 3)
            ->where('state', '!=', 5)
            ->whereDate('created_at', '>=', $now)
            ->get();

        foreach ($orders as $o) {
            $o['activity_log'] = json_decode($o->activity_log);
            $o['address'] = json_decode($o->address);
            $o['customer'] = json_decode($o->customer);
        }

        foreach ($scheduled_orders as $o) {
            $o['activity_log'] = json_decode($o->activity_log);
            $o['address'] = json_decode($o->address);
            $o['customer'] = json_decode($o->customer);

            $logo = $o->store->options['storeLogo'] ?? null;
            $o->setAttribute('store_logo', $logo);
        }

        foreach ($tasks as $t) {
            // fixing customer obj
            $formattedData = [];
            foreach (json_decode($t->order->customer, true) as $key => $value) {
                $formattedData[Str::snake($key)] = $value;
                unset(json_decode($t->order->customer, true)[$key]);
            }
            $t->order->customer = $formattedData;

            // fixing task->order obj
            $t->order['activity_log'] = json_decode($t->order->activity_log);
            $t->order['address'] = json_decode($t->order->address);
            $t->order['content'] = json_decode($t->order->content);
            
            $logo = $t->order->store->options['storeLogo'] ?? null;
            $t->setAttribute('store_logo', $logo);
        }

        return view('dispatch.v2.manage', compact([
            'domain',
            'orders',
            'scheduled_orders',
            'tasks'
        ]));
    }

    public function order(Order $order)
    {
        $orderId = $order->id;

        return view('dispatch.v2.order', compact([
            'orderId',
        ]));
    }

    public function schedule()
    {
        $domain = $this->domain;

        return view('dispatch.v2.schedule', compact(['domain']));
    }

    public function customers()
    {
        $domain = $this->domain;

        return view('dispatch.v2.customers', compact(['domain']));
    }

    public function customer($customerId)
    {
        $domain     = $this->domain;
        $customerId = $customerId;

        return view('dispatch.v2.customer', compact([
            'customerId',
            'domain',
        ]));
    }

    public function pastOrders()
    {
        $domain = $this->domain;

        return view('dispatch.v2.past-orders', compact(['domain']));
    }

    public function pastOrder($orderId)
    {
        $domain  = $this->domain;
        $orderId = $orderId;

        return view('dispatch.v2.past-order', compact([
            'orderId',
            'domain',
        ]));
    }
}
