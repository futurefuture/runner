<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;

class UserOrderController extends Controller
{
    public function index($user)
    {
        $orders = Order::where('user_id', $user)
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return response()->json($orders);
    }

    public function show(User $user, Order $order)
    {
        $orders = Order::where('user_id', $user->id)
            ->where('id', $order->id)
            ->get();

        return response()->json($orders);
    }
}
