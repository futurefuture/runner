<?php

namespace App\Http\Controllers\Dispatch\v2;

use App\Http\Controllers\Controller;
use App\Tag;
use App\Utilities\Filters\TagFilter;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(TagFilter $filter)
    {
        $tags = Tag::filter($filter)->get();

        return $tags;
    }
}
