<?php

namespace App\Http\Controllers\Dispatch\V2;

use App\Http\Controllers\Controller;
use App\RunnerPosition;
use App\Schedule;
use App\User;
use App\Utilities\Filters\ScheduleFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function index(ScheduleFilter $filter)
    {
        $query = Schedule::filter($filter)->orderBy('start', 'desc');
        $schedules = ! ($filter->has('month') || $filter->has('q')) ? $query->limit(100)->get()->toArray() : $query->get()->toArray();
        $schedules = array_map(function ($schedule) {
            $user = User::find($schedule['user_id']);
            $courierPosition = RunnerPosition::find($schedule['job_type']);
            if ($user && $courierPosition) {
                return (object) [
                    'id'              => $schedule['id'],
                    'title'           => $user->first_name.' '.$user->last_name.' / '.$courierPosition->title,
                    'start'           => $schedule['start'],
                    'end'             => $schedule['end'],
                    'backgroundColor' => $schedule['color'],
                    'allDay'          => false,
                ];
            }
        }, $schedules);
        // trim null values away.
        $cleanedSchedules = array_filter($schedules);

        return array_values($cleanedSchedules);
    }

    public function show(ScheduleFilter $filter, $scheduleId)
    {
        $schedule = Schedule::filter($filter)->find($scheduleId);
        $user = User::find($schedule['user_id']);
        $courierPosition = RunnerPosition::find($schedule['job_type']);

        $schedule = (object) [
            'id'                => $schedule['id'],
            'user'              => $user,
            'title'             => $user->first_name.' '.$user->last_name.' / '.$courierPosition->title,
            'start'             => $schedule['start'],
            'end'               => $schedule['end'],
            'backgroundColor'   => $schedule['color'],
            'allDay'            => false,
            'job'               => $courierPosition,
        ];

        return response()->json($schedule);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = User::find($data['employee']);
        $scheduleColor = Schedule::orderBy('id', 'desc')->limit(1)->first()->color;
        $colorKey = array_search($scheduleColor, Schedule::color);

        if ($user) {
            $schedule = new Schedule;
            $schedule->job_type = $data['job_type'];
            $schedule->start = Carbon::parse($data['start']);
            $schedule->end = Carbon::parse($data['end']);
            $schedule->color = ! ($colorKey == 15) ? Schedule::color[$colorKey + 1] : Schedule::color[1];
            $user->schedules()->save($schedule);
        } else {
            return response()->json([
                'error' => 'user not found',
            ], 404);
        }
    }

    public function delete($id)
    {
        $schedule = Schedule::find($id);
        $schedule->delete();
    }

    public function update($scheduleId, Request $request)
    {
        $data = $request->all();
        $schedule = Schedule::find($scheduleId);
        $schedule->user_id = $data['user']['id'];
        $schedule->job_type = $data['job']['id'];
        $schedule->start = Carbon::parse($data['start']);
        $schedule->end = Carbon::parse($data['end']);
        $schedule->save();
    }

    public function getEmployees()
    {
        $employees = User::whereIn('role', [1, 2, 5])->get();

        return $employees;
    }

    public function getCourierPositions()
    {
        $courierPositions = RunnerPosition::all();

        return $courierPositions;
    }
}
