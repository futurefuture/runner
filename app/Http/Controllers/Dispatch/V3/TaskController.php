<?php

namespace App\Http\Controllers\Dispatch\V3;

use App\Container;
use App\Events\TaskStateUpdated;
use App\Http\Controllers\Controller;
use App\Http\Resources\Dispatch\V3\Task as TaskResource;
use App\Http\Resources\Dispatch\V3\TaskCollection;
use App\Order;
use App\Services\NotificationService;
use App\Services\TaskService;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Gets all tasks for given courier.
     */
    public function index(): TaskCollection
    {
        $now   = Carbon::now()->toDateString();
        $tasks = Task::with([
            'address',
            'order',
            'order.user',
            'order.store',
            'order',
            'courier',
            'order.courier',
            'order.gift',
            'order.user.tags',
            'courier.schedules'
        ])
            ->where('state', '!=', 3)
            ->where('state', '!=', 5)
            ->whereDate('created_at', '>=', $now)
            ->get();

        return new TaskCollection($tasks);
    }

    /**
     * Gets single task.
     */
    public function show(Task $task): TaskResource
    {
        return new TaskResource($task);
    }

    /**
     * Creates new task.
     */
    public function create(Request $request): TaskResource
    {
        $data = $request->input('data.attributes');

        if (!isset($data['containerId'])) {
            $container = new Container;
            $container->save();
            $containerId = $container->id;
        } else {
            $containerId = $data['containerId'];
        }

        // check if order
        if (isset($data['orderId'])) {
            $task = Task::where('order_id', '=', $data['orderId'])
                ->whereIn('state', [1, 2])
                ->first();

            if ($task) {
                $task->delete();
            }

            $runner1         = User::find($data['courierId']);
            $order           = Order::find($data['orderId']);
            $order->status   = ORDER::IS_IN_PROCESSING;
            $order->runner_1 = $data['courierId'];

            if ($order->runner_1 != null) {
                $order->save();
            }
        }

        $task    = new Task;
        $courier = User::find($data['courierId']);
        $color   = $courier->schedules()->latest('created_at')->first()->color ?? '#ff00ff';

        $task->notes        = isset($data['notes']) ? $data['notes'] : null;
        $task->phone_number = isset($data['phoneNumber']) ? $data['phoneNumber'] : null;
        $task->courier_id   = $data['courierId'];
        $task->state        = Task::IS_ASSIGNED;
        $task->color        = $color;
        $task->container_id = $containerId;
        $task->name         = isset($data['name']) ? $data['name'] : null;
        $task->organization = isset($data['organization']) ? $data['organization'] : null;
        $task->address_id   = isset($data['orderId']) ? $order->address_id : $data['addressId'];
        $task->order_id     = isset($data['orderId']) ? $data['orderId'] : null;
        $task->type         = isset($data['type']) ? $data['type'] : null;

        $task->save();

        if (!isset($data['containerId'])) {
            $container->courier_id = $data['courierId'];

            $container->save();
        }

        if (isset($data['containerId'])) {
            $taskAtIndex = Task::where('index', '=', $data['index'])
                ->where('container_id', '=', $data['containerId'])
                ->whereIn('state', [1, 2])
                ->first();

            if (!$taskAtIndex) {
                $task->index = $data['index'];

                $task->save();
            } else {
                $task->index          = $data['index'];
                $getAllTaskAfterIndex = Task::where('index', '>=', $data['index'])
                    ->where('container_id', '=', $data['containerId'])
                    ->whereIn('state', [1, 2])
                    ->get();

                foreach ($getAllTaskAfterIndex as $taskIndex) {
                    $taskIndex->index = $taskIndex->index + 1;

                    $taskIndex->save();
                }

                $task->save();
            }
        }

        event(new TaskStateUpdated($task));

        try {
            $notificationService = resolve(NotificationService::class);

            $notificationService->sendCourierPushNotification($task, $runner1);
            $notificationService->orderDispatched($order);

            return new TaskResource($task);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Updates task.
     */
    public function update(Request $request, Task $task): TaskResource
    {
        $data        = $request->input('data.attributes');
        $updatedTask = (new TaskService())->update($task, $data);

        event(new TaskStateUpdated($updatedTask));

        return new TaskResource($updatedTask);
    }

    /**
     * Deletes task.
     */
    public function delete(Task $task): void
    {
        $task->delete();
    }
}
