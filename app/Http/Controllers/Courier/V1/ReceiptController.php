<?php

namespace App\Http\Controllers\Courier\V1;

use App\Http\Controllers\Controller;
use App\Mail\OrderReceived;
use App\Order;
use Illuminate\Support\Facades\Mail;

class ReceiptController extends Controller
{
    public function sendReceipt($order)
    {
        if ($order = Order::find($order)) {
            $order->content = json_decode($order->content);
            if (env('APP_ENV') === 'production') {
                Mail::to($order->content->customer->email)->send(new OrderReceived($order));
            }

            return response()->json([
                'status' => 'success',
            ]);
        } else {
            return response()->json([
                'error' => 'Order not found.',
            ], 404);
        }
    }
}
