<?php

namespace App\Http\Controllers\Courier\V1;

use DB;
use Auth;
use App\Gift;
use App\User;
use App\Order;
use App\Store;
use Validator;
use App\Product;
use App\OrderItem;
use Carbon\Carbon;
use App\RunnerLocation;
use App\RunnerPosition;
use App\Mail\OrderReceived;
use App\Services\S3Service;
use Illuminate\Http\Request;
use App\Services\TaskService;
use App\Services\StripeService;
use App\Http\Controllers\Controller;
use App\Mail\OrderReceivedForRunner;
use Illuminate\Support\Facades\Mail;
use App\Services\NotificationService;

class CourierController extends Controller
{
	private function createOrderItems($orderId)
	{
		$order = Order::find($orderId);

		$items = json_decode($order->content);

		foreach ($items as $k => $item) {
			$product = Product::find($item->id);

			if (property_exists($item, 'type') && $item->type === 'bundle') {
				continue;
			}

			if (property_exists($item, 'price')) {
				$retail_price = ($item->price / 1.1) * 100;
			} elseif (property_exists($item, 'real_price')) {
				$retail_price = $item->real_price * 100;
			} elseif (property_exists($item, 'true_price')) {
				$retail_price = $item->true_price * 100;
			} elseif (property_exists($item, 'retailPrice')) {
				$retail_price = $item->retailPrice;
			} else {
				$retail_price = $product->retail_price;
			}

			if (property_exists($item, 'price')) {
				$runner_price = $item->price * 100;
			} elseif (property_exists($item, 'runner_price')) {
				$runner_price = $item->runner_price * 100;
			} elseif (property_exists($item, 'runnerPrice')) {
				$runner_price = $item->runnerPrice;
			} else {
				$runner_price = null;
			}

			if (property_exists($item, 'name')) {
				$title = $item->name;
			} elseif (property_exists($item, 'title')) {
				$title = $item->title;
			} else {
				$title = null;
			}

			if (property_exists($item, 'qty')) {
				$quantity = $item->qty;
			} elseif (property_exists($item, 'quantity')) {
				$quantity = $item->quantity;
			} else {
				$quantity = null;
			}

			// skip printing to order items table if quantity is 0
			if ((int) $quantity == 0) {
				continue;
			}

			if (property_exists($item, 'package')) {
				$packaging = $item->package;
			} elseif (property_exists($item, 'packaging')) {
				$packaging = $item->packaging;
			} else {
				$packaging = null;
			}

			if (!$product) {
				continue;
			} else {
				$orderItem = new OrderItem([
					'order_id' => $order->id,
					'user_id' => $order->user_id,
					'product_id' => $product->id,
					'title' => $title,
					'quantity' => (int) $quantity,
					'retail_price' => $retail_price,
					'runner_price' => $runner_price,
					'packaging' => $packaging,
					'producer' => $product->producer,
					'category_id' => $product->category_id,
					'created_at' => $order->created_at,
					'updated_at' => $order->created_at,
				]);

				$orderItem->save();
			}
		}
	}

	public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			$errors = json_decode($validator->errors()->toJson());

			foreach ($errors as $key => $value) {
				$temp = $value[0];

				break;
			}

			$errors->error = $temp;

			return response()->json([
				'errors' => $errors,
			], 401);
		}

		if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
			$user = Auth::user();

			if (url()->previous() == 'https://runner.dev/runner' || url()->previous() == 'https://www.getrunner.io/runner') {
				Auth::guard()->login($user);

				return redirect('/runner/orders');
			}

			$success['token'] = $user->createToken('MyApp')
				->accessToken;

			return response()->json([
				'data' => $success,
			]);
		} else {
			if (url()->previous() == 'https://runner.dev/runner' || url()->previous() == 'https://www.getrunner.io/runner') {
				return redirect('/runner');
			}

			return response()->json([
				'errors' => [
					'error' => 'Unauthorised',
				],
			], 401);
		}
	}

	public function getSchedules()
	{
		$user = Auth::user();

		$schedules = array_map(function ($schedule) use ($user) {
			$runnerPosition = RunnerPosition::find($schedule['job_type']);

			return (object) [
				'id' => $schedule['id'],
				'title' => $user->first_name . ' ' . $user->last_name . ' / ' . $runnerPosition->title,
				'start' => $schedule['start'],
				'end' => $schedule['end'],
				'backgroundColor' => $runnerPosition->backgroundColor,
				'allDay' => false,
			];
		}, $user->schedules->toArray());

		return response()->json([
			'data' => $schedules,
		]);
	}

	public function currentOrders()
	{
		$user = Auth::user();

		$orders = Order::whereIn('status', [0, 1, 2, 4, 7])
			->where(function ($query) use ($user) {
				$query
					->where('runner_1', $user->id)
					->orWhere('runner_2', $user->id);
			})
			->get();

		$simplified_orders = [];

		foreach ($orders as $order) {
			$order->customer = json_decode($order->customer);
			$firstName = isset($order->customer->first_name) ? $order->customer->first_name : $order->customer->firstName;
			$lastName = isset($order->customer->last_name) ? $order->customer->last_name : $order->customer->lastName;
			$fullName = $firstName . ' ' . $lastName;

			if ($order->user_id) {
				array_push($simplified_orders, (object) [
					'id' => (string) $order->id,
					'status' => $order->status,
					'customer' => $fullName,
					'gift' => Gift::where('order_id', $order['id'])->first() ? true : false,
				]);
			}
		}

		return response()->json([
			'data' => $simplified_orders,
		]);
	}

	public function pastOrders()
	{
		$user = Auth::user();
		$orders = Order::whereIn('status', [3, 5, 6])
			->where(function ($query) use ($user) {
				$query
					->where('runner_1', $user->id)
					->orWhere('runner_2', $user->id);
			})
			->Orderby('delivered_at', 'DESC')
			->limit(10)
			->get();

		$simplified_orders = [];

		foreach ($orders as $order) {
			$order->customer = json_decode($order->customer);

			if ($order->user_id) {
				array_push($simplified_orders, (object) [
					'id' => (string) $order->id,
					'status' => $order->status,
					'customer' => $order->customer->firstName . ' ' . $order->customer->lastName,
					'gift' => Gift::where('order_id', $order['id'])->first() ? true : false,
				]);
			}
		}

		return response()->json([
			'data' => $simplified_orders,
		]);
	}

	public function currentOrder($id)
	{
		$order = Order::find($id);
		$user = Auth::user();

		if (($order->runner_1 == $user->id || $order->runner_2 == $user->id)
			&& ($order->status == 0 || $order->status == 1 || $order->status == 2 || $order->status == 4 || $order->status == 7)
		) {
			$order->content = json_decode($order->content);
			$order->address = json_decode($order->address);
			$order->customer = json_decode($order->customer);
			$order->customer->first_name = $order->customer->first_name ?? $order->customer->firstName;
			$order->customer->last_name = $order->customer->last_name ?? $order->customer->lastName;
			$order->customer->phone_number = $order->customer->phone_number ?? $order->customer->phoneNumber;

			$formattedOrderContent = [];

			foreach ($order->content as $item) {
				$product = Product::find($item->id);
				$category = \App\Category::find($product->category_id);
				$formattedItem['id'] = (int) $product->sku;
				$formattedItem['type'] = 'product';
				$formattedItem['name'] = $product->title;
				$formattedItem['allowSub'] = true;
				$formattedItem['rowId'] = '12312412412';
				$formattedItem['runner_price'] = (float) $product->runner_price / 100;
				$formattedItem['true_price'] = (float) number_format(($product->runner_price / 1.12) / 100, 2);
				$formattedItem['markup'] = 2.00;
				$formattedItem['package'] = $product->packaging;
				$formattedItem['package'] = $product->packaging;
				$formattedItem['qty'] = isset($item->quantity) ? (int) $item->quantity : (int) $item->qty;
				$formattedItem['secondary_category'] = $category->title;
				$formattedItem['primary_category'] = \App\Category::find($category->parent_id)->title ?? 'Wine';
				$formattedItem['slug'] = $product->slug;
				$formattedItem['thumbnail'] = $product->image;
				$formattedItem['image'] = $product->image;
				$formattedItem['brand'] = $product->producer;
				$formattedItem['origin'] = $product->producing_country;

				array_push($formattedOrderContent, $formattedItem);
			}

			$gift = Gift::where('order_id', $order->id)->first();

			$order->store = Store::where('id', 10)->first();

			if ($gift) {
				$gift->address = json_decode($gift->address);
				$order->gift = $gift;
			}

			return response()->json([
				'data' => [
					'id' => (string) $order->id,
					'user_id' => $order->user_id,
					'status' => $order->status,
					'content' => $formattedOrderContent,
					'card_last_four' => $order->card_last_four,
					'activity_log' => json_decode($order->activity_log),
					'admin_notes' => $order->admin_notes,
					'charge' => $order->charge,
					'runner_1' => $order->runner_1,
					'runner_2' => $order->runner_2,
					'device' => $order->device,
					'customer' => $order->customer,
					'address' => $order->address,
					'unit_number' => $order->unit_number,
					'coupon' => $order->coupon_id,
					'instructions' => $order->instructions,
					'subtotal' => (float) $order->subtotal / 100,
					'cogs' => (float) $order->cogs / 100,
					'discount' => (float) $order->discount / 100,
					'delivery' => (float) $order->delivery / 100,
					'delivery_tax' => (float) $order->delivery_tax / 100,
					'markup' => (float) $order->markup / 100,
					'markup_tax' => (float) $order->markup_tax / 100,
					'display_tax_total' => (float) $order->display_tax_total / 100,
					'tip' => (float) $order->tip / 100,
					'total' => (float) $order->total / 100,
					'stripe_fee' => (float) $order->stripe_fee / 100,
					'signature' => $order->signature,
					'delivered_at' => $order->delivered_at,
					'schedule_time' => $order->schedule_time,
					'additional_info' => $order->additional_info,
					'first_time' => $order->first_time,
					'gift' => $order->gift,
					'store' => $order->store,
				],
			]);
		} else {
			return response()->json([
				'errors' => [
					'error' => 'The order cannot be found',
				],
			], 404);
		}
	}

	public function pastOrder($id)
	{
		$user = Auth::user();
		$order = Order::find($id);

		if (($order->runner_1 == $user->id || $order->runner_2 == $user->id) && ($order->status == 3 || $order->status == 5 || $order->status == 6)
		) {
			$order->content = json_decode($order->content);

			$orderContentCount = count($order->content);

			$products_id = [];

			$stores_id = [];

			if ($orderContentCount == 1) {
				$order->content->qty = (int) $order->content->qty ?? (int) $order->content->quantity;

				if (Product::find($order->content->id)->stock_type == 'VINTAGES') {
					$order->content->isVintages = 1;
				} else {
					$order->content->isVintages = 0;
				}

				array_push($products_id, $order->content->id);
			} else {
				foreach ($order->content as $item) {
					$item->qty = (int) $item->qty ?? (int) $item->quantity;

					if (Product::find($item->id)->stock_type == 'VINTAGES') {
						$item->isVintages = 1;
					} else {
						$item->isVintages = 0;
					}

					array_push($products_id, $item->id);
				}
			}

			$stores_id = DB::table('inventories')
				->select('store_id')
				->where('quantity', '>', 0)
				->whereIn('product_id', $products_id)
				->groupBy('store_id')
				->havingRaw('COUNT(`store_id`) >= ' . count($products_id))
				->get()->toArray();

			for ($i = 0; $i < count($stores_id); ++$i) {
				$stores_id[$i] = $stores_id[$i]->store_id;
			}

			$stores = Store::findMany($stores_id);

			//find closest store have items instock
			$gift = Gift::where('order_id', $order->id)->first();
			if (count($stores) > 0) {
				if ($gift) {
					$order->store = $this->find_closest_marker($gift->address->lat, $gift->address->lng, $stores);
				} else {
					$order->store = $this->find_closest_marker($order->address->lat, $order->address->lng, $stores);
				}
			} else {
				$order->store = Store::where('store_id', 10)->first();
			}

			if ($gift) {
				$gift->address = json_decode($gift->address);
				$order->gift = $gift;
			}

			// dd($order->customer);

			return response()->json([
				'data' => [
					'id' => (string) $order->id,
					'user_id' => $order->user_id,
					'status' => $order->status,
					'content' => $order->content,
					'card_last_four' => $order->card_last_four,
					'activity_log' => json_decode($order->activity_log),
					'admin_notes' => $order->admin_notes,
					'charge' => $order->charge,
					'runner_1' => $order->runner_1,
					'runner_2' => $order->runner_2,
					'device' => $order->device,
					'customer' => json_decode($order->customer),
					'address' => json_decode($order->address),
					'unit_number' => $order->unit_number,
					'coupon' => $order->coupon_id,
					'instructions' => $order->instructions,
					'subtotal' => (float) $order->subtotal / 100,
					'cogs' => (float) $order->cogs / 100,
					'discount' => (float) $order->discount / 100,
					'delivery' => (float) $order->delivery / 100,
					'delivery_tax' => (float) $order->delivery_tax / 100,
					'markup' => (float) $order->markup / 100,
					'markup_tax' => (float) $order->markup_tax / 100,
					'display_tax_total' => (float) $order->display_tax_total / 100,
					'tip' => (float) $order->tip / 100,
					'total' => (float) $order->total / 100,
					'stripe_fee' => (float) $order->stripe_fee / 100,
					'signature' => $order->signature,
					'delivered_at' => $order->delivered_at,
					'schedule_time' => $order->schedule_time,
					'additional_info' => $order->additional_info,
					'first_time' => $order->first_time,
					'gift' => $order->gift,
					'store' => $order->store,
				],
			]);
		} else {
			return response()->json([
				'errors' => [
					'error' => 'The order cannot be found',
				],
			], 404);
		}
	}

	public function updateOrder(Request $request, $id)
	{
		$user = Auth::user();
		$order = Order::find($id);

		if (($order->runner_1 == $user->id || $order->runner_2 == $user->id)) {
			if (!$request->filled('status')) {
				return response()->json([
					'errors' => [
						'error' => 'Could not find the status field',
					],
				], 403);
			}

			$activity_log = json_decode($order->activity_log);

			if ($user->role == 2 || $user->role == 1 || $user->role == 5) {
				if ($request->input('status') == 1 && ($order->status == 0 || $order->status == 7)) {
					array_push($activity_log, (object) [
						'log' => 'processing',
						'time' => Carbon::now('America/Toronto'),
					]);
				} elseif ($request->input('status') == 2 && $order->status == 1) {
					array_push($activity_log, (object) [
						'log' => 'onroute',
						'time' => Carbon::now('America/Toronto'),
					]);
				} elseif ($request->input('status') == 3 && $order->status == 2) {
					if (!$request->filled('signature')) {
						return response()->json([
							'errors' => [
								'error' => 'signature field not found',
							],
						], 403);
					}
					// Ayushi Code to add signature to S3 bucket
					if (env('APP_ENV') === 'production') {
						$url = (new S3Service())->addSignature($request->input('signature'), $order);
						$order->signature = $url;
					}

					/* if (env('APP_ENV') === 'production') {
                        Storage::disk('order_signatures')->put($order->id . '.png', base64_decode(str_replace('data:image/png;base64,', '', $order->signature)));
                    }*/

					array_push($activity_log, (object) [
						'log' => 'delivered',
						'time' => Carbon::now('America/Toronto'),
					]);

					$order->delivered_at = Carbon::now('America/Toronto');
					DB::table('promotions_used')
						->where('order_id', $order->id)
						->update(['used' => 1]);
				} else {
					return response()->json([
						'errors' => [
							'error' => 'Please process in order',
						],
					], 403);
				}
			}

			$order->status = $request->input('status');

			$order->activity_log = json_encode($activity_log);

			if ($order->save()) {
				if ($order->status == 2) {
					if (env('APP_ENV') == 'production') {
						$notificationService = new NotificationService();
						$notificationService->orderOnRoute($order);
					}

					$taskService = (new TaskService())->updateState($order->status, $order, $order->signature);
				} elseif ($order->status == 3) {
					$order->content = json_decode($order->content);
					$order->customer = json_decode($order->customer);
					$runner = User::find($order->runner_1);

					if (env('APP_ENV') === 'production') {
						Mail::to($order->customer->email)->bcc('service@getrunner.io')
							->queue(new OrderReceived($order));
						Mail::to($runner->email)
							->queue(new OrderReceivedForRunner($order));
					}

					try {
						$stripeService = resolve(StripeService::class);
						$charge = $stripeService->captureCharge($order);
					} catch (\Stripe\Error\Card $e) {
						$body = $e->getJsonBody();
						$err = $body['error'];

						return response()->json([
							'errors' => [
								'error' => $err['message'],
							],
						], 400);
					} catch (\Stripe\Error\ApiConnection $e) {
						$body = $e->getJsonBody();
						$err = $body['error'];

						return response()->json([
							'errors' => [
								'error' => $err['message'],
							],
						], 404);
					} finally {
						if (env('APP_ENV') === 'production') {
							$notificationService = new NotificationService();
							$notificationService->orderDelivered($order);
						}

						$taskService = (new TaskService())->updateState($order->status, $order, $order->signature);
						$this->createOrderItems($order->id);
					}
				}

				return response()->json([
					'message' => 'success',
				]);
			} else {
				return response()->json([
					'errors' => [
						'error' => 'Could not update the order',
					],
				], 403);
			}
		}
	}

	public function updateLocation(Request $request)
	{
		$user = Auth::user();

		$data = $request->only('latitude', 'longitude');
		$location = new RunnerLocation();
		$location->latitude = $data['latitude'];
		$location->longitude = $data['longitude'];

		if ($user->runnerlocations()->save($location)) {
			return response()->json([
				'message' => 'success',
			]);
		}

		return response()->json([
			'errors' => [
				'error' => 'Could not update the location',
			],
		], 403);
	}

	public function getOrderItems()
	{
		$user = Auth::user();

		$orders = Order::whereIn('status', [
			0,
			1,
			2,
			4,
			7,
		])
			->where(function ($query) use ($user) {
				$query
					->where('runner_1', $user->id)
					->orWhere('runner_2', $user->id);
			})->get();

		$items = [];

		foreach ($orders as $order) {
			$order->content = json_decode($order->content);
			$items = array_merge($items, $order->content);
		}

		$filteredItems = [];

		foreach ($items as $key => $item) {
			if (array_key_exists($item->id, $filteredItems)) {
				$filteredItems[$item->id]->qty += (int) $item->quantity;
			} else {
				$filteredItems[$item->id] = $item;
			}
		}

		$itemArr = [
			'beer' => [],
			'wine' => [],
			'spirits' => [],
			'coolers' => [],
			'bundles' => [],
		];

		foreach ($filteredItems as $key => $filteredItem) {
			$product = Product::find($filteredItem->id);
			$category = \App\Category::find($product->category_id);
			$primaryCategory = \App\Category::find($category->parent_id);
			$newItem['qty'] = isset($filteredItem->quantity) ? (int) $filteredItem->quantity : (int) $filteredItem->qty;
			$newItem['id'] = (int) $product->sku;
			$newItem['name'] = $product->title;
			$newItem['true_price'] = (float) number_format(($product->runner_price / 1.12) / 100, 2);
			$newItem['runner_price'] = (float) $product->runner_price / 100;
			$newItem['thumbnail'] = $product->image;
			$newItem['image'] = $product->image;
			$newItem['brand'] = $product->producer;
			$newItem['origin'] = $product->producing_country;
			$newItem['package'] = $product->packaging;
			$newItem['primary_category'] = $primaryCategory->title ?? 'Wine';
			$newItem['secondary_category'] = $category->title;
			$newItem['rowId'] = '2312423423';
			$newItem['type'] = 'product';
			$newItem['isVintages'] = 0;

			if ($primaryCategory && array_key_exists(mb_strtolower($primaryCategory->title), $itemArr)) {
				array_push($itemArr[mb_strtolower($primaryCategory->title)], $newItem);
			} else {
				array_push($itemArr['beer'], $newItem);
			}
		}

		if (count($itemArr['wine']) != 0) {
			$wineArr = [
				'Vintages' => [],
				'Regular' => [],
			];

			foreach ($itemArr['wine'] as $key => $wine) {
				$wine['qty'] = $wine['qty'];
				$wineArr['Regular'][] = $wine;
			}

			$regularWineArr = [];

			foreach ($wineArr['Regular'] as $key => $regularWine) {
				if ($product = Product::where('sku', $regularWine['id'])) {
					$regularWine['isVintages'] = 0;
					$regularWineArr[$regularWine['origin']][] = $regularWine;
				}
			}

			$wineArr = array_merge($wineArr, $regularWineArr);

			unset($wineArr['Regular']);

			$itemArr['wine'] = $wineArr;
		} else {
			$itemArr['wine'] = [
				'Vintages' => [],
			];
		}

		foreach ($itemArr as $key => $item) {
			$itemArr[ucfirst($key)] = $itemArr[$key];
			unset($itemArr[$key]);
		}

		return response()->json([
			'data' => $itemArr,
		]);
	}

	private function find_closest_marker($lat1, $lon1, $stores)
	{
		$pi = pi();
		$R = 6371; //equatorial radius
		$distances = [];
		$closest = -1;

		for ($i = 0; $i < count($stores); ++$i) {
			$lat2 = $stores[$i]->latitude;
			$lon2 = $stores[$i]->longitude;

			$chLat = $lat2 - $lat1;
			$chLon = $lon2 - $lon1;

			$dLat = $chLat * ($pi / 180);
			$dLon = $chLon * ($pi / 180);

			$rLat1 = $lat1 * ($pi / 180);
			$rLat2 = $lat2 * ($pi / 180);

			$a = sin($dLat / 2) * sin($dLat / 2) +
				sin($dLon / 2) * sin($dLon / 2) * cos($rLat1) * cos($rLat2);
			$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
			$d = $R * $c;

			$distances[$i] = $d;

			if ($closest == -1 || $d < $distances[$closest]) {
				$closest = $i;
			}
		}

		// (debug) The closest marker is:
		return $stores[$closest];
	}
}
