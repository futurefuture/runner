<?php

namespace App\Http\Controllers\Courier\V1;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class OrderController extends Controller
{
    /**
     * get current user.
     * @return object user
     */
    private function getUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return $user;
    }

    public function getLastOrder()
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found',
            ], 403);
        }

        $lastOrder = $user->orders()->orderby('created_at', 'DESC')->first();

        if ($lastOrder === null) {
            return response()->json([
                'error' => 'User does not have last order.',
            ], 404);
        }

        $lastOrder->content = json_decode($lastOrder->content);

        return $lastOrder->content->items;
    }

    public function getOrders()
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ], 403);
        }

        $orders = $user->orders()->orderBy('id', 'desc')->get()->toArray();

        if ($user->orders()->orderBy('id', 'desc')->count() == 0) {
            return response()->json([
                'error' => 'no order found',
            ], 404);
        }

        $orders = array_map(function ($order) {
            $order['content'] = json_decode($order['content']);

            if ($order['runner_1'] && $order['runner_2']) {
                $runner_1 = User::find($order['runner_1']);
                $runner_2 = User::find($order['runner_2']);

                if ($runner_1 && $runner_2) {
                    $runner_assigned = $runner_1->first_name.' '.$runner_1->last_name.' & '.$runner_2->first_name.' '.$runner_2->last_name;
                } else {
                    $runner_assigned = 'Unknown Runner';
                }
            } elseif ($order['runner_1'] && ! $order['runner_2']) {
                $runner_1 = User::find($order['runner_1']);

                if ($runner_1) {
                    $runner_assigned = $runner_1->first_name.' '.$runner_1->last_name;
                } else {
                    $runner_assigned = 'Unknown Runner';
                }
            } elseif (! $order['runner_1'] && $order['runner_2']) {
                $runner_2 = User::find($order['runner_2']);

                if ($runner_2) {
                    $runner_assigned = $runner_2->first_name.' '.$runner_2->last_name;
                } else {
                    $runner_assigned = 'Unknown Runner';
                }
            } else {
                $runner_assigned = 'No Runner Assigned';
            }

            $items = array_map(function ($item) {
                return (object) [
                    'id'           => $item->id,
                    'name'         => $item->name,
                    'qty'          => $item->qty,
                    'true_price'   => $item->true_price,
                    'runner_price' => $item->runner_price,
                    'package'      => $item->package,
                ];
            }, $order['content']);

            return  (object) [
                    'id'                    => $order['id'],
                    'status'                => $order['status'],
                    'created_at'            => $order['created_at'],
                    'unitno'                => $order['unit_number'],
                    'address'               => json_decode($order['address'])->address,
                    'instructions'          => $order['instructions'],
                    'items'                 => $items,
                    'signature'             => $order['signature'],
                    'runner_assigned'       => $runner_assigned,
                    'subtotal'              => $order['subtotal'] / 100,
                    'tax'                   => $order['display_tax_total'] / 100,
                    'delivery'              => $order['delivery'] / 100,
                    'discount'              => $order['discount'] / 100,
                    'estimated_delivery_at' => Carbon::parse($order['created_at'])->addMinutes(3600)->format('Y-m-d H:i:s'),
                    'tip'                   => $order['tip'] / 100,
                    'total'                 => $order['total'] / 100,
                ];
        }, $orders);

        return $orders;
    }
}
