<?php

namespace App\Http\Controllers\Courier\V1;

use App\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    private function getUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return JWTAuth::parseToken()->authenticate();
    }

    public function getUserInfo()
    {
        $user = $this->getUser();

        return $user;
    }

    public function favourite(Request $request)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        $data = $request->all();

        $validator = Validator::make($data, [
            'id'   => 'required|max:255',
            'type' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        if ($data['type'] == 'product') {
            $user->favourite_products()->toggle($data['id']);
        } elseif ($data['type'] == 'custom_product') {
            $user->favourite_custom_products()->toggle($data['id']);
        } elseif ($data['type'] == 'bundle') {
            $user->favourite_bunldes()->toggle($data['id']);
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function getFavourites(Request $request, $id = null)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        if (! $id) {
            return $user->favourites;
        }

        if ($user->favourite_products()->where('product_id', $id)->count()) {
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function getAddresses()
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        $addresses = [];

        foreach ($user->addresses as $key => $address) {
            $address->address = json_decode($address->address);

            array_push($addresses, json_decode($address));
        }

        return $addresses;
    }

    public function changeDefaultAddress(Request $request)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        if (! $request->filled('id')) {
            return response()->json([
                'error' => 'id for the address not found',
            ]);
        }

        $addresses = $user->addresses;
        if (count($addresses) == 0) {
            return response()->json([
                'error' => 'address not found',
            ]);
        }

        foreach ($addresses as $address) {
            if ($address->id == $request->input('id')) {
                $address->update([
                    'selected'  => 1,
                ]);
            } else {
                $address->update([
                    'selected'  => 0,
                ]);
            }
        }
        $address = $user->addresses()->where('selected', true)->first();
        $address->address = json_decode($address->address);

        return response()->json([
            'status'  => 'success',
            'address' => $address,
        ]);
    }

    public function removeAddress(Request $request)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        if (! $request->filled('id')) {
            return response()->json([
                'error' => 'Address id not found.',
            ]);
        }

        $address = Address::find($request->input('id'));

        if (! $address) {
            return response()->json([
                'error' => 'address not found',
            ], 404);
        }

        $address->delete();

        if ($user->addresses()->where('selected', true)->count() === 0) {
            $user->addresses()->first()->update([
                'selected'  => true,
            ]);
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function createAddress(Request $request)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found.',
            ]);
        }

        if (! $request->filled('address')) {
            return response()->json([
                'error' => 'Address not found.',
            ]);
        }

        $uniqueAddress = true;
        foreach ($user->addresses as $address) {
            if ($address->address == $request->input('address')) {
                $uniqueAddress = false;
                $address->update([
                    'selected'          => true,
                    'unit'              => $request->filled('unit') ? $request->input('unit') : $address->unit,
                    'instructions'      => $request->filled('instructions') ? $request->input('instructions') : $address->instructions,
                ]);
            } else {
                $address->update([
                    'selected'  => false,
                ]);
            }
        }

        if ($uniqueAddress) {
            $address = new Address;
            $address->address = $request->input('address');
            $address->unit = $request->filled('unit') ? $request->input('unit') : null;
            $address->instructions = $request->filled('instructions') ? $request->input('instructions') : null;
            $address->selected = 1;
            $user->addresses()->save($address);
        }
        $address->address = json_decode($address->address);

        return response()->json([
            'status'  => 'success',
            'address' => $address,
        ]);
    }

    public function attachSourceToCustomer(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (! $request->filled('token')) {
            return response()->json([
                'error' => 'No stripe token found.',
            ]);
        } else {
            $token = $request->input('token');
        }

        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No JWT found.',
            ]);
        }

        try {
            $customer = $this->getStripeCustomer();
            $source = $customer->sources->create(['source' => $token]);
            $customer->default_source = $source->id;
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];

            return $err;
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
            ]);
        }

        return response()->json(
            ['status'=>'success']
        );
    }

    public function removeCustomerCard(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No JWT found.',
            ]);
        }

        if (! $request->filled('id')) {
            return response()->json([
                'error' => 'No card id found.',
            ]);
        }
        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->sources->retrieve($request->input('id'))->delete();

        return response()->json([
            'status'    => 'update',
        ]);
    }

    public function selectDefaultCustomerSource(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No JWT found.',
            ]);
        }

        if (! $request->filled('id')) {
            return response()->json([
                'error' => 'No card id found.',
            ]);
        }

        try {
            $customer = \Stripe\Customer::retrieve($user->stripe_id);
            $customer->default_source = $request->input('id');
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];

            return $err;
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
            ]);
        }

        return $customer;
    }

    public function getStripeCustomer()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if ($this->getUser() && JWTAuth::getToken()) {
            $user = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No JWT found.',
            ]);
        }

        if ($user->stripe_id) {
            return $customer = \Stripe\Customer::retrieve($user->stripe_id);
        } else {
            $customer = \Stripe\Customer::create([
                'description' => ucfirst(strtolower($user->first_name)),
                'email'       => strtolower($user->email),
            ]);

            $user->stripe_id = $customer->id;
            $user->save();

            return $customer;
        }
    }

    public function getPreviousAddresses()
    {
        $user = $this->getUser();

        return $user->addresses;
    }
}
