<?php

namespace App\Http\Controllers\Courier\V1;

use App\Coupon;
use App\Http\Controllers\Controller;
use App\Invitation;
use App\Mail\OrderConfirmed;
use App\Order;
use App\Services\MailChimpService;
use App\User;
use Carbon\Carbon;
use Cart;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use JWTAuth;
use Nexmo\Laravel\Facade\Nexmo;
use Tymon\JWTAuth\Exceptions\JWTException;

class PaymentController extends Controller
{
    /**
     * get current user.
     * @return object user
     */
    private function getUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return $user;
    }

    /**
     * get client ip address.
     * @return string IP address
     */
    private function getClientIP()
    {
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            return  $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            return $_SERVER['REMOTE_ADDR'];
        } elseif (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        return '';
    }

    public function reviewOrder(Request $request)
    {
        if ($this->getUser() && JWTAuth::getToken()) {
            $user   = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found',
            ]);
        }

        $data      = $request->all();
        $validator = Validator::make($data, [
            'cart' => 'required',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
    }

    /**
     * place a order.
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function placeOrder(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if ($this->getUser() && JWTAuth::getToken()) {
            $user   = $this->getUser();
            $userId = $user->id;
        } else {
            return response()->json([
                'error' => 'No Authorization header found',
            ]);
        }

        $data      = $request->all();
        $validator = Validator::make($data, [
            'cart'     => 'required',
            'address'  => 'required',
            'time'     => 'required',
            'delivery' => 'required',
            'tip'      => 'required',
            'device'   => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $ip = $this->getClientIP();

        $items        = [];
        $affiliate    = null;
        $coupon       = null;
        $instructions = (object) [];
        $rewardPoints = RewardPoints::where('user_id', $user->id)->get();

        if ($data['time'] === 'asap') {
            $time = 'asap';
        } else {
            $timeArr = explode('-', $data['time']);
            $from    = Carbon::createFromFormat('m-d-H', $timeArr[0] . '-' . $timeArr[1] . '-' . $timeArr[2]);
            $to      = Carbon::createFromFormat('m-d-H', $timeArr[0] . '-' . $timeArr[1] . '-' . $timeArr[3]);
            $time    = $from->format('M') . ' ' . $from->format('jS') . ' ' . $from->format('g') . $from->format('A') . ' - ' . $to->format('g') . $to->format('A');
        }

        if ($user->rewardPointsBalance >= 1000) {
            $discount = 10;
            $user->rewardPointsTransaction(-1000, 4);
        } elseif ($request->filled('affiliate')) {
            // TODO - JAREK Fix this affiliate discount
            $affiliate = Coupon::find($request->input('affiliate'));
            $coupon    = Coupon::find($request->input('affiliate'));
            $discount  = $affiliate->value;
        } elseif ($request->filled('coupon')) {
            $coupon = Coupon::find($request->input('coupon'));

            $exists = DB::table('coupon_user')
                ->where('user_id', $user->id)
                ->where('coupon_id', $coupon->id)
                ->count() > 0;

            if ($exists) {
                return response()->json([
                    'error' => 'coupon has been used',
                ]);
            }

            $discount = $coupon->value;
        } else {
            $discount = 0;
        }

        if ($request->filled('instruction')) {
            $instructions->instruction = $request->input('instruction');
        }
        if ($request->filled('unitno')) {
            $instructions->unitno = $request->input('unitno');
        }

        $instructions = json_encode($instructions);
        Cart::instance($userId)->destroy();

        foreach ($data['cart'] as $item) {
            $item = gettype($item) == 'string' ? json_decode($item) : (object) $item;

            Cart::instance($userId)->add(
                $item->id,
                $item->name,
                $item->qty,
                $item->price
            )->associate(\App\ProductFinal::class);
        }

        foreach (Cart::instance($userId)->content() as $row) {
            array_push($items, (object) [
                'id'                        => $row->id,
                'name'                      => $row->model->name,
                'rowId'                     => $row->rowId,
                'qty'                       => $row->qty,
                'true_price'                => $row->model->price_in_cents / 100,
                'runner_price'              => $row->model->runner_price_in_cents / 100,
                'package'                   => $row->model->package,
                'brand'                     => $row->model->producer_name,
                'primary_category'          => $row->model->primary_category,
                'secondary_category'        => $row->model->secondary_category,
                'origin'                    => $row->model->origin,
                'thumbnail'                 => $row->model->image_url,
            ]);
        }

        if ($user->stripe_id && Cart::instance($userId)->subtotal() != 0) {
            $order = new Order;

            if ($data['time'] == 'asap') {
                $order->status = '0';
            } else {
                $order->status = '7';
            }

            $order->device = $request->input('device');

            $order->customer = json_encode((object) [
                'first_name'   => $user->first_name,
                'last_name'    => $user->last_name,
                'phone_number' => $user->phone_number,
                'email'        => $user->email,
            ]);

            $order->address = json_encode((object) [
                'address' => $request->input('address')['printable_address'],
                'lat'     => $request->input('address')['lat'],
                'lng'     => $request->input('address')['lng'],
            ]);

            $order->coupon_id = $request->input('coupon');

            if ($data['time'] == 'asap') {
                $order->schedule_time = null;
            } else {
                $timeArr = explode('-', $data['time']);

                $date = Carbon::createFromFormat('m-d', $timeArr[0] . '-' . $timeArr[1]);

                $hour = Carbon::createFromFormat('G', $timeArr[2])->hour;
                $date->setTimezone('America/Toronto');
                $date->hour   = $hour;
                $date->minute = 0;
                $date->second = 0;

                $order->schedule_time = $date;
            }

            if ($request->filled('instruction')) {
                $order->instructions = $request->input('instruction');
            } else {
                $order->instructions = null;
            }
            if ($request->filled('unitno')) {
                $order->unit_number = $request->input('unitno');
            } else {
                $order->unit_number = null;
            }

            $order->subtotal          = round(Cart::instance($userId)->subtotal() / 100, 2);
            $order->cogs              = round((Cart::instance($userId)->subtotal() / 100) / 1.1, 2);
            $order->discount          = $discount;
            $order->delivery          = $request->input('delivery');
            $order->delivery_tax      = round($request->input('delivery') * 0.13, 2);
            $order->markup            = round(($order->subtotal - $order->cogs) / 1.13, 2);
            $order->markup_tax        = round(($order->subtotal - $order->cogs) - $order->markup, 2);
            $order->display_tax_total = round(($order->delivery_tax + $order->markup_tax), 2);
            $order->tip               = $request->input('tip');
            $order->total             = $order->subtotal + $order->delivery + $order->display_tax_total + $order->tip - $order->discount;
            $order->stripe_fee        = round($order->total - $order->total * (1 - 0.029) + 0.3, 2);

            if (($user->role == 1) && env('APP_ENV') === 'production') {
                $order->total = 0.51;
            }

            try {
                $charge = \Stripe\Charge::create([
                    'amount'      => round($order->total, 2) * 100,
                    'currency'    => 'cad',
                    'customer'    => $user->stripe_id,
                    'description' => null,
                    'capture'     => false,
                    'metadata'    => [
                        'customer'          => $user->first_name . ' ' . $user->last_name,
                        'email'             => $user->email,
                        'phone'             => $user->phone_number,
                        'IP'                => $ip,
                        'address'           => $order->address,
                        'instructions'      => json_encode($instructions),
                        'time'              => $time,
                        'total'             => $order->tax,
                        'tax'               => $order->display_tax_total,
                        'delivery'          => $request->input('delivery'),
                        'discount'          => round($discount, 2),
                        'tip'               => $request->input('tip'),
                        'runner_subtotal'   => $order->subtotal,
                        'true_subtotal'     => $order->cogs,
                        'markup'            => $order->markup,
                    ],
                ]);
            } catch (\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                return $err;
            } catch (\Stripe\Error\ApiConnection $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                return $err;
            }

            $order->card_last_four = $charge->source->last4;
            $order->charge         = $charge->id;
            $content               = $items;
            $invitation            = Invitation::where('invited_user_id', $user->id)->first();

            if (count($user->orders) === 0 && $invitation) {
                $referrer = User::find($invitation->user_id);

                $referrer->rewardPointsTransaction(1000, 5, [
                    'referrer_id'         => $referrer->id,
                    'referrerer_name'     => $referrer->first_name . $referrer->last_name,
                    'referrer_email'      => $referrer->email,
                ]);
            }

            if (count($user->orders) === 0) {
                $order->first_time = 1;
            }

            $order->content = json_encode($content);

            $order->activity_log = [(object) [
                'log'   => 'received',
                'time'  => Carbon::now(),
            ]];

            $order->activity_log = json_encode($order->activity_log);
            $user->orders()->save($order);

            // Make stripe metadata and description
            $charge->description             = 'Order Number: ' . $order->id;
            $charge->metadata['OrderNumber'] = $order->id;

            $items = array_map(function ($item) {
                return (object) [
                    'id'                        => $item->id,
                    'name'                      => $item->name,
                    'qty'                       => $item->qty,
                    'true_price'                => $item->true_price,
                    'runner_price'              => $item->runner_price,
                    'package'                   => $item->package,
                    'brand'                     => $item->brand,
                    'primary_category'          => $item->primary_category,
                    'secondary_category'        => $item->secondary_category,
                    'origin'                    => $item->origin,
                    'thumbnail'                 => $item->thumbnail,
                ];
            }, $items);

            $charge->metadata['Items'] = Str::limit(json_encode($items), 400);
            $charge->save();

            if ($affiliate) {
                $exists = DB::table('coupon_user')
                    ->where('user_id', $user->id)
                    ->where('coupon_id', $affiliate->id)
                    ->count() > 0;

                if ($exists) { //promote girls path
                    $user->coupons()->updateExistingPivot($coupon->id, ['used' => 1]);
                } else { //runner cards path
                    $user->coupons()->attach($coupon->id, ['used' => 1]);
                    session()->forget('affiliateCoupon');
                }
            } elseif ($coupon) {
                $user->coupons()->attach($coupon->id, ['used' => 1]);
            }

            if (env('APP_ENV') == 'production') {
                $mailChimpService = new MailChimpService($user);
                $mailChimpService->FindorCreateCustomerAndAddOrder($order);
            }
            if (env('APP_ENV') === 'production') {
                Mail::to($user->email, $user->first_name)->bcc('service@getrunner.io', 'Runner')
                    ->queue(new OrderConfirmed($order));
            }
            if ($time == 'asap') {
                $text = $user->first_name . ', thanks for ordering with Runner! Your order will be on it\'s way shortly.';
            } else {
                $text = $user->first_name . ', thanks for ordering with Runner! Your order should be delivered on ' . $time;
            }

            if ($order->total > 20000) {
                $text_200 = ' Your order is above $200, please have your credit card ready to show the runner on arrival.';
            } else {
                $text_200 = ' ';
            }

            if (env('APP_ENV') === 'production') {
                try {
                    Nexmo::message()->send([
                        'to'   => '1' . $user->phone_number,
                        'from' => '12493155516',
                        'text' => $text . $text_200,
                    ]);
                } catch (Exception $e) {
                    Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());
                }
            }

            Cart::instance($userId)->destroy();

            return response()->json([
                'success' => 'order has been placed',
            ]);
        } else {
            return json_encode((object) [
                'message' => 'Sorry, we have encountered a technical issue. Please try refreshing your page. If that doesn\'t work, plesae contact Runner customer support for more information.',
            ]);
        }
    }
}
