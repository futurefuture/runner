<?php

namespace App\Http\Controllers\Courier\V2;

use App\Gift;
use App\Http\Controllers\Controller;
use App\Mail\OrderReceived;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Services\NotificationService;
use App\Store;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    private function createOrderItems($order)
    {
        $items = json_decode($order->content);
        $now   = Carbon::now();

        foreach ($items as $k => $item) {
            $product = Product::find($item->id);

            if (property_exists($item, 'type') && $item->type === 'bundle') {
                continue;
            }

            if (property_exists($item, 'price')) {
                $retail_price = ($item->price / 1.1) * 100;
            } elseif (property_exists($item, 'real_price')) {
                $retail_price = $item->real_price * 100;
            } elseif (property_exists($item, 'true_price')) {
                $retail_price = $item->true_price * 100;
            } elseif (property_exists($item, 'retailPrice')) {
                $retail_price = $item->retailPrice;
            } else {
                $retail_price = $product->retail_price;
            }

            if (property_exists($item, 'price')) {
                $runner_price = $item->price * 100;
            } elseif (property_exists($item, 'runner_price')) {
                $runner_price = $item->runner_price * 100;
            } elseif (property_exists($item, 'runnerPrice')) {
                $runner_price = $item->runnerPrice;
            } else {
                $runner_price = null;
            }

            if (property_exists($item, 'name')) {
                $title = $item->name;
            } elseif (property_exists($item, 'title')) {
                $title = $item->title;
            } else {
                $title = null;
            }

            if (property_exists($item, 'qty')) {
                $quantity = $item->qty;
            } elseif (property_exists($item, 'quantity')) {
                $quantity = $item->quantity;
            } else {
                $quantity = null;
            }

            if (property_exists($item, 'package')) {
                $packaging = $item->package;
            } elseif (property_exists($item, 'packaging')) {
                $packaging = $item->packaging;
            } else {
                $packaging = null;
            }

            if (! $product) {
                continue;
            } else {
                $orderItem = new OrderItem([
                    'order_id'     => $order->id,
                    'user_id'      => $order->user_id,
                    'product_id'   => $product->id,
                    'title'        => $title,
                    'quantity'     => (int) $quantity,
                    'retail_price' => $retail_price,
                    'runner_price' => $runner_price,
                    'packaging'    => $packaging,
                    'producer'     => $product->producer,
                    'category_id'  => $product->category_id,
                    'created_at'   => $now->toDateTimeString(),
                    'updated_at'   => $now->toDateTimeString(),
                ]);

                $orderItem->save();
            }
        }
    }

    /**
     * Gets all current orders.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();

        $orders = Order::whereIn('status', [0, 1, 2, 4, 7])
            ->where(function ($query) use ($user) {
                $query
                    ->where('runner_1', $user->id)
                    ->orWhere('runner_2', $user->id);
            })
            ->get();

        $formattedOrders = [];

        foreach ($orders as $order) {
            $order->customer    = json_decode($order->customer);
            $order->content     = json_decode($order->content);
            $now                = Carbon::now();
            $timeOrderMade      = Carbon::parse($order->created_at);
            $timeSinceOrderMade = $now->diffInMinutes($timeOrderMade);
            $address            = json_decode($order->address);
            $customer           = User::find($order->user_id);

            if ($customer->orders->count() === 1) {
                $isFirstTime = true;
            } else {
                $isFirstTime = false;
            }

            if ($order->user_id) {
                array_push($formattedOrders, (object) [
                    'id'                   => $order->id,
                    'type'                 => 'formatted orders',
                    'attributes'           => [
                        'content'            => $order->content,
                        'address'            => $order->address,
                        'status'             => $order->status,
                        'customer'           => $order->customer->firstName . ' ' . $order->customer->lastName,
                        'phoneNumber'        => $order->customer->phoneNumber,
                        'isGift'             => Gift::where('order_id', $order['id'])->first() ? true : false,
                        'timeSinceOrderMade' => $timeSinceOrderMade,
                        'address'            => $address,
                        'isFirstTime'        => $isFirstTime,
                    ],
                ]);
            }
        }

        return response()->json([
            'data' => $formattedOrders,
        ]);
    }

    /**
     * Gets order.
     *
     * @param int $id
     * @return Response
     */
    public function show($courierId, $orderId)
    {
        $order = Order::find($orderId);
        $user  = Auth::user();

        if (($order->runner_1 == $user->id || $order->runner_2 == $user->id)
            && ($order->status == 0 || $order->status == 1 || $order->status == 2 || $order->status == 4 || $order->status == 7)
        ) {
            $order->content  = json_decode($order->content);
            $order->address  = json_decode($order->address);
            $order->customer = json_decode($order->customer);

            foreach ($order->content as $item) {
                $product = Product::find($item->id);

                // set random until vintage style api sorted
                $item->isVintage = (bool) random_int(0, 1);
            }

            // check if item is gift
            $gift = Gift::where('order_id', $order->id)->first();

            if ($gift) {
                $gift->address = json_decode($gift->address);
                $order->gift   = $gift;
            }

            $now                = Carbon::now();
            $timeOrderMade      = Carbon::parse($order->created_at);
            $timeSinceOrderMade = $now->diffInMinutes($timeOrderMade);

            return response()->json([
                'data' => [
                    'id'                   => (string) $order->id,
                    'title'                => 'orders',
                    'attributes'           => [
                        'userId'             => $order->user_id,
                        'status'             => $order->status,
                        'content'            => $order->content,
                        'cardLastFour'       => $order->card_last_four,
                        'activityLog'        => json_decode($order->activity_log),
                        'adminNotes'         => $order->admin_notes,
                        'customer'           => $order->customer,
                        'address'            => $order->address,
                        'unitNumber'         => $order->unit_number,
                        'instructions'       => $order->instructions,
                        'subtotal'           => $order->subtotal,
                        'total'              => (float) $order->total,
                        'signature'          => $order->signature,
                        'deliveredAt'        => $order->delivered_at,
                        'scheduleTime'       => $order->schedule_time,
                        'additionalInfo'     => $order->additional_info,
                        'isFirstTime'        => $order->first_time,
                        'gift'               => $order->gift,
                        'timeSinceOrderMade' => $timeSinceOrderMade,
                    ],
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'The order cannot be found.',
                ],
            ], 404);
        }
    }

    public function pastOrders()
    {
        $user = Auth::user();

        $orders = Order::whereIn('status', [3, 5, 6])
            ->where(function ($query) use ($user) {
                $query
                    ->where('runner_1', $user->id)
                    ->orWhere('runner_2', $user->id);
            })
            ->Orderby('delivered_at', 'DESC')
            ->limit(10)
            ->get();

        $simplified_orders = [];

        foreach ($orders as $order) {
            $order->customer = json_decode($order->customer);

            if ($order->user_id) {
                array_push($simplified_orders, (object) [
                    'id'       => (string) $order->id,
                    'status'   => $order->status,
                    'customer' => $order->customer->first_name . ' ' . $order->customer->last_name,
                    'gift'     => Gift::where('order_id', $order['id'])->first() ? true : false,
                ]);
            }
        }

        return response()->json([
            'data' => $simplified_orders,
        ]);
    }

    public function pastOrder($id)
    {
        $user  = Auth::user();
        $order = Order::find($id);

        if (($order->runner_1 == $user->id || $order->runner_2 == $user->id) && ($order->status == 3 || $order->status == 5 || $order->status == 6)
        ) {
            $order->content = json_decode($order->content);

            $orderContentCount = count($order->content);

            $products_id = [];

            $stores_id = [];

            if ($orderContentCount == 1) {
                $order->content->qty = (int) $order->content->qty;

                if (ProductFinal::find($order->content->id)->stock_type == 'VINTAGES') {
                    $order->content->isVintages = 1;
                } else {
                    $order->content->isVintages = 0;
                }

                array_push($products_id, $order->content->id);
            } else {
                foreach ($order->content as $item) {
                    $item->qty = (int) $item->qty;

                    if (ProductFinal::find($item->id)->stock_type == 'VINTAGES') {
                        $item->isVintages = 1;
                    } else {
                        $item->isVintages = 0;
                    }

                    array_push($products_id, $item->id);
                }
            }

            $stores_id = DB::table('inventories')
                ->select('store_id')
                ->where('quantity', '>', 0)
                ->whereIn('product_id', $products_id)
                ->groupBy('store_id')
                ->havingRaw('COUNT(`store_id`) >= ' . count($products_id))
                ->get()->toArray();

            for ($i = 0; $i < count($stores_id); $i++) {
                $stores_id[$i] = $stores_id[$i]->store_id;
            }

            $stores = Store::findMany($stores_id);

            //find closest store have items instock
            $gift = Gift::where('order_id', $order->id)->first();

            if (count($stores) > 0) {
                if ($gift) {
                    $order->store = $this->find_closest_marker($gift->address->lat, $gift->address->lng, $stores);
                } else {
                    $order->store = $this->find_closest_marker($order->address->lat, $order->address->lng, $stores);
                }
            } else {
                $order->store = Store::where('store_id', 10)->first();
            }

            if ($gift) {
                $gift->address = json_decode($gift->address);
                $order->gift   = $gift;
            }

            // dd($order->customer);

            return response()->json([
                'data'  => [
                    'id'                  => (string) $order->id,
                    'user_id'             => $order->user_id,
                    'status'              => $order->status,
                    'content'             => $order->content,
                    'card_last_four'      => $order->card_last_four,
                    'activity_log'        => json_decode($order->activity_log),
                    'admin_notes'         => $order->admin_notes,
                    'charge'              => $order->charge,
                    'runner_1'            => $order->runner_1,
                    'runner_2'            => $order->runner_2,
                    'device'              => $order->device,
                    'customer'            => json_decode($order->customer),
                    'address'             => json_decode($order->address),
                    'unit_number'         => $order->unit_number,
                    'coupon'              => $order->coupon_id,
                    'instructions'        => $order->instructions,
                    'subtotal'            => (float) $order->subtotal,
                    'cogs'                => (float) $order->cogs,
                    'discount'            => (float) $order->discount,
                    'delivery'            => (float) $order->delivery,
                    'delivery_tax'        => (float) $order->delivery_tax,
                    'markup'              => (float) $order->markup,
                    'markup_tax'          => (float) $order->markup_tax,
                    'display_tax_total'   => (float) $order->display_tax_total,
                    'tip'                 => (float) $order->tip,
                    'total'               => (float) $order->total,
                    'stripe_fee'          => (float) $order->stripe_fee,
                    'signature'           => $order->signature,
                    'delivered_at'        => $order->delivered_at,
                    'schedule_time'       => $order->schedule_time,
                    'additional_info'     => $order->additional_info,
                    'first_time'          => $order->first_time,
                    'gift'                => $order->gift,
                    'store'               => $order->store,
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'The order cannot be found',
                ],
            ], 404);
        }
    }

    public function updateOrder(Request $request, $id)
    {
        $user  = Auth::user();
        $order = Order::find($id);

        if (($order->runner_1 == $user->id || $order->runner_2 == $user->id)) {
            if (! $request->filled('status')) {
                return response()->json([
                    'errors' => [
                        'error' => 'Could not find the status field',
                    ],
                ], 403);
            }

            $activity_log = json_decode($order->activity_log);

            if ($user->role == 2 || $user->role == 1 || $user->role == 5) {
                if ($request->input('status') == 1 && ($order->status == 0 || $order->status == 7)) {
                    array_push($activity_log, (object) [
                        'log'  => 'processing',
                        'time' => Carbon::now('America/Toronto'),
                    ]);
                } elseif ($request->input('status') == 2 && $order->status == 1) {
                    array_push($activity_log, (object) [
                        'log'  => 'onroute',
                        'time' => Carbon::now('America/Toronto'),
                    ]);
                } elseif ($request->input('status') == 3 && $order->status == 2) {
                    if (! $request->filled('signature')) {
                        return response()->json([
                            'errors' => [
                                'error' => 'signature field not found',
                            ],
                        ], 403);
                    }

                    $order->signature = $request->input('signature');

                    if (env('APP_ENV') === 'production') {
                        Storage::disk('order_signatures')->put($order->id . '.png', base64_decode(str_replace('data:image/png;base64,', '', $order->signature)));
                    }

                    array_push($activity_log, (object) [
                        'log'  => 'delivered',
                        'time' => Carbon::now('America/Toronto'),
                    ]);

                    $order->delivered_at = Carbon::now('America/Toronto');
                    DB::table('promotions_used')
                        ->where('order_id', $order->id)
                        ->update(['used' => 1]);
                } else {
                    return response()->json([
                        'errors' => [
                            'error' => 'Please process in order',
                        ],
                    ], 403);
                }
            }

            $order->status = $request->input('status');

            $order->activity_log = json_encode($activity_log);

            if ($order->save()) {
                if ($order->status === '3') {
                    $order->content  = json_decode($order->content);
                    $order->customer = json_decode($order->customer);
                    if (env('APP_ENV') === 'production') {
                        Mail::to($order->customer->email)->bcc('service@getrunner.io')
                            ->queue(new OrderReceived($order));
                    }
                }

                if ($order->status === 2) {
                    $notificationService = new NotificationService();
                    $notificationService->orderOnRoute($order);
                } elseif ($order->status === 3) {
                    $notificationService = new NotificationService();
                    $notificationService->orderDelivered($order);
                }

                return response()->json([
                    'message' => 'success',
                ]);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => 'Could not update the order',
                    ],
                ], 403);
            }
        }
    }

    public function getCurrentOrderItems()
    {
        $user   = Auth::user();
        $orders = Order::whereIn('status', [0, 1, 2, 4, 7])
            ->where(function ($query) use ($user) {
                $query
                    ->where('runner_1', $user->id)
                    ->orWhere('runner_2', $user->id);
            })->get();

        $items = [];

        foreach ($orders as $order) {
            $order->content = json_decode($order->content);
            $items          = array_merge($items, $order->content);
        }

        $filteredItems = [];

        foreach ($items as $key => $item) {
            if (array_key_exists($item->id, $filteredItems)) {
                $filteredItems[$item->id]->qty += $item->qty;
            } else {
                $filteredItems[$item->id] = $item;
            }
        }

        $itemArr = [
            'beer'      => [],
            'wine'      => [],
            'spirits'   => [],
            'coolers'   => [],
            'bundles'   => [],
        ];

        foreach ($filteredItems as $key => $filteredItem) {
            $filteredItem->qty = (int) $filteredItem->qty;

            if ($filteredItem->primary_category && array_key_exists($filteredItem->primary_category, $itemArr)) {
                array_push($itemArr[$filteredItem->primary_category], $filteredItem);
            } else {
                array_push($itemArr['bundles'], $filteredItem);
            }
        }

        if (count($itemArr['wine']) != 0) {
            $wineArr = [
                'Vintages'  => [],
                'Regular'   => [],
            ];

            foreach ($itemArr['wine'] as $key => $wine) {
                if (ProductFinal::find($wine->id) != null && ProductFinal::find($wine->id)->stock_type === 'VINTAGES') {
                    $wine->isVintages      = 1;
                    $wine->qty             = (int) $wine->qty;
                    $wineArr['Vintages'][] = $wine;
                } else {
                    $wine->qty            = (int) $wine->qty;
                    $wineArr['Regular'][] = $wine;
                }
            }

            $regularWineArr = [];

            foreach ($wineArr['Regular'] as $key => $regularWine) {
                if ($product = ProductFinal::find($regularWine->id)) {
                    $regularWine->isVintages                       = 0;
                    $regularWineArr[$product->producing_country][] = $regularWine;
                }
            }
            $wineArr = array_merge($wineArr, $regularWineArr);

            unset($wineArr['Regular']);

            $itemArr['wine'] = $wineArr;
        } else {
            $itemArr['wine'] = [
                'Vintages'  => [],
            ];
        }

        foreach ($itemArr as $key => $item) {
            $itemArr[ucfirst($key)] = $itemArr[$key];
            unset($itemArr[$key]);
        }

        return response()->json([
            'data' => $itemArr,
        ]);
    }

    public function updateCurrentOrder(Request $request, $id)
    {
        $user  = Auth::user();
        $order = Order::find($id);

        if (($order->runner_1 === $user->id || $order->runner_2 === $user->id)) {
            if (! $request->filled('status')) {
                return response()->json([
                    'errors' => [
                        'error' => 'Could not find the status field',
                    ],
                ], 403);
            }

            $activity_log = json_decode($order->activity_log);

            if ($user->role == 2 || $user->role == 1 || $user->role == 5) {
                if ($request->input('status') == 1 && ($order->status == 0 || $order->status == 7)) {
                    array_push($activity_log, (object) [
                        'log'  => 'processing',
                        'time' => Carbon::now('America/Toronto'),
                    ]);
                } elseif ($request->input('status') == 2 && $order->status == 1) {
                    array_push($activity_log, (object) [
                        'log'  => 'onroute',
                        'time' => Carbon::now('America/Toronto'),
                    ]);
                } elseif ($request->input('status') == 3 && $order->status == 2) {
                    if (! $request->filled('signature')) {
                        return response()->json([
                            'errors' => [
                                'error' => 'signature field not found',
                            ],
                        ], 403);
                    }

                    $order->signature = $request->input('signature');

                    if (env('APP_ENV') === 'production') {
                        Storage::disk('order_signatures')->put($order->id . '.png', base64_decode(str_replace('data:image/png;base64,', '', $order->signature)));
                    }

                    array_push($activity_log, (object) [
                        'log'  => 'delivered',
                        'time' => Carbon::now('America/Toronto'),
                    ]);

                    $order->delivered_at = Carbon::now('America/Toronto');

                    DB::table('promotions_used')
                        ->where('order_id', $order->id)
                        ->update(['used' => 1]);
                } else {
                    return response()->json([
                        'errors' => [
                            'error' => 'Please process in order',
                        ],
                    ], 403);
                }
            }

            $order->status = $request->input('status');

            $order->activity_log = json_encode($activity_log);

            if ($order->save()) {
                if ($order->status === '3') {
                    $order->content  = json_decode($order->content);
                    $order->customer = json_decode($order->customer);
                    if (env('APP_ENV') === 'production') {
                        Mail::to($order->customer->email)->bcc('service@getrunner.io')
                            ->queue(new OrderReceived($order));
                    }
                    $this->createOrderItems($order);
                }

                if ($order->status === 2) {
                    $notificationService = new NotificationService();
                    $notificationService->orderOnRoute($order);
                } elseif ($order->status === 3) {
                    $notificationService = new NotificationService();
                    $notificationService->orderDelivered($order);
                }

                return response()->json([
                    'message' => 'success',
                ]);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => 'Could not update the order',
                    ],
                ], 403);
            }
        }
    }
}
