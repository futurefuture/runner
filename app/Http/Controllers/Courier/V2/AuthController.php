<?php

namespace App\Http\Controllers\Courier\V2;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    /**
     * Logs Runner in.
     *
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $data = $request->input('data.attributes');

        $validator = Validator::make($data, [
            'email'    => 'required|email',
            'password' => 'required',
            // 'fcmToken' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors'      => $errors,
            ], 401);
        }

        if (Auth::attempt([
            'email' => $data['email'],
            'password' => $data['password'],
        ])) {
            $user        = Auth::user();

            // update fcm token
            if (isset($data['fcmToken'])) {
                $user->fcm_token = $data['fcmToken'];
                $user->save();
            }

            $accessToken = $user->createToken('Courier')
                                        ->accessToken;

            return response()->json([
                    'data' => [
                        'type'       => 'access tokens',
                        'attributes' => [
                            'accessToken' => $accessToken,
                            'user'        => [
                                'id'        => $user->id,
                                'firstName' => $user->first_name,
                                'lastName'  => $user->last_name,
                            ],
                        ],
                    ],
                ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'Invalid Credentials',
                ],
            ], 401);
        }
    }

    public function logout()
    {
        $user         = Auth::user();
        $accessTokens = $user->tokens;

        foreach ($accessTokens as $accessToken) {
            $accessToken->revoke();
        }
    }
}
