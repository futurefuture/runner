<?php

namespace App\Http\Controllers\Courier\V2;

use Auth;
use App\Task;
use App\User;
use App\Order;
use App\Product;
use App\OrderItem;
use Carbon\Carbon;
use Stripe\Stripe;
use App\Mail\OrderReceived;
use Illuminate\Http\Request;
use App\Services\StripeService;
use App\Http\Controllers\Controller;
use App\Mail\OrderReceivedForRunner;
use Illuminate\Support\Facades\Mail;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Storage;
use App\Actions\CourierTask\UpdateCourierTask;
use App\Http\Resources\Courier\TaskCollection;
use App\Http\Resources\Courier\Task as TaskResource;

class TaskController extends Controller
{
    /**
     * Gets all tasks for given courier.
     *
     * @param int $courierId
     * @return TaskCollection
     */
    public function index($courierId)
    {
        $tasks = Task::where('courier_id', $courierId)
                    ->where('state', '!=', 3)
                    ->where('state', '!=', 5)
                    ->get();

        return new TaskCollection($tasks);
    }

    /**
     * Gets single task for courier.
     *
     * @param int $courierId
     * @param int $taskId
     * @return TaskResource
     */
    public function show($courierId, $taskId)
    {
        $task = Task::where('courier_id', $courierId)
                            ->where('id', $taskId)
                            ->first();

        return new TaskResource($task);
    }

    /**
     * Updates task.
     */
    public function update(Request $request, User $courier, Task $task, UpdateCourierTask $action): TaskResource
    {
        $data = $request->input('data.attributes');
        $task = $action->execute($courier, $task, $data);

        return new TaskResource($task);
    }

    /**
     * Deletes task.
     *
     * @param int $courierId
     * @param int $taskId
     * @return void
     */
    public function delete($courierId, $taskId)
    {
        $task = Task::find($taskId);

        $task->delete();
    }
}
