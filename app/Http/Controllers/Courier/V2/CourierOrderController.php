<?php

namespace App\Http\Controllers\Courier\V2;

use App\Http\Controllers\Controller;
use App\Task;
use App\Order;
use Carbon\Carbon;
use App\Http\Resources\Courier\Task as TaskResource;
use App\Http\Resources\Courier\OrderSimplified as OrderResource;

class CourierOrderController extends Controller
{
    /**
     * Helper function for structuring the index() return.
     *
     * @param Array $task
     * @return Array
     */
    public function createOrderArr($task)
    {
        $formattedOrder = [];
        $i = 0;
        foreach ($task as $order) {
            $order   = Order::find($task[$i]['order_id']);
            array_push($formattedOrder, (object) new OrderResource($order));
            $i++;
        }

        return [
            'type'  => 'tasks',
            'date'    => Carbon::parse($task[0]['created_at'])->format('m-d-Y'),
            'tasks'        => $formattedOrder,
        ];
    }

    /**
     * Gets all the orders for a courier
     *
     * @param int $courierId
     * @return Array
     */
    public function index($courierId)
    {
        $tasks = Task::where('courier_id', $courierId)
            ->where('state', '!=', 1)
            ->where('state', '!=', 2)
            ->orderBy('created_at', 'DESC')
            ->paginate(10)
            ->groupBy(function ($task) {
                return $task->created_at->format('m-d-Y');
            });

        $tasks = $tasks->toArray();

        $formattedTasks = [];
        $i = 0;
        $keys = array_keys($tasks);
        foreach ($tasks as $task) {
            array_push($formattedTasks, $this->createOrderArr($tasks[$keys[$i]]));
            $i++;
        }

        return response()->json([
            'data' => $formattedTasks,
        ]);
    }
    /**
     * Gets single order information using the Users(Courier) table.
     *
     * @param int $courierId
     *
     * @return TaskResource
     */
    public function show($courierId, $orderId)
    {
        $task = Task::where('courier_id', $courierId)
            ->where('order_id', $orderId)
            ->first();

        return new TaskResource($task);
    }
}
