<?php

namespace App\Http\Controllers\Courier\V2;

use App\Category;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Task;

class OrderItemController extends Controller
{
    public function index($courierId)
    {
        $tasks = Task::where('courier_id', $courierId)
                    ->where('state', '!=', 3)
                    ->where('state', '!=', 5)
                    ->where('type', 'order')
                    ->get();

        $orderItems = [];

        foreach ($tasks as $task) {
            $order        = Order::find($task->order_id);
            $orderContent = json_decode($order->content);

            foreach ($orderContent as $o) {
                $product         = Product::find($o->id);
                $category        = Category::find($product->category_id);
                $primaryCategory = Category::find($category->parent_id) ?? $category;

                $orderItem                     = [];
                $orderItem['sku']              = $product->sku;
                $orderItem['title']            = $product->title;
                $orderItem['quantity']         = $o->quantity;
                $orderItem['image']            = $product->image;
                $orderItem['isVintage']        = false;
                $orderItem['retailPrice']      = $product->retail_price;
                $orderItem['packaging']        = $product->packaging;
                $orderItem['producingCountry'] = $product->producing_country;
                $orderItem['category']         = $category->title;
                $orderItem['primaryCategory']  = $primaryCategory->title;
                $orderItem['customer']         = json_decode($order->customer)->firstName . ' ' . substr(json_decode($order->customer)->lastName, 0, 1);

                array_push($orderItems, [
                    'type'       => 'order items',
                    'id'         => $product->id,
                    'attributes' => $orderItem,
                ]);
            }
        }

        return response()->json([
            'data' => $orderItems,
        ]);
    }
}
