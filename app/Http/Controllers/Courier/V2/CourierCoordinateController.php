<?php

namespace App\Http\Controllers\Courier\V2;

use App\Http\Controllers\Controller;
use Auth;
use App\RunnerLocation;
use DB;
use Illuminate\Http\Request;

class CourierCoordinateController extends Controller
{
    public function store(Request $request, $courierId)
    {
        $user = Auth::user();

        if ($user->id == $courierId) {
            $data = $request->data['attributes'];

            RunnerLocation::create([
                'runner_id' => $courierId,
                'longitude' => $data['longitude'],
                'latitude'  => $data['latitude'],
            ]);

            $courierCoordinate = DB::table('runner_location')->where('runner_id', $courierId)->first();

            return response()->json([
                'data' => [
                    'id'         => $courierCoordinate->id,
                    'title'      => 'courier coordinates',
                    'attributes' => [
                        'courierId'  => $courierCoordinate->runner_id,
                        'longitude'  => $courierCoordinate->longitude,
                        'latitude'   => $courierCoordinate->latitude,
                        'created_at' => $courierCoordinate->created_at,
                        'updated_at' => $courierCoordinate->updated_at,
                    ],
                ],
            ]);
        }
    }
}
