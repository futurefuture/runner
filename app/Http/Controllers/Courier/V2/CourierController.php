<?php

namespace App\Http\Controllers\Courier\V2;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Courier\Courier as CourierResource;

class CourierController extends Controller
{
    /**
     * Updates courier details.
     *
     * @param int $courierId
     */
    public function update(Request $request, $courierId)
    {
        // $user = Auth::user();
        $user = User::find($courierId);
        $data = $request->input('data.attributes');

        if (isset($data['fcmToken'])) {
            $user->fcm_token = $data['fcmToken'];
        }
        if (isset($data['firstName'])) {
            $user->first_name = $data['firstName'];
        }

        if (isset($data['lastName'])) {
            $user->last_name = $data['lastName'];
        }

        if (isset($data['phoneNumber'])) {
            $user->phone_number = $data['phoneNumber'];
        }

        if (isset($data['email'])) {
            $user->email = $data['email'];
        }

        $user->save();

        return new CourierResource($user);
    }

    /**
     * Gets single courier information using the Users table.
     *
     * @param int $courierId
     *
     * @return CourierResource
     */
    public function show($courierId)
    {
        $courier = User::find($courierId);

        return new CourierResource($courier);
    }
}
