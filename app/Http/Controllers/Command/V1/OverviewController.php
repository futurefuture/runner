<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use DB;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function getTempDashNumbers(Request $request)
    {
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        $costOfGoods = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('cogs')->sum();
        $tips = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('tip')->sum();
        $deliveryFees = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('delivery')->sum();
        $discounts = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('discount')->sum();
        $stripeFees = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('stripe_fee')->sum();
        $markup = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('markup')->sum();
        $taxes = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('display_tax_total')->sum();
        $total = Order::where('status', 3)->whereBetween('created_at', [$startDate, $endDate])->pluck('total')->sum();

        $oCostOfGoods = 0;
        $oRunnerPrice = 0;

        OrderItem::whereBetween('created_at', [$startDate, $endDate])->chunk(100, function ($orderItems) use (&$oCostOfGoods, &$oRunnerPrice) {
            foreach ($orderItems as $o) {
                $oCostOfGoods += $o->quantity * $o->retail_price;
                $oRunnerPrice += $o->quantity * $o->runner_price;
            }
        });

        return response()->json([
            'data' => [
                'orders' => [
                    'costOfGoods'  => $costOfGoods,
                    'tips'         => $tips,
                    'deliveryFees' => $deliveryFees,
                    'discounts'    => $discounts,
                    'total'        => $total,
                    'stripeFees'   => $stripeFees,
                    'taxes'        => $taxes,
                    'markup'       => $markup,
                ],
                'orderItems' => [
                    'costOfGoods' => $oCostOfGoods,
                    'markup'      => $oRunnerPrice - $oCostOfGoods,
                ],
            ],
        ]);
    }

    public function getNewNumbers(Request $request, $partnerId = null)
    {
        $spirits = Product::whereIn('category_id', [3, 9, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26])->get()->pluck('id');
        $wine = Product::whereIn('category_id', [4, 6, 7, 10, 11, 12, 13, 14, 15, 16])->get()->pluck('id');
        $beer = Product::whereIn('category_id', [27, 28, 29, 30, 31, 32])->get()->pluck('id');
        $coolers = Product::whereIn('category_id', [5])->get()->pluck('id');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        if ($partnerId == 0) {
            // runner
            // runner all categories
            $allProductSales = 0;
            OrderItem::whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($allOrderItems) use (&$allProductSales) {
                foreach ($allOrderItems as $aoi) {
                    $allProductSales += $aoi->qty * $aoi->true_price;
                }
            });

            // runner spirits
            $totalSpiritSales = 0;

            OrderItem::whereIn('item_id', $spirits)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalSpiritOrderItems) use (&$totalSpiritSales) {
                foreach ($totalSpiritOrderItems as $tsoi) {
                    $totalSpiritSales += $tsoi->qty * $tsoi->true_price;
                }
            });

            // runner wine
            $totalWineSales = 0;

            OrderItem::whereIn('item_id', $wine)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalWineOrderItems) use (&$totalWineSales) {
                foreach ($totalWineOrderItems as $twoi) {
                    $totalWineSales += $twoi->qty * $twoi->true_price;
                }
            });

            // runner beer
            $totalBeerSales = 0;
            OrderItem::whereIn('item_id', $beer)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalBeerOrderItems) use (&$totalBeerSales) {
                foreach ($totalBeerOrderItems as $tboi) {
                    $totalBeerSales += $tboi->qty * $tboi->true_price;
                }
            });

            // runner coolers
            $totalCoolerSales = 0;
            OrderItem::whereIn('item_id', $coolers)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalCoolerOrderItems) use (&$totalCoolerSales) {
                foreach ($totalCoolerOrderItems as $tcoi) {
                    $totalCoolerSales += $tcoi->qty * $tcoi->true_price;
                }
            });
        } else {
            // partner
            // partner all categories
            $allPartnerProducts = DB::table('clients_products')->where('client_id', $partnerId)->get()->pluck('product_id');
            $allProductSales = 0;
            OrderItem::whereBetween('created_at', [
                $startDate, $endDate,
            ])->whereIn('item_id', $allPartnerProducts)->chunk(1000, function ($allOrderItems) use (&$allProductSales) {
                foreach ($allOrderItems as $aoi) {
                    $allProductSales += $aoi->qty * $aoi->true_price;
                }
            });

            // partner spirits
            $totalSpiritSales = 0;
            $spirits = Product::whereIn('category_id', [3, 9, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26])
                                            ->whereIn('id', $allPartnerProducts)
                                            ->get()
                                            ->pluck('id');

            OrderItem::whereIn('item_id', $spirits)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalSpiritOrderItems) use (&$totalSpiritSales) {
                foreach ($totalSpiritOrderItems as $tsoi) {
                    $totalSpiritSales += $tsoi->qty * $tsoi->true_price;
                }
            });

            // partner wine
            $totalWineSales = 0;
            $wine = Product::whereIn('category_id', [4, 6, 7, 10, 11, 12, 13, 14, 15, 16])
                                                ->whereIn('id', $allPartnerProducts)
                                                ->get()
                                                ->pluck('id');

            OrderItem::whereIn('item_id', $wine)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalWineOrderItems) use (&$totalWineSales) {
                foreach ($totalWineOrderItems as $twoi) {
                    $totalWineSales += $twoi->qty * $twoi->true_price;
                }
            });

            // partner beer
            $totalBeerSales = 0;
            $beer = Product::whereIn('category_id', [27, 28, 29, 30, 31, 32])
                                                ->whereIn('id', $allPartnerProducts)
                                                ->get()
                                                ->pluck('id');

            OrderItem::whereIn('item_id', $beer)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalBeerOrderItems) use (&$totalBeerSales) {
                foreach ($totalBeerOrderItems as $tboi) {
                    $totalBeerSales += $tboi->qty * $tboi->true_price;
                }
            });

            // partner coolers
            $totalCoolerSales = 0;
            $coolers = Product::whereIn('category_id', [5])
                                                ->whereIn('id', $allPartnerProducts)
                                                ->get()
                                                ->pluck('id');

            OrderItem::whereIn('item_id', $coolers)->whereBetween('created_at', [
                $startDate, $endDate,
            ])->chunk(1000, function ($totalCoolerOrderItems) use (&$totalCoolerSales) {
                foreach ($totalCoolerOrderItems as $tcoi) {
                    $totalCoolerSales += $tcoi->qty * $tcoi->true_price;
                }
            });
        }

        return response()->json([
            'data' => [
                'allProductSales'  => $allProductSales,
                'totalSpiritSales' => $totalSpiritSales,
                'totalWineSales'   => $totalWineSales,
                'totalBeerSales'   => $totalBeerSales,
                'totalCoolerSales' => $totalCoolerSales,
            ],
        ]);
    }
}
