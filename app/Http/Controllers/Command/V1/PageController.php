<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    protected $domain = 'command';

    /**
     * Gets stores view.
     *
     * @return void
     */
    public function stores()
    {
        return view('command.stores');
    }

    /**
     * Gets store view.
     *
     * @return void
     */
    public function store($storeId)
    {
        return view('command.store', compact(['storeId']));
    }

    /**
     * Gets products view.
     *
     * @return View
     */
    public function products()
    {
        return view('command.products');
    }

    /**
     * Gets product view.
     *
     * @return View
     */
    public function product($productId)
    {
        return view('command.product', compact(['productId']));
    }

    /**
     * Gets tags view.
     *
     * @return View
     */
    public function tags()
    {
        return view('command.tags');
    }

    /**
     * Gets overview view.
     *
     * @return View
     */
    public function overview()
    {
        return view('command.overview');
    }

    public function config()
    {
        return view('command.config');
    }
}
