<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Partner;
use App\PartnerProduct;
use App\Product;
use Illuminate\Http\Request;

class ProductPartnerController extends Controller
{
    /**
     * Gets product partners.
     *
     * @param int $productId
     * @return void
     */
    public function index($productId)
    {
        $products = PartnerProduct::where('product_id', $productId)->get();
        $formattedProducts = [];

        foreach ($products as $p) {
            $partner = Partner::find($p->partner_id);
            $formattedProduct = (object) [
                'type'       => 'product partners',
                'id'         => $p->id,
                'attributes' => [
                    'partnerId'  => $p->partner_id,
                    'productId'  => $p->product_id,
                    'title'      => $partner->title,
                    'created_at' => $p->created_at,
                    'updated_at' => $p->updated_at,
                ],
            ];

            array_push($formattedProducts, $formattedProduct);
        }

        return response()->json([
            'data' => $formattedProducts,
        ]);
    }

    public function create(Request $request, $productId)
    {
        $data = $request->input('data.attributes');
        $product = Product::find($productId);

        $productPartner = PartnerProduct::create([
            'product_id' => $productId,
            'partner_id' => $data['partnerId'],
            'title'      => $product->title,
        ]);
    }

    public function delete($productId, $productPartnerId)
    {
        PartnerProduct::find($productPartnerId)->delete();
    }
}
