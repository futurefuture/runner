<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use App\Services\NotificationService;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function getReviews($type = null)
    {
        if ($type) {
            return Review::with('user')
                    ->where('is_approved', $type)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(15);
        } else {
            return Review::with('user')
                ->where('is_approved', 0)
                ->orderBy('created_at', 'DESC')
                ->paginate(15);
        }
    }

    public function verifyReview($reviewId, Request $request)
    {
        $approve = $request->input('approve');
        $productId = $request->input('productId');
        $product = Product::find($productId);
        $review = Review::find($reviewId);

        if ($approve === 'yes') {
            $review->is_approved = 1;
            $review->save();

            if ($review::where('product_id', $product->id)->count() === 1) {
                $review->user->rewardPointsTransaction(500, 8, [
                    'product_id' => $product->id,
                    'name'       => $product->name,
                ]);

                $isFirstReview = true;
                $notificationService = new NotificationService($order = null);
                $notificationService->sendReviewAcceptedNotification($review, $product, $isFirstReview);
            } else {
                $review->user->rewardPointsTransaction(100, 7, [
                    'product_id' => $product->id,
                    'name'       => $product->name,
                ]);

                $isFirstReview = false;
                $notificationService = new NotificationService($order = null);
                $notificationService->sendReviewAcceptedNotification($review, $product, $isFirstReview);
            }
        } else {
            $review->is_approved = 2;
            $review->save();
        }
    }
}
