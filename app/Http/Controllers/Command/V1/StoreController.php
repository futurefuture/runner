<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Store as StoreResource;
use App\Http\Resources\StoresCollection;
use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Gets all stores.
     *
     * @return StoresCollection
     */
    public function index()
    {
        $stores = Store::all();

        return new StoresCollection($stores);
    }

    /**
     * Gets individual store.
     *
     * @return StoreResource
     */
    public function show($storeId)
    {
        $store = Store::find($storeId);

        return new StoreResource($store);
    }

    /**
     * Updates individual store.
     *
     * @return StoreResource
     */
    public function update(Request $request, $storeId)
    {
        $data = $request->input('data.attributes');
        $store = Store::find($storeId);
        $store->title = $data['title'];
        $store->is_active = $data['isActive'];
        $store->options = $data['options'];
        $store->cart_extras = $data['cartExtras'] ?? null;

        $store->save();
    }
}
