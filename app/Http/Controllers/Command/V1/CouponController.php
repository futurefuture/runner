<?php

namespace App\Http\Controllers\Command\V1;

use App\Coupon;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index()
    {
        return view('command.coupons');
    }

    public function getCoupons($id = null)
    {
        if ($id) {
            $coupon = Coupon::find($id);
            $used_users = $coupon->users()->wherePivot('used', 1)->get()->toArray();

            $used_users = array_map(function ($user) {
                return (object) [
                    'id'    => $user['id'],
                    'name'  => $user['first_name'].' '.$user['last_name'],
                    'email' => $user['email'],
                ];
            }, $used_users);

            return $used_users;
        } else {
            $coupons = Coupon::all();

            foreach ($coupons as $coupon) {
                $coupon->used = $coupon->users()->wherePivot('used', 1)->count();
            }

            return $coupons;
        }
    }

    public function newCoupon(Request $request)
    {
        $data = $request->input('coupon');
        $coupon = new Coupon;
        $coupon->value = $data['value'];
        $coupon->code = strtoupper($data['code']);
        $coupon->active_until = $data['active_until'];

        if ($coupon->save()) {
            return 1;
        }

        return 0;
    }

    public function updateCoupon(Request $request, $coupon)
    {
        $coupon = Coupon::find($coupon);
        $coupon->value = $request->input('value');
        $coupon->code = strtoupper($request->input('code'));
        $coupon->active_until = $request->input('active_until');
        $coupon->is_active = $request->input('is_active');

        if ($coupon->update()) {
            return 1;
        }

        return 0;
    }

    public function validateCoupon(Request $request)
    {
        $coupon = Coupon::where('code', strtoupper($request->input('coupon')))->first();
        $nowInTorontoTz = Carbon::now('America/Toronto');

        if (! $coupon) {
            return 'This coupon is invalid!';
        }

        if (! $coupon->is_active || ! ($nowInTorontoTz <= $coupon->active_until)) {
            return 'This coupon is invalid!';
        }

        $user = Auth::user();
        $used_coupons = $user->coupons()->wherePivot('used', 1)->get()->toArray();

        if (count($used_coupons) > 0) {//check if user have any used coupon
            foreach ($used_coupons as $used_coupon) {
                if ($used_coupon['id'] == $coupon->id) {
                    return 'Sorry, you have already used this coupon!';
                }
            }
        }

        return response()->json($coupon, 200);
    }

    public function affiliateCoupon()
    {
        $user = Auth::user();
        $affiliate_coupon = $user
                                ->coupons()
                                ->wherePivot('used', 0)
                                ->first();

        return response()->json($affiliate_coupon, 200);
    }
}
