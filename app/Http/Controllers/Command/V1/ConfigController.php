<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Gets all configs from the option table.
     *
     * @return Response
     */
    public function index()
    {
        $configs = DB::table('config')->get();
        $formattedConfigs = [];

        foreach ($configs as $config) {
            $formattedConfig = (object) [
                'type'       => 'configs',
                'id'         => $config->id,
                'attributes' => [
                    $config->key => $config->value,
                ],
            ];

            array_push($formattedConfigs, $formattedConfig);
        }

        return response()->json([
            'data' => $formattedConfigs,
        ]);
    }

    /**
     * Stores config data into options table.
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $globalNotification = $request->input('data.globalNotification');
        $globalStoreStatus = $request->input('data.globalStoreStatus');

        DB::table('config')->where('key', 'globalNotifIcation')->update([
            'value' => json_encode($globalNotification),
        ]);

        DB::table('config')->where('key', 'close_store')->update([
            'value' => json_encode($globalStoreStatus),
        ]);
    }
}
