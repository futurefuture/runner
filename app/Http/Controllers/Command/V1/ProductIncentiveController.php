<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductIncentive as ProductIncentiveResource;
use App\Http\Resources\ProductIncentivesCollection;
use App\Incentive;
use App\ProductIncentive;
use DB;
use Illuminate\Http\Request;

class ProductIncentiveController extends Controller
{
    /**
     * Gets product incentives.
     *
     * @param int $productId
     * @return void
     */
    public function index($productId)
    {
        $productIncentives = ProductIncentive::where('product_id', $productId)->get();

        return new ProductIncentivesCollection($productIncentives);
    }

    /**
     * Created new product incentive.
     *
     * @param Request $request
     * @param int $productId
     * @return void
     */
    public function create(Request $request, $productId)
    {
        $data = $request->input('data.attributes');
        $formattedData = (array) [
            'incentive_id'       => $data['incentiveId'],
            'product_id'         => $productId,
            'start_date'         => $data['startDate'],
            'end_date'           => $data['endDate'],
            'savings'            => $data['savings'],
            'is_active'          => $data['isActive'],
            'reward_points'      => $data['rewardPoints'],
            'minimum_quantity'   => $data['minimumQuantity'],
            'coupon_code'        => $data['couponCode'],
            'minimum_cart_value' => $data['minimumCartValue'],
        ];

        $productIncentive = ProductIncentive::create($formattedData);
    }

    /**
     * Updates product incentive.
     *
     * @param Request $request
     * @param int $productId
     * @param int $tagId
     * @return void
     */
    public function update(Request $request, $productId, $productIncentiveId)
    {
        $data = $request->input('data.attributes');
        $productIncentive = ProductIncentive::find($productIncentiveId)->update($data);
    }

    /**
     * Deletes product incentive.
     *
     * @param int $productId
     * @param int $tagId
     * @return void
     */
    public function delete($productId, $productIncentiveId)
    {
        ProductIncentive::find($productIncentiveId)->delete();
    }
}
