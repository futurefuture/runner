<?php

namespace App\Http\Controllers\Command\V1;

use App\Exports\SegmentExport;
use App\Http\Controllers\Controller;
use App\OrderItem;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;

class SegmentController extends Controller
{
    public function getResult(Request $request)
    {
        // $categoryId = $request->input('categoryId');
        // $category = \App\Category::find($categoryId);
        // $ids = [];
        // $children = $category->children()->pluck('id');

        // $jLohr = [
        //     '13545',
        //     '393',
        //     '467',
        //     '3483',
        //     '19085',
        //     '7226',
        //     '8662',
        //     '11217',
        //     '16023',
        //     '16261',
        //     '17676',
        //     '18018'
        // ];

        $wine = [
            528, 3545, 2927, 6137, 8761, 1046, 3396, 1916, 14405, 7461, 2505, 1411, 8091, 14094, 12711, 1228, 2177, 8760, 12319, 1013, 12934, 433, 2802, 2056, 2946, 8588, 3128, 12990, 3158, 4020, 3768, 12397, 13947, 621, 974, 6870, 239, 2775, 6217, 2774, 12312, 680, 3014, 1991, 3249, 7345, 5896, 13607, 3188, 1284, 3295, 14287, 2492, 3651, 7358, 5177, 6291, 11117, 1158, 664, 16006
        ];

        // foreach ($children as $c) {
        //     array_push($ids, $c);
        //     $cat = \App\Category::find($c);

        //     $childs = $cat->children()->pluck('id');

        //     foreach ($childs as $child) {
        //         array_push($ids, $child);
        //     }
        // }

        // $moreIds = array_merge($ids, [$categoryId]);

        $orderItems = OrderItem::with('user')
            ->whereIn('product_id', $wine)
            ->get();

        $newArray = [];

        foreach ($orderItems->unique('user_id') as $orderItem) {
            if (! is_null($orderItem->user)) {
                $formattedUser = [
                    'email' => strtolower($orderItem->user->email),
                    'phone' => $orderItem->user->phone_number,
                    'fn'    => $orderItem->user->first_name,
                    'ln'    => $orderItem->user->last_name,
                    'dob'   => $orderItem->user->date_of_birth,
                    'uid'   => $orderItem->user->facebook_id ?: null,
                ];

                array_push($newArray, $formattedUser);
            }
        }

        return $newArray;
    }

    public function exportSegmentResults(Request $request)
    {
        // $categoryId = $request->input('categoryId');
        // $category = \App\Category::find($categoryId);
        // $ids = [];
        // $children = $category->children()->pluck('id');

        // foreach ($children as $c) {
        //     array_push($ids, $c);
        //     $cat = \App\Category::find($c);

        //     $childs = $cat->children()->pluck('id');

        //     foreach ($childs as $child) {
        //         array_push($ids, $child);
        //     }
        // }

        // $moreIds = array_merge($ids, [$categoryId]);

        // $jLohr = [
        //     '13545',
        //     '393',
        //     '467',
        //     '3483',
        //     '19085',
        //     '7226',
        //     '8662',
        //     '11217',
        //     '16023',
        //     '16261',
        //     '17676',
        //     '18018'
        // ];

        $wine = [
            528, 3545, 2927, 6137, 8761, 1046, 3396, 1916, 14405, 7461, 2505, 1411, 8091, 14094, 12711, 1228, 2177, 8760, 12319, 1013, 12934, 433, 2802, 2056, 2946, 8588, 3128, 12990, 3158, 4020, 3768, 12397, 13947, 621, 974, 6870, 239, 2775, 6217, 2774, 12312, 680, 3014, 1991, 3249, 7345, 5896, 13607, 3188, 1284, 3295, 14287, 2492, 3651, 7358, 5177, 6291, 11117, 1158, 664, 16006
        ];

        $orderItems = OrderItem::with('user')
                        ->whereIn('product_id', $wine)
                        ->get();

        $newArray = [];

        foreach ($orderItems->unique('user_id') as $orderItem) {
            if (! is_null($orderItem->user)) {
                $formattedUser = [
                    'email' => strtolower($orderItem->user->email),
                    'phone' => $orderItem->user->phone_number,
                    'fn'    => $orderItem->user->first_name,
                    'ln'    => $orderItem->user->last_name,
                    'dob'   => $orderItem->user->date_of_birth,
                    'uid'   => $orderItem->user->facebook_id ?: null,
                ];

                array_push($newArray, $formattedUser);
            }
        }

        return Excel::download(new SegmentExport($newArray), 'segment.xlsx');
    }
}
