<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\PartnerProduct;
use App\Product;
use Illuminate\Http\Request;

class PartnerProductController extends Controller
{
    /**
     * Creates partner product.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function create(Request $request, $partnerId)
    {
        $data = $request->input('data.attributes');
        $product = Product::find($data['productId']);

        DB::table('partner_product')->insert([
            'product_id' => $product->id,
            'partner_id' => $partnerId,
            'title'      => $product->title,
        ]);
    }

    public function index($productId)
    {
        $partners = PartnerProduct::where('product_id', $productId)->where('partner_id', $partnerId)->get();
    }
}
