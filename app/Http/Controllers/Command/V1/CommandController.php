<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Order;
use App\RunnerPosition;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    /**
     * Gets dashboard view.
     *
     * @return View
     */
    public function index()
    {
        return view('command.index');
    }

    public function numbers()
    {
        return view('command.numbers');
    }

    /**
     * Gets reports view.
     *
     * @return View
     */
    public function reports()
    {
        return view('command.reports');
    }

    /**
     * Gets segments view.
     *
     * @return View
     */
    public function segments()
    {
        return view('command.segments');
    }

    /**
     * Gets schedule view.
     *
     * @return View
     */
    public function schedule()
    {
        return view('command.schedule');
    }

    /**
     * Gets runner positions view.
     *
     * @return View
     */
    public function runnerPositions()
    {
        return view('command.runner-positions');
    }

    /**
     * Gets job view.
     *
     * @param int $id
     * @return View
     */
    public function product($id)
    {
        return view('command.product', compact('id'));
    }

    /**
     * Gets Employees view.
     *
     * @return View
     */
    public function employees()
    {
        return view('command.employees');
    }

    /**
     * Gets employee view.
     *
     * @param int $id
     * @return View
     */
    public function employee($id)
    {
        return view('command.employee', compact('id'));
    }

    /**
     * Gets invited users view.
     *
     * @return View
     */
    public function invitedUsers()
    {
        return view('command.invited-users');
    }

    /**
     * Gets clients view.
     *
     * @return View
     */
    public function clients()
    {
        return view('command.clients');
    }

    /**
     * Gets client id.
     *
     * @param int $id
     * @return View
     */
    public function client($id)
    {
        return view('command.client', compact('id'));
    }

    /**
     * Gets links view.
     *
     * @return View
     */
    public function links()
    {
        return view('command.links');
    }

    /**
     * Gets customers view.
     *
     * @return View
     */
    public function customers()
    {
        return view('command.customers');
    }

    /**
     * Gets orders view.
     *
     * @return View
     */
    public function ordersIndex()
    {
        return view('command.orders');
    }

    /**
     * Gets featured products.
     *
     * @return View
     */
    public function featuredProducts()
    {
        return view('command.featured-products');
    }

    /**
     * Gets cart starters.
     *
     * @param int $id
     * @return View
     */
    public function cartStarters($id = null)
    {
        if ($id) {
            return view('command.cart-starter', compact('id'));
        } else {
            return view('command.cart-starters');
        }
    }

    /**
     * Gets bundles view.
     *
     * @param int $id
     * @return View
     */
    public function bundles($id = null)
    {
        if ($id) {
            return view('command.bundle', compact('id'));
        } else {
            return view('command.bundles');
        }
    }

    /**
     * Gets custom products view.
     *
     * @param int $id
     * @return View
     */
    public function customProducts($id = null)
    {
        if ($id) {
            return view('command.custom-product', compact('id'));
        } else {
            return view('command.custom-products');
        }
    }

    /**
     * Gets reviews view.
     *
     * @return View
     */
    public function reviews()
    {
        return view('command.reviews');
    }

    public function getEmployees()
    {
        $employees = User::where('role', '<>', 3)->get();

        return $employees;
    }

    public function getEmployee($id, Request $request)
    {
        $employee = User::where('role', '<>', 3)->where('id', $id)->first();

        if ($request->filled('time')) {
            $timeRange    = $request->input('time');
            $timeRange[0] = Carbon::parse($timeRange[0]);
            $timeRange[0]->setTimezone('America/Toronto');
            $timeRange[0]->hour   = 0;
            $timeRange[0]->minute = 0;
            $timeRange[0]->second = 0;
            $timeRange[1]         = Carbon::parse($timeRange[1]);
            $timeRange[1]->setTimezone('America/Toronto');
            $timeRange[1]->hour   = 0;
            $timeRange[1]->minute = 0;
            $timeRange[1]->second = 0;

            $orders = Order::where('status', 3)
                    ->where(function ($query) use ($employee) {
                        $query->where('runner_1', $employee->id)
                            ->orWhere('runner_2', $employee->id);
                    })
                    ->whereBetween('delivered_at', [$timeRange[0], $timeRange[1]])
                    ->orderBy('delivered_at', 'DESC')
                    ->get();

            $schedules = $employee->schedules()->where('start', '>=', $timeRange[0])->where('end', '<=', $timeRange[1])->get();
        } else {
            $orders = Order::where('status', 3)
                    ->where(function ($query) use ($employee) {
                        $query->where('runner_1', $employee->id)
                            ->orWhere('runner_2', $employee->id);
                    })
                    ->orderBy('id', 'DESC')
                    ->get();

            $schedules = $employee->schedules()->get();
        }

        $totalTip  = 0;
        $totalEarn = 0;

        $formattedOrders    = [];
        $formattedSchedules = [];

        foreach ($schedules as $schedule) {
            $hours = Carbon::parse($schedule->start)->diffInMinutes(Carbon::parse($schedule->end)) / 60;
            $earn  = RunnerPosition::find($schedule->job_type)->rate * $hours;
            $totalEarn += $earn;
            array_push($formattedSchedules, [
                'id'    => $schedule->id,
                'start' => $schedule->start,
                'end'   => $schedule->end,
                'hours' => $hours,
                'job'   => RunnerPosition::find($schedule->job_type)->title,
                'earn'  => $earn,
            ]);
        }

        foreach ($orders as $order) {
            $order->content = json_decode($order->content);

            if ($order->runner_1 && $order->runner_2) {
                $tip = round($order->tip * 0.971, 2) / 2;
            } else {
                $tip = round($order->tip * 0.971, 2);
            }
            $totalTip += $tip;

            array_push($formattedOrders, [
                'id'           => $order->id,
                'delivered_at' => Carbon::parse($order->delivered_at)->format('M j, Y H:i'),
                'tip'          => $tip,
            ]);
        }

        $employee->orders   = $formattedOrders;
        $employee->totalTip = $totalTip;

        $employee->schedules = $formattedSchedules;
        $employee->totalEarn = $totalEarn;

        return $employee;
    }
}
