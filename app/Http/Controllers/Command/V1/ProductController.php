<?php

namespace App\Http\Controllers\Command\V1;

use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductsCollection;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Gets all products.
     *
     * @param string $query
     * @return array
     */
    public function index()
    {
        $products = Product::paginate(50);

        return new ProductsCollection($products);
    }

    /**
     * Gets all products based upon search query.
     *
     * @param string $query
     * @return array
     */
    public function search($query)
    {
        $products = Product::where('title', 'LIKE', '%' . $query . '%')
            ->limit(10)
            ->toArray();

        return new ProductsCollection($products);
    }

    /**
     * Gets product.
     *
     * @param string $query
     * @return array
     */
    public function show($productId)
    {
        $product = Product::find($productId);

        return new ProductResource($product);
    }

    /**
     * Updates product.
     *
     * @param Request $request
     * @param int $productId
     * @return void
     */
    public function update(Request $request, $productId)
    {
        $data = $request->input('data.attributes');

        $utilities     = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);

        $product = Product::find($productId)->update($formattedData);
    }
}
