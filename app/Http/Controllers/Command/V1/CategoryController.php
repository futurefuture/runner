<?php

namespace App\Http\Controllers\Command\V1;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriesCollection;
use App\Http\Resources\Category as CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Gets all categories.
     *
     * @return CategoryCollection
     */
    public function index()
    {
        $categories = Category::all();

        return new CategoriesCollection($categories);
    }

    /**
     * Gets individual tag.
     *
     * @return void
     */
    public function show($tagId)
    {
        $tag = Tag::find($tagId);

        return new TagResource($tagId);
    }

    /**
     * Creates new tag.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $data = $request->input('data.attributes');

        $tag = Tag::create([
            'title' => $data['title'],
            'slug'  => $data['slug'],
            'type'  => $data['type'],
        ]);
    }

    /**
     * Deletes tag.
     *
     * @param int $tagId
     * @return void
     */
    public function delete($tagId)
    {
        $tag = Tag::find($tagId);

        $tag->delete();
    }
}
