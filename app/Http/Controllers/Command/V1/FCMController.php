<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\User;
use FCM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class FCMController extends Controller
{
    public function store(Request $request)
    {
        $data     = json_decode($request->input('data'));
        $imageUrl = $request->input('imageUrl');

        // $validator = Validator::make(, [
        //     'title'       => 'required',
        //     'description' => 'required',
        //     'ids'         => 'required',
        //     'data'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), 400);
        // }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $optionBuilder->setMutableContent(true);
        $notificationBuilder = new PayloadNotificationBuilder($request->input('title'));
        $notificationBuilder
            ->setTitle($request->input('title'))
            ->setBody($request->input('description'))
            ->setIcon('http://placekitten.com/200/300')
            ->setClickAction('RunnerProductNotification')
            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'data'     => $data,
        ]);
        $dataBuilder->addData([
            'imageUrl' => $imageUrl,
        ]);
        $option       = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data         = $dataBuilder->build();
        $token        = User::findMany($request->input('ids'))->pluck('fcm_token')->toArray();
        $token        = array_filter($token, function ($value) {
            return $value !== null;
        });

        var_dump($data);

        if (count($token) == 0) {
            return response()->json([
                'error' => [
                    'none of selected customers have fcm',
                ],
            ], 400);
        }

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        return response()->json([
            'success'      => [$downstreamResponse->numberSuccess()],
            'failure'      => [$downstreamResponse->numberFailure()],
            'modification' => [$downstreamResponse->numberModification()],
        ]);
    }
}
