<?php

namespace App\Http\Controllers\Command\V1;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Partner;
use App\Product;
use App\ProductFinal;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class OldPartnerController extends Controller
{
    public function createClient(Request $request)
    {
        $data = $request->except(['brands']);

        $brands = $request->input('brands');

        $client = new Client();

        $client->save($data);

        foreach ($brands as $key => $brand) {
            $client->brands()->create([
                'name'  => $brand,
            ]);

            $products = DB::table('products_final')->where('producer_name', $brand)->get();

            foreach ($products as $key => $product) {
                DB::table('partner_product')->insert([
                    'product_id'     => $product->id,
                    'partner_id'     => $client->id,
                    'title'          => $product->name,
                ]);
            }
        }
    }

    public function getClients($id = null)
    {
        if (! $id) {
            return Partner::all();
        }

        $client = Partner::find($id);

        $client->brands = $client->brands;

        return $client;
    }

    public function getClientReport($id, Request $request)
    {
        if ($request->has('time')) {
            $timeRange = $request->input('time');
            $timeRange[0] = Carbon::parse($timeRange[0]);
            $timeRange[0]->setTimezone('America/Toronto');
            $timeRange[0]->hour = 0;
            $timeRange[0]->minute = 0;
            $timeRange[0]->second = 0;
            $timeRange[1] = Carbon::parse($timeRange[1]);
            $timeRange[1]->setTimezone('America/Toronto');
            $timeRange[1]->hour = 0;
            $timeRange[1]->minute = 0;
            $timeRange[1]->second = 0;

            $client = Partner::find($id);

            $order_ids = Order::where('status', 3)
                            ->whereBetween('created_at', [$timeRange[0], $timeRange[1]])
                            ->pluck('id');

            $client_product_ids = array_unique(DB::table('partner_product')
                                    ->where('partner_id', $id)
                                    ->pluck('product_id')
                                    ->toArray());

            //report2

            $total_beer_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->where('primary_category', 'beer')
                                            ->where('secondary_category', '<>', 'cider')
                                            ->get();
            $total_beer_sales = 0;
            $total_cider_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->where('secondary_category', 'cider')
                                            ->get();
            $total_cider_sales = 0;
            $total_cooler_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->where('primary_category', 'like', '%'.'coolers')
                                            ->get();
            $total_cooler_sales = 0;

            foreach ($total_beer_order_items as $key => $item) {
                $total_beer_sales += $item->runner_price * $item->qty;
            }

            foreach ($total_cider_order_items as $key => $item) {
                $total_cider_sales += $item->runner_price * $item->qty;
            }

            foreach ($total_cooler_order_items as $key => $item) {
                $total_cooler_sales += $item->runner_price * $item->qty;
            }

            $client_beer_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->whereIn('item_id', $client_product_ids)
                                            ->where('primary_category', 'beer')
                                            ->where('secondary_category', '<>', 'cider')
                                            ->get();
            $client_beer_sales = 0;
            $client_cider_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->whereIn('item_id', $client_product_ids)
                                            ->where('secondary_category', 'cider')
                                            ->get();
            $client_cider_sales = 0;
            $client_cooler_order_items = OrderItem::whereIn('order_id', $order_ids)
                                            ->whereIn('item_id', $client_product_ids)
                                            ->where('primary_category', 'like', '%'.'coolers')
                                            ->get();
            $client_cooler_sales = 0;

            foreach ($client_beer_order_items as $key => $item) {
                $client_beer_sales += $item->runner_price * $item->qty;
            }
            foreach ($client_cider_order_items as $key => $item) {
                $client_cider_sales += $item->runner_price * $item->qty;
            }

            foreach ($client_cooler_order_items as $key => $item) {
                $client_cooler_sales += $item->runner_price * $item->qty;
            }

            $beer_percentage = $total_beer_sales != 0 ? $client_beer_sales / $total_beer_sales * 100 .'%' : 0;
            $cider_percentage = $total_cider_sales != 0 ? $client_cider_sales / $total_cider_sales * 100 .'%' : 0;
            $cooler_percentage = $total_cooler_sales != 0 ? $client_cooler_sales / $total_cooler_sales * 100 .'%' : 0;

            $report = [
                ['Beer', null],
                ['Total Beer Sales', '$'.round($total_beer_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_beer_sales, 2)],
                ['Share Percentage', round($beer_percentage, 2).'%'],
                ['Cider', null],
                ['Total Cider Sales', '$'.round($total_cider_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_cider_sales, 2)],
                ['Share Percentage', round($cider_percentage, 2).'%'],
                ['Ready To Drink', null],
                ['Total RTD Sales', '$'.round($total_cooler_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_cooler_sales, 2)],
                ['Share Percentage', round($cooler_percentage, 2).'%'],
            ];

            //report 2 done
            $orderItems = OrderItem::whereIn('item_id', $client_product_ids)
                            ->whereIn('order_id', $order_ids)
                            ->get();

            $items = [];

            foreach ($client_product_ids as $client_product_id) {
                $items[$client_product_id] = [
                    'qty'                => 0,
                    'runner_total_price' => 0,
                ];
            }

            foreach ($orderItems as $orderItem) {
                $items[$orderItem->item_id]['qty'] += $orderItem->qty;
                $items[$orderItem->item_id]['runner_total_price'] += $orderItem->runner_price * $orderItem->qty;
            }

            foreach ($items as $key => $item) {
                if ($key == 0 || $item['qty'] == 0) {
                    unset($items[$key]);
                }
            }

            $products_collection = Product::whereIn('id', array_keys($items))->orderBy('name');

            $products = $products_collection->get();

            $products_name = array_unique($products_collection->pluck('name')->toArray());

            $org_products = [];
            $total_sales = 0;

            foreach ($products_name as $name) {
                array_push($org_products, (object) [
                    'brand'                => $name,
                    'sku'                  => null,
                    'name'                 => null,
                    'packaging'            => null,
                    'sold'                 => null,
                    'true_price'           => null,
                    'runner_price'         => null,
                    'total_runner_sales'   => null,
                ]);

                $temps = Product::whereIn('id', array_keys($items))->where('name', '=', $name)->get();

                foreach ($temps as $temp) {
                    $total_sales += round($items[$temp->id]['runner_total_price'], 2);

                    array_push($org_products, (object) [
                        'brand'                => null,
                        'sku'                  => $temp->id,
                        'name'                 => $temp->name,
                        'packaging'            => $temp->package,
                        'sold'                 => $items[$temp->id]['qty'],
                        'true_price'           => round($temp->price_in_cents / 100, 2),
                        'runner_price'         => round($temp->price_in_cents / 100 * 1.1, 2),
                        'total_runner_sales'   => round($items[$temp->id]['runner_total_price'], 2),
                        'orders'               => json_encode(OrderItem::where('item_id', $temp->id)->whereHas('order', function ($q) use ($timeRange) {
                            $q->whereBetween('delivered_at', [$timeRange[0], $timeRange[1]]);
                        })->pluck('order_id')),
                    ]);
                }
            }

            array_push($org_products, (object) [
                'brand'                => 'TOTAL SALES',
                'sku'                  => null,
                'name'                 => null,
                'packaging'            => null,
                'sold'                 => null,
                'true_price'           => null,
                'runner_price'         => null,
                'total_runner_sales'   => round($total_sales, 2),
            ]);

            return [
                'title'         => $client->title,
                'contact_email' => $client->contact_email,
                'products'      => $org_products,
                'report'        => $report,
            ];
        } else {
            $client = Partner::find($id);

            $client_product_ids = array_unique(DB::table('partner_product')->where('partner_id', $id)->pluck('product_id')->toArray());

            //report2

            $total_beer_order_items = OrderItem::where('primary_category', 'beer')->where('secondary_category', '<>', 'cider')->get();
            $total_beer_sales = 0;
            $total_cider_order_items = OrderItem::where('secondary_category', 'cider')->get();
            $total_cider_sales = 0;
            $total_cooler_order_items = OrderItem::where('primary_category', 'like', '%'.'coolers')->get();
            $total_cooler_sales = 0;
            foreach ($total_beer_order_items as $key => $item) {
                $total_beer_sales += $item->runner_price * $item->qty;
            }
            foreach ($total_cider_order_items as $key => $item) {
                $total_cider_sales += $item->runner_price * $item->qty;
            }
            foreach ($total_cooler_order_items as $key => $item) {
                $total_cooler_sales += $item->runner_price * $item->qty;
            }

            $client_beer_order_items = OrderItem::whereIn('item_id', $client_product_ids)->where('primary_category', 'beer')->where('secondary_category', '<>', 'cider')->get();
            $client_beer_sales = 0;
            $client_cider_order_items = OrderItem::whereIn('item_id', $client_product_ids)->where('secondary_category', 'cider')->get();
            $client_cider_sales = 0;
            $client_cooler_order_items = OrderItem::whereIn('item_id', $client_product_ids)->where('primary_category', 'like', '%'.'coolers')->get();

            $client_cooler_sales = 0;
            foreach ($client_beer_order_items as $key => $item) {
                $client_beer_sales += $item->runner_price * $item->qty;
            }
            foreach ($client_cider_order_items as $key => $item) {
                $client_cider_sales += $item->runner_price * $item->qty;
            }
            foreach ($client_cooler_order_items as $key => $item) {
                $client_cooler_sales += $item->runner_price * $item->qty;
            }

            $beer_percentage = $total_beer_sales != 0 ? $client_beer_sales / $total_beer_sales * 100 : 0;
            $cider_percentage = $total_cider_sales != 0 ? $client_cider_sales / $total_cider_sales * 100 : 0;
            $cooler_percentage = $total_cooler_sales != 0 ? $client_cooler_sales / $total_cooler_sales * 100 : 0;

            $report = [
                ['Beer', null],
                ['Total Beer Sales', '$'.round($total_beer_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_beer_sales, 2)],
                ['Share Percentage', round($beer_percentage, 2).'%'],
                ['Cider', null],
                ['Total Cider Sales', '$'.round($total_cider_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_cider_sales, 2)],
                ['Share Percentage', round($cider_percentage, 2).'%'],
                ['Ready To Drink', null],
                ['Total RTD Sales', '$'.round($total_cooler_sales, 2)],
                ['Total Labatt Sales', '$'.round($client_cooler_sales, 2)],
                ['Share Percentage', round($cooler_percentage, 2).'%'],
            ];

            //report 2 done
            $orderItems = OrderItem::whereIn('item_id', $client_product_ids)->get();

            $items = [];

            foreach ($client_product_ids as $client_product_id) {
                $items[$client_product_id] = [
                    'qty'                => 0,
                    'runner_total_price' => 0,
                ];
            }

            foreach ($orderItems as $orderItem) {
                $items[$orderItem->item_id]['qty'] += $orderItem->qty;
                $items[$orderItem->item_id]['runner_total_price'] += $orderItem->runner_price * $orderItem->qty;
            }

            foreach ($items as $key => $item) {
                if ($key == 0 || $item['qty'] == 0) {
                    unset($items[$key]);
                }
            }

            $products_collection = Product::whereIn('id', array_keys($items))->orderBy('name');

            $products = $products_collection->get();

            $products_name = array_unique($products_collection->pluck('name')->toArray());

            $org_products = [];

            $total_sales = 0;

            foreach ($products_name as $name) {
                array_push($org_products, (object) [
                    'brand'                => $name,
                    'sku'                  => null,
                    'name'                 => null,
                    'packaging'            => null,
                    'sold'                 => null,
                    'true_price'           => null,
                    'runner_price'         => null,
                    'total_runner_sales'   => null,
                ]);

                $temps = Product::whereIn('id', array_keys($items))->where('name', '=', $name)->get();

                foreach ($temps as $temp) {
                    $total_sales += round($items[$temp->id]['runner_total_price'], 2);

                    array_push($org_products, (object) [
                        'brand'                => null,
                        'sku'                  => $temp->id,
                        'name'                 => $temp->name,
                        'packaging'            => $temp->package,
                        'sold'                 => $items[$temp->id]['qty'],
                        'true_price'           => round($temp->price_in_cents / 100, 2),
                        'runner_price'         => round($temp->price_in_cents / 100 * 1.1, 2),
                        'total_runner_sales'   => round($items[$temp->id]['runner_total_price'], 2),
                        'orders'               => json_encode(OrderItem::where('item_id', $temp->id)->pluck('order_id')),
                    ]);
                }
            }

            array_push($org_products, (object) [
                'brand'                => 'TOTAL SALES',
                'sku'                  => null,
                'name'                 => null,
                'packaging'            => null,
                'sold'                 => null,
                'true_price'           => null,
                'runner_price'         => null,
                'total_runner_sales'   => round($total_sales, 2),
            ]);

            return [
                'title'         => $client->title,
                'contact_email' => $client->contact_email,
                'products'      => $org_products,
                'report'        => $report,
            ];
        }
    }
}
