<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Incentive as IncentiveResource;
use App\Http\Resources\IncentivesCollection;
use App\Incentive;
use Illuminate\Http\Request;

class IncentiveController extends Controller
{
    /**
     * Gets all tags.
     *
     * @return IncentivesCollection
     */
    public function index()
    {
        $incentives = Incentive::all();

        return new IncentivesCollection($incentives);
    }

    /**
     * Gets individual tag.
     *
     * @return void
     */
    public function show($tagId)
    {
        $tag = Tag::find($tagId);

        return new TagResource($tagId);
    }

    /**
     * Creates new tag.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $data = $request->input('data.attributes');

        $tag = Tag::create([
            'title' => $data['title'],
            'slug'  => $data['slug'],
            'type'  => $data['type'],
        ]);
    }

    /**
     * Deletes tag.
     *
     * @param int $tagId
     * @return void
     */
    public function delete($tagId)
    {
        $tag = Tag::find($tagId);

        $tag->delete();
    }
}
