<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\PartnersCollection;
use App\Partner;

class PartnerController extends Controller
{
    /**
     * Gets all partners.
     *
     * @return PartnerCollection
     */
    public function index()
    {
        $partners = Partner::all();

        return new PartnersCollection($partners);
    }
}
