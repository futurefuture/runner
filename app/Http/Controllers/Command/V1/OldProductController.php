<?php

namespace App\Http\Controllers\Command\V1;

use Analytics;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use App\ProductFinal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class OldProductController extends Controller
{
    public function GetProductSales($id, Request $request)
    {
        if ($request->filled('time')) {
            $timeRange = $request->input('time');
            $timeRange[0] = Carbon::parse($timeRange[0]);
            $timeRange[0]->setTimezone('America/Toronto');
            $timeRange[0]->hour = 0;
            $timeRange[0]->minute = 0;
            $timeRange[0]->second = 0;
            $timeRange[1] = Carbon::parse($timeRange[1]);
            $timeRange[1]->setTimezone('America/Toronto');
            $timeRange[1]->hour = 0;
            $timeRange[1]->minute = 0;
            $timeRange[1]->second = 0;

            $TotalQtySold = OrderItem::where('item_id', $id)->whereHas('order', function ($q) use ($timeRange) {
                $q->whereBetween('created_at', [$timeRange[0], $timeRange[1]]);
            })->sum('qty');

            $orders = Order::where('status', 3)->where('content', 'like', '%'.$id.'%')->whereBetween('created_at', [$timeRange[0], $timeRange[1]])->get(['id', 'customer', 'content'])->toArray();

            $orders = array_map(function ($order) use ($id) {
                $order['customer'] = json_decode($order['customer']);
                $order['content'] = json_decode($order['content']);
                $item = null;
                foreach ($order['content'] as $struct) {
                    if ($struct->id == $id) {
                        $item = $struct;

                        break;
                    }
                }

                return (object) [
                    'id'    => $order['id'],
                    'name'  => $order['customer']->first_name.' '.$order['customer']->last_name,
                    'email' => $order['customer']->email,
                    'sold'  => $item->qty,
                ];
            }, $orders);

            $PriceDuringPeriod = OrderItem::where('item_id', $id)->whereHas('order', function ($q) use ($timeRange) {
                $q->whereBetween('created_at', [$timeRange[0], $timeRange[1]]);
            })->pluck('runner_price')->toArray();

            $slug = ProductFinal::find($id)->slug;

            $gAnalytics = Analytics::performQuery(
                Period::create($timeRange[0], $timeRange[1]),
                'ga:pageviews',
                [
                    'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:bounceRate,ga:sessionDuration,ga:avgSessionDuration',
                    'filters' => 'ga:pagePath=@'.$slug,
                ]
            );
        } else {
            $TotalQtySold = OrderItem::where('item_id', $id)->sum('qty');

            $orders = Order::where('status', 3)->where('content', 'like', '%'.$id.'%')->get(['id', 'customer', 'content'])->toArray();

            $orders = array_map(function ($order) use ($id) {
                $order['customer'] = json_decode($order['customer']);
                $order['content'] = json_decode($order['content']);
                $item = null;
                foreach ($order['content'] as $struct) {
                    if ($struct->id == $id) {
                        $item = $struct;

                        break;
                    }
                }

                return (object) [
                    'id'    => $order['id'],
                    'name'  => $order['customer']->first_name.' '.$order['customer']->last_name,
                    'email' => $order['customer']->email,
                    'sold'  => $item->qty,
                ];
            }, $orders);

            $PriceDuringPeriod = OrderItem::where('item_id', $id)->pluck('runner_price')->toArray();

            $slug = Product::find($id)->slug;

            $gAnalytics = Analytics::performQuery(
                Period::years(1),
                'ga:pageviews',
                [
                    'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:bounceRate,ga:sessionDuration,ga:avgSessionDuration',
                    'filters' => 'ga:pagePath=@'.$slug,
                ]
            );
        }

        return [
            'gAnalytics'        => $gAnalytics,
            'TotalQtySold'      => $TotalQtySold,
            'PriceDuringPeriod' => array_unique($PriceDuringPeriod),
            'orders'            => $orders,
        ];
    }

    public function GetTopProducts(Request $request, $category = null)
    {
        if ($request->filled('time')) {
            $timeRange = $request->input('time');
            $timeRange[0] = Carbon::parse($timeRange[0]);
            $timeRange[0]->setTimezone('America/Toronto');
            $timeRange[0]->hour = 0;
            $timeRange[0]->minute = 0;
            $timeRange[0]->second = 0;
            $timeRange[1] = Carbon::parse($timeRange[1]);
            $timeRange[1]->setTimezone('America/Toronto');
            $timeRange[1]->hour = 0;
            $timeRange[1]->minute = 0;
            $timeRange[1]->second = 0;

            if ($category) {
                $items = OrderItem::where('primary_category', $category)->whereHas('order', function ($q) use ($timeRange) {
                    $q->whereBetween('created_at', [$timeRange[0], $timeRange[1]]);
                })->get(['name', 'item_id', 'package', 'primary_category'])->toArray();
            } else {
                $items = OrderItem::whereHas('order', function ($q) use ($timeRange) {
                    $q->whereBetween('created_at', [$timeRange[0], $timeRange[1]]);
                })->get(['name', 'item_id', 'package', 'primary_category'])->toArray();
            }

            $productArr = [];
            foreach ($items as $key => $item) {
                $qty = OrderItem::where('item_id', $item['item_id'])->whereHas('order', function ($q) use ($timeRange) {
                    $q->whereBetween('created_at', [$timeRange[0], $timeRange[1]]);
                })->sum('qty');

                $productArr[$item['item_id']] = (object) [
                    'id'                => $item['item_id'],
                    'name'              => $item['name'],
                    'package'           => $item['package'],
                    'primary_category'  => $item['primary_category'],
                    'qty'               => (int) $qty,
                ];
            }

            usort($productArr, function ($a, $b) {
                return -1 * ($a->qty - $b->qty);
            });

            return array_slice($productArr, 0, 10);
        } else {
            if ($category) {
                $items = OrderItem::where('primary_category', $category)->get(['name', 'item_id', 'package', 'primary_category'])->toArray();
            } else {
                $items = OrderItem::get(['name', 'item_id', 'package', 'primary_category'])->toArray();
            }

            $productArr = [];

            foreach ($items as $key => $item) {
                $qty = OrderItem::where('item_id', $item['item_id'])->sum('qty');

                $productArr[$item['item_id']] = (object) [
                    'id'                => $item['item_id'],
                    'name'              => $item['name'],
                    'package'           => $item['package'],
                    'primary_category'  => $item['primary_category'],
                    'qty'               => (int) $qty,
                ];
            }
            usort($productArr, function ($a, $b) {
                if ($a->qty == $b->qty) {
                    return 0;
                }

                return ($a->qty < $b->qty) ? 1 : -1;
            });

            return array_slice($productArr, 0, 10);
        }
    }
}
