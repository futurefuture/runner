<?php

namespace App\Http\Controllers\Command\V1;

use App\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function index()
    {
        return view('dispatch.options');
    }

    public function getOptions()
    {
        $options = Config::all();

        foreach ($options as $option) {
            if ($option->key != 'phone_number') {
                $option->value = json_decode($option->value);
            }
        }

        return $options;
    }

    public function getPhoneNumber()
    {
        $phoneNumber = preg_replace('/^(\d{3})(\d{3})(\d{4})$/', '$1-$2-$3', Config::where('key', 'phone_number')->first());

        if (isset($value)) {
            return response()->json([
                'phone_number' => $value['value'],
            ]);
        } else {
            return response()->json([
                'phone_number' => '647 588 0722',
            ]);
        }
    }

    public function updatePhoneNumber(Request $request)
    {
        $phoneNumber = preg_replace('/\D+/', '', $request['phone_number']);

        if (Config::where('key', 'phone_number')->first()->update(
            ['value' => $phoneNumber]
        ));

        return response()->json([
            'updated' => true,
        ]);
    }

    public function getHours()
    {
        $array = Config::where('key', 'like', '%schedule%')->get();
        $days  = [];

        foreach ($array as $day) {
            $arr               = explode(',', $day['value'], 2);
            $store_open        = str_replace('[', '', $arr[0]);
            $store_close       = str_replace(']', '', $arr[1]);
            $days[$day['key']] = [
                'open'  => (int) $store_open,
                'close' => (int) $store_close,
            ];
        }

        return response()->json($days);
    }

    public function updateHours(Request $request)
    {
        $data = $request->all();

        Config::where('key', 'monday_schedule')->first()->update(
            ['value' => $data['monday_schedule']]
        );
        Config::where('key', 'tuesday_schedule')->first()->update(
            ['value' => $data['tuesday_schedule']]
        );
        Config::where('key', 'wednesday_schedule')->first()->update(
            ['value' => $data['wednesday_schedule']]
        );
        Config::where('key', 'thursday_schedule')->first()->update(
            ['value' => $data['thursday_schedule']]
        );
        Config::where('key', 'friday_schedule')->first()->update(
            ['value' => $data['friday_schedule']]
        );
        Config::where('key', 'saturday_schedule')->first()->update(
            ['value' => $data['saturday_schedule']]
        );
        Config::where('key', 'sunday_schedule')->first()->update(
            ['value' => $data['sunday_schedule']]
        );

        return response()->json([
            'updated' => true,
        ]);
    }

    public function getStoreStatus()
    {
        $value = Config::where('key', 'close_store')->first();

        return response()->json([
            'close_store' => $value['value'],
        ]);
    }

    public function updateStoreStatus(Request $request)
    {
        Config::where('key', 'close_store')->first()->update(
            ['value' => $request['close_store']]
        );

        return response()->json([
            'updated' => true,
        ]);
    }

    public function getCategoriesOrder()
    {
        if (Config::where('key', 'categories_order')->count() == 0) {
            Config::insert([
                'key'   => 'categories_order',
                'value' => '["Beer","Champagne","Coolers","Rose","Wine","Spirits"]',
            ]);
        }

        $order = Config::where('key', 'categories_order')->first()->value;

        return json_decode($order);
    }

    public function updateCategoriesOrder(Request $request)
    {
        if (Config::where('key', 'categories_order')->count() == 0) {
            Config::insert([
                'key'   => 'categories_order',
                'value' => '["beer","champagne","coolers","rose","wine","spirits","cider"]',
            ]);
        }

        if ($request->filled('order')) {
            Config::where('key', 'categories_order')->first()->update(
                ['value' => json_encode($request->input('order'))]
            );
        }

        $order = Config::where('key', 'categories_order')->first()->value;

        return json_decode($order);
    }
}
