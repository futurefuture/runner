<?php

namespace App\Http\Controllers\Command\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tag as TagResource;
use App\Http\Resources\TagsCollection;
use App\Tag;
use DB;
use Illuminate\Http\Request;

class ProductTagController extends Controller
{
    /**
     * Gets product tags.
     *
     * @param int $productId
     * @param int $tagId
     * @return void
     */
    public function index($productId)
    {
        $productTags = DB::table('product_tag')->where('product_id', $productId)->get();
        $formattedProductTags = [];

        foreach ($productTags as $p) {
            $tag = Tag::find($p->tag_id);

            $formattedProductTag = (object) [
                'type'       => 'product tags',
                'id'         => $tag->id,
                'attributes' => [
                    'title' => $tag->title,
                ],
            ];

            array_push($formattedProductTags, $formattedProductTag);
        }

        return response()->json([
            'data' => $formattedProductTags,
        ]);
    }

    /**
     * Creates product tag.
     *
     * @param int $productId
     * @param int $tagId
     * @return void
     */
    public function create($productId, $tagId)
    {
        DB::table('product_tag')->insert([
            'product_id' => $productId,
            'tag_id'     => $tagId,
        ]);
    }

    /**
     * Updates product.
     *
     * @param Request $request
     * @param int $productId
     * @return void
     */
    public function update(Request $request, $productId, $tagId)
    {
        $product_tag = DB::table('product_tag')
            ->where('product_id', $productId)
            ->where('tag_id', $tagId);
        $data = $request->input('data.attributes');

        $product_tag->update(['index' => $data['index']]);
    }

    /**
     * Deletes product tag.
     *
     * @param int $productId
     * @param int $tagId
     * @return void
     */
    public function delete($productId, $tagId)
    {
        DB::table('product_tag')
            ->where('product_id', $productId)
            ->where('tag_id', $tagId)
            ->delete();
    }
}
