<?php

namespace App\Http\Controllers\Partner\V2;

use App\Ad;
use App\Partner;
use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\Ad as AdResource;
use App\Http\Resources\Partner\AdCollection;
use App\Services\Redshift\RedShiftAdsService;
use Illuminate\Http\Request;
use Auth;
use DB;

class PartnerAdController extends Controller
{
    public function index(Partner $partner)
    {
        $ads = $partner->ads;

        return new AdCollection($ads);
    }

    public function show(Partner $partner, Ad $ad)
    {
        $ad = $partner->ads()
            ->where('id', $ad->id)
            ->first();

        return new AdResource($ad);
    }

    public function delete(Partner $partner, Ad $ad)
    {
        Ad::destroy($ad->id);
    }

    /**
     * Function to return the given campaigns ads information.
     */
    public function getAdsInformation(Request $request, Partner $partner, Campaign $campaign)
    {
        $user    = Auth::user();

        if (!$user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $adsInfo = (new RedShiftAdsService($partner, $campaign, $startDate, $endDate))->getAdsInformation();

        return $adsInfo;
    }


    /**
     *  Function to return the given campaigns ads totals.
     */
    public function getAdsTotalInformation(Request $request, Partner $partner, Campaign $campaign)
    {
        $user    = Auth::user();

        if (!$user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $adsInfo = (new RedShiftAdsService($partner, $campaign, $startDate, $endDate))->getAdsTotalInformation();

        return $adsInfo;
    }
}
