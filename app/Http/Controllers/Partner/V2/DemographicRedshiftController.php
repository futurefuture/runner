<?php

namespace App\Http\Controllers\Partner\V2;

use Analytics;
use App\Http\Controllers\Controller;
use App\Partner;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class DemographicRedshiftController extends Controller
{
    protected $age18 = 0;
    protected $age25 = 0;
    protected $age35 = 0;
    protected $age45 = 0;
    protected $age55 = 0;
    protected $age65 = 0;

    public function index($partnerId, Request $request)
    {
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $this->ids = DB::table('partner_product')->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id');

        $query = 'SELECT SUBSTRING(a.postal_code, 1, 3) AS extractCode,count(Distinct oi.id) AS totalOrderCount FROM order_items AS o INNER JOIN addresses AS a ON a.user_id = o.user_id 
        INNER JOIN orders AS oi ON oi.id = o.order_id WHERE oi.STATUS = 3 AND o.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and a.deleted_at is null and a.selected = 1 and o.product_id In(select product_id from partner_product where partner_id =' . $partnerId . ') GROUP BY extractCode ORDER BY totalOrderCount desc limit 5';
        $topPostalCode = DB::select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'id'         => $partnerId,
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    public function redshiftIndex(Request $request, Partner $partner)
    {
        $request   = $request->all();
        $startDate = Carbon::parse($request['startDate'])->addHour(5);
        $endDate   = Carbon::parse($request['endDate'])->addHour(6);
        $this->ids = DB::table('partner_product')->where('partner_id', $partner->id)
            ->where('product_id', '<>', 0)
            ->pluck('product_id')->toArray();
        $ps       = implode("','", $this->ids);
        $this->ps = "'" . $ps . "'";
        $query    = 'SELECT extractCode, COUNT(DISTINCT order_id) AS totalOrderCount FROM(SELECT SUBSTRING(postal_code, 1, 3) AS extractCode, order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id FROM (SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, order_id, postal_code FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ') WHERE product_id IN(' . $this->ps . ')) GROUP BY extractCode ORDER BY totalOrderCount DESC LIMIT 5';

        $topPostalCode = DB::connection('pgsql')->select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'id'         => $partner->id,
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    /**
     * Returns all analytics data by gender.
     * @param Request $request
     * @param int $partnerId
     * @return json
     */
    public function getSalesByGender($partnerId, Request $request)
    {
        $request   = $request->all();
        $startDate = Carbon::parse($request['startDate']);
        $endDate   = Carbon::parse($request['endDate']);

        $user    = Auth::user();
        $partner = Partner::where('id', $partnerId)->where('user_id', $user->id)->first();
        if ($partner) {
            $gaViewId = $partner->ga_view_id;
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:users',
                [
                    'metrics'     => 'ga:users',
                    'dimensions'  => 'ga:userGender',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    public function getRedshiftSalesByGender(Request $request, Partner $partner)
    {
        $slug = DB::table('partner_product')->where('partner_id', $partner->id)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.slug')->toArray();

        $ps = implode("','", $slug);
        $ps = "'" . $ps . "'";

        $partnerProduct = DB::table('partner_product')->where('partner_id', $partner->id)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.id')->toArray();

        $products = implode("','", $partnerProduct);
        $products = "'" . $products . "'";

        $request   = $request->all();
        $startDate = Carbon::parse($request['startDate'])->addHour(5);
        $endDate   = Carbon::parse($request['endDate'])->addHour(6);

        $maleSale   = 0;
        $femaleSale = 0;

        $genderWebQuery = 'SELECT gender, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ')) WHERE product_id IN(' . $products . ') AND device = 1) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ' GROUP BY anonymous_id, gender HAVING gender IS NOT NULL';

        $genderDbWebQuery = DB::connection('pgsql')->select($genderWebQuery);

        foreach ($genderDbWebQuery as $genderWeb) {
            if ($genderWeb->gender == 'male') {
                $maleSale++;
            } elseif ($genderWeb->gender == 'female') {
                $femaleSale++;
            }
        }

        $genderIosQuery = 'SELECT gender, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id,JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ')) WHERE product_id IN(' . $products . ') AND device = 3) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ' GROUP BY anonymous_id, gender HAVING gender IS NOT NULL';

        $genderDbIosQuery = DB::connection('pgsql')->select($genderIosQuery);

        foreach ($genderDbIosQuery as $genderIos) {
            if ($genderIos->gender == 'male') {
                $maleSale++;
            } elseif ($genderIos->gender == 'female') {
                $femaleSale++;
            }
        }

        return response()->json([
            'data'   => [
                'type'       => 'Gender',
                'id'         => $partner->id,
                'attributes' => [
                    'Male'   => $maleSale,
                    'Female' => $femaleSale,
                ],
            ],
        ]);
    }

    public function getSalesByAge(Request $request, Partner $partner)
    {
        $slug = DB::table('partner_product')->where('partner_id', $partner->id)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.slug')->toArray();

        $ps = implode("','", $slug);
        $ps = "'" . $ps . "'";

        $partnerProduct = DB::table('partner_product')->where('partner_id', $partner->id)
            ->join('products', 'partner_product.product_id', '=', 'products.id')
            ->where('partner_product.product_id', '<>', 0)
            ->pluck('products.id')->toArray();

        $products  = implode("','", $partnerProduct);
        $products  = "'" . $products . "'";
        $request   = $request->all();
        $startDate = Carbon::parse($request['startDate'])->addHour(5);
        $endDate   = Carbon::parse($request['endDate'])->addHour(6);

        $ageWebQuery = 'SELECT age, anonymous_id, COUNT(*) FROM javascript_production.identifies WHERE anonymous_id IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, json_extract_path_text(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ')) WHERE product_id IN(' . $products . ') AND device = 1) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ' GROUP BY anonymous_id, age HAVING age IS NOT NULL';

        $ageWebDbQuery = DB::connection('pgsql')->select($ageWebQuery);

        foreach ($ageWebDbQuery as $webage) {
            $this->ageDistribution($webage->age ?? 0);
        }
        $ageIosQuery = 'SELECT age, LOWER(anonymous_id), COUNT(*) FROM ios_production.identifies WHERE LOWER(anonymous_id) IN(SELECT anonymous_id FROM (SELECT device, anonymous_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price FROM(SELECT device, anonymous_id, order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ')) WHERE product_id IN(' . $products . ') AND device = 3) AND received_at BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ' GROUP BY LOWER(anonymous_id), age HAVING age IS NOT NULL';

        $ageIosDbQuery = DB::connection('pgsql')->select($ageIosQuery);

        foreach ($ageIosDbQuery as $iosage) {
            $this->ageDistribution($iosage->age ?? 0);
        }

        return response()->json([
            'data'   => [
                'type'       => 'demographic age distribution',
                'attributes' => [
                    '18-24' => $this->age18 ?? 0,
                    '25-34' => $this->age25 ?? 0,
                    '35-44' => $this->age35 ?? 0,
                    '45-54' => $this->age45 ?? 0,
                    '55-64' => $this->age55 ?? 0,
                    '65+'   => $this->age65 ?? 0,
                ],
            ],
        ]);
    }

    protected function ageDistribution($age)
    {
        switch ($age) {
            case $age > 18 && $age <= 24:
                $this->age18++;
                break;
            case $age > 24 && $age <= 34:
                $this->age25++;
                break;
            case $age > 35 && $age <= 44:
                $this->age35++;
                break;
            case $age > 45 && $age <= 54:
                $this->age45++;
                break;
            case $age > 54 && $age <= 64:
                $this->age55++;
                break;
            case $age > 65:
                $this->age65++;
                break;
            default:
                $this->age0 = 0;
        }
    }
}
