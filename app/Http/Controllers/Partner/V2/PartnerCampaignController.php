<?php

namespace App\Http\Controllers\Partner\V2;

use App\Partner;
use App\Ad;
use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\CampaignCollection;
use App\Http\Resources\Partner\Campaign as CampaignResource;
use Illuminate\Http\Request;
use DB;

class PartnerCampaignController extends Controller
{
    public function index(Request $request, Partner $partner)
    {
        $campaigns  = $partner->campaigns;
        $partnerId  =  $partner->id;
        $from       = $request['startDate'];
        $to         = $request['endDate'];
        $returnInfo = [];

        foreach ($campaigns as $campaign) {
            $campaignId = $campaign->id;
            $query      = 'SELECT ad_id, sum(ad_clicks) AS ad_clicks, ad_impressions FROM(SELECT jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, to_char(jsac.start_date, \'YYYY-MM-DD HH:MM:SS\') AS start_date, to_char(jsac.end_date, \'YYYY-MM-DD HH:MM:SS\') AS end_date,jsac.index,jsac.bid_amount,count(phpai.uuid) AS ad_impressions FROM(SELECT ad_id,count(uuid) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, index, bid_amount FROM javascript_production.ad_clicked WHERE received_at BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . ' AND campaign_id = ' . $campaignId . ' GROUP BY ad_id, ad_type_id, start_date, end_date, INDEX, bid_amount, carousel_id) AS jsac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id = jsac.ad_id GROUP BY jsac.ad_id, ad_clicks, jsac.carousel_id, jsac.ad_type_id, jsac.start_date, jsac.end_date, jsac.bid_amount, jsac.index UNION SELECT STRTOL(iosac.ad_id,10) AS ad_id, ad_clicks, strtol(iosac.carousel_id,10) AS carousel_id, STRTOL(iosac.ad_type_id, 10) AS ad_type_id, iosac.start_date, iosac.end_date, STRTOL(iosac.index,10) AS INDEX, STRTOL(iosac.bid_amount, 10) AS bid_amount, count(phpai.uuid) AS ad_impressions FROM(SELECT ad_id, count(campaign_id) AS ad_clicks, carousel_id, ad_type_id, start_date, end_date, INDEX, bid_amount FROM(SELECT JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'id\',TRUE) AS ad_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i),\'campaign_id\', TRUE) AS campaign_id , JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'carousel_id\', TRUE) AS carousel_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'ad_type_id\', TRUE) AS ad_type_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'start_date\', TRUE) AS start_date,JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'end_date\', TRUE) AS end_date, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i), \'index\', TRUE) AS INDEX, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(params_ads, seq.i),\'bid_amount\', TRUE) AS bid_amount FROM seq_0_to_20 AS seq, ios_production.ad_clicked WHERE seq.i < JSON_ARRAY_LENGTH(params_ads) AND received_at BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . ' AND campaign_id = ' . $campaignId . ') GROUP BY ad_id, ad_type_id, carousel_id, start_date, end_date, INDEX, bid_amount) AS iosac LEFT JOIN php_production.ad_impression AS phpai ON phpai.ad_id = iosac.ad_id GROUP BY iosac.ad_id, iosac.ad_clicks, iosac.carousel_id, iosac.ad_type_id, iosac.start_date, iosac.end_date, iosac.bid_amount, iosac.index) GROUP BY ad_id, ad_impressions';

            $adInfo     = DB::connection('pgsql')->select($query);

            $formattedAdInfo = [
                'totalAdClicks'         => 0,
                'totalBidAmount'        => 0,
                'totalAdImpressions'    => 0,
                'totalATC'              => 0,
            ];

            $totalATCQuery     = 'SELECT sum(adds_to_cart) FROM(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . ' AND campaign_id = ' . $campaignId . ' AND product_id IS NOT NULL) AS jsai INNER JOIN(SELECT product_id,adds_to_cart FROM(SELECT product_id, COUNT(*) AS adds_to_cart FROM(SELECT JSON_EXTRACT_ARRAY_ELEMENT_TEXT(partner_ids, seq.i) AS partner, product_id, uuid,anonymous_id, user_id, timestamp::date FROM php_production.product_added, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(partner_ids)) WHERE partner = ' . $partnerId . '  AND timestamp::date BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . ' GROUP BY product_id)) AS total_atc ON total_atc.product_id = jsai.product_id';

            $totalATC          = DB::connection('pgsql')->select($totalATCQuery);

            $totalRevenueQuery = 'SELECT SUM(price) FROM(SELECT product_id, price FROM(SELECT JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq2.i),\'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq2.i), \'price\') AS price, timestamp::date FROM php_production.order_completed, seq_0_to_20 AS seq2 WHERE seq2.i < JSON_ARRAY_LENGTH(products)) WHERE timestamp::date BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . ') AS completed_orders INNER JOIN(SELECT DISTINCT product_id FROM javascript_production.ad_impression WHERE received_at BETWEEN ' . '\'' . $from . '\'' . ' AND ' . '\'' . $to . '\'' . '  AND campaign_id = ' . $campaignId . ' AND product_id IS NOT NULL) AS partner_products ON completed_orders.product_id = partner_products.product_id';

            $totalRevenue      = DB::connection('pgsql')->select($totalRevenueQuery);

            if (! empty($adInfo)) {
                foreach ($adInfo as $ad) {
                    $adModel = Ad::where('id', $ad->ad_id)->first()->toArray();

                    if ($adModel['partner_id'] == $partnerId) {
                        $formattedAdInfo['totalAdClicks'] += $ad->ad_clicks ?? 0;
                        $formattedAdInfo['totalBidAmount'] += $adModel ? $adModel['bid_amount'] : 0;
                        $formattedAdInfo['totalAdImpressions'] += $ad->ad_impressions ?? 0;
                    }
                }
            }

            $formattedAdInfo['totalATC']        = $totalATC[0]->sum ?? 0;
            $formattedAdInfo['totalRevenue']    = $totalRevenue[0]->sum / 100;
            $formattedAdInfo['totalBidAmount']  = $formattedAdInfo['totalBidAmount'] / 100;
            $campaign['formattedAdInfo']        = $formattedAdInfo;

            array_push($returnInfo, new CampaignResource($campaign));
        }

        return $returnInfo;
    }

    public function show(Partner $partner, Campaign $campaign)
    {
        $campaign = $partner->campaigns()
            ->where('id', $campaign->id)
            ->first();

        return new CampaignResource($campaign);
    }

    public function delete(Partner $partner, Campaign $campaign)
    {
        Campaign::destroy($campaign->id);
    }
}
