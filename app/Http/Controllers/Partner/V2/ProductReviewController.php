<?php

namespace App\Http\Controllers\Partner\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\ProductReviewCollection;
use App\Partner;
use App\Product;

class ProductReviewController extends Controller
{
    public function index(Partner $partner, Product $product)
    {
        $reviews = $product->reviews;

        return new ProductReviewCollection($reviews);
    }
}
