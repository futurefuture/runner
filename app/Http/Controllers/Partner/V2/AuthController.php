<?php

namespace App\Http\Controllers\Partner\V2;

use Auth;
use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/sales';

    protected $fromUrl;

    public function logout(Request $request)
    {
        $user         = Auth::user();
        $accessTokens = $user->tokens;

        foreach ($accessTokens as $accessToken) {
            $accessToken->revoke();
        }

        return response()->json(
            [
                'message' => 'success',
            ]
        );
    }

    public function login(Request $request)
    {
        $data      = (array) $request->input('data.attributes');
        $validator = Validator::make($data, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors'      => $errors,
            ], 401);
        }

        if (Auth::attempt([
            'email' => $data['email'],
            'password' => $data['password'],
        ])) {
            $user    = Auth::user();
            $partner = Partner::where('user_id', $user->id)->first();

            if ($partner) {
                $accessToken = $user->createToken('Runner Partner')
                    ->accessToken;
            }

            return response()->json([
                'data' => [
                    'type'       => 'Bearer Token',
                    'attributes' => [
                        'accessToken' => $accessToken,
                        'user'        => [
                            'id'        => $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                        'partner' => [
                            'id'    => $partner->id,
                            'title' => $partner->title,
                            'image' => $partner->image,
                        ],
                    ],
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'Invalid Credentials',
                ],
            ], 401);
        }
    }
}
