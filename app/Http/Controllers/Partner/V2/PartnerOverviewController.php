<?php

namespace App\Http\Controllers\Partner\V2;

use App\Http\Controllers\Controller;
use App\Partner;
use App\Services\PartnerOverviewService;
use App\Services\PartnerRedShiftOverviewService;
use App\Services\Redshift\RedshiftService;
use App\Utilities\Filters\OrderItemFilter;
use Carbon\Carbon;
use Auth;
use DB;
use Illuminate\Http\Request;

class PartnerOverviewController extends Controller
{
    /**
     * Returns all product sales for partner.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function index(Request $request, $partnerId, OrderItemFilter $filter)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];

        $overview = (new PartnerOverviewService($partnerId, $filter))->all($startDate, $endDate);

        return $overview;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function redShiftIndex(Request $request, $partnerId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }
        //Authenticate using Service Account Key
        $request      = $request->all();
        $startDate    = $request['startDate'];
        $endDate      = $request['endDate'];
        $productClick = (new PartnerRedShiftOverviewService($partnerId, $startDate, $endDate))->all($startDate, $endDate);

        return $productClick;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalCustomer(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request        = $request->all();
        $startDate      = $request['startDate'];
        $endDate        = $request['endDate'];
        $totalCustomers = (new RedshiftService($partner, $startDate, $endDate))->getTotalCustomers();

        return $totalCustomers;
    }

    /**
     * Function to return new customers for the partner.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getNewCustomers(Request $request, $partnerId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }
        $request        = $request->all();
        $startDate      = Carbon::now()->subDays(2);
        $endDate        = Carbon::now()->subDays(1);
        $totalCustomers = (new RedshiftService($partnerId, $startDate, $endDate))->getTotalNewCustomers();

        return $totalCustomers;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalOrder(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request    = $request->all();
        $startDate  = $request['startDate'];
        $endDate    = $request['endDate'];
        $totalOrder = (new RedshiftService($partner, $startDate, $endDate))->getTotalOrder();

        return $totalOrder;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTopProducts(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request     = $request->all();
        $startDate   = $request['startDate'];
        $endDate     = $request['endDate'];
        $topProducts = (new RedshiftService($partner, $startDate, $endDate))->getTopProducts();

        return $topProducts;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTopProductPages(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request          = $request->all();
        $startDate        = $request['startDate'];
        $endDate          = $request['endDate'];
        $topProductsPages = (new RedshiftService($partner, $startDate, $endDate))->getProductPage();

        return $topProductsPages;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getOrderByDevice(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request       = $request->all();
        $startDate     = $request['startDate'];
        $endDate       = $request['endDate'];
        $orderByDevice = (new RedshiftService($partner, $startDate, $endDate))->getOrdersByDevice();

        return $orderByDevice;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getOrderByHour(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request     = $request->all();
        $startDate   = $request['startDate'];
        $endDate     = $request['endDate'];
        $orderByHour = (new RedshiftService($partner, $startDate, $endDate))->getOrderByHour();

        return $orderByHour;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSessions(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $sessions  = (new RedshiftService($partner, $startDate, $endDate))->getSessions();

        return $sessions;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getAddToCart(Request $request, Partner $partner)
    {
        $user = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $addToCart = (new RedshiftService($partner, $startDate, $endDate))->getAddToCart();

        return $addToCart;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getCheckout(Request $request, $partnerId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);
        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $checkout  = (new RedshiftService($partnerId, $startDate, $endDate))->getCheckout();

        return $checkout;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTransactions(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request      = $request->all();
        $startDate    = $request['startDate'];
        $endDate      = $request['endDate'];
        $transactions = (new RedshiftService($partner, $startDate, $endDate))->getTransactions();

        return $transactions;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getSalesByMedium(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request       = $request->all();
        $startDate     = $request['startDate'];
        $endDate       = $request['endDate'];
        $salesByMedium = (new RedshiftService($partner, $startDate, $endDate))->getSalesByMedium();

        return $salesByMedium;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getSalesByCampaign(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $salesByCampaign = (new RedshiftService($partner, $startDate, $endDate))->getSalesByCampaign();

        return $salesByCampaign;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getSalesByCampaignBackTrack(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $salesByCampaign = (new RedshiftService($partner, $startDate, $endDate))->getSalesByCampaignBackTrack();

        return $salesByCampaign;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSessionBySource(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $sessionBySource = (new RedshiftService($partner, $startDate, $endDate))->getSessionsBySource();

        return $sessionBySource;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalProductImpressions(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }
        $request           = $request->all();
        $startDate         = $request['startDate'];
        $endDate           = $request['endDate'];
        $productImpression = (new RedshiftService($partner, $startDate, $endDate))->getProductImpressions();

        return $productImpression;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductViews(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request      = $request->all();
        $startDate    = $request['startDate'];
        $endDate      = $request['endDate'];
        $productViews = (new RedshiftService($partner, $startDate, $endDate))->getProductViews();

        return $productViews;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalSales(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request    = $request->all();
        $startDate  = $request['startDate'];
        $endDate    = $request['endDate'];
        $totalSales = (new RedshiftService($partner, $startDate, $endDate))->getTotalSales();

        return $totalSales;
    }

    public function show(Request $request, Partner $partner, OrderItemFilter $filter)
    {
        $user      = Auth::user();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];

        $this->ids = DB::table('partner_product')->where('partner_id', $partner->id)
            ->where('product_id', '<>', 0)
            ->pluck('product_id')->toArray();
        $ps       = implode("','", $this->ids);
        $this->ps = "'" . $ps . "'";

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $this->store = $partner->store_id;

        $query = 'SELECT SUM(quantity * retail_price) AS revenue, day, month, year FROM (SELECT order_id, JSON_EXTRACT_PATH_TEXT(product, \'productId\') AS product_id, JSON_EXTRACT_PATH_TEXT(product, \'quantity\') AS quantity, JSON_EXTRACT_PATH_TEXT(product, \'price\') AS retail_price, day, month, year FROM (SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product, EXTRACT(day FROM timestamp) as day, EXTRACT(month FROM timestamp) AS month, EXTRACT(year from timestamp) AS year FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products) AND timestamp::date BETWEEN ' . '\'' . $startDate . '\'' . ' AND ' . '\'' . $endDate . '\'' . ') WHERE product_id IN(' . $this->ps . ')) GROUP BY month, day,year ORDER BY month, day ASC';

        $productSales = DB::connection('pgsql')->select($query);

        return $productSales;
    }
}
