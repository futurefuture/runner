<?php

namespace App\Http\Controllers\Partner\V2;

use Analytics;
use App\Http\Controllers\Controller;
use App\Partner;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class DemographicController extends Controller
{
    public function index($partnerId, Request $request)
    {
        $request = $request->all();
        $startDate = $request['startDate'];
        $endDate = $request['endDate'];
        $this->ids = DB::table('partner_product')->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id');
        $query = 'SELECT SUBSTRING(a.postal_code, 1, 3) AS extractCode,count(Distinct oi.id) AS totalOrderCount 
        FROM order_items AS o
        INNER JOIN addresses AS a ON a.user_id = o.user_id 
        INNER JOIN orders AS oi ON oi.id = o.order_id
        WHERE oi.STATUS = 3 AND o.created_at BETWEEN '." '".$startDate."' ".' and '." '".$endDate."' ".' and a.deleted_at is null and a.selected = 1 and o.product_id In(select product_id from partner_product where partner_id ='.$partnerId.')
        GROUP BY extractCode 
        ORDER BY totalOrderCount desc limit 5';
        $topPostalCode = DB::select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'id'         =>  $partnerId,
                'attributes' => $topPostalCode,
            ],
        ]);
    }

    /**
     * Returns all analytics data by gender.
     * @param Request $request
     * @param int $partnerId
     * @return json
     */
    public function getSalesByGender($partnerId, Request $request)
    {
        $request = $request->all();
        $startDate = Carbon::parse($request['startDate']);
        $endDate = Carbon::parse($request['endDate']);

        $user = Auth::user();
        $partner = Partner::where('id', $partnerId)->where('user_id', $user->id)->first();
        if ($partner) {
            $gaViewId = $partner->ga_view_id;
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:users',
                [
                    'metrics'     => 'ga:users',
                    'dimensions'  => 'ga:userGender',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data by age.
     * @param Request $request
     * @param int $partnerId
     * @return json
     */
    public function getSalesByAge($partnerId, Request $request)
    {
        $request = $request->all();
        $startDate = Carbon::parse($request['startDate']);
        $endDate = Carbon::parse($request['endDate']);

        $user = Auth::user();
        $partner = Partner::where('id', $partnerId)->where('user_id', $user->id)->first();
        if ($partner) {
            $gaViewId = $partner->ga_view_id;
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:users',
                [
                    'metrics'     => 'ga:users',
                    'dimensions'  => 'ga:userAgeBracket',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    public function getAllOrderLocation($partnerId, Request $request)
    {
        $request = $request->all();
        $startDate = Carbon::parse($request['startDate']);
        $endDate = Carbon::parse($request['endDate']);

        $query = 'select a.postal_code,a.lat,a.lng 
        FROM order_items AS o
        INNER JOIN orders AS oi ON oi.id = o.order_id
        INNER JOIN addresses AS a ON a.user_id = o.user_id
        WHERE oi.STATUS = 3 AND o.created_at BETWEEN '." '".$startDate."' ".' and '." '".$endDate."' ".' and a.deleted_at is null and a.selected = 1 and o.product_id In(select product_id from partner_product where partner_id ='.$partnerId.')';

        $addresses = DB::select($query);

        return response()->json([
            'data'   => [
                'type'       => 'demographic postal code',
                'id'         =>  $partnerId,
                'attributes' =>  $addresses,
            ],
        ]);
    }
}
