<?php

namespace App\Http\Controllers\Partner\V2;

use Analytics;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Partner;
use App\PartnerProduct;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;

class AnalyticsController extends Controller
{
    private function validateUserIsPartner($userId, $partnerId)
    {
        return Partner::where('id', $partnerId)->where('user_id', $userId)->exists();
    }

    /**
     * Returns all analytics data for transactions module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getTransactions(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:sessions',
                [
                    'metrics' => 'ga:sessions, ga:transactions, ga:productAddsToCart, ga:productCheckouts',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for sales by medium module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByMedium(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:itemRevenue',
                [
                    'metrics'     => 'ga:itemRevenue',
                    'dimensions'  => 'ga:medium',
                    'sort'        => '-ga:itemRevenue',
                    'max-results' => 5,
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for sales by campaign module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSalesByCampaign(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:itemRevenue',
                [
                    'metrics'     => 'ga:itemRevenue',
                    'dimensions'  => 'ga:campaign',
                    'sort'        => '-ga:itemRevenue',
                    'max-results' => 5,
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for sessions module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSessions(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:sessions',
                [
                    'metrics'    => 'ga:sessions',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for page views module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getPageViews(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:pageviews',
                [
                    'metrics'    => 'ga:pageviews',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for page views module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getUsers(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:users',
                [
                    'metrics'    => 'ga:users',
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for sessions by source module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getSessionsBySource(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:sessions',
                [
                    'metrics'     => 'ga:sessions',
                    'dimensions'  => 'ga:source',
                    'sort'        => '-ga:sessions',
                    'max-results' => 5,
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Returns all analytics data for top pages module.
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function getTopPages(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner  = Partner::find($partnerId);
            $gaViewId = $partner->ga_view_id;

            // set google analytics view
            Analytics::setViewId($gaViewId);

            $analyticsData = Analytics::performQuery(
                Period::create($startDate, $endDate),
                'ga:pageviews',
                [
                    'metrics'     => 'ga:pageviews',
                    'dimensions'  => 'ga:pagePath',
                    'sort'        => '-ga:pageviews',
                    'max-results' => 5,
                ]
            );

            return response()->json($analyticsData);
        }
    }

    /**
     * Gets generic sales data.
     *
     * @param Request $request
     * @param int $partnerId
     * @return response
     */
    public function getSalesData(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner         = Partner::find($partnerId);
            $partnerProducts = PartnerProduct::where('partner_id', $partnerId)->pluck('product_id');

            // total sales data
            $totalSales      = 0;
            $orderItemsQuery = OrderItem::whereIn('product_id', $partnerProducts)
                ->whereBetween('created_at', [$startDate, $endDate]);

            $orderItems = $orderItemsQuery->get();

            foreach ($orderItems as $o) {
                $totalSales += $o->quantity * $o->retail_price;
            }

            // total orders
            $totalUniqueOrders = $orderItemsQuery->distinct('order_id')->count('order_id');

            // average cart size
            $orderIds   = $orderItemsQuery->distinct('order_id')->pluck('order_id');
            $cartTotals = 0;

            foreach ($orderIds as $o) {
                $order = Order::find($o);
                $cartTotals += $order->subtotal;
            }

            // total unique customers
            $totalUniqueCustomers = $orderItemsQuery->distinct('user_id')->count('user_id');

            return response()->json([
                'totalSales'           => $totalSales,
                'totalUniqueOrders'    => $totalUniqueOrders,
                'averageCartSize'      => $cartTotals / $totalUniqueOrders,
                'totalUniqueCustomers' => $totalUniqueCustomers,
            ]);
        }
    }

    public function getTopProducts(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner         = Partner::find($partnerId);
            $partnerProducts = PartnerProduct::where('partner_id', $partnerId)->pluck('product_id');

            // grab product ids from partner product list
            $ids = DB::table('partner_product')
                ->where('partner_id', $partnerId)
                ->where('product_id', '<>', 0)
                ->pluck('product_id');

            // grab partner product ids from product final table
            $orderItems = OrderItem::whereIn('product_id', $ids)->whereBetween('created_at', [$startDate, $endDate])->get()->groupBy('product_id');

            $t = [];

            foreach ($orderItems as $key => $source) {
                $unitsSold = 0;
                $totalSold = 0;
                $x         = [];

                foreach ($source as $item) {
                    $unitsSold += $item->quantity;
                    $totalSold += $item->quantity * $item->retail_price;
                }

                $x['id']          = $source[0]->product_id;
                $x['title']       = $source[0]->title;
                $x['producer']    = $source[0]->producer;
                $x['packaging']   = $source[0]->packaging;
                $x['retailPrice'] = $source[0]->retail_price;
                $x['unitsSold']   = $unitsSold;
                $x['totalSold']   = $totalSold;

                array_push($t, $x);
            }

            $productsWithTotalSold = collect(array_values($t))->sortByDesc('totalSold')->take(5)->toArray();

            return $productsWithTotalSold;
        }
    }

    public function getOrdersByDevice(Request $request, $partnerId)
    {
        $user      = Auth::user();
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partner         = Partner::find($partnerId);
            $partnerProducts = PartnerProduct::where('partner_id', $partnerId)->pluck('product_id');

            $orderIds = OrderItem::whereBetween('created_at', [$startDate, $endDate])
                ->whereIn('product_id', $partnerProducts)
                ->distinct('order_id')
                ->pluck('order_id');

            $iosCount        = 0;
            $webDesktopCount = 0;

            foreach ($orderIds as $o) {
                $device = Order::find($o)->device;

                if ($device == 3) {
                    $iosCount++;
                } else {
                    $webDesktopCount++;
                }
            }

            return response()->json([
                'ios'        => $iosCount,
                'webDesktop' => $webDesktopCount,
            ]);
        }
    }
}
