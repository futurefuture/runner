<?php

namespace App\Http\Controllers\Partner\V2;

use App\AdType;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\AdType as AdTypeResource;
use App\Http\Resources\Partner\AdTypeCollection;

class AdTypeController extends Controller
{
    public function index()
    {
        $adTypes = AdType::all();

        return new AdTypeCollection($adTypes);
    }

    public function show(AdType $adType)
    {
        return new AdTypeResource($adType);
    }
}
