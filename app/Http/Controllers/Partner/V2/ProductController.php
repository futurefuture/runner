<?php

namespace App\Http\Controllers\Partner\V2;

use App\Exports\PartnerProductListExport;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\ProductsCollection;
use App\OrderItem;
use App\Partner;
use App\Product;
use App\Services\ProductAnalyticsService;
use App\Services\ProductRedShiftAnalyticsService;
use App\Services\Redshift\RedShiftProductInfoService;
use App\Services\Redshift\RedshiftProductInfoOptimised;
use App\Utilities\Filters\OrderItemFilter;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    protected $ids = [];

    /**
     * Returns all product sales for partner.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function index(Request $request, $partnerId, OrderItemFilter $filter)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        // grab product ids from partner product list
        $ids = DB::table('partner_product')
            ->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id');

        // grab partner product ids from product final table
        $orderItems = OrderItem::filter($filter)->whereIn('product_id', $ids)
            ->select('product_id', 'title', 'producer', 'packaging', 'retail_price', DB::raw('SUM(quantity) as unitsSold'), DB::raw('SUM(quantity*retail_price) as totalSold'))
            ->groupBy('product_id', 'title', 'producer', 'packaging', 'retail_price')
            ->orderBy('totalSold', 'desc')->paginate(20);

        return new ProductsCollection($orderItems);
    }

    /**
     * Returns all product sales for partner.
     */
    public function redshiftIndex(Request $request, Partner $partner, OrderItemFilter $filter)
    {
        $user      = Auth::user();
        $startDate = $request->input('startDate');
        $endDate   = $request->input('endDate');

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        if ($request->input('query') !== null) {
            $this->ids = DB::table('partner_product')
                ->where('partner_id', $partner->id)
                ->where('product_id', '<>', 0)
                ->pluck('product_id')->toArray();
        } else {
            $this->ids = DB::table('partner_product')
                ->where('partner_id', $partner->id)
                ->where('product_id', '<>', 0)
                ->pluck('product_id')->toArray();
        }

        if ($request->input('query')) {
            $runnerProducts = $this->partnerQuery($startDate, $endDate, $request->input('query'), $this->ids);
        } else {
            $runnerProducts = $this->partnerQuery($startDate, $endDate, null, $this->ids);
        }

        $formattedProduct   = [];
        $allRedshiftProduct = [];

        foreach ($runnerProducts as $p) {
            $product                 = [];
            $product['title']        = $p->name;
            $product['id']           = $p->product_id;
            $product['producer']     = $p->producer;
            $product['packaging']    = $p->packaging;
            $product['retail_price'] = $p->retail_price;
            $product['unitsSolds']   = $p->unitsold;
            $product['totalSold']    = $p->totalsold;

            array_push($formattedProduct, $product);
            array_push($allRedshiftProduct, $p->product_id);
        }

        if ($request->input('query') != null) {
            $newProduct = Product::where('title', 'like', '%' . $request->input('query') . '%')
                ->whereIn('id', $this->ids)
                ->get();

            foreach ($newProduct as $p) {
                $product                 = [];
                $product['title']        = $p->title;
                $product['id']           = $p->id;
                $product['producer']     = $p->producer;
                $product['packaging']    = $p->packaging;
                $product['retail_price'] = $p->retail_price;
                $product['unitsSolds']   = 0;
                $product['totalSold']    = 0;

                if (in_array($p->id, $allRedshiftProduct)) {
                } else {
                    array_push($formattedProduct, $product);
                }
            }
        } else {
            foreach ($this->ids as $partnerProduct) {
                $newProduct              = Product::find($partnerProduct);
                $product                 = [];
                $product['title']        = $newProduct->title;
                $product['id']           = $newProduct->id;
                $product['producer']     = $newProduct->producer;
                $product['packaging']    = $newProduct->packaging;
                $product['retail_price'] = $newProduct->retail_price;
                $product['unitsSolds']   = 0;
                $product['totalSold']    = 0;

                if (in_array($newProduct->id, $allRedshiftProduct)) {
                } else {
                    array_push($formattedProduct, $product);
                }
            }
        }

        if ($request->input('sort')) {
            $length = strlen($request->input('sort')) + 1;
            $sign   = substr($request->input('sort'), 0, 1);
            $start  = 0;

            if (in_array($sign, ['-', '+'])) {
                $start = 1;
            }

            $sortBy                = substr($request->input('sort'), $start, $length);
            $column                = array_column($formattedProduct, $sortBy);
            $productsWithTotalSold = in_array($sign, ['-', '+']) ? array_multisort($column, SORT_DESC, $formattedProduct) : array_multisort($column, SORT_ASC, $formattedProduct);
        } else {
            $price = array_column($formattedProduct, 'totalSold');

            $productsWithTotalSold = array_multisort($price, SORT_DESC, $formattedProduct);
        }

        $formattedProduct   = collect($formattedProduct);
        $currentPage        = LengthAwarePaginator::resolveCurrentPage();
        $perPage            = 50;
        $currentPageResults = $formattedProduct->slice(($currentPage - 1) * $perPage, $perPage)->toArray();
        $data               = new LengthAwarePaginator($currentPageResults, count($formattedProduct), $perPage);

        return $data;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function redShiftShow(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request      = $request->all();
        $startDate    = $request['startDate'];
        $endDate      = $request['endDate'];
        $productClick = (new ProductRedShiftAnalyticsService($partnerId, $productId))->getProductAnalytics($startDate, $endDate);

        return $productClick;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function show(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request      = $request->all();
        $startDate    = $request['startDate'];
        $endDate      = $request['endDate'];
        $productClick = (new ProductAnalyticsService($partnerId, $productId))->getProductAnalytics($startDate, $endDate);

        return $productClick;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductRevenue(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request     = $request->all();
        $startDate   = $request['startDate'];
        $endDate     = $request['endDate'];
        $totalReveue = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getProductRevenue();

        return $totalReveue;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductBroadRank(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $broadRank = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getBroadRank();

        return $broadRank;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductSubRank(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $subRank   = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSubRank();

        return $subRank;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalPurchases(Request $request, Partner $partner, Product $product)
    {
        $user = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getTotalPurchases();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalQuantity(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getTotalQuantity();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getStorePurchases(Request $request, $partnerId, $productId)
    {
        $user = Auth::user();

        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getStorePurchases();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getStoreQuantity(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getStoreQuantity();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getStoreFavourite(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getStoreFavourite();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getStoreRevenue(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getStoreRevenue();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalPageViews(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partner, $product, $startDate, $endDate))->getTotalPageViews();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalUniquePageViews(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partner, $product, $startDate, $endDate))->getTotalUniquePageViews();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTotalAddToCart(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $purchases = (new RedshiftProductInfoOptimised($partner, $product, $startDate, $endDate))->getTotalAddToCart();

        return $purchases;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSalesByMedium(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request       = $request->all();
        $startDate     = $request['startDate'];
        $endDate       = $request['endDate'];
        $salesByMedium = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSalesByMedium();

        return $salesByMedium;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductAnalytics(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $analytics = (new RedshiftProductInfoOptimised($partner, $product, $startDate, $endDate))->getWebPageViews();

        return $analytics;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getPageViews(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $analytics = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getPageViews();

        return $analytics;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getUniquePageViews(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $analytics = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getUniquePageViews();

        return $analytics;
    }

    /**
     * Function to return no of clicks from mobile and web.
     * @param int $id
     * @param int $partnerId
     * @param Request $request
     * @return clicks
     */
    public function getAddToCart(Request $request, $partnerId, $productId)
    {
        $user    = Auth::user();
        $partner = Partner::find($partnerId);

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        //Authenticate using Service Account Key
        $request   = $request->all();
        $startDate = $request['startDate'];
        $endDate   = $request['endDate'];
        $analytics = (new RedshiftProductInfoOptimised($partnerId, $productId, $startDate, $endDate))->getAddToCart();

        return $analytics;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSalesByCampaign(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $salesByCampaign = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSalesByCampaign();

        return $salesByCampaign;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSalesByCampaignBackTrack(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect()->route('webPartnerRoute');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $salesByCampaign = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSalesByCampaignBackTrack();

        return $salesByCampaign;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getTopPostalCode(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request     = $request->all();
        $startDate   = $request['startDate'];
        $endDate     = $request['endDate'];
        $topProducts = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getTopPostalCode();

        return $topProducts;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSalesByGender(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request       = $request->all();
        $startDate     = $request['startDate'];
        $endDate       = $request['endDate'];
        $salesByGender = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSalesByGender();

        return $salesByGender;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSalesByAge(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request    = $request->all();
        $startDate  = $request['startDate'];
        $endDate    = $request['endDate'];
        $salesByAge = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSalesByAge();

        return $salesByAge;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getSessionBySource(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request         = $request->all();
        $startDate       = $request['startDate'];
        $endDate         = $request['endDate'];
        $sessionBySource = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getSessionsBySource();

        return $sessionBySource;
    }

    /**
     * Function to return no of clicks from mobile and web.
     */
    public function getProductImpressions(Request $request, Partner $partner, Product $product)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $request     = $request->all();
        $startDate   = $request['startDate'];
        $endDate     = $request['endDate'];
        $impressions = (new RedShiftProductInfoService($partner, $product, $startDate, $endDate))->getProductImpressions();

        return $impressions;
    }

    /**
     * Exports csv of all product sales for partner.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function exportSales(Request $request, $partnerId, OrderItemFilter $filter)
    {
        //dd('here');
        $user    = Auth::user();
        $partner = Partner::find($partnerId);
        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }
        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        // grab product ids from partner product list
        $ids = DB::table('partner_product')
            ->where('partner_id', $partnerId)
            ->where('product_id', '<>', 0)
            ->pluck('product_id');

        // grab partner product ids from product final table
        $orderItems = OrderItem::filter($filter)->whereIn('product_id', $ids)
            ->select('product_id', 'title', 'producer', 'packaging', 'retail_price', DB::raw('SUM(quantity) as unitsSold'), DB::raw('SUM(quantity*retail_price) as totalSold'))
            ->groupBy('product_id', 'title', 'producer', 'packaging', 'retail_price')
            ->orderBy('totalSold', 'desc')->paginate(20);

        $productsWithTotalSold = new ProductsCollection($orderItems);

        return Excel::download(new PartnerProductListExport($productsWithTotalSold), 'products-sales-list-' . $startDate . '-' . $endDate . '.xlsx');
    }

    /**
     * Exports csv of all product sales for partner.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function exportSalesRedshift(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $startDate = Carbon::parse($request->startDate);
        $endDate   = Carbon::parse($request->endDate);

        if ($request->input('query') !== null) {
            $this->ids = DB::table('partner_product')
                ->where('partner_id', $partner->id)
                ->where('product_id', '<>', 0)
                ->pluck('product_id')->toArray();
        } else {
            $this->ids = DB::table('partner_product')
                ->where('partner_id', $partner->id)
                ->where('product_id', '<>', 0)
                ->pluck('product_id')->toArray();
        }

        if ($request->input('query')) {
            $runnerProducts = $this->partnerQuery($startDate, $endDate, $request->input('query'), $this->ids);
        } else {
            $runnerProducts = $this->partnerQuery($startDate, $endDate, null, $this->ids);
        }

        $formattedProduct   = [];
        $allRedshiftProduct = [];

        foreach ($runnerProducts as $p) {
            $product                 = [];
            $product['title']        = $p->name;
            $product['id']           = $p->product_id;
            $product['producer']     = $p->producer;
            $product['packaging']    = $p->packaging;
            $product['retail_price'] = $p->retail_price;
            $product['unitsSolds']   = $p->unitsold;
            $product['totalSold']    = $p->totalsold;

            array_push($formattedProduct, $product);
            array_push($allRedshiftProduct, $p->product_id);
        }

        if ($request->input('query') != null) {
            $newProduct = Product::where('title', 'like', '%' . $request->input('query') . '%')->whereIn('id', $this->ids)->get();

            foreach ($newProduct as $p) {
                $product                 = [];
                $product['title']        = $p->title;
                $product['id']           = $p->id;
                $product['producer']     = $p->producer;
                $product['packaging']    = $p->packaging;
                $product['retail_price'] = $p->retail_price;
                $product['unitsSolds']   = 0;
                $product['totalSold']    = 0;

                if (in_array($p->id, $allRedshiftProduct)) {
                } else {
                    array_push($formattedProduct, $product);
                }
            }
        } else {
            foreach ($this->ids as $partnerProduct) {
                $newProduct              = Product::find($partnerProduct);
                $product                 = [];
                $product['title']        = $newProduct->title;
                $product['id']           = $newProduct->id;
                $product['producer']     = $newProduct->producer;
                $product['packaging']    = $newProduct->packaging;
                $product['retail_price'] = $newProduct->retail_price;
                $product['unitsSolds']   = 0;
                $product['totalSold']    = 0;

                if (in_array($newProduct->id, $allRedshiftProduct)) {
                } else {
                    array_push($formattedProduct, $product);
                }
            }
        }

        $price = array_column($formattedProduct, 'totalSold');

        $productsWithTotalSold = array_multisort($price, SORT_DESC, $formattedProduct);
        $formattedProduct      = collect($formattedProduct);

        return Excel::download(new PartnerProductListExport($formattedProduct), 'runner-products-sales-list-' . $startDate . '-' . $endDate . '.xlsx');
    }

    public function runnerQuery($startDate, $endDate, $query = null)
    {
        $startDate = Carbon::parse($startDate)->addHour(5);
        $endDate   = Carbon::parse($endDate)->addHour(6);
        if ($query) {
            $query = "Select name,product_id,SUM(quantity) as unitSold,producer,packaging,retail_price,SUM(retail_price * quantity) as totalSold from (SELECT name,product_id,quantity,producer,packaging,retail_price,store_id,received_at from(select json_extract_path_text(product, 'name') as name,json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'producer',true) as producer, json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'packaging',true) as packaging,store_id,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') where name LIKE ' . "'%" . $query . "%'" . ' group by product_id,name,producer,packaging,retail_price';
        } else {
            $query = "Select name,product_id,SUM(quantity) as unitSold,producer,packaging,retail_price,SUM(retail_price * quantity) as totalSold from (SELECT name,product_id,quantity,producer,packaging,retail_price,store_id,received_at from(select json_extract_path_text(product, 'name',true) as name,json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'producer',true) as producer, json_extract_path_text(product, 'price') as retail_price,json_extract_path_text(product, 'packaging',true) as packaging,store_id,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true))) where received_at between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ') group by product_id,name,producer,packaging,retail_price';
        }

        return DB::connection('pgsql')->select($query);
    }

    public function partnerQuery($startDate, $endDate, $query, $ids)
    {
        $startDate = Carbon::parse($startDate)->addHour(5);
        $endDate   = Carbon::parse($endDate)->addHour(6);
        $ps        = implode("','", $ids);
        $ps        = "'" . $ps . "'";

        if ($query) {
            $partnerQuery = "Select name,product_id,SUM(quantity) as unitSold,producer,packaging,retail_price,SUM(retail_price * quantity) as totalSold from (SELECT name,product_id,quantity,producer, packaging,store_id,retail_price,received_at from(select json_extract_path_text(product, 'name',true) as name,json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'producer',true) as producer, json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'packaging',true) as packaging,store_id,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true)))where received_at between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id IN( ' . $ps . ')) where name LIKE ' . "'%" . $query . "%'" . ' group by product_id,name,producer,packaging,retail_price';
        } else {
            $partnerQuery = "Select name,product_id,SUM(quantity) as unitSold,producer,packaging,retail_price,SUM(retail_price * quantity) as totalSold from (SELECT name,product_id,quantity,producer, packaging,store_id,retail_price,received_at from(select json_extract_path_text(product, 'name') as name,json_extract_path_text(product, 'productId',true) as product_id,json_extract_path_text(product, 'quantity',true) as quantity,json_extract_path_text(product, 'producer',true) as producer, json_extract_path_text(product, 'price',true) as retail_price,json_extract_path_text(product, 'packaging',true) as packaging,store_id,received_at from(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i,true) AS product,store_id,received_at FROM php_production.order_completed, seq_0_to_20 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products,true)))where received_at between " . "'" . $startDate . "'" . ' and ' . "'" . $endDate . "'" . ' and product_id IN( ' . $ps . '))  group by product_id,name,producer,packaging,retail_price';
        }

        return DB::connection('pgsql')->select($partnerQuery);
    }
}
