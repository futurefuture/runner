<?php

namespace App\Http\Controllers\Partner\V2;

use App\Exports\PartnerReviewExport;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Product;
use App\User;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;

class PartnerReviewController extends Controller
{
    public function index(Partner $partner, Request $request): LengthAwarePaginator
    {
        $products               = $partner->products;
        $formattedReviews       = [];
        $formattedProductReview = [];

        foreach ($products as $p) {
            $reviewsByDate = $p->reviews;

            foreach ($reviewsByDate as $r) {
                $reviews = [];
                $product = Product::find($r->product_id);

                if ($r->user_id) {
                    $user     = User::find($r->user_id);
                    $userName = $user->first_name . ' ' . substr($user->last_name, 0, 1) . '.';
                }

                $reviews['rating']  = $r->value;
                $reviews['product'] = $product->title;
                $reviews['date']    = $r->created_at->toDateString();
                $reviews['review']  = $r->description;
                $reviews['name']    = $userName;

                array_push($formattedReviews, $reviews);
            }
        }

        if ($request->input('sort')) {
            $length = strlen($request->input('sort')) + 1;
            $sign   = substr($request->input('sort'), 0, 1);
            $start  = 0;

            if (in_array($sign, ['-', '+'])) {
                $start = 1;
            }

            $sortBy       = substr($request->input('sort'), $start, $length);
            $column       = array_column($formattedReviews, $sortBy);
            $reviewBySort = in_array($sign, ['-', '+']) ? array_multisort($column, SORT_DESC, $formattedReviews) : array_multisort($column, SORT_ASC, $formattedReviews);
        } else {
            $rating       = array_column($formattedReviews, 'rating');
            $reviewBySort = array_multisort($rating, SORT_DESC, $formattedReviews);
        }

        $formattedProductReview = collect($formattedReviews);
        $currentPage            = LengthAwarePaginator::resolveCurrentPage();
        $perPage                = 50;
        $currentPageResults     = $formattedProductReview->slice(($currentPage - 1) * $perPage, $perPage)->toArray();
        $data                   = new LengthAwarePaginator($currentPageResults, count($formattedProductReview), $perPage);

        return $data;
    }

    public function reviewExport(Request $request, Partner $partner)
    {
        $products               = $partner->products;
        $formattedReviews       = [];
        $formattedProductReview = [];

        foreach ($products as $p) {
            $reviewsByDate = $p->reviews;

            foreach ($reviewsByDate as $r) {
                $reviews = [];
                $product = Product::find($r->product_id);

                if ($r->user_id) {
                    $user     = User::find($r->user_id);
                    $userName = $user->first_name . ' ' . substr($user->last_name, 0, 1) . '.';
                }

                $reviews['rating']  = $r->value;
                $reviews['product'] = $product->title;
                $reviews['date']    = $r->created_at->toDateString();
                $reviews['review']  = $r->description;
                $reviews['name']    = $userName;

                array_push($formattedReviews, $reviews);
            }
        }

        if ($request->input('sort')) {
            $length = strlen($request->input('sort')) + 1;
            $sign = substr($request->input('sort'), 0, 1);
            $start = 0;

            if (in_array($sign, ['-', '+'])) {
                $start = 1;
            }

            $sortBy       = substr($request->input('sort'), $start, $length);
            $column       = array_column($formattedReviews, $sortBy);
            $reviewBySort = in_array($sign, ['-', '+']) ? array_multisort($column, SORT_DESC, $formattedReviews) : array_multisort($column, SORT_ASC, $formattedReviews);
        } else {
            $rating = array_column($formattedReviews, 'rating');
            $reviewBySort = array_multisort($rating, SORT_DESC, $formattedReviews);
        }

        $formattedProductReview = collect($formattedReviews);

        return Excel::download(new PartnerReviewExport(collect($formattedProductReview)), 'products-reviews-list.xlsx');
    }

    /**
     * Gets the count of new reviews from yesterday.
     *
     * @param Partner $partner
     * @param Request $request
     * @return Number
     */
    public function getNewReviewsCount(Partner $partner, Request $request)
    {
        $startDate  = Carbon::now()->subDays(2);
        $endDate    = Carbon::now()->subDays(1);
        $partnerId  = (int) (Partner::where('user_id', $partner->user_id)->value('id'));
        $query      = 'SELECT * FROM reviews INNER JOIN partner_product ON reviews.product_id=partner_product.product_id WHERE partner_product.partner_id=' . "'" . $partnerId . "'" . ' AND reviews.created_at BETWEEN ' . "'" . $startDate . "'" . ' AND ' . "'" . $endDate . "'" . '';
        $count      = count(DB::select($query));

        return $count;
    }
}
