<?php

namespace App\Http\Controllers\Partner\V2;

use App\Partner;
use App\Campaign;
use App\Http\Controllers\Controller;
use App\Ad;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PartnerCampaignAdController extends Controller
{
    public function index(Request $request, Partner $partner, Campaign $campaign)
    {
        $formattedAds = [];

        $request = $request->all();
        $from = Carbon::parse($request['startDate'])->addHour(5);
        $to   = Carbon::parse($request['endDate'])->addHour(6);

        $ads = Ad::where('campaign_id', '=', $campaign->id)->select('id', 'ad_type_id', 'bid_amount', 'index', 'start_date', 'end_date')->get();

        foreach ($ads as $ad) {

            $impressionsQuery = "select count(*) from php_development.ad_impression where ad_id = " . $ad->id . " and received_at between " . "'" . $from . "'" . " and " . "'" . $to . "'" . "";

            $impression = DB::connection('pgsql')->select($impressionsQuery);

            $ad->impressions = $impression[0]->count;
            $ad->clicks      = $impression[0]->count;

            array_push($formattedAds, $ad);
        }

        return $formattedAds;
    }
}
