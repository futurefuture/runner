<?php

namespace App\Http\Controllers\Partner\V2;

use App\Exports\PartnerCategoryListExport;
use App\Http\Controllers\Controller;
use App\OrderItem;
use App\Partner;
use App\PartnerProduct;
use App\Product;
use App\Services\CategoryService;
use App\Utilities\Filters\OrderItemFilter;
use Auth;
use DB;
use Excel;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CategoryController extends Controller
{
    private function validateUserIsPartner($userId, $partnerId)
    {
        return Partner::where('id', $partnerId)->where('user_id', $userId)->exists();
    }

    /**
     * Returns category percentages for partner.
     *
     * @param Request $request
     * @param int $partnerId
     * @return void
     */
    public function index(Request $request, $partnerId, OrderItemFilter $filter)
    {
        $user = Auth::user();

        if ($this->validateUserIsPartner($user->id, $partnerId)) {
            $partnerProducts = PartnerProduct::where('partner_id', $partnerId)->pluck('product_id');
            $allCategories   = PartnerProduct::select(
                DB::raw('count(partner_product.product_id) as product'),
                'categories.id as productCategoryId',
                'categories.parent_id'
            )
                ->join('products', 'products.id', '=', 'partner_product.product_id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->where('partner_product.partner_id', $partnerId)
                ->groupBy('categories.id')->get()->toArray();

            $formattedParentCategoryIds  = (new CategoryService())->getParentCategory($allCategories);
            $formattedProductCategoryIds = (new CategoryService())->getPartnerProductCategory($allCategories);

            return (new CategoryService())->getFormattedCategory($formattedParentCategoryIds, $formattedProductCategoryIds, $partnerProducts, $filter);
        }
    }

    /**
     * Returns category percentages for partner.
     */
    public function redShiftIndex(Request $request, Partner $partner)
    {
        $user    = Auth::user();

        if (! $user->can('view', $partner)) {
            return redirect('/partner/login');
        }

        $startDate = Carbon::parse($request['startDate'])->addHour(5);
        $endDate   = Carbon::parse($request['endDate'])->addHour(6);

        $arrProduct    = PartnerProduct::where('partner_id', $partner)->pluck('product_id')->toArray();
        $allCategories = PartnerProduct::select(
            DB::raw('count(partner_product.product_id) as product'),
            'categories.id as productCategoryId',
            'categories.parent_id'
        )
            ->join('products', 'products.id', '=', 'partner_product.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->where('partner_product.partner_id', $partner)
            ->groupBy('categories.id')->get()->toArray();

        $formattedParentCategoryIds  = (new CategoryService())->getParentCategory($allCategories);
        $formattedProductCategoryIds = (new CategoryService())->getPartnerProductCategory($allCategories);
        $formattedCategories         = (new CategoryService())->getRedshiftCategoryOptimised($arrProduct, $partner, $startDate, $endDate);

        return response()->json([
            'data' => $formattedCategories,
        ]);
    }

    /**
     * Exports csv of all product sales for partner.
     */
    public function exportSales(Request $request, Partner $partner)
    {
        $user          = Auth::user();
        $startDate     = Carbon::parse($request['startDate'])->addHour(5);
        $endDate       = Carbon::parse($request['endDate'])->addHour(6);
        $arrProduct    = PartnerProduct::where('partner_id', $partner->id)->pluck('product_id')->toArray();
        $allCategories = PartnerProduct::select(
            DB::raw('count(partner_product.product_id) as product'),
            'categories.id as productCategoryId',
            'categories.parent_id'
        )
            ->join('products', 'products.id', '=', 'partner_product.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->where('partner_product.partner_id', $partner->id)
            ->groupBy('categories.id')->get()->toArray();

        $categoryWithTotalSold = (new CategoryService())->getRedshiftCategoryOptimised($arrProduct, $partner, $startDate, $endDate);

        return Excel::download(new PartnerCategoryListExport($categoryWithTotalSold), 'category-sales-list-' . $startDate . '-' . $endDate . '.xlsx');
    }
}
