<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReviewsCollection;
use App\Product;
use App\Review;
use Auth;
use Illuminate\Http\Request;
use Validator;

class ProductReviewController extends Controller
{
    public function create(Request $request, $productId)
    {
        $user = Auth::user();
        $data = $request->input('data');
        $validator = Validator::make($request->input('data.attributes'), [
            'value' => 'required|min:1|numeric',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors' => $errors,
            ], 401);
        }

        $review = new Review([
            'value'          => $data['attributes']['value'],
            'product_id'     => $productId,
            'title'          => $data['attributes']['title'],
            'description'    => $data['attributes']['description'],
        ]);
        $user->reviews()->save($review);
    }

    public function update(Request $request, $productId, $reviewId)
    {
        $data = $request->input('data');
        $validator = Validator::make($request->input('data.attributes'), [
            'helpful' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors' => $errors,
            ], 401);
        }

        $review = Review::find($reviewId);

        if ($data['attributes']['helpful'] === 1) {
            $review->helpful += 1;
            $review->save();
        } elseif ($data['attributes']['helpful'] === 0) {
            $review->unhelpful += 1;
            $review->save();
        }
    }

    public function index($productId)
    {
        $product = Product::find($productId);

        if ($product) {
            $reviews = $product
                ->reviews()
                ->where('is_approved', 1)
                ->whereHas('user')
                ->orderBy('helpful', 'DESC')
                ->orderBy('created_at', 'DESC')
                ->limit(5)
                ->get();

            return new ReviewsCollection($reviews);
        }
    }
}
