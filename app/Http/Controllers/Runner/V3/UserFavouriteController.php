<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductsCollection;
use App\Product;
use Auth;
use DB;

class UserFavouriteController extends Controller
{
    public function index($userId, $inventoryId)
    {
        $user = Auth::user();
        $favourites = DB::table('favourites')->where('user_id', $user->id)
                        ->get();
        $products = [];

        foreach ($favourites as $f) {
            $product = Product::find((int) $f->product_id);

            array_push($products, $product);
        }

        return new ProductsCollection(collect($products));
    }

    public function delete($userId, $inventoryId, $favouriteId)
    {
        $user = Auth::user();

        DB::table('favourites')->where('user_id', $user->id)
            ->where('product_id', $favouriteId)
            ->delete();
    }

    public function create($userId, $inventoryId, $favouriteId)
    {
        $user = Auth::user();

        DB::table('favourites')->insert([
            'user_id'      => $user->id,
            'product_id'   => $favouriteId,
        ]);
    }
}
