<?php

namespace App\Http\Controllers\Runner\V3;

use App\Address;
use App\Cart;
use App\Events\NewOrder;
use App\Gift;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\Invitation;
use App\Mail\OC;
use App\Order;
use App\Product;
use App\Services\NotificationService;
use App\Services\SegmentService;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class PaymentController extends Controller
{
    public function create(Request $request)
    {
        // check if global store is closed
        $globalStoreStatus = DB::table('config')
            ->where('key', 'close_store')
            ->first();

        if ($globalStoreStatus->value == 'true') {
            return response()->json([
                'errors' => [
                    'error' => 'Sorry, but the store is temporarily closed. Please check back later!',
                ],
            ], 500);
        }

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $inventoryId = (int) $request->input('data.attributes.inventoryId');
        $anonymousId = $request->input('data.attributes.anonymousId');
        $device      = (int) $request->input('data.attributes.device');
        $appVersion  = $request->input('data.attributes.appVersion');
        $inventory   = Inventory::find($inventoryId);
        $store       = $inventory->store;
        $user        = User::find($request->input('data.attributes.userId'));
        $address     = Address::where('user_id', $user->id)->where('selected', 1)->first();
        $zipMessage  = 'The Zip Code associated with your card is incorrect. If you aleady have a card added, please delete and re-enter your credit card information.';

        // hack to autoselect adelaide address for user if they dont have address
        if ($request->input('data.attributes.isGift') == true && ! $address) {
            $giftDetails = $request->input('data.attributes.giftDetails');

            $address = (object) [
                'formatted_address' => $giftDetails['address']['formattedAddress'],
                'lat'               => $giftDetails['address']['lat'],
                'lng'               => $giftDetails['address']['lng'],
                'instructions'      => isset($giftDetails['address']['instructions']) ? $giftDetails['address']['instructions'] : '',
                'unitNumber'        => isset($giftDetails['address']['unitNumber']) ? $giftDetails['address']['unitNumber'] : '',
            ];
        } else {
            $address = Address::where('user_id', $user->id)->where('selected', 1)->first();
        }

        if (($user->email == 'timofeisopin@gmail.com' || $user->email == 'jake@wearefuturefuture.com' || $user->email == 'jarek@wearefuturefuture.com' || $user->email == 'dummy@getrunner.io') && env('APP_ENV') == 'production') {
            $chargeAmount = 51;
        }

        if (! $address) {
            return response()->json([
                'errors' => [
                    'error' => 'Please enter an address or make sure that one is selected.',
                ],
            ], 400);
        }

        $cart = Cart::where('user_id', $user->id)
            ->where('inventory_id', $inventoryId)
            ->first();

        if (! $cart) {
            return response()->json([
                'errors' => [
                    'error' => 'It looks like you\'ve changed your address. Please create a new cart based upon your NEW location\'s inventory by going back to the storefront.',
                ],
            ], 400);
        }

        $cartContent    = json_decode($cart->content);
        $cartSubtotal   = (int) $cartContent->subTotal;
        $cartTotal      = (int) $cartContent->total;
        $deliveryFee    = (int) $cartContent->deliveryFee;
        $deliveryFeeTax = (int) $deliveryFee * 0.13;
        $costOfGoods    = (int) $cartSubtotal / 1.12;
        $markup         = (int) ($cartSubtotal - $costOfGoods);
        $markupTax      = (int) $markup * 0.13;
        $tip            = (int) $request->input('data.attributes.tip');
        $serviceFee     = array_key_exists('serviceFee', $cartContent) ? (int) $cartContent->serviceFee : 0;
        $serviceTax     = (int) $serviceFee * 0.13;
        $totalTax       = (int) $markupTax + $deliveryFeeTax + $serviceTax;
        $discount       = (int) $cartContent->discount;
        $discountType   = null;
        $promotions     = [];
        $chargeAmount   = (int) ($cartTotal + $tip);
        $stripeFee      = (int) ($chargeAmount * 0.029 + 30);

        if ($request->input('data.attributes.isGift') == true) {
            $giftDetails = $request->input('data.attributes.giftDetails');

            $validator = Validator::make($giftDetails, [
                'firstName'   => 'required',
                'lastName'    => 'required',
                'email'       => 'required|email',
                'phoneNumber' => 'required',
            ]);

            if ($validator->fails()) {
                $errors = json_decode($validator->errors()->toJson());

                foreach ($errors as $key => $value) {
                    $temp = $value[0];

                    break;
                }

                $errors->error = $temp;

                return response()->json([
                    'errors' => $errors,
                ], 401);
            }
        }

        if (($user->email == 'jake@wearefuturefuture.com' || $user->email == 'jarek@wearefuturefuture.com' || $user->email == 'dummy@getrunner.io') && env('APP_ENV') == 'production') {
            $chargeAmount = 51;
        }

        if ($request->filled('data.attributes.stripeToken')) {
            $stripeToken = $request->input('data.attributes.stripeToken');
        }

        if ($request->filled('data.attributes.token')) {
            $stripeToken = $request->input('data.attributes.token');
        }

        try {
            if (! empty($stripeToken)) {
                $charge = \Stripe\Charge::create([
                    'amount'      => $chargeAmount,
                    'currency'    => 'cad',
                    'source'      => $stripeToken,
                    'description' => 'Order',
                    'metadata'    => [
                        'customer'     => $user->first_name . ' ' . $user->last_name,
                        'email'        => $user->email,
                        'phone'        => $user->phone_number,
                        'address'      => $address->formatted_address,
                        'instructions' => $address->instructions,
                        'time'         => $request->input('data.attributes.orderDateTime'),
                        'total'        => number_format($chargeAmount / 100, 2, '.', ','),
                        'tax'          => number_format($totalTax / 100, 2, '.', ','),
                        'delivery'     => number_format($deliveryFee / 100, 2, '.', ','),
                        'discount'     => number_format($discount / 100, 2, '.', ','),
                        'tip'          => number_format($tip / 100, 2, '.', ','),
                        'subtotal'     => number_format($cartSubtotal / 100, 2, '.', ','),
                        'appVersion'   => $appVersion,
                    ],
                    'capture' => false,
                ]);
            } else {
                $charge = \Stripe\Charge::create([
                    'amount'        => $chargeAmount,
                    'currency'      => 'cad',
                    'customer'      => $user->stripe_id,
                    'description'   => 'Order',
                    'metadata'      => [
                        'customer'     => $user->first_name . ' ' . $user->last_name,
                        'email'        => $user->email,
                        'phone'        => $user->phone_number,
                        'address'      => $address->formatted_address,
                        'instructions' => $address->instructions,
                        'time'         => $request->input('data.attributes.orderDateTime'),
                        'total'        => number_format($chargeAmount / 100, 2, '.', ','),
                        'tax'          => number_format($totalTax / 100, 2, '.', ','),
                        'delivery'     => number_format($deliveryFee / 100, 2, '.', ','),
                        'discount'     => number_format($discount / 100, 2, '.', ','),
                        'tip'          => number_format($tip / 100, 2, '.', ','),
                        'subtotal'     => number_format($cartSubtotal / 100, 2, '.', ','),
                        'appVersion'   => $appVersion,
                    ],
                    'capture' => false,
                ]);
            }
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            if (strpos($err['message'], 'zip') == true) {
                return response()->json([
                    'errors' => [
                        'error' => $zipMessage,
                    ],
                ], 400);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => $err['message'],
                    ],
                ], 400);
            }
        } catch (\Stripe\Error\ApiConnection $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return response()->json([
                'errors' => [
                    'error' => $err['message'],
                ],
            ], 404);
        }

        if ($request->input('data.attributes.orderDateTime' == ':00:00')) {
            $dt = Carbon::now()->addHour();
        } else {
            $dt = Carbon::parse($request->input('data.attributes.orderDateTime'), 'America/Toronto');
        }

        $now = Carbon::now();

        if ($dt->diffInMinutes($now) > 180) {
            $status       = 7;
            $scheduleTime = $dt->toDateTimeString();
        } else {
            $status       = 0;
            $scheduleTime = null;
        }

        $groupedAddress = json_encode((object) [
            'address' => $address->formatted_address,
            'lat'     => (float) $address->lat,
            'lng'     => (float) $address->lng,
        ]);

        if (! $device) {
            $device = 3;
        }

        if (isset($cartContent->incentive)) {
            $incentiveId = $cartContent->incentive->id;
        } else {
            $incentiveId = null;
        }

        foreach ($cartContent->products as $p) {
            $product = Product::find($p->id);
            // reward points
            if ($product->incentives()->where('incentive_id', 7)->pluck('reward_points')->first()) {
                $q = $product->incentives()->where('incentive_id', 7)->first();

                if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                    $rewardPoints = $q->reward_points;
                } else {
                    $rewardPoints = 0;
                }
            } else {
                $rewardPoints = 0;
            }

            if ($rewardPoints > 0) {
                $user->rewardPointsTransaction($rewardPoints, 8, [
                    'productId'       => $rewardPoints,
                    'productTitle'    => $product->title,
                    'productPackaing' => $product->packaging,
                ]);
            }
        }

        $order = Order::create([
            'user_id'        => $user->id,
            'inventory_id'   => $inventoryId,
            'store_id'       => $store->id,
            'status'         => $status,
            'schedule_time'  => $scheduleTime,
            'content'        => json_encode($cartContent->products),
            'card_last_four' => $charge->source->last4,
            'activity_log'   => json_encode([(object) [
                'log'   => 'received',
                'time'  => \Carbon\Carbon::now(),
            ]]),
            'charge'   => $charge->id,
            'device'   => $device,
            'customer' => json_encode((object) [
                'firstName'   => $user->first_name,
                'lastName'    => $user->last_name,
                'phoneNumber' => $user->phone_number,
                'email'       => $user->email,
            ]),
            'address'           => $groupedAddress,
            'unit_number'       => $address->unit_number ?? null,
            'instructions'      => $address->instructions ?? null,
            'coupon_id'         => $discountType === 'coupon' ? $coupon->id : null,
            'subtotal'          => $cartSubtotal,
            'cogs'              => $costOfGoods,
            'discount'          => $discount,
            'delivery'          => $deliveryFee,
            'delivery_tax'      => $deliveryFeeTax,
            'markup'            => $markup,
            'markup_tax'        => $markupTax,
            'display_tax_total' => $totalTax,
            'tip'               => $tip,
            'device'            => $device,
            'total'             => $chargeAmount,
            'service_fee'       => $serviceFee,
            'incentive_id'      => $incentiveId,
            'stripe_fee'        => $stripeFee,
            'first_time'        => $user->orders()->count() ? 0 : 1,
            'app_version'       => $appVersion,
            'address_id'        => $address->id ?? null,
            'postal_code'       => $address->postal_code,
        ]);

        if ($request->input('data.attributes.isGift') == true) {
            $giftDetails = [];

            foreach ($request->input('data.attributes.giftDetails') as $key => $r) {
                $giftDetails[$key] = $r;
            }

            if (array_key_exists('address', $giftDetails)) {
                $address = json_encode((object) [
                    'address'      => $giftDetails['address']['formattedAddress'],
                    'lat'          => $giftDetails['address']['lat'],
                    'lng'          => $giftDetails['address']['lng'],
                ]);
                $unitNumber   = isset($giftDetails['address']['unitNumber']) ? $giftDetails['address']['unitNumber'] : '';
                $instructions = isset($giftDetails['address']['unitNumber']) ? $giftDetails['address']['unitNumber'] : '';
            } else {
                $address      = $groupedAddress;
                $unitNumber   = $address->unit_number ?? null;
                $instructions = $address->instructions ?? null;
            }

            $gift = Gift::create([
                'order_id'     => $order->id,
                'first_name'   => $giftDetails['firstName'],
                'last_name'    => $giftDetails['lastName'],
                'address'      => $address,
                'unit_number'  => $unitNumber,
                'email'        => $giftDetails['email'],
                'instructions' => $instructions,
                'is_19'        => true,
                'phone_number' => $giftDetails['phoneNumber'],
                'message'      => $giftDetails['personalMessage'],
                'gift_options' => null,
            ]);
        }

        if (count($promotions) > 0) {
            $discount = 0;

            foreach ($promotions as $key => $promotion) {
                if ($discount < $promotion['discount']) {
                    $discount     = $promotion['discount'];
                    $discountType = $promotion['name'];
                }
            }

            $total -= $discount;

            try {
                foreach ($promotions as $key => $promotion) {
                    DB::table('promotions_used')->insert([
                        'order_id'      => $order->id,
                        'promotion_id'  => $promotion['id'],
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);
                }
            } catch (Illuminate\Database\QueryException $e) {
                return response()->json([
                    'errors'      => [
                        'error' => 'There is an error for importing order items. Please contact customer support.',
                    ],
                ], 401);
            }
        }

        // check for coupon
        if (isset($cartContent->coupon)) {
            $coupon = \App\Coupon::where('code', $cartContent->coupon->code)->first();

            if ($coupon) {
                $user->coupons()->attach($coupon->id, [
                    'used'     => 1,
                    'order_id' => $order->id,
                ]);
            } elseif ($cartContent->coupon->code === 'Reward Points') {
                if ($user->rewardPointsBalance() < 1000) {
                    return response()->json([
                        'errors'      => [
                            'error' => 'You do not have enough reward points.',
                        ],
                    ], 401);
                } else {
                    $user->rewardPointsTransaction(-1000, 4, [
                        'order_id' => $order->id,
                    ]);
                }
            }
        }

        $invitation = Invitation::where('invited_user_id', $user->id)->first();

        if ($invitation && $user->orders()->count() === 1) {
            $referrer = User::find($invitation->user_id);

            $referrer->rewardPointsTransaction(1000, 5, [
                'invited_id'    => $user->id,
                'invited_name'  => $user->first_name . ' ' . $user->last_name,
                'invited_email' => $user->email,
            ]);

            $notificationService = new NotificationService();
            $notificationService->sendReferrerNotification($order, $referrer);
        }

        if (isset($gift)) {
            $order->gift = $gift;
        }

        Mail::to($user->email)
            ->bcc('service@getrunner.io')
            ->queue(new OC($order));

        $notificationService = new NotificationService();

        try {
            $notificationService->orderPlaced($order);
        } catch (\Exception $e) {
            return response()->json([
                'errors'      => [
                    'error' => $e,
                ],
            ], 401);
        }

        // Add 100 reward points to user.
        $user->rewardPointsTransaction(100, 2, [
            'order_id' => $order->id,
        ]);

        Cart::destroy($cart->id);
        $cart->delete();

        event(new NewOrder($order));

        $formattedProducts = [];
        $quantity          = 0;

        foreach ($cartContent->products as $product) {
            $partnerIds   = [];
            $orderProduct = [];
            $newProduct   = Product::find($product->id);

            foreach ($newProduct->partners as $p) {
                array_push($partnerIds, (string) $p->id);
            }

            $orderProduct['productId']  = $newProduct->id;
            $orderProduct['sku']        = $newProduct->sku;
            $orderProduct['name']       = $newProduct->title;
            $orderProduct['price']      = $newProduct->retail_price;
            $orderProduct['packaging']  = $newProduct->packaging;
            $orderProduct['producer']   = $newProduct->producer;
            $orderProduct['quantity']   = $product->quantity;
            $orderProduct['category']   = $newProduct->category_id;
            $orderProduct['partnerIds'] = $partnerIds;
            $quantity += $product->quantity;

            array_push($formattedProducts, $orderProduct);
        }

        (new SegmentService())->trackOrderCompleted($user, $formattedProducts, $order, $store, $quantity, $device, $anonymousId, $inventoryId);

        return response()->json([
            'data' => [
                'type'       => 'orders',
                'id'         => $order->id,
                'attributes' => $order,
            ],
        ]);
    }
}
