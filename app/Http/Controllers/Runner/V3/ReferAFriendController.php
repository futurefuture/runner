<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Mail\FriendReferred;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mailgun\Mailgun;
use Nexmo\Laravel\Facade\Nexmo;

class ReferAFriendController extends Controller
{
    private function validateEmail($email)
    {
        $mg           = Mailgun::create(env('MAILGUN_SECRET'));
        $mgValidation = $mg->get('address/private/validate', ['address' => $email]);

        if ($mgValidation->http_response_body->is_valid) {
            $parts            = explode('@', $email);
            $domain           = array_pop($parts);
            $notAllowedDomain = ['qq.com'];

            if (in_array($domain, $notAllowedDomain)) {
                return response()->json([
                    'errors' => [
                        'error' => 'Invalid Email',
                    ],
                ], 401);
            }
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'Invalid Email',
                ],
            ], 401);
        }
    }

    public function create(Request $request)
    {
        $user = Auth::user();
        $data = $request->input('data.attributes');

        if ($data['type'] == 'email') {
            // $this->validateEmail($data['contactInfo']);

            if (env('APP_ENV') === 'production') {
                Mail::to($data['contactInfo'])->queue(new FriendReferred($user));
            }
            DB::table('invite_user')->insert([
                    'user_id'       => $user->id,
                    'email_sent_to' => $data['contactInfo'],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now(),
                ]);
        } else {
            $text = 'Hey it\'s ' . $user->first_name . '! I\'ve been using Runner to have alcohol delivered to my door - thought you\'d love it too. Get $10 off: ' . env('APP_URL') . '/register/' . ' !';

            try {
                Nexmo::message()->send([
                        'to'   => '1' . $data['contactInfo'],
                        'from' => '12493155516',
                        'text' => $text,
                    ]);
            } catch (Exception $e) {
                Log::info('Queue job for schedule order reminder doesnt work because of ' . $e->getMessage());
            }

            DB::table('invite_user')->insert([
                    'user_id'              => $user->id,
                    'phone_number_sent_to' => $data['contactInfo'],
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                ]);
        }

        return response()->json([
                'message' => 'success',
            ]);
    }
}
