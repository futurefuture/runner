<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\PostalCode;
use App\Store;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class PostalCodeController extends Controller
{
    /**
     * Gets stores according to postal code.
     *
     * @param Request $request
     * @return StoreCollection
     */
    public function getStores(Request $request)
    {
        $postalCodeInput = explode(' ', $request->input('postalCode'))[0];
        $postalCode = PostalCode::where('postal_code', $postalCodeInput)->first();
        if (! $postalCode) {
            $formattedInventories = [];
        } else {
            $inventories = $postalCode->inventories;
            $formattedInventories = [];

            foreach ($inventories as $inventory) {
                $inventoryIsOpen = (bool) $inventory->is_active;
                $inventoryHours = $inventory->options['hours'];
                $now = Carbon::now();
                $dayOfWeek = $now->dayOfWeek;
                $inventoryOpen = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[0]);
                $inventoryClosed = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[1]);

                if (! $now->between($inventoryOpen, $inventoryClosed, false)) {
                    $inventoryIsOpen = false;
                }

                $store = Store::find($inventory->store_id);

                $formattedInventory = (object) [
                    'type'       => 'stores',
                    'id'         => $store->id,
                    'sortOrder'  => $store->options['sortOrder'] ?? null,
                    'attributes' => [
                        'title'          => $store->title,
                        'vendor'         => $inventory->vendor,
                        'address'        => $inventory->title,
                        'isOpen'         => $inventoryIsOpen,
                        'notification'   => $inventory->options['notification'] ?? null,
                        'storeLogo'      => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                        'inventoryId'    => $inventory->id,
                        'locationId'     => $inventory->location_id,
                        'subDomain'      => $store->sub_domain,
                        'inventoryHours' => $inventoryHours,
                        'sortOrder'      => $store->options['sortOrder'] ?? null,
                        'towerImage'     => $store->options['towerImage'] ?? null,
                        'layout'         => [
                            [
                                'type'       => 'layout patterns',
                                'id'         => 1,
                                'attributes' => [
                                    'sortOrder'     => 0,
                                    'url'           => '/runner/inventories/1/categories/6/products',
                                    'icon'          => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                                    'title'         => 'Featured Spirits',
                                    'description'   => 'Select spirits curated by Runner',
                                    'link'          => [
                                        'title'         => 'Shop Spirits',
                                        'type'          => 'category',
                                        'storeId'       => 1,
                                        'url'           => '/runner/inventories/1/categories/6/products',
                                        'prettyUrl'     => '/category/alcohol/spirits',
                                    ],
                                ],
                            ],
                            [
                                'type'       => 'layout patterns',
                                'id'         => 2,
                                'attributes' => [
                                    'sortOrder'     => 1,
                                    'icon'          => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                                    'title'         => 'Taste the world',
                                    'description'   => 'Pick a destination and experience peculiar tast across the globe',
                                    'link'          => [
                                        'title'         => 'Show all',
                                        'type'          => 'filters',
                                        'storeId'       => 1,
                                        'url'           => '/runner/inventories/11/categories/11/products?filters=taste-the-world',
                                        'prettyUrl'     => '/category/alcohol/spirits?filters=taste-the-world',
                                    ],
                                    'carousel'      => [
                                        [
                                            'sortOrder'     => 0,
                                            'imageDesktop'  => '',
                                            'imageMobile'   => '',
                                            'link'          => [
                                                'title'         => 'Japan',
                                                'type'          => 'filters',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/11/products?filters=japan',
                                                'prettyUrl'     => '/category/alcohol/spirits?filters=japan',
                                            ],
                                        ],
                                        [
                                            'sortOrder'     => 1,
                                            'image'         => '',
                                            'link'          => [
                                                'type'          => 'filters',
                                                'title'         => 'Spain',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/11/products?filters=spain',
                                                'prettyUrl'     => '/category/alcohol/spirits?filters=spain',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'type'       => 'layout patterns',
                                'id'         => 3,
                                'attributes' => [
                                    'sortOrder' => 2,
                                    'carousel'  => [
                                        [
                                            'sortOrder'     => 0,
                                            'imageDesktop'  => '',
                                            'imageMobile'   => '',
                                            'link'          => [
                                                'type'          => 'filters',
                                                'title'         => 'New Canned Wine',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/11/products?filters=new-canned-wine',
                                                'prettyUrl'     => '/category/alcohol/spirits?filters=new-canned-wine',
                                            ],
                                        ],
                                        [
                                            'sortOrder'     => 1,
                                            'imageDesktop'  => '',
                                            'imageMobile'   => '',
                                            'link'          => [
                                                'type'          => 'filters',
                                                'title'         => 'Father\'s Day',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/11/products?filters=fathers-day',
                                                'prettyUrl'     => '/category/alcohol/spirits?filters=fathers-day',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'type'       => 'layout patterns',
                                'id'         => 4,
                                'attributes' => [
                                    'sortOrder' => 3,
                                    'carousel'  => [
                                        [
                                            'sortOrder'     => 0,
                                            'link'          => [
                                                'type'          => 'category',
                                                'title'         => 'Red Wine',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/6',
                                                'prettyUrl'     => '/category/alcohol/wine/red',
                                            ],
                                        ],
                                        [
                                            'sortOrder'     => 1,
                                            'link'          => [
                                                'type'          => 'category',
                                                'title'         => 'White Wine',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/5',
                                                'prettyUrl'     => '/category/alcohol/wine/white',
                                            ],
                                        ],
                                        [
                                            'sortOrder'     => 2,
                                            'link'          => [
                                                'type'          => 'category',
                                                'title'         => 'Rosé',
                                                'storeId'       => 1,
                                                'url'           => '/runner/inventories/11/categories/14',
                                                'prettyUrl'     => '/category/alcohol/wine/rose',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'type'       => 'layout patterns',
                                'id'         => 5,
                                'attributes' => [
                                    'sortOrder'     => 4,
                                    'icon'          => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                                    'title'         => 'Explore Stores',
                                    'description'   => 'Discover quality aged wine',
                                    'link'          => [
                                        'type'          => 'storeFront',
                                        'title'         => 'Shop Now',
                                        'storeId'       => 2,
                                        'url'           => '',
                                        'prettyUrl'     => '/',
                                    ],
                                ],
                            ],
                            [
                                'type'       => 'layout patterns',
                                'id'         => 6,
                                'attributes' => [
                                    'sortOrder'     => 5,
                                    'type'          => 6,
                                    'icon'          => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                                    'title'         => 'Explore Gift Ideas',
                                    'description'   => 'Send your love with Crown Flora Studio',
                                    'imageDesktop'  => '',
                                    'imageMobile'   => '',
                                    'blockBgColor'  => '#393939',
                                    'blockMessage'  => 'Surprise your loved ones with gifts',
                                    'link'          => [
                                        'title'      => 'Shop Now',
                                        'type'       => 'storeFront',
                                        'storeId'    => 4,
                                        'url'        => '',
                                        'prettyUrl'  => '/',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];

                array_push($formattedInventories, $formattedInventory);
            }
        }

        return response()->json([
            'data' => $formattedInventories,
        ]);
    }

    public function storeOutOfBoundsPostalCode(Request $request)
    {
        $data = $request->input('data.attributes');

        DB::table('out_of_bounds_postal_codes')->insert([
            'postal_code' => $data['postalCode'],
            'created_at'  => Carbon::now()->toDateTimeString(),
            'updated_at'  => Carbon::now()->toDateTimeString(),
        ]);

        $outOfBoundsPostalCode = DB::table('out_of_bounds_postal_codes')
            ->where('postal_code', $data['postalCode'])
            ->first();

        return response()->json([
            'data' => [
                'type'       => 'out of bounds postal codes',
                'id'         => $outOfBoundsPostalCode->id,
                'attributes' => [
                    'postalCode' => $outOfBoundsPostalCode->postal_code,
                ],
            ],
        ]);
    }
}
