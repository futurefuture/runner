<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ProductResource;
use App\Product;

class ProductController extends Controller
{
    public function getProduct($productId)
    {
        $product = Product::find($productId);

        return new ProductResource($product);
    }
}
