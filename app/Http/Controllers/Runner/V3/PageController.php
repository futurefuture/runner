<?php

namespace App\Http\Controllers\Runner\V3;

use DB;
use App\Tag;
use App\Services\TagService;
use App\User;
use App\Order;
use App\Store;
use App\Product;
use App\Category;
use App\Inventory;
use App\PostalCode;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use App\Http\Resources\Runner\ProductCollection;
use App\Http\Resources\Runner\User as UserResource;
use App\Actions\InventoryCategoryProduct\GetInventoryCategoryProducts;
use App\Http\Resources\Runner\Review as ReviewResource;
use App\Http\Resources\Runner\Incentive;
use App\Http\Resources\Runner\Partner;
use App\Http\Resources\Runner\Inventory as InventoryResource;

class PageController extends Controller
{
    protected $subDomain = 'www';
    protected $storeId;
    protected $domain;
    protected $storeInfo;

    private function getDomainDetails()
    {
        // set HTTP_HOST for tests as well
        if (isset($_SERVER['HTTP_HOST']) && ! empty($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            $host = 'www.runner.test';
        }

        $sub = explode('.', $host)[0];

        if ($sub != '') {
            $this->subDomain = explode('.', $host)[0];
        }

        // set subdomain if subdomain is not set
        if ($sub == 'runner' || $sub == 'getrunner') {
            $this->subDomain = 'www';
        }

        $this->domain = env('APP_DOMAIN');
        $store        = Store::where('sub_domain', $this->subDomain)->first(['id', 'options', 'title']);

        if (! $store) {
            $store = Store::find(8);
        }

        $this->storeId   = $store->id ?? 8;
        $this->storeInfo = [
            'id'          => $store->id ?? 8,
            'image'       => $store->options['towerImage'] ?? '',
            'title'       => $store->title ?? 'Runner',
            'description' => 'test',
        ];
    }

    public function __construct()
    {
        $this->getDomainDetails();
    }

    public function rewards()
    {
        $domain    = env('APP_DOMAIN');
        $storeInfo = $this->storeInfo;

        return view('runner.rewards', compact([
            'domain',
            'storeInfo',
        ]));
    }

    public function explore(Request $request)
    {
        $domain      = $this->domain;
        $subDomain   = $this->subDomain;
        $store       = Store::find($this->storeId);

        if ($store->id === 8) {
            $store->title = 'Curated Convenience';
        }

        $secret      = null;
        $userId      = null;
        $accessToken = null;
        $categories  = $store->options['categories'] ?? null;

        // check for encrypted user id
        if ($request->input('secret')) {
            $secret = $request->input('secret');
        }

        if ($secret !== null) {
            $user        = User::find(Crypt::decryptString($secret));
            $userId      = $user->id;
            $accessToken = $user->createToken('Runner')
                ->accessToken;
        }

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $this->storeId,
            'secret'      => $secret,
            'userId'      => (string) $userId,
            'accessToken' => $accessToken,
            'categories'  => $categories,
        ]);

        return view('runner.explore', compact([
            'pageAttributes',
        ]));
    }

    public function newAccountSettings(Request $request)
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $store     = Store::find($this->storeId);

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'store'     => $store,
            'storeId'   => (string) $this->storeId,
        ]);

        return view('runner.new-account-settings', compact([
            'pageAttributes',
        ]));
    }

    public function newLogin()
    {
        $domain = $this->domain;

        $pageAttributes = json_encode([
            'domain' => $domain,
        ]);

        return view('runner.new-login', compact([
            'pageAttributes',
        ]));
    }

    public function newRegister(Request $request)
    {
        $domain   = $this->domain;
        $secret   = null;
        $referrer = null;

        // check for encrypted user
        if ($request->input('secret')) {
            $secret = json_encode(decrypt($request->input('secret')));
        }

        if ($request->input('inviteCode')) {
            $inviteCode = $request->input('inviteCode');
            $referrer   = new UserResource(User::where('invite_code', $inviteCode)->first());
        }

        $pageAttributes = json_encode([
            'domain'   => $domain,
            'secret'   => $secret,
            'referrer' => $referrer,
        ]);

        return view('runner.new-register', compact([
            'pageAttributes',
        ]));
    }

    public function checkout()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $storeInfo = $this->storeInfo;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.checkout', compact([
            'pageAttributes',
        ]));
    }

    public function confirmation(Request $request)
    {
        $orderId   = $request->input('orderId');
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $store     = Store::find($storeId);
        $order     = Order::find($orderId);

        $pageAttributes = json_encode([
            'orderId'   => (string) $order->id,
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
            'store'     => $store,
            'userId'    => (string) $order->user_id,
        ]);

        return view('runner.confirmation', compact([
            'pageAttributes',
        ]));
    }

    public function searchResults()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = (string) $this->storeId;
        $storeInfo = $this->storeInfo;

        return view('runner.search-results', compact([
            'domain',
            'subDomain',
            'storeId',
            'storeInfo',
        ]));
    }

    public function about()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.about', compact([
            'pageAttributes',
        ]));
    }

    public function addresses()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.addresses', compact([
            'pageAttributes',
        ]));
    }

    public function paymentMethods()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.payment-methods', compact([
            'pageAttributes',
        ]));
    }

    public function termsConditions()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.terms-conditions', compact([
            'pageAttributes',
        ]));
    }

    public function privacyPolicy()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.privacy-policy', compact([
            'pageAttributes',
        ]));
    }

    public function faq()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.faq', compact([
            'pageAttributes',
        ]));
    }

    public function accountDetails()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $storeInfo = $this->storeInfo;

        return view('runner.account-details', compact([
            'domain',
            'subDomain',
            'storeId',
            'storeInfo',
        ]));
    }

    public function accountSettings()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $storeInfo = $this->storeInfo;

        return view('runner.account-settings', compact([
            'domain',
            'subDomain',
            'storeId',
            'storeInfo',
        ]));
    }

    public function referAFriend()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $storeInfo = $this->storeInfo;

        return view('runner.refer-a-friend', compact([
            'domain',
            'subDomain',
            'storeId',
            'storeInfo',
        ]));
    }

    public function paymentInformation()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;
        $storeInfo = $this->storeInfo;

        return view('runner.payment-information', compact([
            'domain',
            'subDomain',
            'storeId',
            'storeInfo',
        ]));
    }

    public function usual()
    {
        $domain          = $this->domain;
        $subDomain       = $this->subDomain;
        $storeId         = $this->storeId;
        $store           = Store::find($storeId);
        $category        = Category::find(27);
        $categoryService = new CategoryService();
        $breadCrumbs     = json_encode($categoryService->getBreadcrumbs($category));
        $categories      = json_encode($categoryService->categories($category));

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $storeId,
            'category'    => $category,
            'categories'  => $categories,
            'breadCrumbs' => $breadCrumbs,
        ]);

        return view('runner.landing-pages.usual', compact([
            'pageAttributes',
        ]));
    }

    public function pizzaRoulette()
    {
        $domain          = $this->domain;
        $subDomain       = $this->subDomain;
        $storeId         = $this->storeId;
        $store           = Store::find($storeId);
        $category        = Category::find(27);
        $categoryService = new CategoryService();
        $breadCrumbs     = json_encode($categoryService->getBreadcrumbs($category));
        $categories      = json_encode($categoryService->categories($category));

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $storeId,
            'category'    => $category,
            'categories'  => $categories,
            'breadCrumbs' => $breadCrumbs,
        ]);

        return view('runner.landing-pages.pizza-roulette', compact([
            'pageAttributes',
        ]));
    }

    public function product(Request $request, $productSlug)
    {
        $product         = Product::where('slug', $productSlug)->first();
        $domain          = $this->domain;
        $subDomain       = $this->subDomain;
        $store           = Store::find($this->storeId);

        if (! $product) {
            $pageAttributes = json_encode([
                'domain'      => $domain,
                'subDomain'   => $subDomain,
                'store'       => $store,
                'storeId'     => (string) $this->storeId,
            ]);

            return view('errors.404', compact([
                'pageAttributes'
            ]));
        }

        $fullPostalCode  = isset($request->all()['postalCode']) ? $request->all()['postalCode'] : 'M6J1G5';
        $postalCode      = PostalCode::find(substr($fullPostalCode, 0, 3));
        $inventories     = $postalCode->inventories->pluck('id');

        $inventoryQuery  = DB::table('inventories')
            ->join('inventory_territory', 'inventories.id', '=', 'inventory_territory.inventory_id')
            ->join('territories', 'territories.id', '=', 'inventory_territory.territory_id')
            ->select('inventories.*')
            ->where('territories.id', $postalCode->territory->id)
            ->where('inventories.store_id', $store->id);
        $inventoryId     = isset($inventoryQuery->pluck('id')[0]) ? $inventoryQuery->pluck('id')[0] : 1;

        $category        = Category::find($product->category_id);
        $categoryService = new CategoryService();
        $breadCrumbs     = json_encode($categoryService->getBreadcrumbs($category));
        $categories      = json_encode($categoryService->categories($category));

        $images = [];

        array_push($images, [
            'image' => $product->image,
            'index' => 0,
        ]);

        if ($product->productImages) {
            foreach ($product->productImages as $i) {
                array_push($images, [
                    'image' => $i->image,
                    'index' => $i->index,
                ]);
            }
        }

        $requestParams = $request->request->all();
        $ads           = $product
            ->ads()
            ->active()
            ->where('ad_type_id', 1)
            ->get();
        $relatedAds    = [];

        if (array_key_exists('filter', $requestParams)) {
            $filterInfo = $request->request->all()['filter'];

            if (array_key_exists('tags', $filterInfo)) {
                $relatedTag = $filterInfo['tags'];

                foreach ($ads as $ad) {
                    if (Tag::find($ad->tag_id)->slug == $relatedTag) {
                        array_push($relatedAds, $ad);
                    }
                }
            }
        } else {
            // show all the products ads even if not related (will only appear if not on the explore page)?
            $relatedAds = $ads;
            //  if not above we can just leave the $relatedAds empty, thus returning no ads.
            // $relatedAds = [];
        }

        $formattedData = [];
        $formattedData =  [
            'type'                       => 'products',
            'id'                         => (string) $product->id,
            'sku'                        => $product->sku,
            'upc'                        => $product->upc,
            'is_active'                  => $product->is_active,
            'title'                      => $product->title,
            'long_description'           => $product->long_description,
            'short_description'          => $product->short_description,
            'retail_price'               => $product->retail_price,
            'wholesale_price'            => $product->wholesale_price,
            'limited_time_offer_price'   => $product->limited_time_offer_price,
            'limited_time_offer_savings' => $product->limited_time_offer_savings,
            'markup_percentage'          => $product->markup_percentage,
            'markup'                     => $product->markup,
            'runner_price'               => $product->runner_price,
            'images'                     => $images,
            'image_thumbnail'            => $product->image_thumbnail,
            'packaging'                  => $product->packaging,
            'alcohol_content'            => $product->alcohol_content,
            'sugar_content'              => $product->sugar_content * 100,
            'slug'                       => $product->slug,
            'category_id'                => (string) $product->category_id,
            'style'                      => $product->style,
            'producing_country'          => $product->producing_country,
            'producer'                   => $product->producer,
            'reward_points'              => $product->reward_points,
            'quantity'                   => $product->inventories()->where('inventory_id', $inventoryId)->where('product_id', $product->id)->first()->pivot->quantity ?? null,
            'delivery_window'            => '1-2 Hours',
            'case_deal'                  => $product->getCaseDealAttribute(),
            'incentives'                 => Incentive::collection($product->incentives->where('is_active', 1)->where('incentive_id', 3)),
            'average_rating'             => $product->average_rating,
            'favourite_id'               => (string) $product->favouriteId ?? null,
            'reviews_count'              => $product->reviews_count,
            'reviews'                    => ReviewResource::collection($product->reviews),
            'inventories'                => InventoryResource::collection($product->inventories),
            'store'                      => $store,
            'is_card'                    => $product->category_id == 34 ? true : false,
            'partners'                   => Partner::collection($product->partners),
            'ads'                        => $relatedAds,
        ];

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $this->storeId,
            'product'     => $formattedData,
            'breadCrumbs' => $breadCrumbs,
            'category'    => $category,
            'categories'  => $categories,
        ]);

        return view('runner.product', compact([
            'pageAttributes',
        ]));
    }

    public function category(Request $request, $categorySlug, GetInventoryCategoryProducts $action)
    {
        $fullPostalCode        = isset($request->all()['postalCode']) ? $request->all()['postalCode'] : 'M6J1G5';
        $domain                = $this->domain;
        $subDomain             = $this->subDomain;
        $store                 = Store::find($this->storeId);
        $category              = Category::where('slug', $categorySlug)->first();

        if (! $category) {
            $category = Category::find(4);
        }

        $category->description = strip_tags($category->description);

        if (! $category) {
            $pageAttributes = json_encode([
                'domain'      => $domain,
                'subDomain'   => $subDomain,
                'store'       => $store,
                'storeId'     => (string) $this->storeId,
            ]);

            return view('errors.404', compact([
                'pageAttributes'
            ]));
        }

        $categoryService = new CategoryService();
        $postalCode      = PostalCode::find(substr($fullPostalCode, 0, 3));
        $inventories     = $postalCode->inventories->pluck('id');
        $inventoryQuery  = DB::table('inventories')
            ->join('inventory_territory', 'inventories.id', '=', 'inventory_territory.inventory_id')
            ->join('territories', 'territories.id', '=', 'inventory_territory.territory_id')
            ->select('inventories.*')
            ->where('territories.id', $postalCode->territory->id)
            ->where('inventories.store_id', $store->id);
        $inventoryId        = isset($inventoryQuery->pluck('id')[0]) ? $inventoryQuery->pluck('id')[0] : 1;
        $inventory          = Inventory::where('id', $inventoryId)->first();
        $breadCrumbs        = json_encode($categoryService->getBreadcrumbs($category));
        $categories         = json_encode($categoryService->categoriesWithCount($inventory, $category));
        $products           = $action->execute($request, $inventory, $category);
        $tags               = $category->tags;
        $productsCollection = new ProductCollection($products);

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $this->storeId,
            'breadCrumbs' => $breadCrumbs,
            'category'    => $category,
            'tags'        => $tags,
            'categories'  => $categories,
            'products'    => $productsCollection,
            'meta'        => $products,
        ]);

        return view('runner.category', compact([
            'pageAttributes',
        ]));
    }

    public function tag(Request $request, $tagSlug)
    {
        $fullPostalCode  = isset($request->all()['postalCode']) ? $request->all()['postalCode'] : 'M6J1G5';
        $domain          = $this->domain;
        $subDomain       = $this->subDomain;
        $store           = Store::find($this->storeId);
        $tag             = Tag::where('slug', $tagSlug)->first();

        if (! $tag) {
            $pageAttributes = json_encode([
                'domain'      => $domain,
                'subDomain'   => $subDomain,
                'store'       => $store,
                'storeId'     => (string) $this->storeId,
            ]);

            return view('errors.404', compact([
                'pageAttributes'
            ]));
        }

        $tagService      = new TagService();
        $postalCode      = PostalCode::find(substr($fullPostalCode, 0, 3));
        $inventories     = $postalCode->inventories->pluck('id');
        $inventoryQuery  = DB::table('inventories')
            ->join('inventory_territory', 'inventories.id', '=', 'inventory_territory.inventory_id')
            ->join('territories', 'territories.id', '=', 'inventory_territory.territory_id')
            ->select('inventories.*')
            ->where('territories.id', $postalCode->territory->id)
            ->where('inventories.store_id', $store->id);
        $inventoryId     = isset($inventoryQuery->pluck('id')[0]) ? $inventoryQuery->pluck('id')[0] : 1;
        $inventory       = Inventory::where('id', $inventoryId)->first();
        $tags            = json_encode($tagService->tags($tag));
        $products        = app('App\Http\Controllers\Runner\V4\InventoryTagProductController')->index($request, $inventory, $tag);
        if (! $tag) {
            return view('errors.404');
        }

        $pageAttributes = json_encode([
            'domain'      => $domain,
            'subDomain'   => $subDomain,
            'store'       => $store,
            'storeId'     => (string) $this->storeId,
            'tag'         => $tag,
            'tags'        => $tags,
            'products'    => $products,
            'meta'        => $products->resource,
        ]);

        return view('runner.tags', compact([
            'pageAttributes',
        ]));
    }

    public function newOrders()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $store     = Store::find($this->storeId);

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'store'     => $store,
            'storeId'   => (string) $this->storeId,
        ]);

        return view('runner.new-orders', compact([
            'pageAttributes',
        ]));
    }

    public function newSearchResults()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $store     = Store::find($this->storeId);

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'store'     => $store,
            'storeId'   => (string) $this->storeId,
        ]);

        return view('runner.new-search-results', compact([
            'pageAttributes',
        ]));
    }

    public function newOrder(Order $order)
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $orderId   = $order->id;
        $store     = Store::find($this->storeId);

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'orderId'   => (string) $orderId,
            'storeId'   => (string) $store->id,
        ]);

        return view('runner.new-order', compact([
            'pageAttributes',
        ]));
    }

    public function newRewardPoints()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $store     = Store::find($this->storeId);

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'store'     => $store,
            'storeId'   => (string) $this->storeId,
        ]);

        return view('runner.new-reward-points', compact([
            'pageAttributes',
        ]));
    }

    public function newInviteFriends()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.new-invite-friends', compact([
            'pageAttributes',
        ]));
    }

    public function sop()
    {
        $domain    = $this->domain;
        $subDomain = $this->subDomain;
        $storeId   = $this->storeId;

        $pageAttributes = json_encode([
            'domain'    => $domain,
            'subDomain' => $subDomain,
            'storeId'   => (string) $storeId,
        ]);

        return view('runner.sop', compact([
            'pageAttributes',
        ]));
    }
}
