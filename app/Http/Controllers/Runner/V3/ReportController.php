<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\order;
use DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function getTopFiveCategory()
    {
        $topCat = DB::table('order_items')
                ->select(['category_id', DB::raw('COUNT(quantity) as quantity')])
                ->groupBy('category_id')
                ->orderBy('quantity', 'DESC')
                ->take(5)
                ->get()->toArray();
        dd($topCat);
    }

    public function getDeliveryDuration()
    {
        $topCat = DB::table('orders')
                ->select('category_id', count(`quantity`))
                ->groupBy('category_id')
                ->orderBy(count(`quantity`), 'DESC')
                ->take(5)
                ->get()->toArray();
        dd($topCat);
    }
}
