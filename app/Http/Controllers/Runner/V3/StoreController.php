<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\InventoriesCollection;
use App\Http\Resources\Inventory as InventoryResource;
use App\Http\Resources\Store as StoreResource;
use App\Http\Resources\StoresCollection;
use App\Inventory;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function show(Request $request, $storeId = null)
    {
        if ($storeId === null) {
            return new StoresCollection(Store::paginate());
        } else {
            $store = Store::find($storeId);

            return new StoreResource($store);
        }
    }

    public function getInventories(Request $request, $storeId, $inventoryId = null)
    {
        if ($inventoryId === null) {
            return new InventoriesCollection(Inventory::paginate());
        } else {
            $inventory = Store::find($storeId)->inventories()->find($inventoryId);

            return new InventoryResource($inventory);
        }
    }

    public function getAvailableDeliveryTimes($storeId)
    {
        $now = Carbon::now();
        // $now                              = Carbon::parse('2019-02-27 22:31:00');
        $currentDayOfTheWeek = $now->dayOfWeekIso;
        $currentHour = $now->hour;
        $currentMinute = $now->minute;
        $aMonthFromNow = Carbon::now()->addDays(30);
        $hourOpen = 11;
        $hourClosed = $currentDayOfTheWeek !== 7 ? 22 : 19;
        $minuteClosed = 30;
        $timeClosed = Carbon::createFromTime($hourClosed, $minuteClosed);
        $hoursUntilClosed = $now->greaterThan(Carbon::createFromTime(21, 00)) ? ($hourClosed - $currentHour) : ($hourClosed - $currentHour - 1);
        $availableTimeSlots = [];
        $availableDaySlots = [];
        $regularTimeSlots = [];
        $giftTimeSlots = [];

        if ($now->greaterThan(Carbon::createFromTime(21, 30))) {
            $hoursUntilClosed = 0;
            $hourClosed = 21;
        }

        for ($j = 0; $j < 30; $j++) {
            if ($j == 0) {
                $hour = $currentHour < $hourOpen ? $hourOpen : $currentHour + 1;
                $date = $now->toDateString();

                for ($i = 0; $i < $hoursUntilClosed; $i++) {
                    if ($i == 0) {
                        if ($hoursUntilClosed >= 1) {
                            $regularTimeSlots[$i] = [
                                'hour'         => $hour,
                                'interval'     => '2 hours or less',
                                'rewardPoints' => null,
                                'regularPrice' => 999,
                                'salePrice'    => 999,
                            ];
                        } else {
                            $regularTimeSlots = [];
                        }
                    } else {
                        $regularTimeSlots[$i] = [
                            'hour'         => $hour,
                            'interval'     => Carbon::createFromTime($hour, 0, 0)->format('ga').' - '.Carbon::createFromTime($hour + 1, 0, 0)->format('ga'),
                            'rewardPoints' => null,
                            'regularPrice' => 999,
                            'salePrice'    => 999,
                        ];
                    }

                    $hour++;
                }
                if ($currentHour < 12) {
                    $giftTimeSlots = [
                        [
                            'hour'                => 17,
                            'interval'            => '5pm - 10pm',
                            'rewardPoints'        => null,
                            'regularPrice'        => 999,
                            'salePrice'           => 999,
                        ],
                    ];
                } else {
                    $giftTimeSlots = [
                        // [
                        //     'hour'                => 12,
                        //     'interval'            => '12pm - 5pm',
                        //     'rewardPoints'        => null,
                        //     'regularPrice'        => 999,
                        //     'salePrice'           => 999,
                        // ]
                    ];
                }
            } else {
                $newHoursUntilClosed = $hourClosed - $hourOpen;
                $hour = $hourOpen;
                $date = $now->addDays(1)->toDateString();

                for ($i = 0; $i < $newHoursUntilClosed; $i++) {
                    $regularTimeSlots[$i] = [
                        'hour'         => $hour,
                        'interval'     => Carbon::createFromTime($hour, 0, 0)->format('ga').' - '.Carbon::createFromTime($hour + 1, 0, 0)->format('ga'),
                        'rewardPoints' => null,
                        'regularPrice' => 999,
                        'salePrice'    => 999,
                    ];

                    $hour++;
                }

                $giftTimeSlots = [
                    [
                        'hour'                => 12,
                        'interval'            => '12pm - 5pm',
                        'rewardPoints'        => null,
                        'regularPrice'        => 999,
                        'salePrice'           => 999,
                    ],
                    [
                        'hour'                => 17,
                        'interval'            => '5pm - 10pm',
                        'rewardPoints'        => null,
                        'regularPrice'        => 999,
                        'salePrice'           => 999,
                    ],
                ];
            }

            $availableTimeSlots[$j] = [
                'type'             => 'available time slots',
                'id'               => $j + 1,
                'date'             => $date,
                'regularTimeSlots' => array_values($regularTimeSlots),
                'giftTimeSlots'    => $giftTimeSlots,
            ];
        }

        return response()->json([
            'data' => $availableTimeSlots,
        ]);
    }

    /**
     * Function to get all Store time by Inventory.
     *
     * @param [type] $inventoryId
     * @return array
     */
    public function getAvailableDeliveryTimesByInventory($inventoryId)
    {
        $availableTimeSlots = [];
        $inventory = Inventory::find($inventoryId);
        $inventoryHours = $inventory->options['hours'];
        $now = Carbon::now();
        $currentHour = $now->hour;
        $aMonthFromNow = Carbon::now()->addDays(30);
        $dates = $this->generateDateRange(Carbon::now(), $aMonthFromNow);

        foreach ($dates as $key => $val) {
            $regularTimeSlot = [];
            if ($key == 0) {
                $storeTime = $inventoryHours[$val];
                $storeVar = $this->getHoursClosedUntil($storeTime, $key);

                $hoursUntilClosed = $storeVar['hoursUntilClosed'];
                if ($now->greaterThan($storeVar['lastHour']) || $storeVar['hourOpen'] == $storeVar['hourClosed']) {
                    $hoursUntilClosed = 0;
                }

                $hour = $currentHour < $storeVar['hourOpen'] ? $storeVar['hourOpen'] : $currentHour;
                $date = $now->toDateString();
                //case when time is 12 midnight to 11 in morning
                $timezone = 'America/Toronto';
                $tomorrow = Carbon::parse('tomorrow 11am', $timezone);

                if ($now->hour >= 0 && $now->hour < $tomorrow->hour) {
                    $storeTime = $inventoryHours[$now->dayOfWeek];
                    $storeVar = $this->getHoursClosedUntil($storeTime, 1);
                    $hoursUntilClosed = $storeVar['hoursUntilClosed'];
                }

                if ($hoursUntilClosed >= 1) {
                    for ($i = 0; $i < $hoursUntilClosed; $i++) {
                        if ($i == 0 && ! ($now->hour >= 0 && $now->hour < $tomorrow->hour)) {
                            $regularTimeSlot[$i] = [
                                'hour'         => $hour,
                                'interval'     => '2 hours or less',
                                'rewardPoints' => null,
                                'regularPrice' => 999,
                                'salePrice'    => 999,
                            ];
                        } else {
                            $regularTimeSlot[$i] = [
                                'hour'         => $hour,
                                'interval'     => Carbon::createFromTime($hour, 0, 0)->format('ga').' - '.Carbon::createFromTime($hour + 1, 0, 0)->format('ga'),
                                'rewardPoints' => null,
                                'regularPrice' => 999,
                                'salePrice'    => 999,
                            ];
                        }

                        $hour++;
                    }
                } else {
                    $regularTimeSlot = [];
                }

                $availableTimeSlots[$key] = [
                    'type'             => 'available time slots',
                    'id'               => $key + 1,
                    'date'             => $date,
                    'regularTimeSlots' => array_values($regularTimeSlot),
                ];
            } else {
                $date = $now->addDays(1)->toDateString();
                $futureStoreTime = $inventoryHours[$val];
                $storeVar = $this->getHoursClosedUntil($futureStoreTime, $key);
                $newHoursUntilClosed = $storeVar['hoursUntilClosed'];
                $futureHour = $storeVar['hourOpen'];
                $futureTimeSlot = [];

                if ($storeVar['hourOpen'] == $storeVar['hourClosed']) {
                    $newHoursUntilClosed = 0;
                    $futureTimeSlot = [];
                } else {
                    for ($x = 0; $x < $newHoursUntilClosed; $x++) {
                        $futureTimeSlot[$x] = [
                            'hour'         => $futureHour,
                            'interval'     => Carbon::createFromTime($futureHour, 0, 0)->format('ga').' - '.Carbon::createFromTime($futureHour + 1, 0, 0)->format('ga'),
                            'rewardPoints' => null,
                            'regularPrice' => 999,
                            'salePrice'    => 999,
                        ];

                        $futureHour++;
                    }
                }
                $availableTimeSlots[$key] = [
                    'type'               => 'available time slots',
                    'id'                 => $key + 1,
                    'date'               => $date,
                    'regularTimeSlots'   => array_values($futureTimeSlot),
                ];
            }
        }

        return response()->json([
            'data' => $availableTimeSlots,
        ]);
    }

    /**
     * Function to generate all dates for 1 month.
     *
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @return void
     */
    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->dayOfWeek;
        }

        return $dates;
    }

    /**
     * Function to get all Store parameter.
     *
     * @param [type] $storeTime
     * @param [type] $key
     * @return array
     */
    private function getHoursClosedUntil($storeTime, $key)
    {
        $hourExp = explode('-', $storeTime);
        $hourOpen = (int) $hourExp[0];
        $hourClosed = $hourExp[1];
        $closedHour = (int) explode(':', $hourClosed)[0];
        $closedMin = (int) explode(':', $hourClosed)[1];
        $lastHour = Carbon::createFromTime($closedHour, $closedMin);
        $currentHour = Carbon::now()->hour;
        $currentHourInCarbon = $key == 0 ? Carbon::createFromTime($currentHour) : Carbon::createFromTime($hourOpen);

        $hoursUntilClosedInMin = $currentHourInCarbon->diffInMinutes($lastHour);
        $hoursUntilClosed = (int) (ceil($hoursUntilClosedInMin / 60));

        return $storeVar[] = [
            'hourOpen'         => $hourOpen,
            'hourClosed'       => $closedHour,
            'lastHour'         => $lastHour,
            'hoursUntilClosed' => $hoursUntilClosed,
        ];
    }
}
