<?php

namespace App\Http\Controllers\Runner\V3;

use App\Address;
use App\Http\Controllers\Controller;
use App\Http\Resources\Address as AddressResource;
use App\Http\Resources\AddressesCollection;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function create(Request $request, $userId)
    {
        // unselect other addresses
        $addresses = Address::where('user_id', $userId)->get();

        if ($addresses) {
            foreach ($addresses as $address) {
                $address->selected = 0;
                $address->update();
            }
        }

        // create new address
        $address = new Address();
        $address->user_id = $userId;
        $address->formatted_address = $request->input('data.attributes.formattedAddress');
        $address->place_id = $request->input('data.attributes.placeId');
        $address->lat = $request->input('data.attributes.lat');
        $address->lng = $request->input('data.attributes.lng');
        $address->postal_code = $request->input('data.attributes.postalCode');
        $address->unit_number = $request->input('data.attributes.unitNumber') ?? null;
        $address->instructions = $request->input('data.attributes.instructions') ?? null;
        $address->business_name = $request->input('data.attributes.businessName') ?? null;
        $address->selected = 1;

        $address->save();

        $addresses = Address::where('user_id', $userId)->get();

        return new AddressesCollection($addresses);
    }

    public function index($userId)
    {
        $addresses = Address::where('user_id', $userId)->get();

        return new AddressesCollection($addresses);
    }

    public function update(Request $request, $userId, $addressId)
    {
        $address = Address::find($addressId);

        // change selected address
        if ($request->input('data.attributes.selected')) {
            // unselect other addresses
            $addresses = Address::where('user_id', $userId)->get();

            if ($addresses) {
                foreach ($addresses as $a) {
                    $a->selected = 0;
                    $a->update();
                }
            }

            $address->selected = $request->input('data.attributes.selected');
            $address->update();
        }

        // change selected address
        if ($request->input('data.attributes')) {
            if ($request->input('data.attributes.formattedAddress')) {
                $address->formatted_address = $request->input('data.attributes.formattedAddress');
            }
            if ($request->input('data.attributes.lat')) {
                $address->lat = (float) $request->input('data.attributes.lat');
            }
            if ($request->input('data.attributes.lng')) {
                $address->lng = (float) $request->input('data.attributes.lng');
            }
            if ($request->input('data.attributes.placeId')) {
                $address->place_id = $request->input('data.attributes.placeId');
            }
            if ($request->input('data.attributes.postalCode')) {
                $address->postal_code = $request->input('data.attributes.postalCode');
            }
            if ($request->input('data.attributes.unitNumber')) {
                $address->unit_number = $request->input('data.attributes.unitNumber');
            }
            if ($request->input('data.attributes.businessName')) {
                $address->business_name = $request->input('data.attributes.businessName');
            }
            if ($request->input('data.attributes.instructions')) {
                $address->instructions = $request->input('data.attributes.instructions');
            }
            $address->update();
        }

        $addresses = Address::where('user_id', $userId)->get();

        return new AddressesCollection($addresses);
    }

    public function delete($userId, $addressId)
    {
        $address = Address::find($addressId);

        if ($address) {
            $address->delete();
        }

        // grab all remaining addresses
        $addresses = Address::where('user_id', $userId)->get();

        if ($addresses) {
            foreach ($addresses as $address) {
                $address->selected = 0;
                $address->update();
            }

            $newSelected = count($addresses) > 0 ? $addresses[0] : $address;
            $newSelected->selected = 1;
            $newSelected->update();
        }

        return new AddressesCollection($addresses);
    }
}
