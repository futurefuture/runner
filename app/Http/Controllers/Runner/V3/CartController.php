<?php

namespace App\Http\Controllers\Runner\V3;

use App\Cart;
use App\Category;
use App\Coupon;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\Product;
use App\Services\SegmentService;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Segment;

class CartController extends Controller
{
    public function create(Request $request, $userId, $inventoryId)
    {
        $cartItems = $request->input('data.attributes.products');
        $formattedProducts = [];
        $cart = [];
        $cartSubtotal = 0;
        $itemsTax = 0;
        $cartTax = 0;
        $cartDiscount = 0;
        $cartTotal = 0;
        $totalRewardPoints = 0;
        $rewardPointsUsed = 0;
        $deliveryFee = 999;
        $deliveryTax = $deliveryFee * 0.13;
        $serviceFeeTax = 0;
        $serviceFee = 0;
        $user = User::find($userId);
        $isGift = false;
        $today = Carbon::now()->toDateString();

        // check if gift item
        if ($inventoryId == 11) {
            $serviceFee = 500;
            $serviceFeeTax = 500 * 0.13;
            $cart['isGift'] = true;
        }

        if ($request->input('data.attributes.couponCode')) {
            $couponCode = $request->input('data.attributes.couponCode');
            $coupon = Coupon::where('code', $couponCode)
                ->where('is_active', 1)
                ->first();

            if ($coupon) {
                if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $userId)->where('used', 1)->doesntExist()) {
                    $cart['coupon'] = (object) [
                        'code'  => $coupon->code,
                        'value' => (int) $coupon->value,
                    ];
                    $cartDiscount = (int) $coupon->value;
                } else {
                    return response()->json([
                        'errors' => [
                            [
                                'status' => 404,
                                'title'  => 'Invalid Coupon Code',
                                'detail' => 'The coupon code given has already been used.',
                            ],
                        ],
                    ], 404);
                }
            } else {
                return response()->json([
                    'errors' => [
                        [
                            'status' => 404,
                            'title'  => 'Invalid Coupon Code',
                            'detail' => 'The coupon code given is not a valid coupon code.',
                        ],
                    ],
                ], 404);
            }
        }

        // check for reward points
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 1000,
            ];

            $cart['coupon'] = $coupon;
            $cartDiscount = (int) $coupon->value;
        }

        foreach ($cartItems as $cartItem) {
            // disallow negative quantity
            if ((int) $cartItem['quantity'] < 0) {
                return false;
            }

            $product = Product::find((int) $cartItem['id']);

            // check for limited time offer on product
            $runnerPrice = 0;
            $retailPrice = 0;
            $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
                ->where('product_id', $product->id)
                ->where('incentive_id', 1)
                ->where('is_active', 1)
                ->first();

            if ($productIncentiveLimitedTimeOffer && Carbon::parse($productIncentiveLimitedTimeOffer->start_date)->isPast() && Carbon::parse($productIncentiveLimitedTimeOffer->end_date)->isFuture()) {
                $runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
                $retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
            } else {
                $runnerPrice = $product->runner_price;
                $retailPrice = $product->retail_price;
            }

            // check for quantity discount
            $productQuantityDiscount = DB::table('incentive_product')
                ->where('product_id', $product->id)
                ->where('incentive_id', 3)
                ->first();

            if ($productQuantityDiscount && ($cartItem['quantity'] >= $productQuantityDiscount->minimum_quantity) && Carbon::parse($productQuantityDiscount->start_date)->isPast() && Carbon::parse($productQuantityDiscount->end_date)->isFuture()) {
                $incentive = (object) [
                    'id'    => $productQuantityDiscount->id,
                    'value' => (int) $productQuantityDiscount->savings,
                ];

                $cart['coupon'] = null;

                $cart['incentive'] = $incentive;
                $cartDiscount = (int) $productQuantityDiscount->savings;
            }

            $formattedProduct['id'] = $product->id;
            $formattedProduct['title'] = $product->title;
            $formattedProduct['runnerPrice'] = $runnerPrice;
            $formattedProduct['retailPrice'] = $retailPrice;
            $formattedProduct['image'] = $product->image;
            $formattedProduct['quantity'] = (int) $cartItem['quantity'];
            $formattedProduct['packaging'] = $product->packaging;
            $formattedProduct['rewardPoints'] = $product->rewardPoints;
            $formattedProduct['allowSub'] = $cartItem['allowSub'];
            $formattedProduct['type'] = $cartItem['type'] ?? 'regular';
            $formattedProduct['subTotal'] = $runnerPrice * $cartItem['quantity'];

            $cartSubtotal += $runnerPrice * $cartItem['quantity'];
            $itemsTax += (($runnerPrice - $runnerPrice / 1.12) * 0.13) * $cartItem['quantity'];
            array_push($formattedProducts, $formattedProduct);
        }

        $cartTax = round($itemsTax + $deliveryTax + $serviceFeeTax);

        $cart['products'] = $formattedProducts;
        $cart['subTotal'] = $cartSubtotal;
        $cart['deliveryFee'] = $deliveryFee;
        $cart['serviceFee'] = $serviceFee;
        $cart['tax'] = $cartTax;
        $cart['rewardPointsUsed'] = 0;
        $cart['isGift'] = $isGift;
        $cart['discount'] = $cartDiscount;
        $cart['serviceFee'] = $serviceFee;
        $cart['total'] = round($cartSubtotal + $cartTax + $deliveryFee + $serviceFee - $cartDiscount);
        $cart = Cart::updateOrCreate(
            [
                'user_id'      => (int) $userId,
                'inventory_id' => (int) $inventoryId,
            ],
            [
                'content'      => json_encode($cart),
            ]
        );
        $product = Product::find($formattedProducts[0]['id']);
        $category = Category::find($product->category_id);

        $quantity = $formattedProducts[0]['quantity'];
        $coupon = $request->input('data.attributes.couponCode') ?? null;
        $storeId = Inventory::find($inventoryId)->first()->store_id;

        (new SegmentService())->trackProductAdded($user, $product, $cart, $category, $quantity, $coupon, $storeId, $inventoryId);
    }

    public function update(Request $request, $userId, $storeId, $cartId)
    {
        $oldCart = Cart::find($cartId);
        $newCartItems = $request->input('data.attributes.products');
        $oldCartItems = json_decode($oldCart->content)->products;
        $oldCartContent = json_decode($oldCart->content);
        $oldCartItemIds = [];
        $cart = [];
        $today = Carbon::now()->toDateString();

        if (isset($oldCartContent->coupon)) {
            $cart['coupon'] = $oldCartContent->coupon;
        }

        $cartDiscount = $oldCartContent->discount;
        $newCartItemIds = [];
        $oldCartItemIds = [];
        $isGift = false;
        $serviceFee = 0;
        $serviceFeeTax = 0;

        if ($request->input('data.attributes.couponCode')) {
            $couponCode = $request->input('data.attributes.couponCode');

            if ($couponCode === 'CLEARCOUPON') {
                $cart['coupon'] = null;
                $cartDiscount = 0;
            } elseif ($couponCode === 'CLEARCART') {
                Cart::destroy($cartId);
            } else {
                $coupon = Coupon::where('code', $couponCode)
                    ->where('is_active', 1)
                    ->first();

                if ($coupon) {
                    if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $userId)->where('used', 1)->doesntExist()) {
                        $cart['coupon'] = (object) [
                            'code'  => $coupon->code,
                            'value' => (int) $coupon->value,
                        ];

                        $cartDiscount = (int) $coupon->value;
                    } else {
                        return response()->json([
                            'errors' => [
                                [
                                    'status' => 404,
                                    'title'  => 'Invalid Coupon Code',
                                    'detail' => 'The coupon code given has already been used.',
                                ],
                            ],
                        ], 404);
                    }
                } else {
                    return response()->json([
                        'errors' => [
                            [
                                'status' => 404,
                                'title'  => 'Invalid Coupon Code',
                                'detail' => 'The coupon code given is not a valid coupon code.',
                            ],
                        ],
                    ], 404);
                }
            }
        }

        foreach ($oldCartItems as $oldCartItem) {
            (int) array_push($oldCartItemIds, $oldCartItem->id);
        }

        collect($newCartItems)->each(function ($newCartItem) use (&$oldCartItemIds, &$oldCartItems, &$formattedProducts, &$cartSubTotal, &$cartTax, &$cart, &$cartDisctount, &$today) {
            if (in_array($newCartItem['id'], $oldCartItemIds)) {
                foreach ($oldCartItems as $key => $oldCartItem) {
                    if ($newCartItem['id'] === $oldCartItem->id) {
                        if ($newCartItem['quantity'] === 0) {
                            unset($oldCartItems[$key]);
                        }

                        // disallow negative quantity
                        if ((int) $newCartItem['quantity'] < 0) {
                            continue;
                        }

                        $oldCartItem->quantity = (int) $newCartItem['quantity'];
                        $oldCartItem->allowSub = $newCartItem['allowSub'];
                        $oldCartItem->subTotal = $newCartItem['quantity'] * $oldCartItem->runnerPrice;
                    }
                }

                $oldCartItems = (array) array_values($oldCartItems);
            } else {
                $product = Product::find($newCartItem['id']);

                // check for limited time offer on product
                $runnerPrice = 0;
                $retailPrice = 0;
                $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
                    ->where('product_id', $product->id)
                    ->where('incentive_id', 1)
                    ->where('is_active', 1)
                    ->first();

                if ($productIncentiveLimitedTimeOffer && Carbon::parse($productIncentiveLimitedTimeOffer->start_date)->isPast() && Carbon::parse($productIncentiveLimitedTimeOffer->end_date)->isFuture()) {
                    $runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
                    $retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
                } else {
                    $runnerPrice = $product->runner_price;
                    $retailPrice = $product->retail_price;
                }

                $newProduct['id'] = $product->id;
                $newProduct['title'] = $product->title;
                $newProduct['type'] = $newCartItem['type'];
                $newProduct['runnerPrice'] = $runnerPrice;
                $newProduct['retailPrice'] = $retailPrice;
                $newProduct['quantity'] = (int) $newCartItem['quantity'];
                $newProduct['packaging'] = $product->packaging;
                $newProduct['image'] = $product->image;
                $newProduct['rewardPoints'] = $product->rewardPoints;
                $newProduct['allowSub'] = $newCartItem['allowSub'];
                $newProduct['subTotal'] = $newCartItem['quantity'] * $runnerPrice;

                $cartSubTotal += $newProduct['subTotal'];
                $cartTax += (($runnerPrice - $runnerPrice / 1.12) * 0.13) * $newCartItem['quantity'];

                if ($newCartItem['type'] === 'giftItem') {
                    foreach ($oldCartItems as $key => $o) {
                        if ($o->type === 'giftItem') {
                            array_splice($oldCartItems, $key, 1);
                        }
                    }
                }

                if ($newCartItem['type'] === 'giftCard') {
                    foreach ($oldCartItems as $key => $o) {
                        if ($o->type === 'giftCard') {
                            array_splice($oldCartItems, $key, 1);
                        }
                    }
                }

                if ($newCartItem['type'] === 'bottleTag') {
                    foreach ($oldCartItems as $key => $o) {
                        if ($o->type === 'bottleTag') {
                            array_splice($oldCartItems, $key, 1);
                        }
                    }
                }

                array_push($oldCartItems, (object) $newProduct);
            }
        });

        $cartSubTotal = 0;
        $itemsTax = 0;
        $cartTotal = 0;
        $deliveryFee = $oldCartContent->deliveryFee;
        $deliveryTax = $deliveryFee * 0.13;

        foreach ($oldCartItems as $oldCartItem) {
            // check for quantity discount
            $productQuantityDiscount = DB::table('incentive_product')
                ->where('product_id', $oldCartItem->id)
                ->where('incentive_id', 3)
                ->first();

            if ($productQuantityDiscount && ($oldCartItem->quantity >= $productQuantityDiscount->minimum_quantity) && Carbon::parse($productQuantityDiscount->start_date)->isPast() && Carbon::parse($productQuantityDiscount->end_date)->isFuture()) {
                $incentive = (object) [
                    'id'    => $productQuantityDiscount->id,
                    'value' => (int) $productQuantityDiscount->savings,
                ];

                $cart['coupon'] = null;

                $cart['incentive'] = $incentive;
                $cartDiscount = (int) $productQuantityDiscount->savings;
            } elseif ($productQuantityDiscount && ($oldCartItem->quantity < $productQuantityDiscount->minimum_quantity)) {
                unset($cart['incentive']);
                $cartDiscount = 0;
            }

            $cartSubTotal += $oldCartItem->subTotal;
            $itemsTax += (($oldCartItem->runnerPrice - $oldCartItem->runnerPrice / 1.12) * 0.13) * $oldCartItem->quantity;
        }

        $cartTax = round($itemsTax + $deliveryTax + $serviceFeeTax);
        $cartTotal = $cartSubTotal + $cartTax;

        $cart['products'] = $oldCartItems;
        $cart['isGift'] = $isGift;
        $cart['serviceFee'] = $serviceFee;
        $cart['subTotal'] = $cartSubTotal;
        $cart['tax'] = round($cartTax);
        $cart['deliveryFee'] = $deliveryFee;
        $cart['discount'] = $cartDiscount;
        $cart['total'] = round($cartSubTotal + $cartTax + $deliveryFee + $serviceFee - $cartDiscount);

        if (count($oldCartItems) === 0) {
            Cart::destroy($cartId);
        } else {
            $oldCart->update([
                'content' => json_encode($cart),
            ]);
        }
    }

    public function index($userId, $inventoryId, $cartId = null)
    {
        if ($cartId) {
            $cart = Cart::find($cartId);

            if ($cart) {
                $cartContent = json_decode($cart->content);

                return response()->json([
                    'data' => [
                        'type'           => 'shopping carts',
                        'id'             => $cart->id,
                        'attributes'     => [
                            'products'    => $cartContent->products,
                            'coupon'      => $cartContent->coupon,
                            'incentive'   => $cartContent->incentive,
                            'subTotal'    => $cartContent->subTotal,
                            'deliveryFee' => $cartContent->deliveryFee,
                            'serviceFee'  => $cartContent->serviceFee,
                            'tax'         => $cartContent->tax,
                            'discount'    => $cartContent->discount,
                            'total'       => $cartContent->total,
                        ],
                    ],
                ]);
            } else {
                return response(404);
            }
        } else {
            $cart = Cart::where('user_id', $userId)
                ->where('inventory_id', $inventoryId)
                ->first();
            if (! $cart || ! $userId) {
                return response()->json([
                    'data' => [],
                ]);
            } else {
                $cartContent = json_decode($cart->content);

                foreach ($cartContent->products as $c) {
                    $c->quantity = (int) $c->quantity;
                }

                return response()->json([
                    'data' => [
                        [
                            'type'           => 'shopping carts',
                            'id'             => $cart->id,
                            'attributes'     => [
                                'products'    => $cartContent->products,
                                'isGift'      => isset($cartContent->isGift) ? $cartContent->isGift : null,
                                'serviceFee'  => $cartContent->serviceFee ?? 0,
                                'coupon'      => isset($cartContent->coupon) ? $cartContent->coupon : null,
                                'incentive'   => isset($cartContent->incentive) ? $cartContent->incentive : null,
                                'subTotal'    => $cartContent->subTotal,
                                'deliveryFee' => $cartContent->deliveryFee,
                                'tax'         => $cartContent->tax,
                                'discount'    => $cartContent->discount,
                                'total'       => $cartContent->total,
                            ],
                        ],
                    ],
                ]);
            }
        }
    }
}
