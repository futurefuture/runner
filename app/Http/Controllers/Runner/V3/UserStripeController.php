<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use Auth;
use Illuminate\Http\Request;

class UserStripeController extends Controller
{
    public function getCustomer()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();

        $stripeCustomer = \Stripe\Customer::retrieve($user->stripe_id);

        return response()->json([
            'data' => [
                'type'       => 'stripe customers',
                'id'         => $user->stripe_id,
                'attributes' => $stripeCustomer,
            ],
        ]);
    }

    public function createSource(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();
        $zipMessage = 'The Zip Code associated with your card is incorrect. If you aleady have a card added, please delete and re-enter your credit card information.';

        try {
            $customer = \Stripe\Customer::retrieve($user->stripe_id);
            $source = $customer->sources->create([
                'source' => $request->input('data')['id'],
            ]);
            $customer->default_source = $source->id;
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];

            if (strpos($err['message'], 'zip') == true) {
                return response()->json([
                    'errors' => [
                        'error' => $zipMessage,
                    ],
                ], 400);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => $err['message'],
                    ],
                ], 400);
            }

            return response()->json([
                'errors' => [
                    'error' => $z,
                ],
            ], 400);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'errors' => [
                    'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
                ],
            ]);
        }

        return $this->getCustomer();
    }

    public function updateSource(Request $request, $userId, $sourceId)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();

        try {
            $customer = \Stripe\Customer::retrieve($user->stripe_id);
            $customer->default_source = $sourceId;
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];

            return response()->json([
                'errors' => [
                    'error' => $err['message'].' { CODE: '.$err['code'].' } ',
                ],
            ], 400);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'errors' => [
                    'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
                ],
            ]);
        }

        return $this->getCustomer();
    }

    public function deleteSource(Request $request, $userId, $sourceId)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();

        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $customer->sources->retrieve($sourceId)->delete();

        return $this->getCustomer();
    }
}
