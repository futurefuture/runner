<?php

namespace App\Http\Controllers\Runner\V3;

use DB;
use Auth;
use Segment;
use App\User;
use Socialite;
use Validator;
use Carbon\Carbon;
use Mailgun\Mailgun;
use App\Events\UserCreated;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\SegmentService;
use App\Services\MailChimpService;
use App\Actions\GetUserGenderAction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $mg = Mailgun::create(env('MAILGUN_SECRET'));

        if ($request->input('email')) {
            $mgValidation = $mg->get('address/private/validate', [
                'address' => $request->input('email'),
            ]);
        }

        if ($mgValidation && $mgValidation->http_response_body->is_valid) {
            $parts            = explode('@', $request->input('email'));
            $domain           = array_pop($parts);
            $notAllowedDomain = ['qq.com', 'huawei.com'];

            if (in_array($domain, $notAllowedDomain)) {
                return response()->json([
                    'errors' => [
                        'error' => 'Invalid Email',
                    ],
                ], 401);
            }

            $validator = Validator::make($request->all(), [
                'email'       => 'required|email|max:255|unique:users',
                'firstName'   => 'required|max:255',
                'lastName'    => 'required|max:255',
                'phoneNumber' => 'required|unique:users,phone_number',
                'dateOfBirth' => 'required|date|before:19 years ago',
                'isLegalAge'  => 'required',
            ]);

            // check that user has agreed to be above 19 years old
            if ($request->input('isLegalAge') == false) {
                return response()->json([
                    'errors' => [
                        'error' => 'Please check that you are above 19 years of age to register.',
                    ],
                ], 401);
            }

            if ($validator->fails()) {
                $errors = json_decode($validator->errors()->toJson());

                foreach ($errors as $key => $value) {
                    $temp = $value[0];

                    break;
                }

                $errors->error = $temp;

                return response()->json([
                    'errors' => $errors,
                ], 401);
            }
            $parts  = explode('@', $request->input('email'));
            $domain = array_pop($parts);

            $notAllowedDomain = ['qq.com'];

            if (in_array($domain, $notAllowedDomain)) {
                return response()->json([
                    'errors' => [
                        'error' => 'Invalid Email',
                    ],
                ], 401);
            }

            // create stripe user
            try {
                $customer = \Stripe\Customer::create([
                    'description' => ucfirst(strtolower($request->input('firstName'))),
                    'email'       => strtolower($request->input('email')),
                ]);
            } catch (\Stripe\Error\Base $e) {
                return response()->json([
                    'errors' => [
                        'error' => 'There has been an error in registering your account.',
                    ],
                ], 401);
            }

            $user = User::create([
                'first_name'    => ucfirst(strtolower($request->input('firstName'))),
                'last_name'     => ucfirst(strtolower($request->input('lastName'))),
                'phone_number'  => preg_replace('/\D/', '', $request->input('phoneNumber')),
                'date_of_birth' => new Carbon($request->input('dateOfBirth'), 'America/Toronto'),
                'email'         => strtolower($request->input('email')),
                'password'      => $request->filled('password') ? bcrypt($request->input('password')) : null,
                'facebook_id'   => $request->filled('facebookId') ? $request->input('facebookId') : null,
                'google_id'     => $request->filled('googleId') ? $request->input('googleId') : null,
                'stripe_id'     => $customer->id,
                'is_19'         => 1,
                'invite_code'   => strtoupper(Str::random(6)),
                ''              => Str::random(60),
                'gender'        => $request->filled('gender') ? $request->input('gender') : null,
                'age_range'     => $request->filled('ageRange') ? $request->input('ageRange') : null,
                'avatar_image'  => $request->filled('avatarImage') ? $request->input('avatarImage') : null,
                'address'       => $request->filled('postalCode') ? $request->input('postalCode') : null,
            ]);

            $getUserGenderAction = new GetUserGenderAction;
            $getUserGenderAction->execute($user);

            $isReferrer = DB::table('invite_user')
                ->where('email_sent_to', $user->email)
                ->orWhere('phone_number_sent_to', $user->phone_number)
                ->first();

            // add 1000 reward points to account if referrer exists.
            if ($isReferrer) {
                $referrer = User::find($isReferrer->user_id);

                $user->rewardPointsTransaction(1000, 1, [
                    'referrer_id'    => $referrer->id,
                    'referrer_name'  => $referrer->first_name . $referrer->last_name,
                    'referrer_email' => $referrer->email,
                ]);

                $referrer
                    ->invites()
                    ->create([
                        'invited_user_id' => $user->id,
                        'created_at'      => Carbon::now(),
                        'updated_at'      => Carbon::now(),
                    ]);
            }

            $accessToken = $user->createToken('Runner')
                ->accessToken;

            event(new UserCreated($user));

            if (env('APP_ENV') === 'production') {
                // add user to mailchimp
                $mailChimpService = new MailChimpService($user);
                $mailChimpService->addSubscriber();
            }
            if (env('APP_ENV') === 'staging' || env('APP_ENV') === 'local') {
                (new SegmentService())->trackSignedUp($user);
            }
            // try {
            //     Segment::track([
            //         "userId"     => $user->id,
            //         "event"      => "Signed Up",
            //         "properties" => [
            //             "was_awesome" => true,
            //         ]
            //     ]);
            // } catch (Exception $e) {
            //     Log::info('Event track doesnt work because of ' . $e->getMessage());
            //     return $e->getMessage();
            // }
            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => $accessToken,
                        'user'        => [
                            'id'        => $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                    ],
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'Invalid Email',
                ],
            ], 401);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors'      => $errors,
            ], 401);
        }

        if (Auth::attempt([
            'email' => request('email'),
            'password' => request('password'),
        ])) {
            $user        = Auth::user();
            $accessToken = $user
                ->createToken('Runner')
                ->accessToken;
            if (env('APP_ENV') === 'staging' || env('APP_ENV') === 'local') {
                (new SegmentService())->trackLoggedIn($user);
            }

            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => $accessToken,
                        'user'        => [
                            'id'        => $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                    ],
                ],
            ]);
        } else {
            return response()->json([
                'errors' => [
                    'error' => 'Invalid Credentials',
                ],
            ], 401);
        }
    }

    public function logout()
    {
        $user         = Auth::user();
        $accessTokens = $user->tokens;

        foreach ($accessTokens as $accessToken) {
            $accessToken->revoke();
        }
    }

    public function redirectToProvider($socialPlatform)
    {
        return Socialite::driver($socialPlatform)
            ->scopes(['email'])
            ->stateless()
            ->redirect();
    }

    public function handleProviderCallback($socialPlatform)
    {
        $socialPlatformUser = Socialite::driver($socialPlatform)
            ->stateless()
            ->user();

        if ($socialPlatform == 'facebook') {
            // check if user exists to login or register
            if (User::where('facebook_id', $socialPlatformUser->id)->first()) {
                $user = User::where('facebook_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', ['secret' => $secret]);
            } else {
                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'facebook_id' => $socialPlatformUser->id,
                        'avatar'      => $socialPlatformUser->avatar,
                    ]);
                } else {
                    $user = (object) [
                        'facebookId' => $socialPlatformUser->id,
                        'firstName'  => explode(' ', $socialPlatformUser->name)[0],
                        'lastName'   => explode(' ', $socialPlatformUser->name)[1],
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@facebook.com',
                        'avatar'     => $socialPlatformUser->avatar,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@register', ['secret' => $secret]);
                }
            }
        } elseif ($socialPlatform == 'google') {
            // check if user exists to login or register
            if (User::where('google_id', $socialPlatformUser->id)->first()) {
                $user = User::where('google_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', ['secret' => $secret]);
            } else {
                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'google_id'   => $socialPlatformUser->id,
                        'avatar'      => $socialPlatformUser->avatar_original,
                    ]);
                } else {
                    $user = (object) [
                        'googleId'   => $socialPlatformUser->id,
                        'firstName'  => $socialPlatformUser->user['name']['givenName'] ?? 'firstName',
                        'lastName'   => $socialPlatformUser->user['name']['familyName'] ?? $socialPlatformUser->id,
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@googleplus.com',
                        'avatar'     => $socialPlatformUser->avatar_original,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@register', ['secret' => $secret]);
                }
            }
        }
    }
}
