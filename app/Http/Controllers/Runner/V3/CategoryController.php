<?php

namespace App\Http\Controllers\Runner\V3;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends Controller
{
    public function show()
    {
        $categories = Category::all();
        $formattedCategories = [];

        foreach ($categories as $category) {
            $formattedCategory = (object) [
                'type'       => 'categories',
                'id'         => $category->id,
                'attributes' => [
                    'title'    => $category->title,
                    'parentId' => $category->parent_id,
                    'slug'     => $category->slug,
                    'image'    => 'https://placekitten.com/200/300',
                ],
            ];

            array_push($formattedCategories, $formattedCategory);
        }

        return response()->json([
            'data' => $formattedCategories,
        ]);
    }

    public function getFilters($categoryId = null)
    {
        // 1 = alcohol
        // 2 = beer
        // 3 = spirits
        // 4 = wine

        if ($categoryId == 4) {
            $categories = Category::whereIn('id', [6, 7, 8, 9, 10, 11])->get();

            return response()->json([
                'data' => [
                    [
                        'type'       => 'filters',
                        'id'         => 1,
                        'attributes' => [
                            'title'   => 'Categories',
                            'slug'    => null,
                            'options' => $categories,
                        ],
                    ],
                ],
            ]);
        } elseif ($categoryId == 8) {
            return response()->json([
                'data' => [
                    [
                        'type'       => 'filters',
                        'id'         => 1,
                        'attributes' => [
                            'title'   => 'Sales & Promotions',
                            'slug'    => null,
                            'options' => [
                                [
                                    'title' => 'Sale',
                                    'id'    => null,
                                    'slug'  => 'sale',
                                ],
                                [
                                    'title' => 'Reward Points',
                                    'id'    => null,
                                    'slug'  => 'reward-points',
                                ],
                            ],
                        ],
                    ],
                    [
                        'type'       => 'filters',
                        'id'         => 3,
                        'attributes' => [
                            'title'      => 'Country',
                            'slug'       => 'country',
                            'options'    => [
                                [
                                    'title' => 'Italy',
                                    'id'    => null,
                                    'slug'  => 'italy',
                                ],
                                [
                                    'title' => 'France',
                                    'id'    => null,
                                    'slug'  => 'france',
                                ],
                                [
                                    'title' => 'USA',
                                    'id'    => null,
                                    'slug'  => 'usa',
                                ],
                                [
                                    'title' => 'Canada',
                                    'id'    => null,
                                    'slug'  => 'canada',
                                ],
                                [
                                    'title' => 'Australia',
                                    'id'    => null,
                                    'slug'  => 'australia',
                                ],
                                [
                                    'title' => 'Spain',
                                    'id'    => null,
                                    'slug'  => 'spain',
                                ],
                                [
                                    'title' => 'Argentina',
                                    'id'    => null,
                                    'slug'  => 'argentina',
                                ],
                                [
                                    'title' => 'Chile',
                                    'id'    => null,
                                    'slug'  => 'chile',
                                ],
                            ],
                        ],
                    ],
                ],
            ]);
        }
    }
}
