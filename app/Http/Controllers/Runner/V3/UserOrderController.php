<?php

namespace App\Http\Controllers\Runner\V3;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Http\Resources\NewOrder as NewOrderResource;
use App\Http\Resources\NewOrdersCollection;
use App\Order;
use App\Product;
use Auth;
use DB;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
    public function index($userId)
    {
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)
                    ->orderBy('id', 'desc')
                    ->paginate(100);

        return new NewOrdersCollection($orders);
    }

    public function show($userId, $orderId)
    {
        $user = Auth::user();
        $order = Order::find($orderId);

        return new NewOrderResource($order);
    }

    public function reOrder(Request $request, $userId, $orderId = null)
    {
        $user = Auth::user();
        $data = $request->input('data');

        if (! $user->orders()->where('status', 3)->count()) {
            return response()->json([
                'errors'      => [
                    'error' => 'No order was found.',
                    ],
            ], 404);
        }

        $inventoryId = $data['attributes']['inventoryId'];

        // set to 00 on web to grab most recent order
        if ($orderId != 00) {
            $order = $user->orders()
                    ->where('inventory_id', $inventoryId)
                    ->where('id', $orderId)
                    ->first();
        } elseif ($orderId == 00) {
            $order = $user->orders()
                            ->where('status', 3)
                            ->where('inventory_id', $inventoryId)
                            ->orderBy('created_at', 'desc')
                            ->first();
        }

        if (! $order) {
            return response()->json([
            'errors'      => [
                'error' => 'No order was found.',
                ],
            ], 404);
        }

        $cartItems = json_decode($order->content);
        $cart = [];
        $formattedProducts = [];
        $cartSubtotal = 0;
        $cartTax = 0;
        $cartDiscount = 0;
        $cartTotal = 0;
        $totalRewardPoints = 0;
        $rewardPointsUsed = 0;
        $deliveryFee = 999;

        // check for reward points
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 10,
            ];
            $cart['coupon'] = $coupon;
            $cartDiscount = (int) $coupon->value * 100;
        }

        foreach ($cartItems as $cartItem) {
            // return response()->json($cartItem);
            // check for kraken products and then add coupon
            if (in_array($cartItem->id, [2133, 3583, 4596, 5107, 6155, 7003])) {
                $coupon = (object) [
                    'code'  => 'KRAKEN'.$userId,
                    'value' => (int) 10,
                ];
                $cart['coupon'] = $coupon;
                $cartDiscount = (int) $coupon->value * 100;
            }

            $product = Product::find((int) $cartItem->id);
            // check for limited time offer on product
            $runnerPrice = 0;
            $retailPrice = 0;
            $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
                                                    ->where('product_id', $product->id)
                                                    ->where('incentive_id', 1)
                                                    ->where('is_active', 1)
                                                    ->first();
            if ($productIncentiveLimitedTimeOffer) {
                $runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
                $retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
            } else {
                $runnerPrice = $product->runner_price;
                $retailPrice = $product->retail_price;
            }
            $formattedProduct['id'] = $product->id;
            $formattedProduct['title'] = $product->title;
            $formattedProduct['runnerPrice'] = $runnerPrice;
            $formattedProduct['retailPrice'] = $retailPrice;
            $formattedProduct['image'] = $product->image;
            $formattedProduct['quantity'] = isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty;
            $formattedProduct['packaging'] = $product->packaging;
            $formattedProduct['rewardPoints'] = $product->rewardPoints;
            $formattedProduct['allowSub'] = isset($cartItem->allowSub) ? $cartItem->allowSub : true;
            $formattedProduct['subTotal'] = $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            $cartSubtotal += $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);
            $cartTax += (($runnerPrice - $runnerPrice / 1.12) * 0.13) * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            array_push($formattedProducts, $formattedProduct);
        }

        $cart['products'] = $formattedProducts;
        $cart['subTotal'] = $cartSubtotal;
        $cart['deliveryFee'] = $deliveryFee;
        $cart['tax'] = round($cartTax + ($deliveryFee * 0.13));
        $cart['rewardPointsUsed'] = 0;
        $cart['discount'] = $cartDiscount;
        $cart['total'] = round($cartSubtotal + $cartTax + $deliveryFee + ($deliveryFee * 0.13) - $cartDiscount);

        // check for old cart with old inventory and delete
        $oldCart = Cart::where('user_id', (int) $userId)
                        ->where('inventory_id', $inventoryId)
                        ->first();

        if ($oldCart) {
            Cart::destroy($oldCart->id);
        }

        $newCart = Cart::create([
            'user_id'      => (int) $userId,
            'inventory_id' => (int) $inventoryId,
            'content'      => json_encode($cart),
        ]);

        return response()->json([
            'data' => [
                'type'       => 'carts',
                'id'         => $newCart->id,
                'attributes' => [
                    'products'    => $cart['products'],
                    'coupon'      => isset($cart['coupon']) ? $cart['coupon'] : null,
                    'subTotal'    => $cart['subTotal'],
                    'deliveryFee' => $cart['deliveryFee'],
                    'tax'         => $cart['tax'],
                    'discount'    => $cart['discount'],
                    'total'       => $cart['total'],
                    'inventoryId' => $inventoryId,
                ],
            ],
        ]);
    }
}
