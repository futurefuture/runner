<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\RewardPoint as RewardPointResource;
use App\RewardPoint;
use App\User;
use Auth;
use Illuminate\Http\Request;

class RewardPointsController extends Controller
{
    public function getRewardPointsHistory($userId)
    {
        $user = User::find($userId);
        $rewardPointsHistory = RewardPointResource::collection(RewardPoint::where('user_id', $user->id)
                                ->orderBy('id', 'desc')
                                ->paginate());

        return $rewardPointsHistory;
    }

    public function getRewardPointsBalance($userId)
    {
        $user = Auth::user();
        $rewardPointsBalance = $user->rewardPointsBalance();

        return response()->json([
            'data' => $rewardPointsBalance,
        ], 200);
    }
}
