<?php

namespace App\Http\Controllers\Runner\V3;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\InventoriesCollection;
use App\Http\Resources\Inventory as InventoryResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductsCollection;
use App\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function show(Request $request, $inventoryId = null)
    {
        if ($inventoryId === null) {
            return new InventoriesCollection(Inventory::paginate(20));
        } else {
            $inventory = Inventory::find($inventoryId);

            return new InventoryResource($inventory);
        }
    }

    private function getCategoriesByParentId($categoryId)
    {
        $category_data             = [];
        $parentCategory            = Category::find($categoryId);
        $formattedParentCategory[] = [
            'id'        => $parentCategory->id,
            'title'     => $parentCategory->title,
            'slug'      => $parentCategory->slug,
            'parent_id' => $parentCategory->parent_id,
        ];
        $category_query = Category::where('parent_id', $categoryId)->get();

        foreach ($category_query as $category) {
            $category_data[] = [
                'id'        => $category->id,
                'title'     => $category->title,
                'slug'      => $category->slug,
                'parent_id' => $category->parent_id,
            ];
            $children = $this->getCategoriesByParentId($category->id);

            if ($children) {
                $category_data = array_merge($children, $category_data);
            }
        }

        return array_merge($category_data, $formattedParentCategory);
    }

    public function getSearch(Request $request, $inventoryId = null, $search = null)
    {
        $isWeb     = $request->has('web');
        $inventory = Inventory::find($inventoryId);
        $query     = Product::where('is_active')
            ->where('title', 'LIKE', '%' . $search . '%')
            ->limit(20)
            ->get();
        $products = [];

        foreach ($query as $q) {
            if ($isWeb) {
                $product = $inventory
                    ->productsAll()
                    ->where('is_active', '!=', 0)
                    ->find($q->id);
            } else {
                $product = $inventory
                    ->productsInStock()
                    ->where('is_active', '!=', 0)
                    ->find($q->id);
            }
            if (! $product) {
                continue;
            }

            array_push($products, $product);
        }

        $products = collect($products);

        return new ProductsCollection($products);
    }

    public function getProducts(Request $request, $inventoryId, $categoryId, $productId = null)
    {
        $filters = $request->query('filters') ?? null;
        $isWeb   = $request->has('web');

        if ($filters) {
            $filters = explode(',', $filters);
        }

        $limit         = (int) $request->input('limit');
        $sort          = $request->input('sort');
        $filterNames   = [];
        $tags          = [];
        $tagProductIds = [];
        $inventory     = Inventory::find($inventoryId);
        $category      = Category::find($categoryId);
        $allCategories = Category::all()->toArray();
        $topProductIds = '';
        $categoryIds   = [];
        $categoryId    = (int) $categoryId;

        $t = Category::where('parent_id', $categoryId)
            ->get();

        // set to search all categories if cat is 99
        if ($categoryId == 0) {
            $categories = Category::all();
            foreach ($categories as $category) {
                array_push($categoryIds, $category->id);
            }
        } else {
            $categories = $this->getCategoriesByParentId($categoryId);
            foreach ($categories as $category) {
                array_push($categoryIds, $category['id']);
            }
        }
        // wine
        if ($categoryId === 4) {
            $topProductIds = '3188, 3745, 2353, 3128, 3249, 128, 13947, 2199, 12860, 508, 7226, 2802, 2180, 6151, 2189, 6132, 393, 528, 1284, 419, 191, 23, 8760, 12966, 681, 2180, 6151, 1078, 218, 506, 1078, 4020, 691, 3768, 7358, 11008, 661, 5960, 10108, 11294, 12853, 6387';
        }

        // white wine
        if ($categoryId === 7) {
            $topProductIds = '3128, 3249, 128, 241, 2199, 508, 681, 2189, 528, 191, 1314, 1011, 12381, 664, 4040, 12030, 13601, 508, 7844, 2177, 8040, 3120, 2576, 3766, 12940, 4683, 14234, 12934, 5839, 14920, 1274, 744, 5896, 4882, 5545, 12319, 2580, 8449, 3966';
        }

        // pilsner
        if ($categoryId === 168) {
            $topProductIds = '844';
        }

        // red wine
        if ($categoryId === 6) {
            $topProductIds = '3188, 2353, 13947, 7226, 2180, 6151, 393, 1284, 419, 239, 8760, 12966, 218, 1078, 3768, 7358, 12930, 1520,  4020, 506, 2244, 1916, 1173, 2800, 127, 812, 1216, 3551, 7345, 14418, 671, 2263, 8588, 3295, 4388, 2492, 8640, 621, 413, 139';
        }

        // coolers
        if ($categoryId === 5) {
            $topProductIds = '10889, 6833, 8207, 5594, 5581, 4713, 1510, 10871, 10798, 3870, 11534, 6174, 11534, 8198, 6659, 11382, 8214, 18449';
        }

        // spirits
        if ($categoryId === 3) {
            $topProductIds = '5349, 5326, 3711, 1855, 272, 6476, 2502, 8278, 2777, 2715, 2131, 7498, 9807, 17249, 4415, 2120, 11668, 2578';
        }

        // wheat
        if ($categoryId === 88) {
            $topProductIds = '4891';
        }

        // vodka
        if ($categoryId === 20) {
            $topProductIds = '4169, 7719, 2325, 1265, 2161, 5379, 940, 280, 8027,9996, 416, 1986, 2715, 13119, 12438, 197, 2502, 4515, 6456, 10661, 8338, 10236, 5879, 1881, 1237, 496, 4941, 4662, 5669, 201, 6732, 3854, 3853, 5883, 994, 1079, 2119';
        }

        // beer and cider
        if ($categoryId === 2) {
            $topProductIds = '1597, 9729, 8272, 8716, 12039, 8438, 296, 9044, 1808, 3210, 7784, 6604, 9415, 2421, 6849, 10601, 10949, 7197, 226, 4209, 11840, 11924, 14138, 2787, 7237, 7090, 7752, 3283, 4891, 844';
        }

        // rose
        if ($categoryId === 10) {
            $topProductIds = '6866, 1157, 1815, 1509, 352, 10763, 11117, 4785, 5660, 6867, 14929, 12911, 1142, 3031,4680, 3689, 3185, 3625, 2311, 8302';
        }

        // champagne
        if ($categoryId === 11) {
            $topProductIds = '7280, 5003, 1444, 8719, 408, 8735, 6932, 7863, 3389, 11950, 4557, 3692, 11325, 1714, 4989, 1015, 2066';
        }

        // sparkling wine
        if ($categoryId === 12) {
            $topProductIds = '2005, 3014, 13715, 7901, 2510, 8091, 2012, 319, 1177, 5222, 13912, 2524, 1963, 2035, 5542, 922, 12397, 1991, 11935, 14094, 6387';
        }

        // lager
        if ($categoryId === 27) {
            $topProductIds = '296, 9091, 10452, 11202, 8856, 9729, 9415, 7168, 7150, 8962, 3457, 3458, 4848, 1597, 293, 4639, 199, 1683, 13287, 3282, 844, 2178';
        }

        // ale
        if ($categoryId === 28) {
            $topProductIds = '909, 2178, 2349, 2766, 7804, 1238, 6317, 6604, 5010, 12039, 5332, 5502, 6856, 10601, 11903, 6851, 7752, 9044, 10835, 1544, 11754, 7752, 4891';
        }

        // whiskeys
        if ($categoryId === 19) {
            $topProductIds = '579, 1982, 7617, 990, 336, 3463, 231, 238, 346, 404, 331, 304, 225, 7813, 1026, 2974, 11185, 673, 5661, 5793';
        }

        // cognac/armagnac
        if ($categoryId === 18) {
            $topProductIds = '11389, 11255, 10156, 8921, 8455, 5681, 5019, 4998, 4318, 1629, 769, 590, 536, 310, 268, 245';
        }

        // tequila
        if ($categoryId === 23) {
            $topProductIds = '2160, 2711, 1581, 251, 463, 3682, 2785, 2163, 1579, 6403, 1577, 1576, 1116, 1189, 5032, 516, 517, 611, 633, 8278';
        }

        // liqeuer/liquor
        if ($categoryId === 9) {
            $topProductIds = '705, 1091, 687, 571, 2979, 1180, 1602, 1637, 11992, 7718, 2137, 2911, 1140, 374, 297, 249, 282, 244, 240, 291';
        }

        // rum
        if ($categoryId === 21) {
            $topProductIds = '316, 2565, 3921,4688, 247, 272, 652, 888, 725, 5464, 5465, 1040, 1439, 1611, 202, 228, 220, 2019, 2133, 248';
        }

        // gift card
        if ($categoryId === 34) {
            $topProductIds = '17322';
        }

        if ($filters) {
            foreach ($filters as $key => $value) {
                if ($value === null) {
                    array_push($filterNames, $key);
                } else {
                    array_push($filterNames, $value);
                }
            }

            if (count($filterNames) === 1) {
                $tag           = \App\Tag::where('slug', $filterNames)->first();
                $tagProductIds = $tag->products->pluck('id')->toArray();

                if ($tag->id === 2) {
                    $topProductIds = '156, 158, 107, 168, 116, 105, 167, 82, 127, 128';
                } elseif ($tag->id === 3) {
                    $topProductIds = '64, 84, 80, 155, 90, 63, 117, 76, 61, 81, 91';
                } elseif ($tag->id === 4) {
                    $topProductIds = '23, 11, 42, 4, 14, 50, 34, 32, 30, 18';
                } elseif ($tag->id === 1) {
                    $topProductIds = '95, 160, 169, 148, 89, 105, 151, 86, 139, 67';
                } elseif ($tag->id === 27) {
                    $topProductIds = '17815, 17813, 17814, 17811, 17812';
                } elseif ($tag->id === 28) {
                    $topProductIds = '5032, 331, 4792, 673, 1917';
                } elseif ($tag->id === 29) {
                    $topProductIds = '17808, 17802, 17806, 17802, 17801';
                } elseif ($tag->id === 30) {
                    $topProductIds = '3018, 4453, 16006, 528, 828, 6387';
                } elseif ($tag->id === 31) {
                    $topProductIds = '2178, 16817, 1597, 1683, 17069';
                } elseif ($tag->id === 47 || $tag->id === 40) {
                    $topProductIds = '7090, 7752, 3283, 4891, 844';
                } elseif ($tag->id === 45 || $tag->id === 39) {
                    $topProductIds = '661, 5960, 10108, 11294, 12853, 6387';
                } elseif ($tag->id === 46 || $tag->id === 41) {
                    $topProductIds = '17249, 4415, 2120, 11668, 2578';
                } elseif ($tag->id === 48 || $tag->id === 42) {
                    $topProductIds = '8198, 6659, 11382, 8214, 18449';
                } elseif ($tag->id === 5) {
                    $topProductIds = '198, 197, 196, 195, 194';
                }
            } else {
                foreach ($filterNames as $filterName) {
                    $tag           = \App\Tag::where('title', $filterName)->first();
                    $tagProducts[] = $tag->products->pluck('id')->toArray();
                    array_push($tags, $tag->id);
                }

                if (in_array(2, $tags)) {
                    $topProductIds = '156, 158, 107, 168, 116, 105, 167, 82, 127, 128';
                } elseif (in_array(1, $tags)) {
                    $topProductIds = '95, 160, 169, 148, 89, 105, 151, 86, 139, 67';
                } elseif (in_array(3, $tags)) {
                    $topProductIds = '64, 84, 80, 155, 90, 63, 117, 76, 61, 81, 91';
                } elseif (in_array(4, $tags)) {
                    $topProductIds = '23, 11, 42, 4, 14, 50, 34, 32, 30, 18';
                } elseif ($tag->id === 5) {
                    $topProductIds = '198, 197, 196, 195, 194';
                }
                foreach ($tagProducts as $product) {
                    $tagProductIds = array_merge($tagProductIds, $product);
                }
            }
        }

        if (! $sort) {
            $s = ['title', 'asc'];
        } else {
            $topProductIds = null;
            $s             = explode(':', $sort);

            if ($s[0] === 'price') {
                $s[0] = 'runner_price';
            }
        }

        if ($productId === null) {
            // is website and not app
            if ($isWeb) {
                $query = $inventory->productsAll()
                    ->whereIn('category_id', $categoryIds)
                    ->where('is_active', '!=', 0)
                    ->when($tagProductIds, function ($query, $tagProductIds) {
                        return $query->whereIn('products.id', $tagProductIds);
                    })
                    ->when($topProductIds, function ($query, $topProductIds) {
                        return $query->orderByRaw('FIELD(products.id,' . $topProductIds . ') desc');
                    })
                    ->orderBy($s[0], $s[1]);
            } else {
                $query = $inventory->productsInStock()
                    ->whereIn('category_id', $categoryIds)
                    ->where('is_active', '!=', 0)
                    ->when($tagProductIds, function ($query, $tagProductIds) {
                        return $query->whereIn('products.id', $tagProductIds);
                    })
                    ->when($topProductIds, function ($query, $topProductIds) {
                        return $query->orderByRaw('FIELD(products.id,' . $topProductIds . ') desc');
                    })
                    ->orderBy($s[0], $s[1]);
            }

            if ($limit) {
                $products = $query->limit($limit)->get();
            } else {
                $products = $query->paginate(20);
            }

            return new ProductsCollection($products);
        } else {
            if ($isWeb) {
                $product = $inventory->productsAll()->find($productId);
            } else {
                $product = $inventory->productsInStock()->find($productId);
            }

            return new ProductResource($product);
        }
    }
}
