<?php

namespace App\Http\Controllers\Runner\V3;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewOrder as NewOrderResource;
use App\Http\Resources\NewOrdersCollection;
use App\Http\Resources\NewUser as NewUserResource;
use App\Order;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function show(Request $request, $userId)
    {
        $user = Auth::user();

        return new NewUserResource($user);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $formattedRequest = [];
        $data = $request->input('data.attributes');

        if (isset($data['firstName'])) {
            $formattedRequest['first_name'] = $data['firstName'];
        }
        if (isset($data['lastName'])) {
            $formattedRequest['last_name'] = $data['lastName'];
        }
        if (isset($data['phoneNumber'])) {
            $formattedRequest['phone_number'] = $data['phoneNumber'];
        }
        if (isset($data['email'])) {
            $formattedRequest['email'] = $data['email'];
        }
        if (isset($data['fcmToken'])) {
            $formattedRequest['fcm_token'] = $data['fcmToken'];
        }
        if (isset($data['avatarImage'])) {
            $formattedRequest['avatar_image'] = $data['avatarImage'];
        }
        if (isset($data['promoNotification'])) {
            $formattedRequest['promo_notification'] = $data['promoNotification'];
        }
        if (isset($data['data'])) {
            $formattedRequest['data'] = $data['data'];
        }
        if (isset($data['newPassword'])) {
            if (Hash::check($data['oldPassword'], $user->password)) {
                $formattedRequest['password'] = bcrypt($data['newPassword']);
            } else {
                return response()->json([
                    'errors' => [
                        [
                            'status' => 403,
                            'title'  => 'Invalid Password',
                            'detail' => 'The password you gave does not match your old password.',
                        ],
                    ],
                ], 403);
            }
        }

        $detail = 'Whoops, looks like there was an error. Please contact our support line for help.';

        try {
            $user->update($formattedRequest);
        } catch (\Exception $e) {
            return $e;
            if ($e->errorInfo[1] === 1062) {
                if (strpos($e->errorInfo[2], $formattedRequest['phone_number'])) {
                    $detail = 'Whoops, looks like that phone number is already in use.';
                }
                if (strpos($e->errorInfo[2], $formattedRequest['email'])) {
                    $detail = 'Whoops, looks like that email address is already in use.';
                }
            }

            return response()->json([
                'errors' => [
                    [
                        'status' => 403,
                        'title'  => 'Invalid Entry',
                        'detail' => $detail,
                    ],
                ],
            ], 403);
        }
    }

    public function getOrders($userId, $orderId = null)
    {
        $user = Auth::user();

        if ($orderId !== null) {
            $order = Order::find($orderId);

            return new NewOrderResource($order);
        } else {
            $orders = Order::where('user_id', $user->id)
                                ->paginate();

            return new NewOrdersCollection($orders);
        }
    }
}
