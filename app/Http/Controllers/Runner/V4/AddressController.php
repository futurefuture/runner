<?php

namespace App\Http\Controllers\Runner\V4;

use App\Actions\GetStoresValidatedByPostalCodeAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function validatePostalCode(Request $request, GetStoresValidatedByPostalCodeAction $action)
    {
        $isWeb = false;

        if ($request->input('isWeb')) {
            $isWeb = true;
        }

        $stores          = $action->execute($request->input('postalCode'), $isWeb);
        $formattedStores = [];

        // create custom resource
        foreach ($stores as $s) {
            $formattedStore = [
                'type'       => 'validated stores by postal code',
                'id'         => (string) $s['id'],
                'attributes' => [
                    'title'            => $s['title'],
                    'index'            => $s['index'],
                    'storeLogo'        => $s['options']['storeLogo'] ?? null,
                    'invertedLogo'     => $s['options']['invertedLogo'] ?? false,
                    'navigationLogo'   => $s['options']['navigationLogo'] ?? null,
                    'hours'            => $s['options']['hours'] ?? null,
                    'blockImage'       => $s['options']['blockImage'] ?? null,
                    'nextDeliveryTime' => $s['options']['nextDeliveryTime'] ?? null,
                    'aboutText'        => $s['options']['aboutText'] ?? null,
                    'subDomain'        => $s['sub_domain'],
                    'inventoryId'      => (string) $s['inventory_id'],
                    'categories'       => isset($s['options']['categories']) ? $s['options']['categories'] : null,
                ],
            ];

            array_push($formattedStores, $formattedStore);
        }

        return response()->json([
            'data' => $formattedStores,
        ]);
    }
}
