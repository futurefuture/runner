<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\Product as ProductResource;
use App\Product;

class ProductController extends Controller
{
    /**
     * Gets product.
     *
     * @param string $query
     * @return array
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }
}
