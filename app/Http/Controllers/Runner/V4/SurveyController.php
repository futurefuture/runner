<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\Survey;
use App\Http\Resources\Runner\Survey as SurveyResource;
use App\Http\Resources\Runner\SurveyCollection;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Get all surveys.
     *
     *
     * @return Collection
     */
    public function index()
    {
        $surveys = Survey::get();

        return new SurveyCollection($surveys);
    }

    /**
     * Get one survey.
     *
     * @param Survey $survey
     *
     * @return Resource
     */
    public function show(Survey $survey)
    {
        return new SurveyResource($survey);
    }
}
