<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\RewardPointCollection;
use App\RewardPoint;
use App\User;

class UserRewardPointController extends Controller
{
    public function index(User $user)
    {
        $rewardPoints = RewardPoint::where('user_id', $user->id)->get();

        return new RewardPointCollection($rewardPoints);
    }
}
