<?php

namespace App\Http\Controllers\Runner\V4;

use App\Ad;
use App\Tag;
use App\Store;
use App\Layout;
use App\Carousel;
use App\Inventory;
use Carbon\Carbon;
use App\PostalCode;
use App\StoreLayout;
use Illuminate\Http\Request;
use App\LayoutStoreTerritory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Jobs\TrackAdImpression;
use App\Http\Resources\Runner\Store as StoreResource;

class StoreController extends Controller
{
    /**
     * Gets store by id
     */
    public function show(Request $request, Store $store): StoreResource
    {
        $postalCodeFirstThree = substr($request->input('postalCode'), 0, 3);
        $postalCode           = PostalCode::find($postalCodeFirstThree);

        if ($postalCode) {
            $layoutStoreTerritory = LayoutStoreTerritory::where('store_id', $store->id)
                ->where('territory_id', $postalCode->territory->id)->first();
            $layout                     = Layout::find($layoutStoreTerritory->layout_id);
            $componentContents          = $layout->components->pluck('content');
            $formattedComponentContents = [];

            foreach ($componentContents as $cc) {
                $storeId = isset($cc['attributes']['link']['storeId']) ? $cc['attributes']['link']['storeId'] : 1;

                $inventory = DB::table('inventories')
                    ->join('inventory_territory', 'inventories.id', '=', 'inventory_territory.inventory_id')
                    ->join('territories', 'territories.id', '=', 'inventory_territory.territory_id')
                    ->select('inventories.*')
                    ->where('territories.id', $postalCode->territory->id)
                    ->where('inventories.store_id', $storeId)
                    ->first();

                // featured product
                // if ($cc['id'] == 1) {
                //     $filter = $cc['attributes']['link']['filters'][0];
                //     $tag    = Tag::find($filter['id']);

                //     if (count($tag->ads()->active()->get()) > 0) {
                //         TrackAdImpression::dispatch('tagProductImpression', $tag)->onQueue('analytics');
                //     }
                // }

                // check for carouselId
                if (isset($cc['attributes']['carouselId'])) {
                    $carousel = Carousel::where('id', $cc['attributes']['carouselId'])
                        ->with([
                            'carouselComponents',
                            'carouselComponents.ads'
                        ])
                        ->first();
                    $carouselComponents = $carousel->carouselComponents;

                    $cc['attributes']['carousel'] = [];

                    foreach ($carouselComponents as $d) {
                        $e        = $d->content;
                        $e['ads'] = $d->ads;

                        array_push($cc['attributes']['carousel'], $e);
                    }
                }

                array_push($formattedComponentContents, json_decode(preg_replace('/{{inventoryId}}/', $inventory->id, json_encode($cc))));
            }

            $store->layout = $formattedComponentContents;
        } else {
            $storeLayout = StoreLayout::find(2);

            $store->layout = $storeLayout->layout;
        }

        return new StoreResource($store);
    }

    /**
     * Get available inventory times by inventory
     */
    public function getAvailableDeliveryTimesByInventory(Inventory $inventory)
    {
        $availableTimeSlots = [];
        $inventoryHours     = $inventory->options['hours'];
        $now                = Carbon::now();
        $currentHour        = $now->hour;
        $aMonthFromNow      = Carbon::now()->addDays(30);
        $dates              = $this->generateDateRange(Carbon::now(), $aMonthFromNow);

        if ($inventory->id === 11 || $inventory->id === 23) {
            $eleven = Carbon::createFromTime(11, 0, 0);
            $four   = Carbon::createFromTime(16, 0, 0);

            foreach ($dates as $key => $val) {
                $regularTimeSlot = [];

                if ($key == 0) {
                    $date = $now->toDateString();

                    // if (Carbon::now()->lessThanOrEqualTo($eleven)) {
                    //     // $regularTimeSlot[0] = [
                    //     //     'hour'         => 12,
                    //     //     'interval'     => 'Between 12pm and 5pm',
                    //     //     'rewardPoints' => null,
                    //     //     'regularPrice' => 999,
                    //     //     'salePrice'    => 999,
                    //     // ];
                    //     $regularTimeSlot[0] = [
                    //         'hour'         => 17,
                    //         'interval'     => 'Between 5pm and 9pm',
                    //         'rewardPoints' => null,
                    //         'regularPrice' => 999,
                    //         'salePrice'    => 999,
                    //     ];
                    // } elseif (Carbon::now()->lessThanOrEqualTo($four)) {
                    //     $regularTimeSlot[0] = [
                    //         'hour'         => 17,
                    //         'interval'     => 'Between 5pm and 9pm',
                    //         'rewardPoints' => null,
                    //         'regularPrice' => 999,
                    //         'salePrice'    => 999,
                    //     ];
                    // } else {
                    $regularTimeSlot = [];
                    // }

                    $availableTimeSlots[$key] = [
                        'type'             => 'available time slots',
                        'id'               => sprintf($key + 1),
                        'date'             => $date,
                        'regularTimeSlots' => array_values($regularTimeSlot),
                    ];
                } else {
                    $date                = $now->addDays(1)->toDateString();
                    $futureStoreTime     = $inventoryHours[$val];
                    $storeVar            = $this->getHoursClosedUntil($futureStoreTime, $key);
                    $newHoursUntilClosed = $storeVar['hoursUntilClosed'];
                    $futureHour          = $storeVar['hourOpen'];
                    $futureTimeSlot      = [];

                    // $regularTimeSlot[0] = [
                    //     'hour'         => 12,
                    //     'interval'     => 'Between 12pm and 5pm',
                    //     'rewardPoints' => null,
                    //     'regularPrice' => 999,
                    //     'salePrice'    => 999,
                    // ];
                    $regularTimeSlot[0] = [
                        'hour'         => 17,
                        'interval'     => 'Between 5pm and 9pm',
                        'rewardPoints' => null,
                        'regularPrice' => 999,
                        'salePrice'    => 999,
                    ];

                    $availableTimeSlots[$key] = [
                        'type'               => 'available time slots',
                        'id'                 => sprintf($key + 1),
                        'date'               => $date,
                        'regularTimeSlots'   => array_values($regularTimeSlot),
                    ];
                }
            }
        } else {
            foreach ($dates as $key => $val) {
                $regularTimeSlot = [];

                if ($key == 0) {
                    $storeTime        = $inventoryHours[$val];
                    $storeVar         = $this->getHoursClosedUntil($storeTime, $key);
                    $hoursUntilClosed = $storeVar['hoursUntilClosed'];

                    if ($now->greaterThan($storeVar['lastHour']) || $storeVar['hourOpen'] == $storeVar['hourClosed']) {
                        $hoursUntilClosed = 0;
                    }
                    $hour = $currentHour < $storeVar['hourOpen'] ? $storeVar['hourOpen'] : $currentHour;
                    $date = $now->toDateString();

                    //case when time is 12 midnight to 11 in morning
                    $timezone = 'America/Toronto';
                    $tomorrow = Carbon::parse('tomorrow 12am', $timezone);

                    if ($now->hour >= 0 && $now->hour < $tomorrow->hour) {
                        $storeTime        = $inventoryHours[$now->dayOfWeek];
                        $storeVar         = $this->getHoursClosedUntil($storeTime, 1);
                        $hoursUntilClosed = $storeVar['hoursUntilClosed'];
                    }

                    if ($hoursUntilClosed >= 1) {
                        for ($i = 0; $i < $hoursUntilClosed; $i++) {
                            if ($i == 0) {
                                $regularTimeSlot[$i] = [
                                    'hour'         => $hour,
                                    'interval'     => '3 hours or less',
                                    'rewardPoints' => null,
                                    'regularPrice' => 999,
                                    'salePrice'    => 999,
                                ];
                            } else {
                                $regularTimeSlot[$i] = [
                                    'hour'         => $hour,
                                    'interval'     => Carbon::createFromTime($hour, 0, 0)->format('ga') . ' - ' . Carbon::createFromTime($hour + 3, 0, 0)->format('ga'),
                                    'rewardPoints' => null,
                                    'regularPrice' => 999,
                                    'salePrice'    => 999,
                                ];
                            }

                            $hour++;
                        }
                    } else {
                        $regularTimeSlot = [];
                    }

                    $availableTimeSlots[$key] = [
                        'type'             => 'available time slots',
                        'id'               => sprintf($key + 1),
                        'date'             => $date,
                        'regularTimeSlots' => array_values($regularTimeSlot),
                    ];
                } else {
                    $date                = $now->addDays(1)->toDateString();
                    $futureStoreTime     = $inventoryHours[$val];
                    $storeVar            = $this->getHoursClosedUntil($futureStoreTime, $key);
                    $newHoursUntilClosed = $storeVar['hoursUntilClosed'];
                    $futureHour          = $storeVar['hourOpen'];
                    $futureTimeSlot      = [];

                    if ($storeVar['hourOpen'] == $storeVar['hourClosed']) {
                        $newHoursUntilClosed = 0;
                        $futureTimeSlot      = [];
                    } else {
                        for ($x = 0; $x < $newHoursUntilClosed; $x++) {
                            $futureTimeSlot[$x] = [
                                'hour'         => $futureHour,
                                'interval'     => Carbon::createFromTime($futureHour, 0, 0)->format('ga') . ' - ' . Carbon::createFromTime($futureHour + 3, 0, 0)->format('ga'),
                                'rewardPoints' => null,
                                'regularPrice' => 999,
                                'salePrice'    => 999,
                            ];

                            $futureHour++;
                        }
                    }
                    $availableTimeSlots[$key] = [
                        'type'               => 'available time slots',
                        'id'                 => sprintf($key + 1),
                        'date'               => $date,
                        'regularTimeSlots'   => array_values($futureTimeSlot),
                    ];
                }
            }
        }

        return response()->json([
            'data' => $availableTimeSlots,
        ]);
    }

    /**
     * Function to generate all dates for 1 month.
     */
    private function generateDateRange(Carbon $start_date, Carbon $end_date): array
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->dayOfWeek;
        }

        return $dates;
    }

    /**
     * Function to get all store parameters.
     */
    private function getHoursClosedUntil($storeTime, $key): array
    {
        $hourExp               = explode('-', $storeTime);
        $hourOpen              = (int) $hourExp[0];
        $hourClosed            = $hourExp[1];
        $closedHour            = (int) explode(':', $hourClosed)[0];
        $closedMin             = (int) explode(':', $hourClosed)[1];
        $lastHour              = Carbon::createFromTime($closedHour, $closedMin);
        $currentHour           = Carbon::now()->hour;
        $currentHourInCarbon   = $key == 0 ? Carbon::createFromTime($currentHour) : Carbon::createFromTime($hourOpen);
        $hoursUntilClosedInMin = $currentHourInCarbon->diffInMinutes($lastHour);
        $hoursUntilClosed      = (int) (ceil($hoursUntilClosedInMin / 60));

        return $storeVar[] = [
            'hourOpen'         => $hourOpen,
            'hourClosed'       => $closedHour,
            'lastHour'         => $lastHour,
            'hoursUntilClosed' => $hoursUntilClosed,
        ];
    }
}
