<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\Refund;
use App\Http\Resources\Runner\Refund as RefundResource;
use App\Http\Resources\Runner\RefundCollection;
use Illuminate\Http\Request;

class RefundController extends Controller
{
    /**
     * Get all refunds.
     *
     * @param string $query
     *
     * @return array
     */
    public function index()
    {
        $refunds = Refund::get();

        return new RefundCollection($refunds);
    }

    /**
     * Get one refund.
     *
     * @param string $query
     *
     * @return array
     */
    public function show(Refund $refund)
    {
        return new RefundResource($refund);
    }

    /**
     * Updates refund.
     *
     * @param Request $request
     * @param string  $query
     *
     * @return array
     */
    public function update(Request $request, Refund $refund)
    {
        $data = $request->input('data.attributes');

        if (isset($data['orderId'])) {
            $refund->order_id = $data['orderId'];
        }
        if (isset($data['userId'])) {
            $refund->user_id = $data['userId'];
        }

        if (isset($data['refundId'])) {
            $refund->refund_id = $data['refundId'];
        }

        if (isset($data['chargeId'])) {
            $refund->charge_id = $data['chargeId'];
        }

        if (isset($data['amount'])) {
            $refund->amount = $data['amount'];
        }

        if (isset($data['reason'])) {
            $refund->reason = $data['reason'];
        }

        $refund->save();

        return new RefundResource($refund);
    }

    /**
     * Updates refund.
     *
     * @param Request $request
     * @param string  $query
     *
     * @return array
     */
    public function delete(Refund $refund)
    {
        $refund->delete();

        return response()->json([
            'success' => 'order deleted.',
        ]);
    }
}
