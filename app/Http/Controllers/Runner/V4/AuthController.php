<?php

namespace App\Http\Controllers\Runner\V4;

use App\Actions\CreateUserAction;
use App\Actions\LoginUserAction;
use App\Actions\LogoutUserAction;
use App\Actions\MailgunValidateEmailAction;
use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Socialite;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request, MailgunValidateEmailAction $mgAction, CreateUserAction $action)
    {
        $utilities            = new Utilities();
        $data                 = $utilities->changeCamelCaseToSnakeCase($request->input('data.attributes'));
        $email                = $data['email'];

        $validator = Validator::make($data, [
            'email'         => 'required|email|max:255|unique:users,email,NULL',
            'phone_number'  => 'required|unique:users,phone_number,NULL',
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'date_of_birth' => 'required|date|before:19 years ago',
            'is_legal_age'  => 'required',
            'address'       => 'required',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors' => $errors,
            ], 401);
        }

        try {
            $mgAction->execute($email);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 401);
        }

        try {
            $user = $action->execute($data);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 401);
        }

        // check if brand commerce registration
        if (isset($data['source'])) {
            return response()->json([
                'data' => [
                    'type'       => 'brand commerce registration',
                    'attributes' => [
                        'user'        => [
                            'id'        => (string) $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                    ],
                ],
            ]);
        } else {
            $accessToken = $user->createToken('Runner')
                ->accessToken;

            return response()->json([
                'data' => [
                    'type'       => 'access tokens',
                    'attributes' => [
                        'accessToken' => $accessToken,
                        'user'        => [
                            'id'        => (string) $user->id,
                            'firstName' => $user->first_name,
                            'lastName'  => $user->last_name,
                        ],
                    ],
                ],
            ]);
        }
    }

    public function login(Request $request, LoginUserAction $action)
    {
        $data        = $request->input('data.attributes');
        $socialLogin = [];
        $email       = $data['email'] ?? null;
        $password    = $data['password'] ?? null;
        $anonymousId = $data['anonymousId'];

        if (isset($data['googleId'])) {
            $socialLogin = [
                'type' => 'googleId',
                'id'   => $data['googleId'],
            ];
        } elseif (isset($data['facebookId'])) {
            $socialLogin = [
                'type' => 'facebookId',
                'id'   => $data['facebookId'],
            ];
        } elseif (isset($data['appleId'])) {
            $socialLogin = [
                'type' => 'appleId',
                'id'   => $data['appleId'],
            ];
        }

        if (empty($socialLogin)) {
            $validator = Validator::make($request->input('data.attributes'), [
                'email'    => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                $errors = json_decode($validator->errors()->toJson());

                foreach ($errors as $key => $value) {
                    $temp = $value[0];

                    break;
                }

                $errors->error = $temp;

                return response()->json([
                    'errors' => $errors,
                ], 401);
            }
        }

        try {
            $userInfo = $action->execute($email, $password, $socialLogin, $anonymousId);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 401);
        }

        return response()->json([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => $userInfo['accessToken'],
                    'user'        => [
                        'id'         => (string) $userInfo['id'],
                        'firstName'  => $userInfo['first_name'],
                        'lastName'   => $userInfo['last_name'],
                        'googleId'   => $userInfo['google_id'] ?? null,
                        'facebookId' => $userInfo['facebook_id'] ?? null,
                        'appleId'    => $userInfo['apple_id'] ?? null,
                    ],
                ],
            ],
        ]);
    }

    public function logout(LogoutUserAction $action)
    {
        $user = Auth::user();

        try {
            $action->execute($user);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e,
                ],
            ], 401);
        }
    }

    public function redirectToProvider($socialPlatform)
    {
        return Socialite::driver($socialPlatform)
            ->scopes([
                'email',
            ])
            ->stateless()
            ->redirect();
    }

    public function handleProviderCallback($socialPlatform)
    {
        $socialPlatformUser = Socialite::driver($socialPlatform)
            ->stateless()
            ->user();

        if ($socialPlatform == 'facebook') {
            // check if user exists to login or register
            if (User::where('facebook_id', $socialPlatformUser->id)->first()) {
                $user = User::where('facebook_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', ['secret' => $secret]);
            } else {
                $firstName = explode(' ', $socialPlatformUser->name)[0];
                $lastName  = explode(' ', $socialPlatformUser->name)[1];

                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'facebook_id' => $socialPlatformUser->id,
                        'avatar'      => $socialPlatformUser->avatar,
                    ]);
                } else {
                    $user = (object) [
                        'facebookId' => $socialPlatformUser->id,
                        'firstName'  => $firstName,
                        'lastName'   => $lastName,
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@facebook.com',
                        'avatar'     => $socialPlatformUser->avatar,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@newRegister', [
                        'secret' => $secret,
                    ]);
                }
            }
        } elseif ($socialPlatform == 'google') {
            // check if user exists to login or register
            if (User::where('google_id', $socialPlatformUser->id)->first()) {
                $user = User::where('google_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', [
                    'secret' => $secret,
                ]);
            } else {
                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'google_id' => $socialPlatformUser->id,
                        'avatar'    => $socialPlatformUser->avatar_original,
                    ]);
                } else {
                    $user = (object) [
                        'googleId'   => $socialPlatformUser->id,
                        'firstName'  => $socialPlatformUser->user['name']['givenName'] ?? 'firstName',
                        'lastName'   => $socialPlatformUser->user['name']['familyName'] ?? $socialPlatformUser->id,
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@googleplus.com',
                        'avatar'     => $socialPlatformUser->avatar_original,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@newRegister', [
                        'secret' => $secret,
                    ]);
                }
            }
        }
    }

    public function tokenGrant(Request $request)
    {
        $baseUrl = env('APP_URL');
        $data    = $request->input('data.attributes');
        $http    = new Client([
            'verify' => false
        ]);
        try {
            $response = $http->post($baseUrl . '/oauth/token', [
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => $data['clientId'],
                    'client_secret' => $data['clientSecret'],
                    'username'      => $data['email'],
                    'password'      => $data['password'],
                    'scope'         => ''
                ],
            ]);

            $tempRes            = json_decode((string) $response->getBody(), true);
            $userId             = User::where('email', $data['email'])->first()->id;
            $tempRes['user_id'] = $userId;

            return response()->json($tempRes);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return $e;
        }
    }
}
