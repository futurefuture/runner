<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\SurveyResponse;
use App\SurveyOption;
use App\Survey;
use App\Http\Resources\Runner\SurveyResponse as SurveyResponseResource;
use App\Http\Resources\Runner\SurveyResponseCollection;
use App\Actions\CreateSurveyResponseAction;
use Illuminate\Http\Request;

class SurveyResponseController extends Controller
{
    /**
     * Helper function for returning the wanted data structure.
     *
     * @param Array $surveyRes
     * @return Array
     */
    public function returnSurveyResArr($surveyRes)
    {
        $formattedReturn = [];
        $i               = 0;
        foreach ($surveyRes as $res) {
            array_push($formattedReturn, $res);
            $i++;
        }

        return [
            'type'              => 'survey response',
            'attributes'        => $formattedReturn,
        ];
    }

    /**
     * Helper function for structuring the create() return.
     *
     * @param SurveyOption $option
     * @param Number $count
     * @return Array
     */
    private function restructureSurveyArr($option, $count)
    {
        return [
            'surveyOptionId' => (string) $option->id,
            'title'          => $option->title,
            'percentage'     => $count * 100,
        ];
    }

    /**
     * Creates a survey response given by a user.
     *
     * @param Request $request
     * @param CreateSurveyResponseAction $action
     *
     * @return Collection
     */
    public function create(Request $request, CreateSurveyResponseAction $action, Survey $survey)
    {
        $data = $request->input('data.attributes');

        $action->execute($data);

        // gets count of the survey option
        $surveyResponses = SurveyResponse::get()
            ->where('survey_id', $survey->id)
            ->groupBy(function ($surveyRes) {
                return $surveyRes->survey_option_id;
            })->map(function ($option) {
                return $option->count();
            });

        $optionIdArr = array_keys($surveyResponses->toArray());
        $totalRes    = 0;
        $j           = 0;

        foreach ($optionIdArr as $c) {
            $totalRes += $surveyResponses[$c];
            $j++;
        }

        $formattedSurveyRes = [];
        $i                  = 0;

        foreach ($surveyResponses as $res) {
            $option = SurveyOption::find($optionIdArr[$i]);

            if ($totalRes > 0) {
                array_push($formattedSurveyRes, ($this->restructureSurveyArr($option, ($surveyResponses->toArray()[$optionIdArr[$i]]) / $totalRes)));
            }

            $i++;
        }

        $this->returnSurveyResArr($formattedSurveyRes);

        return response()->json([
            'data' => $this->returnSurveyResArr($formattedSurveyRes),
        ]);
    }

    /**
     * Get all surveys.
     *
     *
     * @return Collection
     */
    public function index()
    {
        $surveyResponses = SurveyResponse::get();

        return new SurveyResponseCollection($surveyResponses);
    }

    /**
     * Get one survey.
     *
     * @param SurveyResponse $surveyResponse
     *
     * @return Resource
     */
    public function show(SurveyResponse $surveyResponse)
    {
        return new SurveyResponseResource($surveyResponse);
    }
}
