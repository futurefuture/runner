<?php

namespace App\Http\Controllers\Runner\V4;

use App\Review;
use App\Product;
use App\Classes\Utilities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use App\Actions\CreateProductReviewAction;
use App\Http\Resources\Runner\ReviewCollection;
use App\Http\Resources\Runner\Review as ReviewResource;

class ProductReviewController extends Controller
{
    /**
     * Gets reviews according to product id.
     *
     * @param Request $request
     * @return ReviewCollection
     */
    public function index(Product $product)
    {
        $query = $product->reviews()
            ->where('is_approved', true)
            ->whereHas('user')
            ->getQuery();

        $reviews = QueryBuilder::for($query)
            ->allowedSorts([
                AllowedSort::field('value'),
                AllowedSort::field('helpful'),
                AllowedSort::field('createdAt', 'created_at'),
            ])
            ->get();

        return new ReviewCollection($reviews);
    }

    /**
     * Creates review according to product id for user.
     *
     * @param Request $request
     * @return ReviewCollection
     */
    public function create(Request $request, Product $product, CreateProductReviewAction $action)
    {
        $data   = $request->input('data.attributes');
        $review = $action->execute($product, $data);

        return new ReviewResource($review);
    }

    public function update(Request $request, $productId, $reviewId)
    {
        $data      = $request->input('data');
        $review    = Review::find($reviewId);

        if (isset($data['attributes']['helpful'])) {
            $review->helpful += 1;
            $review->save();
        } elseif (isset($data['attributes']['unHelpful'])) {
            $review->unhelpful += 1;
            $review->save();
        }

        return new ReviewResource($review);
    }
}
