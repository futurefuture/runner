<?php

namespace App\Http\Controllers\Runner\V4;

use App\User;
use Illuminate\Http\Request;
use App\Services\StripeService;
use App\Http\Controllers\Controller;
use App\Actions\CreateCustomerSourceAction;
use App\Actions\DeleteCustomerSourceAction;
use App\Actions\UpdateCustomerSourceAction;

class UserStripeController extends Controller
{
    public function getCustomer(User $user)
    {
        $stripeService  = resolve(StripeService::class);
        $stripeCustomer = $stripeService->getCustomer($user);

        return response()->json([
            'data' => [
                'type'       => 'stripe customers',
                'id'         => $user->stripe_id,
                'attributes' => $stripeCustomer,
            ],
        ]);
    }

    public function createSource(Request $request, User $user, CreateCustomerSourceAction $action)
    {
        $zipMessage = 'The Zip Code associated with your card is incorrect. If you aleady have a card added, please delete and re-enter your credit card information.';
        $data       = $request->input('data');

        try {
            $action->execute($user, $data);
        } catch (\Exception $e) {
            $error = $e->getMessage();

            if (strpos($error, 'zip') == true) {
                return response()->json([
                    'errors' => [
                        'error' => $zipMessage,
                    ],
                ], 400);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => $error,
                    ],
                ], 400);
            }
        }

        return $this->getCustomer($user);
    }

    public function updateDefaultSource(User $user, $sourceId, UpdateCustomerSourceAction $action)
    {
        try {
            $action->execute($user, $sourceId);
            // $customer                 = \Stripe\Customer::retrieve($user->stripe_id);
            // $customer->default_source = $sourceId;
            // $customer->save();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return response()->json([
                'errors' => [
                    'error' => $err['message'] . ' { CODE: ' . $err['code'] . ' } ',
                ],
            ], 400);
        } catch (\Stripe\Error\ApiConnection $e) {
            return response()->json([
                'errors' => [
                    'error' => 'We are experiencing difficulty processing your card, please refresh the page or contact customer service.',
                ],
            ]);
        }

        return $this->getCustomer($user);
    }

    public function deleteSource(User $user, $sourceId, DeleteCustomerSourceAction $action)
    {
        try {
            $action->execute($user, $sourceId);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 400);
        }

        return $this->getCustomer($user);
    }
}
