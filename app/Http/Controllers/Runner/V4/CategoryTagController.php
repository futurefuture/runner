<?php

namespace App\Http\Controllers\Runner\V4;

use App\Category;
use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\CategoryTag as CategoryTagResource;
use App\Http\Resources\Runner\CategoryTagCollection;
use App\Tag;
use Illuminate\Http\Request;

class CategoryTagController extends Controller
{
    public function index($categoryId)
    {
        $user = Category::find($categoryId)->tags()->isActive()->isVisible()->paginate(10);

        return new CategoryTagCollection($user);
    }
}
