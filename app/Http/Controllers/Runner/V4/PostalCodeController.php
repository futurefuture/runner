<?php

namespace App\Http\Controllers\Runner\V4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\CreateOutOfBoundsPostalCodeAction;

class PostalCodeController extends Controller
{
    public function storeOutOfBoundsPostalCode(Request $request, CreateOutOfBoundsPostalCodeAction $action)
    {
        $data = $request->input('data.attributes');

        $outOfBoundsPostalCode = $action->execute($data);

        return response()->json([
            'data' => [
                'id'         => $outOfBoundsPostalCode->id,
                'type'       => 'out of bounds postal codes',
                'attributes' => [
                    'postalCode' => $outOfBoundsPostalCode->postal_code,
                ],
            ],
        ]);
    }
}
