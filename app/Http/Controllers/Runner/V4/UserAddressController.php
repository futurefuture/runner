<?php

namespace App\Http\Controllers\Runner\V4;

use App\User;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\UserAddress\CreateUserAddress;
use App\Actions\DeleteUserAddressAction;
use App\Actions\UpdateUserAddressAction;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Runner\AddressCollection;
use App\Http\Resources\Runner\Address as AddressResource;

class UserAddressController extends Controller
{
    public function create(Request $request, User $user, CreateUserAddress $action)
    {
        $data = $request->input('data.attributes');

        $validator = Validator::make($data, [
            'formattedAddress'  => 'required',
            'lat'               => 'required',
            'lng'               => 'required',
            'postalCode'        => 'required',
        ]);

        if ($validator->fails()) {
            $errors = json_decode($validator->errors()->toJson());

            foreach ($errors as $key => $value) {
                $temp = $value[0];

                break;
            }

            $errors->error = $temp;

            return response()->json([
                'errors' => $errors,
            ], 401);
        }

        $address = $action->execute($user, $data);

        return new AddressResource($address);
    }

    public function index(User $user)
    {
        $addresses = Address::where('user_id', $user->id)->get();

        return new AddressCollection($addresses);
    }

    public function update(Request $request, User $user, Address $address, UpdateUserAddressAction $action)
    {
        $data    = $request->input('data.attributes');
        $address = $action->execute($user, $address, $data);

        return new AddressResource($address);
    }

    public function delete(User $user, Address $address, DeleteUserAddressAction $action)
    {
        $addresses = $action->execute($user, $address);

        return new AddressCollection($addresses);
    }
}
