<?php

namespace App\Http\Controllers\Runner\V4;

use App\Category;
use App\Http\Controllers\Controller;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    public function index()
    {
        return (new CategoryService())->index();
    }

    public function show(Category $category)
    {
        return (new CategoryService())->show($category);
    }

    public function getChildCategory($categoryId)
    {
        $categoriesChild = (new CategoryService())->getAllChildren($categoryId);

        return $categoriesChild;
    }

    public function getParentCategory($categoryId)
    {
        return (new CategoryService())->getAllParents($categoryId);
    }
}
