<?php

namespace App\Http\Controllers\Runner\V4;

use App\User;
use App\Store;
use App\Product;
use App\Favourite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\CreateUserFavouriteAction;
use App\Actions\DeleteUserFavouriteAction;
use App\Http\Resources\Runner\FavouriteCollection;
use App\Http\Resources\Runner\Product as ProductResource;
use App\Http\Resources\Runner\Favourite as FavouriteResource;

class UserFavouriteController extends Controller
{
    public function index(User $user)
    {
        $favourites = Favourite::where('user_id', $user->id)->get();

        return new FavouriteCollection($favourites);
    }

    public function create(Request $request, User $user, CreateUserFavouriteAction $action)
    {
        $data      = $request->input('data.attributes');
        $favourite = $action->execute($user, $data);

        return new FavouriteResource($favourite);
    }

    public function delete(User $user, Favourite $favourite, DeleteUserFavouriteAction $action)
    {
        $action->execute($favourite);
    }

    public function indexByStore(User $user)
    {
        $favourites = Favourite::where('user_id', $user->id)->with([
            'product',
            'product.productImages',
            'product.reviews',
            'product.incentives',
            'product.partners'
        ])->get();
        $storeIds            = [];
        $formattedFavourites = [];

        foreach ($favourites as $f) {
            array_push($storeIds, $f->store_id);
        }

        $cleanedStoreIds = array_unique($storeIds);

        foreach ($cleanedStoreIds as $c) {
            $store          = Store::find($c);
            $formattedStore = [
                'id'        => (string) $store->id,
                'title'     => $store->title,
                'storeLogo' => $store->options['storeLogo'],
                'products'  => [],
            ];

            foreach ($favourites as $f) {
                if ($f->store_id == $c) {
                    $p = $f->product;

                    if ($p) {
                        $product              = new ProductResource($p);
                        $product->favouriteId = (string) $f->id;
                        array_push($formattedStore['products'], $product);
                    }
                }
            }

            array_push($formattedFavourites, $formattedStore);
        }

        return response()->json($formattedFavourites);
    }
}
