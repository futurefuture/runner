<?php

namespace App\Http\Controllers\Runner\V4;

use App\Actions\GetProductsSearchedByQueryStringAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request, GetProductsSearchedByQueryStringAction $action)
    {
        $query          = $request->input('q');
        $fullPostalCode = $request->input('postalCode');
        $products       = $action->execute($query, $fullPostalCode);

        return response()->json([
            'data' => collect($products),
        ]);
    }
}
