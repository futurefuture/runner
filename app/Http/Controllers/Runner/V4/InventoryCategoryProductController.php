<?php

namespace App\Http\Controllers\Runner\V4;

use App\Product;
use App\Category;
use App\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use App\Utilities\Filters\FiltersTags;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Resources\Runner\ProductCollection;
use App\Http\Resources\Runner\Product as ProductResource;
use App\Actions\InventoryCategoryProduct\GetInventoryCategoryProducts;

class InventoryCategoryProductController extends Controller
{
    public function index(Request $request, Inventory $inventory, Category $category, GetInventoryCategoryProducts $action)
    {
        $products = $action->execute($request, $inventory, $category);

        return new ProductCollection($products);
    }

    public function show(Inventory $inventory, Category $category, Product $product)
    {
        $query = $inventory
            ->productsAll()
            ->select('products.*', 'quantity')
            ->where('products.id', $product->id)
            ->getQuery();

        $product = QueryBuilder::for($query)
            ->allowedIncludes(['reviews'])
            ->first();

        if ($product) {
            return new ProductResource($product);
        }
    }

    public function relatedProducts(Request $request, Inventory $inventory, Category $category, Product $product)
    {
        $relatedProductIds = $product->relatedProducts->unique('id')->pluck('id')->toArray();

        if (count($relatedProductIds) >= 3) {
            $allProducts = $inventory
                ->productsAll()
                ->join('related_products', 'products.id', '=', 'related_products.related_product_id')
                ->join('products as p', 'p.id', '=', 'related_products.product_id')
                ->where('p.id', $product->id)
                ->whereIn('related_products.related_product_id', $relatedProductIds)
                ->select('products.*', 'related_products.index', 'quantity')
                ->with([
                    'partners',
                    'incentives',
                    'productImages',
                    'ads'
                ])
                ->orderBy('related_products.index')
                ->get();
        } else {
            $limit = $request->input('limit') ?? 20;
            $query = $inventory
                ->productsAll()
                ->select('products.*', 'quantity')
                ->with([
                    'partners',
                    'incentives',
                    'productImages',
                    'ads'
                ])
                ->whereHas('categories', function ($q) use ($category) {
                    $q->where('categories.id', $category->id);
                })->getQuery();

            $allProducts = QueryBuilder::for($query)
                ->allowedFilters([
                    AllowedFilter::custom('tags', new FiltersTags),
                    AllowedFilter::scope('price'),
                    AllowedFilter::scope('averageRating'),
                ])
                ->allowedIncludes(['reviews'])
                ->defaultSort('-average_rating')
                ->allowedSorts([
                    AllowedSort::field('price', 'runner_price'),
                    AllowedSort::field('averageRating', 'average_rating'),
                ])
                ->where('products.id', '!=', $product->id)
                ->paginate($limit);
        }

        return new ProductCollection($allProducts);
    }
}
