<?php

namespace App\Http\Controllers\Runner\V4;

use App\Http\Controllers\Controller;
use App\Inventory;
use App\Store;
use Carbon\Carbon;

class StoreFrontController extends Controller
{
    public function show(Inventory $inventory)
    {
        $inventoryIsOpen = (bool) $inventory->is_active;
        $inventoryHours = $inventory->options['hours'];
        $now = Carbon::now();
        $dayOfWeek = $now->dayOfWeek;
        $inventoryOpen = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[0]);
        $inventoryClosed = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[1]);

        if (! $now->between($inventoryOpen, $inventoryClosed, false)) {
            $inventoryIsOpen = false;
        }

        $store = Store::find(8);

        $formattedInventory = (object) [
            'type'       => 'store fronts',
            'id'         => $store->id,
            'attributes' => [
                'title'     => $store->title,
                'inventory' => [
                    'id'           => $inventory->id,
                    'vendor'       => $inventory->vendor,
                    'address'      => $inventory->title,
                    'isOpen'       => $inventoryIsOpen,
                    'notification' => $inventory->options['notification'] ?? null,
                    'hours'        => $inventory->options['hours'],
                ],
                'storeLogo'   => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
                'locationId'  => $inventory->location_id,
                'subDomain'   => $store->sub_domain,
                'sortOrder'   => $store->options['sortOrder'] ?? null,
                'towerImage'  => $store->options['towerImage'] ?? null,
                'blockImage'  => $store->options['blockImage'] ?? null,
                'layouts'     => $store->layout ?? null,
            ],
        ];

        return response()->json([
            'data' => $formattedInventory,
        ]);
    }
}
