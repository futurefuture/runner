<?php

namespace App\Http\Controllers\Runner\V4;

use Auth;
use App\Cart;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use App\Services\CartService;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use App\Actions\CreateUserOrderAction;
use App\Actions\ReOrderUserOrderAction;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Runner\OrderCollection;
use App\Http\Resources\Runner\Order as OrderResource;

class UserOrderController extends Controller
{
    /**
     * function to get all users orders.
     *
     * @return array
     */
    public function index(User $user)
    {
        $orders = Order::where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->paginate(20);

        return new OrderCollection($orders);
    }

    /**
     * function to get order details.
     *
     * @param [type] $userId
     * @param [type] $orderId
     * @return array
     */
    public function show(User $user, Order $order)
    {
        $orders = Order::where('user_id', $user->id)->where('id', $order->id)->get();

        if ($orders) {
            return new OrderResource($order);
        } else {
            return response()->json([
                'error' => 'Order not found',
            ], 400);
        }
    }

    /**
     *  function to create order and initiate payment.
     *
     * @param Request $request
     * @return array
     */
    public function create(Request $request, User $user, CreateUserOrderAction $action)
    {
        $zipMessage = 'The Zip Code associated with your card is incorrect. If you aleady have a card added, please delete and re-enter your credit card information.';
        $data       = $request->input('data.attributes');

        if ($request->input('data.attributes.isGift') == true) {
            $giftData = $request->input('data.attributes.giftDetails');

            $validator = Validator::make($giftData, [
                'firstName'   => 'required',
                'lastName'    => 'required',
                'email'       => 'required|email',
                'phoneNumber' => 'required',
            ]);

            if ($validator->fails()) {
                $errors = json_decode($validator->errors()->toJson());

                foreach ($errors as $key => $value) {
                    $temp = $value[0];

                    break;
                }

                $errors->error = $temp;

                return response()->json([
                    'errors' => $errors,
                ], 401);
            }
        }

        try {
            $order = $action->execute($data);

            return new OrderResource($order);
        } catch (\Exception $e) {
            $error = $e->getMessage();

            if (strpos($error, 'zip') == true) {
                return response()->json([
                    'errors' => [
                        'error' => $zipMessage,
                    ],
                ], 400);
            } else {
                return response()->json([
                    'errors' => [
                        'error' => $error,
                    ],
                ], 400);
            }
        }
    }

    public function reOrder(Request $request, User $user, Order $order, ReOrderUserOrderAction $action)
    {
        $data               = $request->input('data.attributes');
        $inventoryId        = $data['inventoryId'];
        $cart               = $action->execute($user, $order, $inventoryId);

        return response()->json([
            'data' => [
                'type'       => 'carts',
                'id'         => $cart->id,
                'attributes' => [
                    'products'    => json_decode($cart->content)->products,
                    'coupon'      => isset(json_decode($cart->content)->coupon) ? json_decode($cart->content)->coupon : null,
                    'subTotal'    => json_decode($cart->content)->subTotal,
                    'deliveryFee' => json_decode($cart->content)->deliveryFee,
                    'tax'         => json_decode($cart->content)->tax,
                    'discount'    => json_decode($cart->content)->discount,
                    'total'       => json_decode($cart->content)->total,
                    'inventoryId' => $inventoryId,
                ],
            ],
        ]);
    }
}
