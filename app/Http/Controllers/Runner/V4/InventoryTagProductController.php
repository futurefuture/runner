<?php

namespace App\Http\Controllers\Runner\V4;

use App\Tag;
use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\ProductCollection;
use App\Inventory;
use App\Utilities\Filters\FiltersTags;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class InventoryTagProductController extends Controller
{
    public function index(Request $request, Inventory $inventory, Tag $tag)
    {
        $limit = $request->input('limit') ?? 20;

        $query = $inventory
            ->productsAll()
            ->join('product_tag', 'products.id', '=', 'product_tag.product_id')
            ->join('tags', 'tags.id', '=', 'product_tag.tag_id')
            ->where('product_tag.tag_id', '=', $tag->id)
            ->select('products.*', 'product_tag.index', 'quantity')
            ->getQuery();

        $allProducts = QueryBuilder::for($query)
            ->allowedFilters([
                AllowedFilter::custom('tags', new FiltersTags),
                AllowedFilter::scope('price'),
                AllowedFilter::scope('averageRating'),
            ])
            ->allowedIncludes([
                'reviews',
            ])
            ->allowedSorts([
                AllowedSort::field('price', 'runner_price'),
                AllowedSort::field('title'),
                AllowedSort::field('averageRating', 'average_rating'),
            ])
            ->orderBy('product_tag.index')
            ->orderBy('quantity', 'desc')
            ->defaultSort('-average_rating')
            ->paginate($limit)
            ->appends($request->except('page'));

        return new ProductCollection($allProducts);
    }
}
