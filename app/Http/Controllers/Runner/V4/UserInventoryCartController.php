<?php

namespace App\Http\Controllers\Runner\V4;

use App\Cart;
use App\User;
use App\Inventory;
use Illuminate\Http\Request;
use App\Services\CartService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Actions\CreateUserInventoryCartAction;
use App\Actions\UpdateUserInventoryCartAction;

class UserInventoryCartController extends Controller
{
    public function index(User $user, Inventory $inventory)
    {
        $cart = Cart::where('user_id', $user->id)
            ->where('inventory_id', $inventory->id)
            ->first();

        // hack to grab most recent LCBO cart if no cart
        if (! $cart && $inventory->store_id === 1) {
            $inventoryIds = Inventory::where('store_id', 1)->pluck('id');
            $cart         = Cart::where('user_id', $user->id)
                ->whereIn('inventory_id', $inventoryIds)
                ->latest('updated_at')
                ->first();
        }

        if (! $cart || ! $user->id) {
            return response()->json([
                'data' => [],
            ]);
        } else {
            $cartContent = json_decode($cart->content);

            foreach ($cartContent->products as $c) {
                $c->quantity = (int) $c->quantity;
                $c->id       = (string) $c->id;
                $c->allowSub = (bool) $c->allowSub;
            }

            return response()->json([
                'data' => [
                    [
                        'type'           => 'shopping carts',
                        'id'             => (string) $cart->id,
                        'attributes'     => [
                            'products'    => $cartContent->products,
                            'isGift'      => isset($cartContent->isGift) ? $cartContent->isGift : null,
                            'serviceFee'  => $cartContent->serviceFee ?? 0,
                            'coupon'      => isset($cartContent->coupon) ? $cartContent->coupon : null,
                            'incentive'   => isset($cartContent->incentive) ? $cartContent->incentive : null,
                            'subTotal'    => $cartContent->subTotal,
                            'deliveryFee' => $cartContent->deliveryFee,
                            'tax'         => $cartContent->tax,
                            'discount'    => $cartContent->discount,
                            'total'       => $cartContent->total,
                        ],
                    ],
                ],
            ]);
        }
    }

    /**
     * function returns cart infomation.
     *
     * @param [type] $userId
     * @param [type] $inventoryId
     * @param [type] $cartId
     * @return array
     */
    public function show(User $user, Inventory $inventory, Cart $cart)
    {
        if ($cart) {
            $cartContent = json_decode($cart->content);

            foreach ($cartContent->products as $c) {
                $c->quantity = (int) $c->quantity;
                $c->id       = (string) $c->id;
                $c->allowSub = (bool) $c->allowSub;
            }

            return response()->json([
                'data' => [
                    'type'           => 'shopping carts',
                    'id'             => (string) $cart->id,
                    'attributes'     => [
                        'products'    => $cartContent->products,
                        'coupon'      => $cartContent->coupon ?? null,
                        'incentive'   => $cartContent->incentive ?? null,
                        'subTotal'    => $cartContent->subTotal,
                        'deliveryFee' => $cartContent->deliveryFee,
                        'serviceFee'  => $cartContent->serviceFee,
                        'tax'         => $cartContent->tax,
                        'discount'    => $cartContent->discount,
                        'total'       => $cartContent->total,
                        'isGift'      => $cartContent->isGift,
                    ],
                ],
            ]);
        } else {
            return response(404);
        }
    }

    /**
     * function to create cart.
     */
    public function create(Request $request, User $user, Inventory $inventory, CreateUserInventoryCartAction $action)
    {
        $data = $request->input('data.attributes');

        try {
            $cart = $action->execute($user, $inventory, $data);

            return response()->json([
                'data' => [
                    'type'           => 'shopping carts',
                    'id'             => (string) $cart->id,
                    'attributes'     => [
                        'products'    => $cart->content->products,
                        'coupon'      => $cart->content->coupon ?? null,
                        'incentive'   => $cart->incentive ?? null,
                        'subTotal'    => $cart->content->subTotal,
                        'deliveryFee' => $cart->content->deliveryFee,
                        'serviceFee'  => $cart->content->serviceFee,
                        'tax'         => $cart->content->tax,
                        'discount'    => $cart->content->discount,
                        'total'       => $cart->content->total,
                        'isGift'      => $cart->content->isGift,
                    ],
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 422);
        }

        return $cart;
    }

    /**
     * function to update cart.
     */
    public function update(Request $request, User $user, Inventory $inventory, Cart $cart, UpdateUserInventoryCartAction $action)
    {
        $data = $request->input('data.attributes');

        try {
            $cart = $action->execute($user, $cart->id, $data);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 422);
        }

        return $cart;
    }
}
