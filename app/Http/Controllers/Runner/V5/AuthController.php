<?php

namespace App\Http\Controllers\Runner\V5;

use App\Actions\CreateUserAction;
use App\Actions\LoginUserAction;
use App\Actions\LogoutUserAction;
use App\Actions\MailgunValidateEmailAction;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Socialite;

class AuthController extends Controller
{
    public function register(Request $request, MailgunValidateEmailAction $mgAction, CreateUserAction $action)
    {
        $request->validate([
            'email'         => 'required|email|max:255|unique:users,email,NULL',
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'phone_number'  => 'required|unique:users,phone_number,NULL',
            'date_of_birth' => 'required|date|before:19 years ago',
            'is_legal_age'  => 'required',
            'address'       => 'required',
            'anonymous_id'  => 'required'
        ]);

        $data = $request->all();

        try {
            $mgAction->execute($data['email']);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 401);
        }

        try {
            $user = $action->execute($data);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ], 401);
        }

        // check if brand commerce registration
        if (isset($data['source'])) {
            return response()->json([
                'id'         => (string) $user->id,
                'first_name' => $user->first_name,
                'last_name'  => $user->last_name,
            ]);
        } else {
            $accessToken = $user
                ->createToken('Runner')
                ->accessToken;

            return response()->json([
                'access_token' => $accessToken,
                'user'         => [
                    'id'         => (string) $user->id,
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                ],
            ]);
        }
    }

    public function login(Request $request, LoginUserAction $action)
    {
        $socialLogin = [];
        $email       = $request->input('email');
        $password    = $request->input('password');
        $anonymousId = $request->input('anonymous_id');

        if ($request->input('google_id')) {
            $socialLogin = [
                'type' => 'googleId',
                'id'   => $request->input('google_id'),
            ];
        } elseif ($request->input('facebook_id')) {
            $socialLogin = [
                'type' => 'facebookId',
                'id'   => $request->input('facebook_id'),
            ];
        } elseif ($request->input('apple_id')) {
            $socialLogin = [
                'type' => 'appleId',
                'id'   => $request->input('apple_id'),
            ];
        }

        if (empty($socialLogin)) {
            $request->validate([
                'email'        => 'required|email',
                'password'     => 'required',
                'anonymous_id' => 'required'
            ]);
        } else {
            $request->validate([
                'anonymous_id' => 'required'
            ]);
        }

        try {
            $userInfo = $action->execute($email, $password, $socialLogin, $anonymousId);
        } catch (\Exception $e) {
            report($e);

            return $e->getMessage();
        }

        return response()->json([
            'access_token' => $userInfo['accessToken'],
            'user'         => [
                'id'          => (string) $userInfo['id'],
                'first_name'  => $userInfo['first_name'],
                'last_name'   => $userInfo['last_name'],
                'google_id'   => $userInfo['google_id'] ?? null,
                'facebook_id' => $userInfo['facebook_id'] ?? null,
                'apple_id'    => $userInfo['apple_id'] ?? null,
            ],
        ]);
    }

    public function logout(LogoutUserAction $action)
    {
        $user = Auth::user();

        try {
            $action->execute($user);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e,
                ],
            ], 401);
        }
    }

    public function redirectToProvider($socialPlatform)
    {
        return Socialite::driver($socialPlatform)
            ->scopes([
                'email',
            ])
            ->stateless()
            ->redirect();
    }

    public function handleProviderCallback($socialPlatform)
    {
        $socialPlatformUser = Socialite::driver($socialPlatform)
            ->stateless()
            ->user();

        if ($socialPlatform == 'facebook') {
            // check if user exists to login or register
            if (User::where('facebook_id', $socialPlatformUser->id)->first()) {
                $user = User::where('facebook_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', ['secret' => $secret]);
            } else {
                $firstName = explode(' ', $socialPlatformUser->name)[0];
                $lastName  = explode(' ', $socialPlatformUser->name)[1];

                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'facebook_id' => $socialPlatformUser->id,
                        'avatar'      => $socialPlatformUser->avatar,
                    ]);
                } else {
                    $user = (object) [
                        'facebookId' => $socialPlatformUser->id,
                        'firstName'  => $firstName,
                        'lastName'   => $lastName,
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@facebook.com',
                        'avatar'     => $socialPlatformUser->avatar,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@newRegister', [
                        'secret' => $secret,
                    ]);
                }
            }
        } elseif ($socialPlatform == 'google') {
            // check if user exists to login or register
            if (User::where('google_id', $socialPlatformUser->id)->first()) {
                $user = User::where('google_id', $socialPlatformUser->id)->first();

                // encrytpted user id
                $secret = Crypt::encryptString($user->id);

                return redirect()->action('Runner\V3\PageController@explore', [
                    'secret' => $secret,
                ]);
            } else {
                // update user if facebook email exists
                if (User::where('email', $socialPlatformUser->email)->first()) {
                    $user = User::where('email', $socialPlatformUser->email)->first();

                    $user->update([
                        'google_id' => $socialPlatformUser->id,
                        'avatar'    => $socialPlatformUser->avatar_original,
                    ]);
                } else {
                    $user = (object) [
                        'googleId'   => $socialPlatformUser->id,
                        'firstName'  => $socialPlatformUser->user['name']['givenName'] ?? 'firstName',
                        'lastName'   => $socialPlatformUser->user['name']['familyName'] ?? $socialPlatformUser->id,
                        'email'      => $socialPlatformUser->email ?? $socialPlatformUser->name . '@googleplus.com',
                        'avatar'     => $socialPlatformUser->avatar_original,
                    ];

                    // encrytpted user info
                    $secret = encrypt($user);

                    return redirect()->action('Runner\V3\PageController@newRegister', [
                        'secret' => $secret,
                    ]);
                }
            }
        }
    }

    public function tokenGrant(Request $request)
    {
        $baseUrl = env('APP_URL');
        $data    = $request->input('data.attributes');
        $http    = new Client([
            'verify' => false
        ]);
        try {
            $response = $http->post($baseUrl . '/oauth/token', [
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => $data['clientId'],
                    'client_secret' => $data['clientSecret'],
                    'username'      => $data['email'],
                    'password'      => $data['password'],
                    'scope'         => ''
                ],
            ]);

            $tempRes            = json_decode((string) $response->getBody(), true);
            $userId             = User::where('email', $data['email'])->first()->id;
            $tempRes['user_id'] = $userId;

            return response()->json($tempRes);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return $e;
        }
    }
}
