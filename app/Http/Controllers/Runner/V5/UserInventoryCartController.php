<?php

namespace App\Http\Controllers\Runner\V5;

use App\Classes\Utilities;
use App\Cart;
use App\User;
use App\Inventory;
use Illuminate\Http\Request;
use App\Services\CartService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Actions\V5\CreateUserInventoryCartAction;
use App\Actions\V5\UpdateUserInventoryCartAction;

class UserInventoryCartController extends Controller
{
    public function index(User $user, Inventory $inventory)
    {
        $utilities     = new Utilities();

        $cart = Cart::where('user_id', $user->id)
            ->where('inventory_id', $inventory->id)
            ->first();

        // hack to grab most recent LCBO cart if no cart
        if (!$cart && $inventory->store_id === 1) {
            $inventoryIds = Inventory::where('store_id', 1)->pluck('id');
            $cart         = Cart::where('user_id', $user->id)
                ->whereIn('inventory_id', $inventoryIds)
                ->latest('updated_at')
                ->first();
        }

        if (!$cart || !$user->id) {
            return response()->json([]);
        } else {
            $cartContent = json_decode($cart->content);
            foreach ($cartContent->products as $c) {
                $c->quantity = (int) $c->quantity;
                $c->id       = (string) $c->id;
                $c->allowSub = (bool) $c->allowSub;
            }

            $cartProds = $utilities->changeCamelCaseToSnakeCase($cartContent->products);
            return response()->json([
                'type'           => 'shopping carts',
                'id'             => (string) $cart->id,
                'products'    => $cartProds,
                'is_gift'      => isset($cartContent->isGift) ? $cartContent->isGift : null,
                'service_fee'  => $cartContent->serviceFee ?? 0,
                'coupon'      => isset($cartContent->coupon) ? $cartContent->coupon : null,
                'incentive'   => isset($cartContent->incentive) ? $cartContent->incentive : null,
                'sub_total'    => $cartContent->subTotal,
                'delivery_fee' => $cartContent->deliveryFee,
                'tax'         => $cartContent->tax,
                'discount'    => $cartContent->discount,
                'total'       => $cartContent->total,
            ]);
        }
    }

    /**
     * function returns cart infomation.
     *
     * @param [type] $userId
     * @param [type] $inventoryId
     * @param [type] $cartId
     * @return array
     */
    public function show(User $user, Inventory $inventory, Cart $cart)
    {
        $utilities     = new Utilities();

        if ($cart) {
            $cartContent = json_decode($cart->content);

            foreach ($cartContent->products as $c) {
                $c->quantity = (int) $c->quantity;
                $c->id       = (string) $c->id;
                $c->allowSub = (bool) $c->allowSub;
            }

            $cartProds = $utilities->changeCamelCaseToSnakeCase($cartContent->products);
            return response()->json([
                'type'           => 'shopping carts',
                'id'             => (string) $cart->id,
                'products'    => $cartProds,
                'coupon'      => $cartContent->coupon ?? null,
                'incentive'   => $cartContent->incentive ?? null,
                'sub_total'    => $cartContent->subTotal,
                'delivery_fee' => $cartContent->deliveryFee,
                'service_fee'  => $cartContent->serviceFee,
                'tax'         => $cartContent->tax,
                'discount'    => $cartContent->discount,
                'total'       => $cartContent->total,
                'is_gift'      => $cartContent->isGift,
            ]);
        } else {
            return response(404);
        }
    }

    /**
     * function to create cart.
     *
     * @param Request $request
     * @param [type] $userId
     * @param [type] $inventoryId
     * @return array
     */
    public function create(Request $request, User $user, Inventory $inventory, CreateUserInventoryCartAction $action)
    {
        $data = $request->input();
        $utilities     = new Utilities();

        try {
            $cart = $action->execute($user, $inventory, $data);


            $cartProds = $utilities->changeCamelCaseToSnakeCase($cart->content->products);
            return response()->json([
                'type'           => 'shopping carts',
                'id'             => (string) $cart->id,
                'products'       => $cartProds,
                'coupon'         => $cart->content->coupon ?? null,
                'incentive'      => $cart->incentive ?? null,
                'sub_total'      => $cart->content->subTotal,
                'delivery_fee'   => $cart->content->deliveryFee,
                'service_fee'    => $cart->content->serviceFee,
                'tax'            => $cart->content->tax,
                'discount'       => $cart->content->discount,
                'total'          => $cart->content->total,
                'is_gift'        => $cart->content->isGift,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 422);
        }

        return $cart;
    }

    /**
     * function to update cart.
     *
     * @param Request $request
     * @param [type] $userId
     * @param [type] $inventoryId
     * @return array
     */
    public function update(Request $request, User $user, Inventory $inventory, Cart $cart, UpdateUserInventoryCartAction $action)
    {
        $data = $request->input();

        try {
            $cart = $action->execute($user, $cart->id, $data);
            // $cart = (new CartService)->update($request, $user, $cart->id);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 422);
        }

        return $cart;
    }
}
