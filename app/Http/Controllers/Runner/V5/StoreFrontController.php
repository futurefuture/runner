<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use App\Inventory;
use App\Store;
use Carbon\Carbon;

class StoreFrontController extends Controller
{
    public function show(Inventory $inventory)
    {
        $inventoryIsOpen = (bool) $inventory->is_active;
        $inventoryHours = $inventory->options['hours'];
        $now = Carbon::now();
        $dayOfWeek = $now->dayOfWeek;
        $inventoryOpen = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[0]);
        $inventoryClosed = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[1]);

        if (!$now->between($inventoryOpen, $inventoryClosed, false)) {
            $inventoryIsOpen = false;
        }

        $store = Store::find(8);

        $formattedInventory = (object) [
            'type'       => 'store fronts',
            'id'         => $store->id,
            'title'     => $store->title,
            'inventory' => [
                'id'           => $inventory->id,
                'vendor'       => $inventory->vendor,
                'address'      => $inventory->title,
                'is_open'       => $inventoryIsOpen,
                'notification' => $inventory->options['notification'] ?? null,
                'hours'        => $inventory->options['hours'],
            ],
            'store_logo'   => Store::find($inventory->store_id)->options['storeLogo'] ?? null,
            'location_id'  => $inventory->location_id,
            'sub_domain'   => $store->sub_domain,
            'sort_order'   => $store->options['sortOrder'] ?? null,
            'tower_image'  => $store->options['towerImage'] ?? null,
            'block_image'  => $store->options['blockImage'] ?? null,
            'layouts'     => $store->layout ?? null,
        ];

        return response()->json($formattedInventory);
    }
}
