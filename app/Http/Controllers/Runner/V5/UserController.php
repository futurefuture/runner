<?php

namespace App\Http\Controllers\Runner\V5;

use App\User;
use Illuminate\Http\Request;
use App\Actions\UpdateUserAction;
use App\Actions\ReferAFriend\ReferAFriendAction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;
use App\Http\Resources\Runner\V5\User as UserResource;

class UserController extends Controller
{
    public function show(User $user)
    {
        if (Auth::user()->id === $user->id) {
            return response()->json(new UserResource($user));
        }
    }

    public function update(Request $request, User $user, UpdateUserAction $action)
    {
        $data = $request->input();

        if (Auth::user()->id === $user->id) {
            try {
                $user = $action->execute($user, $data);
            } catch (\Exception $e) {
                if ($e->errorInfo[1] === 1062) {
                    if (strpos($e->errorInfo[2], 'phone_number')) {
                        $detail = 'Whoops, looks like that phone number is already in use.';
                    }
                    if (strpos($e->errorInfo[2], 'email')) {
                        $detail = 'Whoops, looks like that email address is already in use.';
                    }
                } else {
                    $detail = $e->getMessage();
                }

                return response()->json([
                    'errors' => [
                        'error' => $detail,
                    ],
                ], 401);
            }

            return response()->json(new UserResource($user));
        }
    }

    public function updatePassword(Request $request, User $user)
    {
        $data = $request->input();
        if (Auth::user()->id === $user->id) {
            if (isset($data['old_password'])) {
                if (Hash::check($data['old_password'], $user->password)) {
                    $user->update([
                        'password' => Hash::make($data['new_password']),
                    ]);

                    return response()->json(new UserResource($user));
                } else {
                    return response()->json([
                        'errors' => [
                            [
                                'status' => 403,
                                'title'  => 'Invalid Password',
                                'detail' => 'The password you gave does not match your old password.',
                            ],
                        ],
                    ], 403);
                }
            }
        }
    }

    public function referAFriend(User $user, Request $request, ReferAFriendAction $action)
    {
        $data = $request->input();

        try {
            $action->execute($user, $data);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e->getMessage(),
                ],
            ]);
        }
    }
}
