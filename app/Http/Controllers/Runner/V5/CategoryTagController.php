<?php

namespace App\Http\Controllers\Runner\V5;

use App\Category;
use App\Classes\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\V5\CategoryTag as CategoryTagResource;
use App\Http\Resources\Runner\V5\CategoryTagCollection;
use App\Tag;
use Illuminate\Http\Request;

class CategoryTagController extends Controller
{
    public function index($categoryId)
    {
        $user = Category::find($categoryId)->tags()->isActive()->isVisible()->paginate(10);

        return new CategoryTagCollection($user);
    }

    public function show($categoryId, $tagId)
    {
        $tag = Category::find($categoryId)->tags()->where('tag_id', $tagId)->isActive()->isVisible()->first();

        return response()->json(new CategoryTagResource($tag));
    }
}
