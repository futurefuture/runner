<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use App\Http\Resources\Runner\V5\Product as ProductResource;
use App\Product;

class ProductController extends Controller
{
    /**
     * Gets product.
     *
     * @param string $query
     * @return array
     */
    public function show(Product $product)
    {
        return response()->json(new ProductResource($product));
    }
}
