<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use App\Survey;
use App\Http\Resources\Runner\V5\Survey as SurveyResource;
use App\Http\Resources\Runner\V5\SurveyCollection;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Get all surveys.
     *
     *
     * @return Collection
     */
    public function index()
    {
        $surveys = Survey::get();

        return new SurveyCollection($surveys);
    }

    /**
     * Get one survey.
     *
     * @param Survey $survey
     *
     * @return Resource
     */
    public function show(Survey $survey)
    {
        return response()->json(new SurveyResource($survey));
    }
}
