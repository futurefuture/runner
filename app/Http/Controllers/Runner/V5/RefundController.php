<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use App\Refund;
use App\Http\Resources\Runner\V5\Refund as RefundResource;
use App\Http\Resources\Runner\V5\RefundCollection;
use Illuminate\Http\Request;

class RefundController extends Controller
{
    /**
     * Get all refunds.
     *
     * @param string $query
     *
     * @return array
     */
    public function index()
    {
        $refunds = Refund::get();

        return new RefundCollection($refunds);
    }

    /**
     * Get one refund.
     *
     * @param string $query
     *
     * @return array
     */
    public function show(Refund $refund)
    {
        return response()->json(new RefundResource($refund));
    }
}
