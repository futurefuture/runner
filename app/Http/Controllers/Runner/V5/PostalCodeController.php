<?php

namespace App\Http\Controllers\Runner\V5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\V5\CreateOutOfBoundsPostalCodeAction;

class PostalCodeController extends Controller
{
    public function storeOutOfBoundsPostalCode(Request $request, CreateOutOfBoundsPostalCodeAction $action)
    {
        $data = $request->input();

        $outOfBoundsPostalCode = $action->execute($data);

        return response()->json([
            'id'         => $outOfBoundsPostalCode->id,
            'type'       => 'out of bounds postal codes',
            'postal_code' => $outOfBoundsPostalCode->postal_code,
        ]);
    }
}
