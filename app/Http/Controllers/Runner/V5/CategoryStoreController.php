<?php

namespace App\Http\Controllers\Runner\V5;

use App\Category;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\Store;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class CategoryStoreController extends Controller
{
    public function index(Request $request, Category $category)
    {
        $postalCode           = substr($request->input('postalCode'), 0, 3);
        $inventoryPostalCodes = DB::table('inventory_postal_code')
            ->where('postal_code', $postalCode)
            ->get();
        $inventories          = [];
        $formattedInventories = [];

        foreach ($inventoryPostalCodes as $i) {
            $inventory = Inventory::where('is_active', 1)
                ->where('id', $i->inventory_id)
                ->first();

            if ($inventory && $inventory->productsAll()->where('category_id', $category->id)->count() > 0) {
                array_push($inventories, $inventory);
            }
        }

        foreach ($inventories as $inventory) {
            $inventoryIsOpen = (bool) $inventory->is_active;
            $inventoryHours  = $inventory->options['hours'];
            $now             = Carbon::now();
            $dayOfWeek       = $now->dayOfWeek;
            $inventoryOpen   = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[0]);
            $inventoryClosed = Carbon::parse(explode('-', $inventoryHours[$dayOfWeek])[1]);

            if (!$now->between($inventoryOpen, $inventoryClosed, false)) {
                $inventoryIsOpen = false;
            }

            $store = Store::where('is_active', 1)
                ->where('id', $inventory->store_id)
                ->first();

            $formattedInventory = (object) [
                'type'       => 'stores',
                'id'         => (string) $store->id,
                'title'            => $store->title,
                'store_logo'        => Store::find($inventory->store_id)->options['store_logo'] ?? null,
                'inverted_logo'     => Store::find($inventory->store_id)->options['inverted_logo'] ?? false,
                'navigation_logo'   => Store::find($inventory->store_id)->options['navigation_logo'] ?? null,
                'hours'            => Store::find($inventory->store_id)->options['hours'] ?? null,
                'block_image'       => $store->options['block_image'] ?? null,
                'next_delivery_time' => $store->options['next_delivery_time'] ?? null,
                'about_text'        => $store->options['about_text'] ?? null,
                'sub_domain'        => $store->sub_domain,
                'inventory_id'      => (string) $inventory->id,
                'tower_image'       => $store->options['tower_image'] ?? null,
                'categories'       => $store->categories ?? null,
            ];

            array_push($formattedInventories, $formattedInventory);
        }

        return response()->json([
            'data' => $formattedInventories,
        ]);
    }
}
