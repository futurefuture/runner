<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use Auth;
use DB;

class ConfigController extends Controller
{
    // t
    public function getGlobalNotification()
    {
        $user = Auth::user();

        $notification = DB::table('config')
                            ->where('key', 'globalNotification')
                            ->first();

        return response()->json($notification);
    }

    public function index()
    {
        $notification    = DB::table('config')->where('key', 'globalNotification')->first();
        $storeCategories = DB::table('config')->where('key', 'storeCategories')->first();

        return response()->json([
            'type'              => 'configs',
            'id'                => 1,
            'ags'               => '18570a327b9fce163e19ce027f79a3bb',
            'ai'           => [
                'gifts'    => 'gifts',
                'runner'   => 'new_products',
            ],
            'aid'               => '1250283544',
            'gmk'               => 'AIzaSyBGp2tDORwlAzcrrdA_eKBffxsZLf_kNdQ',
            'aai'               => 'EWG2ZDNJEQ',
            'zac'               => '4eWpbBn0mSlawTtakFCDwFilkuwfGaFH',
            'ami'               => 'merchant.com.futurefuture.runner',
            'adk'               => '6LnJkXQVVY2tme76JZJV3b',
            'notification'      => json_decode($notification->value),
            'app_version'       => 'Version 5.0.0',
            'store_categories'  => json_decode($storeCategories->value),
        ]);
    }
}
