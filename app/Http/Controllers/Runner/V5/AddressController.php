<?php

namespace App\Http\Controllers\Runner\V5;

use App\Actions\GetStoresValidatedByPostalCodeAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function validatePostalCode(Request $request, GetStoresValidatedByPostalCodeAction $action)
    {
        $isWeb = false;

        if ($request->input('is_web')) {
            $isWeb = true;
        }

        $stores          = $action->execute($request->input('postalCode'), $isWeb);
        $formattedStores = [];

        // create custom resource
        foreach ($stores as $s) {
            $formattedStore = [
                'type'       => 'validated stores by postal code',
                'id'         => (string) $s['id'],
                'title'            => $s['title'],
                'index'            => $s['index'],
                'store_logo'        => $s['options']['storeLogo'] ?? null,
                'inverted_logo'     => $s['options']['invertedLogo'] ?? false,
                'navigation_logo'   => $s['options']['navigationLogo'] ?? null,
                'hours'            => $s['options']['hours'] ?? null,
                'block_image'       => $s['options']['blockImage'] ?? null,
                'next_delivery_time' => $s['options']['nextDeliveryTime'] ?? null,
                'about_text'        => $s['options']['aboutText'] ?? null,
                'sub_domain'        => $s['sub_domain'],
                'inventory_id'      => (string) $s['inventory_id'],
                'categories'       => isset($s['options']['categories']) ? $s['options']['categories'] : null,
            ];

            array_push($formattedStores, $formattedStore);
        }

        return response()->json([
            'data' => $formattedStores,
        ]);
    }
}
