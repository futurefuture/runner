<?php

namespace App\Http\Controllers\Runner\V5;

use App\Category;
use App\Http\Controllers\Controller;
use App\Services\V5\CategoryService;

class CategoryController extends Controller
{
    public function index()
    {
        return (new CategoryService())->index();
    }

    public function show(Category $category)
    {
        return response()->json((new CategoryService())->show($category));
    }
}
