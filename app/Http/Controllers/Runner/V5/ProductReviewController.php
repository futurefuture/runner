<?php

namespace App\Http\Controllers\Runner\V5;

use App\Review;
use App\Product;
use App\Classes\Utilities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use App\Actions\V5\CreateProductReviewAction;
use App\Http\Resources\Runner\V5\ReviewCollection;
use App\Http\Resources\Runner\V5\Review as ReviewResource;

class ProductReviewController extends Controller
{
    /**
     * Gets reviews according to product id.
     *
     * @param Request $request
     * @return ReviewCollection
     */
    public function index(Product $product)
    {
        $query = $product->reviews()
            ->where('is_approved', true)
            ->whereHas('user')
            ->getQuery();

        $reviews = QueryBuilder::for($query)
            ->allowedSorts([
                AllowedSort::field('value'),
                AllowedSort::field('helpful'),
                AllowedSort::field('createdAt', 'created_at'),
            ])
            ->get();

        return new ReviewCollection($reviews);
    }

    /**
     * Creates review according to product id for user.
     *
     * @param Request $request
     * @return ReviewCollection
     */
    public function create(Request $request, Product $product, CreateProductReviewAction $action)
    {
        $data   = $request->input();
        $review = $action->execute($product, $data);

        return response()->json(new ReviewResource($review));
    }

    public function update(Request $request, $productId, $reviewId)
    {
        $data      = $request->input();
        $review    = Review::find($reviewId);

        if (isset($data['helpful'])) {
            $review->helpful += 1;
            $review->save();
        } elseif (isset($data['unhelpful'])) {
            $review->unhelpful += 1;
            $review->save();
        }

        return response()->json(new ReviewResource($review));
    }
}
