<?php

namespace App\Http\Controllers\Runner\V5;

use App\Http\Controllers\Controller;
use App\SurveyOption;
use App\Http\Resources\Runner\V5\SurveyOption as SurveyOptionResource;
use App\Http\Resources\Runner\V5\SurveyOptionCollection;
use Illuminate\Http\Request;

class SurveyOptionController extends Controller
{
    /**
     * Get all surveys.
     *
     *
     * @return Collection
     */
    public function index()
    {
        $surveyOptions = SurveyOption::get();
        return new SurveyOptionCollection($surveyOptions);
    }

    /**
     * Get one survey.
     *
     * @param SurveyOption $surveyOption
     *
     * @return Resource
     */
    public function show(SurveyOption $surveyOption)
    {
        return response()->json(new SurveyOptionResource($surveyOption));
    }
}
