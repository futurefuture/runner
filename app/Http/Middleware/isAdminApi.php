<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class isAdminApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->role == 1) {
            return $next($request);
        }

        dd("Sorry, You don't have access to this! If you need more information please contact service@getrunner.io");
    }
}
