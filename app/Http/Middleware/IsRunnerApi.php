<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsRunnerApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && (Auth::user()->role == 2 || Auth::user()->role == 1 || Auth::user()->role == 5)) {
            return $next($request);
        }

        dd("Sorry, You don't have access to this! If you need more information please contact service@getrunner.io");
    }
}
