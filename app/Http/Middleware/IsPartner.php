<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsPartner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && (Auth::user()->role === 1 || Auth::user()->role === 7)) {
            return $next($request);
        }

        return redirect('/login');
    }
}
