<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class isRunner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && (Auth::user()->role == 2 || Auth::user()->role == 1 || Auth::user()->role == 5)) {
            return $next($request);
        }

        return redirect('/runner');
    }
}
