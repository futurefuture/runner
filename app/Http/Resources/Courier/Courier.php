<?php

namespace App\Http\Resources\Courier;

use Illuminate\Http\Resources\Json\JsonResource;

class Courier extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'couriers',
            'id'         => (string) $this->id,
            'attributes' => [
                'firstName'   => $this->first_name,
                'lastName'    => $this->last_name,
                'dateOfBirth' => $this->date_of_birth,
                'phoneNumber' => $this->phone_number,
                'email'       => $this->email,
            ],
        ];
    }
}
