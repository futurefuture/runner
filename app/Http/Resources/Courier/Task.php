<?php

namespace App\Http\Resources\Courier;

use App\User;
use App\Order;
use App\Store;
use App\Address;
use App\Product;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Courier\Order as OrderResource;
use App\Http\Resources\Courier\Courier as CourierResource;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $address = Address::withTrashed()
            ->find($this->address_id);
        $order = Order::find($this->order_id);

        if ($order) {
            $orderContent = json_decode($order->content);

            foreach ($orderContent as $o) {
                $product = Product::find($o->id);
                $o->sku  = $product->sku;
            }

            $store   = Store::find($order->store_id)->options;
        }

        $logo    = $store['storeLogo'] ?? null;
        $courier = User::find($this->courier_id);

        return [
            'type'       => 'tasks',
            'id'         => (string) $this->id,
            'storeLogo'  => $logo,
            'attributes' => [
                'state'        => $this->state,
                'index'        => $this->index,
                'notes'        => $this->notes,
                'phoneNumber'  => $this->phone_number,
                'name'         => $this->name,
                'organization' => $this->organization ?? 'runner',
                'address'      => $address,
                'courier'      => new CourierResource($courier),
                'order'        => new OrderResource($order),
                'container'    => $this->container_id ?? null,
                'color'        => Schedule::where('user_id', $courier->id)->latest('created_at')->first()->color ?? null,
                'type'         => $this->type,
                'createdAt'    => Carbon::parse($this->created_at)->toDateTimeString(),
                'signature'    => $this->signature,
                'imageUrl'     => $this->image_url,
            ],
        ];
    }
}
