<?php

namespace App\Http\Resources\Courier;

use App\Http\Resources\Courier\Task as TaskResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => TaskResource::collection($this->collection),
        ];
    }
}
