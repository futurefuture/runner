<?php

namespace App\Http\Resources\Courier;

use App\Gift;
use App\User;
use App\Store;
use Exception;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (strpos($this->charge, '[') !== false) {
            $arrCharge = json_decode($this->charge);
            $count     = count($arrCharge);
            $index     = count($arrCharge) - $count;
            $chargeId  = $arrCharge[$index];
        } else {
            $chargeId = $this->charge;
        }

        try {
            if (env('APP_ENV') === 'production') {
                $charge     = \Stripe\Charge::retrieve($chargeId);
                $isApplePay = $charge->source->tokenization_method != null ? true : false;
            }
        } catch (Exception $e) {
            Log::info('No charge Id found');
        }

        $orderContent = json_decode($this->content);
        $store        = Store::find($this->store_id);
        $storeOptions = $store->options;

        if (isset($this->runner_1)) {
            $courier = User::find($this->runner_1) ?? null;
        } else {
            $courier = null;
        }

        // check if gift and add gift details if so
        $gift = Gift::where('order_id', $this->id)->first();

        if ($gift) {
            $formattedGift                = [];
            $formattedGift['id']          = $gift->id;
            $formattedGift['orderId']     = $gift->order_id;
            $formattedGift['firstName']   = $gift->first_name;
            $formattedGift['lastName']    = $gift->last_name;
            $formattedGift['isNineteen']  = $gift->is_19;
            $formattedGift['email']       = $gift->email;
            $formattedGift['phoneNumber'] = $gift->phone_number;
            $formattedGift['message']     = $gift->message;
            $formattedGift['giftOptions'] = $gift->gift_options;
            $data['attributes']['gift']   = $formattedGift;
        }

        $customer       = json_decode($this->customer);
        $user           = User::where('id', $this->user_id)->first();
        $tags           = $user->tags;
        $customer->tags = $tags;

        foreach ($orderContent as $o) {
            $isRush      = false;
            $product     = Product::where('id', $o->id)->first();
            $inventories = $product->inventories->pluck('id')->toArray();

            if (count($inventories) === 1 && in_array(18, $inventories)) {
                $isRush = true;
            }

            if (! $product) {
                throw new Exception('no product found');
            }

            $o->producing_country = $product->producing_country;
            $o->sku               = $product->sku;
            $o->isRush            = $isRush;
        }

        $data = [
            'type'       => 'orders',
            'id'         => (string) $this->id,
            'attributes' => [
                'orderId'           => $this->id,
                'store'             => [
                    'id'    => $store->id ?? null,
                    'logo'  => $storeOptions['storeLogo'] ?? null,
                ],
                'inventoryId'       => $this->inventory_id,
                'userId'            => $this->user_id,
                'status'            => $this->status,
                'orderContent'      => $orderContent,
                'courier'           => [
                    'id'        => $courier->id ?? null,
                    'firstName' => $courier->first_name ?? null,
                    'lastName'  => $courier->last_name ?? null,
                ],
                'admin_notes'     => $this->admin_notes,
                'device'          => $this->device,
                'customer'        => $customer,
                'address'         => json_decode($this->address),
                'unitNumber'      => $this->unit_number,
                'coupon'          => $this->coupon_id,
                'instructions'    => $this->instructions,
                'subtotal'        => $this->subtotal / 100,
                'serviceFee'      => $this->service_fee / 100,
                'discount'        => $this->discount / 100,
                'delivery'        => $this->delivery / 100,
                'tip'             => $this->tip / 100,
                'displayTaxTotal' => $this->display_tax_total / 100,
                'total'           => $this->total / 100,
                'signature'       => $this->signature,
                'scheduleTime'    => $this->schedule_time,
                'deliveredAt'     => $this->delivered_at,
                'createdAt'       => Carbon::parse($this->created_at)->toDateTimeString(),
                'gift'            => $formattedGift ?? null,
                'inventoryId'     => $this->inventory_id ?? null,
                'firstTime'       => $this->first_time,
                'paymentMethod'   => $charge->source->brand ?? 'VISA',
                'riskScore'       => $charge->outcome->risk_score ?? '2',
                'isApplePay'      => $isApplePay ?? null,
            ],
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/orders/' . $this->id),
            ],
        ];

        return $data;
    }
}
