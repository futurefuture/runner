<?php

namespace App\Http\Resources\Courier;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderSimplified extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $customer       = json_decode($this->customer);
        $user           = User::where('id', $this->user_id)->first();
        $tags           = $user->tags;
        $customer->tags = $tags;

        $data = [
            'type'       => 'orders',
            'id'         => (string) $this->id,
            'attributes' => [
                'orderId'      => $this->id,
                'userId'       => $this->user_id,
                'status'       => $this->status,
                'customer'     => $customer,
                'scheduleTime' => $this->schedule_time,
                'deliveredAt'  => $this->delivered_at,
                'createdAt'    => Carbon::parse($this->created_at)->toDateTimeString(),
            ],
        ];

        return $data;
    }
}
