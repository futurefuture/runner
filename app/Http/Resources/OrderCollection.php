<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($o) {
                $o->customer = json_decode($o->customer);
                $o->address = json_decode($o->address);
                $items = gettype($o->content) == 'string' ? json_decode($o->content) : $o->content;

                return [
                    'id'    =>  $o->id,
                    'status'    =>  $o->status,
                    'customer'  =>  $o->customer,
                    'items'   =>  $items,
                    'address'   =>  $o->address,
                    'unit_number'      =>  $o->unit_number,
                    'created_at'    =>  $o->created_at->format('Y-m-d H:i:s'),
                    'delivered_at'  =>  $o->delivered_at,
                ];
            }),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
