<?php

namespace App\Http\Resources;

use App\Http\Resources\PartnerProduct as PartnerProductResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PartnerProductsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PartnerProductResource::collection($this->collection),
        ];
    }
}
