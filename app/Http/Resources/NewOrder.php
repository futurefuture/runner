<?php

namespace App\Http\Resources;

use App\Category;
use App\Gift;
use App\Invitation;
use App\Product;
use App\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NewOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $content = json_decode($this->content);

        if ($this->store_id === null) {
            foreach ($content as $c) {
                $c->title       = $c->name ?? 'test';
                $c->quantity    = $c->qty ?? 1;
                $c->runnerPrice = $c->runner_price ?? 100;
            }
        }

        // check if gift and add gift details if so
        $gift = Gift::where('order_id', $this->id)->first();

        if (isset($this->runner_1)) {
            $courier = User::find($this->runner_1) ?? null;
        } else {
            $courier = null;
        }

        if ($gift) {
            $formattedGift = [];

            $formattedGift['id']          = $gift->id;
            $formattedGift['orderId']     = $gift->order_id;
            $formattedGift['firstName']   = $gift->first_name;
            $formattedGift['lastName']    = $gift->last_name;
            $formattedGift['lastName']    = $gift->last_name;
            $formattedGift['address']     = json_decode($gift->address);
            $formattedGift['isNineteen']  = json_decode($gift->is_19);
            $formattedGift['email']       = json_decode($gift->email);
            $formattedGift['phoneNumber'] = json_decode($gift->phone_number);
            $formattedGift['message']     = json_decode($gift->message);
            $formattedGift['giftOptions'] = json_decode($gift->gift_options);
            $data['attributes']['gift']   = $formattedGift;
        }

        $invite = Invitation::where('invited_user_id', $this->user_id)->first();

        // change from timestamp object to string
        $created_at = Carbon::parse($this->created_at)->toDateTimeString();

        if ($this->coupon_id) {
            $discountType = 'Coupon';
        } elseif ($this->coupon_id === null && $this->first_time === 1 && $this->discount && $invite) {
            $discountType = 'Referred By ' . $invite->email;
        } elseif ($this->coupon_id === null && $this->first_time === 0 && $this->discount) {
            $discountType = 'Reward Points';
        } else {
            $discountType = null;
        }

        foreach ($content as $c) {
            $product = Product::find($c->id);

            if ($product) {
                $category       = Category::find($product->category_id) ?? Category::find(1);
                $c->retailPrice = $product->retail_price;
                $c->brand       = $product->producer;
                $c->category    = $category->slug;
            }
        }

        $store = Store::find($this->store_id);

        $data = [
            'type'       => 'orders',
            'id'         => (string) $this->id,
            'attributes' => [
                'orderId'           => $this->id,
                'store'             => $store,
                'inventoryId'       => $this->inventory_id,
                'user_id'           => $this->user_id,
                'status'            => $this->status,
                'items'             => $content,
                'runner_1'          => $this->runner_1,
                'runner_2'          => $this->runner_2,
                'courier'           => [
                    'id'        => $courier->id ?? null,
                    'firstName' => $courier->first_name ?? null,
                    'lastName'  => $courier->last_name ?? null,
                ],
                'device'            => $this->device,
                'customer'          => json_decode($this->customer),
                'address'           => json_decode($this->address),
                'unit_number'       => $this->unit_number,
                'coupon'            => $this->coupon_id,
                'instructions'      => $this->instructions,
                'subtotal'          => $this->subtotal / 100,
                'service_fee'       => $this->service_fee / 100,
                'discount'          => $this->discount / 100,
                'discountType'      => $discountType,
                'delivery'          => $this->delivery / 100,
                'tip'               => $this->tip / 100,
                'display_tax_total' => $this->display_tax_total / 100,
                'total'             => $this->total / 100,
                'signature'         => $this->signature,
                'schedule_time'     => $this->schedule_time,
                'delivered_at'      => $this->delivered_at,
                'created_at'        => $created_at,
                'gift'              => $formattedGift ?? null,
                'inventoryId'       => $this->inventory_id ?? null,
            ],
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/orders/' . $this->id),
            ],
        ];

        return $data;
    }
}
