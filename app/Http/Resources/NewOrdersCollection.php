<?php

namespace App\Http\Resources;

use App\Http\Resources\NewOrder as NewOrderResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class NewOrdersCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => NewOrderResource::collection($this->collection),
        ];
    }
}
