<?php

namespace App\Http\Resources;

use App\Http\Resources\Partner as PartnerResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PartnersCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PartnerResource::collection($this->collection),
        ];
    }
}
