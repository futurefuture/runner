<?php

namespace App\Http\Resources;

use App\Http\Resources\Address as AddressResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AddressesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => AddressResource::collection($this->collection),
        ];
    }
}
