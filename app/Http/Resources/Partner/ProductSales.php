<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductSales extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'day'          => $this->day,
            'month'        => $this->month,
            'year'         => $this->year,
            'totalRevenue' => $this->totalRevenue,
        ];
    }
}
