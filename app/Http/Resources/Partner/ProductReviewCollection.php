<?php

namespace App\Http\Resources\Partner;

use App\Http\Resources\Partner\ProductReview as ProductReview;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductReviewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ProductReview::collection($this->collection),
        ];
    }
}
