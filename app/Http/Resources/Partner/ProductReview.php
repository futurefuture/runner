<?php

namespace App\Http\Resources\Partner;

use App\Product;
use App\User;
use Illuminate\Http\Resources\Json\Resource;

class ProductReview extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);
        $product = Product::find($this->product_id);

        if ($user) {
            $userName = $user->first_name.' '.substr($user->last_name, 0, 1).'.';
        }

        return [
            'type'             => 'reviews',
            'id'               => $this->id,
            'attributes'       => [
                'title'       => $this->title,
                'value'       => $this->value,
                'product'     => $product,
                'userId'      => $this->user_id,
                'firstName'   => $user->first_name,
                'lastName'    => $user->last_name,
                'userName'    => $userName ?? null,
                'avatarImage' => $user->avatar_image,
                'isApproved'  => $this->is_approved,
                'description' => $this->description,
                'helpful'     => $this->helpful,
                'unhelpful'   => $this->unhelpful,
                'createdAt'   => $this->created_at,
            ],
        ];
    }
}
