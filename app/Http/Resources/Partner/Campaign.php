<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class Campaign extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'campaigns',
            'id'         => (string) $this->id,
            'attributes' => [
                'partnerId'       => $this->partner_id,
                'title'           => $this->title,
                'state'           => $this->state,
                'startDate'       => $this->start_date,
                'endDate'         => $this->end_date,
                'createdAt'       => $this->created_at,
                'updatedAt'       => $this->updated_at,
                'totalBudget'     => $this->formattedAdInfo['totalBidAmount'],
                'totalImpression' => $this->formattedAdInfo['totalAdImpressions'],
                'totalClicks'     => $this->formattedAdInfo['totalAdClicks'],
                'totalATC'        => $this->formattedAdInfo['totalATC'],
            ],
        ];
    }
}
