<?php

namespace App\Http\Resources\Partner;

use App\Http\Resources\Partner\Campaign as CampaignResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CampaignCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => CampaignResource::collection($this->collection),
        ];
    }
}
