<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class Ad extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $adTitle = '';

        if ($this->ad_type_id === 1) {
            $adTitle = 'Product Carousel';
        } else if ($this->ad_type_id === 2) {
            $adTitle = 'Hero Carousel';
        };

        return [
            'type'       => 'ads',
            'id'         => (string) $this->id,
            'attributes' => [
                'adType'     => $adTitle,
                'partnerId'  => $this->partner_id,
                'campaignId' => $this->campaign_id,
                'adTitle'    => $this->ad_title,
                'content'    => $this->content,
                'products'   => $this->products,
                'state'      => $this->campaign_id,
                'bidType'    => $this->bid_type,
                'budget'     => $this->budget / 100,
                'startDate'  => $this->start_date,
                'endDate'    => $this->end_date,
                'position'   => $this->index,
            ],
        ];
    }
}
