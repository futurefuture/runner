<?php

namespace App\Http\Resources\Partner;

use App\Http\Resources\Partner\ProductSales as ProductSalesResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductSalesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ProductSalesResource::collection($this->collection),
        ];
    }
}
