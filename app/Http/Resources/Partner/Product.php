<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'partner products',
            'id'         => (string) $this->product_id,
            'attributes' => [
                'id'          => $this->product_id,
                'title'       => $this->title,
                'producer'    => $this->producer,
                'packaging'   => $this->packaging,
                'retailPrice' => $this->retail_price,
                'unitsSold'   => $this->unitsSold,
                'totalSold'   => $this->totalSold,
            ],
        ];
    }
}
