<?php

namespace App\Http\Resources;

use App\RewardPoint;
use Illuminate\Http\Resources\Json\Resource;

class NewUser extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $rewardPoints = RewardPoint::where('user_id', $this->id)->get();
        $rewardPointsBalance = 0;

        foreach ($rewardPoints as $rewardPoint) {
            $rewardPointsBalance += $rewardPoint->value;
        }

        return [
            'type'                 => 'users',
            'id'                   => $this->id,
            'attributes'           => [
                'firstName'         => $this->first_name,
                'lastName'          => $this->last_name,
                'dateOfBirth'       => $this->date_of_birth,
                'phoneNumber'       => $this->phone_number,
                'email'             => $this->email,
                'avatarImage'       => $this->avatar_image,
                'inviteCode'        => $this->invite_code,
                'rewardPoints'      => $rewardPointsBalance,
                'promoNotification' => $this->promo_notification,
                'orderNotification' => $this->order_notification,
            ],
        ];
    }
}
