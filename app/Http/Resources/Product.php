<?php

namespace App\Http\Resources;

use App\Category;
use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $category = Category::find($this->category_id);
        $slug = $category->slug;
        $limitedTimeOfferPrice = null;
        $rewardPoints = 0;
        $productRating = round(Review::where('product_id', $this->id)
            ->where('is_approved', 1)
            ->avg('value')) ?? 99;
        $reviewsCount = Review::where('product_id', $this->id)
            ->where('is_approved', 1)
            ->get()
            ->count();
        $reviewsAverage = (int) (Review::where('product_id', $this->id)
            ->where('is_approved', 1)
            ->avg('value') * 100) ?? (0 * 100);

        $reviews = (object) [
            'count'   => $reviewsCount,
            'average' => $reviewsAverage,
        ];

        if ($category->id !== 1 && $category->id !== 33 && $category->id !== 34 && $category->id !== 192) {
            while ($category->parent_id !== 1 && $category->parent_id !== 33 && $category->parent_id !== 34 && $category->id !== 192) {
                $category = Category::find($category->parent_id);
                $slug = $category->slug.'/'.$slug;
            }
        }

        // limited time offer
        if ($this->incentives()->where('incentive_id', 1)->where('is_active', 1)->pluck('savings')->first()) {
            $d = $this->incentives()->where('incentive_id', 1)->where('is_active', 1)->first();
            $limitedTimeOfferPrice = $this->runner_price - $d->savings;
            $limitedTimeOfferSavings = $d->savings;
        } else {
            $limitedTimeOfferPrice = null;
            $limitedTimeOfferSavings = null;
        }

        // quantity discount
        if ($this->incentives()->where('incentive_id', 3)->pluck('savings', 'minimum_quantity')->first()) {
            $q = $this->incentives()->where('incentive_id', 3)->first();

            if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                $quantityDiscount = (object) [
                    'savings'  => $q->savings,
                    'quantity' => $q->minimum_quantity,
                ];
            } else {
                $quantityDiscount = null;
            }
        } else {
            $quantityDiscount = null;
        }

        // reward points
        if ($this->incentives()->where('incentive_id', 7)->pluck('reward_points')->first()) {
            $q = $this->incentives()->where('incentive_id', 7)->first();

            if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                $rewardPoints = $q->reward_points;
            } else {
                $rewardPoints = 0;
            }
        } else {
            $rewardPoints = 0;
        }

        return [
            'type'       => 'products',
            'id'         => (string) $this->id,
            'attributes' => [
                'sku'                     => $this->sku,
                'upc'                     => $this->upc,
                'isActive'                => $this->is_active,
                'title'                   => $this->title,
                'index'                   => $this->index,
                'longDescription'         => $this->long_description,
                'shortDescription'        => $this->short_description,
                'retailPrice'             => $this->retail_price,
                'wholesalePrice'          => $this->wholesale_price,
                'limitedTimeOfferPrice'   => $limitedTimeOfferPrice,
                'limitedTimeOfferSavings' => $limitedTimeOfferSavings,
                'markupPercentage'        => $this->markup_percentage,
                'markup'                  => $this->markup,
                'runnerPrice'             => $this->runner_price,
                'image'                   => $this->image,
                'imageThumbnail'          => $this->image_thumbnail,
                'packaging'               => $this->packaging,
                'alcoholContent'          => $this->alcohol_content,
                'sugarContent'            => $this->sugar_content * 100,
                'slug'                    => $this->slug,
                'categoryId'              => $this->category_id,
                'category'                => $category->slug,
                // 'store'                   => $this->store(),
                'style'                   => isset($this->tags->where('type', 'style')->first()->title) ? $this->tags->where('type', 'style')->first()->title : null,
                'producingCountry'        => $this->producing_country,
                'producer'                => $this->producer,
                'productRating'           => $productRating,
                'reviews'                 => $reviews,
                'rewardPoints'            => $rewardPoints,
                'isGift'                  => $this->isGift,
                'quantity'                => $this->pivot->quantity ?? null,
                'caseDeal'                => $this->incentives()->where('incentive_id', 2)->pluck('savings')->first(),
                'quantityDiscount'        => $quantityDiscount,
                'inStock'                 => $this->inStock,
            ],
            'links' => [
                'self' => url('/v3/runner/products/'.$this->id),
            ],
        ];
    }
}
