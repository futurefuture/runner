<?php

namespace App\Http\Resources;

use App\Http\Resources\Incentive as IncentiveResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class IncentivesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => IncentiveResource::collection($this->collection),
        ];
    }
}
