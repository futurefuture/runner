<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Partner extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        return [
            'type'       => 'partners',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'       => $this->title,
                'user_id'     => $this->user_id,
                'image'       => $this->image,
                'email'       => $user->email,
                'firstName'   => $user->first_name,
                'lastName'    => $user->first_name,
                'phoneNumber' => $user->phone_number,
                'storeId'     => $this->store_id,
                'created_at'  => $this->created_at,
                'updated_at'  => $this->updated_at,
            ],
        ];
    }
}
