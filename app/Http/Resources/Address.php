<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Address extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'             => 'addresses',
            'id'               => (string) $this->id,
            'attributes'       => [
                'userId'           => (string) $this->user_id,
                'formattedAddress' => $this->formatted_address,
                'lat'              => (float) $this->lat,
                'lng'              => (float) $this->lng,
                'placeId'          => $this->place_id,
                'postalCode'       => $this->postal_code,
                'unitNumber'       => $this->unit_number,
                'businessName'     => $this->business_name,
                'instructions'     => $this->instructions,
                'selected'         => $this->selected,
            ],
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
