<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'categories',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'      => $this->title,
                'parent_id'  => $this->parent_id,
                'slug'       => $this->slug,
                'type'       => $this->type,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
        ];
    }
}
