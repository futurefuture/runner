<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class ProductFinal extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $currentTime = Carbon::now();

        return [
            'id'                                    => $this->id,
            'name'                                  => $this->name,
            'limited_time_offer_savings_in_cents'   => $this->formatted_limited_time_offer_savings_in_cents,
            'primary_category'                      => $this->primary_category,
            'secondary_category'                    => $this->secondary_category,
            'origin'                                => $this->origin,
            'package'                               => $this->package,
            'alcohol_content'                       => $this->alcohol_content,
            'sugar_content'                         => $this->sugar_content,
            'sugar_in_grams_per_liter'              => $this->sugar_in_grams_per_liter,
            'producer_name'                         => $this->producer_name,
            'released_on'                           => $this->released_on,
            'description'                           => $this->description,
            'tasting_note'                          => $this->tasting_note,
            'runner_price_in_cents'                 => $this->runner_price_in_cents,
            'image_url'                             => $this->image_url,
            'style'                                 => $this->style,
            'slug'                                  => $this->slug,
            'regular_price_in_cents'                => $this->regular_price_in_cents,
            'limited_time_offer_ends_on'            => $this->formatted_limited_time_offer_ends_on,
            'wine_rating'                           => $this->wine_rating,
            'rating'                                => $this->rating,
        ];
    }
}
