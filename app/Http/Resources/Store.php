<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'stores',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'            => $this->title,
                'isActive'         => $this->is_active,
                'layout'           => $this->layout,
                'options'          => [
                    'towerImage'      => $this->options['towerImage'] ?? null,
                    'blockImage'      => $this->options['blockImage'] ?? null,
                    'storeLogo'       => $this->options['storeLogo'] ?? null,
                    'navigationLogo'  => $this->options['navigationLogo'] ?? null,
                    'bannerLogo'      => $this->options['bannerLogo'] ?? null,
                    'primaryColor'    => $this->options['primaryColor'] ?? null,
                    'secondaryColor'  => $this->options['secondaryColor'] ?? null,
                    'hours'           => $this->options['hours'] ?? null,
                    'storeHighlight'  => [
                        'icon' => $this->options['storeHighlight']['icon'] ?? null,
                        'text' => $this->options['storeHighlight']['text'] ?? null,
                    ],
                    'carousel'         => $this->options['carousel'] ?? null,
                    'bannerImage'      => $this->options['bannerImage'] ?? null,
                    'nextDeliveryTime' => $this->options['nextDeliveryTime'] ?? null,
                    'tagLine'          => $this->options['tagLine'] ?? null,
                    'aboutText'        => $this->options['aboutText'] ?? null,
                    'exploreBlurb'     => $this->options['exploreBlurb'] ?? null,
                    'featuredProducts' => $this->options['featuredProducts'] ?? null,
                    'categories'       => $this->options['categories'] ?? null,
                    'layouts'          => $this->options['layouts'] ?? null,
                    'giftOptions'      => isset($this->options['giftOptions']) ? $this->options['giftOptions'] : null,
                ],
            ],
            'links' => [
                'self' => url('/v3/runner/stores/'.$this->id),
            ],
        ];
    }
}
