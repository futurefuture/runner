<?php

namespace App\Http\Resources;

use App\Incentive;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductIncentive extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $incentive = Incentive::find($this->incentive_id);

        return [
            'type'       => 'product incentives',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'            => $incentive->title,
                'incentiveId'      => $this->incentive_id,
                'productId'        => $this->product_id,
                'startDate'        => $this->start_date,
                'endDate'          => $this->end_date,
                'savings'          => $this->savings,
                'isActive'         => $this->is_active,
                'rewardPoints'     => $this->reward_points,
                'minimumQuantity'  => $this->minimum_quantity,
                'couponCode'       => $this->coupon_code,
                'minimumCartValue' => $this->minimum_cart_value,
                'created_at'       => $this->created_at,
                'updated_at'       => $this->updated_at,
            ],
        ];
    }
}
