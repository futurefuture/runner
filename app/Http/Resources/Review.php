<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\Resource;

class Review extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        if ($user) {
            $userName = $user->first_name.' '.substr($user->last_name, 0, 1).'.';
        }

        return [
            'type'             => 'reviews',
            'id'               => $this->id,
            'attributes'       => [
                'title'          => $this->title,
                'value'          => $this->value,
                'product_id'     => $this->product_id,
                'user_id'        => $this->user_id,
                'firstName'      => $user->first_name,
                'lastName'       => $user->last_name,
                'userName'       => $userName ?? null,
                'avatarImage'    => $user->avatar_image,
                'is_approved'    => $this->is_approved,
                'description'    => $this->description,
                'helpful'        => $this->helpful,
                'unhelpful'      => $this->unhelpful,
            ],
        ];
    }
}
