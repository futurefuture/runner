<?php

namespace App\Http\Resources\Dispatch;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'type'       => 'users',
            'id'         => (string) $this->id,
            'attributes' => [
                'firstName'      => $this->first_name,
                'lastName'       => $this->last_name,
                'dateOfBirth'    => $this->date_of_birth,
                'phoneNumber'    => $this->phone_number,
                'email'          => $this->email,
                'gender'         => $this->gender,
                'facbookId'      => $this->facebook_id,
                'googleId'       => $this->google_id,
                'unit'           => $this->unit,
                'stripeId'       => $this->stripe_id,
                'rewardPoints'   => $this->rewardPoints,
                'stripeId'       => $this->stripe_id,
                'createdAt'      => $this->created_at,
                'address'        => $this->address_objects,
                'notes'          => $this->notes,
            ],
        ];

        return $data;
    }
}
