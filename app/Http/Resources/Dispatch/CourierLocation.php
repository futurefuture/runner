<?php

namespace App\Http\Resources\Dispatch;

use App\RunnerLocation;
use Illuminate\Http\Resources\Json\JsonResource;

class CourierLocation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'type'       => 'locations',
            'id'         =>  $this->id,
            'attributes' => [
                'runner_id'  => $this->runner_id,
                'latitude'   => $this->latitude,
                'longitude'  => $this->longitude,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
        ];

        return $data;
    }
}
