<?php

namespace App\Http\Resources\Dispatch;

use App\Classes\Utilities;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $utilities    = new Utilities();
        $storeOptions = $this->store->options;

        if ($this->gift) {
            $formattedGift                = [];
            $formattedGift['id']          = $this->gift->id;
            $formattedGift['orderId']     = $this->gift->order_id;
            $formattedGift['firstName']   = $this->gift->first_name;
            $formattedGift['lastName']    = $this->gift->last_name;
            $formattedGift['isNineteen']  = $this->gift->is_19;
            $formattedGift['email']       = $this->gift->email;
            $formattedGift['phoneNumber'] = $this->gift->phone_number;
            $formattedGift['message']     = $this->gift->message;
            $formattedGift['giftOptions'] = $this->gift->gift_options;
            $data['attributes']['gift']   = $formattedGift;
        }
        // this one does not work for status 7?
        if ($this->status !== 7) {
            $c = $utilities->changeCamelCaseToSnakeCase(json_decode($this->customer, true));
        } else {
            if (is_array(json_decode($this->customer, true))) {
                $c = $utilities->changeCamelCaseToSnakeCase(json_decode($this->customer, true));
            } else {
                $c = [];
            }
        }
        // $c = $utilities->changeCamelCaseToSnakeCase(array_map('intval', explode(',', $this->customer)));

        // $customer->tags = $this->user->tags;

        $data = [
            'type'  => 'orders',
            'id'    => $this->id,
            'store' => [
                'id'    => $this->store->id ?? null,
                'logo'  => $storeOptions['storeLogo'] ?? null,
            ],
            'inventory_id'  => $this->inventory_id,
            'user_id'       => $this->user->id,
            'status'        => $this->status,
            'content'       => json_decode($this->content),
            'courier'       => [
                'id'         => $this->courier->id ?? null,
                'first_name' => $this->courier->first_name ?? null,
                'last_name'  => $this->courier->last_name ?? null,
            ],
            'admin_notes'       => $this->admin_notes,
            'device'            => $this->device,
            'customer'          => $c,
            'address'           => json_decode($this->address),
            'unit_number'       => $this->unit_number,
            'coupon'            => $this->coupon_id,
            'instructions'      => $this->instructions,
            'subtotal'          => $this->subtotal / 100,
            'service_fee'       => $this->service_fee / 100,
            'discount'          => $this->discount / 100,
            'delivery'          => $this->delivery / 100,
            'tip'               => $this->tip / 100,
            'display_tax_total' => $this->display_tax_total / 100,
            'total'             => $this->total / 100,
            'signature'         => $this->signature,
            'schedule_time'     => $this->schedule_time,
            'delivered_at'      => $this->delivered_at,
            'created_at'        => Carbon::parse($this->created_at)->toDateTimeString(),
            'gift'              => $formattedGift ?? null,
            'inventory_id'      => $this->inventory_id ?? null,
            'first_time'        => $this->first_time,
            'payment_method'    => $this->card_type ?? 'VISA',
            'risk_score'        => $this->risk_score ?? '2',
            'is_apple_pay'      => $this->paid_via_apple_pay,
            'has_rush_products' => $this->has_rush_products
        ];

        return $data;
    }
}
