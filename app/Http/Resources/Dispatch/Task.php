<?php

namespace App\Http\Resources\Dispatch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Dispatch\Order as OrderResource;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $logo = $this->order->store->options['storeLogo'] ?? null;

        return [
            'type'       => 'tasks',
            'id'         => (string) $this->id,
            'storeLogo'  => $logo,
            'attributes' => [
                'state'        => $this->state,
                'index'        => $this->index,
                'notes'        => $this->notes,
                'phoneNumber'  => $this->phone_number,
                'name'         => $this->name,
                'organization' => $this->organization ?? 'runner',
                'address'      => $this->address,
                'courier'      => $this->courier->only([
                    'id',
                    'first_name',
                    'role'
                ]),
                'order'        => new OrderResource($this->order),
                'container'    => $this->container_id ?? null,
                'color'        => $this->color,
                'type'         => $this->type,
                'createdAt'    => Carbon::parse($this->created_at)->toDateTimeString(),
                'signature'    => $this->signature,
            ],
        ];
    }
}
