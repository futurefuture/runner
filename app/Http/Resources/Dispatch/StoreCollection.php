<?php

namespace App\Http\Resources\Dispatch;

use App\Http\Resources\Dispatch\Store as StoreResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StoreCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => StoreResource::collection($this->collection),
        ];
    }
}
