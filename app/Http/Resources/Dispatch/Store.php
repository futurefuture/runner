<?php

namespace App\Http\Resources\Dispatch;

use App\Address;
use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $location = Address::withTrashed()->find($this->address_id);

        $data = [
            'type' => 'inventories',
            'id' => (string) $this->id,
            'attributes' => [
                'title' => $this->id,
                'vendor' => $this->vendor,
                'location' => [
                    'address' => $location->formatted_address,
                    'lat' => $location->lat,
                    'lng' => $location->lng,
                    'postalCode' => $location->postal_code,
                ],
                'store' => [
                    'id' => $this->id ?? null,
                    'logo' => $this->options['storeLogo'] ?? null,
                    'hours' => $this->options['hours'] ?? null,
                ],
            ],
        ];

        return $data;
    }
}
