<?php

namespace App\Http\Resources;

use App\Http\Resources\ProductIncentive as ProductIncentiveResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductIncentivesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ProductIncentiveResource::collection($this->collection),
        ];
    }
}
