<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'tags',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'      => $this->title,
                'slug'       => $this->slug,
                'type'       => $this->type,
            ],
        ];
    }
}
