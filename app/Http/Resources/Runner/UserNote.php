<?php

namespace App\Http\Resources\Runner;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserNote extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        return [
            'type'       => 'user notes',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'      => $this->title,
                'body'       => $this->body,
                'userId'     => $user->id,
                'createdAt'  => $this->created_at,
                'updatedAt'  => $this->updated_at,
            ],
        ];
    }
}
