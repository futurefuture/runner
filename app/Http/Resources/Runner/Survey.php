<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Runner\SurveyOption as SurveyOptionResource;

class Survey extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'surveys',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'           => $this->title,
                'question'        => $this->question,
                'startDate'       => $this->start_date,
                'endDate'         => $this->end_date,
                'results'         => $this->results,
                'segment'         => $this->segment,
                'partnerId'       => (string) $this->partner_id,
                'rewardPoints'    => 500,
                'backgroundColor' => '#fefefe',
                'options'         => SurveyOptionResource::collection($this->surveyOptions),
                'createdAt'       => $this->created_at,
                'updatedAt'       => $this->updated_at,
            ],
        ];
    }
}
