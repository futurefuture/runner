<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Category as CategoryResource;
use App\Http\Resources\Runner\TagResource;
use Illuminate\Http\Resources\Json\Resource;

class Category extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'categories',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'       => $this->title,
                'parentId'    => (string) $this->parent_id,
                'slug'        => $this->slug,
                'options'     => json_decode($this->options),
                'squareImage' => $this->square_image,
                'bannerImage' => $this->banner_image,
                'pillImage'   => $this->pill_image,
                'description' => $this->description,
                'children'    => CategoryResource::collection($this->whenLoaded('children')),
                'tags'        => TagResource::collection($this->whenLoaded('tags')),
            ],
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
