<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Ad as AdResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AdCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => AdResource::collection($this->collection),
        ];
    }
}
