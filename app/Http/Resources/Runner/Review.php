<?php

namespace App\Http\Resources\Runner;

use App\User;
use Illuminate\Http\Resources\Json\Resource;

class Review extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        if ($user) {
            $userName = $user->first_name . ' ' . substr($user->last_name, 0, 1) . '.';
        }

        return [
            'type'             => 'reviews',
            'id'               => (string) $this->id,
            'attributes'       => [
                'title'          => $this->title,
                'value'          => $this->value,
                'productId'      => (string) $this->product_id,
                'userId'         => (string) $this->user_id ?? null,
                'firstName'      => $user->first_name ?? null,
                'lastName'       => $user->last_name ?? null,
                'userName'       => $userName ?? null,
                'avatarImage'    => $user->avatar_image ?? null,
                'isApproved'     => $this->is_approved,
                'description'    => $this->description,
                'helpful'        => $this->helpful,
                'unHelpful'      => $this->unhelpful,
                'createdAt'      => $this->created_at->format('Y-m-d H:i:s'),
            ],
        ];
    }
}
