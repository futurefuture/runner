<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Incentice as IncentiveResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class InventoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => Incentive::collection($this->collection),
        ];
    }
}
