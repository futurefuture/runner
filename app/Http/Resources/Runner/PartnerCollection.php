<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Partner as PartnerResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PartnerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PartnerResource::collection($this->collection),
        ];
    }
}
