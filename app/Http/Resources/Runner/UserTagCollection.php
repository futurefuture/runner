<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\UserTag as UserTagResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserTagCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => UserTagResource::collection($this->collection),
        ];
    }
}
