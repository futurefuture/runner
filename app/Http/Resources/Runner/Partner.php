<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\JsonResource;

class Partner extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'partners',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'  => $this->title,
                'userId' => (string) $this->user_id,
                'image'  => $this->image,
            ],
        ];
    }
}
