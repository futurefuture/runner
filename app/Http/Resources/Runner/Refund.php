<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class Refund extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'refunds',
            'id' => (string) $this->id,
            'attributes' => [
                'orderId' => $this->order_id,
                'userId' => $this->user_id,
                'refundId' => $this->refund_id,
                'chargeId' => $this->charge_id,
                'amount' => $this->amount,
                'reason' => $this->reason,
                'createdAt' => $this->created_at,
            ],
        ];
    }
}
