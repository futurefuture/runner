<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\JsonResource;

class UserTag extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'tags',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'     => $this->title,
                'slug'      => $this->slug,
                'type'      => $this->type,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
        ];
    }
}
