<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\SurveyOption as SurveyOptionResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SurveyOptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => SurveyOptionResource::collection($this->collection),
        ];
    }
}
