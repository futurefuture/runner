<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ValidatedStoreByPostalCode extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'validated stores by postal code',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'            => $this->title,
                'storeLogo'        => $this->options['storeLogo'] ?? null,
                'navigationLogo'   => $this->options['navigationLogo'] ?? null,
                'hours'            => $this->options['hours'] ?? null,
                'blockImage'       => $this->options['blockImage'] ?? null,
                'nextDeliveryTime' => $this->options['nextDeliveryTime'] ?? null,
                'aboutText'        => $this->options['aboutText'] ?? null,
                'subDomain'        => $this->sub_domain,
                'inventoryId'      => $this->inventory_id,
                'categories'       => CategoryResource::collection($this->categories) ?? null,
            ],
        ];
    }
}
