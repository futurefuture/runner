<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class SurveyOptionCount extends Resource
{
    protected $count;

    public function count($value)
    {
        $this->count = $value;

        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'survey options',
            'id'         => (string) $this->id,
            'attributes' => [
                'surveyId'   => (string) $this->survey_id,
                'title'      => $this->title,
                'percentage' => $this->count * 100,
                'createdAt'  => $this->created_at,
                'updatedAt'  => $this->updated_at,
            ],
        ];
    }
}
