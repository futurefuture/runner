<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class Ad extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'             => 'ads',
            'id'               => (string) $this->id,
            'attributes'       => [
                'adTypeId'            => $this->ad_type_id,
                'partnerId'           => $this->partner_id,
                'campaignId'          => $this->campaign_id,
                'content'             => $this->content,
                'products'            => $this->products,
                'productId'           => $this->product_id,
                'categoryId'          => $this->category_id,
                'tagId'               => $this->tag_id,
                'carouselId'          => $this->carousel_id,
                'carouselComponentId' => $this->carousel_component_id,
                'bidType'             => $this->bid_type,
                'bidAmount'           => $this->bid_amount,
                'state'               => $this->state,
                'index'               => $this->index,
                'startDate'           => $this->start_date,
                'endDate'             => $this->end_date
            ],
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
