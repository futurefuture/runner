<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\ValidatedStoreByPostalCode as ValidatedStoreByPostalCodeResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ValidatedStoreByPostalCodeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ValidatedStoreByPostalCodeResource::collection($this->collection),
        ];
    }
}
