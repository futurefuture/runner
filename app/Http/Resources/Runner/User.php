<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'         => 'users',
            'id'           => (string) $this->id,
            'attributes'   => [
                'firstName'           => $this->first_name,
                'lastName'            => $this->last_name,
                'dateOfBirth'         => $this->date_of_birth,
                'phoneNumber'         => $this->phone_number,
                'email'               => $this->email,
                'gender'              => $this->gender,
                'avatarImage'         => $this->avatar_image,
                'inviteCode'          => $this->invite_code,
                'rewardPointsBalance' => $this->rewardPointsBalance(),
                'addresses'           => Address::collection($this->whenLoaded('addresses')),
                'favourites'          => Favourite::collection($this->whenLoaded('favourites')),
                'notifications'       => [
                    'promoNotification' => (bool) $this->promo_notification,
                    'orderNotification' => (bool) $this->order_notification,
                ],
                'createdAt' => $this->created_at,
            ],
        ];
    }
}
