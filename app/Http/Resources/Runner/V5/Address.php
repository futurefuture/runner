<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;

class Address extends Resource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'type'              => 'addresses',
      'id'                => (string) $this->id,
      'user_id'           => (string) $this->user_id,
      'formatted_address' => $this->formatted_address,
      'lat'               => (float) $this->lat,
      'lng'               => (float) $this->lng,
      'place_id'          => $this->place_id,
      'postal_code'       => $this->postal_code,
      'unit_number'       => $this->unit_number,
      'business_name'     => $this->business_name,
      'instructions'      => $this->instructions,
      'selected'          => $this->selected,
      'links' => [
        'self' => url('/v5/runner/users/' . $this->user_id . '/addresses/' . $this->id),
      ],
    ];
  }
}
