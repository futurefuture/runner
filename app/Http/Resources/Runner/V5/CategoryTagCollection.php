<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\CategoryTag as CategoryTagResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryTagCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => CategoryTagResource::collection($this->collection),
        ];
    }
}
