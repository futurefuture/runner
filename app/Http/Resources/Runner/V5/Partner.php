<?php

namespace App\Http\Resources\Runner\V5;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Partner extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'partners',
            'id'            => (string) $this->id,
            'title'         => $this->title,
            'user_id'       => $this->user_id,
            'image'         => $this->image,
            'email'         => $this->user->email ?? '',
            'first_name'    => $this->user->first_name ?? '',
            'last_name'     => $this->user->first_name ?? '',
            'phone_number'  => $this->user->phone_number ?? '',
            'store_id'      => $this->store_id,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ];
    }
}
