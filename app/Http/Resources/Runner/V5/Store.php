<?php

namespace App\Http\Resources\Runner\V5;

use App\Classes\Utilities;
use App\PostalCode;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Runner\V5\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $utilities     = new Utilities();

        $unwrapped_layout = [];
        foreach ($this->layout as $l) {
            $layout_id = $l['id'];
            $layout_attributes = $l['attributes'];
            $layout_attributes['id'] = $layout_id;

            array_push($unwrapped_layout, $layout_attributes);
        }

        $unwrapped_layout = $utilities->changeCamelCaseToSnakeCase($unwrapped_layout);

        // manipulating inventory_id into this resource
        $fullPostalCode  = isset($request->all()['postalCode']) ? $request->all()['postalCode'] : 'M6J1G5';
        $postalCode      = PostalCode::find(substr($fullPostalCode, 0, 3));
        $inventoryQuery  = DB::table('inventories')
            ->join('inventory_territory', 'inventories.id', '=', 'inventory_territory.inventory_id')
            ->join('territories', 'territories.id', '=', 'inventory_territory.territory_id')
            ->select('inventories.*')
            ->where('territories.id', $postalCode->territory->id)
            ->where('inventories.store_id', $this->id);
        $inventoryId     = isset($inventoryQuery->pluck('id')[0]) ? $inventoryQuery->pluck('id')[0] : 1;



        return [
            'id'         => (string) $this->id,
            'title'        => $this->title,
            'layout'       => $unwrapped_layout,
            'notification' => $this->notification ?? null,
            // 'inventory'        => $this->territory,
            'inventory_id' => $inventoryId,
            'index'   => $this->index,
            'options' => [
                'tower_image'     => $this->options['towerImage'] ?? null,
                'block_mage'     => $this->options['blockImage'] ?? null,
                'store_logo'      => $this->options['storeLogo'] ?? null,
                'navigation_logo' => $this->options['navigationLogo'] ?? null,
                'banner_logo'     => $this->options['bannerLogo'] ?? null,
                'primary_color'   => $this->options['primaryColor'] ?? null,
                'secondary_color' => $this->options['secondaryColor'] ?? null,
                'hours'          => $this->options['hours'] ?? null,
                'store_highlight' => [
                    'icon' => $this->options['storeHighlight']['icon'] ?? null,
                    'text' => $this->options['storeHighlight']['text'] ?? null,
                ],
                'banner_image'      => $this->options['bannerImage'] ?? null,
                'next_delivery_time' => $this->nextDeliveryTime(),
                'tag_line'          => $this->options['tagLine'] ?? null,
                'about_text'        => $this->options['aboutText'] ?? null,
                'explore_blurb'     => $this->options['exploreBlurb'] ?? null,
                'categories'       => CategoryResource::collection($this->categories),
                'gift_options'      => isset($this['options']['giftOptions']) ? $this['options']['giftOptions'] : null,
                'inverted_logo'     => $this->options['invertedLogo'] ?? false,
            ],
            'cart_extras'       => $this->cart_extras,
            'is_for_recipient'   => $this->is_for_recipient,
            'next_delivery_time' => $this->nextDeliveryTime(),
            'delivery_fee'      => $this->deliveryFee(),
            'links' => [
                'self' => url('/v4/runner/stores/' . $this->id),
            ],
        ];
    }
}
