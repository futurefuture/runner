<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;

class SurveyResponse extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'survey responses',
            'id'         => (string) $this->id,
            'survey_id'       => (string) $this->survey_id,
            'survey_option_id' => (string) $this->survey_option_id,
            'user_id'         => (string) $this->user_id,
            'created_at'      => $this->created_at,
            'updated_at'      => $this->updated_at,
        ];
    }
}
