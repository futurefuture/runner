<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;

class Refund extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'refunds',
            'id' => (string) $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'refund_id' => $this->refund_id,
            'charge_id' => $this->charge_id,
            'amount' => $this->amount,
            'reason' => $this->reason,
            'created_at' => $this->created_at,
        ];
    }
}
