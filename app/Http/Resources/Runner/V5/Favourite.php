<?php

namespace App\Http\Resources\Runner\V5;

use App\Store;
use App\Classes\Utilities;
use Illuminate\Http\Resources\Json\Resource;

class Favourite extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $utilities     = new Utilities();
        $store = Store::find($this->store_id);
        $store['options'] = $utilities->changeCamelCaseToSnakeCase($store['options']);

        return [
            'type'       => 'favourites',
            'id'         => (string) $this->id,
            'user_id'    => (string) $this->user_id,
            'product_id' => (string) $this->product_id,
            'store'      => $store,
            'links' => [
                'self' => url('/runner/v4' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
