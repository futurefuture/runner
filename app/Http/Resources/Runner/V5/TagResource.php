<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'tags',
            'id'         => (string) $this->id,
            'title'      => $this->title,
            'slug'       => $this->slug,
            'type'       => $this->type,
        ];
    }
}
