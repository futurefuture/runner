<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryTag extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'tags',
            'id'         => (string) $this->id,
            'title'      => $this->title,
            'slug'       => $this->slug,
            'tag_type'   => $this->type,
            'isActive'   => $this->is_active,
            'isVisible'  => $this->is_visible,
        ];
    }
}
