<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Store as StoreResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StoresCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => StoreResource::collection($this->collection),
        ];
    }
}
