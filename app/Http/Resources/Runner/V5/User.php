<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'                  => 'users',
            'id'                    => (string) $this->id,
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'date_of_birth'         => $this->date_of_birth,
            'phone_number'          => $this->phone_number,
            'email'                 => $this->email,
            'gender'                => $this->gender,
            'avatar_image'          => $this->avatar_image,
            'invite_code'           => $this->invite_code,
            'reward_points_balance' => $this->rewardPointsBalance(),
            'addresses'             => Address::collection($this->whenLoaded('addresses')),
            'favourites'            => Favourite::collection($this->whenLoaded('favourites')),
            'notifications'         => [
                'promo_notification' => (bool) $this->promo_notification,
                'order_notification' => (bool) $this->order_notification,
                'cart_notification'     => $this->cart_notification,
            ],
            'created_at'            => $this->created_at,
        ];
    }
}
