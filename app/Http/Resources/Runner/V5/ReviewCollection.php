<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Review as ReviewResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ReviewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ReviewResource::collection($this->collection),
        ];
    }
}
