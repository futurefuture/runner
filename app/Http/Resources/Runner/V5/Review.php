<?php

namespace App\Http\Resources\Runner\V5;


use App\User;
use Illuminate\Http\Resources\Json\Resource;

class Review extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        if ($user) {
            $userName = $user->first_name . ' ' . substr($user->last_name, 0, 1) . '.';
        }

        return [
            'type'              => 'reviews',
            'id'                => (string) $this->id,
            'title'             => $this->title,
            'value'             => $this->value,
            'product_id'        => (string) $this->product_id,
            'user_id'           => (string) $this->user_id ?? null,
            'first_name'        => $user->first_name ?? null,
            'last_name'         => $user->last_name ?? null,
            'user_name'         => $userName ?? null,
            'avatar_image'      => $user->avatar_image ?? null,
            'is_approved'       => $this->is_approved,
            'description'       => $this->description,
            'helpful'           => $this->helpful,
            'unhelpful'         => $this->unhelpful,
            'created_at'        => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
