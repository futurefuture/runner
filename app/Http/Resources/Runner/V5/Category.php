<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Category as CategoryResource;
use App\Http\Resources\Runner\V5\TagResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'categories',
            'id'         => (string) $this->id,
            'title'       => $this->title,
            'parent_id'    => (string) $this->parent_id,
            'slug'        => $this->slug,
            'options'     => json_decode($this->options),
            'square_image' => $this->square_image,
            'banner_image' => $this->banner_image,
            'description' => $this->description,
            'children'    => CategoryResource::collection($this->whenLoaded('children')),
            'tags'        => TagResource::collection($this->whenLoaded('tags')),
            'links' => [
                'self' => url('/v3/runner/users/' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
