<?php

namespace App\Http\Resources\Runner\V5;

use App\Classes\Utilities;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($o) {
                $o->customer = json_decode($o->customer);
                $o->address = json_decode($o->address);
                $items = gettype($o->content) == 'string' ? json_decode($o->content) : $o->content;

                return [
                    'id'    =>  $o->id,
                    'store'           => [
                        'id'        => (string) $o->store->id,
                        'title'     => $o->store->title,
                        'logo'      => $o->store->options['storeLogo'] ?? null,
                        'sub_domain' => $o->store->sub_domain,
                    ],
                    'reward_points'    => (int) 100,
                    'status'    =>  $o->status,
                    'customer'  =>  $o->customer,
                    'items'   => (new Utilities())->changeCamelCaseToSnakeCase($items),
                    'address'   =>  $o->address,
                    'unit_number'      =>  $o->unit_number,
                    'created_at'    =>  $o->created_at->format('Y-m-d H:i:s'),
                    'delivered_at'  =>  $o->delivered_at,
                    'scheduled_time' => $o->schedule_time,
                ];
            }),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
