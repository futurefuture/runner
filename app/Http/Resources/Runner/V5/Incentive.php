<?php

namespace App\Http\Resources\Runner\V5;


use Illuminate\Http\Resources\Json\Resource;

class Incentive extends Resource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'type'       => 'incentives',
      'id'         => (string) $this->id,
      'incentiveId'      => (string) $this->incentive_id,
      'productId'        => (string) $this->product_id,
      'startDate'        => $this->start_date,
      'endDate'          => $this->end_date,
      'savings'          => $this->savings,
      'isActive'         => $this->is_active,
      'rewardPoints'     => $this->reward_points,
      'minimumQuantity'  => $this->minimum_quantity,
      'couponCode'       => $this->coupon_code,
      'minimumCartValue' => $this->minimum_cart_value,
    ];
  }
}
