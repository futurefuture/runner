<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Favourite as FavouriteResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FavouriteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => FavouriteResource::collection($this->collection),
        ];
    }
}
