<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;

class SurveyOption extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'survey options',
            'id'         => (string) $this->id,
            'survey_id'  => (string) $this->survey_id,
            'title'     => $this->title,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
