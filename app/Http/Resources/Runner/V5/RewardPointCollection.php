<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\RewardPoint as RewardPointResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RewardPointCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => RewardPointResource::collection($this->collection),
        ];
    }
}
