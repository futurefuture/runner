<?php

namespace App\Http\Resources\Runner\V5;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Runner\V5\SurveyOption as SurveyOptionResource;

class Survey extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'              => 'surveys',
            'id'                => (string) $this->id,
            'title'             => $this->title,
            'question'          => $this->question,
            'start_date'        => $this->start_date,
            'end_date'          => $this->end_date,
            'results'           => $this->results,
            'segment'           => $this->segment,
            'partner_id'        => (string) $this->partner_id,
            'reward_points'     => 500,
            'background_color'  => '#fefefe',
            'options'           => SurveyOptionResource::collection($this->surveyOptions),
            'created_at'        => $this->created_at,
            'updated_at'        => $this->updated_at,
        ];
    }
}
