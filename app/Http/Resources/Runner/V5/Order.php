<?php

namespace App\Http\Resources\Runner\V5;

use App\Gift;
use App\Invitation;
use App\Store;
use App\User;
use App\Classes\Utilities;
use DB;
use Illuminate\Http\Resources\Json\Resource;

class Order extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $utilities     = new Utilities();
        $content       = json_decode($this->content);
        $store         = Store::find($this->store_id);

        if (!$store) {
            $store = Store::find(1);
        }

        $items = gettype($content) !== 'array' ? $content->items : $content;

        foreach ($items as $key => $item) {
            $item->type  = isset($item->type) ? $item->type : 'product';
            $item->image = isset($item->image) ? $item->image : '';
            $item->id    = (string) $item->id;

            if (isset($item->partnerIds) && !empty($item->partnerIds)) {
                for ($i = 0; $i < count($item->partnerIds); $i++) {
                    $item->partnerIds[$i] = (string) $item->partnerIds[$i];
                }
            } else {
                $item->partnerIds = [];
            }

            if (isset($item->allowSub)) {
                $item->allowSub = (bool) $item->allowSub;
            }
        }

        if (isset($this->runner_1)) {
            $courier = User::find($this->runner_1) ?? null;
        } else {
            $courier = null;
        }

        $invite = Invitation::where('invited_user_id', $this->user_id)->first();
        $coupon = DB::table('coupon_user')->where('order_id', $this->id)->first();

        if ($coupon) {
            $discountType = 'Coupon';
        } elseif ($this->coupon_id === null && $this->first_time === 1 && $this->discount && $invite) {
            $discountType = 'Referred By ' . $invite->email;
        } elseif ($this->coupon_id === null && $this->first_time === 0 && $this->discount) {
            $discountType = 'Reward Points';
        } else {
            $discountType = null;
        }

        $data = [
            'type'               => 'order',
            'id'                 => $this->id,
            'user_id'            => $this->user_id,
            'status'             => $this->status,
            'items'              => $utilities->changeCamelCaseToSnakeCase($items),
            'card_last_four'     => $this->card_last_four,
            'courier'            => $courier,
            'device'             => $this->device,
            'customer'           => json_decode($this->customer),
            'address'            => json_decode($this->address),
            'unit_number'        => $this->unit_number,
            'coupon'             => (string) $this->coupon_id,
            'instructions'       => $this->instructions,
            'subtotal'           => $this->subtotal,
            'service_fee'        => $this->service_fee,
            'discount'           => $this->discount,
            'discount_type'      => $discountType,
            'delivery'           => $this->delivery,
            'tip'                => $this->tip,
            'display_tax_total'  => $this->display_tax_total,
            'total'              => $this->total,
            'signature'          => $this->signature,
            'schedule_time'      => $this->schedule_time,
            'delivered_at'       => $this->delivered_at,
            'created_at'         => $this->created_at->format('Y-m-d H:i:s'),
            'inventory_id'       => $this->inventory_id,
            'gift'               => null,
        ];

        $gift = Gift::where('order_id', $this->id)->first();

        if ($gift) {
            $formattedGift                 = [];
            $formattedGift['id']           = (string) $gift->id;
            $formattedGift['first_name']   = $gift->first_name;
            $formattedGift['last_name']    = $gift->last_name;
            $formattedGift['is_nineteen']  = true;
            $formattedGift['email']        = $gift->email;
            $formattedGift['phone_number'] = $gift->phone_number;
            $formattedGift['message']      = $gift->message;
            $data['gift']                  = $formattedGift;
            $data['service_fee']           = 500;
        }

        return [
            'type'             => 'order',
            'id'               => $this->id,
            'user_id'          => (string) $data['user_id'],
            'store'            => [
                'id'         => (string) $store->id,
                'title'      => $store->title,
                'logo'       => $store->options['storeLogo'] ?? null,
                'sub_domain' => $store->sub_domain,
            ],
            'status'            => $data['status'],
            'items'             => $data['items'],
            'card_last_four'    => $data['card_last_four'],
            'courier'           => [
                'id'         => $courier ? (string) $courier['id'] : '',
                'first_name' => $courier ? $courier['first_name'] : '',
                'last_name'  => $courier ? $courier['last_name'] : '',
            ],
            'device'            => $data['device'],
            'customer'          => $data['customer'],
            'address'           => $data['address'],
            'unit_number'       => $data['unit_number'],
            'instructions'      => $data['instructions'],
            'sub_total'         => $data['subtotal'],
            'service_fee'       => $data['service_fee'],
            'discount'          => $data['discount'],
            'discount_type'     => $data['discount_type'],
            'delivery'          => $data['delivery'],
            'tip'               => $data['tip'],
            'display_tax_total' => $data['display_tax_total'],
            'total'             => $data['total'],
            'signature'         => $data['signature'],
            'scheduled_time'    => $data['schedule_time'],
            'delivered_at'      => $data['delivered_at'],
            'created_at'        => $data['created_at'],
            'reward_points'     => (int) 100,
            'gift'              => $data['gift'],
            'inventory_id'      => (string) $data['inventory_id'],
        ];
    }
}
