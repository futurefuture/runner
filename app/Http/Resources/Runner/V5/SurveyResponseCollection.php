<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\SurveyResponse as SurveyResponseResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SurveyResponseCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => SurveyResponseResource::collection($this->collection),
        ];
    }
}
