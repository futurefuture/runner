<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Inventory as InventoryResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class InventoriesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => InventoryResource::collection($this->collection),
        ];
    }
}
