<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Review as ReviewResource;
use App\Http\Resources\Runner\V5\Store as StoreResource;
use App\Http\Resources\Runner\V5\Incentive as Incentive;
use App\Http\Resources\Runner\V5\Inventory as Inventory;
use App\Http\Resources\Runner\V5\Partner as Partner;
use App\Http\Resources\Runner\Ad as AdResource;
use App\Tag;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $images = [];

        array_push($images, [
            'image' => $this->image,
            'index' => 0,
        ]);

        if ($this->productImages) {
            foreach ($this->productImages as $i) {
                array_push($images, [
                    'image' => $i->image,
                    'index' => $i->index,
                ]);
            }
        }

        $requestParams = $request->request->all();
        $ads           = $this
            ->ads()
            ->active()
            ->where('ad_type_id', 1)
            ->get();
        $relatedAds    = [];

        if (array_key_exists('filter', $requestParams)) {
            $filterInfo = $request->request->all()['filter'];

            if (array_key_exists('tags', $filterInfo)) {
                $relatedTag = $filterInfo['tags'];

                foreach ($ads as $ad) {
                    if (Tag::find($ad->tag_id)->slug == $relatedTag) {
                        array_push($relatedAds, $ad);
                    }
                }
            }
        } else {
            // show all the products ads even if not related (will only appear if not on the explore page)?
            $relatedAds = $ads;
            //  if not above we can just leave the $relatedAds empty, thus returning no ads.
            // $relatedAds = [];
        }

        return [
            'type'                      => 'products',
            'id'                        => (string) $this->id,
            'sku'                       => $this->sku,
            'upc'                       => $this->upc,
            'is_active'                 => $this->is_active,
            'title'                     => $this->title,
            'long_description'          => $this->long_description,
            'short_description'         => $this->short_description,
            'retail_price'              => $this->retail_price,
            'wholesale_price'           => $this->wholesale_price,
            'limited_time_offer_price'  => $this->limited_time_offer_price,
            'limited_time_offer_savings' => $this->limitedTimeOfferSavings,
            'markup_percentage'         => $this->markup_percentage,
            'markup'                    => $this->markup,
            'runner_price'              => $this->runner_price,
            'images'                    => $images,
            'image_thumbnail'           => $this->image_thumbnail,
            'packaging'                 => $this->packaging,
            'alcohol_content'           => $this->alcohol_content,
            'sugar_content'             => $this->sugar_content * 100,
            'slug'                      => $this->slug,
            'category_id'               => (string) $this->category_id,
            'style'                     => $this->style,
            'producing_country'         => $this->producing_country,
            'producer'                  => $this->producer,
            'reward_points'             => $this->reward_points,
            'quantity'                  => $this->quantity ?? null,
            'delivery_window'           => '1-2 Hours',
            'case_deal'                 => $this->case_deal,
            'incentives'                => Incentive::collection($this->incentives->where('is_active', 1)->where('incentive_id', 3)),
            'average_rating'            => $this->average_rating,
            'favourite_id'              => (string) $this->favouriteId ?? null,
            'reviews_count'             => $this->reviews_count,
            'reviews'                   => $this->when($request->input('include') == 'reviews', ReviewResource::collection($this->reviews)),
            'inventories'               => Inventory::collection($this->whenLoaded('inventories')),
            'store'                     => new StoreResource($this->whenLoaded('store')),
            'is_card'                   => $this->category_id == 34 ? true : false,
            'partners'                  => Partner::collection($this->partners),
            'ads'                       => $relatedAds,
        ];
    }
}
