<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Survey as SurveyResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SurveyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => SurveyResource::collection($this->collection),
        ];
    }
}
