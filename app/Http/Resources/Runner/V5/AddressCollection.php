<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Address as AddressResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AddressCollection extends ResourceCollection
{
  /**
   * Transform the resource collection into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'data' => AddressResource::collection($this->collection),
    ];
  }
}
