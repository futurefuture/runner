<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\Refund as RefundResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RefundCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => RefundResource::collection($this->collection),
        ];
    }
}
