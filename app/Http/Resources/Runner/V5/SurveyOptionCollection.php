<?php

namespace App\Http\Resources\Runner\V5;

use App\Http\Resources\Runner\V5\SurveyOption as SurveyOptionResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SurveyOptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => SurveyOptionResource::collection($this->collection),
        ];
    }
}
