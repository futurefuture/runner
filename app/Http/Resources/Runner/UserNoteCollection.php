<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\UserNote as UserNoteResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserNoteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => UserNoteResource::collection($this->collection),
        ];
    }
}
