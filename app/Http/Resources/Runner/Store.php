<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'stores',
            'id'         => (string) $this->id,
            'attributes' => [
                'title'        => $this->title,
                'layout'       => $this->layout,
                'notification' => $this->notification ?? null,
                // 'inventory'        => $this->territory,
                'index'   => $this->index,
                'options' => [
                    'towerImage'     => $this->options['towerImage'] ?? null,
                    'blockImage'     => $this->options['blockImage'] ?? null,
                    'storeLogo'      => $this->options['storeLogo'] ?? null,
                    'navigationLogo' => $this->options['navigationLogo'] ?? null,
                    'bannerLogo'     => $this->options['bannerLogo'] ?? null,
                    'primaryColor'   => $this->options['primaryColor'] ?? null,
                    'secondaryColor' => $this->options['secondaryColor'] ?? null,
                    'hours'          => $this->options['hours'] ?? null,
                    'storeHighlight' => [
                        'icon' => $this->options['storeHighlight']['icon'] ?? null,
                        'text' => $this->options['storeHighlight']['text'] ?? null,
                    ],
                    'bannerImage'      => $this->options['bannerImage'] ?? null,
                    'nextDeliveryTime' => $this->nextDeliveryTime(),
                    'tagLine'          => $this->options['tagLine'] ?? null,
                    'aboutText'        => $this->options['aboutText'] ?? null,
                    'exploreBlurb'     => $this->options['exploreBlurb'] ?? null,
                    'categories'       => CategoryResource::collection($this->categories),
                    'giftOptions'      => isset($this['options']['giftOptions']) ? $this['options']['giftOptions'] : null,
                    'invertedLogo'     => $this->options['invertedLogo'] ?? false,
                ],
                'cartExtras'       => $this->cart_extras,
                'isForRecipient'   => $this->is_for_recipient,
                'nextDeliveryTime' => $this->nextDeliveryTime(),
                'deliveryFee'      => $this->deliveryFee(),
            ],
            'links' => [
                'self' => url('/v4/runner/stores/' . $this->id),
            ],
        ];
    }
}
