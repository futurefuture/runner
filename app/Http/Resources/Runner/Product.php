<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Review as ReviewResource;
use App\Http\Resources\Runner\Store as StoreResource;
use App\Http\Resources\Runner\Ad as AdResource;
use App\Tag;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $images = [];

        array_push($images, [
            'image' => $this->image,
            'index' => 0,
        ]);

        if ($this->productImages) {
            foreach ($this->productImages as $i) {
                array_push($images, [
                    'image' => $i->image,
                    'index' => $i->index,
                ]);
            }
        }

        $requestParams = $request->request->all();
        $relatedAds    = [];

        if (array_key_exists('filter', $requestParams)) {
            $filterInfo = $request->request->all()['filter'];

            if (array_key_exists('tags', $filterInfo)) {
                $relatedTag = $filterInfo['tags'];

                foreach ($this->ads as $ad) {
                    if (Tag::find($ad->tag_id)->slug == $relatedTag) {
                        array_push($relatedAds, $ad);
                    }
                }
            }
        } else {
            // show all the products ads even if not related (will only appear if not on the explore page)?
            $relatedAds = $this->ads;
            //  if not above we can just leave the $relatedAds empty, thus returning no ads.
            // $relatedAds = [];
        }

        // if ($this->limitedTimeOfferIncentives) {
        //     $limitedTimeOfferPrice = $this->runner_price - $this->limitedTimeOfferIncentives()->first()->savings;
        // } else {
        //     $limitedTimeOfferPrice = 0;
        // }

        return [
            'type'       => 'products',
            'id'         => (string) $this->id,
            'attributes' => [
                'sku'                     => $this->sku,
                'upc'                     => $this->upc,
                'isActive'                => $this->is_active,
                'title'                   => $this->title,
                'longDescription'         => $this->long_description,
                'shortDescription'        => $this->short_description,
                'retailPrice'             => $this->retail_price,
                'wholesalePrice'          => $this->wholesale_price,
                'limitedTimeOfferPrice'   => $this->limited_time_offer_price,
                'limitedTimeOfferSavings' => $this->limited_time_offer_savings,
                'markupPercentage'        => $this->markup_percentage,
                'markup'                  => $this->markup,
                'runnerPrice'             => $this->runner_price,
                'images'                  => $images,
                'imageThumbnail'          => $this->image_thumbnail,
                'packaging'               => $this->packaging,
                'alcoholContent'          => $this->alcohol_content,
                'sugarContent'            => $this->sugar_content * 100,
                'slug'                    => $this->slug,
                'categoryId'              => (string) $this->category_id,
                'style'                   => $this->style,
                'producingCountry'        => $this->producing_country,
                'producer'                => $this->producer,
                'rewardPoints'            => $this->reward_points,
                'quantity'                => $this->quantity ?? null,
                'deliveryWindow'          => '1-2 Hours',
                'caseDeal'                => $this->case_deal,
                'incentives'              => Incentive::collection($this->incentives->where('is_active', 1)->where('incentive_id', 3)),
                'averageRating'           => $this->average_rating,
                'favouriteId'             => (string) $this->favouriteId ?? null,
                'reviewsCount'            => $this->reviews_count,
                'reviews'                 => $this->when($request->input('include') == 'reviews', ReviewResource::collection($this->reviews)),
                'inventories'             => Inventory::collection($this->whenLoaded('inventories')),
                'store'                   => new StoreResource($this->whenLoaded('store')),
                'isCard'                  => $this->category_id == 34 ? true : false,
                'partners'                => Partner::collection($this->partners),
                'ads'                     => $relatedAds,
            ],
        ];
    }
}
