<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class SurveyResponse extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'survey responses',
            'id'         => (string) $this->id,
            'attributes' => [
                'surveyId'       => (string) $this->survey_id,
                'surveyOptionId' => (string) $this->survey_option_id,
                'userId'         => (string) $this->user_id,
                'createdAt'      => $this->created_at,
                'updatedAt'      => $this->updated_at,
            ],
        ];
    }
}
