<?php

namespace App\Http\Resources\Runner;

use App\Store;
use Illuminate\Http\Resources\Json\Resource;

class Favourite extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $store = Store::find($this->store_id);

        return [
            'type'       => 'favourites',
            'id'         => (string) $this->id,
            'attributes' => [
                'userId'    => (string) $this->user_id,
                'productId' => (string) $this->product_id,
                'store'     => $store,
            ],
            'links' => [
                'self' => url('/runner/v4' . $this->user_id . '/addresses/' . $this->id),
            ],
        ];
    }
}
