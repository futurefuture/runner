<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\Favourite as FavouriteResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FavouriteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
              'data' => FavouriteResource::collection($this->collection),
        ];
    }
}
