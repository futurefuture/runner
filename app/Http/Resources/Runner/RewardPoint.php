<?php

namespace App\Http\Resources\Runner;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class RewardPoint extends JsonResource
{
    public function __construct($resource)
    {
        static::$wrap = null;
        $this->resource = $resource;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'reward points',
            'id'            => (string) $this->id,
            'attributes'    => [
                'userId'    => (string) $this->user_id,
                'value'     => $this->value,
                'type'      => $this->type,
                'details'   => $this->details,
                'createdAt' => Carbon::parse($this->created_at)->toDateTimeString(),
                'updatedAt' => Carbon::parse($this->created_at)->toDateTimeString(),
            ],
        ];
    }
}
