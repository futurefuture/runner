<?php

namespace App\Http\Resources\Runner;

use Illuminate\Http\Resources\Json\Resource;

class SurveyOption extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'survey options',
            'id'         => (string) $this->id,
            'attributes' => [
                'surveyId'  => (string) $this->survey_id,
                'title'     => $this->title,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
        ];
    }
}
