<?php

namespace App\Http\Resources\Runner;

use App\Http\Resources\Runner\RewardPoint as RewardPointResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RewardPointCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => RewardPointResource::collection($this->collection),
        ];
    }
}
