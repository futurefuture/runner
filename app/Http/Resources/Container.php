<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Container extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'containers',
            'id'         => (string) $this->id,
            'attributes' => [
                'courierId' => $this->courier_id,
                'tasks'     => json_decode($this->tasks),
            ],
        ];
    }
}
