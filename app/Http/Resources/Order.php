<?php

namespace App\Http\Resources;

use App\Gift;
use App\Invitation;
use DB;
use Illuminate\Http\Resources\Json\Resource;

class Order extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $content = json_decode($this->content);

        $items = gettype($content) !== 'array' ? $content->items : $content;

        foreach ($items as $key => $item) {
            $item->type  = isset($item->type) ? $item->type : 'product';
            $item->image = isset($item->thumbnail) ? $item->thumbnail : '';
        }

        $invite = Invitation::where('invited_user_id', $this->user_id)->first();

        $coupon = DB::table('coupon_user')->where('order_id', $this->id)->first();

        if ($coupon) {
            $discountType = 'Coupon';
        } elseif ($this->coupon_id === null && $this->first_time === 1 && $this->discount && $invite) {
            $discountType = 'Referred By ' . $invite->email;
        } elseif ($this->coupon_id === null && $this->first_time === 0 && $this->discount) {
            $discountType = 'Reward Points';
        } else {
            $discountType = null;
        }

        $data = [
            'id'                => $this->id,
            'user_id'           => $this->user_id,
            'status'            => $this->status,
            'items'             => $items,
            'card_last_four'    => $this->card_last_four,
            'runner_1'          => $this->runner_1,
            'runner_2'          => $this->runner_2,
            'device'            => $this->device,
            'customer'          => json_decode($this->customer),
            'address'           => json_decode($this->address),
            'unit_number'       => $this->unit_number,
            'coupon'            => $this->coupon_id,
            'instructions'      => $this->instructions,
            'subtotal'          => $this->subtotal / 100,
            'service_fee'       => $this->service_fee / 100,
            'discount'          => $this->discount / 100,
            'discountType'      => $discountType,
            'delivery'          => $this->delivery / 100,
            'tip'               => $this->tip / 100,
            'display_tax_total' => $this->display_tax_total / 100,
            'total'             => $this->total / 100,
            'signature'         => $this->signature,
            'schedule_time'     => $this->schedule_time,
            'delivered_at'      => $this->delivered_at,
            'created_at'        => $this->created_at->format('M dS, Y h:i A'),
            'gift'              => null,
        ];

        $gift = Gift::where('order_id', $this->id)->first();

        if ($gift) {
            $formattedGift                                = [];
            $formattedGift['address']['formattedAddress'] = json_decode($gift->address)->address;
            $formattedGift['firstName']                   = $gift->first_name;
            $formattedGift['lastName']                    = $gift->last_name;
            $formattedGift['email']                       = $gift->email;
            $formattedGift['phoneNumber']                 = $gift->phone_number;
            $formattedGift['personalMessage']             = $gift->message;
            $data['gift']                                 = $formattedGift;
            $data['serviceFee']                           = 500;
        }

        return $data;
    }
}
