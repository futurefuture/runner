<?php

namespace App\Http\Resources;

use App\RewardPoint;
use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $rewardPoints = RewardPoint::where('user_id', $this->id)->get();
        $rewardPointsBalance = 0;

        foreach ($rewardPoints as $rewardPoint) {
            $rewardPointsBalance += $rewardPoint->value;
        }

        return [
            'id'                   => $this->id,
            'first_name'           => $this->first_name,
            'last_name'            => $this->last_name,
            'date_of_birth'        => $this->date_of_birth,
            'phone_number'         => $this->phone_number,
            'email'                => $this->email,
            'invite_code'          => $this->invite_code,
            'address'              => json_decode(optional($this->addresses()->where('selected', 1)->first())->address),
            'unit'                 => optional($this->addresses()->where('selected', 1)->first())->unit,
            'instructions'         => optional($this->addresses()->where('selected', 1)->first())->instructions,
            'rewardPoints'         => $rewardPointsBalance,
            'loyaltyPoints'        => $rewardPointsBalance,
            'promo_notification'   => $this->promo_notification,
            'order_notification'   => $this->order_notification,
        ];
    }
}
