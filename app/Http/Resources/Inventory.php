<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Inventory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'inventories',
            'id'         => (string) $this->id,
            'attributes' => [
                'isActive'      => $this->is_active,
                'title'         => $this->title,
                'hours'         => $this->options['hours'],
                'locationId'    => $this->location_id,
                'phoneNumber'   => $this->options['phoneNumber'],
                'address'       => $this->options['address'],
            ],
            'links' => [
                'self' => url('/v3/runner/inventories/'.$this->id),
            ],
        ];
    }
}
