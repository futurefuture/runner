<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    // ad type 1 product carousel
    // ad type 2 hero carousel

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content'  => 'array',
        'products' => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_type_id',
        'partner_id',
        'campaign_id',
        'ad_title',
        'content',
        'products',
        'product_id',
        'category_id',
        'tag_id',
        'carousel_id',
        'carousel_component_id',
        'bid_type',
        'bid_amount',
        'budget',
        'cost',
        'state',
        'index',
        'start_date',
        'end_date'
    ];

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function carousel()
    {
        return $this->belongsTo(Carousel::class);
    }

    public function carouselComponent()
    {
        return $this->belongsTo(CarouselComponent::class);
    }

    public function scopeActive($query)
    {
        $now = Carbon::now();

        return $query->where('start_date', '<=', $now)->where('end_date', '>', $now);
    }
}
