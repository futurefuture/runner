<?php

namespace App;

use App\Product;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'user_id',
        'product_id',
        'title',
        'quantity',
        'retail_price',
        'runner_price',
        'packaging',
        'producer',
        'category_id',
        'created_at',
        'updated_at',
    ];

    public function order()
    {
        return $this->belongsTo(\App\Order::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
