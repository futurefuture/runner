<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class InventoryProductNew extends Model
{
    protected $table = 'inventory_product_new';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inventory_id',
        'product_id',
        'quantity',
    ];
}
