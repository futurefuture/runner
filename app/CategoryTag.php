<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_tag';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_tag',
        'tag_id',
    ];
}
