<?php

namespace App;

use App\Store;
use App\Component;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Layout extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'layouts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'type',
    ];

    public function components()
    {
        return $this->belongsToMany(Component::class);
    }

    public function layoutStoreTerritories()
    {
        return $this->hasMany(LayoutStoreTerritory::class);
    }
}
