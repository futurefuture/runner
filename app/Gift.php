<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gift extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gifts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'first_name',
        'last_name',
        'address',
        'unit_number',
        'is_19',
        'email',
        'phone_number',
        'message',
        'instructions',
        'gift_options',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
