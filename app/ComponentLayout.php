<?php

namespace App;

use App\Component;
use App\Layout;
use Illuminate\Database\Eloquent\Model;

class ComponentLayout extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'component_layout';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'component_id',
        'layout_id',
        'index',
    ];

    public function component()
    {
        return $this->belongsTo(Component::class);
    }

    public function layout()
    {
        return $this->belongsTo(Layout::class);
    }
}
