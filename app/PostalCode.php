<?php

namespace App;

use App\Inventory;
use App\Store;
use App\Territory;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostalCode extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $primaryKey = 'postal_code';

    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'postal_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code',
    ];

    public function stores()
    {
        return $this->belongsToMany(Store::class, 'postal_code_store', 'postal_code', 'store_id');
    }

    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'inventory_postal_code', 'postal_code', 'inventory_id');
    }

    public function territory()
    {
        return $this->belongsTo(Territory::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
