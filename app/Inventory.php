<?php

namespace App;

use App\PostalCode;
use App\Product;
use App\Store;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'vendor',
        'store_id',
        'options',
        'is_active',
        'address_id',
    ];

    /**
     * The products that belong to the inventory.
     *
     * @return void
     */
    public function productsInStock()
    {
        return $this->belongsToMany(Product::class, 'inventory_product', 'inventory_id', 'product_id')
            ->withPivot('quantity')
            ->wherePivot('quantity', '>', 0);
    }

    /**
     * The products that belong to the inventory.
     *
     * @return void
     */
    public function productsAll()
    {
        return $this
            ->belongsToMany(Product::class, 'inventory_product', 'inventory_id', 'product_id')
            ->where('products.is_active', 1)
            ->withPivot('quantity');
    }

    /**
     * The product that belong to the inventory.
     *
     * @return void
     */
    public function product($productId)
    {
        return $this
            ->belongsToMany(Product::class, 'inventory_product', 'inventory_id', 'product_id')
            ->where('products.id', $productId)
            ->withPivot('quantity');
    }

    /**
     * Get the store that owns the inventory.
     *
     * @return void
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function postalCodes()
    {
        return $this->belongsToMany(PostalCode::class, 'inventory_postal_code', 'inventory_id', 'postal_code');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
