<?php

namespace App\Console;

use App\Jobs\GetActualCogs;
use App\Jobs\CachePartnerResults;
use App\Jobs\RushLateNightToRush;
use App\Jobs\RushToRushLateNight;
use App\Jobs\ReindexAllAdDependencies;
use App\Jobs\GetEstimatedDailyCourierSpend;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // send email with details from previous day
        $schedule->command('runner:daily-status')
            ->dailyAt('11:00')
            ->onOneServer();

        // another runner report
        $schedule->command('runner:report')
            ->dailyAt('08:30')
            ->onOneServer();

        // excel doc export with product tags and categories
        $schedule->command('runner:product-category-tag-details-excel')
            ->dailyAt('06:00')
            ->onOneServer();

        // truncate runners' locations table
        $schedule->command('runner:truncate-runner-locations')
            ->dailyAt('23:59')
            ->onOneServer()
            ->emailOutputTo('admin@getrunner.io');

        // run dispatch user with no puchase email
        // killed for time being
        // $schedule->command('runner:deuwnp')
        //     ->everyMinute()
        //     ->onOneServer();

        // run dispatch user with abandonded cart email
        // killed for time being
        // $schedule->command('runner:deuwac')
        //     ->everyMinute()
        //     ->onOneServer();

        // change old product incentives to inactive
        $schedule->command('runner:update-product-incentives')
            ->dailyAt('06:00')
            ->onOneServer();

        // update out of date coupons to inactive
        $schedule->command('runner:update-coupons-to-inactive')
            ->dailyAt('06:00')
            ->onOneServer();

        // changes sheduled orders to appear in current state
        $schedule->command('order:publish_received')
            ->everyFiveMinutes()
            ->onOneServer();

        // update product average rating
        $schedule->command('runner:update-product-average-rating')
            ->dailyAt('04:00')
            ->onOneServer();

        // update product reviews count
        $schedule->command('runner:update-product-reviews-count')
            ->dailyAt('04:00')
            ->onOneServer();

        // $schedule->command('product:automation')
        //     ->dailyAt('04:00')
        //     ->onOneServer();

        // $schedule->command('product:added')
        //     ->dailyAt('08:00');

        // $schedule->command('inventory:product-automation')
        //     ->dailyAt('06:00')
        //     ->onOneServer();

        // $schedule->command('telescope:prune')->daily();

        $schedule->job(new GetEstimatedDailyCourierSpend)->daily()->at('08:00');
        $schedule->job(new GetActualCogs)->daily()->at('08:10');
        $schedule->job(new ReindexAllAdDependencies)->daily()->at('06:00');
        $schedule->job(new CachePartnerResults)->daily()->at('01:00');
        $schedule->job(new RushToRushLateNight)->daily()->at('09:20');
        $schedule->job(new RushLateNightToRush)->daily()->at('23:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
