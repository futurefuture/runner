<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class TruncateRunnerLocationsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:truncate-runner-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncates Runner\'s Location Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('runner_location')
            ->truncate();
    }
}
