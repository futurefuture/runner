<?php

namespace App\Console\Commands;

use App\Category;
use App\Product;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ProductsUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Trims package size.
     *
     * @param [type] $productSize
     * @param [type] $sellingPackage
     * @return void
     */
    private function filterPackage($productSize, $sellingPackage)
    {
        if (strpos($productSize, 'x')) {
            $productSize = str_replace(' x ', 'x', $productSize);
        }

        if (strpos($productSize, 'mL')) {
            $productSize = str_replace(' mL', 'mL', $productSize);
        }

        return $productSize . ' ' . $sellingPackage;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '2048M');

        $client               = new Client();
        $response             = $client->request('GET', 'https://api.lcbo.com/v7/products');
        $formattedResponse    = json_decode($response->getBody()->getContents());
        $totalOffsetInteger   = ($formattedResponse->resultCount / 100) + 1;
        $totalOffsetRemaining = $formattedResponse->resultCount % 100;

        $this->info('getting products start');
        $productsBar = $this->output->createProgressBar($formattedResponse->resultCount);

        for ($i = 0; $i < $totalOffsetInteger + 1; $i++) {
            $response          = $client->request('GET', 'https://api.lcbo.com/v7/products/?numProductsPerPage=100&paginationOffset=' . $i);
            $formattedResponse = json_decode($response->getBody()->getContents());
            $products          = $formattedResponse->products;

            foreach ($products as $product) {
                if ($product->availability === 'ONLINE_ONLY') {
                    continue;
                }

                if ($product->itemNumber === '666032') {
                    echo 'got it';
                }

                // replace http to https in image links
                // $product->imageUrl      = str_replace('http://', 'https://', $product->imageUrl);
                // $product->imageUrlThumb = str_replace('http://', 'https://', $product->imageUrlThumb);

                // clean up retail price
                $retailPrice = ! $product->limitedTimeOffer ? (int) (str_replace('$ ', '', $product->price) * 100) : (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100);

                $runnerProduct = Product::withTrashed()->where('sku', $product->itemNumber)->first();

                if ($product->subSubCategoryName == '') {
                    $category = Category::where('title', $product->subSubCategoryName)->first();
                }

                if (! $runnerProduct) {
                    $newProduct = Product::create([
                            'sku'               => $product->itemNumber,
                            'upc'               => (int) $product->upcNumber,
                            'title'             => $product->itemName,
                            'long_description'  => $product->tastingNotes,
                            'retail_price'      => $retailPrice,
                            'markup_percentage' => 0.12,
                            'markup'            => (int) $retailPrice * 0.12,
                            'runner_price'      => (int) $retailPrice * 1.12,
                            'image'             => $product->imageUrl,
                            'image_thumbnail'   => $product->imageUrlThumb,
                            'packaging'         => $this->filterPackage($product->productSize, $product->sellingPackage),
                            'alcohol_content'   => $product->alcoholPercentage * 100,
                            'sugar_content'     => (int) $product->sugarContent > 0 ? (int) $product->sugarContent : null,
                            'slug'              => Str::slug($product->itemName . '-' . $this->filterPackage($product->productSize, $product->sellingPackage)),
                            'producing_country' => $product->producingCountry,
                            'producer'          => $product->producer,
                            'category_id'       => isset($category->id) ? $category->id : 1,
                        ]);
                } else {
                    $runnerProduct->retail_price = $retailPrice;
                    $runnerProduct->markup       = (int) $retailPrice * 0.12;
                    $runnerProduct->runner_price = (int) $retailPrice * 1.12;

                    $runnerProduct->save();

                    $alreadyExistingIncentive = DB::table('incentive_product')
                                                        ->where('product_id', $runnerProduct->id)
                                                        ->where('start_date', Carbon::now()->toDateString())
                                                        ->first();
                }

                // add limited time offer to incentives table
                if ($product->limitedTimeOffer) {
                    $savings = (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100) - (int) (str_replace('$ ', '', $product->price) * 100);

                    if (! $alreadyExistingIncentive && $runnerProduct) {
                        DB::table('incentive_product')->insert([
                                'incentive_id' => 1,
                                'product_id'   => $runnerProduct->id,
                                'start_date'   => Carbon::now()->toDateString(),
                                'end_date'     => Carbon::parse($product->ltoExpiration)->toDateString(),
                                'savings'      => $savings,
                                'is_active'    => 1,
                            ]);
                    }
                }

                $productsBar->advance();
            }
        }

        $productsBar->finish();
    }
}
