<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UpdateStripeId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-jarek-stripe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Jarek\'s Stripe Id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stripeId         = 'cus_EBZBXikaRda3j9';
        $jarek            = User::find(131);
        $jarek->stripe_id = $stripeId;

        $jarek->save();
    }
}
