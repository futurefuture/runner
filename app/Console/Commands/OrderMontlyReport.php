<?php

namespace App\Console\Commands;

use App\Address;
use App\Exports\OrderSalesReportExport;
use App\Mail\OrderSalesMail;
use App\Order;
use Carbon\Carbon;
use Excel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class OrderMontlyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:monthly-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to Dispaly order details with postal code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = Carbon::now()
            ->subDays(30)
            ->toDateString();
        $endDate = Carbon::now()
            ->subDays(1)
            ->toDateString();

        $orders = Order::whereBetween('created_at', [$startDate, $endDate])
            ->select([
                'id',
                'user_id',
                'charge',
                'runner_1',
                'subtotal',
                'cogs',
                'discount',
                'delivery',
                'markup',
                'display_tax_total',
                'tip',
                'total',
                'stripe_fee',
                'service_fee',
                'created_at',
                'delivered_at',
                'updated_at',
                'address_id',
            ])
            ->get();

        $orders->map(function ($order) {
            $order->subtotal = $order->subtotal / 100;
            $order->cogs = $order->cogs / 100;
            $order->discount = $order->discount / 100;
            $order->delivery = $order->delivery / 100;
            $order->markup = $order->markup / 100;
            $order->display_tax_total = $order->display_tax_total / 100;
            $order->tip = $order->tip / 100;
            $order->total = $order->total / 100;
            $order->stripe_fee = $order->stripe_fee / 100;
            $order->service_fee = $order->service_fee / 100;
            $postal_code = Address::withTrashed()->where('id', $order->address_id)->first();
            $firstThree = $postal_code ? substr($postal_code->postal_code, 0, 3) : null;
            $order->address_id = $firstThree;

            return $order;
        });

        $file = Excel::download(new OrderSalesReportExport($orders), 'order_sales_export_' . $startDate . '-' . $endDate . '.xlsx')
            ->getFile();

        $emails = [
            'ops@getrunner.io'
        ];

        Mail::send(new OrderSalesMail($startDate, $endDate, $file, $emails));
    }
}
