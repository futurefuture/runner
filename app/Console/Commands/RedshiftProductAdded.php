<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class RedshiftProductAdded extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:product_added';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query       = 'SELECT product_id, uuid FROM php_production.product_removed WHERE partner_ids IS null ORDERY BY uuid ASC';
        $products    = DB::connection('pgsql')
            ->select($query);
        $progressBar = $this->output->createProgressBar(count($products));

        foreach ($products as $product) {
            $progressBar->advance();
            $partnerIds = DB::table('partner_product')
                ->where('product_id', $product->product_id)
                ->where('product_id', '<>', 0)
                ->pluck('partner_id')
                ->toArray();

            if (! $partnerIds) {
                $update      = 'update php_production.product_removed set partner_ids = "[]" where uuid =' . $product->uuid;
                $orderUpdate = DB::connection('pgsql')
                    ->update($update);
            } else {
                $ps          = implode(',', $partnerIds);
                $update      = 'update php_production.product_removed set partner_ids =' . "'[" . $ps . "]'  where uuid =" . $product->uuid;
                $orderUpdate = DB::connection('pgsql')
                    ->update($update);
            }
        }

        $progressBar->finish();
    }
}
