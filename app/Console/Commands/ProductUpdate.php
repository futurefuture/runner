<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use App\Category;
use App\Product;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use App\Utilities\Jobs\ProductUpdateAutomation as Job;
use Exception;
use Excel;
use App\Exports\ProductUpdateExport;
use App\Mail\ProductUpdateMail;
use Illuminate\Support\Facades\Mail;
use Laravel\Scout\Searchable;

class ProductUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:automation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update products table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Trims package size.
     *
     * @param [type] $productSize
     * @param [type] $sellingPackage
     * @return void
     */
    private function filterPackage($productSize, $sellingPackage)
    {
        if (strpos($productSize, 'x')) {
            $productSize = str_replace(' x ', 'x', $productSize);
        }

        if (strpos($productSize, 'mL')) {
            $productSize = str_replace(' mL', 'mL', $productSize);
        }

        return $productSize . ' ' . $sellingPackage;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $now    = Carbon::now();

        $productUpdateFormatted = [];

        $product_import_complete = DB::select("select payload from failed_jobs where connection ='product_import_complete' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');
        if ($product_import_complete) {
            $this->info('Product import is completed for ' . $now);
        } else {
            try {
                try {
                    $response = $client->request('GET', 'https://api.lcbo.com/v6/products');
                } catch (Exception $e) {
                    DB::table('failed_jobs')->insert([
                        'connection' => 'product_import',
                        'queue'      => 'default',
                        'payload'    => json_encode((object) [
                            'paginationOffset' => 0,
                        ]),
                        'exception' => $e,
                        'failed_at' => $now
                    ]);

                    return $e;
                    DB::statement("Delete from  failed_jobs where connection ='product_import' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');
                }

                $formattedResponse  = json_decode($response->getBody()->getContents());
                $totalOffsetInteger = ($formattedResponse->resultCount / 100) + 1;

                $totalOffsetRemaining = $formattedResponse->resultCount % 100;

                $productsBar = $this->output->createProgressBar($formattedResponse->resultCount);

                if (! Schema::hasTable('products_new')) {
                    DB::statement('CREATE TABLE products_new as select * from products');
                    DB::statement('ALTER TABLE products_new CHANGE id id INT(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY');
                }

                if (! Schema::hasTable('incentive_product_new')) {
                    DB::statement('CREATE TABLE incentive_product_new as select * from incentive_product');
                    DB::statement('ALTER TABLE incentive_product_new CHANGE id  id INT(10)AUTO_INCREMENT PRIMARY KEY');
                }

                $paginationOffset = DB::select("select payload from failed_jobs where connection ='product_import' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');

                if ($paginationOffset) {
                    $i = json_decode($paginationOffset[0]->payload)->paginationOffset;
                    DB::statement("Delete from  failed_jobs where connection ='product_import' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');
                } else {
                    $i = 0;
                }

                for ($i; $i < $totalOffsetInteger + 1; $i++) {
                    try {
                        $response = $client->request('GET', 'https://api.lcbo.com/v6/products/?numProductsPerPage=1000&paginationOffset=' . $i);
                    } catch (Exception $e) {
                        DB::table('failed_jobs')->insert([
                            'connection' => 'product_import',
                            'queue'      => 'default',
                            'payload'    => json_encode((object) [
                                'paginationOffset' => $i,
                            ]),
                            'exception' => $e,
                            'failed_at' => $now
                        ]);

                        return $e;
                    }
                    $formattedResponse = json_decode($response->getBody()->getContents());
                    $products          = $formattedResponse->products;

                    foreach ($products as $product) {
                        if ($product->availability === 'ONLINE_ONLY') {
                            continue;
                        }

                        // clean up retail price
                        $retailPrice = ! $product->limitedTimeOffer ? (int) (str_replace('$ ', '', $product->price) * 100) : (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100);

                        if ($product->subSubCategoryName == '') {
                            $category = Category::where('title', $product->subSubCategoryName)->first();
                        }
                        // $productUpdate = new ProductUpdateAutomation();

                        $runnerProduct = Product::withTrashed()->where('sku', $product->itemNumber)->first();

                        $productUpdate = [];
                        if (! $runnerProduct) {
                            $product = DB::table('products_new')->insert([
                                'sku'               => $product->itemNumber,
                                'upc'               => (int) $product->upcNumber,
                                'title'             => $product->itemName,
                                'long_description'  => $product->tastingNotes,
                                'retail_price'      => $retailPrice,
                                'markup_percentage' => 0.12,
                                'markup'            => (int) $retailPrice * 0.12,
                                'runner_price'      => (int) $retailPrice * 1.12,
                                'image'             => $product->imageUrl,
                                'image_thumbnail'   => $product->imageUrlThumb,
                                'packaging'         => $this->filterPackage($product->productSize, $product->sellingPackage),
                                'alcohol_content'   => $product->alcoholPercentage * 100,
                                'sugar_content'     => (int) $product->sugarContent > 0 ? (int) $product->sugarContent : null,
                                'slug'              => Str::slug($product->itemName . '-' . $this->filterPackage($product->productSize, $product->sellingPackage)),
                                'producing_country' => $product->producingCountry,
                                'producer'          => $product->producer,
                                'category_id'       => isset($category->id) ? $category->id : 1,
                                'created_at'        => $now,
                                'updated_at'        => $now
                            ]);

                            // $productUpdate['sku']        = $product->itemNumber;
                            // $productUpdate['created_at'] = $now;

                            // array_push($productUpdateFormatted, $productUpdate);
                            $product->searchable();
                        } else {
                            DB::table('products_new')->where('id', '=', $runnerProduct->id)->update([
                                'retail_price' => $retailPrice,
                                'markup'       => (int) $retailPrice * 0.12,
                                'runner_price' => (int) $retailPrice * 1.12,
                                'updated_at'   => $now
                            ]);
                            // $runnerProduct->retail_price = $retailPrice;
                            // $runnerProduct->markup = (int) $retailPrice * 0.12;
                            // $runnerProduct->runner_price = (int) $retailPrice * 1.12;

                            // $runnerProduct->save();

                            $alreadyExistingIncentive = DB::table('incentive_product_new')
                                ->where('product_id', $runnerProduct->id)
                                ->where('start_date', Carbon::now()->toDateString())
                                ->first();
                        }

                        if ($product->limitedTimeOffer) {
                            $savings = (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100) - (int) (str_replace('$ ', '', $product->price) * 100);

                            if (! $alreadyExistingIncentive && $runnerProduct) {
                                DB::table('incentive_product_new')->insert([
                                    'incentive_id' => 1,
                                    'product_id'   => $runnerProduct->id,
                                    'start_date'   => Carbon::now()->toDateString(),
                                    'end_date'     => Carbon::parse($product->ltoExpiration)->toDateString(),
                                    'savings'      => $savings,
                                    'is_active'    => 1,
                                ]);
                            }
                        }
                        $productsBar->advance();
                    }
                }
            } catch (Exception $e) {
                DB::table('failed_jobs')->insert([
                    'connection' => 'product_import',
                    'queue'      => 'default',
                    'payload'    => json_encode((object) [
                        'paginationOffset' => $i,
                    ]),
                    'exception' => $e,
                    'failed_at' => $now
                ]);

                return $e;
            }

            $totalOldProduct = DB::select('select count(*) from products');
            $totalProduct    = DB::select('select count(*) from products_new');

            if ($totalOldProduct == $totalProduct || $totalProduct > $totalOldProduct) {
                if (! Schema::hasTable('product_old_table')) {
                    $renameProductsTable   = 'RENAME TABLE products TO product_old_table';
                    $products_table_rename = DB::statement($renameProductsTable);
                }

                if (Schema::hasTable('products_new')) {
                    $renameProductNewTable    = 'RENAME TABLE products_new to products';
                    $product_new_table_rename = DB::statement($renameProductNewTable);
                }

                DB::statement('ALTER TABLE category_product DROP FOREIGN KEY category_product_product_id_foreign');

                DB::statement('ALTER TABLE category_product ADD CONSTRAINT category_product_product_id_foreign
                FOREIGN KEY (product_id) REFERENCES products(id)');

                DB::statement('ALTER TABLE inventory_product DROP FOREIGN KEY inventory_product_product_id_foreign');

                DB::statement('set foreign_key_checks=0');

                DB::statement('ALTER TABLE inventory_product ADD CONSTRAINT inventory_product_product_id_foreign
                FOREIGN KEY (product_id) REFERENCES products(id)');

                DB::statement('ALTER TABLE product_tag DROP FOREIGN KEY product_tag_product_id_foreign');

                DB::statement('ALTER TABLE product_tag ADD CONSTRAINT product_tag_product_id_foreign
                FOREIGN KEY (product_id) REFERENCES products(id)');

                $dropProductOldTable = 'Drop table product_old_table';
                $dropTable           =  DB::statement($dropProductOldTable);

                $renameProductIncentive =  'RENAME TABLE incentive_product to incentive_product_old';
                $incentive_product_old  = DB::statement($renameProductIncentive);

                $renameProductIncentiveNew =  'RENAME TABLE incentive_product_new to incentive_product';
                $incentive_product_new     = DB::statement($renameProductIncentiveNew);

                DB::statement('Drop table incentive_product_old');
            }

            $productsBar->finish();
        }
    }
}
