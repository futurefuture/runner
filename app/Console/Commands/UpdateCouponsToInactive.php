<?php

namespace App\Console\Commands;

use App\Coupon;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateCouponsToInactive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-coupons-to-inactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check against date and changes coupon to inactive if after active_until date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coupons = Coupon::all();

        foreach ($coupons as $c) {
            $activeUntil = Carbon::parse($c->active_until);

            if ($activeUntil->isPast()) {
                $c->is_active = 0;
                $c->save();
            }
        }
    }
}
