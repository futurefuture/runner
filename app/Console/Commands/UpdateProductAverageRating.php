<?php

namespace App\Console\Commands;

use App\Product;
use App\Review;
use Illuminate\Console\Command;

class UpdateProductAverageRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-product-average-rating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates average rating column on products table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reviews = Review::all();
        $uniqueProducts = $reviews->unique('product_id');
        $progress = $this->output->createProgressBar(count($reviews));

        foreach ($uniqueProducts as $u) {
            $product = Product::find($u->product_id);

            if ($product) {
                $averageRating = $reviews->where('product_id', $product->id)->avg('value');
                $product->average_rating = $averageRating * 100;
                $product->save();
                $progress->advance();
            }
        }

        $progress->finish();
    }
}
