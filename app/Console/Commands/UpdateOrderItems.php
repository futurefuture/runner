<?php

namespace App\Console\Commands;

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Console\Command;

class UpdateOrderItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:order_items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::WhereBetween('created_at', ['2019-07-01', '2019-08-1'])
            ->chunk(100, function ($orders) {
                $ordersBar = $this->output->createProgressBar(count($orders));

                foreach ($orders as $order) {
                    $ordersBar->advance();
                    $items = json_decode($order->content);

                    foreach ($items as $k => $item) {
                        $product = Product::withTrashed()
                        ->where('id', $item->id)
                        ->first();

                        if (property_exists($item, 'price')) {
                            $retail_price = ($item->price / 1.1) * 100;
                        } elseif (property_exists($item, 'real_price')) {
                            $retail_price = $item->real_price * 100;
                        } elseif (property_exists($item, 'true_price')) {
                            $retail_price = $item->true_price * 100;
                        } elseif (property_exists($item, 'retailPrice')) {
                            $retail_price = $item->retailPrice;
                        } else {
                            $retail_price = $product->retail_price;
                        }

                        if (property_exists($item, 'price')) {
                            $runner_price = $item->price * 100;
                        } elseif (property_exists($item, 'runner_price')) {
                            $runner_price = $item->runner_price * 100;
                        } elseif (property_exists($item, 'runnerPrice')) {
                            $runner_price = $item->runnerPrice;
                        } else {
                            $runner_price = null;
                        }

                        if (property_exists($item, 'name')) {
                            $title = $item->name;
                        } elseif (property_exists($item, 'title')) {
                            $title = $item->title;
                        } else {
                            $title = null;
                        }

                        if (property_exists($item, 'qty')) {
                            $quantity = $item->qty;
                        } elseif (property_exists($item, 'quantity')) {
                            $quantity = $item->quantity;
                        } else {
                            $quantity = null;
                        }

                        // skip printing to order items table if quantity is 0
                        if ((int) $quantity == 0) {
                            continue;
                        }

                        if (property_exists($item, 'package')) {
                            $packaging = $item->package;
                        } elseif (property_exists($item, 'packaging')) {
                            $packaging = $item->packaging;
                        } else {
                            $packaging = null;
                        }

                        $order_item = OrderItem::where('order_id', $order->id)->first();

                        if (! $product) {
                            continue;
                        } else {
                            if ($order_item) {
                                continue;
                            } else {
                                $orderItem = new OrderItem([
                                'order_id'      => $order->id,
                                'user_id'       => $order->user_id,
                                'product_id'    => $product->id,
                                'title'         => $title,
                                'quantity'      => (int) $quantity,
                                'retail_price'  => $retail_price,
                                'runner_price'  => $runner_price,
                                'packaging'     => $packaging,
                                'producer'      => $product->producer,
                                'category_id'   => $product->category_id,
                                'created_at'    => $order->created_at,
                                'updated_at'    => $order->created_at,
                            ]);

                                $orderItem->save();
                            }
                        }
                    }
                }

                $ordersBar->finish();
            });
    }
}
