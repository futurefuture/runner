<?php

namespace App\Console\Commands;

use App\Category;
use App\Product;
use App\Services\CategoryService;
use Illuminate\Console\Command;

class AddAllProductsToCategoryProductTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:add-products-to-pc-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $progress = $this->output->createProgressBar(Product::count());

        Product::chunk(1000, function ($products) use ($progress) {
            foreach ($products as $p) {
                $category = Category::find($p->category_id);
                $breadCrumbs = (new CategoryService())->getBreadCrumbs($category);
                $categoryIds = [];

                foreach ($breadCrumbs as $b) {
                    if ($b['id'] != null) {
                        array_push($categoryIds, $b['id']);
                    }
                }

                foreach ($categoryIds as $c) {
                    $p->categories()->attach($c);
                }

                $progress->advance();
            }
        });

        $progress->finish();
    }
}
