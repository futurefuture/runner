<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use Excel;
use App\Exports\OrderDiscrepanciesReportExport;
use App\Mail\OrderDiscrepanciesMail;
use Illuminate\Support\Facades\Mail;

class stripeDiscrepancies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:discrepancies {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to find discrepancies between order and order items table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $startDate = $arguments['startDate'];
        $endDate   = $arguments['endDate'];
        $orders    = Order::whereBetween('created_at', [$startDate, $endDate])
            ->where('status', '=', 3)
            ->select([
                'id',
                'user_id',
                'content',
                'charge',
                'runner_1',
                'subtotal',
                'cogs',
                'discount',
                'delivery',
                'markup',
                'display_tax_total',
                'tip',
                'total',
                'stripe_fee',
                'service_fee',
                'created_at',
                'delivered_at',
                'updated_at',
                'address_id',
            ])
            ->get();
        $orderDiscrepancy = [];

        foreach ($orders as $order) {
            $products       = json_decode($order->content);
            $orderTotal     = 0;
            $orderFormatted = [];

            foreach ($products as $product) {
                $sumProduct = isset($product->runnerPrice) ? $product->runnerPrice * $product->quantity : 0;
                $orderTotal = $orderTotal + $sumProduct;
            }

            if ($orderTotal != $order->subtotal) {
                $orderFormatted['order_id']        = $order->id;
                $orderFormatted['cogs']            = $order->cogs;
                $orderFormatted['contentTotal']    = $orderTotal;
                $orderFormatted['subtotal']        = $order->subtotal;
                $orderFormatted['created_at']      = $order->created_at;

                array_push($orderDiscrepancy, $orderFormatted);
            }
        }

        $file = Excel::download(new OrderDiscrepanciesReportExport($orderDiscrepancy), 'order_discrepancy_export_' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'jarek@getrunner.io'
        ];

        Mail::send(new OrderDiscrepanciesMail($startDate, $endDate, $file, $emails));
    }
}
