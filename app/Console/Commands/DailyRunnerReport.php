<?php

namespace App\Console\Commands;

use App\Exports\RunnerReportExport;
use App\Mail\RunnerReportMail;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DailyRunnerReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to tell no of orders and cogs by runner';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::now()
            ->subDays(1)
            ->toDateString();

        $query = 'SELECT SUM(o.cogs) / 100 AS cogs, COUNT(*) AS totalOrders, o.runner_1, u.first_name FROM orders as o INNER JOIN users AS u ON u.id = o.runner_1 WHERE o.status = 3 AND o.runner_1 IN(SELECT user_id FROM schedules WHERE DATE(start) = ' . " '" . $start . "' " . ' AND job_type IN(1, 2, 4, 3)) AND DATE(o.delivered_at)  = ' . " '" . $start . "' " . '  AND user_id NOT IN(131, 1600) GROUP BY o.runner_1, u.first_name ORDER BY totalOrders DESC';

        $res = DB::select($query);

        $file = Excel::download(new RunnerReportExport($res), 'runner-cogs-export-' . $start . '.xlsx')
            ->getFile();

        $emails = [
            'ops@getrunner.io'
        ];

        Mail::send(new RunnerReportMail($start, $file, $emails));
    }
}
