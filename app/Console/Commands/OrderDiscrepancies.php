<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\OrderItem;
use DB;
use Excel;
use App\Exports\OrderItemDiscrepanciesReportExport;
use App\Mail\OrderItemDiscrepanciesMail;
use Illuminate\Support\Facades\Mail;

class OrderDiscrepancies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:discrepancies {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to find discrepancies between order and order items table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $startDate = $arguments['startDate'];
        $endDate   = $arguments['endDate'];

        $orders = DB::select('SELECT o.id, JSON_LENGTH(o.content) AS items, COUNT(oi.id) AS orderItem FROM orders AS o LEFT JOIN order_items AS oi ON oi.order_id = o.id WHERE DATE(o.created_at) BETWEEN ' . "'" . $startDate . "'" . ' AND ' . "'" . $endDate . "'" . ' AND o.status = 3 GROUP BY o.id, items HAVING items != orderItem');

        $file = Excel::download(new OrderItemDiscrepanciesReportExport($orders), 'order_discrepancy_export_' . $startDate . '-' . $endDate . '.xlsx')
            ->getFile();

        $emails = [
            'jarek@getrunner.io'
        ];

        Mail::send(new OrderItemDiscrepanciesMail($startDate, $endDate, $file, $emails));
    }
}
