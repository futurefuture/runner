<?php

namespace App\Console\Commands;

use App\Mail\UserSignedUpWithNoPurchase;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DispatchEmailUserWithNoPurchase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:deuwnp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $thirteenMinutesAgo = Carbon::now()->subMinutes(13)->toDateTimeString();
        $twelveMinutesAgo   = Carbon::now()->subMinutes(12)->toDateTimeString();

        $recentUsers = User::whereBetween('created_at', [
            $thirteenMinutesAgo,
            $twelveMinutesAgo,
        ])->get();

        foreach ($recentUsers as $recentUser) {
            if ($recentUser->orders()->count() < 1) {
                Mail::to($recentUser->email)
                    ->bcc('service@getrunner.io')
                    ->queue(new UserSignedUpWithNoPurchase($recentUser));
            }
        }
    }
}
