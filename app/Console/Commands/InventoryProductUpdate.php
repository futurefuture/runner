<?php

namespace App\Console\Commands;

use App\Inventory;
use App\InventoryProduct;
use App\Product;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Console\Command;
use App\InventoryProductNew;

class InventoryProductUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:product-automation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update inventory_product table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $now    = Carbon::now();

        $inventory_import_complete = DB::select("select payload from failed_jobs where connection ='inventory_import_complete' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');

        if ($inventory_import_complete) {
            $this->info('Product import is completed for' . $now);
        } else {
            try {
                if (!Schema::hasTable('inventory_product_new')) {
                    DB::statement('CREATE TABLE inventory_product_new as select * from inventory_product');
                    DB::statement('ALTER TABLE inventory_product_new CHANGE id id INT(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY');
                }

                $lcboStores = Inventory::where('vendor', 'LCBO')
                    ->where('is_active', 1)
                    ->whereNotNull('location_id')
                    ->get();

                foreach ($lcboStores as $lcboStore) {
                    try {
                        $paginationOffset = DB::select("select payload from failed_jobs where connection ='inventory_import_failed' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');
                        if ($paginationOffset) {
                            $lcboStore->location_id = json_decode($paginationOffset[0]->payload)->paginationOffset;
                            DB::statement("Delete from  failed_jobs where connection ='product_import' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');
                        }
                        try {
                            $response = $client->request('GET', 'https://api.lcbo.com/store_inventory/?locationNumber=' . $lcboStore->location_id);
                        } catch (Exception $e) {
                            DB::table('failed_jobs')->insert([
                                'connection' => 'inventory_import_failed',
                                'queue'      => 'default',
                                'payload'    => json_encode((object) [
                                    'paginationOffset' => $lcboStore->location_id,
                                ]),
                                'exception' => $e,
                                'failed_at' => $now
                            ]);

                            return $e;
                        }
                    } catch (Exception $e) {
                        DB::table('failed_jobs')->insert([
                            'connection' => 'inventory_import_failed',
                            'queue'      => 'default',
                            'payload'    => json_encode((object) [
                                'paginationOffset' => $lcboStore->location_id,
                            ]),
                            'exception' => $e,
                            'failed_at' => $now
                        ]);

                        return $e;
                    }

                    $products = json_decode($response->getBody()->getContents())->products;

                    $progressBar = $this->output->createProgressBar(count($products));

                    foreach ($products as $product) {
                        $actualProduct = Product::where('sku', $product->product__id)
                            ->where('is_updatable', true)
                            ->first();

                        if ($actualProduct) {
                            // $inventoryProduct = InventoryProduct::where('product_id', '=', $actualProduct->id)->where('inventory_id', '=', $lcboStore->id)->first();

                            // if (!$inventoryProduct) {
                            //     DB::table('inventory_product_new')->insert([
                            //         'product_id'   => $actualProduct->id,
                            //         'inventory_id' => $lcboStore->id,
                            //         'quantity'     => $product->total_inv,
                            //         'created_at'   => $now,
                            //         'updated_at'   => $now
                            //     ]);
                            // } else {
                            //     DB::table('inventory_product_new')->where([['product_id', '=', $actualProduct->id], ['inventory_id', '=', $lcboStore->id]])->update([
                            //         'product_id'   => $actualProduct->id,
                            //         'inventory_id' => $lcboStore->id,
                            //         'quantity'     => $product->total_inv
                            //     ]);
                            // }

                            InventoryProductNew::updateOrCreate([
                                'product_id'   => $actualProduct->id,
                                'inventory_id' => $lcboStore->id,
                            ], [
                                'inventory_id' => $lcboStore->id,
                                'product_id'   => $actualProduct->id,
                                'quantity'     => $product->total_inv < 0 ? 0 : $product->total_inv,
                                'created_at'   => $now,
                                'updated_at'   => $now
                            ]);
                        }

                        $progressBar->advance();
                    }
                }

                DB::statement('RENAME TABLE inventory_product to inventory_product_old');

                DB::statement('RENAME TABLE inventory_product_new to inventory_product');

                DB::statement('Drop table inventory_product_old');

                // DB::statement("ALTER TABLE inventory_product ADD CONSTRAINT inventory_product_product_id_foreign
                // FOREIGN KEY (product_id) REFERENCES products(id)");

                // DB::statement("ALTER TABLE inventory_product ADD CONSTRAINT inventory_product_inventory_id_foreign
                // FOREIGN KEY (inventory_id) REFERENCES inventories(id)");

                DB::table('failed_jobs')->insert([
                    'connection' => 'inventory_product_update_complete',
                    'queue'      => 'default',
                    'payload'    => json_encode((object) [
                        'inventoryUpdate' => 'complete'
                    ]),
                    'exception'  => 'No Exception',
                    'failed_at'  => $now
                ]);
            } catch (Exception $e) { }
        }
    }
}
