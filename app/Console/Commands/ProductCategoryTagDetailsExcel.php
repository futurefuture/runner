<?php

namespace App\Console\Commands;

use Excel;
use App\Tag;
use App\Order;
use App\OrderItem;
use App\Product;
use App\ProductTag;
use App\Category;
use App\CategoryProduct;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProductCategoryTagDailyStatusMail;
use App\Exports\ProductCategoryTagDailyStatusExport;

class ProductCategoryTagDetailsExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:product-category-tag-details-excel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send admin an email for all the products information about the categories and tags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // PRODUCT - CATEGORY & TAGS
        $startDate  = Carbon::now()->subDays(2)->toDateString();
        $endDate    = Carbon::now()->subDays(1)->toDateString();
        $orderItems = OrderItem::whereBetween('created_at', [
            $startDate,
            $endDate
        ])->get()->groupBy('product_id')->toArray();

        $prodReturn = [];
        foreach ($orderItems as $item) {
            // CATEGORIES
            $catTitleArr = [];
            $catArr      = CategoryProduct::where('product_id', $item[0]['product_id'])
                ->pluck('category_id')
                ->toArray();

            foreach ($catArr as $catId) {
                $catTitle = Category::where('id', $catId)
                    ->value('slug');

                array_push($catTitleArr, $catTitle);
            }

            // TAGS
            $tagTitleArr = [];
            $countryTag = "";
            $varietalTag = "";

            $tagArr      = ProductTag::where('product_id', $item[0]['product_id'])
                ->pluck('tag_id')
                ->toArray();

            foreach ($tagArr as $tagId) {
                $tagTitle = Tag::where('id', $tagId)->where('type', '!=', 'country')->where('type', '!=', 'varietal')->value('slug');

                if (Tag::where('id', $tagId)->where('type', 'country')->first()) {
                    $countryTag = Tag::where('id', $tagId)->where('type', 'country')->value('slug');
                }

                if (Tag::where('id', $tagId)->where('type', 'varietal')->first()) {
                    $varietalTag = Tag::where('id', $tagId)->where('type', 'varietal')->value('slug');
                }

                array_push($tagTitleArr, $tagTitle);
            }

            array_push($prodReturn, (object) [
                'id'         => $item[0]['product_id'],
                'orderId'    => $item[0]['order_id'],
                'sku'        => Product::find($item[0]['product_id'])->sku,
                'slug'       => Product::find($item[0]['product_id'])->slug,
                'title'      => $item[0]['title'],
                'categories' => $catTitleArr,
                'tags'       => $tagTitleArr,
                'countryTag' => $countryTag,
                'varietalTag' => $varietalTag
            ]);
        }
        $file = Excel::download(new ProductCategoryTagDailyStatusExport($prodReturn), 'product_category_tag_daily_status' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'ops@getrunner.io',
            'ryan@ryankagan.ca'
        ];

        Mail::send(new ProductCategoryTagDailyStatusMail($startDate, $endDate, $file, $emails));

        $this->info('Product Category Tag Excel email sent');
    }
}
