<?php

namespace App\Console\Commands;

use Excel;
use App\Order;
use App\Schedule;
use Carbon\Carbon;
use App\Mail\DailyStatusSent;
use Illuminate\Console\Command;
use App\Exports\DailyStatusExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DailyStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:daily-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send admin an email for all the late orders today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // set yesterday
        $date = Carbon::now()
            ->subDays(1)
            ->startOfDay()
            ->toDateTimeString();
        $now = Carbon::now()
            ->startOfDay()
            ->toDateTimeString();

        // grab orders with order item discrepencies
        $ordersWithOrderItemDiscrepancies = DB::select('SELECT orders.id, orders.status, JSON_LENGTH(orders.content) AS c, COUNT(order_items.id) AS oi FROM orders LEFT JOIN order_items ON orders.id = order_items.order_id WHERE orders.status = 3 AND delivered_At BETWEEN "' . $date . '" AND "' . $now . '" GROUP BY orders.id, c HAVING c != oi;');

        // get runner hours
        $scheduledRunners = Schedule::whereBetween('start', [
            $date,
            $now
        ])
            ->whereNotIn('user_id', [131, 1600])
            ->get();
        $formattedRunners    = [];
        $runnerHoursWorked   = 0;
        $dispatchHoursWorked = 0;

        foreach ($scheduledRunners as $scheduledRunner) {
            if ($scheduledRunner->job_type == 2) {
                $scheduledRunner->end         = Carbon::parse($scheduledRunner->end)->toTimeString();
                $scheduledRunner->start       = Carbon::parse($scheduledRunner->start)->toTimeString();
                $scheduledRunner->name        = $scheduledRunner->user->first_name;
                $scheduledRunner->hoursWorked = Carbon::parse($scheduledRunner->end)->diffInMinutes(Carbon::parse($scheduledRunner->start)) / 60;
                $dispatchHoursWorked += $scheduledRunner->hoursWorked;

                array_push($formattedRunners, $scheduledRunner);
            } elseif ($scheduledRunner->job_type == 1) {
                $scheduledRunner->end         = Carbon::parse($scheduledRunner->end)->toTimeString();
                $scheduledRunner->start       = Carbon::parse($scheduledRunner->start)->toTimeString();
                $scheduledRunner->name        = $scheduledRunner->user->first_name;
                $scheduledRunner->hoursWorked = Carbon::parse($scheduledRunner->end)->diffInMinutes(Carbon::parse($scheduledRunner->start)) / 60;
                $runnerHoursWorked += $scheduledRunner->hoursWorked;

                array_push($formattedRunners, $scheduledRunner);
            }
        }

        // general
        $ordersCreated   = Order::whereBetween('created_at', [$date, $now])->count();
        $ordersDelivered = Order::whereBetween('delivered_at', [$date, $now])->count();
        $ordersCancelled = Order::whereBetween('updated_at', [$date, $now])
            ->where('status', 5)
            ->count();

        // finances
        $revenue = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('total')
            ->sum();
        $costOfGoodsSold = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('cogs')
            ->sum();
        $deliveryFees = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('delivery')
            ->sum();
        $tips = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('tip')
            ->sum();
        $markup = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('markup')
            ->sum();
        $serviceFees = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('service_fee')
            ->sum();
        $discounts = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('discount')
            ->sum();
        $stripeFees = Order::where('status', 3)
            ->whereBetween('delivered_at', [$date, $now])
            ->whereNotIn('user_id', [131, 1600])
            ->pluck('stripe_fee')
            ->sum();

        $finances = [
            'revenue'          => $revenue / 100,
            'costOfGoodsSold'  => $costOfGoodsSold / 100,
            'runnerCosts'      => $runnerHoursWorked * 12.20,
            'dispatchCosts'    => $dispatchHoursWorked * 15.00,
            'deliveryFees'     => $deliveryFees / 100,
            'serviceFees'      => $serviceFees / 100,
            'discounts'        => $discounts / 100,
            'stripeFees'       => $stripeFees / 100,
            'tips'             => $tips / 100,
            'markup'           => $markup / 100,
        ];

        $general = [
            'ordersCreated'   => $ordersCreated,
            'ordersDelivered' => $ordersDelivered,
            'ordersCancelled' => $ordersCancelled
        ];

        $file = Excel::download(new DailyStatusExport($formattedRunners, $finances, $date), 'daily-status-' . $date . '.xlsx')
            ->getFile();

        $emails = [
            'ops@getrunner.io',
            'ryan@ryankagan.ca'
        ];

        Mail::send(new DailyStatusSent($formattedRunners, $finances, $date, $emails, $file, $ordersWithOrderItemDiscrepancies, $general));

        $this->info('Late orders email sent');
    }
}
