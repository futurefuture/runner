<?php

namespace App\Console\Commands;

use App\Inventory;
use App\InventoryProduct;
use App\Product;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class InventoriesUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:test-inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '2048M');

        $lcboStores = Inventory::where('vendor', 'LCBO')
            ->where('is_active', 1)
            ->whereNotNull('location_id')
            ->get();

        $this->info('getting products start');

        foreach ($lcboStores as $lcboStore) {
            // set all lcbo inventories to 0 first
            InventoryProduct::where('inventory_id', $lcboStore->id)->update([
                'quantity' => 0
            ]);

            $client      = new Client();
            $response    = $client->request('GET', 'https://api.lcbo.com/store_inventory/?locationNumber=' . $lcboStore->location_id);
            $products    = json_decode($response->getBody()->getContents())->products;
            $progressBar = $this->output->createProgressBar(count($products));

            foreach ($products as $product) {
                if ($product->total_inv < 10) {
                    continue;
                }

                $actualProduct = Product::where('sku', $product->product__id)
                    ->where('is_updatable', true)
                    ->first();

                if ($actualProduct) {
                    InventoryProduct::updateOrCreate([
                        'product_id'   => $actualProduct->id,
                        'inventory_id' => $lcboStore->id,
                    ], [
                        'inventory_id' => $lcboStore->id,
                        'product_id'   => $actualProduct->id,
                        'quantity'     => $product->total_inv,
                    ]);
                }

                $progressBar->advance();
            }
        }

        $progressBar->finish();
    }
}
