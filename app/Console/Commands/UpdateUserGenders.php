<?php

namespace App\Console\Commands;

use App\Actions\GetUserGenderAction;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateUserGenders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-user-genders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all user genders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(GetUserGenderAction $getUserGenderAction)
    {
        $userCount = User::all()->count();
        $progress = $this->output->createProgressBar($userCount);

        User::chunk(1000, function ($users) use ($getUserGenderAction, $progress) {
            foreach ($users as $u) {
                if ($u->gender == null) {
                    $getUserGenderAction->execute($u);
                }

                $progress->advance();
            }
        });

        $progress->finish();
    }
}
