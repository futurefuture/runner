<?php

namespace App\Console\Commands;

use App\Cart;
use App\Mail\UserAbandonedCart;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DispatchEmailUserWithAbandonedCart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:deuwac';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $threeHoursAgo = Carbon::now()
                            ->subHours(3)
                            ->toDateTimeString();
        $oneDayAgo = Carbon::now()
                        ->subDay(1)
                        ->toDateTimeString();
        $freshCarts = Cart::where('marketing_status', 0)
                        ->where('updated_at', '<=', $threeHoursAgo)
                        ->get();
        $oldCarts = Cart::where('marketing_status', 1)
                        ->where('updated_at', '<=', $oneDayAgo)
                        ->get();

        foreach ($freshCarts as $f) {
            $user = User::find($f->user_id);

            if ($user) {
                Mail::to($user->email)
                    ->bcc('service@getrunner.io')
                    ->queue(new UserAbandonedCart($user, $f));

                $f->marketing_status = 1;
                $f->save();
            }
        }

        foreach ($oldCarts as $o) {
            $user = User::find($o->user_id);

            if ($user) {
                if (! $user->couponsUsed()->where('code', 'CARTSAVE')->exists()) {
                    Mail::to($user->email)
                    ->bcc('service@getrunner.io')
                    ->queue(new UserAbandonedCart($user, $o));
                }

                $o->marketing_status = 2;
                $o->save();
            }
        }
    }
}
