<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class UpdateProductIncentivesToInactive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-product-incentives';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks end date on product incentives and updates to inactive if past end date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productIncentives = DB::table('incentive_product')
            ->get();

        foreach ($productIncentives as $pI) {
            $endDate = Carbon::parse($pI->end_date);

            if ($endDate->isPast()) {
                DB::table('incentive_product')->where('id', $pI->id)->update([
                    'is_active' => 0,
                ]);
            }
        }
    }
}
