<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class RedShiftOrderTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:order_completed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update partner ids in order completed table';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query    = 'SELECT JSON_EXTRACT_PATH_TEXT(product, "product_id") as product_id, order_id FROM(SELECT order_id, JSON_EXTRACT_ARRAY_ELEMENT_TEXT(products, seq.i) AS product FROM php_production.order_completed, seq_0_to_5000 AS seq WHERE seq.i < JSON_ARRAY_LENGTH(products))';
        $products = DB::connection('pgsql')
            ->select($query);
        $progressBar = $this->output->createProgressBar(count($products));

        foreach ($products as $product) {
            $progressBar->advance();

            $partnerIds = DB::table('partner_product')
                ->where('product_id', $product->product_id)
                ->where('product_id', '<>', 0)
                ->pluck('partner_id')
                ->toArray();

            if (! $partnerIds) {
                $update      = 'update php_production.order_completed set partner_ids = "[]" where order_id =' . $product->order_id;
                $orderUpdate = DB::connection('pgsql')->update($update);
            } else {
                $ps          = implode(',', $partnerIds);
                $update      = 'update php_production.order_completed set partner_ids =' . "'[" . $ps . "]'  where order_id =" . $product->order_id;
                $orderUpdate = DB::connection('pgsql')->update($update);
            }
        }

        $progressBar->finish();
    }
}
