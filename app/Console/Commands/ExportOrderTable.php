<?php

namespace App\Console\Commands;

use App\Exports\OrdersExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ExportOrderTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:export-orders {month} {dates*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports orders table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();

        Excel::store(new OrdersExport($arguments['dates'][0], $arguments['dates'][1]), $arguments['month'].'.xlsx');
    }
}
