<?php

namespace App\Console\Commands;

use App\Utilities\Jobs\PublishScheduledOrdersToReceived as Job;
use Illuminate\Console\Command;

class PublishOrderToReceived extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:publish_received';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check schedule time and change it to status 1, if it is within 2 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Job::run();
    }
}
