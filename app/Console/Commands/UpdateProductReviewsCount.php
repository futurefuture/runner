<?php

namespace App\Console\Commands;

use App\Product;
use App\Review;
use Illuminate\Console\Command;

class UpdateProductReviewsCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runner:update-product-reviews-count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the number of reviews on the products table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reviews        = Review::all();
        $uniqueProducts = $reviews->unique('product_id');
        $progress       = $this->output->createProgressBar(count($reviews));

        foreach ($uniqueProducts as $u) {
            $product = Product::find($u->product_id);
            $count   = Review::where('product_id', $u->product_id)->count();

            if ($product) {
                $product->reviews_count = $count;
                $product->save();
                $progress->advance();
            }
        }

        $progress->finish();
    }
}
