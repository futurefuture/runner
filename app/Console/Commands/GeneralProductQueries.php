<?php

namespace App\Console\Commands;

use App\User;
use App\Order;
use App\Address;
use App\Partner;
use App\Product;
use App\Category;
use App\OrderItem;
use Carbon\Carbon;
use App\Exports\OrderDetails;
use App\Mail\OrderDetailsMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class GeneralProductQueries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature     = 'runner:gpq';
    protected $categoryArray = [];
    protected $count         = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private static function getchildren($src_arr, $parent_id)
    {
        $allchildren = [];

        foreach ($src_arr as $idx => $row) {
            if ($row['parent_id'] == $parent_id) {
                $children = static::getchildren($src_arr, $row['id']);
                if ($children) {
                    $allchildren = array_merge($allchildren, $children);
                } else {
                    $allchildren[] = $row;
                }
            }
        }

        var_dump($allchildren);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getAllChildCategories(3);

        dd(implode('", "', $this->categoryArray));

        $orderItems = OrderItem::whereIn('category_id', $this->categoryArray)
            ->pluck('user_id')->unique;
    }

    private function getAllCategories($categoryId)
    {
        $category = Category::find($categoryId);

        if ($this->count === 0) {
            array_push($this->categoryArray, $categoryId);
        }
        $this->count++;

        if (! $category) {
            return $this->categoryArray;
        } else {
            if ($category->parent_id) {
                array_push($this->categoryArray, $category->parent_id);
            }

            return $this->getAllCategories($category->parent_id);
        }
    }

    private function getAllChildCategories($categoryId)
    {
        $category = Category::find($categoryId);
        $children = Category::where('parent_id', $categoryId)->get();

        if ($this->count === 0) {
            array_push($this->categoryArray, $categoryId);
        }
        $this->count++;

        if (! $children) {
            return $this->categoryArray;
        } else {
            foreach ($children as $c) {
                array_push($this->categoryArray, $c->id);

                $this->getAllChildCategories($c->id);
            }
        }
    }
}
