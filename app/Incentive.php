<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Incentive extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incentives';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
