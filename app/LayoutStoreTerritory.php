<?php

namespace App;

use App\Store;
use App\Layout;
use App\Territory;
use Illuminate\Database\Eloquent\Model;

class LayoutStoreTerritory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'layout_store_territory';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'layout_id',
        'store_id',
        'territory_id',
    ];

    /**
     * Get the layout associated with the territory's store layout.
     *
     * @return void
     */
    public function layout()
    {
        return $this->belongsTo(Layout::class);
    }

    /**
     * Get the store associated with the territory's store layout.
     *
     * @return void
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Get the territory associated with the store layout.
     *
     * @return void
     */
    public function territory()
    {
        return $this->belongsTo(Territory::class);
    }
}
