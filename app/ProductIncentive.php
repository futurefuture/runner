<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductIncentive extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incentive_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'incentive_id',
        'product_id',
        'start_date',
        'end_date',
        'savings',
        'is_active',
        'reward_points',
        'minimum_quantity',
        'coupon_code',
        'minimum_cart_value',
    ];
}
