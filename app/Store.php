<?php

namespace App;

use App\Category;
use App\Inventory;
use App\StoreLayout;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    protected $id;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options'          => 'array',
        'layout'           => 'array',
        'cart_extras'      => 'array',
        'is_for_recipient' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'options',
        'layout',
        'cart_extras',
        'is_active',
        'index',
    ];

    /**
     * The inventories that belong to the store.
     *
     * @return void
     */
    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'inventory_store', 'store_id', 'inventory_id');
    }

    /**
     * The store layouts that belong to the store.
     *
     * @return void
     */
    public function storeLayouts()
    {
        return $this->hasMany(StoreLayout::class, 'store_id');
    }

    /**
     * The categories that belong to the store.
     *
     * @return void
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)
            ->where('is_visible', true)
            ->with('tags');
    }

    /**
     * The tags that belong to the store.
     *
     * @return void
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function nextDeliveryTime()
    {
        $eleven = Carbon::createFromTime(11, 0, 0);
        $four   = Carbon::createFromTime(16, 0, 0);

        if ($this->id === 2) {
            return 'Within 1 hour';
        } elseif ($this->id === 10 || $this->id === 4) {
            if (Carbon::now()->lessThanOrEqualTo($eleven)) {
                return 'Between 12pm and 5pm';
            } elseif (Carbon::now()->lessThanOrEqualTo($four)) {
                return 'Between 5pm and 9pm';
            } else {
                return 'Tomorrow';
            }
        } else {
            return 'Within 2 hours';
        }
    }

    public function deliveryFee()
    {
        return 999;
    }
}
