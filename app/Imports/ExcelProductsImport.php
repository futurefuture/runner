<?php

namespace App\Imports;

use App\ProductTag;
use App\CategoryProduct;
use App\Tag;
use App\Category;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelProductsImport implements ToCollection, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * Finds the row values from an Excel file and creates values for ProductTag/CategoryProduct Models.
     *
     * @param Collection $rows
     * @return void
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $i = 8;

            while ($i <= 14) {
                if (isset($row[$i])) {
                    if (Tag::where('slug', $row[$i])->value('id') !== null) {
                        ProductTag::firstOrCreate([
                            'product_id' => $row[0],
                            'tag_id'     => Tag::where('slug', $row[$i])->value('id'),
                            'index'      => 300,
                        ]);
                    }
                }
                $i += 1;
            }

            $j = 5;

            while ($j <= 7) {
                if (isset($row[$j])) {
                    if (Category::where('slug', $row[$j])->value('id') !== null) {
                        CategoryProduct::firstOrCreate([
                            'category_id' => Category::where('slug', $row[$j])->value('id'),
                            'product_id'  => $row[0],
                            'index'       => 300,
                        ]);
                    }
                }

                $j += 1;
            }
        }
    }
}
