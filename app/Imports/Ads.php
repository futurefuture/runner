<?php

namespace App\Imports;

use App\Ad;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class Ads implements ToCollection, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows): void
    {
        foreach ($rows as $row) {
            $ad = Ad::updateOrCreate(
                [
                    'ad_type_id'            => $row[3],
                    'partner_id'            => $row[4],
                    'campaign_id'           => $row[5],
                    'ad_title'              => $row[2],
                    'product_id'            => $row[6],
                    'category_id'           => $row[8],
                    'tag_id'                => $row[9],
                    'carousel_component_id' => $row[11],
                    'start_date'            => $row[0],
                    'end_date'              => $row[1]
                ],
                [
                    'index'       => $row[7],
                    'carousel_id' => $row[10],
                    'bid_type'    => $row[12],
                    'bid_amount'  => $row[13],
                    'state'       => $row[14]
                ]
            );
        }
    }
}
