<?php

namespace App\Imports;

use App\Product;
use DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class IncentiveProductImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $productId = Product::where('sku', $row[2])->pluck('id')->first();

            DB::table('incentive_product')->insert([
                'incentive_id'           => $row[1],
                'product_id'             => $productId,
                'start_date'             => $row[3],
                'end_date'               => $row[4],
                'savings'                => $row[5],
                'is_active'              => $row[6],
            ]);
        }
    }
}
