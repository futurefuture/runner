<?php

namespace App;

use App\User;
use App\Product;
use App\Category;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tag extends Model
{
    use Searchable;

    protected $table = 'tags';

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'tags';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'type',
        'is_active',
        'is_visible',
    ];

    /**
     * Gets the products associated with the tag.
     *
     * @return array
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_tag', 'tag_id', 'product_id')
            ->withPivot('index');
    }

    /**
     * Gets the products associated with the tag.
     *
     * @return array
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Gets the products associated with the tag.
     *
     * @return array
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_tag', 'tag_id', 'category_id');
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    /**
     * Gets the tags that are active.
     *
     * @return array
     */
    public function scopeIsActive()
    {
        return $this->where('is_active', 1);
    }

    /**
     * Gets the tags that are visible.
     *
     * @return array
     */
    public function scopeIsVisible()
    {
        return $this->where('is_visible', 1);
    }
}
