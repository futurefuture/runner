<?php

namespace App\Classes;

use Illuminate\Support\Str;

class Utilities
{
    public static function toLowerCase($str)
    {
        return strtolower($str);
    }

    public function changeCamelCaseToSnakeCase($data)
    {
        $formattedData = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = $this->changeCamelCaseToSnakeCase($value);
            } else if (is_object($value)) {
                $value = $this->changeCamelCaseToSnakeCase((array) $value);
            }

            $formattedData[Str::snake($key)] = $value;

            unset($data[$key]);
        }

        return $formattedData;
    }

    public function changeSnakeCaseToCamelCase($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = $this->changeSnakeCaseToCamelCase($value);
            }

            $formattedData[Str::camel($key)] = $value;

            unset($data[$key]);
        }

        return $formattedData;
    }
}
