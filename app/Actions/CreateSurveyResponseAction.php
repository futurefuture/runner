<?php

namespace App\Actions;

use App\SurveyResponse;

class CreateSurveyResponseAction
{
    public function execute(array $data): SurveyResponse
    {
        $surveyResponse = SurveyResponse::create([
            'survey_id'         => $data['surveyId'],
            'survey_option_id'  => $data['surveyOptionId'],
            'user_id'           => $data['userId'],
        ]);

        return $surveyResponse;
    }
}
