<?php

namespace App\Actions;

use App\Review;
use App\Product;

class CreateProductReviewAction
{
    public function execute(Product $product, array $data): Review
    {
        $review = Review::create([
            'title'       => $data['title'],
            'value'       => $data['value'],
            'product_id'  => $product->id,
            'user_id'     => $data['userId'],
            'description' => $data['description'],
        ]);

        return $review;
    }
}
