<?php

namespace App\Actions;

use App\User;
use App\Services\StripeService;

class CreateCustomerSourceAction
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function execute(User $user, array $data)
    {
        $stripeService = resolve(StripeService::class);
        $source        = $stripeService->createSource($user, $data['id']);

        return $source;
    }
}
