<?php

namespace App\Actions;

use App\User;

class LogoutUserAction
{
    public function execute(User $user): void
    {
        $accessTokens = $user->tokens;

        foreach ($accessTokens as $accessToken) {
            $accessToken->revoke();
        }
    }
}
