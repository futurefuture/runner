<?php

namespace App\Actions\ReferAFriend;

use App\Actions\MailgunValidateEmailAction;
use App\Mail\FriendReferred;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class ReferAFriendAction
{
    protected $mgValidation;

    public function __construct(MailgunValidateEmailAction $action)
    {
        $this->mgValidation = $action;
    }

    public function execute(User $user, array $data)
    {
        if ($data['type'] == 'email') {
            $email = $data['contact_info'];

            // validate email
            $this->mgValidation->execute($email);

            // email to referred friend
            Mail::to($email)->queue(new FriendReferred($user));

            DB::table('invite_user')->insert([
                'user_id'       => $user->id,
                'email_sent_to' => $email,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);
        } else {
            $phoneNumber = $data['contact_info'];
            $url         = env('APP_URL') . '/register/inviteCode=' . $user->invite_code;
            $text        = 'Hey it\'s ' . $user->first_name . '! I\'ve been using Runner to have alcohol delivered to my door - thought you\'d love it too. Get $10 off: ' . $url . ' !';

            if (env('APP_ENV') === 'production') {
                Nexmo::message()->send([
                    'to'   => '1' . $phoneNumber,
                    'from' => '12493155516',
                    'text' => $text,
                ]);
            }

            DB::table('invite_user')->insert([
                'user_id'              => $user->id,
                'phone_number_sent_to' => $data['contact_info'],
                'created_at'           => Carbon::now(),
                'updated_at'           => Carbon::now(),
            ]);
        }
    }
}
