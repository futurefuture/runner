<?php

namespace App\Actions\Order;

use App\Task;
use App\User;
use App\Order;
use App\Refund;
use App\Product;
use App\Inventory;
use Carbon\Carbon;
use App\Events\NewOrder;
use App\InventoryProduct;
use App\Mail\OrderCancelled;
use App\Events\TaskStateUpdated;
use App\Services\Gateways\Gateway;
use App\Services\RewardPointService;
use Illuminate\Support\Facades\Mail;
use App\Actions\InventoryProduct\UpdateInventoryProductQuantity;

class CancelOrder
{
    protected $updateInventoryProductQuantity;

    public function __construct()
    {
        $this->updateInventoryProductQuantity = new UpdateInventoryProductQuantity();
    }

    public function execute(Order $order): Order
    {
        $activityLog = $order->activity_log != null ? json_decode($order->activity_log) : [];
        $user        = User::find($order->user_id);
        $inventory   = Inventory::find($order->inventory_id);

        array_push($activityLog, (object) [
            'log'  => 'cancelled',
            'time' => Carbon::now(),
        ]);

        (new RewardPointService())->addPoints(-100, USER::CANCEL_PURCHASE, null, $user->id);

        $order->update([
            'refund' => $order->total,
            'status' => 5
        ]);

        event(new NewOrder($order));

        if (env('APP_ENV') === 'production') {
            Mail::to($user->email)->send(new OrderCancelled($order));
        }

        $task = Task::where('order_id', '=', $order->id)->first();

        if ($task) {
            $task->state = 5;
            $task->save();

            event(new TaskStateUpdated($task));
        }

        // send order to refund table since cancelled (will cost more since delivery != indivudal order content)
        Refund::create([
            'order_id'  => $order->id,
            'user_id'   => $order->user_id,
            'refund_id' => '',
            'charge_id' => $order->charge ?? '',
            'amount'    => $order->refund ?? 0,
            'reason'    => '',
        ]);

        if (is_array($order->content)) {
            foreach ($order->content as $q) {
                $product   = Product::find($q->id);

                $this->updateInventoryProductQuantity->execute($product, $inventory, $q->quantity);
            }
        } elseif (is_string($order->content)) {
            foreach (json_decode($order->content) as $q) {
                $product   = Product::find($q->id);

                $this->updateInventoryProductQuantity->execute($product, $inventory, $q->quantity);
            }
        }

        return $order;
    }
}
