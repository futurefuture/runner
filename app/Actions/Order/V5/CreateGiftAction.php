<?php

namespace App\Actions\Order\V5;

use App\Gift;
use App\Order;

class CreateGiftAction
{
    public function execute(array $data, Order $order): Gift
    {
        $gift = Gift::create([
            'order_id'     => $order->id,
            'first_name'   => $data['first_name'],
            'last_name'    => $data['last_name'],
            'address'      => 'test',
            'unit_number'  => 'test',
            'email'        => $data['email'],
            'instructions' => 'test',
            'is_19'        => true,
            'phone_number' => $data['phone_number'],
            'message'      => $data['personal_message'],
        ]);

        return $gift;
    }
}
