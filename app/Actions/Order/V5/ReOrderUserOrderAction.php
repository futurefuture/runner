<?php

namespace App\Actions\Order\V5;

use App\Cart;
use App\User;
use App\Order;
use App\Product;
use Exception;
use Illuminate\Support\Facades\DB;

class ReOrderUserOrderAction
{
    public function execute(User $user, Order $order, string $inventoryId): Cart
    {
        // verify that user has orders
        if (!$user->orders()->where('status', 3)->count()) {
            throw new Exception('sorry, but you don\'t have any orders');
        }

        $cartItems         = json_decode($order->content);
        $cart              = [];
        $formattedProducts = [];
        $cartSubtotal      = 0;
        $cartTax           = 0;
        $cartDiscount      = 0;
        $deliveryFee       = 999;

        // check for reward points
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 10,
            ];
            $cart['coupon'] = $coupon;
            $cartDiscount   = (int) $coupon->value * 100;
        }

        foreach ($cartItems as $cartItem) {
            $product = Product::find((int) $cartItem->id);
            // check for limited time offer on product
            $runnerPrice                      = 0;
            $retailPrice                      = 0;
            $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
                ->where('product_id', $product->id)
                ->where('incentive_id', 1)
                ->where('is_active', 1)
                ->first();
            if ($productIncentiveLimitedTimeOffer) {
                $runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
                $retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
            } else {
                $runnerPrice = $product->runner_price;
                $retailPrice = $product->retail_price;
            }
            $formattedProduct['id']           = $product->id;
            $formattedProduct['title']        = $product->title;
            $formattedProduct['runner_price']  = $runnerPrice;
            $formattedProduct['retail_price']  = $retailPrice;
            $formattedProduct['image']        = $product->image;
            $formattedProduct['quantity']     = isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty;
            $formattedProduct['packaging']    = $product->packaging;
            $formattedProduct['reward_points'] = $product->rewardPoints;
            $formattedProduct['allow_sub']     = isset($cartItem->allowSub) ? $cartItem->allowSub : true;
            $formattedProduct['sub_total']     = $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            $cartSubtotal += $runnerPrice * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);
            $cartTax += (($runnerPrice - $runnerPrice / 1.12) * 0.13) * (isset($cartItem->quantity) ? $cartItem->quantity : $cartItem->qty);

            array_push($formattedProducts, $formattedProduct);
        }

        $cart['products']         = $formattedProducts;
        $cart['sub_total']         = $cartSubtotal;
        $cart['delivery_fee']      = $deliveryFee;
        $cart['tax']              = round($cartTax + ($deliveryFee * 0.13));
        $cart['reward_points_used'] = 0;
        $cart['discount']         = $cartDiscount;
        $cart['total']            = round($cartSubtotal + $cartTax + $deliveryFee + ($deliveryFee * 0.13) - $cartDiscount);

        $oldCart = Cart::where('user_id', (int) $user->id)
            ->where('inventory_id', $inventoryId)
            ->first();

        if ($oldCart) {
            Cart::destroy($oldCart->id);
        }

        $newCart = Cart::create([
            'user_id'      => $user->id,
            'inventory_id' => $inventoryId,
            'content'      => json_encode($cart),
        ]);

        return $newCart;
    }
}
