<?php

namespace App\Actions;

use App\User;
use App\Services\StripeService;

class DeleteCustomerSourceAction
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function execute(User $user, string $sourceId)
    {
        $stripeService = resolve(StripeService::class);
        $source        = $stripeService->deleteSource($user, $sourceId);

        return $source;
    }
}
