<?php

namespace App\Actions\OrderItem;

use App\Order;
use App\Product;
use App\OrderItem;

class CreateOrderItems
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function execute()
    {
        $items = json_decode($this->order->content);

        foreach ($items as $key => $value) {
            $product = Product::withTrashed()->find($value->id);

            if ((int) $value->quantity == 0) {
                continue;
            }

            if (! $product) {
                continue;
            } else {
                OrderItem::updateOrCreate(
                    [
                        'order_id'   => $this->order->id,
                        'user_id'    => $this->order->user_id,
                        'product_id' => $product->id
                    ],
                    [
                        'title'          => $value->title,
                        'quantity'       => (int) $value->quantity,
                        'retail_price'   => $value->retailPrice,
                        'runner_price'   => $value->runnerPrice,
                        'packaging'      => $value->packaging,
                        'producer'       => $product->producer,
                        'category_id'    => $product->category_id,
                        'created_at'     => $this->order->created_at,
                        'updated_at'     => $this->order->created_at,
                        'pending_review' => true
                    ]
                );
            }
        }
    }
}
