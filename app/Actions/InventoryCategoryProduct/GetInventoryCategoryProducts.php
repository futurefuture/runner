<?php

namespace App\Actions\InventoryCategoryProduct;

use App\Category;
use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use App\Utilities\Filters\FiltersTags;

class GetInventoryCategoryProducts
{
    public function execute(Request $request, Inventory $inventory, Category $category)
    {
        $limit = $request->input('limit') ?? 20;

        if ($category->id != 210) {
            $query = $inventory
                ->productsAll()
                ->join('category_product', 'products.id', '=', 'category_product.product_id')
                ->join('categories', 'categories.id', '=', 'category_product.category_id')
                ->where('category_product.category_id', '=', $category->id)
                ->select('products.*', 'category_product.index', 'quantity')
                ->with([
                    'reviews',
                    'partners',
                    'incentives',
                    'productImages',
                    'ads',
                ])
                ->getQuery();

            $singleTags = true;

            if (isset($request->input('filter')['tags'])) {
                if (count(explode(',', $request->input('filter')['tags'])) >= 2) {
                    $singleTags = false;
                }
            }

            if (isset($request->input('filter')['tags']) && ! $singleTags) {
                $allProducts = QueryBuilder::for($query)
                    ->allowedFilters([
                        AllowedFilter::custom('tags', new FiltersTags),
                        AllowedFilter::scope('price'),
                        AllowedFilter::scope('averageRating'),
                    ])
                    ->allowedIncludes([
                        'reviews',
                    ])
                    ->allowedSorts([
                        AllowedSort::field('price', 'runner_price'),
                        AllowedSort::field('title'),
                        AllowedSort::field('averageRating', 'average_rating'),
                    ])
                    ->orderBy('quantity', 'desc')
                    ->defaultSort('-average_rating')
                    ->paginate($limit)
                    ->appends($request->except('page'));
            } else {
                $allProducts = QueryBuilder::for($query)
                    ->allowedFilters([
                        AllowedFilter::custom('tags', new FiltersTags),
                        AllowedFilter::scope('price'),
                        AllowedFilter::scope('averageRating'),
                    ])
                    ->allowedIncludes([
                        'reviews',
                    ])
                    ->allowedSorts([
                        AllowedSort::field('price', 'runner_price'),
                        AllowedSort::field('title'),
                        AllowedSort::field('averageRating', 'average_rating'),
                    ])
                    ->orderBy('category_product.index')
                    ->orderBy('quantity', 'desc')
                    ->defaultSort('-average_rating')
                    ->paginate($limit)
                    ->appends($request->except('page'));
            }
        } else {
            $query = $inventory
                ->productsAll()
                ->getQuery();

            $allProducts = QueryBuilder::for($query)
                ->allowedFilters([
                    AllowedFilter::custom('tags', new FiltersTags),
                    AllowedFilter::scope('price'),
                    AllowedFilter::scope('averageRating'),
                ])
                ->allowedIncludes([
                    'reviews',
                ])
                ->allowedSorts([
                    AllowedSort::field('price', 'runner_price'),
                    AllowedSort::field('title'),
                    AllowedSort::field('averageRating', 'average_rating'),
                ])
                ->orderBy('quantity', 'desc')
                ->defaultSort('-average_rating')
                ->paginate($limit)
                ->appends($request->except('page'));
        }

        return $allProducts;
    }
}
