<?php

namespace App\Actions;

use App\Cart;
use App\User;
use Exception;
use App\Product;
use App\Category;
use App\Inventory;
use Carbon\Carbon;
use App\InventoryProduct;
use App\Services\SegmentService;
use Illuminate\Support\Facades\DB;

class CreateUserInventoryCartAction
{
    protected $cart;
    protected $cartDiscount  = 0;
    protected $itemsTax      = 0;
    protected $cartSubTotal  = 0;
    protected $deliveryFee   = 999;
    protected $deliveryTax   = 130;
    protected $serviceFee    = 0;
    protected $serviceFeeTax = 0;
    protected $isGift        = false;
    protected $runnerPrice   = 0;
    protected $retailPrice   = 0;
    protected $cartTax       = 0;

    public function execute(User $user, Inventory $inventory, array $data): Cart
    {
        $products    = $data['products'];
        $anonymousId = isset($data['anonymousId']) ? $data['anonymousId'] : null;
        $couponCode  = isset($data['couponCode']) ? $data['couponCode'] : null;

        $this->checkIfIsGiftCart($inventory->id);

        if ($couponCode) {
            $this->verifyCouponCode($user, $couponCode);
        }

        $this->ensureRewardPointsExist($user);

        $products = $this->cartProducts($products, $couponCode, $user, $inventory);

        $this->cartTax = round($this->itemsTax + $this->deliveryTax + $this->serviceFeeTax);

        $this->cart['products']         = $products;
        $this->cart['subTotal']         = $this->cartSubTotal;
        $this->cart['deliveryFee']      = $this->deliveryFee;
        $this->cart['serviceFee']       = $this->serviceFee;
        $this->cart['tax']              = $this->cartTax;
        $this->cart['rewardPointsUsed'] = 0;
        $this->cart['isGift']           = $this->isGift;
        $this->cart['discount']         = $this->cartDiscount;
        $this->cart['serviceFee']       = $this->serviceFee;
        $this->cart['total']            = round($this->cartSubTotal + $this->cartTax + $this->deliveryFee + $this->serviceFee - $this->cartDiscount);

        $cart = Cart::updateOrCreate(
            [
                'user_id'      => (int) $user->id,
                'inventory_id' => (int) $inventory->id,
            ],
            [
                'content' => json_encode($this->cart),
            ]
        );

        $cart->content = json_decode($cart->content);
        $product       = Product::find($products[0]['id']);
        $category      = Category::find($product->category_id);
        $quantity      = $products[0]['quantity'];
        $coupon        = $couponCode ?? null;
        $storeId       = Inventory::find($inventory->id)->first()->store_id;

        (new SegmentService())->trackProductAdded($user, $product, $cart, $category, $quantity, $coupon, $storeId, $inventory->id, $anonymousId);

        return $cart;
    }

    private function checkIfIsGiftCart($inventoryId)
    {
        if ($inventoryId == 11 || $inventoryId == 23) {
            $this->isGift        = true;
            $this->serviceFee    = 500;
            $this->serviceFeeTax = 500 * 0.13;
        }
    }

    private function verifyCouponCode(User $user, string $couponCode)
    {
        $coupon = Coupon::where('code', $couponCode)
            ->where('is_active', 1)
            ->first();
        if ($coupon) {
            if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $user->id)->where('used', 1)->doesntExist()) {
                $this->cart['coupon'] = (object) [
                    'code'  => $coupon->code,
                    'value' => (int) $coupon->value,
                ];
                $this->cartDiscount = (int) $coupon->value;
            } else {
                throw new Exception('this coupon code given has already been used');
            }
        } else {
            throw new Exception('this coupon code is invalid');
        }
    }

    private function ensureRewardPointsExist(User $user)
    {
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 1000,
            ];

            $this->cart['coupon'] = $coupon;
            $this->cartDiscount   = (int) $coupon->value;
        }
    }

    private function cartProducts(array $products, string $couponCode = null, User $user, Inventory $inventory)
    {
        $formattedProducts = [];

        foreach ($products as $p) {
            $quantity = (int) $p['quantity'];

            // disallow negative quantity
            if ($quantity < 0) {
                return false;
            }

            $product = Product::find((int) $p['id']);

            // check quantity is in inventory
            $this->validateIsInInventory($product, $quantity, $inventory);

            // check for limited time offer on product
            $this->ensureProductLimitedOfferExists($product);

            // check for quantity discount
            $this->ensureProductQuantityDiscountExists($product, $quantity, 'create', $couponCode, $user);
            $partnerIds = [];

            foreach ($product->partners as $pp) {
                array_push($partnerIds, (string) $pp->id);
            }

            $formattedProduct['id']           = $product->id;
            $formattedProduct['title']        = $product->title;
            $formattedProduct['runnerPrice']  = $this->runnerPrice;
            $formattedProduct['retailPrice']  = $this->retailPrice;
            $formattedProduct['image']        = $product->image;
            $formattedProduct['quantity']     = $quantity;
            $formattedProduct['packaging']    = $product->packaging;
            $formattedProduct['producer']     = $product->producer;
            $formattedProduct['rewardPoints'] = $product->rewardPoints;
            $formattedProduct['allowSub']     = $p['allowSub'];
            $formattedProduct['type']         = $p['type'] ?? 'regular';
            $formattedProduct['subTotal']     = $this->runnerPrice * $quantity;
            $formattedProduct['partnerIds']   = $partnerIds;

            $this->cartSubTotal += $this->runnerPrice * $quantity;

            if (! $product->is_runner_owned) {
                $this->itemsTax += (($this->runnerPrice - $this->runnerPrice / 1.12) * 0.13) * $quantity;
            } else {
                $this->itemsTax += ($product->runner_price * 0.13) * $quantity;
            }

            array_push($formattedProducts, $formattedProduct);
        }

        return $formattedProducts;
    }

    private function validateIsInInventory(Product $product, int $quantity, Inventory $inventory)
    {
        $inventoryQuantity = InventoryProduct::where('inventory_id', $inventory->id)
            ->where('product_id', $product->id)
            ->first()
            ->quantity;

        if ($quantity > $inventoryQuantity) {
            throw new \Exception('We only have ' . $inventoryQuantity . ' in stock. Please select less than that.');
        }
    }

    private function ensureProductLimitedOfferExists(Product $product): void
    {
        if ($product->limited_time_offer_price) {
            $this->runnerPrice = $product->limited_time_offer_price;
            $this->retailPrice = $product->retail_price;
        } else {
            $this->runnerPrice = $product->runner_price;
            $this->retailPrice = $product->retail_price;
        }
        // $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
        //     ->where('product_id', $product->id)
        //     ->where('incentive_id', 1)
        //     ->where('is_active', 1)
        //     ->first();

        // if ($productIncentiveLimitedTimeOffer && Carbon::parse($productIncentiveLimitedTimeOffer->start_date)->isPast() && Carbon::parse($productIncentiveLimitedTimeOffer->end_date)->isFuture()) {
        //     $this->runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
        //     $this->retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
        // } else {
        //     $this->runnerPrice = $product->runner_price;
        //     $this->retailPrice = $product->retail_price;
        // }
    }

    private function ensureProductQuantityDiscountExists($product, $quantity, $type, $couponCode, $user)
    {
        $productQuantityDiscount = DB::table('incentive_product')
            ->where('product_id', $product->id)
            ->where('incentive_id', 3)
            ->first();

        if ($productQuantityDiscount && $quantity > $productQuantityDiscount->minimum_quantity) {
            if (Carbon::parse($productQuantityDiscount->start_date)->isPast() && Carbon::parse($productQuantityDiscount->end_date)->isFuture()) {
                $incentive = (object) [
                    'id'    => (string) $productQuantityDiscount->id,
                    'value' => (int) $productQuantityDiscount->savings,
                ];

                $this->cart['coupon'] = null;

                $this->cart['incentive'] = $incentive;
                $this->cartDiscount      = (int) $productQuantityDiscount->savings;
            }
        } elseif ($productQuantityDiscount && $quantity < $productQuantityDiscount->minimum_quantity) {
            unset($this->cart['incentive']);
            if ($couponCode || $user->rewardPointsBalance() >= 1000) {
            } else {
                $this->cartDiscount = 0;
            }
        }
    }
}
