<?php

namespace App\Actions\User;

use App\User;

class DeleteUser
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function execute(): void
    {
        $this->user->delete();
    }
}
