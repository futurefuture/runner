<?php

namespace App\Actions;

use App\Cart;
use App\User;
use Exception;
use App\Coupon;
use App\Product;
use App\Category;
use App\Inventory;
use Carbon\Carbon;
use App\InventoryProduct;
use App\Services\SegmentService;
use Illuminate\Support\Facades\DB;

class UpdateUserInventoryCartAction
{
    protected $cart;
    protected $cartDiscount  = 0;
    protected $itemsTax      = 0;
    protected $cartSubTotal  = 0;
    protected $deliveryFee   = 999;
    protected $deliveryTax   = 130;
    protected $serviceFee    = 0;
    protected $serviceFeeTax = 0;
    protected $isGift        = false;
    protected $runnerPrice   = 0;
    protected $retailPrice   = 0;
    protected $cartTax       = 0;

    public function execute(User $user, string $cartId, array $data): Cart
    {
        $oldCart            = Cart::find($cartId);
        $newCartItems       = $data['products'] ?? null;
        $anonymousId        = isset($data['anonymousId']) ? $data['anonymousId'] : null;
        $couponCode         = isset($data['couponCode']) ? $data['couponCode'] : null;
        $oldCartItems       = json_decode($oldCart->content)->products;
        $oldCartContent     = json_decode($oldCart->content);
        $oldCartItemIds     = [];
        $cart               = [];

        if (isset($oldCartContent->coupon)) {
            $cart['coupon'] = $oldCartContent->coupon;
        }

        $this->cartDiscount = $oldCartContent->discount;
        $this->checkIfIsGiftCart($oldCart->inventory_id);

        if ($couponCode) {
            $couponCode = $data['couponCode'];

            if ($couponCode === 'CLEARCOUPON') {
                $cart['coupon']     = null;
                $this->cartDiscount = 0;
            } elseif ($couponCode === 'CLEARCART') {
                Cart::destroy($cartId);
            } else {
                $this->verifyCoupon($user, $cartId, $data['couponCode']);
            }
        }

        $this->ensureRewardPointsExist($user);

        foreach ($oldCartItems as $oldCartItem) {
            (int) array_push($oldCartItemIds, $oldCartItem->id);
        }

        $quantity          = $newCartItems ? $newCartItems['0']['quantity'] : 0;
        $coupon            = isset($data['couponCode']) ? $data['couponCode'] : null;

        $newerCartItems    = $this->getCartItemsToUpdate($newCartItems, $oldCartItemIds, $oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId);

        foreach ($newerCartItems as $nc) {
            $product = Product::find($nc->id);

            $this->cartSubTotal += $nc->subTotal;

            if (! $product->is_runner_owned) {
                $this->itemsTax += (($nc->runnerPrice - $nc->runnerPrice / 1.12) * 0.13) * $nc->quantity;
            } else {
                $this->itemsTax += ($product->runner_price * 0.13) * $nc->quantity;
            }
        }

        $this->deliveryFee = $oldCartContent->deliveryFee;
        $deliveryTax       = $this->deliveryFee * 0.13;

        foreach ($oldCartItems as $o) {
            // check for quantity discount
            $this->ensureProductQuantityDiscountExists($oldCartItem, $oldCartItem->quantity, 'update', $couponCode, $user);
        }

        $this->cartTax             = round($this->itemsTax + $deliveryTax + $this->serviceFeeTax);
        $this->cart['products']    = array_merge($newerCartItems);
        $this->cart['isGift']      = $this->isGift;
        $this->cart['serviceFee']  = $this->serviceFee;
        $this->cart['subTotal']    = $this->cartSubTotal;
        $this->cart['tax']         = round($this->cartTax);
        $this->cart['deliveryFee'] = $this->deliveryFee;
        $this->cart['discount']    = $this->cartDiscount;
        $this->cart['total']       = round($this->cartSubTotal + $this->cartTax + $this->deliveryFee + $this->serviceFee - $this->cartDiscount);

        if (count($newerCartItems) === 0) {
            Cart::destroy($cartId);
        } else {
            $oldCart->update([
                'content' => json_encode($this->cart),
            ]);
        }

        return $oldCart;
    }

    private function checkIfIsGiftCart($inventoryId)
    {
        if ($inventoryId == 11 || $inventoryId == 23) {
            $this->isGift        = true;
            $this->serviceFee    = 500;
            $this->serviceFeeTax = 500 * 0.13;
        }
    }

    private function verifyCoupon(User $user, string $cartId, string $couponCode)
    {
        $coupon = Coupon::where('code', $couponCode)
            ->where('is_active', 1)
            ->first();

        if ($coupon && $coupon->is_for_single_product) {
            // make sure cart has item in there and minimum wquantity
            $cart = Cart::find($cartId);

            $cartItems = json_decode($cart->content)->products;

            foreach ($cartItems as $c) {
                if ($c->id == $coupon->product_id && $c->quantity >= $coupon->minimum_quantity) {
                    if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $user->id)->where('used', 1)->doesntExist()) {
                        $this->cart['coupon'] = (object) [
                            'code'  => $coupon->code,
                            'value' => (int) $coupon->value,
                        ];
                        $this->cartDiscount = (int) $coupon->value;
                    } else {
                        throw new Exception('this coupon code given has already been used');
                    }
                }
            }
        } elseif ($coupon) {
            if (DB::table('coupon_user')->where('coupon_id', $coupon->id)->where('user_id', $user->id)->where('used', 1)->doesntExist()) {
                $this->cart['coupon'] = (object) [
                    'code'  => $coupon->code,
                    'value' => (int) $coupon->value,
                ];
                $this->cartDiscount = (int) $coupon->value;
            } else {
                throw new Exception('this coupon code given has already been used');
            }
        } else {
            throw new Exception('this coupon code is invalid');
        }
    }

    private function getCartItemsToUpdate($newCartItems, $oldCartItemIds, $oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId)
    {
        collect($newCartItems)->each(function ($newCartItem) use (&$oldCartItemIds, &$oldCartItems, $user, $oldCart, $coupon, $quantity, $anonymousId) {
            if (in_array($newCartItem['id'], $oldCartItemIds)) {
                foreach ($oldCartItems as $key => $oldCartItem) {
                    if ($newCartItem['id'] == $oldCartItem->id) {
                        $product = Product::find($newCartItem['id']);
                        $category = Category::find($product->category_id);
                        $inventory = Inventory::find($oldCart->inventory_id);
                        $storeId = $inventory->store_id;

                        if ($newCartItem['quantity'] == 0) {
                            unset($oldCartItems[$key]);
                        }

                        // disallow negative quantity
                        if ((int) $newCartItem['quantity'] < 0) {
                            continue;
                        }

                        $newQuantity = (int) $newCartItem['quantity'] - $oldCartItem->quantity;

                        if ((int) $newCartItem['quantity'] > 0) {
                            $this->validateIsInInventory($product, (int) $newCartItem['quantity'], $inventory);
                        }

                        // check if product removed or added
                        if ($newQuantity > 0) {
                            (new SegmentService())->trackProductAdded($user, $product, $oldCart, $category, $newQuantity, $coupon, $storeId, $inventory->id, $anonymousId);
                        } else {
                            (new SegmentService())->trackProductRemoved($user, $product, $oldCart, $category, abs($newQuantity), $coupon, $storeId, $inventory->id, $anonymousId);
                        }

                        $oldCartItem->quantity = (int) $newCartItem['quantity'];
                        $oldCartItem->allowSub = $newCartItem['allowSub'];
                        $oldCartItem->subTotal = $newCartItem['quantity'] * $oldCartItem->runnerPrice;
                    }
                }

                $oldCartItems = (array) array_values($oldCartItems);

                return $oldCartItems;
            } else {
                $product = Product::find($newCartItem['id']);
                $partnerIds = [];

                foreach ($product->partners as $p) {
                    array_push($partnerIds, (string) $p->id);
                }

                $category = Category::find($product->category_id);

                // check for limited time offer on product
                $this->ensureProductLimitedOfferExists($product);

                $newProduct['id'] = $product->id;
                $newProduct['title'] = $product->title;
                $newProduct['type'] = $newCartItem['type'];
                $newProduct['runnerPrice'] = $this->runnerPrice;
                $newProduct['retailPrice'] = $this->retailPrice;
                $newProduct['quantity'] = (int) $newCartItem['quantity'];
                $newProduct['packaging'] = $product->packaging;
                $newProduct['producer'] = $product->producer;
                $newProduct['image'] = $product->image;
                $newProduct['rewardPoints'] = $product->rewardPoints;
                $newProduct['allowSub'] = $newCartItem['allowSub'];
                $newProduct['subTotal'] = $newCartItem['quantity'] * $this->runnerPrice;
                $newProduct['partnerIds'] = $partnerIds;

                array_push($oldCartItems, (object) $newProduct);

                $inventory = Inventory::find($oldCart->inventory_id);
                $storeId = $inventory->store_id;

                (new SegmentService())->trackProductAdded($user, $product, $oldCart, $category, $quantity, $coupon, $storeId, $inventory->id, $anonymousId);
            }
        });

        return $oldCartItems;
    }

    private function validateIsInInventory(Product $product, int $quantity, Inventory $inventory)
    {
        $inventoryQuantity = InventoryProduct::where('inventory_id', $inventory->id)
            ->where('product_id', $product->id)
            ->first()
            ->quantity;

        if ($quantity > $inventoryQuantity) {
            throw new \Exception('We only have ' . $inventoryQuantity . ' in stock. Please select less than that.');
        }
    }

    private function ensureProductLimitedOfferExists(Product $product): void
    {
        if ($product->limited_time_offer_price) {
            $this->runnerPrice = $product->limited_time_offer_price;
            $this->retailPrice = $product->retail_price;
        } else {
            $this->runnerPrice = $product->runner_price;
            $this->retailPrice = $product->retail_price;
        }

        // $productIncentiveLimitedTimeOffer = DB::table('incentive_product')
        //     ->where('product_id', $product->id)
        //     ->where('incentive_id', 1)
        //     ->where('is_active', 1)
        //     ->first();

        // if ($productIncentiveLimitedTimeOffer && Carbon::parse($productIncentiveLimitedTimeOffer->start_date)->isPast() && Carbon::parse($productIncentiveLimitedTimeOffer->end_date)->isFuture()) {
        //     $this->runnerPrice = $product->runner_price - $productIncentiveLimitedTimeOffer->savings;
        //     $this->retailPrice = $product->retail_price - $productIncentiveLimitedTimeOffer->savings;
        // } else {
        //     $this->runnerPrice = $product->runner_price;
        //     $this->retailPrice = $product->retail_price;
        // }
    }

    private function ensureProductQuantityDiscountExists($product, $quantity, $type, $couponCode, $user)
    {
        $productQuantityDiscount = DB::table('incentive_product')
            ->where('product_id', $product->id)
            ->where('incentive_id', 3)
            ->where('is_active', 1)
            ->where('minimum_quantity', '<=', $quantity)
            ->orderBy('minimum_quantity', 'DESC')
            ->get()
            ->toArray();

        if ($productQuantityDiscount) {
            if (Carbon::parse($productQuantityDiscount[0]->start_date)->isPast() && Carbon::parse($productQuantityDiscount[0]->end_date)->isFuture()) {
                $incentive = (object) [
                    'id'    => (string) $productQuantityDiscount[0]->id,
                    'value' => (int) $productQuantityDiscount[0]->savings,
                ];
                $this->cart['coupon'] = null;

                $this->cart['incentive'] = $incentive;
                $this->cartDiscount      = (int) $productQuantityDiscount[0]->savings;
            }
        } else {
            unset($this->cart['incentive']);

            if ($couponCode || $user->rewardPointsBalance() >= 1000) {
            } else {
                $this->cartDiscount = 0;
            }
        }
    }

    private function ensureRewardPointsExist(User $user)
    {
        if ($user->rewardPointsBalance() >= 1000) {
            $coupon = (object) [
                'code'  => 'Reward Points',
                'value' => (int) 1000,
            ];

            $this->cart['coupon'] = $coupon;
            $this->cartDiscount   = (int) $coupon->value;
        }
    }
}
