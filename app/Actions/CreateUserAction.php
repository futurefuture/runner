<?php

namespace App\Actions;

use App\User;
use Exception;
use Carbon\Carbon;
use App\Events\UserCreated;
use Illuminate\Support\Str;
use App\Services\AddressService;
use App\Services\SegmentService;
use App\Services\Gateways\Gateway;
use App\Services\MailChimpService;
use Illuminate\Support\Facades\DB;
use App\Actions\GetUserGenderAction;
use App\Services\RewardPointService;

class CreateUserAction
{
    protected $getUserGenderAction;

    public function __construct(GetUserGenderAction $action)
    {
        $this->getUserGenderAction = $action;
    }

    public function execute($data): User
    {
        // validate is legal drinking age
        $this->validateIsLegalAge($data['is_legal_age']);

        // create stripe user
        $customer = Gateway::via('stripe')->storeCustomer($data);

        $user = User::create([
            'first_name'    => ucfirst(strtolower($data['first_name'])),
            'last_name'     => ucfirst(strtolower($data['last_name'])),
            'phone_number'  => preg_replace('/\D/', '', $data['phone_number']),
            'date_of_birth' => new Carbon($data['date_of_birth'], 'America/Toronto'),
            'email'         => strtolower($data['email']),
            'password'      => isset($data['password']) ? bcrypt($data['password']) : null,
            'facebook_id'   => isset($data['facebook_id']) ? $data['facebook_id'] : null,
            'google_id'     => isset($data['google_id']) ? $data['google_id'] : null,
            'apple_id'      => isset($data['apple_id']) ? $data['apple_id'] : null,
            'stripe_id'     => $customer->id,
            'is_19'         => 1,
            'invite_code'   => strtoupper(Str::random(6)),
            'gender'        => isset($data['gender']) ? $data['gender'] : null,
            'age_range'     => isset($data['age_range']) ? $data['age_range'] : null,
            'avatar_image'  => isset($data['avatar_image']) ? $data['avatar_image'] : null,
            'address'       => $data['address']['postal_code'],
            'source'        => isset($data['source']) ? $data['source'] : null
        ]);

        // grab gender with genderize api and save to user
        $this->getUserGenderAction->execute($user);

        // create address for user
        $address = [
            'user_id'           => $user->id,
            'formatted_address' => $data['address']['formatted_address'],
            'lat'               => $data['address']['lat'],
            'lng'               => $data['address']['lng'],
            'place_id'          => $data['address']['place_id'],
            'postal_code'       => $data['address']['postal_code'],
            'unit_number'       => isset($data['address']['unit_number']) ? $data['address']['unit_number'] : null,
            'selected'          => 1,
        ];

        $this->createAddress($address);
        $this->checkIfWasReferred($user, $data);

        (new SegmentService())->trackSignedUp($user, $data['anonymous_id']);

        return $user;
    }

    private function validateIsLegalAge($isLegalAge)
    {
        if ($isLegalAge == false) {
            throw new Exception('must be of legal drinking age');
        }
    }

    private function createAddress($address)
    {
        (new AddressService())->create($address);
    }

    private function checkIfWasReferred($user, $data)
    {
        // check if referred user id came through ios branch and create record
        if (isset($data['referrerUserId'])) {
            $referrerUserId = $data['referrerUserId'];

            DB::table('invite_user')->insert([
                'user_id'       => $referrerUserId,
                'email_sent_to' => $user->email,
            ]);
        }

        $userWasReferred = DB::table('invite_user')
            ->where('email_sent_to', $user->email)
            ->orWhere('phone_number_sent_to', $user->phone_number)
            ->first();

        // add 1000 reward points to account if referrer exists.
        if ($userWasReferred) {
            $referrer = User::find($userWasReferred->user_id);

            (new RewardPointService())->addPoints(1000, USER::REGISTER_REFERRER, [
                'referrer_id'     => $referrer->id,
                'referrer_name'   => $referrer->first_name . $referrer->last_name,
                'referrer_email'  => $referrer->email,
            ], $user->id);

            $referrer->invites()
                ->create([
                    'invited_user_id' => $user->id,
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now(),
                ]);
        }
    }
}
