<?php

namespace App\Actions;

use App\Inventory;
use App\PostalCode;
use App\Product;
use App\Store;
use Illuminate\Support\Facades\DB;

class GetProductsSearchedByQueryStringAction
{
    public function execute(string $query, string $fullPostalCode)
    {
        $query       = strtolower($query);
        $postalCode  = PostalCode::find(substr($fullPostalCode, 0, 3));
        $inventories = $postalCode->inventories->pluck('id');
        $searchy     = Product::search($query)
            ->where('is_active', 1)
            ->get();
        $searchyIds = [];

        foreach ($searchy as $s) {
            array_push($searchyIds, $s->id);
        }

        $totalQuery = array_merge($searchyIds, $this->appendCompetitiveProducts($query));
        $products   = [];

        foreach ($totalQuery as $q) {
            $inventoryIds = DB::table('inventory_product')
                ->where('product_id', $q)
                ->whereIn('inventory_id', $inventories)
                ->pluck('inventory_id');

            $product             = Product::find($q);
            if ($product) {
                $product             = $product->toArray();
                $product['stores']   = [];
                $product['quantity'] = 100;

                foreach ($inventoryIds as $i) {
                    $inventory = Inventory::find($i);
                    $store     = Store::where('id', $inventory->store_id)
                        ->where('is_active', 1)
                        ->first();

                    if (! $store) {
                        continue;
                    }

                    $formattedStore = [
                    'type'       => 'stores',
                    'id'         => (string) $store->id,
                    'attributes' => [
                        'title'     => $store->title,
                        'storeLogo' => $store->options['storeLogo'],
                        'subDomain' => $store->sub_domain,
                        'aboutText' => $store->options['aboutText'],
                    ],
                ];

                    array_push($product['stores'], $formattedStore);
                }

                if (! empty($product['stores'])) {
                    array_push($products, $product);
                }
            }
        }

        $formattedProducts = [];

        foreach ($products as $p) {
            $images = [
                [
                    'index' => 0,
                    'image' => $p['image'],
                ],
            ];

            $formattedProduct = [
                'type'       => 'products',
                'id'         => (string) $p['id'],
                'attributes' => [
                    'sku'                     => $p['sku'],
                    'upc'                     => $p['upc'],
                    'isActive'                => $p['is_active'],
                    'title'                   => $p['title'],
                    'longDescription'         => $p['long_description'],
                    'shortDescription'        => $p['short_description'],
                    'retailPrice'             => $p['retail_price'],
                    'markupPercentage'        => $p['markup_percentage'],
                    'markup'                  => $p['markup'],
                    'runnerPrice'             => $p['runner_price'],
                    'images'                  => $images,
                    'imageThumbnail'          => $p['image_thumbnail'],
                    'packaging'               => $p['packaging'],
                    'alcoholContent'          => $p['alcohol_content'],
                    'sugarContent'            => $p['sugar_content'] * 100,
                    'slug'                    => $p['slug'],
                    'categoryId'              => (string) $p['category_id'],
                    'style'                   => $p['style'] ?? null,
                    'producingCountry'        => $p['producing_country'],
                    'producer'                => $p['producer'],
                    'rewardPoints'            => $p['reward_points'] ?? null,
                    'quantity'                => $p['quantity'],
                    'caseDeal'                => $p['case_deal'] ?? null,
                    'incentives'              => $p['incentives'] ?? [],
                    'averageRating'           => $p['average_rating'],
                    'favouriteId'             => isset($p['favouriteId']) ? (string) $p['favouriteId'] : null,
                    'reviewsCount'            => $p['reviews_count'],
                    'stores'                  => $p['stores'],
                ],
            ];

            array_push($formattedProducts, $formattedProduct);
        }

        return $formattedProducts;
    }

    private function appendCompetitiveProducts($query)
    {
        $products;

        if (in_array($query, [
            'beer',
        ])) {
            $products = [
                844,
                8642,
                3282,
                11202,
                2425,
            ];
        } elseif (in_array($query, [
            'lager',
        ])) {
            $products = [
                844,
                8642,
                3283,
                11202,
                2425,
            ];
        } elseif (in_array($query, [
            'light beer',
        ])) {
            $products = [
                844,
                8642,
                3283,
                11202,
                2425,
            ];
        } elseif (in_array($query, [
            'ace hill',
            'labatt max',
            'mill street organic',
            'budweiser',
            'amsterdam',
        ])) {
            $products = [
                11202,
            ];
        } elseif (in_array($query, [
            'wine',
        ])) {
            $products = [
                5177,
                20120,
                7300,
                6139,
                889,
            ];
        } elseif (in_array($query, [
            'red wine',
            'j lohr',
            'j. lohr',
            'josh cellars',
            'casillero del diablo',
        ])) {
            $products = [
                5177,
                20120,
            ];
        } elseif (in_array($query, [
            'cabernet sauvignon',
            'j. lohr seven oaks cabernet sauvignon',
            'j lohr seven oaks cabernet sauvignon',
            'josh cellars cabernet sauvignon',
            'casillero del diablo reserva cabernet sauvignon',
        ])) {
            $products = [
                5177,
            ];
        } elseif (in_array($query, [
            'shiraz',
        ])) {
            $products = [
                20120,
            ];
        } elseif (in_array($query, [
            'stella',
            'corona extra',
            'corona',
            'michelob',
            'modelo'
        ])) {
            $products = [
                3318,
                844
            ];
        } elseif (in_array($query, [
            'belgian moon',
            'shock top',
            'goose island 312',
        ])) {
            $products = [
                4891
            ];
        } elseif (in_array($query, [
            'casillero',
            'casillero del diablo',
            'fantini',
            'mcmanis'
        ])) {
            $products = [
                5177,
                20120
            ];
        } else {
            $products = [];
        }

        return $products;
    }
}
