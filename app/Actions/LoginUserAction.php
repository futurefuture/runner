<?php

namespace App\Actions;

use App\Services\SegmentService;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class LoginUserAction
{
    public function execute($email, $password, array $socialLogin, string $anonymousId)
    {
        if (! $socialLogin) {
            if (Auth::attempt([
                'email' => $email,
                'password' => $password,
            ])) {
                $user        = Auth::user();
                $accessToken = $user
                    ->createToken('Runner')
                    ->accessToken;
                $user->toArray();
                $user['accessToken'] = $accessToken;

                (new SegmentService())->trackLoggedIn($user, $anonymousId);

                return $user;
            } else {
                throw new Exception('invalid credentials');
            }
        } else {
            $user = $this->socialLogin($socialLogin, $email, $anonymousId);

            return $user;
        }
    }

    private function socialLogin($socialLogin, $email, $anonymousId)
    {
        // if ($user) {
        //     throw new Exception('sorry, but we already have an account with this email address');
        // }

        if ($socialLogin['type'] == 'googleId') {
            $user = User::where('google_id', '=', $socialLogin['id'])
                ->first();
        } elseif ($socialLogin['type'] == 'facebookId') {
            $user = User::where('facebook_id', '=', $socialLogin['id'])
                ->first();
        } elseif ($socialLogin['type'] == 'appleId') {
            $user = User::where('apple_id', '=', $socialLogin['id'])
                ->first();
        } else {
            $user = User::where('email', $email)->first();
        }

        if (! $user) {
            return false;
        }

        $accessToken = $user
                ->createToken('Runner')
                ->accessToken;
        $user->toArray();
        $user['accessToken'] = $accessToken;

        (new SegmentService())->trackLoggedIn($user, $anonymousId);

        return $user;
    }
}
