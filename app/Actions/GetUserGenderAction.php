<?php

namespace App\Actions;

use App\Services\GenderizeApi;
use App\User;

class GetUserGenderAction
{
    private $genderize;

    public function __construct()
    {
        $this->genderize = new GenderizeApi();
    }

    public function execute(User $user)
    {
        $gender = $this->getGender($user->first_name);

        $user->gender = $gender;

        $user->save();
    }

    private function getGender($firstName)
    {
        return $this->genderize->getGender($firstName);
    }
}
