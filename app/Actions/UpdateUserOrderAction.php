<?php

namespace App\Actions;

use App\Cart;
use App\Gift;
use App\User;
use App\Order;
use App\Store;
use App\Coupon;
use App\Address;
use App\Mail\OC;
use App\Product;
use App\Inventory;
use Carbon\Carbon;
use App\Invitation;
use App\PostalCode;
use App\StoreLayout;
use App\Events\NewOrder;
use App\Services\SegmentService;
use App\Actions\CreateGiftAction;
use Illuminate\Support\Facades\DB;
use App\Actions\CreateChargeAction;
use App\Services\RewardPointService;
use Illuminate\Support\Facades\Mail;
use App\Services\NotificationService;

class UpdateUserOrderAction
{
    protected $status = 0;
    protected $cart;
    protected $cartContent;
    protected $cardLastFour;
    protected $activityLog;
    protected $adminNotes    = null;
    protected $chargeId;
    protected $courierId     = null;
    protected $device        = 3;
    protected $customer;
    protected $address;
    protected $unitNumber;
    protected $instructions;
    protected $postalCode;
    protected $coupon         = null;
    protected $couponId       = null;
    protected $cogs           = 0;
    protected $discount       = 0;
    protected $itemsTax       = 0;
    protected $cartSubTotal   = 0;
    protected $deliveryFee    = 1000;
    protected $deliveryFeeTax = 130;
    protected $markup         = 0;
    protected $markupTax      = 0;
    protected $serviceFee     = 0;
    protected $serviceFeeTax  = 0;
    protected $totalTax       = 0;
    protected $tip            = 0;
    protected $total          = 0;
    protected $refund         = null;
    protected $signature      = null;
    protected $deliveredAt    = null;
    protected $additionalInfo = null;
    protected $firstTime      = false;
    protected $incentiveId    = null;
    protected $storeId;
    protected $inventoryId;
    protected $appVersion;
    protected $isGift         = false;
    protected $runnerPrice    = 0;
    protected $retailPrice    = 0;
    protected $cartTax        = 0;
    protected $createChargeAction;

    public function __construct()
    {
        $this->createChargeAction = new CreateChargeAction();
        $this->activityLog        = json_encode([(object) [
            'log'  => 'received',
            'time' => \Carbon\Carbon::now(),
        ]]);
    }

    private function getCostOfGoods(array $products): int
    {
        $costOfGoods = 0;

        foreach ($products as $p) {
            $costOfGoods += $p->retailPrice * $p->quantity;
        }

        return (int) $costOfGoods;
    }

    private function setCart(int $userId, int $inventoryId): void
    {
        $this->cart = Cart::where('user_id', $userId)
            ->where('inventory_id', $inventoryId)
            ->first();
    }

    public function execute(array $data, Order $order): Order
    {
        $userId            = $data['userId'];
        $user              = User::find($userId);
        $this->inventoryId = (int) $data['inventoryId'];
        $anonymousId       = $data['anonymousId'];
        $this->device      = (int) $data['device'];
        $inventory         = Inventory::find($this->inventoryId);
        $store             = $inventory->store;
        $this->storeId     = $store->id;
        $this->appVersion  = $data['appVersion'];
        $address           = Address::where('user_id', $userId)
            ->where('selected', 1)
            ->first();

        if ($address) {
            $this->verifyStoreOpen($address, $store);
        }

        $dt  = Carbon::parse($data['orderDateTime'], 'America/Toronto');
        $now = Carbon::now();

        // set order cart
        $this->setCart($userId, $this->inventoryId);

        $this->cartContent    = json_decode($this->cart->content);
        $products             = $this->cartContent->products;
        $this->cogs           = $this->getCostOfGoods($products);
        $this->coupon         = isset($this->cartContent->coupon) ? $this->cartContent->coupon : null;
        $cartSubTotal         = (int) $this->cartContent->subTotal;
        $subTotal             = $cartSubTotal;
        $cartTotal            = (int) $this->cartContent->total;
        $this->deliveryFee    = (int) $this->cartContent->deliveryFee;
        $this->markup         = (int) ($cartSubTotal - $this->cogs);
        $this->markupTax      = (int) ($this->markup * 0.13);
        $this->tip            = (int) $data['tip'];
        $this->serviceFee     = $this->cartContent->serviceFee ?? 0;
        $this->serviceFeeTax  = (int) $this->serviceFee * 0.13;
        $this->totalTax       = (int) $this->markupTax + $this->deliveryFeeTax + $this->serviceFeeTax;
        $this->discount       = (int) $this->cartContent->discount;
        $chargeAmount         = (int) ($cartTotal + $this->tip);
        $this->total          = $chargeAmount;

        if ($this->coupon && $this->coupon->code != 'Reward Points') {
            $this->couponId = Coupon::where('code', $this->coupon->code)
                ->first()
                ->id;
            $coupon     = Coupon::where('code', $this->coupon->code)->first();
            $couponCode = $this->coupon->code;
        } elseif ($this->coupon && $this->coupon->code == 'Reward Points') {
            $couponCode = $this->coupon->code;
        }

        $this->handleProductRewardPoints($this->cartContent->products, $user);

        $order = new Order();

        $this->address = json_encode((object) [
            'address' => $address->formatted_address,
            'lat'     => (float) $address->lat,
            'lng'     => (float) $address->lng,
        ]);
        $this->unitNumber = $address->unit_number;
        $this->postalCode = $address->postal_code;

        if (isset($data['isGift']) && $data['isGift'] == true) {
            $giftData = $data['giftDetails'];

            if (isset($giftData['address'])) {
                $address = (object) [
                    'formatted_address' => $giftData['address']['formattedAddress'],
                    'lat'               => $giftData['address']['lat'],
                    'lng'               => $giftData['address']['lng'],
                    'postal_code'       => $giftData['address']['postalCode'],
                    'instructions'      => isset($giftData['address']['instructions']) ? $giftData['address']['instructions'] : '',
                    'unit_number'       => isset($giftData['address']['unitNumber']) ? $giftData['address']['unitNumber'] : '',
                ];
            }
        }

        $this->customer = json_encode((object) [
            'firstName'   => $user->first_name,
            'lastName'    => $user->last_name,
            'phoneNumber' => $user->phone_number,
            'email'       => $user->email,
        ]);

        $this->firstTime          = $user->orders()->count() ? 0 : 1;
        $this->incentiveId        = isset($this->cartContent->incentive) ? (string) $this->cartContent->incentive->id : null;

        $order->user_id           = $userId;
        $order->inventory_id      = $this->inventoryId;
        $order->store_id          = $this->storeId;
        $order->status            = $dt->diffInMinutes($now) > 180 ? 7 : $this->status;
        $order->schedule_time     = $dt->diffInMinutes($now) > 180 ? $dt->toDateTimeString() : null;
        $order->content           = json_encode($this->cartContent->products);
        $order->activity_log      = $this->activityLog;
        $order->admin_notes       = $this->adminNotes;
        $order->runner_1          = $this->courierId;
        $order->device            = $this->device;
        $order->customer          = $this->customer;
        $order->address           = $this->address;
        $order->postal_code       = $this->postalCode;
        $order->unit_number       = $this->unitNumber;
        $order->instructions      = $this->instructions;
        $order->coupon_id         = $this->couponId;
        $order->subtotal          = $subTotal;
        $order->cogs              = $this->cogs;
        $order->discount          = $this->discount;
        $order->delivery          = $this->deliveryFee;
        $order->delivery_tax      = $this->deliveryFeeTax;
        $order->markup            = $this->markup;
        $order->markup_tax        = $this->markupTax;
        $order->display_tax_total = $this->totalTax;
        $order->tip               = $this->tip;
        $order->total             = $this->total;
        $order->service_fee       = $this->serviceFee;
        $order->incentive_id      = $this->incentiveId;
        $order->first_time        = $this->firstTime;
        $order->app_version       = $this->appVersion;
        $order->address_id        = $address->id ?? null;
        $order->stripe_fee        = (int) ($chargeAmount * 0.023 + 30);
        $order->refund            = $this->refund;
        $charge                   = $this->createChargeAction->execute($order, $user, $data);
        $this->cardLastFour       = $charge->payment_method_details->card->last4;
        $this->chargeId           = $charge->id;
        $order->card_last_four    = $this->cardLastFour;
        $order->charge            = $this->chargeId;
        $order->signature         = $this->signature;
        $order->delivered_at      = $this->deliveredAt;

        $order->save();

        if (isset($data['isGift']) && $data['isGift'] == true) {
            $giftAction = new CreateGiftAction();
            $giftAction->execute($giftData, $order);
        }

        if (isset($data['isGift']) && $data['isGift'] == true) {
            $this->handleGiftOrder($data, $this->address, $order);
        }
        // check for coupon
        if ($this->coupon && $this->coupon->code != 'ADMIN907') {
            $this->updateUserCouponUsedAfterOrder($this->coupon->code, $user, $order);
        }

        $this->rewardReferrerForFirstOrder($user, $order);

        if (env('APP_ENV') === 'production') {
            if ($this->inventoryId == '23' && $userId != 131) {
                Mail::to($user->email)
                ->bcc([
                    'voiseykristen@gmail.com',
                    'kristen@cocktailemporium.com',
                    'service@getrunner.io',
                    'jake@getrunner.io',
                    'jarek@getrunner.io',
                ])
                ->queue(new OC($order));
            } else {
                Mail::to($user->email)
                ->bcc('service@getrunner.io')
                ->queue(new OC($order));
            }
        }

        $notificationService = new NotificationService();

        try {
            $notificationService->orderPlaced($order);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'error' => $e,
                ],
            ], 401);
        }

        // Add 100 reward points to user.
        (new RewardPointService())->addPoints(100, USER::MAKE_PURCHASE, [
            'order_id' => $order->id,
        ], $userId);

        Cart::destroy($this->cart->id);

        event(new NewOrder($order));

        $formattedProduct = [];
        $quantity         = 0;

        foreach ($this->cartContent->products as $product) {
            $partnerIds   = [];
            $orderProduct = [];
            $isVintage    = false;
            $newProduct   = Product::find($product->id);

            // check if product is a vintage
            foreach ($newProduct->tags as $t) {
                if ($t->id === 11) {
                    $isVintage = true;
                }
            }

            foreach ($newProduct->partners as $p) {
                array_push($partnerIds, (string) $p->id);
            }

            $orderProduct['productId']        = $newProduct->id;
            $orderProduct['sku']              = $newProduct->sku;
            $orderProduct['name']             = $newProduct->title;
            $orderProduct['price']            = $newProduct->retail_price;
            $orderProduct['quantity']         = $product->quantity;
            $orderProduct['packaging']        = $newProduct->packaging;
            $orderProduct['producer']         = $newProduct->producer;
            $orderProduct['producingCountry'] = $newProduct->producing_country;
            $orderProduct['category']         = $newProduct->category_id;
            $orderProduct['isVintage']        = $isVintage;
            $orderProduct['partnerIds']       = $partnerIds;
            $quantity += $product->quantity;

            array_push($formattedProduct, $orderProduct);
        }

        (new SegmentService())->trackOrderCompleted($user, $formattedProduct, $order, $store, $quantity, $this->device, $anonymousId, $this->inventoryId);

        return $order;
    }

    private function verifyStoreOpen(Address $address = null, Store $store)
    {
        // return true;
        if ($address) {
            // check if global store is closed
            $globalStoreStatus = DB::table('config')
                ->where('key', 'close_store')
                ->first()->value;
            $postalCode   = PostalCode::find(substr($address->postal_code, 0, 3));
            $territoryId  = $postalCode->territory->id;
            $storeLayout  = StoreLayout::whereIn('territory_id', $territoryId)
                ->where('store_id', $store->id)
                ->first();
            $canTakePayment = $storeLayout->can_take_payment;

            if (! $canTakePayment || $globalStoreStatus) {
                throw new \Exception('sorry, but this store is temporarily closed. please check back later.');
            }
        }
    }

    private function handleProductRewardPoints(array $products, User $user): void
    {
        foreach ($products as $p) {
            $product = Product::find($p->id);
            // reward points
            if ($product->incentives()->where('incentive_id', 7)->pluck('reward_points')->first()) {
                $q = $product->incentives()->where('incentive_id', 7)->first();

                if (Carbon::parse($q->start_date)->isPast() && Carbon::parse($q->end_date)->isFuture()) {
                    $rewardPoints = $q->reward_points;
                } else {
                    $rewardPoints = 0;
                }
            } else {
                $rewardPoints = 0;
            }

            if ($rewardPoints > 0) {
                (new RewardPointService())->addPoints($rewardPoints, USER::PROMO_PURCHASE, [
                    'productId'       => $rewardPoints,
                    'productTitle'    => $product->title,
                    'productPackaing' => $product->packaging,
                ], $user->id);
            }
        }
    }

    private function handleGiftOrder($data, $address, $order)
    {
        foreach ($data['giftDetails'] as $key => $r) {
            $giftDetails[$key] = $r;
        }

        if (array_key_exists('address', $giftDetails)) {
            $address = json_encode((object) [
                'address' => $giftDetails['address']['formattedAddress'],
                'lat'     => $giftDetails['address']['lat'],
                'lng'     => $giftDetails['address']['lng'],
            ]);
            $this->unitNumber   = isset($giftDetails['address']['unitNumber']) ? $giftDetails['address']['unitNumber'] : '';
            $this->instructions = isset($giftDetails['address']['instructions']) ? $giftDetails['address']['instructions'] : '';
        } else {
            $unitNumber   = $this->unitNumber;
            $instructions = $this->instructions;
        }

        Gift::create([
            'order_id'     => $order->id,
            'first_name'   => $giftDetails['firstName'],
            'last_name'    => $giftDetails['lastName'],
            'address'      => $address,
            'unit_number'  => $unitNumber,
            'email'        => $giftDetails['email'],
            'instructions' => $instructions,
            'is_19'        => true,
            'phone_number' => $giftDetails['phoneNumber'],
            'message'      => $giftDetails['personalMessage'],
        ]);

        $giftDetails = [];
    }

    private function updateUserCouponUsedAfterOrder(string $code, User $user, Order $order)
    {
        $coupon = \App\Coupon::where('code', $code)->first();

        if ($coupon) {
            $user->coupons()->attach($coupon->id, [
                'used'     => 1,
                'order_id' => $order->id,
            ]);
        } elseif ($code === 'Reward Points') {
            if ($user->rewardPointsBalance() < 1000) {
                return response()->json([
                    'errors' => [
                        'error' => 'You do not have enough reward points.',
                    ],
                ], 401);
            } else {
                (new RewardPointService())->addPoints(-1000, USER::REWARDS_POINTS_DISCOUNT, [
                    'order_id' => $order->id,
                ], $user->id);
            }
        }
    }

    private function rewardReferrerForFirstOrder(User $user, Order $order): void
    {
        $invitation = Invitation::where('invited_user_id', $user->id)->first();

        if ($invitation && $user->orders()->count() === 1) {
            $referrer = User::find($invitation->user_id);

            (new RewardPointService())->addPoints(1000, USER::REFERRER_AWARDED, [
                'invited_id'    => $user->id,
                'invited_name'  => $user->first_name . ' ' . $user->last_name,
                'invited_email' => $user->email,
            ], $user->id);

            $notificationService = new NotificationService();
            $notificationService->sendReferrerNotification($order, $referrer);
        }
    }
}
