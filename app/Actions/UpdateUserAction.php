<?php

namespace App\Actions;

use App\Classes\Utilities;
use App\User;

class UpdateUserAction
{
    protected $utilites;

    public function __construct(Utilities $utilities)
    {
        $this->utilities = $utilities;
    }

    public function execute(User $user, array $data): User
    {
        $formattedData = $this->utilities->changeCamelCaseToSnakeCase($data);

        // encrypt password if exists
        if (isset($formattedData['password'])) {
            $formattedData['password'] = bcrypt($formattedData['password']);
        }

        $user->update($formattedData);

        return $user;
    }
}
