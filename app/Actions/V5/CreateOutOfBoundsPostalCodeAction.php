<?php

namespace App\Actions\V5;

use App\OutOfBoundsPostalCode;

class CreateOutOfBoundsPostalCodeAction
{
    public function execute(array $data)
    {
        $outOfBoundsPostalCode = OutOfBoundsPostalCode::create([
            'postal_code' => $data['postal_code']
        ]);

        return $outOfBoundsPostalCode;
    }
}
