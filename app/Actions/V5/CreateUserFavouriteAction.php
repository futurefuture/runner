<?php

namespace App\Actions\V5;

use App\User;
use App\Favourite;

class CreateUserFavouriteAction
{
    public function execute(User $user, array $data): Favourite
    {
        $favourite = Favourite::where('user_id', $user->id)
            ->create([
                'user_id'    => $user->id,
                'store_id'   => $data['store_id'],
                'product_id' => $data['product_id'],
            ]);

        return $favourite;
    }
}
