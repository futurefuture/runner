<?php

namespace App\Actions\V5;

use App\Review;
use App\Product;

class CreateProductReviewAction
{
    public function execute(Product $product, array $data): Review
    {
        $review = Review::create([
            'title'       => $data['title'],
            'value'       => $data['value'],
            'product_id'  => $product->id,
            'user_id'     => $data['user_id'],
            'description' => $data['description'],
        ]);

        return $review;
    }
}
