<?php

namespace App\Actions\V5;

use App\SurveyResponse;

class CreateSurveyResponseAction
{
    public function execute(array $data): SurveyResponse
    {
        $surveyResponse = SurveyResponse::create([
            'survey_id'         => $data['survey_id'],
            'survey_option_id'  => $data['survey_option_id'],
            'user_id'           => $data['user_id'],
        ]);

        return $surveyResponse;
    }
}
