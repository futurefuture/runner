<?php

namespace App\Actions\CourierTask;

use App\Task;
use App\User;
use App\Order;
use App\Product;
use App\OrderItem;
use Carbon\Carbon;
use App\Mail\OrderReceived;
use App\Services\StripeService;
use App\Mail\OrderReceivedForRunner;
use Illuminate\Support\Facades\Mail;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Storage;
use App\Actions\OrderItem\CreateOrderItems;

class UpdateCourierTask
{
    public function execute(User $courier, Task $task, array $data)
    {
        if (isset($data['notes'])) {
            $task->notes = $data['notes'];
        }

        if (isset($data['phoneNumber'])) {
            $task->phone_number = $data['phoneNumber'];
        }

        if (isset($data['courierId'])) {
            $task->courier_id = $data['courierId'];
        }

        if (isset($data['containerId'])) {
            $task->container_id = $data['containerId'];
        }

        if (isset($data['name'])) {
            $task->name = $data['name'];
        }

        if (isset($data['organization'])) {
            $task->organization = $data['organization'];
        }

        if (isset($data['addressId'])) {
            $task->address_id = $data['addressId'];
        }

        if (isset($data['type'])) {
            $task->type = $data['type'];
        }

        if (isset($data['notes'])) {
            $note = [
                'time' => Carbon::now(),
                'type' => 'courier',
                'note' => $data['notes']
            ];

            $task->notes = json_encode($note);
        }

        if ($task->state == 1) {
            if ($data['state'] === 2 && $data['orderId']) {
                $order         = Order::find($data['orderId']);
                $order->status = $data['state'];
                $task->state   = $data['state'];

                $task->save();
                $order->save();

                if (env('APP_ENV') === 'production') {
                    $notificationService = new NotificationService();

                    $notificationService->orderOnRoute($order);
                }
            }
        }

        if ($task->state == 2) {
            if ($data['state'] == 3 && $data['orderId'] && ($data['signature'] || $data['imageUrl'])) {
                if (isset($data['signature'])) {
                    $signature = $data['signature'];

                    $storagePath = Storage::disk('runner-task-signatures')
                            ->put($task->id . '.png', base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $signature)), 'public');

                    $signatureUrl    = Storage::disk('runner-task-signatures')->url($task->id . '.png');
                    $task->signature = $signatureUrl;
                }

                if (isset($data['imageUrl'])) {
                    $image = $data['imageUrl'];

                    $storagePath = Storage::disk('runner-courier-task-images')
                            ->put($task->id . '.png', base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image)), 'public');

                    $imageUrl        = Storage::disk('runner-courier-task-images')->url($task->id . '.png');
                    $task->image_url = $imageUrl;
                }

                $now                 = Carbon::now()->toDateTimeString();
                $order               = Order::find($data['orderId']);
                $action              = new CreateOrderItems($order);
                $action->execute();

                $order->status       = $data['state'];
                $order->delivered_at = $now;
                $order->signature    = $task->signature;
                $order->save();

                $task->state       = $data['state'];

                try {
                    $stripeService = resolve(StripeService::class);
                    $charge        = $stripeService->captureCharge($order);
                } catch (\Stripe\Error\Card $e) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];

                    return response()->json([
                    'errors' => [
                        'error' => $err['message'],
                    ],
                ], 400);
                } catch (\Stripe\Error\ApiConnection $e) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];

                    return response()->json([
                        'errors' => [
                            'error' => $err['message'],
                        ],
                    ], 404);
                }

                $task->save();

                if (env('APP_ENV') === 'production') {
                    $order->customer     = json_decode($order->customer);
                    $runner              = User::find($order->runner_1);
                    $notificationService = new NotificationService();

                    $notificationService->orderDelivered($order);

                    Mail::to($order->customer->email)
                        ->bcc('service@getrunner.io')
                        ->queue(new OrderReceived($order));
                    Mail::to($runner->email)
                        ->queue(new OrderReceivedForRunner($order));
                }
            }
        }

        return $task;
    }
}
