<?php

namespace App\Actions;

use App\OutOfBoundsPostalCode;

class CreateOutOfBoundsPostalCodeAction
{
    public function execute(array $data)
    {
        $outOfBoundsPostalCode = OutOfBoundsPostalCode::create([
            'postal_code' => $data['postalCode']
        ]);

        return $outOfBoundsPostalCode;
    }
}
