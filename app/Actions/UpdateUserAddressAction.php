<?php

namespace App\Actions;

use App\User;
use App\Address;
use App\Classes\Utilities;

class UpdateUserAddressAction
{
    public function execute(User $user, Address $address, array $data): Address
    {
        if (isset($data['selected'])) {
            // unselect other addresses
            $otherAddresses = Address::where('user_id', $user->id)
                ->where('id', '!=', $address->id)
                ->get();

            if ($otherAddresses) {
                foreach ($otherAddresses as $a) {
                    $a->selected = 0;
                    $a->update();
                }
            }
        }

        $utilities     = new Utilities();
        $formattedData = $utilities->changeCamelCaseToSnakeCase($data);

        $address->update($formattedData);

        return $address;
    }
}
