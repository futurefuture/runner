<?php

namespace App\Actions;

use App\User;
use App\Order;
use App\Services\StripeService;

class CreateChargeAction
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function execute(Order $order, User $user, array $data)
    {
        $stripeService = resolve(StripeService::class);
        $charge        = $stripeService->createCharge($order, $user, $data);

        return $charge;
    }
}
