<?php

namespace App\Actions;

use App\PostalCode;
use App\Store;

class GetStoresValidatedByPostalCodeAction
{
    protected $stores = [];

    public function execute(string $fullPostalCode, bool $isWeb)
    {
        $postalCode = PostalCode::where('postal_code', substr($fullPostalCode, 0, 3))->with([
            'territory',
            'territory.inventories'
        ])->first();

        if (! $postalCode) {
            // add runner store even if postal code doesn't exist
            if ($isWeb) {
                $this->appendRunnerStore();
            }

            return $this->stores;
        }

        foreach ($postalCode->territory->inventories as $i) {
            $store = Store::where('id', $i->store->id)
                ->where('is_active', 1)
                ->first();

            if ($store) {
                $store                 = $store->toArray();
                $store['inventory_id'] = $i->id;

                array_push($this->stores, $store);
            }
        }

        $this->appendRunnerStore();

        return $this->stores;
    }

    private function appendRunnerStore()
    {
        $runner = Store::find(8);

        array_push($this->stores, $runner);
    }
}
