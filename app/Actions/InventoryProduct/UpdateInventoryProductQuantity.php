<?php

namespace App\Actions\InventoryProduct;

use App\Product;
use App\Inventory;
use App\InventoryProduct;

class UpdateInventoryProductQuantity
{
    public function execute(Product $product, Inventory $inventory, int $quantity): InventoryProduct
    {
        $inventoryProduct = InventoryProduct::where('inventory_id', $inventory->id)
            ->where('product_id', $product->id)->first();

        $newQuantity = $inventoryProduct->quantity + $quantity;

        $inventoryProduct->update([
            'quantity' => $newQuantity
        ]);

        return $inventoryProduct;
    }
}
