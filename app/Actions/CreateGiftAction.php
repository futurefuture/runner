<?php

namespace App\Actions;

use App\Gift;
use App\Order;

class CreateGiftAction
{
    public function execute(array $data, Order $order): Gift
    {
        $gift = Gift::create([
            'order_id'     => $order->id,
            'first_name'   => $data['firstName'],
            'last_name'    => $data['lastName'],
            'address'      => 'test',
            'unit_number'  => 'test',
            'email'        => $data['email'],
            'instructions' => 'test',
            'is_19'        => true,
            'phone_number' => $data['phoneNumber'],
            'message'      => $data['personalMessage'],
        ]);

        return $gift;
    }
}
