<?php

namespace App\Actions;

use App\User;
use App\Services\StripeService;

class UpdateCustomerSourceAction
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function execute(User $user, string $sourceId)
    {
        $stripeService = resolve(StripeService::class);
        $source        = $stripeService->updateDefaultSource($user, $sourceId);

        return $source;
    }
}
