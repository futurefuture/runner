<?php

namespace App\Actions;

use App\User;
use App\Order;
use App\Services\StripeService;

class CreateRefundAction
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function execute(Order $order, int $amount)
    {
        $stripeService = resolve(StripeService::class);
        $refund        = $stripeService->createRefund($order, $amount);

        return $refund;
    }
}
