<?php

namespace App\Actions\UserAddress;

use App\User;
use App\Address;

class CreateUserAddress
{
    public function execute(User $user, array $data)
    {
        if ($user->addresses) {
            foreach ($user->addresses as $address) {
                $address->selected = 0;

                $address->update();
            }
        }

        $address                    = new Address();
        $address->user_id           = $user->id;
        $address->formatted_address = $data['formattedAddress'];
        $address->place_id          = $data['placeId'] ?? null;
        $address->lat               = $data['lat'];
        $address->lng               = $data['lng'];
        $address->postal_code       = $data['postalCode'];
        $address->unit_number       = $data['unitNumber'] ?? null;
        $address->instructions      = $data['instructions'] ?? null;
        $address->business_name     = $data['businessName'] ?? null;
        $address->selected          = 1;

        $address->save();

        $selectedAddress = Address::where('user_id', $user->id)
            ->where('selected', 1)
            ->first();

        return $selectedAddress;
    }
}
