<?php

namespace App\Actions\UserAddress\V5;

use App\User;
use App\Address;

class CreateUserAddress
{
    public function execute(User $user, array $data)
    {
        if ($user->addresses) {
            foreach ($user->addresses as $address) {
                $address->selected = 0;

                $address->update();
            }
        }

        $address                    = new Address();
        $address->user_id           = $user->id;
        $address->formatted_address = $data['formatted_address'];
        $address->place_id          = $data['place_id'] ?? null;
        $address->lat               = $data['lat'];
        $address->lng               = $data['lng'];
        $address->postal_code       = $data['postal_code'];
        $address->unit_number       = $data['unit_number'] ?? null;
        $address->instructions      = $data['instructions'] ?? null;
        $address->business_name     = $data['business_name'] ?? null;
        $address->selected          = 1;

        $address->save();

        $selectedAddress = Address::where('user_id', $user->id)
            ->where('selected', 1)
            ->first();

        return $selectedAddress;
    }
}
