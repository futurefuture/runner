<?php

namespace App\Actions;

use App\User;
use App\Favourite;

class CreateUserFavouriteAction
{
    public function execute(User $user, array $data): Favourite
    {
        $favourite = Favourite::where('user_id', $user->id)
            ->create([
                'user_id'    => $user->id,
                'store_id'   => $data['storeId'],
                'product_id' => $data['productId'],
            ]);

        return $favourite;
    }
}
