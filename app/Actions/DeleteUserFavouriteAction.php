<?php

namespace App\Actions;

use App\Favourite;

class DeleteUserFavouriteAction
{
    public function execute(Favourite $favourite): void
    {
        $favourite->delete();
    }
}
