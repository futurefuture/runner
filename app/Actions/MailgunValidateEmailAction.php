<?php

namespace App\Actions;

use Exception;
use Mailgun\Mailgun;

class MailgunValidateEmailAction
{
    private $email;
    private $blackList = [
        'qq.com',
        'huawei.com',
    ];

    public function __construct()
    {
        $apiKey        = 'pubkey-a7cb356834d5b11259e412e6c37c1824';
        $this->mailgun = Mailgun::create($apiKey);
    }

    public function execute($email)
    {
        $this->email = $email;

        if (! $this->validateAgainstMailgun($this->email) || ! $this->validateAgainstBlackList($this->email)) {
            throw new Exception('email is blacklisted');
        } else {
            return false;
        }
    }

    private function validateAgainstMailgun($email)
    {
        return true;

        // $response = $this->mailgun->emailValidation()->validate($email);

        // if (! $response->isValid) {
        //     return false;
        // } else {
        //     return true;
        // }
    }

    private function validateAgainstBlackList($email)
    {
        $parts  = explode('@', $email);
        $domain = array_pop($parts);

        if (in_array($domain, $this->blackList)) {
            return false;
        } else {
            return true;
        }
    }
}
