<?php

namespace App\Actions;

use App\User;
use App\Address;
use Illuminate\Database\Eloquent\Collection;

class DeleteUserAddressAction
{
    public function execute(User $user, Address $address): Collection
    {
        $address->delete();

        // grab all remaining addresses
        $addresses = Address::where('user_id', $user->id)->get();

        if ($addresses) {
            foreach ($addresses as $address) {
                $address->selected = 0;
                $address->update();
            }

            $newSelected           = count($addresses) > 0 ? $addresses[0] : $address;
            $newSelected->selected = 1;
            $newSelected->update();
        }

        return $addresses;
    }
}
