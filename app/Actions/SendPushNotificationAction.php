<?php

namespace App\Actions;

use App\Services\FCMService;

class SendPushNotificationAction
{
    /**
     * @param mixed $users
     */
    public function execute(string $application, $users)
    {
        $fcmService = resolve(FCMService::class);

        $response = $fcmService->sendPushNotification($application, $users);

        return $response;
    }
}
