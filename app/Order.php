<?php

namespace App;

use App\Address;
use App\Gift;
use App\Inventory;
use App\Store;
use App\Task;
use App\User;
use App\Utilities\Filters\QueryFilter;
use App\Classes\Utilities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    const IS_RECEIVED      = 0;
    const IS_IN_PROCESSING = 1;
    const IS_ON_ROUTE      = 2;
    const IS_DELIVERED     = 3;
    const IS_CANCELLED     = 5;
    const IS_SCHEDULED     = 7;

    // device constant
    const IS_IOS     = 3;
    const IS_MOBILE  = 2;
    const IS_DESKTOP = 1;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        // 'customer' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'content',
        'status',
        'card_last_four',
        'card_type',
        'activity_log',
        'admin_notes',
        'charge',
        'runner_1',
        'runner_2',
        'device',
        'subtotal',
        'cogs',
        'discount',
        'delivery',
        'delivery_tax',
        'markup',
        'markup_tax',
        'display_tax_total',
        'tip',
        'total',
        'customer',
        'address',
        'unit_number',
        'instructions',
        'coupon',
        'signature',
        'schedule_time',
        'additional_info',
        'first_time',
        'stripe_fee',
        'inventory_id',
        'service_fee',
        'incentive_id',
        'has_rush_products',
        'store_id',
        'app_version',
        'address_id',
        'postal_code',
        'refund'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function courier()
    {
        return $this->belongsTo(User::class, 'runner_1', 'id');
    }

    public function addressObject()
    {
        return $this->belongsTo(Address::class, 'address_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function gift()
    {
        return $this->hasOne(Gift::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function task()
    {
        return $this->hasOne(Task::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }
}
