<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SegmentExport implements FromCollection, ShouldAutoSize
{
    protected $newArray;

    public function __construct($newArray)
    {
        $this->newArray = collect($newArray);
    }

    public function collection()
    {
        return $this->newArray;
    }
}
