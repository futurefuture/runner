<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrderDiscrepanciesReportExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $orderDiscrepancies;

    public function __construct($orderDiscrepancies)
    {
        $this->orderDiscrepancies = $orderDiscrepancies;
    }

    public function view(): View
    {
        return view('exports.order_discrepancies_export', [
            'orderDiscrepancies' => $this->orderDiscrepancies,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'C' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
