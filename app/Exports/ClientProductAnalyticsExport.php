<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ClientProductAnalyticsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $productData;

    public function __construct($productData)
    {
        $this->productData = collect($productData);
    }

    public function headings(): array
    {
        return [
            'Product ID',
            'Time Period',
            'Average Session Duration',
            'Bounce Rate',
            'Bounces',
            'Page Views',
            'Add To Cards',
            'Sessions',
            'Unique Page Views',
            'Total Sold',
        ];
    }

    public function collection()
    {
        return $this->productData;
    }
}
