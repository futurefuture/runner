<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrderItemDiscrepanciesReportExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $orderItemDiscrepancies;

    public function __construct($orderItemDiscrepancies)
    {
        $this->orderItemDiscrepancies = $orderItemDiscrepancies;
    }

    public function view(): View
    {
        return view('exports.order_item_discrepancies_export', [
            'orderItemDiscrepancies' => $this->orderItemDiscrepancies,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [];
    }
}
