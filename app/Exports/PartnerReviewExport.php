<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PartnerReviewExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $reviews;

    public function __construct($reviews)
    {
        $this->reviews = $reviews;
    }

    public function view(): View
    {
        return view('exports.review-list-export', [
            'reviews' => $this->reviews,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [];
    }
}
