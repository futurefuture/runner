<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PartnerCategoryListExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $productsWithTotalSold;

    public function __construct($categoryWithTotalSold)
    {
        $this->categoryWithTotalSold = $categoryWithTotalSold;
    }

    public function view(): View
    {
        return view('exports.category-list-export', [
            'categoryWithTotalSold' => $this->categoryWithTotalSold,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'C' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
