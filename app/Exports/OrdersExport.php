<?php

namespace App\Exports;

use App\Order;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class OrdersExport implements FromCollection, WithHeadings, WithColumnFormatting, WithChunkReading
{
    use Exportable;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    public function headings(): array
    {
        return [
            'Type',
            'ContactName',
            'InvoiceDate',
            'DueDate',
            'InvoiceNumber',
            'Description',
            'Quantity',
            'UnitAmount',
            'Discount',
            'AccountCode',
            'TaxType',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'I' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function collection()
    {
        $orders = Order::whereBetween('created_at', [
            $this->startDate,
            $this->endDate,
        ])
            ->select([
                'id',
                'status',
                'charge',
                'subtotal',
                'cogs',
                'discount',
                'delivery',
                'delivery_tax',
                'markup',
                'markup_tax',
                'display_tax_total',
                'tip',
                'total',
                'stripe_fee',
                'service_fee',
                'created_at',
                'updated_at',
            ])
            ->get();

        $orders->map(function ($order) {
            $order->subtotal = $order->subtotal / 100;
            $order->cogs = $order->cogs / 100;
            $order->discount = $order->discount / 100;
            $order->delivery = $order->delivery / 100;
            $order->delivery_tax = $order->delivery_tax / 100;
            $order->markup = $order->markup / 100;
            $order->markup_tax = $order->markup_tax / 100;
            $order->display_tax_total = $order->display_tax_total / 100;
            $order->tip = $order->tip / 100;
            $order->total = $order->total / 100;
            $order->stripe_fee = $order->stripe_fee / 100;
            $order->service_fee = $order->service_fee / 100;

            return $order;
        });

        $formattedCollection = [];

        foreach ($orders as $o) {
            $sales = [
                'type'          => 'Ventas de alcohol',
                'contactName'   => 'FutureFuture Inc.',
                'invoiceDate'   => Carbon::parse($o->created_at)->toDateString(),
                'dueDate'       => Carbon::parse($o->created_at)->toDateString(),
                'invoiceNumber' => $o->id,
                'description'   => $o->charge,
                'quantity'      => 1,
                'unitAmount'    => $o->subtotal,
                'discount'      => $o->discount ?? null,
                'accountCode'   => 277,
                'taxType'       => 'Tax Exempt (0%)',
            ];
            $delivery = [
                'type'          => 'Domicilios',
                'contactName'   => 'FutureFuture Inc.',
                'invoiceDate'   => Carbon::parse($o->created_at)->toDateString(),
                'dueDate'       => Carbon::parse($o->created_at)->toDateString(),
                'invoiceNumber' => $o->id,
                'description'   => $o->charge,
                'quantity'      => 1,
                'unitAmount'    => $o->delivery * 1.13,
                'discount'      => null,
                'accountCode'   => 275,
                'taxType'       => 'Tax Exempt (0%)',
            ];
            $tips = [
                'type'          => 'Propina',
                'contactName'   => 'FutureFuture Inc.',
                'invoiceDate'   => Carbon::parse($o->created_at)->toDateString(),
                'dueDate'       => Carbon::parse($o->created_at)->toDateString(),
                'invoiceNumber' => $o->id,
                'description'   => $o->charge,
                'quantity'      => 1,
                'unitAmount'    => $o->tip,
                'discount'      => null,
                'accountCode'   => 276,
                'taxType'       => 'Tax Exempt (0%)',
            ];
            $stripeFee = [
                'type'          => 'compras de tajerta',
                'contactName'   => 'FutureFuture Inc.',
                'invoiceDate'   => Carbon::parse($o->created_at)->toDateString(),
                'dueDate'       => Carbon::parse($o->created_at)->toDateString(),
                'invoiceNumber' => $o->id,
                'description'   => $o->charge,
                'quantity'      => 1,
                'unitAmount'    => $o->stripe_fee,
                'discount'      => null,
                'accountCode'   => 575,
                'taxType'       => 'Tax Exempt (0%)',
            ];

            array_push($formattedCollection, $sales, $delivery, $tips, $stripeFee);
        }

        return collect($formattedCollection);
    }
}
