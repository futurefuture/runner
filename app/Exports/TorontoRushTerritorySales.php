<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TorontoRushTerritorySales implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $torontoRushSales;

    public function __construct($torontoRushSales)
    {
        $this->torontoRushSales = $torontoRushSales;
    }

    public function view(): View
    {
        return view('exports.torronto_rush_territory_sales_export', [
            'torontoRushSales' => $this->torontoRushSales,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
