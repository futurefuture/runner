<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class TorontoGiftTerritoryCustomers implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $torontoTerritoryCustomer;

    public function __construct($torontoTerritoryCustomer)
    {
        $this->torontoTerritoryCustomer = $torontoTerritoryCustomer;
    }

    public function view(): View
    {
        return view('exports.torronto_gift_territory_customer_export', [
            'torontoTerritoryCustomer' => $this->torontoTerritoryCustomer,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [];
    }
}
