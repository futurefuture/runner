<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class CourierHours implements FromArray
{
    protected $schedules;

    public function __construct(array $schedules)
    {
        $this->schedules = $schedules;
    }

    public function array(): array
    {
        return $this->schedules;
    }
}
