<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PartnerSalesExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $productsWithTotalSold;

    public function __construct($productsWithTotalSold)
    {
        $this->productsWithTotalSold = collect($productsWithTotalSold);
    }

    public function view(): View
    {
        return view('exports.products-sales-export', [
            'productsWithTotalSold' => $this->productsWithTotalSold,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
