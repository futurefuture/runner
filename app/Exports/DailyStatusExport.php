<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DailyStatusExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $torontoSouthSales;

    public function __construct($formattedRunners, $finances, $date)
    {
        $this->scheduledRunners = $formattedRunners;
        $this->date             = $date;
        $this->finances         = $finances;
    }

    public function view(): View
    {
        return view('exports.daily_status_export', [
            'date'                      => $this->date,
            'scheduledRunners'          => $this->scheduledRunners,
            'finances'                  => $this->finances,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
