<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PartnerProductListExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $productsWithTotalSold;

    public function __construct($productsWithTotalSold)
    {
        $this->productsWithTotalSold = $productsWithTotalSold;
    }

    public function view(): View
    {
        return view('exports.products-list-export', [
            'productsWithTotalSold' => $this->productsWithTotalSold,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            // 'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
