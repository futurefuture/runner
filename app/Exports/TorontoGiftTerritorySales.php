<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TorontoGiftTerritorySales implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $torontoRushSales;

    public function __construct($torontoGiftSales)
    {
        $this->torontoGiftSales = $torontoGiftSales;
    }

    public function view(): View
    {
        return view('exports.torronto_gift_territory_sales_export', [
            'torontoGiftSales' => $this->torontoGiftSales,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
