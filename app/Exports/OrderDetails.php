<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrderDetails implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $orders;

    public function __construct($orders)
    {
        $this->orders = $orders;
    }

    public function view(): View
    {
        return view('exports.orderDetails', [
            'orders' => $this->orders,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [];
    }
}
