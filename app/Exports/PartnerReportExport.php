<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PartnerReportExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $partnerSalesData;

    protected $timeRange;

    public function __construct($partnerSalesData)
    {
        $this->partnerSalesByPriceBand = collect($partnerSalesData['partnerSalesByPriceBand']);
        $this->timeRange = $partnerSalesData['timeRange'];
    }

    public function view(): View
    {
        return view('exports.report-data', [
            'timeRange'               => $this->timeRange,
            'partnerSalesByPriceBand' => $this->partnerSalesByPriceBand,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
