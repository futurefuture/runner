<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TorontoLCBOTerritorySales implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $torontoSouthSales;

    public function __construct($torontoSouthSales)
    {
        $this->torontoSouthSales = $torontoSouthSales;
    }

    public function view(): View
    {
        return view('exports.torronto_territory_sales_export', [
            'torontoSouthSales' => $this->torontoSouthSales,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
