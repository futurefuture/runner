<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ProductUpdateExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $orderItemDiscrepancies;

    public function __construct($productUpdate)
    {
        $this->productUpdate = $productUpdate;
    }

    public function view(): View
    {
        return view('exports.product_update_export', [
            'productUpdate' => $this->productUpdate,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [];
    }
}
