<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class RunnerReportExport implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $runnerReport;

    public function __construct($runnerReport)
    {
        $this->runnerReport = $runnerReport;
    }

    public function view(): View
    {
        return view('exports.runner_report_export', [
            'runnerReport' =>  $this->runnerReport,
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }
}
