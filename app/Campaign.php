<?php

namespace App;

use App\Partner;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'campaigns';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }
}
