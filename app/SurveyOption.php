<?php

namespace App;

use App\Survey;
use Illuminate\Database\Eloquent\Model;

class SurveyOption extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id',
        'title',
    ];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
