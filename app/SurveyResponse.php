<?php

namespace App;

use App\Survey;
use App\SurveyOption;
use App\User;
use Illuminate\Database\Eloquent\Model;

class SurveyResponse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_responses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id',
        'survey_option_id',
        'user_id',
    ];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
    public function surveyOption()
    {
        return $this->belongsTo(SurveyOption::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
