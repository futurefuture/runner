<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutOfBoundsPostalCode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'out_of_bounds_postal_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code',
    ];
}
