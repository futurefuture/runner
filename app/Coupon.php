<?php

namespace App;

use App\User;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'code',
        'active_until',
        'is_active',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'active_until',
    ];

    public function users()
    {
        return $this
            ->belongsToMany(User::class)
            ->withPivot('used')
            ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
