<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Territory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'territories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    public function postal_codes()
    {
        return $this->hasMany(PostalCode::class);
    }

    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'inventory_territory', 'territory_id', 'inventory_id');
    }
}
