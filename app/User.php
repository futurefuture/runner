<?php

namespace App;

use App\Favourite;
use App\Services\AddressService;
use App\UserNote;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    /** Reward point constant  */
    const REGISTER                = 0;
    const REGISTER_REFERRER       = 1;
    const MAKE_PURCHASE           = 2;
    const CANCEL_PURCHASE         = 3;
    const REWARDS_POINTS_DISCOUNT = 4;
    const REFERRER_AWARDED        = 5;
    const REWARD_DISPATCH         = 6;
    const REVIEW_APPROVED         = 7;
    const PROMO_PURCHASE          = 8;

    use HasApiTokens, Notifiable, SoftDeletes;

    protected $dispatchesEvents = [
        'deleted' => \App\Events\UserDeleted::class,
        'created' => \App\Events\UserCreated::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'date_of_birth',
        'phone_number',
        'email',
        'password',
        'stripe_id',
        'address',
        'is_19',
        'facebook_id',
        'google_id',
        'apple_id',
        'invite_code',
        'unit',
        'fcm_token',
        'address',
        'age_range',
        'gender',
        'avatar_image',
        'promo_notification',
        'cart_notification',
        'order_notification',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Gets the roles associated with the user.
     *
     * @return array
     */
    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);
    }

    public function orders()
    {
        return $this->hasMany(\App\Order::class);
    }

    public function carts()
    {
        return $this->hasMany(\App\Cart::class);
    }

    public function coupons()
    {
        return $this
            ->belongsToMany(\App\Coupon::class)
            ->withPivot('used')
            ->withTimestamps();
    }

    public function favourites()
    {
        return $this->hasMany(Favourite::class);
    }

    public function favourite_products()
    {
        return $this->belongsToMany(\App\Product::class, 'favourites', 'user_id', 'product_id');
    }

    public function getFavouritesAttribute()
    {
        $favourite_products = $this->favourite_products()->get();

        foreach ($favourite_products as $f) {
            $f->type = 'product';
        }

        return $favourite_products;
    }

    public function runnerlocations()
    {
        return $this->hasMany(\App\RunnerLocation::class, 'runner_id');
    }

    public function invites()
    {
        return $this->hasMany(\App\Invitation::class, 'user_id');
    }

    public function addresses()
    {
        return $this->hasMany(\App\Address::class);
    }

    public function notes()
    {
        return $this->hasMany(UserNote::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(\App\Tag::class);
    }

    public function schedules()
    {
        return $this->hasMany(\App\Schedule::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(\App\Review::class, 'user_id');
    }

    public function couponsUsed()
    {
        return $this->belongsToMany(Coupon::class, 'coupon_user', 'user_id', 'coupon_id');
    }

    // public function getAddressAttribute($value)
    // {
    //     $address = $this->addresses()->where('selected', true)->first();

    //     if ($address) {
    //         return json_decode($address->address);
    //     } else {
    //         return;
    //     }
    // }

    // public function getUnitAttribute($value)
    // {
    //     $address = $this->addresses()->where('selected', true)->first();

    //     if ($address) {
    //         return $address->unit;
    //     } else {
    //         return;
    //     }
    // }

    public function getInstructionsAttribute($value)
    {
        $address = $this->addresses()->where('selected', true)->first();

        if ($address) {
            return $address->instructions;
        } else {
            return;
        }
    }

    /**
     * One to many relationship between user and rewards.
     */
    public function rewardsPoints()
    {
        return $this->hasMany(\App\RewardPoint::class, 'user_id');
    }

    /**
     * Creates a reward points transaction for the user.
     *
     * @param int $points
     * @param int $type
     * @param array $values
     * @return void
     */
    public function rewardPointsTransaction($points, $type, $values = null): void
    {
        $this->rewardsPoints()->create([
            'value'   => $points,
            'type'    => $type,
            'details' => json_encode($values),
        ]);
    }

    public function rewardPointsBalance()
    {
        $rewardPoints        = RewardPoint::where('user_id', $this->id)->get();
        $rewardPointsBalance = 0;

        foreach ($rewardPoints as $rewardPoint) {
            $rewardPointsBalance += $rewardPoint->value;
        }

        return $rewardPointsBalance;
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
