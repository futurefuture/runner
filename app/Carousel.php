<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carousels';

    public function carouselComponents()
    {
        return $this->belongsToMany(CarouselComponent::class)
            ->withPivot('index')
            ->where('index', '!=', 300);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }
}
