<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarouselComponent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carousel_components';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];

    public function carousels()
    {
        return $this->belongsToMany(Carousel::class)->withPivot('index');
    }

    public function ads()
    {
        return $this->hasMany(Ad::class)->active();
    }
}
