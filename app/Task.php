<?php

namespace App;

use App\Container;
use App\Order;
use App\User;
use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // States
    // 0 - Unassigned
    // 1 - Assigned
    // 2 - Active
    // 3 - Completed
    const IS_ASSIGNED = 1;

    const IS_COMPLETED = 3;

    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'index',
        'notes',
        'phone_number',
        'name',
        'organization',
        'address_id',
        'order_id',
        'container_id',
        'type',
        'courier_id',
        'signature',
    ];

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function courier()
    {
        return $this->belongsTo(User::class, 'courier_id');
    }

    public function container()
    {
        return $this->belongsTo(Container::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    // public function schedule()
    // {
    //     return $this->belongsToManySchedule::where('user_id', $this->courier->id)->latest('created_at')->first();
    // }
}
