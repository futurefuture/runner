<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partner_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'partner_id',
        'title',
    ];
}
