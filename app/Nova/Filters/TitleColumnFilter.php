<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use philperusse\Filters\ColumnFilter as Filter;

class TitleColumnFilter extends Filter
{
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $args = collect($value)->values()->filter();

        if ($args->isEmpty()) {
            return $query;
        }

        if ($args[1] == 'LIKE') {
            $args[2] = '%'.$args[2].'%';
        }

        return $query->where(...$args->all());
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request) : array
    {
        return array_merge(parent::options($request), [
            'columns' => [
                'products.title' => 'Title',
            ],
            'operators' => [
                'LIKE' => 'LIKE',
             ],
        ]);
    }
}
