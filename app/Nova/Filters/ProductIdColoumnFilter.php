<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use philperusse\Filters\ColumnFilter as Filter;

class ProductIdColoumnFilter extends Filter
{
    public $name = 'Product ID';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $args = collect($value)->values()->filter();

        if ($args->isEmpty()) {
            return $query;
        }

        return $query->where(...$args->all());
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request): array
    {
        return array_merge(parent::options($request), [
            'columns' => [
                'products.id' => 'ID',
            ],

        ]);
    }
}
