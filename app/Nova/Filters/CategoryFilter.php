<?php

namespace App\Nova\Filters;

use DB;
use Illuminate\Http\Request;
use philperusse\Filters\ColumnFilter as Filter;

class CategoryFilter extends Filter
{
    public $name = 'Category ID';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $args = collect($value)->values()->filter();

        if ($args->isEmpty()) {
            return $query;
        }

        $categories        = DB::select('select c1.id as child_id from categories c1 left join categories c2 on c2.id = c1.parent_id left join categories c3 on c3.id = c2.parent_id left join categories c4 on c4.id = c3.parent_id where ' . $value['data'] . ' in (c1.parent_id, c2.parent_id, c3.parent_id) union select id from categories where id =' . $value['data']);
        $formattedCategory = [];

        foreach ($categories as $category) {
            array_push($formattedCategory, $category->child_id);
        }

        return $query->whereIn('categories.id', $formattedCategory);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request): array
    {
        return array_merge(parent::options($request), [
            'columns' => [
                'categories.id' => 'ID',
            ],
        ]);
    }
}
