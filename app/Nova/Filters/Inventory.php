<?php

namespace App\Nova\Filters;

use App\Inventory as InventoryModel;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class Inventory extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('inventory_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $inventories          = InventoryModel::all();
        $formattedInventories = [];

        foreach ($inventories as $i) {
            $formattedInventories[$i->title] = $i->id;
        }

        // dd($formattedInventories);

        return $formattedInventories;
    }
}
