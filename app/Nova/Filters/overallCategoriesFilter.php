<?php

namespace App\Nova\Filters;

use DB;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class overallCategoriesFilter extends Filter
{
    public $name = 'Category ID';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $categories = DB::select('select c1.id as child_id from categories c1 left join categories c2 on c2.id = c1.parent_id left join categories c3 on c3.id = c2.parent_id left join categories c4 on c4.id = c3.parent_id where '.$value.' in (c1.parent_id, c2.parent_id, c3.parent_id) union select id from categories where id ='.$value);

        $formattedCategory = [];
        foreach ($categories as $category) {
            array_push($formattedCategory, $category->child_id);
        }

        return $query->whereIn('category_id', $formattedCategory);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Wine' => '4',
            'Beer & Cider' => '2',
            'Siprits' => '3',
            'Coolers' => '5',
        ];
    }
}
