<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Chaseconey\ExternalImage\ExternalImage;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Order::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['address'];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('User')->searchable(),
            Number::make('Status'),
            Code::make('Content')->hideFromIndex(),
            Text::make('Stripe Charge ID', 'charge')->hideFromIndex(),
            BelongsTo::make('Courier', 'courier', \App\Nova\User::class)
                ->nullable()->searchable(),
            Text::make('Device'),
            BelongsTo::make('Address'),
            Text::make('Instructions', 'instructions')->hideFromIndex(),
            Text::make('Unit Number', 'unit_number')->hideFromIndex(),
            Number::make('Subtotal')
                ->hideFromIndex(),
            Number::make('Discount')
                ->hideFromIndex(),
            BelongsTo::make('Coupon')
                ->hideFromIndex(),
            Number::make('Delivery')
                ->hideFromIndex(),
            Number::make('Markup')
                ->hideFromIndex(),
            Number::make('Tip')
                ->hideFromIndex(),
            Number::make('Total')
                ->hideFromIndex(),
            Number::make('Refund')
                ->hideFromIndex(),
            Number::make('Stripe Fee')
                ->hideFromIndex(),
            Number::make('Cost Of Goods', 'cogs')
                ->hideFromIndex(),
            Text::make('Delivered At', 'delivered_at')
                ->hideFromIndex(),
            Text::make('Scheduled Time', 'schedule_time')
                ->hideFromIndex(),
            Boolean::make('First Time', 'first_time')
                ->hideFromIndex(),
            ExternalImage::make('Signature')
                ->hideFromIndex(),
            BelongsTo::make('Inventory'),
            BelongsTo::make('Store'),
            HasOne::make('Task'),
            HasMany::make('Order Items', 'orderItems'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [new Filters\OrderIdFilter];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
            new Lenses\OrderDiscrepancy(),
            new Lenses\CourierTips(),
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new \App\Nova\Actions\UpdateOrderStatus,
            new \App\Nova\Actions\SendOrderReceipt,
            new \App\Nova\Actions\UpdateOrderItems,
            (new DownloadExcel)->withHeadings(),
        ];
    }
}
