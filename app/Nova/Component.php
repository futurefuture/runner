<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Epartment\NovaDependencyContainer\HasDependencies;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;

class Component extends Resource
{
    use HasDependencies;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Layouts';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Component::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content'  => 'array',
    ];

    protected $componentOne =
        '{
            "type": "layout patterns",
            "id": "1",
            "attributes": {
                "sortOrder": 6,
                "url": "runner\/v4\/inventories\/18\/categories\/4\/products?filter[tags]=rush-featured-wine&limit=5",
                "icon": "https:\/\/res.cloudinary.com\/dcnts7jzv\/image\/upload\/v1559250651\/rush\/RushIconLogoCircle.png",
                "title": "Rush Wine",
                "subTitle": "A curated selection of the best wines available at the LCBO",
                "link": {
                    "title": "Shop All",
                    "subTitle": null,
                    "type": "filters",
                    "categoryId": "4",
                    "storeId": "2",
                    "url": "runner\/v4\/inventories\/18\/categories\/4\/products?filter[tags]=rush-featured-wine&limit=5",
                    "prettyUrl": "category\/wine?filter[tags]=rush-featured-wine",
                    "subDomain": "rush",
                    "filters": [
                        {
                            "type": "tags",
                            "id": "45",
                            "attributes": {
                                "title": "Rush Wine",
                                "slug": "rush-featured-wine",
                                "type": "promotion"
                            }
                        }
                    ]
                }
            }
        }';
    protected $componentTwo =
        '{
            "type": "layout patterns",
            "id": "2",
            "attributes": {
                "sortOrder": 1,
                "icon": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1561563972/lcbo/iOSRunnerIconLcboLogo.png",
                "title": "Taste the world",
                "subTitle": "Pick a destination and experience peculiar tast across the globe",
                "link": {
                    "title": "Show all",
                    "subTitle": null,
                    "type": "filters",
                    "categoryId": null,
                    "storeId": "1",
                    "url": "runner/v4/inventories/11/categories/11/products?filters=taste-the-world",
                    "prettyUrl": "category/alcohol/spirits?filters=taste-the-world",
                    "subDomain": null
                },
                "carousel": [
                    {
                        "sortOrder": 0,
                        "imageDesktop": "",
                        "imageMobile": "",
                        "link": {
                            "title": "Japan",
                            "subTitle": null,
                            "type": "filters",
                            "categoryId": null,
                            "storeId": "1",
                            "url": "runner/v4/inventories/11/categories/11/products?filters=japan",
                            "prettyUrl": "category/alcohol/spirits?filters=japan",
                            "subDomain": null
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 1,
                        "image": "",
                        "link": {
                            "type": "filters",
                            "title": "Spain",
                            "categoryId": null,
                            "subTitle": null,
                            "storeId": "1",
                            "url": "runner/v4/inventories/11/categories/11/products?filters=spain",
                            "prettyUrl": "category/alcohol/spirits?filters=spain",
                            "subDomain": null
                        },
                        "button": null
                    }
                ]
            }
        }';
    protected $componentThree =
        '{
            "type": "layout patterns",
            "id": "3",
            "attributes": {
                "sortOrder": 0,
                "url": null,
                "icon": null,
                "title": null,
                "subTitle": null,
                "carousel": [
                    {
                        "sortOrder": 0,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RunnerDryJanBannerWeb.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RunnerDryJanBannerMobile.jpg",
                        "link": {
                            "type": "category",
                            "productId": null,
                            "altText": null,
                            "title": null,
                            "subTitle": null,
                            "storeId": "2",
                            "url": "runner\/v4\/inventories\/18\/categories\/183\/products?filter[tags]=non-alcoholic",
                            "prettyUrl": "category\/non-alcoholic",
                            "subDomain": "rush",
                            "categoryId": "183",
                            "filters": null
                        }
                    },
                    {
                        "sortOrder": 1,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/StacktBelgianMoonBannerDesktop.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/StacktBelgianMoonBannerMobile.jpg",
                        "link": {
                            "type": "product",
                            "productId": "7752",
                            "altText": null,
                            "title": null,
                            "subTitle": null,
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/2\/product\/belgian-moon-473ml-can",
                            "prettyUrl": "product\/belgian-moon-473ml-can",
                            "subDomain": "rush",
                            "categoryId": "2"
                        }
                    },
                    {
                        "sortOrder": 2,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushWineBannerWeb.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushWineBannerMobile.jpg",
                        "link": {
                            "type": "category",
                            "title": null,
                            "subTitle": null,
                            "storeId": "2",
                            "url": "runner\/v4\/inventories\/18\/categories\/4\/",
                            "prettyUrl": "category\/wine",
                            "subDomain": "rush",
                            "categoryId": "4",
                            "filters": null
                        }
                    },
                    {
                        "sortOrder": 3,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushChipandBeerBannerWeb.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushChipandBeerBannerMobile.jpg",
                        "link": {
                            "type": "filters",
                            "title": null,
                            "subTitle": null,
                            "storeId": "2",
                            "url": "runner\/v4\/inventories\/18\/categories\/1\/products?filter[tags]=rush-bundles",
                            "prettyUrl": "category\/alcohol?filter[tags]=rush-bundles",
                            "subDomain": "rush",
                            "categoryId": "1",
                            "filters": [
                                {
                                    "type": "tags",
                                    "id": "27",
                                    "attributes": {
                                        "title": "Rush Bundles",
                                        "slug": "rush-bundles",
                                        "type": "promotion"
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }';
    protected $componentFour =
        '{
            "type": "layout patterns",
            "id": "4",
            "attributes": {
                "sortOrder": 3,
                "url": null,
                "icon": null,
                "title": null,
                "subTitle": null,
                "carousel": [
                    {
                        "sortOrder": 0,
                        "link": {
                            "type": "category",
                            "title": "Red Wine",
                            "subTitle": null,
                            "storeId": "1",
                            "categoryId": "6",
                            "url": "runner/v4/inventories/11/categories/6",
                            "prettyUrl": "category/alcohol/wine/red",
                            "subDomain": null
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 1,
                        "link": {
                            "type": "category",
                            "title": "White Wine",
                            "subTitle": null,
                            "storeId": "1",
                            "categoryId": null,
                            "url": "runner/v4/inventories/11/categories/5",
                            "prettyUrl": "category/alcohol/wine/white",
                            "subDomain": null
                        },
                        "button": null
                    },
                    {
                    "sortOrder": 2,
                    "link": {
                        "type": "category",
                        "title": "Rosé",
                        "subTitle": null,
                        "storeId": "1",
                        "categoryId": null,
                        "url": "runner/v4/inventories/11/categories/14",
                        "prettyUrl": "category/alcohol/wine/rose",
                        "subDomain": null
                    },
                    "button": null
                    }
                ]
            }
        }';
    protected $componentFive =
        '{
            "type": "layout patterns",
            "id": "5",
            "attributes": {
                "sortOrder": 4,
                "icon": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1561563972/lcbo/iOSRunnerIconLcboLogo.png",
                "title": "Explore Stores",
                "subTitle": "Discover quality aged wine",
                "link": null,
                "carousel": [
                    {
                        "sortOrder": null,
                        "imageDesktop": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562690780/runner%20v4/images/CatergoryWine.jpg",
                        "imageMobile": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562690780/runner%20v4/images/CatergoryWine.jpg",
                        "link": {
                            "title": null,
                            "subTitle": null,
                            "type": "storeFront",
                            "categoryId": null,
                            "storeId": "1",
                            "url": "runner/v4/inventories/1/categories/6/products",
                            "prettyUrl": "category/alcohol/wine",
                            "subDomain": null
                        },
                        "button": {
                            "title": "Shop Now",
                            "subTitle": null
                        }
                    }
                ]
            }
        }';
    protected $componentSix =
        '{
            "type": "layout patterns",
            "id": "6",
            "attributes": {
                "sortOrder": 8,
                "icon": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695477/gift/IconGiftCircle.png",
                "title": "Explore Gift Ideas",
                "subTitle": "Send your love with Crown Flora Studio",
                "images": [
                    {
                        "imageDesktop": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695939/gift/GiftBannerImage1.png",
                        "imageMobile": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695939/gift/GiftBannerImage1.png",
                        "link": {
                            "title": null,
                            "subTitle": null,
                            "type": "storeFront",
                            "categoryId": null,
                            "storeId": "4",
                            "url": "",
                            "prettyUrl": "/",
                            "subDomain": null
                        },
                        "button": null
                    },
                    {
                        "imageDesktop": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695940/gift/GiftBannerImage2.png",
                        "imageMobile": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695940/gift/GiftBannerImage2.png",
                        "link": {
                            "title": null,
                            "subTitle": null,
                            "type": "storeFront",
                            "storeId": "4",
                            "categoryId": null,
                            "url": "",
                            "prettyUrl": "/",
                            "subDomain": null
                        },
                        "button": null
                    },
                    {
                        "imageDesktop": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695939/gift/GiftBannerImage3.png",
                        "imageMobile": "https://res.cloudinary.com/dcnts7jzv/image/upload/v1562695939/gift/GiftBannerImage3.png",
                        "link": {
                            "title": null,
                            "subTitle": null,
                            "type": "storeFront",
                            "storeId": "4",
                            "categoryId": null,
                            "url": "",
                            "prettyUrl": "/",
                            "subDomain": null
                        },
                        "button": null
                    }
                ],
                "blockBgColor": "#393939",
                "blockMessage": "Surprise your loved ones with gifts",
                "link": {
                    "title": "Shop Now",
                    "subTitle": null,
                    "type": "storeFront",
                    "storeId": "4",
                    "categoryId": null,
                    "url": "",
                    "prettyUrl": "/",
                    "subDomain": null
                },
                "button": {
                    "title": "Shop Now",
                    "subTitle": null
                }
            }
        }';
    protected $componentSeven =
        '{
            "type": "layout patterns",
            "id": "7",
            "attributes": {
                "sortOrder": 2,
                "icon": null,
                "title": "Filter stores by category",
                "subTitle": "Have your essentials delivered in less than 2hrs",
                "link": null,
                "carousel": [
                    {
                        "sortOrder": 0,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySendGifts.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySendGifts.jpg",
                        "link": {
                            "type": "storeFront",
                            "title": "Gift",
                            "subTitle": "",
                            "storeId": "10",
                            "url": "\/",
                            "prettyUrl": "\/",
                            "subDomain": "cocktailemporium",
                            "categoryId": null
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 0,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryWIne.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryWIne.jpg",
                        "link": {
                            "title": "Wine",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "4",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/4\/products",
                            "prettyUrl": "category\/wine",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 1,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryBeersandciders.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryBeersandciders.jpg",
                        "link": {
                            "title": "Beer & Cider",
                            "type": "categoryStore",
                            "subTitle": null,
                            "categoryId": "2",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/2\/products",
                            "prettyUrl": "category\/beer-cider",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 2,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySpirits.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySpirits.jpg",
                        "link": {
                            "title": "Spirits",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "3",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/3\/products",
                            "prettyUrl": "category\/spirits",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 3,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategorySnacks.png",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategorySnacks.png",
                        "link": {
                            "title": "Snacks",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "193",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/193\/products",
                            "prettyUrl": "category\/snacks",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 4,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySpecialtyWater.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategorySpecialtyWater.jpg",
                        "link": {
                            "title": "Specialty Water",
                            "subTitle": null,
                            "type": "filters",
                            "categoryId": "192",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/192\/products?filter[tags]=rush-specialty-water",
                            "prettyUrl": "category\/beverages?filter[tags]=rush-specialty-water",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 5,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryCoolers.jpg",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/categories\/CategoryCoolers.jpg",
                        "link": {
                            "title": "Coolers",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "5",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/5\/products",
                            "prettyUrl": "category\/coolers",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 6,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategoryBeverages.png",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategoryBeverages.png",
                        "link": {
                            "title": "Beverages",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "192",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/192\/products",
                            "prettyUrl": "category\/beverages",
                            "subDomain": "rush"
                        },
                        "button": null
                    },
                    {
                        "sortOrder": 7,
                        "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategoryJuices.png",
                        "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/runner\/categories\/CategoryJuices.png",
                        "link": {
                            "title": "Juice & Mix",
                            "subTitle": null,
                            "type": "categoryStore",
                            "categoryId": "199",
                            "storeId": "1",
                            "url": "runner\/v4\/inventories\/18\/categories\/199\/products",
                            "prettyUrl": "category\/juice-mix",
                            "subDomain": "rush"
                        },
                        "button": null
                    }
                ]
            }
        }';
    protected $componentEight =
        '{
            "type": "layout patterns",
            "id": "8",
            "attributes": {
                "sortOrder": 4,
                "icon": null,
                "title": "What are you shopping for?",
                "subTitle": "Find essentials quickly",
                "link": null
            }
        }';
    protected $componentNine =
        '{
            "type": "layout patterns",
            "id": "9",
            "attributes": {
                "sortOrder": 2,
                "icon": null,
                "title": "A Curated Selection",
                "subTitle": "The Toronto Life + Runner Storefront will house Toronto Life\'s favourite picks every month. Think sommelier-selected items on sale, curated monthly, and featuring top picks from Toronto Life\'s team of incredible tastemakers. Find the latest on food, style, culture, and neighbourhoods in the city we call home at torontolife.com",
                "link": null
            }
        }';
    protected $componentTen =
        '{
        "type": "layout patterns",
        "id": "10",
        "attributes": {
            "sortOrder": 2,
            "url": null,
            "icon": "https:\/\/runner-file-manager.s3.amazonaws.com\/rush\/RushIconLogoCircle.png",
            "buttonColor": "#CE8BFF",
            "title": "RUSH",
            "subTitle": "Shop alcohol & snacks at the same time. Probably the best conveience store ever. Delivery in as little as 1hr.",
            "link": {
                "title": "Shop Rush",
                "subTitle": null,
                "type": "storeFront",
                "categoryId": null,
                "storeId": "2",
                "url": "\/",
                "prettyUrl": "\/",
                "subDomain": "rush",
                "filters": null
            },
            "carousel": [
                {
                    "sortOrder": 0,
                    "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushBeersCategoryCard.jpg",
                    "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushBeersCategoryCard.jpg",
                    "link": {
                        "type": "storeFront",
                        "altText": null,
                        "title": null,
                        "subTitle": null,
                        "storeId": "2",
                        "url": "runner\/v4\/inventories\/2\/categories\/2\/category\/beer-cider?filter[tags]=rush-featured-beer-cider",
                        "prettyUrl": "category\/beer-cider?filter[tags]=rush-featured-beer-cider",
                        "subDomain": "rush",
                        "categoryId": "2",
                        "filters": null
                    }
                },
                {
                    "sortOrder": 1,
                    "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushCuratedWinesCategoryCard.jpg",
                    "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushCuratedWinesCategoryCard.jpg",
                    "link": {
                        "type": "storeFront",
                        "altText": null,
                        "title": null,
                        "subTitle": null,
                        "storeId": "2",
                        "url": "runner\/v4\/inventories\/4\/categories\/4\/category\/wine?filter[tags]=rush-featured-wine",
                        "prettyUrl": "category\/wine?filter[tags]=rush-featured-wine",
                        "subDomain": "rush",
                        "categoryId": "4",
                        "filters": null
                    }
                },
                {
                    "sortOrder": 2,
                    "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushDrinkMixersCategoryCard.jpg",
                    "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushDrinkMixersCategoryCard.jpg",
                    "link": {
                        "type": "storeFront",
                        "altText": null,
                        "title": null,
                        "subTitle": null,
                        "storeId": "2",
                        "url": "runner\/v4\/inventories\/2\/categories\/192\/category\/beverages?filter[tags]=rush-featured-mix",
                        "prettyUrl": "category\/beverages?filter[tags]=rush-featured-mix",
                        "subDomain": "rush",
                        "categoryId": "192",
                        "filters": null
                    }
                },
                {
                    "sortOrder": 3,
                    "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushCuratedSpiriitsCategoryCard.jpg",
                    "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushCuratedSpiriitsCategoryCard.jpg",
                    "link": {
                        "type": "storeFront",
                        "altText": null,
                        "title": null,
                        "subTitle": null,
                        "storeId": "2",
                        "url": "runner\/v4\/inventories\/2\/categories\/2\/category\/spirits?filter[tags]=rush-featured-spirits",
                        "prettyUrl": "category\/spirits?filter[tags]=rush-featured-spirits",
                        "subDomain": "rush",
                        "categoryId": "3",
                        "filters": null
                    }
                },
                {
                    "sortOrder": 4,
                    "imageDesktop": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushFreshSnacksCategoryCard.jpg",
                    "imageMobile": "https:\/\/runner-file-manager.s3.amazonaws.com\/ducky\/RushFreshSnacksCategoryCard.jpg",
                    "link": {
                        "type": "storeFront",
                        "altText": null,
                        "title": null,
                        "subTitle": null,
                        "storeId": "2",
                        "url": "runner\/v4\/inventories\/2\/categories\/193\/category\/snacks",
                        "prettyUrl": "category\/snacks",
                        "subDomain": "rush",
                        "categoryId": "193",
                        "filters": null
                    }
                }
            ]
        }
    }';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title', 'title'),
            Text::make('Slug', 'slug'),
            Select::make('Component Type', 'type')->options([
                0  => 'Product Carousel',
                1  => 'Tower',
                2  => 'Hero Carousel',
                3  => 'Category Pill',
                4  => 'Single Banner',
                5  => 'Tiled',
                6  => 'Category Images',
                7  => 'Reward Points',
                8  => 'Title & Description',
                9  => 'Wide Tile Carousel',
            ])->displayUsingLabels(),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->default($this->componentOne)
                    ->json()
                    ->hideFromIndex()
            ])->dependsOn('type', 0),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->default($this->componentTwo)
                    ->json()
                    ->hideFromIndex()
            ])->dependsOn('type', 1),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentThree),
            ])->dependsOn('type', 2),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentFour),
            ])->dependsOn('type', 3),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentFive),
            ])->dependsOn('type', 4),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentSix),
            ])->dependsOn('type', 5),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentSeven),
            ])->dependsOn('type', 6),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentEight),
            ])->dependsOn('type', 7),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentNine),
            ])->dependsOn('type', 8),
            NovaDependencyContainer::make([
                Code::make('Content', 'content')
                    ->json()
                    ->hideFromIndex()
                    ->default($this->componentTen),
            ])->dependsOn('type', 9),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
