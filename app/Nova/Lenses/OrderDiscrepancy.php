<?php

namespace App\Nova\Lenses;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\LensRequest;
use Laravel\Nova\Lenses\Lens;

class OrderDiscrepancy extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            // $query->select('orders.*')
            //     ->leftjoin('order_items', 'orders.id', '=', 'order_items.order_id')
            //     ->where('orders.status', 3)
            //     ->where('order_items.order_id', null)
            $query
                ->select('orders.id', 'orders.status', DB::raw('JSON_LENGTH(orders.content) as items'), DB::raw('count(order_items.id) as orderItem'))
                ->leftjoin('order_items', 'orders.id', '=', 'order_items.order_id')
                ->where('orders.status', '=', 3)
                ->groupBy('orders.id', 'items')
                ->havingRaw('items != orderItem')
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Number::make('items', 'items', function ($value) {
                return number_format($value);
            }),
            Number::make('orderItem', 'orderItem', function ($value) {
                return number_format($value);
            })
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new \App\Nova\Filters\OrderStartDateFilter,
            new \App\Nova\Filters\OrderEndDateFilter,
        ];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'order-discrepancy';
    }
}
