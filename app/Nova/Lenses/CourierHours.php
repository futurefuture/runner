<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Fields\Number;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Http\Requests\LensRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class CourierHours extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->select('schedules.id AS id', 'users.id', 'users.first_name AS first_name', 'users.last_name AS last_name', 'schedules.start AS start', 'schedules.end AS end', DB::raw('TIMESTAMPDIFF(HOUR, schedules.start, schedules.end) AS hours_worked'))
                ->leftJoin('users', 'schedules.user_id', '=', 'users.id')
                ->where('users.role', '=', 2)
                ->orderBy('users.id')
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Text::make('First Name', 'first_name')
                ->sortable(),
            Text::make('Last Name', 'last_name')
                ->sortable(),
            Text::make('Start', 'start')
                ->sortable(),
            Text::make('End', 'end')
                ->sortable(),
            Number::make('Hours Worked', 'hours_worked'),
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new \App\Nova\Filters\ScheduleStartDate,
            new \App\Nova\Filters\ScheduleEndDate,
        ];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)->withHeadings(),
        ];
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'couriers';
    }
}
