<?php

namespace App\Nova\Lenses;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\LensRequest;
use Laravel\Nova\Lenses\Lens;

class CategoryTotalRevenue extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query
                ->select('categories.id', 'categories.title', DB::raw('sum(order_items.quantity * order_items.retail_price) as revenue'), DB::raw('year(order_items.created_at) as year'))
                ->join('order_items', 'categories.id', '=', 'order_items.category_id')
                ->orderBy('categories.id', 'asc')
                ->orderBy('year', 'desc')
                ->groupBy('categories.id', DB::raw('year(order_items.created_at)'))
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Text::make('Title')->sortable(),
            Number::make('Revenue', 'revenue', function ($value) {
                return '$'.number_format($value / 100, 2);
            }),
            Text::make('Year'),
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new \App\Nova\Filters\ProductsTotalSoldStartDate,
            new \App\Nova\Filters\ProductsTotalSoldEndDate,
            new \App\Nova\Filters\CategoryFilter,
        ];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'category-total-revenue';
    }
}
