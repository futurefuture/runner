<?php

namespace App\Nova\Lenses;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\LensRequest;
use Laravel\Nova\Lenses\Lens;

class ProductSalesByAge extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        // return $request->withOrdering($request->withFilters(
        //     $query
        //         ->select('products.id', 'products.title', DB::raw('Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) as age'), DB::raw('sum(order_items.quantity) as quantity'))
        //         ->join('order_items', 'products.id', '=', 'order_items.product_id')
        //         ->join('users', 'order_items.user_id', '=', 'users.id')
        //         ->groupBy('products.id', 'products.title', 'age')
        // ));

        return $request->withOrdering($request->withFilters(
            $query
                ->select('products.id', 'products.title', DB::raw('CASE WHEN Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) >= 19 && Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) <=24  THEN "19-24" WHEN Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) >= 25 && Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) <=34  THEN "25-34" WHEN Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) >= 35 && Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) <=44  THEN "35-44" WHEN Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) >= 45 && Floor(DATEDIFF(CURDATE(),users.date_of_birth)/365.25) <=54  THEN "45-54" ELSE "55-64" END AS age'), DB::raw('sum(order_items.quantity) as quantity'))
                ->join('order_items', 'products.id', '=', 'order_items.product_id')
                ->join('users', 'order_items.user_id', '=', 'users.id')
                ->groupBy('products.id', 'products.title', 'age')
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Text::make('Title')->sortable(),
            Text::make('age'),
            Number::make('Quantity', 'quantity', function ($value) {
                return number_format($value);
            }),
        ];
    }

    /**
     * Get the cards available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new \App\Nova\Filters\ProductsTotalSoldStartDate,
            new \App\Nova\Filters\ProductsTotalSoldEndDate,
            new \App\Nova\Filters\TitleColumnFilter,
            new \App\Nova\Filters\ProductIdColoumnFilter,
            new \App\Nova\Filters\ageFilter,
        ];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'products-sales-by-age';
    }
}
