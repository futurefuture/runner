<?php

namespace App\Nova;

use Chaseconey\ExternalImage\ExternalImage;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Product::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
        'packaging',
        'runner_price',
    ];

    public function title()
    {
        return $this->title . ' - ' . $this->packaging . ' - ' . $this->id;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('sku')->sortable(),
            Text::make('Title', 'title')->sortable(),
            Boolean::make('Is Runner Owned', 'is_runner_owned', function () {
                return true;
            }),
            ExternalImage::make('Image'),
            Number::make('Markup Percentage', 'markup_percentage')
                ->step(0.01)
                ->hideFromIndex(),
            Number::make('Markup', 'markup')
                ->hideFromIndex(),
            Number::make('Category ID', 'category_id')
                ->hideFromIndex(),
            Currency::make('Markup', function () {
                return $this->markup / 100;
            }),
            Number::make('Retail Price', 'runner_price')
                ->hideFromIndex(),
            Number::make('Wholesale Price', 'retail_price')
                ->hideFromIndex(),
            Number::make('Limited Time Offer Price', 'limited_time_offer_price')
                ->hideFromIndex(),
            Number::make('Limited Time Offer Savings', 'limited_time_offer_savings')
                ->hideFromIndex(),
            TextArea::make('Long Description', 'long_description')
                ->alwaysShow()
                ->hideFromIndex(),
            Textarea::make('Short Description', 'short_description')->hideFromIndex(),
            Text::make('Packaging', 'packaging')->hideFromIndex(),
            Text::make('Alcohol Content', 'alcohol_content')->hideFromIndex(),
            Text::make('Sugar Content', 'sugar_content')->hideFromIndex(),
            Text::make('Slug', 'slug')->hideFromIndex(),
            Boolean::make('Is Active', 'is_active'),
            DateTime::make('Created At', 'created_at')->hideFromIndex(),
            DateTime::make('Updated At', 'updated_at')->hideFromIndex(),
            DateTime::make('Deleted At', 'deleted_at')->hideFromIndex(),
            BelongsToMany::make('Tags')
                ->fields(function () {
                    return [
                        Number::make('Tag Index', 'index')
                            ->sortable(),
                    ];
                }),
            BelongsToMany::make('Inventories')
                ->fields(function () {
                    return [
                        Number::make('Quantity', 'quantity'),
                    ];
                }),
            BelongsToMany::make('Categories')
                ->fields(function () {
                    return [
                        Number::make('Category Index', 'index')
                            ->sortable(),
                    ];
                }),
            HasMany::make('Reviews'),
            HasMany::make('Product Images', 'productImages', \App\Nova\ProductImage::class)
                ->hideFromIndex(),
            // BelongsToMany::make('Incentives'),
            BelongsToMany::make('Related Products', 'relatedProducts', self::class)
                ->fields(function () {
                    return [
                        Number::make('Index', 'index')
                            ->sortable(),
                    ];
                })
                ->searchable(),
            Boolean::make('Is Updateable', 'is_updatable'),
            BelongsToMany::make('Partners'),
            HasMany::make('Ads'),
            Boolean::make('Is Alcohol', 'is_alcohol'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
            new Lenses\ProductsTotalSold,
            new Lenses\ProductsTotalRevenue,
            new Lenses\ProductTopPostalCode,
            new Lenses\ProductSalesByAge,
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new DownloadExcel,
            new \App\Nova\Actions\ImportProductInfoExcel,
        ];
    }
}
