<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Text;
use App\Nova\Actions\ImportAds;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;

class Ad extends Resource
{
    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Partners';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Ad';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        // ad type 1 product carousel
        // ad type 2 hero carousel
        return [
            ID::make()->sortable(),
            Number::make('Ad Type Id', 'ad_type_id')->sortable(),
            BelongsTo::make('Campaign', 'campaign')->sortable(),
            BelongsTo::make('Partner', 'partner')->sortable(),
            Code::make('Content')
                ->json()
                ->hideFromIndex(),
            Code::make('Products')
                ->json()
                ->hideFromIndex(),
            Number::make('Product Id', 'product_id'),
            BelongsTo::make('Category', 'category')->sortable()->nullable(),
            BelongsTo::make('Tag', 'tag')->sortable()->nullable(),
            BelongsTo::make('Carousel', 'carousel')->sortable()->nullable(),
            Number::make('Carousel Component Id', 'carousel_component_id')->sortable()->nullable(),
            Text::make('Bid Type', 'bid_type')->sortable(),
            Number::make('Bid Amount', 'bid_amount')->sortable(),
            Number::make('Index'),
            Select::make('State')->options([
                'active'   => 'active',
                'complete' => 'complete',
                'pending'  => 'pending',
                'paused'   => 'paused'
            ]),
            DateTime::make('Start Date', 'start_date'),
            DateTime::make('End Date', 'end_date'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new ImportAds,
        ];
    }
}
