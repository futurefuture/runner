<?php

namespace App\Nova\Actions;

use App\Container;
use App\Task;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;

class UpdateOrderStatus extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            $courier         = User::find($fields->courier);
            $model->status   = $fields->status;
            $model->runner_1 = $courier->id;

            if ($model->task) {
                $model->task->update([
                    'state'      => $fields->status,
                    'courier_id' => $courier->id,
                ]);
                $model->save();
            } else {
                $container = Container::create([
                    'courier_id' => $courier->id,
                ]);

                $task = new Task([
                    'state'        => $fields->status,
                    'index'        => 0,
                    'notes'        => null,
                    'phone_number' => null,
                    'courier_id'   => $courier->id,
                    'container_id' => $container->id,
                    'name'         => null,
                    'organization' => null,
                    'address_id'   => $model->address_id,
                    'order_id'     => $model->id,
                    'type'         => 'order',
                    'signature'    => null,
                ]);

                $model->save();
                $model->task()->save($task);
            }
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        $couriers = User::where('role', 2)
            ->orWhere('role', 1)
            ->get()
            ->pluck('first_name', 'id');

        return [
            Number::make('Status'),
            Select::make('Courier')->options($couriers),
        ];
    }
}
