<?php

namespace App\Nova\Actions;

use App\Product;
use App\Services\NotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class UpdateReviewStatus extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            if ($fields->is_approved == 1) {
                $model->is_approved = 1;
                $model->save();
                $product = Product::find($model->product_id);

                if ($model::where('product_id', $product->id)->count() == 1) {
                    $model->user->rewardPointsTransaction(500, 8, [
                        'product_id' => $product->id,
                        'name'       => $product->name,
                    ]);

                    $isFirstReview = true;
                    $notificationService = new NotificationService($order = null);
                    $notificationService->sendReviewAcceptedNotification($model, $product, $isFirstReview);
                } else {
                    $model->user->rewardPointsTransaction(100, 7, [
                        'product_id' => $product->id,
                        'name'       => $product->name,
                    ]);

                    $isFirstReview = false;
                    $notificationService = new NotificationService($order = null);
                    $notificationService->sendReviewAcceptedNotification($model, $product, $isFirstReview);
                }
            } else {
                $model->is_approved = 2;
                $model->save();
            }
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Is Approved', 'is_approved')->options([
                1 => 'accept',
                2 => 'deny',
            ]),
        ];
    }
}
