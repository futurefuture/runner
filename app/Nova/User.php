<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone_number',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Avatar::make('Image')->thumbnail(function () {
                return $this->avatar_image;
            }),
            Text::make('First Name', 'first_name')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Last Name', 'last_name')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),
            Text::make('Phone Number', 'phone_number'),
            Text::make('Birthday', 'date_of_birth')
                ->hideFromIndex(),
            Text::make('Facebook ID', 'facebook_id')
                ->hideFromIndex(),
            Text::make('Google ID', 'google_id')
                ->hideFromIndex(),
            Text::make('Stripe ID', 'stripe_id')
                ->hideFromIndex(),
            Text::make('Invite Code', 'invite_code')
                ->hideFromIndex(),
            Text::make('Gender', 'gender')
                ->hideFromIndex(),
            Text::make('FCM Token', 'fcm_token')
                ->hideFromIndex(),
            Number::make('Role', 'role')
                ->hideFromIndex(),
            Boolean::make('Is 19+', 'is_19')
                ->hideFromIndex(),
            Boolean::make('Promo Notifications', 'promo_notification')
                ->hideFromIndex(),
            Boolean::make('Order Notifications', 'order_notification')
                ->hideFromIndex(),
            Boolean::make('Cart Notifications', 'cart_notification')
                ->hideFromIndex(),
            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),
            HasMany::make('Carts'),
            HasMany::make('Favourites'),
            BelongsToMany::make('Coupons Used', 'couponsUsed', \App\Nova\Coupon::class),
            HasMany::make('Reward Points', 'rewardsPoints', \App\Nova\RewardPoint::class),
            BelongsToMany::make('Tags'),
            HasMany::make('Orders'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new DownloadExcel,
        ];
    }
}
