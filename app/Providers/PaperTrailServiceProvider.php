<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PaperTrailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (app('app')->environment() == 'production') {
            // $monolog = app(\Illuminate\Log\Logger::class)->getMonolog();
            // $syslog = new \Monolog\Handler\SyslogHandler('papertrail');
            // $formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');

            // $syslog->setFormatter($formatter);
            // $monolog->pushHandler($syslog);
        }
    }
}
