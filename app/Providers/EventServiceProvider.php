<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserCreated' => [
            'App\Listeners\SendWelcomeEmail',
            'App\Listeners\AddUserToMailChimp',
        ],
        'App\Events\UserDeleted' => [
            'App\Listeners\SendUserDeletedEmail',
            'App\Listeners\RemoveUserFromMailChimp',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
