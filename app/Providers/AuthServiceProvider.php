<?php

namespace App\Providers;

use App\User;
use App\Partner;
use App\Policies\UserPolicy;
use Laravel\Passport\Passport;
use App\Policies\PartnerPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model'    => 'App\Policies\ModelPolicy',
        Partner::class => PartnerPolicy::class,
        User::class    => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Passport::routes(null, ['middleware' => [\Barryvdh\Cors\HandleCors::class]]);
        Passport::routes();

        Passport::tokensCan([
            'view-partner-dashboard' => 'View Partner Dashboard',
        ]);
    }
}
