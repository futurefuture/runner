<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // Mapping of lcbo web routes.
        $this->mapLCBOWebRoutes();

        // Mapping of toronto life web routes.
        $this->mapTorontoLifeWebRoutes();

        // Mapping of gift web routes.
        $this->mapGiftWebRoutes();

        // Mapping of buyers and cellars web routes.
        $this->mapBuyersAndCellarsWebRoutes();

        // dispatch
        $this->mapDispatchV1WebRoutes();
        $this->mapDispatchV2WebRoutes();

        $this->mapDispatchV1ApiRoutes();
        $this->mapDispatchV2ApiRoutes();
        $this->mapDispatchV3ApiRoutes();

        // partner
        $this->mapPartnerApiRoutes();

        // runner
        $this->mapRunnerV3WebRoutes();

        $this->mapRunnerV3ApiRoutes();
        $this->mapRunnerv4ApiRoutes();
        $this->mapRunnerv5ApiRoutes();

        // courier
        $this->mapCourierV1ApiRoutes();
        $this->mapCourierV2ApiRoutes();

        // command
        $this->mapCommandV1ApiRoutes();
        $this->mapCommandV1WebRoutes();

        // Mapping of depreacated rush api routes.
        $this->mapDeprecatedRushApiRoutes();
    }

    /**
     * Define the "runner v3 web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRunnerV3WebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Runner\V3',
        ], function ($router) {
            require base_path('routes/runner/v3/web.php');
        });
    }

    /**
     * Define the "runner v3 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapRunnerV3ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Runner\V3',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/v3/runner/',
        ], function ($router) {
            require base_path('routes/runner/v3/api.php');
        });
    }

    /**
     * Define the "runner v4 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapRunnerV4ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Runner\V4',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/runner/v4/',
        ], function ($router) {
            require base_path('routes/runner/v4/api.php');
        });
    }

    /**
     * Define the "runner v5 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapRunnerV5ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Runner\V5',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/runner/v5/',
        ], function ($router) {
            require base_path('routes/runner/v5/api.php');
        });
    }

    /**
     * Define the "command v1 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapCommandV1ApiRoutes()
    {
        Route::group([
            'namespace'  => $this->namespace . '\Command\V1',
            'prefix'     => '/api/v1/command/',
        ], function ($router) {
            require base_path('routes/command/v1/api.php');
        });
    }

    /**
     * Define the "command v1 web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapCommandV1WebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Command\V1',
            'prefix'     => '/command/',
        ], function ($router) {
            require base_path('routes/command/v1/web.php');
        });
    }

    /**
     * Define the "dispatch v1 web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDispatchV1WebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'prefix'     => '/dispatch/',
            'namespace'  => $this->namespace . '\Dispatch\V1',
        ], function ($router) {
            require base_path('routes/dispatch/v1/web.php');
        });
    }

    /**
     * Define the "dispatch v3 web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDispatchV2WebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Dispatch\V2',
            'domain'     => 'dispatch.' . env('APP_DOMAIN'),
        ], function ($router) {
            require base_path('routes/dispatch/v2/web.php');
        });
    }

    /**
     * Define the "dispatch v1 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDispatchV1ApiRoutes()
    {
        Route::group([
            'namespace'  => $this->namespace . '\Dispatch\V1',
            'prefix'     => '/api/v1/dispatch/',
        ], function ($router) {
            require base_path('routes/dispatch/v1/api.php');
        });
    }

    /**
     * Define the "dispatch v2 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDispatchV2ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Dispatch\V2',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/dispatch/v2/',
        ], function ($router) {
            require base_path('routes/dispatch/v2/api.php');
        });
    }

    /**
     * Define the "dispatch v3 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDispatchV3ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Dispatch\V3',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/dispatch/v3/',
        ], function ($router) {
            require base_path('routes/dispatch/v3/api.php');
        });
    }

    /**
     * Define the "courier v1 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapCourierV1ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Courier\V1',
            'prefix'     => '/api/v1/courier/',
        ], function ($router) {
            require base_path('routes/courier/v1/api.php');
        });
    }

    /**
     * Define the "courier v2 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapCourierV2ApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Courier\V2',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/courier/v2/',
        ], function ($router) {
            require base_path('routes/courier/v2/api.php');
        });
    }

    /**
     * Define the "lcbo web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapLCBOWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Runner\V3',
            'domain'     => 'lcbo.' . env('APP_DOMAIN'),
        ], function ($router) {
            require base_path('routes/runner/v3/web.php');
        });
    }

    /**
     * Define the "torontolife web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapTorontoLifeWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Runner\V3',
            'domain'     => 'torontolife.' . env('APP_DOMAIN'),
        ], function ($router) {
            require base_path('routes/runner/v3/web.php');
        });
    }

    /**
     * Define the "buyers and cellars" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapBuyersAndCellarsWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Runner\V3',
            'domain'     => 'buyersandcellars.' . env('APP_DOMAIN'),
        ], function ($router) {
            require base_path('routes/runner/v3/web.php');
        });
    }

    /**
     * Define the "gift web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapGiftWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace . '\Runner\V3',
            'domain'     => 'gift.' . env('APP_DOMAIN'),
        ], function ($router) {
            require base_path('routes/runner/v3/web.php');
        });
    }

    /**
     * Define the "partner v2 api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapPartnerApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace . '\Partner\V2',
            'domain'     => 'api.' . env('APP_DOMAIN'),
            'prefix'     => '/partner/v2/',
        ], function ($router) {
            require base_path('routes/partner/v2/api.php');
        });
    }

    /**
     * Define the "deprecated rush api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDeprecatedRushApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'prefix'     => '/api/v3/rush/',
        ], function ($router) {
            require base_path('routes/deprecated/api.php');
        });
    }
}
