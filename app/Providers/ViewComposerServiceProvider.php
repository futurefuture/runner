<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('errors.404', function ($view) {
            $view->with([
                'domain'    => env('APP_DOMAIN'),
                'subDomain' => 'www',
                'storeId'   => 1,
            ]);
        });

        view()->composer('errors::404', function ($view) {
            $view->with([
                'domain'    => env('APP_DOMAIN'),
                'subDomain' => 'www',
                'storeId'   => 1,
            ]);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}
