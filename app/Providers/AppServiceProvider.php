<?php

namespace App\Providers;

use App\Store;
use Illuminate\Support\ServiceProvider;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;

// use App\Http\Resources\RewardPoint as RewardPointResource;

class AppServiceProvider extends ServiceProvider
{
    protected $subDomain = 'www';
    protected $storeId;
    protected $domain;

    private function getDomainDetails()
    {
        // set HTTP_HOST for tests as well
        if (isset($_SERVER['HTTP_HOST']) && ! empty($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            $host = 'www.runner.test';
        }

        $sub = explode('.', $host)[0];

        if ($sub != '') {
            $this->subDomain = explode('.', $host)[0];
        }

        $this->domain = env('APP_DOMAIN');
        $store        = Store::where('sub_domain', $this->subDomain)->first();

        if (! $store) {
            $store = Store::find(8);
        }

        $this->storeId = $store->id ?? 8;
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->environment('local')) {
            \URL::forceScheme('https');
        }

        view()->composer('errors::404', function ($view) {
            $this->getDomainDetails();

            $domain = $this->domain;
            $subDomain = $this->subDomain;
            $storeId = $this->storeId;

            $pageAttributes = json_encode([
                'domain'    => $domain,
                'subDomain' => $subDomain,
                'storeId'   => $storeId,
            ]);
            $view->with('pageAttributes', $pageAttributes);
        });

        $this->app->bind(
            'Google\AdsApi\AdWords\AdWordsServices',
            function () {
                return new AdWordsServices();
            }
        );
        $this->app->bind(
            'Google\Auth\FetchAuthTokenInterface',
            function () {
                // Generate a refreshable OAuth2 credential for authentication
                // from the config file.
                return (new OAuth2TokenBuilder())->fromFile(
                    config_path() . '/adsapi_php.ini'
                )->build();
            }
        );
        $this->app->bind(
            'Google\AdsApi\AdWords\AdWordsSessionBuilder',
            function () {
                return new AdWordsSessionBuilder();
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
