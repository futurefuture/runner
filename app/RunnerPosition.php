<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunnerPosition extends Model
{
    protected $table = 'runner_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'rate',
        'backgroundColor',
    ];
}
