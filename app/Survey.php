<?php

namespace App;

use App\User;
use App\Order;
use App\Partner;
use App\SurveyOption;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    protected $casts = [
        'start_date' => 'datetime',
        'end_date'   => 'datetime',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'results',
        'segment',
        'partner_id',
    ];

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function surveyOptions()
    {
        return $this->hasMany(SurveyOption::class);
    }
}
