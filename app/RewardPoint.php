<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RewardPoint extends Model
{
    protected $table = 'reward_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'value',
        'type',
        'details',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
