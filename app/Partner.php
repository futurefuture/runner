<?php

namespace App;

use App\Product;
use App\Store;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partners';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'user_id',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'partner_product', 'partner_id', 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_partner', 'partner_id', 'category_id');
    }

    public function ads()
    {
        return $this->HasMany(Ad::class);
    }

    public function campaigns()
    {
        return $this->HasMany(Campaign::class);
    }
}
