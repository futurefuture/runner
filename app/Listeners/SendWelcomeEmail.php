<?php

namespace App\Listeners;

use Exception;
use App\Events\UserCreated;
use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;

        Mail::to($user->email, $user->first_name)
                ->bcc('service@getrunner.io', 'Runner')
                ->send(new UserRegistered($user));
    }

    /**
     * Handle a job failure.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed($exception)
    {
        throw new Exception($exception);
    }
}
