<?php

namespace App\Listeners;

use Exception;
use App\Events\UserDeleted;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\UserDeleted as UserDeletedMail;

class SendUserDeletedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $user = $event->user;

        Mail::to($user->email, $user->first_name)
                ->bcc('service@getrunner.io', 'Runner')
                ->send(new UserDeletedMail($user));
    }

    /**
     * Handle a job failure.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed($exception)
    {
        throw new Exception($exception);
    }
}
