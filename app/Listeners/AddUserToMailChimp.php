<?php

namespace App\Listeners;

use Exception;
use App\Events\UserCreated;
use App\Services\MailChimpService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddUserToMailChimp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;

        $mailChimpService = new MailChimpService($user);
        $mailChimpService->addSubscriber();
    }

    /**
     * Handle a job failure.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed($exception)
    {
        throw new Exception($exception);
    }
}
