<?php

namespace App\Listeners;

use App\Events\UserDeleted;
use App\Services\MailChimpService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemoveUserFromMailChimp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $user = $event->user;

        $mailChimpService = new MailChimpService($user);
        $mailChimpService->removeSubscriber();
    }
}
