<?php

namespace App\Jobs;

use App\Product;
use App\Inventory;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RushToRushLateNight implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // switch territory and store over to temp inventory
        DB::table('inventory_territory')
            ->where('inventory_id', 18)
            ->where('territory_id', 1)
            ->update([
                'inventory_id' => 24
            ]);

        // east
        DB::table('inventory_territory')
            ->where('inventory_id', 25)
            ->where('territory_id', 3)
            ->update([
                'inventory_id' => 26
            ]);

        // copy inventory amounts over
        $inventoryProducts = DB::table('inventory_product')
            ->where('inventory_id', 18)
            ->get();

        foreach ($inventoryProducts as $i) {
            $product = Product::find($i->product_id);

            if ($product && $product->is_alcohol === 0) {
                DB::table('inventory_product')
                    ->updateOrInsert(
                        [
                            'product_id'   => $product->id,
                            'inventory_id' => 24,
                        ],
                        [
                            'quantity' => $i->quantity
                        ]
                    );
            }
        }

        // east
        $inventoryProducts = DB::table('inventory_product')
            ->where('inventory_id', 25)
            ->get();

        foreach ($inventoryProducts as $i) {
            $product = Product::find($i->product_id);

            if ($product && $product->is_alcohol === 0) {
                DB::table('inventory_product')
                    ->updateOrInsert(
                        [
                            'product_id'   => $product->id,
                            'inventory_id' => 26,
                        ],
                        [
                            'quantity' => $i->quantity
                        ]
                    );
            }
        }

        // switch runner layout
        DB::table('layout_store_territory')
            ->where('store_id', 8)
            ->where('territory_id', 1)
            ->update([
                'layout_id' => 9
            ]);

        // east
        DB::table('layout_store_territory')
            ->where('store_id', 8)
            ->where('territory_id', 3)
            ->update([
                'layout_id' => 9
            ]);

        // switch rush layout
        DB::table('layout_store_territory')
            ->where('store_id', 2)
            ->where('territory_id', 1)
            ->update([
                'layout_id' => 9
            ]);

        // east
        DB::table('layout_store_territory')
            ->where('store_id', 2)
            ->where('territory_id', 3)
            ->update([
                'layout_id' => 9
            ]);

        // switch late night to active
        Inventory::where('id', 24)->update([
            'is_active' => true
        ]);

        // east
        Inventory::where('id', 26)->update([
            'is_active' => true
        ]);

        // switch rush to inactive
        Inventory::where('id', 18)->update([
            'is_active' => false
        ]);

        // east
        Inventory::where('id', 25)->update([
            'is_active' => false
        ]);
    }
}
