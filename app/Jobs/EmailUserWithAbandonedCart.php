<?php

namespace App\Jobs;

use App\Cart;
use App\Mail\UserRegistered;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EmailUserWithAbandonedCart implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $cart;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Cart $f)
    {
        $this->user = $user;
        $this->cart = $f;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find(131);
        if (env('APP_ENV') === 'production') {
            Mail::to($this->user->email)
                ->queue(new UserRegistered($this->user));
        }
        // try {
        //     Mail::to($this->user->email)
        //         ->queue(new UserAbandonedCart($this->user, $this->cart));
        // } catch (Exception $e) {
        //     return $e->getMessage();
        // }
    }
}
