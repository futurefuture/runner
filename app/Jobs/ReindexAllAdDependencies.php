<?php

namespace App\Jobs;

use App\Ad;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReindexAllAdDependencies implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ads = Ad::where('state', 'active')->get();
        $now =  Carbon::now();

        foreach ($ads as $a) {
            $endDate = Carbon::parse($a->end_date);

            if ($now->greaterThanOrEqualTo($endDate)) {
                $a->state = 'completed';
                $a->save();
            }
        }

        dispatch(new ReindexProductTags);
        dispatch(new ReindexCategoryProduct);
        dispatch(new ReindexCarouselComponents);
    }
}
