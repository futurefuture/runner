<?php

namespace App\Jobs;

use App\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Nexmo\Laravel\Facade\Nexmo;

class ScheduleOrderReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;

        if (env('APP_ENV') === 'production') {
            try {
                Nexmo::message()->send([
                    'to'   => '16477746899',
                    'from' => '12493155516',
                    'text' => 'Order#'.$order->id.' is scheduled for '.$order->schedule_time,
                ]);

                Nexmo::message()->send([
                    'to'   => '14164564322',
                    'from' => '12493155516',
                    'text' => 'Order#'.$order->id.' is scheduled for '.$order->schedule_time,
                ]);
            } catch (Exception $e) {
                Log::info('Queue job for schedule order reminder doesnt work because of '.$e->getMessage());
            }
        }
    }
}
