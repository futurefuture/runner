<?php

namespace App\Jobs;

use App\Carousel;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReindexCarouselComponents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $carousels = Carousel::all();

        foreach ($carousels as $c) {
            $ads = $c->ads()
                ->where('state', '!=', 'completed')
                ->get();

            foreach ($ads as $a) {
                $carouselComponentWithAdIndex = DB::table('carousel_carousel_component')
                        ->where('carousel_id', $c->id)
                        ->where('index', $a->index)
                        ->get();

                if ($carouselComponentWithAdIndex) {
                    foreach ($carouselComponentWithAdIndex as $cc) {
                        DB::table('carousel_carousel_component')
                                ->where('id', $cc->id)
                                ->update([
                                    'index' => 300
                                ]);
                    }
                }

                DB::table('carousel_carousel_component')
                    ->updateOrInsert(
                        [
                            'carousel_component_id' => $a->carousel_component_id,
                            'carousel_id'           => $c->id,
                        ],
                        [
                            'index'      => $a->index
                        ]
                    );
            }
        }
    }
}
