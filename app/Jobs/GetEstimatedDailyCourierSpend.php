<?php

namespace App\Jobs;

use App\User;
use App\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Exports\EstimatedDailyCourierSpend;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\EstimatedDailyCourierSpendMail;

class GetEstimatedDailyCourierSpend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startDate = Carbon::now()->subDays(2)->toDateString();
        $endDate   = Carbon::now()->subDays(1)->toDateString();
        $orders    = Order::where('status', 3)
            ->whereBetween('delivered_at', [
                $startDate,
                $endDate
            ])
            ->get();
        $runners = [];

        foreach ($orders as $o) {
            $runner     = User::find($o->runner_1);
            $runnerName = $runner->first_name . ' ' . $runner->last_name;

            $actualCogs = 0;
            $content    = json_decode($o->content);

            foreach ($content as $i) {
                $actualCogs += $i->retailPrice * $i->quantity;
            }

            if (! array_key_exists($runnerName, $runners)) {
                $runners[$runnerName] = $actualCogs;
            } else {
                $runners[$runnerName] += $actualCogs;
            }
        }

        $file = Excel::download(new EstimatedDailyCourierSpend($runners), 'estimated_daily_courier_spend' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'jarek@getrunner.io',
            'jake@getrunner.io',
            'anand@getrunner.io'
        ];

        Mail::send(new EstimatedDailyCourierSpendMail($startDate, $endDate, $file, $emails));
    }
}
