<?php

namespace App\Jobs;

use App\Task;
use App\User;
use App\Order;
use App\Address;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TempCleanup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Order::withTrashed()->select('user_id')->chunk(100, function ($orders) {
            foreach ($orders as $o) {
                // dd($o);
                $user = User::withTrashed()->findOrFail($o->user_id);

                // if (! $user) {
                //     // var_dump($o->id);
                //     $o->user_id = 131;
                //     $o->save();
                // }
            }
        });
    }
}
