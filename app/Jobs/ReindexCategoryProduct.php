<?php

namespace App\Jobs;

use App\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReindexCategoryProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $categories = Category::all();

        foreach ($categories as $c) {
            $ads = $c->ads()
                ->where('state', '!=', 'completed')
                ->get();

            foreach ($ads as $a) {
                $categoryProductWithAdIndex = DB::table('category_product')
                    ->where('category_id', $c->id)
                    ->where('index', $a->index)
                    ->get();

                if ($categoryProductWithAdIndex) {
                    foreach ($categoryProductWithAdIndex as $cp) {
                        DB::table('category_product')
                            ->where('id', $cp->id)
                            ->update([
                                'index' => 300
                            ]);
                    }
                }

                DB::table('category_product')
                    ->updateOrInsert(
                        [
                            'product_id'  => $a->product_id,
                            'category_id' => $c->id,
                        ],
                        [
                            'index' => $a->index
                        ]
                    );
            }
        }
    }
}
