<?php

namespace App\Jobs;

use App\Tag;
use Illuminate\Bus\Queueable;
use App\Services\SegmentService;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TrackAdImpression implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $service;
    protected $tag;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $type, Tag $tag)
    {
        $this->type    = $type;
        $this->service = resolve(SegmentService::class);
        $this->tag     = $tag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        if ($this->type === 'tagProductImpression') {
            $products = $this->tag->products;

            foreach ($products as $p) {
                $ads = $p->ads()->active()->where('tag_id', $this->tag->id)->distinct()->get();

                foreach ($ads as $a) {
                    $this->service->trackAdImpression($a);
                }
            }
        }
    }
}
