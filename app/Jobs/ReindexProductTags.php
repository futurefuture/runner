<?php

namespace App\Jobs;

use App\Tag;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReindexProductTags implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tags = Tag::all();

        foreach ($tags as $t) {
            $ads = $t->ads()
                ->where('state', '!=', 'completed')
                ->get();

            foreach ($ads as $a) {
                $productTagWithAdIndex = DB::table('product_tag')
                    ->where('tag_id', $t->id)
                    ->where('index', $a->index)
                    ->get();

                if ($productTagWithAdIndex) {
                    foreach ($productTagWithAdIndex as $p) {
                        DB::table('product_tag')
                            ->where('id', $p->id)
                            ->update([
                                'index' => 300
                            ]);
                    }
                }

                DB::table('product_tag')
                    ->updateOrInsert(
                        [
                            'product_id' => $a->product_id,
                            'tag_id'     => $t->id,
                        ],
                        [
                            'index'      => $a->index
                        ]
                    );
            }
        }
    }
}
