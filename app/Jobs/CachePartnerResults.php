<?php

namespace App\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CachePartnerResults implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startDate = Carbon::now()->firstOfMonth()->toDateString();
        $endDate   = Carbon::now()->toDateString();

        $prevStartDate = Carbon::now()->subMonths(1)->firstOfMonth()->toDateString();
        $prevEndDate   = Carbon::now()->subMonths(1)->toDateString();

        // init client of guzzle
        $client = new Client();

        // auth
        $res = $client->request('POST', 'https://api.getrunner.io/partner/v2/login', [\GuzzleHttp\RequestOptions::JSON => [
            'data' => [
                'attributes' => [
                    'email'       => 'ejgallo@getrunner.io',
                    'password'    => 'ejgallo'
                ],
            ],
        ]]);
        $token   = json_decode($res->getBody()->getContents(), true)['data']['attributes']['accessToken'];
        $headers = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-type'  => 'application/json'
            ]
        ];

        // re-initialize client to get default headers
        $client = new Client($headers);

        $apiArr = [
            // overview
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/add-to-cart?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/order-by-device?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/product-impressions?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/partners/9/overview/product-sales?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview//sales-by-campaign-back-track?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview//sales-by-campaign?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/sales-by-medium?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/session-by-source?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/top-product-pages?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/top-products?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-customer?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-order?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-sales?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/transactions?startDate=' . $startDate . '&endDate=' . $endDate,

            // products
            'https://api.getrunner.io/partner/v2/redshift/partners/9/products?startDate=' . $startDate . '&endDate=' . $endDate,

            // categories
            'https://api.getrunner.io/partner/v2/redshift/partners/9/categories?startDate=' . $startDate . '&endDate=' . $endDate,

            // demographics
            'https://api.getrunner.io/partner/v2/redshift/partners/9/top/postalcode?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/sales-by-age?startDate=' . $startDate . '&endDate=' . $endDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/sales-by-gender?startDate=' . $startDate . '&endDate=' . $endDate,

            // reviews
            'https://api.getrunner.io/partner/v2/partners/9/reviews?startDate=' . $startDate . '&endDate=' . $endDate,
        ];

        $apiArrComparison = [
            // overview
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/add-to-cart?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/order-by-device?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/product-impressions?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/partners/9/overview/product-sales?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview//sales-by-campaign-back-track?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview//sales-by-campaign?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/sales-by-medium?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/session-by-source?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/top-product-pages?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/top-products?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-customer?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-order?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/total-sales?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/overview/transactions?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,

            // products
            'https://api.getrunner.io/partner/v2/redshift/partners/9/products?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,

            // categories
            'https://api.getrunner.io/partner/v2/redshift/partners/9/categories?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,

            // demographics
            'https://api.getrunner.io/partner/v2/redshift/partners/9/top/postalcode?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/sales-by-age?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            'https://api.getrunner.io/partner/v2/redshift/partners/9/sales-by-gender?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,

            // reviews
            'https://api.getrunner.io/partner/v2/partners/9/reviews?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
        ];

        foreach ($apiArr as $api) {
            $request = new \GuzzleHttp\Psr7\Request('GET', $api);
            $promise = $client->sendAsync($request)->then(function ($response) {
                echo 'I completed! ' . $response->getBody();
            });
            $promise->wait();
        }

        foreach ($apiArrComparison as $api) {
            $request = new \GuzzleHttp\Psr7\Request('GET', $api);
            $promise = $client->sendAsync($request)->then(function ($response) {
                echo 'I completed! ' . $response->getBody();
            });
            $promise->wait();
        }

        // products cacheing
        $request = new \GuzzleHttp\Psr7\Request('GET', 'https://api.getrunner.io/partner/v2/redshift/partners/9/products?startDate=' . $startDate . '&endDate=' . $endDate);
        $promise = $client->sendAsync($request)->then(function ($response) {
            $i = 0;
            $top5prod = [];
            foreach (json_decode($response->getBody(), true)['data'] as $prod) {
                array_push($top5prod, $prod);
                $i += 1;
                if ($i == 5) {
                    break;
                }
            }

            return $top5prod;
        });
        $top5prod = $promise->wait();

        foreach ($top5prod as $prod) {
            $prodArr = [
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/broad-category-rank?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/impressions?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/top/postalcode?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-age?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-campaign-back-track?startDate=' . $startDate . '&endDate=' . $endDate,
                // 'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-campaign?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-gender?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-medium?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/session-by-source?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sub-category-rank?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-add-to-cart?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-page-views?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-purchases?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-quantity?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-unique-page-views?startDate=' . $startDate . '&endDate=' . $endDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . 'total-revenue?startDate=' . $startDate . '&endDate=' . $endDate,
            ];

            $prodArrComparison = [
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/broad-category-rank?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/impressions?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/top/postalcode?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-age?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-campaign-back-track?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                // 'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-campaign?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-gender?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sales-by-medium?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/session-by-source?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/sub-category-rank?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-add-to-cart?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-page-views?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-purchases?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-quantity?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . '/total-unique-page-views?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
                'https://api.getrunner.io/partner/v2/redshift/partners/9/products/' . $prod['id'] . 'total-revenue?startDate=' . $prevStartDate . '&endDate=' . $prevEndDate,
            ];

            foreach ($prodArr as $api) {
                $request = new \GuzzleHttp\Psr7\Request('GET', $api);
                $promise = $client->sendAsync($request)->then(function ($response) {
                    echo 'I completed! ' . $response->getBody();
                });
                $promise->wait();
            }

            foreach ($prodArrComparison as $api) {
                $request = new \GuzzleHttp\Psr7\Request('GET', $api);
                $promise = $client->sendAsync($request)->then(function ($response) {
                    echo 'I completed! ' . $response->getBody();
                });
                $promise->wait();
            }
        }
    }
}
