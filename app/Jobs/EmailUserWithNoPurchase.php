<?php

namespace App\Jobs;

use App\Mail\UserSignedUpWithNoPurchase;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EmailUserWithNoPurchase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $thirteenMinutesAgo = Carbon::now()->subMinutes(13)->toDateTimeString();
        $twelveMinutesAgo = Carbon::now()->subMinutes(12)->toDateTimeString();

        $recentUsers = User::whereBetween('created_at', [
            $thirteenMinutesAgo,
            $twelveMinutesAgo,
        ])->get();

        foreach ($recentUsers as $recentUser) {
            if ($recentUser->orders()->count() < 1) {
                if (env('APP_ENV') === 'production') {
                    Mail::to($recentUser->email)
                        ->bcc('service@getrunner.io')
                        ->queue(new UserSignedUpWithNoPurchase($recentUser));
                }
            }
        }
    }
}
