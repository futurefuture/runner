<?php

namespace App\Jobs;

use App\Order;
use Carbon\Carbon;
use App\Exports\OrderDetails;
use Illuminate\Bus\Queueable;
use App\Mail\OrderDetailsMail;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetActualCogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startDate = Carbon::now()->subDays(2)->toDateString();
        $endDate   = Carbon::now()->subDays(1)->toDateString();

        $orders = Order::whereBetween('delivered_at', [
            $startDate,
            $endDate
        ])->get();

        $array = [];

        foreach ($orders as $o) {
            $actualCogs = 0;
            $content    = json_decode($o->content);

            foreach ($content as $i) {
                $actualCogs += $i->retailPrice * $i->quantity;
            }

            $actualTotal = $o->subtotal + $o->delivery + $o->display_tax_total + $o->tip;

            $d = [
                    'id'          => $o->id,
                    'status'      => $o->status,
                    'discount'    => $o->discount,
                    'actualTotal' => $actualTotal,
                    'total'       => $o->total,
                    'actualCogs'  => $actualCogs,
                    'cogs'        => $o->cogs,
                    'difference'  => $actualTotal - $o->total
                ];

            array_push($array, $d);
        }

        $file = Excel::download(new OrderDetails($array), 'order_details_' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'jarek@getrunner.io',
            'jake@getrunner.io',
            'anand@getrunner.io'
        ];

        Mail::send(new OrderDetailsMail($startDate, $endDate, $file, $emails));
    }
}
