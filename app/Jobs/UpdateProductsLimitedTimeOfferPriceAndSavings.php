<?php

namespace App\Jobs;

use App\Product;
use App\IncentiveProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class updateProductsLimitedTimeOfferPriceAndSavings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $limitedTimeOffers = IncentiveProduct::where('is_active', 1)->where('incentive_id', 1)->get();

        foreach ($limitedTimeOffers as $l) {
            $product = Product::find($l->product_id);

            $product->update([
                'limited_time_offer_savings' => $l->savings,
                'limited_time_offer_price'   => $product->runner_price - $l->savings
            ]);
        }
    }
}
