<?php

namespace App;

use App\Inventory;
use App\Territory;
use Illuminate\Database\Eloquent\Model;

class StoreLayout extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_layouts';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'layout'       => 'array',
        'notification' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id',
        'territory_id',
        'layout',
    ];

    public function territory()
    {
        return $this->belongsTo(Territory::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
