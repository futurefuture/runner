<?php

namespace App;

use App\Utilities\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    const color = [
        1  => '#F65EB7',
        2  => '#F35EF6',
        3  => '#8F5EF6',
        4  => '#5EA4F6',
        5  => '#5EE3F6',
        6  => '#5EF6AD',
        7  => '#9EF65E',
        8  => '#F6CF5E',
        9  => '#F68D5E',
        10 => '#FF9ED7',
        11 => '#FD90FF',
        12 => '#B895FF',
        13 => '#7AB5FB',
        14 => '#8DF1FF',
        15 => '#99FFCE',
        16 => '#C4FF9A',
        17 => '#FFE7A0',
        18 => '#FC9C72',
    ];

    use SoftDeletes;
    protected $table = 'schedules';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'job_type',
        'start',
        'end',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
