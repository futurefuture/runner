<?php

namespace App\Utilities\Jobs;

use DB;
use Exception;
use App\Product;
use App\Category;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class ProductUpdateAutomation
{
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function run()
    {
        $client                   = new Client();
        $now                      = Carbon::now();
        $alreadyExistingIncentive = null;

        try {
            if (Schema::hasTable('products_new')) {
                DB::statement('Drop table products_new');
            }

            if (Schema::hasTable('incentive_product_new')) {
                DB::statement('Drop table incentive_product_new');
            }
            $response           = $client->request('GET', 'https://api.lcbo.com/v6/products');
            $formattedResponse  = json_decode($response->getBody()->getContents());
            $totalOffsetInteger = ($formattedResponse->resultCount / 100) + 1;

            $totalOffsetRemaining = $formattedResponse->resultCount % 100;

            $createTableSqlString = 'CREATE TABLE products_new as select * from products';

            $product_new = DB::statement($createTableSqlString);

            $createTableProductIncentive = 'CREATE TABLE incentive_product_new as select * from incentive_product';

            $incentive_product = DB::statement($createTableProductIncentive);

            $autoincrementIncentive = 'ALTER TABLE incentive_product_new CHANGE id  id INT(10)AUTO_INCREMENT PRIMARY KEY';

            $incentive_product_alter = DB::statement($autoincrementIncentive);

            $truncateTableCommand  = 'TRUNCATE TABLE products_new';

            //$product_new_table_truncate = DB::statement($truncateTableCommand);

            $autoincrement = 'ALTER TABLE products_new CHANGE id  id INT(10)AUTO_INCREMENT PRIMARY KEY';

            $product_new_autoincrement = DB::statement($autoincrement);

            $paginationOffset = DB::select("select payload from failed_jobs where connection ='product_import' and date(failed_at) =" . "'" . $now->toDateString() . "'" . ' order by id desc limit 1 ');

            if (json_decode($paginationOffset[0]->payload)->paginationOffset > 0) {
                $i = json_decode($paginationOffset[0]->payload)->paginationOffset;
            } else {
                $i = 0;
            }

            for ($i; $i < $totalOffsetInteger + 1; $i++) {
                $response = $client->request('GET', 'https://api.lcbo.com/v6/products/?numProductsPerPage=1000&paginationOffset=' . $i);

                $formattedResponse = json_decode($response->getBody()->getContents());
                $products          = $formattedResponse->products;

                foreach ($products as $product) {
                    if ($product->availability === 'ONLINE_ONLY') {
                        continue;
                    }

                    if ($product->itemNumber === '666032') {
                        echo 'got it';
                    }

                    // clean up retail price
                    $retailPrice = ! $product->limitedTimeOffer ? (int) (str_replace('$ ', '', $product->price) * 100) : (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100);

                    $runnerProduct = Product::withTrashed()->where('sku', $product->itemNumber)->first();

                    if ($product->subSubCategoryName == '') {
                        $category = Category::where('title', $product->subSubCategoryName)->first();
                    }

                    $productUpdate = new ProductUpdateAutomation();

                    $runnerProduct = DB::select('Select * from products_new where sku ='
                        . $product->itemNumber);

                    if (! $runnerProduct) {
                        DB::table('products_new')->insert([
                            'sku'               => $product->itemNumber,
                            'upc'               => (int) $product->upcNumber,
                            'title'             => $product->itemName,
                            'long_description'  => $product->tastingNotes,
                            'retail_price'      => $retailPrice,
                            'markup_percentage' => 0.12,
                            'markup'            => (int) $retailPrice * 0.12,
                            'runner_price'      => (int) $retailPrice * 1.12,
                            'image'             => $product->imageUrl,
                            'image_thumbnail'   => $product->imageUrlThumb,
                            'packaging'         => $productUpdate->filterPackage($product->productSize, $product->sellingPackage),
                            'alcohol_content'   => $product->alcoholPercentage * 100,
                            'sugar_content'     => (int) $product->sugarContent > 0 ? (int) $product->sugarContent : null,
                            'slug'              => Str::slug($product->itemName . '-' . $productUpdate->filterPackage($product->productSize, $product->sellingPackage)),
                            'producing_country' => $product->producingCountry,
                            'producer'          => $product->producer,
                            'category_id'       => isset($category->id) ? $category->id : 1,
                            'created_at'        => $now,
                            'updated_at'        => $now
                        ]);
                    } else {
                        $updateProduct =  DB::table('products_new')->update([
                            'retail_price' => $retailPrice,
                            'markup'       => (int) $retailPrice * 0.12,
                            'runner_price' => (int) $retailPrice * 1.12
                        ]);

                        $alreadyExistingIncentive = DB::table('incentive_product_new')
                            ->where('product_id', $runnerProduct[0]->id)
                            ->where('start_date', Carbon::now()->toDateString())
                            ->first();
                    }

                    if ($product->limitedTimeOffer) {
                        $savings = (int) (str_replace('$ ', '', $product->ltoRegularPrice) * 100) - (int) (str_replace('$ ', '', $product->price) * 100);

                        if (! $alreadyExistingIncentive && $runnerProduct) {
                            DB::table('incentive_product_new')->insert([
                                'incentive_id' => 1,
                                'product_id'   => $runnerProduct[0]->id,
                                'start_date'   => Carbon::now()->toDateString(),
                                'end_date'     => Carbon::parse($product->ltoExpiration)->toDateString(),
                                'savings'      => $savings,
                                'is_active'    => 1,
                            ]);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e;
        }

        $totalOldProduct = DB::select('select count(*) from products');
        $totalProduct    = DB::select('select count(*) from products_new');

        if ($totalOldProduct == $totalProduct || $totalProduct > $totalOldProduct) {
            $renameProductsTable   = 'RENAME TABLE products TO product_old_table';
            $products_table_rename = DB::statement($renameProductsTable);

            $renameProductNewTable    = 'RENAME TABLE products_new to products';
            $product_new_table_rename = DB::statement($renameProductNewTable);

            $dropProductOldTable = 'Drop table product_old_table';
            $dropTable           =  DB::statement($dropProductOldTable);

            $renameProductIncentive =  'RENAME TABLE incentive_product to incentive_product_old';
            $incentive_product_old  = DB::statement($renameProductIncentive);

            $renameProductIncentiveNew =  'RENAME TABLE incentive_product_new to incentive_product';
            $incentive_product_new     = DB::statement($renameProductIncentiveNew);
        }
    }

    private function filterPackage($productSize, $sellingPackage)
    {
        if (strpos($productSize, 'x')) {
            $productSize = str_replace(' x ', 'x', $productSize);
        }

        if (strpos($productSize, 'mL')) {
            $productSize = str_replace(' mL', 'mL', $productSize);
        }

        return $productSize . ' ' . $sellingPackage;
    }
}
