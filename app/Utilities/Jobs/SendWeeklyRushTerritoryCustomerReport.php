<?php

namespace App\Utilities\Jobs;

use App\Exports\TorontoRushTerritoryCustomers;
use App\Mail\TorontoRushTerritoryCustomerMail;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Mail;

class SendWeeklyRushTerritoryCustomerReport
{
    public static function run()
    {
        $formattedSales = [];
        $startDate      = Carbon::now()->SubDays(7)->toDateString();
        $endDate        = Carbon::now()->toDateString();
        $teritorySouth  = ['M6S', 'M6P', 'M6R', 'M6K', 'M6H', 'M6G', 'M6J', 'M5V', 'M5T', 'M5S', 'M5R', 'M4W', 'M4Y', 'M4X', 'M5G', 'M5B', 'M5H', 'M5X', 'M5E', 'M5C', 'M5A', 'M5K', 'M5L', 'M5W', 'M5J'];

        $customer = new self();

        array_push($formattedSales, ['user' => 'Rush Toronto South', 'total' => null, 'day' => null]);

        $type           = null;
        $southTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritorySouth, $formattedSales, $type);

        $file = Excel::download(new TorontoRushTerritoryCustomers($southTerritory), 'rush_territory_customer-export-' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'coulter@getcharly.com',
            'ops@getrunner.io'
        ];

        Mail::send(new TorontoRushTerritoryCustomerMail($startDate, $endDate, $file, $emails));
    }

    public function territoryCustomerQuery($startDate, $endDate, array $postalCode, $formattedSales, $type)
    {
        $ps = implode("','", $postalCode);
        $ps = "'" . $ps . "'";

        $custQuery = 'select "Total User" as user,count(*) as TotalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 2 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as totalcust group by day UNION select "New User" as user,count(*) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 and oi.first_time = 1 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 2 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as totalcust group by day UNION select "MAP 1+" as user,count(user_id) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 2 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as map where  totalOrder>1 group by day UNION  select "MAP 5+" as user,count(user_id) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3  AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 2 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as map where  totalOrder>5 group by day';

        dd($custQuery);
        $resCustQuery = DB::select($custQuery);

        foreach ($resCustQuery as $res) {
            $sales = [];

            $sales['user']  = $res->user;
            $sales['day']   = $res->day;
            $sales['total'] = $res->TotalUser;

            array_push($formattedSales, $sales);
        }

        array_push($formattedSales, ['user' => $type, 'total' => null, 'day' => null]);

        return $formattedSales;
    }
}
