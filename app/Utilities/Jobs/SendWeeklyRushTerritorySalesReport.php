<?php

namespace App\Utilities\Jobs;

use App\Exports\TorontoRushTerritorySales;
use App\Mail\TorontoRushTerritorySalesMail;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Mail;

class SendWeeklyRushTerritorySalesReport
{
    public static function run($startDate, $endDate)
    {
        $formattedSales = [];
        $nullArr        = ['day' => 'Rush Toronto South', 'totalOrder' => null, 'retailPrice' => null, 'markup' => null, 'delivery' => null, 'tip' => null, 'discount' => null];
        //$startDate      = Carbon::now()->SubDays(7)->toDateString();
        //$endDate        = Carbon::now()->toDateString();

        $teritorySouth = ['M6S', 'M6P', 'M6R', 'M6K', 'M6H', 'M6G', 'M6J', 'M5V', 'M5T', 'M5S', 'M5R', 'M4W', 'M4Y', 'M4X', 'M5G', 'M5B', 'M5H', 'M5X', 'M5E', 'M5C', 'M5A', 'M5K', 'M5L', 'M5W', 'M5J'];

        $order = new self();

        array_push($formattedSales, $nullArr);

        $type           = null;
        $southTerritory = $order->territoryCustomerQuery($startDate, $endDate, $teritorySouth, $formattedSales, $type);

        $file = Excel::download(new TorontoRushTerritorySales($southTerritory), 'rush_territory_order_sales_export_' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'ops@getrunner.io',
            'coulter@getcharly.com'
        ];

        Mail::send(new TorontoRushTerritorySalesMail($startDate, $endDate, $file, $emails));
    }

    public function territoryCustomerQuery($startDate, $endDate, array $postalCode, $formattedSales, $type)
    {
        $nullArr = ['day' => $type, 'totalOrder' => null, 'retailPrice' => null, 'markup' => null, 'delivery' => null, 'tip' => null, 'discount' => null];
        $ps      = implode("','", $postalCode);
        $ps      = "'" . $ps . "'";

        $orderQuery = 'select  day,SUM(totalOrder) as totalOrder,SUM(markup)/100 as markup,SUM(delivery)/100 as delivery,SUM(tip)/100 as tip,SUM(discount)/100 as discount from (SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode,date(o.created_at) as day, SUM(o.markup) as markup,SUM(o.delivery) as delivery,SUM(o.tip) as tip,count(o.id) as totalOrder,SUM(o.discount) as discount  FROM orders AS o INNER JOIN addresses AS a ON o.address_id = a.id WHERE o.STATUS = 3 and o.store_id = 2 AND date(o.created_at) between ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . 'and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,date(o.created_at) ) as post group by day';

        $resOrderQuery = DB::select($orderQuery);

        foreach ($resOrderQuery as $res) {
            $sales       = [];
            $retailQuery = 'select Sum(retail_price)/100 as retail_price from (SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode,SUM(o.retail_price * o.quantity) as retail_price, date(o.created_at) day FROM order_items AS o INNER JOIN orders AS oi ON oi.id = o.order_id INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 and oi.store_id = 2 AND date(o.created_at)= ' . " '" . $res->day . "' " . 'and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,date(o.created_at)) as post group by day';

            $resRetail = DB::select($retailQuery) ? DB::select($retailQuery)['0']->retail_price : 0;
            // $sales['name']       = $res->name;
            $sales['day']         = $res->day;
            $sales['totalOrder']  = $res->totalOrder;
            $sales['retailPrice'] = $resRetail;
            $sales['markup']      = $res->markup;
            $sales['delivery']    = $res->delivery;
            $sales['tip']         = $res->tip;
            $sales['discount']    = $res->discount;

            array_push($formattedSales, $sales);
        }
        array_push($formattedSales, $nullArr);

        return $formattedSales;
    }
}
