<?php

namespace App\Utilities\Jobs;

use App\Exports\TorontoLCBOTerritoryCustomers;
use App\Mail\TorontoLCBOTerritoryCustomerMail;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Mail;

class SendWeeklyLCBOTerritoryCustomerReport
{
    public static function run()
    {
        $formattedSales = [];
        $startDate      = Carbon::now()->SubDays(7)->toDateString();
        $endDate        = Carbon::now()->toDateString();
        $teritorySouth  = ['M6S', 'M6P', 'M6R', 'M6K', 'M6H', 'M6G', 'M6J', 'M5V', 'M5T', 'M5S', 'M5R', 'M4W', 'M4Y', 'M4X', 'M5G', 'M5B', 'M5H', 'M5X', 'M5E', 'M5C', 'M5A', 'M5K', 'M5L', 'M5W', 'M5J'];

        $teritoryEast        = ['M4M', 'M4K', 'M4H', 'M4J', 'M4L', 'M4E', 'M4C', 'M4B'];
        $teritoryNorth       = ['M3K', 'M3H', 'M2R', 'M2N', 'M2M', 'M2K', 'M2J', 'M2H', 'M3M'];
        $teritoryEtobicoke   = ['M8Y', 'M8X', 'M8V', 'M8Z', 'M9A', 'M9B', 'M9C', 'M8W', 'M9P', 'M9R'];
        $teritoryMississauga = ['L5N', 'L5M', 'L5L', 'L5K', 'L5J', 'L5W', 'L5V', 'L5C', 'L5H', 'L5R', 'L5B', 'L5G', 'L4Z', 'L5A', 'L4W', 'L4Y', 'L5E', 'L4X', 'L5S'];

        $teritoryCentral = ['M6N', 'M6E', 'M6C', 'M5P', 'M4V', 'M4T', 'M4G', 'M3C', 'M4A', 'M4P', 'M4R', 'M5N', 'M6B', 'M6M', 'M9N', 'M6L', 'M6A', 'M5M', 'M4N', 'M3B', 'M3A', 'M2L', 'M2P', 'M4P', 'M4S'];

        $customer = new self();

        array_push($formattedSales, ['user' => 'LCBO Toronto South', 'total' => null, 'day' => null]);

        $type           = 'LCBO Toronto East';
        $southTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritorySouth, $formattedSales, $type);

        $eastType      = 'LCBO Toronto Central';
        $eastTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritoryEast, $southTerritory, $eastType);

        $centralType      = 'LCBO Toronto North';
        $centralTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritoryCentral, $eastTerritory, $centralType);

        $northType      = 'LCBO Toronto Etobicoke';
        $northTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritoryNorth, $centralTerritory, $northType);

        $etobicokeType      = 'LCBO Toronto Mississauga';
        $etobicokeTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritoryEtobicoke, $northTerritory, $etobicokeType);

        $missiType      = null;
        $missiTerritory = $customer->territoryCustomerQuery($startDate, $endDate, $teritoryMississauga, $etobicokeTerritory, $missiType);

        $file = Excel::download(new TorontoLCBOTerritoryCustomers($missiTerritory), 'territory_customer-export-' . $startDate . '-' . $endDate . '.xlsx')->getFile();

        $emails = [
            'coulter@getcharly.com',
            'ops@getrunner.io'
        ];

        Mail::send(new TorontoLCBOTerritoryCustomerMail($startDate, $endDate, $file, $emails));
    }

    public function territoryCustomerQuery($startDate, $endDate, array $postalCode, $formattedSales, $type)
    {
        $ps = implode("','", $postalCode);
        $ps = "'" . $ps . "'";

        $custQuery = 'select "Total User" as user,count(*) as TotalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 1 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as totalcust group by day UNION select "New User" as user,count(*) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 and oi.first_time = 1 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 1 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as totalcust group by day UNION select "MAP 1+" as user,count(user_id) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3 AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 1 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as map where  totalOrder>1 group by day UNION  select "MAP 5+" as user,count(user_id) as totalUser,day from(SELECT SUBSTRING(a.postal_code, 1, 3) AS extractcode, count(oi.id) AS totalOrder,oi.user_id,date(oi.created_at) as day FROM orders as oi INNER JOIN addresses AS a ON oi.address_id = a.id WHERE oi.STATUS = 3  AND oi.created_at BETWEEN ' . " '" . $startDate . "' " . ' and ' . " '" . $endDate . "' " . ' and oi.store_id = 1 and SUBSTRING(a.postal_code, 1, 3) In(' . $ps . ') GROUP BY extractcode,user_id,date(oi.created_at) ORDER BY totalOrder desc) as map where  totalOrder>5 group by day';

        $resCustQuery = DB::select($custQuery);

        foreach ($resCustQuery as $res) {
            $sales = [];

            $sales['user']  = $res->user;
            $sales['day']   = $res->day;
            $sales['total'] = $res->TotalUser;

            array_push($formattedSales, $sales);
        }

        array_push($formattedSales, ['user' => $type, 'total' => null, 'day' => null]);

        return $formattedSales;
    }
}
