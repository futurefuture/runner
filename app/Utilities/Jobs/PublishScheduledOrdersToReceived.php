<?php

namespace App\Utilities\Jobs;

use App\Events\NewOrder;
use App\Order;
use Carbon\Carbon;

class PublishScheduledOrdersToReceived
{
    public static function run()
    {
        $scheduledOrders = Order::Where('status', '=', 7)->whereDate('schedule_time', '>=', Carbon::today()->toDateString())->get();

        foreach ($scheduledOrders as $order) {
            $scheduleTime = Carbon::parse($order->schedule_time);
            $minDiff      = $scheduleTime->diffInMinutes(Carbon::now());
            $hours        = (int) (ceil($minDiff / 60));

            if ($hours <= 3) {
                $order->status = Order::IS_RECEIVED;
                $order->save();

                event(new NewOrder($order));
            }
        }

        return true;
    }
}
