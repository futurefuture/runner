<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersLimit implements Filter
{
    public function __invoke(Builder $query, $value, string $property) : Builder
    {
        return $query->limit($value);
    }
}
