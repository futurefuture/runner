<?php

namespace App\Utilities\Filters;

class PostalCodeFilter extends QueryFilter
{
    public function postalCode($code)
    {
        if ($code) {
            $postalCodeInput = explode(' ', $code)[0];

            return  $this->builder->where('postal_code', 'like', '%'.$postalCodeInput.'%');
        } else {
            return;
        }
    }
}
