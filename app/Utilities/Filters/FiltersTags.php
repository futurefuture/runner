<?php

namespace App\Utilities\Filters;

use DB;
use App\Ad;
use App\Tag;
use Carbon\Carbon;
use App\Services\SegmentService;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersTags implements Filter
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        $now = Carbon::now();
        $tagArray = [];

        if (!is_array($value)) {
            $value = [$value];
        }


        $query->join('product_tag', 'products.id', '=', 'product_tag.product_id')
            ->join('tags', 'tags.id', '=', 'product_tag.tag_id');

        foreach ($value as $filter) {
            $tagId = Tag::where('slug', $filter)->first() ? Tag::where('slug', $filter)->first()->id : -1;

            // track ad impression
            $ad = Ad::where('ad_type_id', 1)
                ->where('tag_id', $tagId)
                ->where('start_date', '<=', $now)
                ->where('end_date', '>', $now)
                ->first();

            if ($ad) {
                $segmentService = resolve(SegmentService::class);
                $segmentService->trackAdImpression($ad);
            }
            if ($tagId !== -1) {
                array_push($tagArray, $tagId);
            }

            if (count($value) < 2) {
                $query->where('product_tag.tag_id', '=', $tagId);
            }
        }

        $subq = 'SELECT product_id, count FROM(SELECT count(*)AS count, product_id FROM( SELECT DISTINCT product_id, tag_id FROM product_tag WHERE tag_id in(' . implode(",", $tagArray) . ')) AS p GROUP BY product_id) AS p2 WHERE count >= ' . count($tagArray) . '';


        if (count($value) >= 2) {
            $query->select('products.*', 'quantity')
                ->joinSub($subq, 'subq', function ($join) {
                    $join->on('products.id', '=', 'subq.product_id');
                })
                ->groupBy('products.id', 'quantity');
        } else {
            $query->select('products.*', 'quantity', 'product_tag.index')
                ->orderBy('product_tag.index', 'asc');
        }

        return $query;
    }
}
