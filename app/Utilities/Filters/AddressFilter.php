<?php

namespace App\Utilities\Filters;

class AddressFilter extends QueryFilter
{
    public function userId($user_id = null)
    {
        return $this->builder->where('user_id', $user_id);
    }
}
