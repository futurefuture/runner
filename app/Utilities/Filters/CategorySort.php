<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;

class CategorySort implements \Spatie\QueryBuilder\Sorts\Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'DESC' : 'ASC';

        $query->whereHas('category', function (Builder $query) {
            return $query->orderBy('index', 'desc');
        });
        // $query->orderByRaw("LENGTH(`{$property}`) {$direction}");
    }
}
