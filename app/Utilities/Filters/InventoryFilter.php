<?php

namespace App\Utilities\Filters;

class InventoryFilter extends QueryFilter
{
    public function limit($limit)
    {
        if ($limit) {
            return $this->builder->take($limit);
        } else {
            return $this->builder->paginate(20);
        }
    }
}
