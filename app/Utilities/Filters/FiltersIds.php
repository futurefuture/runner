<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersIds implements Filter
{
    public function __invoke(Builder $query, $value, string $property) : Builder
    {
        if (! is_array($value)) {
            $value = [$value];
        }

        foreach ($value as $filter) {
            $query->where('id', $filter);
        }

        return $query;
    }
}
