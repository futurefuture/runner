<?php

namespace App\Utilities\Filters;

class UserFilter extends QueryFilter
{
    public function role($role = 0)
    {
        if (! is_array($role)) {
            $role = [$role];
        }

        return  $this->builder->WhereIn('role', $role);
    }
}
