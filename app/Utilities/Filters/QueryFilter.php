<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class QueryFilter
{
    protected $request;

    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        foreach ($this->requests() as $name => $value) {
            $name = Str::camel($name);
            if (method_exists($this, $name)) {
                call_user_func_array([$this, $name], [$value]);
            }
        }

        return $this->builder;
    }

    public function random()
    {
        return $this->builder->inRandomOrder();
    }

    public function latest()
    {
        return $this->builder->latest();
    }

    public function recent()
    {
        return $this->builder->orderBy('updated_at', 'desc');
    }

    public function requests()
    {
        return $this->request->all();
    }

    public function ajax()
    {
        return $this->request->ajax();
    }

    public function all()
    {
        return $this->request->all();
    }

    public function input($string)
    {
        return $this->request->input($string);
    }

    public function has($string)
    {
        return $this->request->has($string);
    }

    public function userId($id = null)
    {
        return $this->builder->where('user_id', $id);
    }

    public function status($status = 0)
    {
        $allStatus = explode(',', $status);

        return $this->builder->whereIn('status', $allStatus)->orderBy('created_at', 'desc');
    }
}
