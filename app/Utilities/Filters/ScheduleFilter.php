<?php

namespace App\Utilities\Filters;

use Carbon\Carbon;
use DB;

class ScheduleFilter extends QueryFilter
{
    public function filters($filter)
    {
        $now       = Carbon::now();
        $relatable = [
            'user',
            'user.runnerlocations'
        ];

        if ($filter === 'onDuty') {
            return $this->builder->where(DB::raw('HOUR(end)'), '>', $now->hour)->whereDate('start', '>=', $now->toDateString())->whereDate('end', '<=', $now->toDateString())->with($relatable);
        } elseif ($filter === 'onDutyNow') {
            return $this->builder->where(DB::raw('HOUR(start)'), '<', $now->hour)->where(DB::raw('HOUR(end)'), '>', $now->hour)->whereDate('start', '>=', $now->toDateString())->whereDate('end', '<=', $now->toDateString())->with($relatable);
        }
    }

    public function day($date)
    {
        $startDate = Carbon::parse($date);
        $endDate   = Carbon::parse($date)->addDays(1);

        return $this->builder->whereDate('start', '=', $startDate)->whereDate('end', '<=', $endDate);
    }

    public function week($date)
    {
        $startDate = Carbon::parse($date);
        $endDate   = Carbon::parse($date)->addDays(7);

        return $this->builder->whereDate('start', '>=', $startDate)->whereDate('end', '<=', $endDate);
    }

    public function month($date)
    {
        $startDate = Carbon::parse($date);
        $endDate   = Carbon::parse($date)->addDays(30);

        return $this->builder->whereDate('start', '>=', $startDate)->whereDate('end', '<=', $endDate);
    }

    public function q($q)
    {
        if ($q) {
            $this->builder->whereHas('user', function ($query) use ($q) {
                return $query->where('first_name', '=', $q);
            });
        } else {
            return;
        }
    }

    public function job_type($type)
    {
        if ($type) {
            return $this->builder->where('job_type', '=', $type);
        }
    }
}
