<?php

namespace App\Utilities\Filters;

use App\Order;
use Carbon\Carbon;

class OrderFilter extends QueryFilter
{
    public function query($query)
    {
        if ($query == 'ready') {
            return $this->builder->whereIn('status', [Order::IS_RECEIVED, Order::IS_SCHEDULED])
                ->orderBy('created_at', 'desc')
                ->whereNull('runner_1')
                ->whereNull('runner_2');
        } elseif ($query == 'received') {
            return $this->builder->where('status', Order::IS_RECEIVED)
                ->orderBy('created_at', 'desc')
                ->paginate(50);
        } elseif ($query == 'scheduled') {
            return $this->builder->where('status', Order::IS_SCHEDULED);
        } elseif ($query == 'in_progress') {
            return $this->builder->whereIn('status', [Order::IS_IN_PROCESSING, Order::IS_ON_ROUTE]);
        } else {
            return $this->builder->whereIn('status', [Order::IS_DELIVERED, Order::IS_CANCELLED]);
        }
    }

    public function startDate($date)
    {
        $startDate = Carbon::parse($date);

        return $this->builder->whereDate('created_at', '>=', $startDate);
    }

    public function endDate($date)
    {
        $endDate = Carbon::parse($date);

        return $this->builder->whereDate('created_at', '<=', $endDate);
    }
}
