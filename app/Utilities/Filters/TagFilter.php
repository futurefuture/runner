<?php

namespace App\Utilities\Filters;

use App\Tag;

class TagFilter extends QueryFilter
{
    public function filters($title)
    {
        $filterNames = explode(',', $title);
        if ($filterNames) {
            return $this->builder->whereIn('title', $filterNames);

            /*  $this->builder->whereHas('products', function ($query) use ($tagIds) {
                return $query->whereIn('tag_id', $tagIds);
            });*/
        }
    }

    public function type($type)
    {
        return $this->builder->where('type', $type);
    }
}
