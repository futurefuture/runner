<?php

namespace App\Utilities\Filters;

use App\Order;
use App\OrderItem;
use Carbon\Carbon;

class OrderItemFilter extends QueryFilter
{
    public function q($q)
    {
        if ($q) {
            return  $this->builder->where('title', 'like', '%'.$q.'%');
        } else {
            return;
        }
    }

    public function dateBetween($date)
    {
        $startDate = Carbon::parse($date['0']);
        $endDate = Carbon::parse($date['1']);

        return $this->builder->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
    }

    public function startDate($date)
    {
        $startDate = Carbon::parse($date);

        return $this->builder->whereDate('created_at', '>=', $startDate);
    }

    public function endDate($date)
    {
        $endDate = Carbon::parse($date);

        return $this->builder->whereDate('created_at', '<=', $endDate);
    }
}
