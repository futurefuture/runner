<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class InventoryProduct extends Model
{
    protected $table = 'inventory_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inventory_id',
        'product_id',
        'quantity',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
