<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunnerLocation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'runner_location';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude',
        'longitude',
        'runner_id',
    ];

    public function runner()
    {
        return $this->belongsTo(self::class);
    }
}
