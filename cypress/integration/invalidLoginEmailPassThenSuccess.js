describe('The Home Page', () => {
  it('successfully loads', () => {
    // invalid email
    cy.visit('/login');
    cy.get('input[name=email]').type('emailneedsmail');
    cy.get('input[name=password]').type('runner12345');
    cy.get('#button-component').click();
    cy.wait(500);
    cy.get('.notification-content').should(($notificationContent) => {
      expect($notificationContent.first()).to.contain('The email must be a valid email address.');
    });
    cy.reload();
    // email does not exist
    cy.visit('/login');
    cy.get('input[name=email]').type('emailneedsmail@mail.com');
    cy.get('input[name=password]').type('runner12345');
    cy.get('#button-component').click();
    cy.wait(500);
    cy.get('.notification-content').should(($notificationContent) => {
      expect($notificationContent.first()).to.contain('Invalid Credentials');
    });
    cy.reload();
    // incorrect password
    cy.visit('/login');
    cy.get('input[name=email]').type('jarek@wearefuturefuture.com');
    cy.get('input[name=password]').type('yourmom2');
    cy.get('#button-component').click();
    cy.wait(500);
    cy.get('.notification-content').should(($notificationContent) => {
      expect($notificationContent.first()).to.contain('Invalid Credentials');
    });
    cy.reload();
    // success
    cy.visit('/login');
    cy.get('input[name=email]').type('jarek@wearefuturefuture.com');
    cy.get('input[name=password]').type('yourmom1');
    cy.get('#button-component').click();
    cy.wait(5000);
    cy.url().should('eq', 'https://www.runner.test/');
  });
});
