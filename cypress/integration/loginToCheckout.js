describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/login');
    cy.get('input[name=email]').type('jarek@wearefuturefuture.com');
    cy.get('input[name=password]').type('yourmom1');
    cy.get('#button-component').click();
    cy.wait(500);
    cy.url().should('eq', 'https://www.runner.test/');
    cy.reload();
    cy.get('div.stores').click();
    cy.get('a.card').first().click();
    cy.url().should('eq', 'https://rush.runner.test/');
    cy.wait(5000);
    cy.get('div.scroll-wrap').first().find('>div').eq(0)
      .click();
    cy.wait(3000);
    cy.url().should('include', '/product');
    cy.get('button.button-component').contains('Add To Cart').click();
    cy.wait(500);
    cy.get('.cart-count').should('contain', 1);
    cy.get('.cart').click();
    cy.wait(500);
    cy.get('#cart .checkout button').click();
    cy.wait(500);
    cy.url().should('include', 'checkout');
    cy.wait(5000);
  });
});
