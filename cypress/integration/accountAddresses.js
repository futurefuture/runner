describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/login');
    cy.get('input[name=email]').type('jarek@wearefuturefuture.com');
    cy.get('input[name=password]').type('yourmom1');
    cy.get('button.button-component').first().click();
    cy.wait(500);
    cy.url().should('eq', 'https://www.runner.test/');
    cy.reload();
    cy.visit('/account/addresses');
    cy.wait(1500);
    cy.url().should('eq', 'https://www.runner.test/account/addresses');
    // add and delete address
    cy.get('button.button-component').first().click();
    cy.get('div.new-address-box');
    cy.get('input#account_address').type('99 Bristol Rd E, Mississauga, ON, Canada');
    cy.wait(3000);
    cy.get('input#account_address').type('{downarrow}{enter}');
    cy.wait(500);
    cy.get('input#unit-number-input').type('172');
    cy.get('input#business-name-input').type('test');
    cy.get('input#postal-code-input').type('L4Z 3P4');
    cy.get('textarea#instructions-input').type('test');
    // adds
    cy.get('button.button-component').eq(1).click();
    cy.wait(3000);
    cy.get('button.button-component').first().click();
    cy.wait(3000);
    cy.get('div.new-address-box');
    cy.get('button.button-component').first().click();
    // deletes the added item
    cy.get('img.marker-icon').last().click().get('img.delete-btn')
      .click(20);
    cy.wait(3000);
    // makes last item default address
    cy.get('img.marker-icon').first().click();
    cy.wait(3000);
    cy.reload();
  });
});
