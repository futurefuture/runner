describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/register');
    cy.get('input[name=email]').type('cypress_test@mail.com');
    cy.get('input[name=password]').type('runner');
    cy.get('#button-component').click();
    cy.get('input[name=first_name]').type('Cypress');
    cy.get('input[name=last_name]').type('test');
    cy.get('input[name=phone_number]').type('123-321-1234');
    cy.get('select[name=birthday_month]').select('September');
    cy.get('select[name=birthday_day]').select('24');
    cy.get('select[name=birthday_year]').select('1997');
    // add and delete address
    cy.get('input#addressInput').type('99 Bristol Rd E');
    cy.wait(3000);
    cy.get('input#addressInput').type('{downarrow}{enter}');
    cy.wait(500);
    cy.get('div.v-switch-button').click();
    cy.get('#button-component').click();
    cy.get('button.button-component').first().click();
    cy.wait(5000);
    cy.visit('/');
    cy.get('.menu-icon > img').click();
    cy.get('.logout > #button-component > .button-component').click();
  });
});
