describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/explore');
    cy.url().should('eq', 'https://www.runner.test/');
  });
});
