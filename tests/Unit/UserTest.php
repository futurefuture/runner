<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Events\UserCreated;
use App\Events\UserDeleted;
use App\Mail\UserRegistered;
use App\Actions\User\DeleteUser;
use App\Actions\CreateUserAction;
use App\Actions\GetUserGenderAction;
use Illuminate\Support\Facades\Mail;
use App\Listeners\AddUserToMailChimp;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Events\CallQueuedListener;
use App\Listeners\RemoveUserFromMailChimp;
use App\Mail\UserDeleted as UserDeletedMail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function test_user_created_event_sent_when_user_created()
    {
        Event::fake();

        $data  = [
            'first_name'    => 'Jarek',
            'last_name'     => 'Hardy',
            'date_of_birth' => '1985-12-09',
            'email'         => 'jarek@wearefuturefuture.com',
            'phone_number'  => '6477746899',
            'password'      => 'secret',
            'address'       => [
                'postal_code'       => 'L1T 0E8',
                'formatted_address' => '123 Main Street',
                'lat'               => 83.12,
                'lng'               => 121.34,
                'place_id'          => '2121321'
            ],
            'anonymous_id' => '1231231231',
            'is_legal_age' => true
        ];

        $getUserGenderAction = new GetUserGenderAction();
        $createUserAction    = new CreateUserAction($getUserGenderAction);
        $createUserAction->execute($data);

        Event::assertDispatched(UserCreated::class, function ($e) use ($data) {
            return $e->user->first_name === $data['first_name'];
        });
    }

    /**
     * @test
     */
    public function test_email_is_sent_when_user_created_event_fired()
    {
        Mail::fake();

        $user = factory(User::class)->create([
            'email' => 'jarek@wearefuturefuture.com'
        ]);

        Mail::assertSent(UserRegistered::class, function ($mail) use ($user) {
            return $mail->user->first_name === $user->first_name;
        });
    }

    /**
     * @test
     */
    public function test_mailchimp_subscriber_is_added_with_user_created_event()
    {
        Queue::fake();

        $user = factory(User::class)->create();

        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class === AddUserToMailChimp::class;
        });
    }

    /**
     * @test
     */
    public function test_user_deleted_event_sent_when_user_deleted()
    {
        Event::fake();
        $user = factory(User::class)->create();

        $deleteUserAction    = new DeleteUser($user);
        $deleteUserAction->execute();

        Event::assertDispatched(UserDeleted::class, function ($e) use ($user) {
            return $e->user === $user;
        });
    }

    /**
     * @test
     */
    public function test_user_deleted_event_sends_an_email()
    {
        Mail::fake();

        $user = factory(User::class)->create([
            'email' => 'jarek@wearefuturefuture.com'
        ]);

        $user->delete();

        Mail::assertSent(UserDeletedMail::class, function ($mail) use ($user) {
            return $mail->user->email === $user->email;
        });
    }

    /**
     * @test
     */
    public function test_mailchimp_subscriber_is_removed_with_user_deleted_event()
    {
        Queue::fake();

        $user = factory(User::class)->create();

        $user->delete();

        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class === RemoveUserFromMailChimp::class;
        });
    }
}
