<?php

namespace Tests\Unit;

use App\User;
use App\Order;
use Tests\TestCase;
use App\Actions\OrderItem\CreateOrderItems;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderItemTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_create_order_items()
    {
        $user  = factory(User::class)->create();
        $order = factory(Order::class)->create([
            'user_id' => $user
        ]);

        $createOrderItems = new CreateOrderItems($order);

        $createOrderItems->execute();

        $this->assertDatabaseHas('order_items', [
            'user_id'  => $user->id,
            'order_id' => $order->id
        ]);
    }
}
