<?php

namespace Tests\Actions;

use App\Order;
use App\Inventory;
use Tests\TestCase;
use App\InventoryProduct;
use App\Actions\Order\CancelOrder;
use App\Actions\UpdateUserOrderAction;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function test_it_can_update_user_order()
    {
        // create order
        // update order
        // test new order details are correct

        // $order  = factory(Order::class)->create();
        // $action = new UpdateUserOrderAction();

        // $action->execute(['test'], $order);
    }

    /**
    * @test
    */
    public function test_it_can_cancel_an_order()
    {
        $inventory = factory(Inventory::class)->create();
        $order     = factory(Order::class)->create([
            'status'       => 2,
            'inventory_id' => $inventory->id
        ]);
        $productId        = json_decode($order->content)[0]->id;
        InventoryProduct::create([
            'inventory_id' => $inventory->id,
            'product_id'   => $productId,
            'quantity'     => 6
        ]);
        $action = new CancelOrder();

        $action->execute($order);

        $this->assertDatabaseHas('orders', [
            'status' => 5
        ]);
    }
}
