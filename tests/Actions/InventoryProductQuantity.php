<?php

namespace Tests\Actions;

use App\Product;
use App\Inventory;
use Tests\TestCase;
use App\InventoryProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Actions\InventoryProduct\UpdateInventoryProductQuantity;

class InventoryProductQuantity extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function test_it_can_increase_a_product_inventories_quantity()
    {
        $product          = factory(Product::class)->create();
        $inventory        = factory(Inventory::class)->create();
        $inventoryProduct = InventoryProduct::create([
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 1
        ]);

        $action = new UpdateInventoryProductQuantity();

        $response = $action->execute($product, $inventory, 3);

        $this->assertInstanceOf(InventoryProduct::class, $response);
        $this->assertDatabaseHas('inventory_product', [
            'id'       => $inventoryProduct->id,
            'quantity' => 4
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_decrease_a_product_inventories_quantity()
    {
        $product          = factory(Product::class)->create();
        $inventory        = factory(Inventory::class)->create();
        $inventoryProduct = InventoryProduct::create([
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 4
        ]);

        $action = new UpdateInventoryProductQuantity();

        $response = $action->execute($product, $inventory, -1);

        $this->assertInstanceOf(InventoryProduct::class, $response);
        $this->assertDatabaseHas('inventory_product', [
            'id'       => $inventoryProduct->id,
            'quantity' => 3
        ]);
    }
}
