<?php

namespace Tests\Actions;

use App\User;
use App\Address;
use Tests\TestCase;
use App\Actions\UserAddress\CreateUserAddress;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAddress extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function test_it_can_create_a_user_address()
    {
        $user = factory(User::class)->create();
        $data = [
            'formattedAddress' => '80 Yorkville Ave, Toronto, ON M5R 2C2, Canada',
            'placeId'          => null,
            'lat'              => '43.671394',
            'lng'              => '-79.391328',
            'postalCode'       => 'M5R 2C2',
            'unitNumber'       => '1103',
        ];

        $action = new CreateUserAddress();

        $response = $action->execute($user, $data);

        $this->assertInstanceOf(Address::class, $response);
        $this->assertDatabaseHas('addresses', [
            'user_id' => $user->id
        ]);
    }
}
