<?php

namespace Tests\Feature\Api\Partner\V2;

use App\Ad;
use App\User;
use App\Partner;
use App\Campaign;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CampaignTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/partner/v2';

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    // public function test_it_can_get_a_partners_campaigns()
    // {
    //     $startDate = Carbon::now()->startOfDay()->subDays(1)->toDateString();
    //     $endDate   = Carbon::now()->startOfDay()->addDays(1)->toDateString();
    //     $user      = factory(User::class)->create();
    //     $partner   = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(Campaign::class, 3)->create([
    //         'partner_id' => $partner->id,
    //         'start_date' => $startDate,
    //         'end_date'   => $endDate
    //     ]);
    //     $firstCampaign = Campaign::first();
    //     factory(Ad::class)->create([
    //         'partner_id'  => $partner->id,
    //         'start_date'  => $startDate,
    //         'end_date'    => $endDate,
    //         'campaign_id' => $firstCampaign->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->get($this->baseUrl . '/partners/' . $partner->id . '/campaigns?startDate=' . $startDate . '&endDate=' . $endDate);

    //     $response->assertJsonStructure([
    //         'data' => [
    //             [
    //                 'type',
    //                 'id',
    //                 'attributes' => [
    //                     'partnerId',
    //                     'title',
    //                     'state',
    //                     'startDate',
    //                     'endDate',
    //                     'createdAt',
    //                     'updatedAt'
    //                 ]
    //             ]
    //         ]
    //     ]);
    // }

    /**
     * @test
     */
    // public function test_it_can_get_a_partners_campaign()
    // {
    //     $user    = factory(User::class)->create();
    //     $partner = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(Campaign::class, 3)->create([
    //         'partner_id' => $partner->id
    //     ]);
    //     $campaign = factory(Campaign::class)->create([
    //         'partner_id' => $partner->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->get($this->baseUrl . '/partners/' . $partner->id . '/campaigns/' . $campaign->id);

    //     $response->assertJsonStructure([
    //         'data' => [
    //             'type',
    //             'id',
    //             'attributes' => [
    //                 'partnerId',
    //                 'title',
    //                 'state',
    //                 'startDate',
    //                 'endDate',
    //                 'createdAt',
    //                 'updatedAt'
    //             ]
    //         ]
    //     ]);
    // }

    /**
     * @test
     */
    // public function test_it_can_delete_a_partners_ad()
    // {
    //     $user    = factory(User::class)->create();
    //     $partner = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(Campaign::class, 3)->create([
    //         'partner_id' => $partner->id
    //     ]);
    //     $campaign = factory(Campaign::class)->create([
    //         'partner_id' => $partner->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->delete($this->baseUrl . '/partners/' . $partner->id . '/campaigns/' . $campaign->id);

    //     $response->assertStatus(200);

    //     $this->assertDatabaseMissing('ads', [
    //         'id' => $campaign->id
    //     ]);
    // }
}
