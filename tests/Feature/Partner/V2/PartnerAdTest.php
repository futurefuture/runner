<?php

namespace Tests\Feature\Api\Partner\V2;

use App\Ad;
use App\User;
use App\Partner;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PartnerAdTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/partner/v2';

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    // public function test_it_can_get_a_partners_ads()
    // {
    //     $user    = factory(User::class)->create();
    //     $partner = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(Ad::class, 3)->create([
    //         'partner_id' => $partner->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->get($this->baseUrl . '/partners/' . $partner->id . '/ads');

    //     $response->assertJsonStructure([
    //         'data' => [
    //             [
    //                 'type',
    //                 'id',
    //                 'attributes' => [
    //                     'adTypeId',
    //                     'partnerId',
    //                     'campaignId',
    //                     'content',
    //                     'products',
    //                     'state',
    //                     'bidType',
    //                     'bidAmount',
    //                     'createdAt',
    //                     'updatedAt'
    //                 ]
    //             ]
    //         ]
    //     ]);
    // }

    /**
     * @test
     */
    // public function test_it_can_get_a_partners_ad()
    // {
    //     $user    = factory(User::class)->create();
    //     $partner = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     $ads = factory(Ad::class, 3)->create([
    //         'partner_id' => $partner->id
    //     ]);
    //     $ad = factory(Ad::class)->create([
    //         'partner_id' => $partner->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->get($this->baseUrl . '/partners/' . $partner->id . '/ads/' . $ad->id);

    //     $response->assertJsonStructure([
    //         'data' => [
    //             'type',
    //             'id',
    //             'attributes' => [
    //                 'adTypeId',
    //                 'partnerId',
    //                 'campaignId',
    //                 'content',
    //                 'products',
    //                 'state',
    //                 'bidType',
    //                 'bidAmount',
    //                 'createdAt',
    //                 'updatedAt'
    //             ]
    //         ]
    //     ]);
    // }

    /**
     * @test
     */
    // public function test_it_can_delete_a_partners_ad()
    // {
    //     $user    = factory(User::class)->create();
    //     $partner = factory(Partner::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(Ad::class, 3)->create([
    //         'partner_id' => $partner->id
    //     ]);
    //     $ad = factory(Ad::class)->create([
    //         'partner_id' => $partner->id
    //     ]);

    //     $this->actingAs($user, 'api');

    //     $response = $this->delete($this->baseUrl . '/partners/' . $partner->id . '/ads/' . $ad->id);

    //     $response->assertStatus(200);

    //     $this->assertDatabaseMissing('ads', [
    //         'id' => $ad->id
    //     ]);
    // }
}
