<?php

namespace Tests\Feature\Api\Partner\V2;

use App\Partner;
use App\Product;
use App\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PartnerReviewTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/partner/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $user          = factory(User::class)->create();
        $this->partner = factory(Partner::class)->create([
            'user_id' => $user->id,
        ]);
        $this->product = factory(Product::class)->create();
        $this->partner->products()->attach($this->product);
        $this->actingAs($user, 'api');
    }

    /**
     * @test
     */
    public function test_it_can_get_product_reviews()
    {
        factory(Review::class)->create([
            'product_id' => $this->product->id,
        ]);

        $response = $this->get($this->baseUrl . '/partners/' . $this->partner->id . '/reviews');
        $response->assertJsonStructure([
            'data' => [
                [
                    'rating',
                    'product',
                    'date'
                ],
            ],
        ]);
    }
}
