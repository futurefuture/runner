<?php

namespace Tests\Feature\Partner\V2;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $admin;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    public function test_partner_can_login()
    {
        $this->artisan('passport:install');

        $user = factory(\App\User::class)->create([
            'role'     => 7,
        ]);

        $partner = factory(\App\Partner::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->post('https://api.runner.test/partner/v2/login', [
            'data' => [
                'attributes' => [
                    'email'    => $user->email,
                    'password' => 'secret',
                ],
            ],
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'type',
                    'attributes' => [
                        'accessToken',
                        'user' => [
                            'id',
                            'firstName',
                            'lastName',
                        ],
                        'partner' => [
                            'id',
                            'title',
                            'image',
                        ],
                    ],
                ],
            ]);
    }
}
