<?php

namespace Tests\Feature\Courier\V2;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourierTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/courier/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_a_courier()
    {
        $courier = factory(\App\User::class)->create();

        $this->actingAs($courier, 'api');

        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_a_courier()
    {
        $courier = factory(\App\User::class)->create();

        $this->actingAs($courier, 'api');

        $response = $this->put($this->baseUrl . '/couriers/' . $courier->id, [
          'data' => [
            'attributes' => [
              'firstName' => 'Newfirstname',
            ],
          ],
        ]);

        $this->assertDataBaseHas('users', [
          'first_name' => 'Newfirstname',
        ]);

        $response->assertJsonStructure([
          'data' => [
              'type',
              'id',
              'attributes' => [
                  'firstName',
                  'lastName',
                  'dateOfBirth',
                  'phoneNumber',
                  'email',
              ],
          ],
      ]);
    }
}
