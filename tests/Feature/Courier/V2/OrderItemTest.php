<?php

namespace Tests\Feature\Courier\V2;

use App\User;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderItemTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    public function test_can_get_order_items()
    {
        $courier  = factory(\App\User::class)->create();
        $customer = factory(\App\User::class)->create();
        $this->actingAs($courier, 'api');
        $categoryOne = factory(\App\Category::class)->create();
        $categoryTwo = factory(\App\Category::class)->create();
        $productOne  = factory(Product::class)->create([
            'category_id' => $categoryOne->id,
        ]);
        $productTwo = factory(Product::class)->create([
            'category_id' => $categoryTwo->id,
        ]);

        $orderContent = [
            [
                'id'          => $productOne->id,
                'title'       => $productOne->title,
                'type'        => 'regular',
                'runnerPrice' => $productOne->runnerPrice,
                'quantity'    => 2,
                'packaging'   => $productOne->packaging,
                'image'       => $productOne->image,
            ],
            [
                'id'          => $productTwo->id,
                'title'       => $productTwo->title,
                'type'        => 'regular',
                'runnerPrice' => $productTwo->runnerPrice,
                'quantity'    => 2,
                'packaging'   => $productTwo->packaging,
                'image'       => $productTwo->image,
            ],
        ];

        $customer = json_encode([
            'firstName'   => $customer->first_name,
            'lastName'    => $customer->last_name,
            'phoneNumber' => $customer->phone_number,
            'email'       => $customer->email,
        ]);

        $order = factory(\App\Order::class)->create([
            'content'  => json_encode($orderContent),
            'customer' => $customer,
        ]);

        $task = factory(\App\Task::class)->create([
            'order_id'   => $order->id,
            'courier_id' => $courier->id,
            'type'       => 'order',
        ]);

        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/order-items');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'title',
                        'quantity',
                        'isVintage',
                        'retailPrice',
                        'packaging',
                        'producingCountry',
                        'category',
                        'customer',
                    ],
                ],
                [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'title',
                        'quantity',
                        'isVintage',
                        'retailPrice',
                        'packaging',
                        'producingCountry',
                        'category',
                        'customer',
                    ],
                ],
            ],
        ]);
    }
}
