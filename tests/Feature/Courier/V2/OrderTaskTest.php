<?php

namespace Tests\Feature\Courier\V2;

use App\User;
use App\Store;
use Tests\TestCase;
use App\Services\StripeService;
use App\Services\StripeServiceFake;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTaskTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_assigning_order_creates_task()
    {
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $user = factory(\App\User::class)->create([
            'role' => 1,
        ]);
        $address = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
            'selected' => 1,
        ]);
        $order = factory(\App\Order::class)->create([
            'user_id'  => $user->id,
            'status'   => 0,
            'runner_1' => $courier->id,
        ]);

        $this->actingAs($user, 'api');

        $response = $this->put('https://runner.test/api/v1/dispatch/assign_runners', [
            'orderId'  => $order->id,
            'runner_1' => $courier->id,
            'runner_2' => null,
        ]);

        $this->assertDatabaseHas('tasks', [
            'courier_id' => $courier->id,
            'order_id'   => $order->id,
            'state'      => 1,
        ]);

        $this->assertDatabaseHas('orders', [
            'id'     => $order->id,
            'status' => 1,
        ]);
    }

    /**
     * @test
     */
    public function test_updating_order_updates_task()
    {
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $user = factory(\App\User::class)->create([
            'role' => 1,
        ]);
        $address = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
            'selected' => 1,
        ]);
        $order = factory(\App\Order::class)->create([
            'user_id'  => $user->id,
            'status'   => 1,
            'runner_1' => $user->id,
        ]);
        $task = factory(\App\Task::class)->create([
            'order_id'   => $order->id,
            'state'      => 1,
            'courier_id' => $user->id,
        ]);

        $this->actingAs($user, 'api');

        $response = $this->put('https://runner.test/api/v1/courier/orders/' . $order->id, [
            'status' => 2,
        ]);

        $this->assertDatabaseHas('tasks', [
            'courier_id' => $user->id,
            'order_id'   => $order->id,
            'state'      => 2,
        ]);

        $this->assertDatabaseHas('orders', [
            'id'     => $order->id,
            'status' => 2,
        ]);
    }

    /**
     * @test
     */
    public function test_updating_order_with_signature_updates_task_with_signature()
    {
        // swap out for fake stripe service
        $fakeStripeService = new StripeServiceFake;
        $this->swap(StripeService::class, $fakeStripeService);

        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $user = factory(\App\User::class)->create([
            'role' => 1,
        ]);
        $address = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
            'selected' => 1,
        ]);
        $customer = json_encode([
            'first_name'   => 'jarek',
            'last_name'    => 'hardy',
            'phone_number' => '6477746899',
            'email'		      => 'jarek@getrunner.io',
        ]);

        $order = factory(\App\Order::class)->states('new')->create([
            'user_id' 	 => $user->id,
            'status'    => 2,
            'runner_1' 	=> $user->id,
            'customer'	 => $customer,
        ]);
        $task = factory(\App\Task::class)->create([
            'order_id'   => $order->id,
            'state'      => 2,
            'courier_id' => $user->id,
        ]);

        $this->actingAs($user, 'api');

        $response = $this->put('https://runner.test/api/v1/courier/orders/' . $order->id, [
            'status'    => 3,
            'signature' => 'test.png',
        ]);

        $this->assertDatabaseHas('tasks', [
            'courier_id' => $user->id,
            'order_id'   => $order->id,
            'state'      => 3,
            'signature'  => 'https://order-signatures.s3.amazonaws.com/1.png',
        ]);

        $this->assertDatabaseHas('orders', [
            'id'     => $order->id,
            'status' => 3,
        ]);
    }

    /**
     * @test
     */
    public function test_updating_task_updates_order_with_signature()
    {
        $fakeStripeService = new StripeServiceFake;
        $this->swap(StripeService::class, $fakeStripeService);
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $store = factory(Store::class)->create();
        $order = factory(\App\Order::class)->create([
            'status'   => 2,
            'store_id' => $store->id
        ]);
        $address = factory(\App\Address::class)->create();
        $task    = factory(\App\Task::class)->create([
            'order_id'   => $order->id,
            'address_id' => $address->id,
            'state'      => 2,
            'courier_id' => $courier->id
        ]);
        $this->actingAs($courier, 'api');

        $this->put('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks/' . $task->id, [
            'data' => [
                'attributes' => [
                    'orderId'   => $order->id,
                    'state'     => 3,
                    'signature' => 'test.png',
                    'type'      => 'order',
                ],
            ],
        ]);

        $this->assertDatabaseHas('tasks', [
            'order_id'  => $order->id,
            'state'     => 3,
            'signature' => 'https://runner-task-signatures.s3.amazonaws.com/1.png',
        ]);

        $this->assertDatabaseHas('orders', [
            'id'        => $order->id,
            'status'    => 3,
            'signature' => 'https://runner-task-signatures.s3.amazonaws.com/1.png',
        ]);
    }
}
