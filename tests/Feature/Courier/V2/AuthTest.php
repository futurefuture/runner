<?php

namespace Tests\Feature\Courier\V2;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->artisan('passport:install');
    }

    public function test_courier_can_login()
    {
        $courier = factory(\App\User::class)->create();

        $response = $this->post('https://api.runner.test/courier/v2/login', [
            'data' => [
                'attributes' => [
                    'email'    => $courier->email,
                    'password' => 'secret',
                    'fcmToken' => '123'
                ]
            ]
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);

        $this->assertDatabaseHas('users', [
            'fcm_token' => '123'
        ]);
    }
}
