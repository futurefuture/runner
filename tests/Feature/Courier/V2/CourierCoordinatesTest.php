<?php

namespace Tests\Feature\Courier\V2;

use DB;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourierCoordinatesTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/courier/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->artisan('passport:install');
    }

    /**
     * @test
     */
    public function test_it_can_add_courier_coordinates()
    {
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $this->actingAs($courier, 'api');
        $response = $this->post($this->baseUrl . '/couriers/' . $courier->id . '/coordinates', [
            'data' => [
                'type'       => 'courier coordinates',
                'attributes' => [
                    'longitude' => -79.38179524,
                    'latitude'  => 43.63974196,
                ],
            ],
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('runner_location', [
            'runner_id' => $courier->id,
            'longitude' => -79.38179524,
            'latitude'  => 43.63974196,
        ]);
    }
}
