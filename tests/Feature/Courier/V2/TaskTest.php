<?php

namespace Tests\Feature\Courier\V2;

use App\Tag;
use App\Task;
use App\User;
use App\Order;
use App\Store;
use App\Address;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/courier/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->inventory = factory(Inventory::class)->create();
        $this->product   = factory(\App\Product::class)->create();
        DB::table('inventory_product')->insert([
            'product_id'   => $this->product->id,
            'inventory_id' => $this->inventory->id,
            'quantity'     => 10
        ]);
        $this->store   = factory(Store::class)->create();
        $this->content = json_encode([
            [
                'id'                => $this->product->id,
                'title'             => $this->product->title,
                'quantity'          => 3,
                'runner_price'      => $this->product->runner_price,
                'packaging'         => $this->product->packaging,
                'image'             => $this->product->image,
                'producingCountry'  => $this->product->producing_country,
                'isVintage'         => true,
            ],
        ]);
        $this->order = factory(Order::class)->create([
            'content'      => $this->content,
            'deleted_at'   => null,
            'store_id'     => $this->store->id,
            'inventory_id' => null,
            'incentive_id' => null,
            'app_version'  => null,
            'address_id'   => 121,
            'status' 	     => 1,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_tasks()
    {
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        factory(\App\Task::class)->create([
            'courier_id' => $courier->id,
            'order_id'   => $this->order->id,
            'state'      => 1
        ]);

        $this->actingAs($courier, 'api');

        $response = $this->get($this->baseUrl . '/couriers/' . $courier->id . '/tasks');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'type',
                        'id',
                        'storeLogo',
                        'attributes' => [
                            'state',
                            'index',
                            'notes',
                            'phoneNumber',
                            'name',
                            'organization',
                            'address',
                            'courier',
                            'order' => [
                                'attributes' => [
                                    'orderContent' => [
                                        [
                                            'id',
                                            'title',
                                            'quantity',
                                            'runner_price',
                                            'packaging',
                                            'image',
                                            'producingCountry',
                                            'isVintage',
                                            'producing_country',
                                            'sku',
                                            'isRush'
                                        ]
                                    ]
                                ]
                            ],
                            'container',
                            'type',
                            'createdAt',
                            'signature',
                        ],
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_task()
    {
        $courier = factory(\App\User::class)->create();
        $task    = factory(\App\Task::class)->create([
            'order_id'     => $this->order->id,
            'courier_id'   => $courier->id,
            'organization' => 'runner',
            'state'        => 1,
        ]);

        $this->actingAs($courier, 'api');

        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks/' . $task->id);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'state',
                    'notes',
                    'index',
                    'phoneNumber',
                    'name',
                    'organization',
                    'address',
                    'order',
                    'type',
                    'createdAt',
                    'signature',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_get_completed_tasks()
    {
        $courier = factory(\App\User::class)->create();
        $this->actingAs($courier, 'api');

        factory(\App\Task::class)->create([
            'order_id'     => $this->order->id,
            'courier_id'   => $courier->id,
            'organization' => 'runner',
            'state'        => 3,
        ]);

        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks');

        $response->assertJson([
            'data' => [],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_in_progress_tasks()
    {
        $courier = factory(\App\User::class)->create();
        $this->actingAs($courier, 'api');

        $task = factory(\App\Task::class)->create([
            'order_id'     => $this->order->id,
            'courier_id'   => $courier->id,
            'organization' => 'runner',
            'state'        => 1,
        ]);

        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'state',
                        'notes',
                        'index',
                        'phoneNumber',
                        'name',
                        'organization',
                        'address',
                        'order',
                        'type',
                        'createdAt',
                        'signature',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_tasks_with_order_and_gift()
    {
        $courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        factory(\App\Gift::class)->create([
            'order_id' => $this->order->id,
        ]);
        $task = factory(\App\Task::class)->create([
            'courier_id' => $courier->id,
            'order_id'   => $this->order->id,
        ]);
        $this->actingAs($courier, 'api');
        $response = $this->get($this->baseUrl . '/couriers/' . $courier->id . '/tasks/' . $task->id);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'storeLogo',
                'attributes' => [
                    'state',
                    'index',
                    'notes',
                    'phoneNumber',
                    'name',
                    'organization',
                    'address',
                    'order' => [
                        'attributes' => [
                            'gift' => [
                                'id',
                                'orderId',
                                'firstName',
                                'lastName',
                                'isNineteen',
                                'email',
                                'phoneNumber',
                                'message',
                            ]
                        ],
                    ],
                    'type',
                    'createdAt',
                    'signature',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_a_task()
    {
        $courier = factory(\App\User::class)->create();
        $store   = factory(Store::class)->create();
        $order   = factory(Order::class)->create([
            'store_id' => $store->id
        ]);
        $task    = factory(\App\Task::class)->create([
            'courier_id' => $courier->id,
            'state'      => 1,
            'order_id'   => $order->id
        ]);
        $this->actingAs($courier, 'api');

        $response = $this->put('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks/' . $task->id, [
            'data' => [
                'type'       => 'tasks',
                'attributes' => [
                    'state'   => 2,
                    'orderId' => (string) $order->id
                ],
            ],
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'id'    => $task->id,
            'state' => 2,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_delete_a_task()
    {
        $courier = factory(\App\User::class)->create();
        $this->actingAs($courier, 'api');
        $task = factory(\App\Task::class)->create();

        $response = $this->delete('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks/' . $task->id);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_tag_associated_a_task_customer()
    {
        $courier = factory(\App\User::class)->create();
        $this->actingAs($courier, 'api');
        $customer = factory(User::class)->create();
        $tag      = factory(Tag::class)->create();
        $customer->tags()->attach($tag);
        $store = factory(Store::class)->create();
        $order = factory(Order::class)->create([
            'user_id'  => $customer->id,
            'store_id' => $store->id
        ]);
        $address  = factory(Address::class)->create();
        $task     = factory(Task::class)->create([
            'order_id'   => $order->id,
            'address_id' => $address->id,
            'courier_id' => $courier->id
        ]);
        $response = $this->get('https://api.runner.test/courier/v2/couriers/' . $courier->id . '/tasks/' . $task->id);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'storeLogo',
                'attributes' => [
                    'state',
                    'index',
                    'notes',
                    'phoneNumber',
                    'name',
                    'organization',
                    'address',
                    'order' => [
                        'attributes' => [
                            'customer' => [
                                'tags' => [
                                    []
                                ]
                            ]
                        ],
                    ],
                    'type',
                    'createdAt',
                    'signature',
                ],
            ],
        ]);
    }
}
