<?php

namespace Tests\Feature\Dispatch\V2;

use App\User;
use App\UserNote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserNoteTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     */
    public function test_it_can_get_notes()
    {
        $response = $this->get($this->baseUrl . '/user/' . $this->user->id . '/notes');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_create_notes()
    {
        $data = [
            'data' => [
                'attributes' => [
                    'title' => 'create notes title',
                    'body'  => 'create notes body',
                ],
            ],
        ];
        $response = $this->post($this->baseUrl . '/user/' . $this->user->id . '/notes', $data);
        $response->assertStatus(201);
    }
}
