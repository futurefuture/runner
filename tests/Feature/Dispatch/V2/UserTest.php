<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    public function signInAsDispatch($opts = [])
    {
        $user = factory(\App\User::class)->create([
            'role' => 5,
        ]);
        $this->actingAs($user, 'api');

        return $user;
    }

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = $this->signInAsDispatch();
    }

    /**
     * @test
     */
    public function test_it_can_get_all_users()
    {
        $users    = factory(User::class, 10)->create();
        $response = $this->get($this->baseUrl . '/users');
        $this->assertEquals(json_decode($response->getContent())->total, $users->count() + 1);
    }

    /**
     * @test
     */
    public function test_it_can_get_users_info_after_dispatch_login()
    {
        $response = $this->get($this->baseUrl . '/users/' . $this->user->id)->assertJson([
            'data' => [
                'id' => $this->user->id,
            ],
        ]);
    }

    /**@test */
    public function test_it_can_update_user_password()
    {
        $update = [
            'password' => '',
        ];
        $response = $this->put($this->baseUrl . '/users/' . $this->user->id, [
            'user' => [
                'password' => '',
            ],
        ]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_update_user_info_after_dispatch_login()
    {
        $update = [
            'first_name' => 'Test First Name',
            'last_name'  => 'Test last Name',
            'email'      => 'test@test.com',
        ];
        $response = $this->put($this->baseUrl . '/users/' . $this->user->id, [
            'user' => [
                'first_name' => 'Test First Name',
                'last_name'  => 'Test last Name',
                'email'      => 'test@test.com',
            ],
        ]);

        $this->assertDatabaseHas('users', [
            'id'         => $this->user->id,
            'first_name' => $update['first_name'],
            'last_name'  => $update['last_name'],
            'email'      => $update['email'],
        ]);
    }

    /**@test */
    public function test_it_can_delete_user()
    {
        $response = $this->delete($this->baseUrl . '/users/' . $this->user->id);
        $response->assertStatus(200);
        $this->assertSoftDeleted('users', [
            'id' => $this->user->id,
        ]);
    }
}
