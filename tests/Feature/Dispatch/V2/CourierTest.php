<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\Schedule;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CourierTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    public function signInAsDispatch($opts = [])
    {
        $user = factory(\App\User::class)->create([
            'role' => 5,
        ]);
        $this->actingAs($user, 'api');

        return $user;
    }

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = $this->signInAsDispatch();
    }

    /**
     * @test
     */
    public function test_it_can_get_couriers()
    {
        $couriers = factory(User::class)->create([
            'role' => 2,
        ]);
        $joyeco = factory(User::class)->create([
            'id' => 13440,
        ]);
        $schedule = factory(Schedule::class)->create([
            'user_id' => $couriers->id,
        ]);
        $response = $this->get($this->baseUrl . '/couriers');
        //dd($response->getContent());
        $response->assertJsonStructure([
            [
                'scheduledRunner' => [
                    [
                        'id',
                        'user_id',
                        'job_type',
                        'start',
                        'end',
                        'deleted_at',
                        'created_at',
                        'updated_at',
                    ],
                ],
                'joey' => [],
            ],
        ]);
    }

    // /**@test */
    // public function test_it_can_get_courier_position(){
    //     $courier_pos = factory(RunnerPosition::class)->create();
    //     $response = $this->get($this->baseUrl . '/courier-positions');
    //     $response->assertJsonStructure([
    //         [
    //           'id',
    //           'title',
    //           'description',
    //           'rate',
    //           'backgroundColor',
    //           'created_at',
    //           'updated_at'
    //         ]
    //     ]);

    // }

    // /**@test */
    // public function test_it_can_get_courier_on_duty(){
    //     $runner1 = factory(User::class)->create([
    //         'role' => 2
    //     ]);
    //     $runner2 = factory(User::class)->create([
    //         'role' => 2
    //     ]);
    //     $order = factory(Order::class,2)->create([
    //         'status'   => 2,
    //         'runner_1' => $runner1->id,
    //         'runner_2' => $runner2->id
    //     ]);
    //     $courier1_onDuty = Order::whereIn('status', [0, 1, 2, 7])
    //                     ->pluck('runner_1')
    //                     ->toArray();
    //     $courier2_onDuty = Order::whereIn('status', [0, 1, 2, 7])
    //                      ->pluck('runner_2')
    //                      ->toArray();

    //     $couriers_onDuty = array_unique(array_filter(array_merge($courier1_onDuty, $courier2_onDuty)));
    //     $response = $this->get($this->baseUrl . '/couriers_onduty');
    //     $this->assertEquals(count($couriers_onDuty),count(json_decode($response->getContent())));
    // }
}
