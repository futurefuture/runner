<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\Task;
use App\User;
use App\Order;
use App\Store;
use App\Address;
use App\Product;
use App\Inventory;
use Tests\TestCase;
use App\InventoryProduct;
use App\Services\OrderService;
use App\Services\StripeService;
use App\Services\StripeServiceFake;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->product = factory(\App\Product::class)->create([
            'runner_price' => 1586,
        ]);
        $inventory     = factory(Inventory::class)->create();
        InventoryProduct::create([
            'inventory_id' => $inventory->id,
            'product_id'   => $this->product->id,
            'quantity'     => 4
        ]);
        $this->store   = factory(Store::class)->create();
        $this->content = json_encode([
            [
                'id'                => $this->product->id,
                'title'             => $this->product->title,
                'quantity'          => 3,
                'runner_price'      => $this->product->runner_price,
                'packaging'         => $this->product->packaging,
                'image'             => $this->product->image,
                'producingCountry'  => $this->product->producing_country,
                'isVintage'         => true,
            ],
        ]);
        $this->order = factory(Order::class)->create([
            'content'      => $this->content,
            'deleted_at'   => null,
            'store_id'     => $this->store->id,
            'inventory_id' => $inventory->id,
            'incentive_id' => null,
            'app_version'  => null,
            'address_id'   => 121,
            'status' 	     => 1,
            'charge'	      => 'ch_1F0YqtByumHgsxlUGlojy8nh',
            'subtotal'	    => $this->product->runner_price * 3,
        ]);
    }

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    /**
     * @test
     */
    public function test_it_can_get_all_orders()
    {
        $response = $this->get($this->baseUrl . '/orders');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_update_order()
    {
        $response = $this->put($this->baseUrl . '/orders/' . $this->order->id, [
            'data' => [
                'attributes' => [
                    'cardLastFour' => '1111',
                ],
            ],
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_updating_an_order_address_updates_task_address_id()
    {
        $address = factory(Address::class)->create();
        $task    = factory(Task::class)->create([
            'order_id'   => $this->order->id,
            'address_id' => 12312
        ]);
        $response = $this->put($this->baseUrl . '/orders/' . $this->order->id, [
            'data' => [
                'attributes' => [
                    'formattedAddress' => $address->formatted_address,
                    'lat'              => $address->lat,
                    'lng'              => $address->lng,
                    'postalCode'       => $address->postalCode,
                    'unitno'           => null,
                    'instructions'     => null,
                ],
            ],
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'address_id' => 2
        ]);

        $this->assertDatabaseHas('orders', [
            'address_id' => 2
        ]);
    }

    /**
     * @test
     */
    public function it_can_refund_from_dispatch()
    {
        $content = json_decode($this->order->content);
        $data    = [];
        foreach ($content as $newItem) {
            array_push($data, [
                'id'          => $newItem->id,
                'title'       => $newItem->title,
                'quantity'    => ($newItem->quantity) - 1,
                'runnerPrice' => $newItem->runner_price,
                'truePrice'   => ($newItem->runner_price / 1.12),
                'packaging'   => $newItem->packaging,
            ]);
        }
        $subtotal     = $this->product->runner_price * 2;
        $refundAmount = (int) ($this->order->subtotal - $subtotal);

        $refund = (new OrderService($this->order))->subTotal($data);
        $this->assertEquals($refundAmount, ($this->order->subtotal - $refund));
    }

    /**
     * @test
     */
    public function test_it_change_task_state_when_order_is_cancelled()
    {
        $this->courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);

        $task = factory(\App\Task::class)->create([
            'courier_id' => $this->courier->id,
            'order_id'   => $this->order->id,
            'state'		    => 1,
        ]);

        $response = $this->put($this->baseUrl . '/orders/' . $this->order->id, [
            'data' => [
                'attributes' => [
                    'status' => 5,
                ],
            ],
        ]);

        $response->assertStatus(200);
        $newTask = Task::find($task->id);
        $this->assertEquals(5, $newTask->state);
    }

    public function test_it_can_update_order_items()
    {
        // swap out for fake stripe service
        $fakeStripeService = new StripeServiceFake;
        $this->swap(StripeService::class, $fakeStripeService);

        $order    = factory(Order::class)->create();
        $product  = factory(Product::class)->create();
        $newItems = [
            [
                'id'           => $product->id,
                'type'         => 'product',
                'title'        => $product->title,
                'quantity'     => 10,
                'runnerPrice'  => $product->runner_price,
                'retailPrice'  => $product->retail_price,
                'packaging'    => $product->packaging,
                'image'        => $product->image,
                'rewardPoints' => 0,
                'allowSub'     => 'true'
            ],
            [
                'productId'       => $product->id,
                'productQuantity' => 2
            ]
        ];
        $data    = [
            'newItems' => $newItems,
            'isVerify' => false
        ];

        $subTotal        = (int) (($product->runner_price * 12));
        $markup          = (int) (12 * ($product->runner_price - $product->retail_price));
        $displayTaxTotal = (int) (($markup + 1000) * 0.13);

        $this->put($this->baseUrl . '/orders/' . $order->id . '/items', $data);

        $this->assertDatabaseHas('orders', [
            'id'                => $order->id,
            // 'content'           => json_encode($newItems),
            'subtotal'          => $subTotal,
            'cogs'              => (int) (($product->retail_price * 12)),
            'markup'            => $markup,
            'display_tax_total' => $displayTaxTotal,
            'total'             => $subTotal + 1000 + $displayTaxTotal + $order->tip - $order->discount
        ]);
    }
}
