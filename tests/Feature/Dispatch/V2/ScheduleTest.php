<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\RunnerPosition;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    public function signInAsDispatch($opts = [])
    {
        $user = factory(\App\User::class)->create([
            'role' => 5,
        ]);
        $this->actingAs($user, 'api');

        return $user;
    }

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user    = $this->signInAsDispatch();
        $this->courier = factory(User::class)->create([
            'role' => 2,
        ]);
    }

    /**@test */
    public function test_it_can_create_new_schedule()
    {
        $schedule = factory(Schedule::class)->create();

        $data = [
            'employee'  => $this->courier->id,
            'job_type'  => 1,
            'start'     => Carbon::now(),
            'end'       => Carbon::now()->addMinutes(120),
            'color'     => '#ffffff',
        ];
        $response = $this->post($this->baseUrl . '/schedules/', $data);

        $this->assertDatabaseHas(
            'schedules',
            [
                'user_id'     => $this->courier->id,
                'job_type'    => $data['job_type'],
                'start'       => $data['start']->toDateTimeString(),
                'end'         => $data['end']->toDateTimeString(),
                'deleted_at'  => null,
                'created_at'  => $data['start']->toDateTimeString(),
                'updated_at'  => $data['start']->toDateTimeString(),
                'color'       => '#F65EB7',
            ]
        );
    }

    /**@test */
    public function test_it_can_update_schedule()
    {
        $schedule = factory(Schedule::class)->create([
            'user_id' => $this->courier->id,
        ]);

        $update = [
            'user'  => ['id' => $this->courier->id],
            'job'   => ['id' => 2],
            'start' => Carbon::now()->addMinutes(10),
            'end'   => Carbon::now()->addMinutes(140),
        ];
        $response = $this->put($this->baseUrl . '/schedules/' . $schedule->id, $update);
        $this->assertDatabaseHas('schedules', [
            'user_id'     => $this->courier->id,
            'job_type'    => $update['job']['id'],
            'start'       => $update['start'],
            'end'         => $update['end'],
        ]);
    }

    /**@test */

    public function test_it_can_get_schedules()
    {
        $courier_pos = factory(RunnerPosition::class)->create([
            'id' => 1,
        ]);
        $schedule1 = factory(Schedule::class)->create([
            'user_id'  => $this->courier->id,
            'job_type' => 1,
        ]);
        $schedule2 = factory(Schedule::class)->create([
            'user_id'  => $this->courier->id,
            'job_type' => 1,
            'start'    => Carbon::now()->addDay(1),
        ]);
        $response = $this->json('GET', $this->baseUrl . '/schedules', ['sort' => '-start']);
        $response->assertJsonStructure([
            [
                'id',
                'title',
                'start',
                'end',
                'backgroundColor',
                'allDay',
            ],
        ]);
    }

    /**@test */

    public function test_it_can_get_a_schedule()
    {
        $courier_pos = factory(RunnerPosition::class)->create([
            'id' => 1,
        ]);
        $schedule = factory(Schedule::class)->create([
            'user_id'  => $this->courier->id,
            'job_type' => 1,
        ]);
        $response = $this->get($this->baseUrl . '/schedules/' . $schedule->id)->assertJson([
            'id' => $schedule->id,
        ]);
    }

    /**@test */

    public function test_it_can_remove_schedule()
    {
        $schedule = factory(Schedule::class)->create([
            'user_id'  => $this->courier->id,
            'job_type' => 1,
        ]);
        $response = $this->delete($this->baseUrl . '/schedules/' . $schedule->id);
        $this->assertSoftDeleted('schedules', [
            'id' => $schedule->id,
        ]);
    }
}
