<?php

namespace Tests\Feature\Api\Dispatch\V2;

use DB;
use App\User;
use App\Product;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderInventoryTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**@test */
    public function test_can_get_inventories_for_order()
    {
        $user = factory(\App\User::class)->create([
            'role' => 1,
        ]);
        $productOne   = factory(Product::class)->create();
        $productTwo   = factory(Product::class)->create();
        $orderContent = json_encode([
            [
                'id'           => $productOne->id,
                'title'        => $productOne->title,
                'runnerPrice'  => (int) $productOne->runner_price,
                'image'        => $productOne->image,
                'quantity'     => 3,
                'packaging'    => $productOne->packaging,
                'rewardPoints' => null,
                'allowSub'     => false,
                'subTotal'     => (int) $productOne->runner_price * 3,
            ],
            [
                'id'           => $productTwo->id,
                'title'        => $productTwo->title,
                'runnerPrice'  => (int) $productTwo->runner_price,
                'image'        => $productTwo->image,
                'quantity'     => 3,
                'packaging'    => $productTwo->packaging,
                'rewardPoints' => null,
                'allowSub'     => false,
                'subTotal'     => (int) $productTwo->runner_price * 3,
            ],
        ]);
        $order        = factory(\App\Order::class)->states('new')->create();
        $addressOne   = factory(\App\Address::class)->create();
        $addressTwo   = factory(\App\Address::class)->create();
        $inventoryOne = factory(Inventory::class)->create([
            'address_id' => $addressOne->id,
        ]);
        $inventoryTwo = factory(Inventory::class)->create([
            'address_id' => $addressTwo->id,
        ]);
        DB::table('inventory_product')->insert([
            [
                'id'           => 1,
                'inventory_id' => $inventoryOne->id,
                'product_id'   => $productOne->id,
                'quantity'     => 100,
            ],
            [
                'id'           => 2,
                'inventory_id' => $inventoryOne->id,
                'product_id'   => $productTwo->id,
                'quantity'     => 100,
            ],
            [
                'id'           => 3,
                'inventory_id' => $inventoryTwo->id,
                'product_id'   => $productTwo->id,
                'quantity'     => 100,
            ],
        ]);
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/orders/' . $order->id . '/inventories');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'id',
                        'title',
                        'vendor',
                        'store_id',
                        'location_id',
                        'options',
                        'is_active',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                        'address_id',
                        'inStock',
                        'products',
                        'totalItems',
                    ],
                ],
            ],
        ]);
    }
}
