<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\User;
use App\Address;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAddressTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    public function signInAsDispatch($opts = [])
    {
        $user = factory(\App\User::class)->create([
            'role' => 5,
        ]);
        $this->actingAs($user, 'api');

        return $user;
    }

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user    = $this->signInAsDispatch();
        $this->address = factory(Address::class)->create([
            'user_id' => $this->user->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_all_user_addresses()
    {
        $response = $this->get($this->baseUrl . '/addresses');
        $response->assertJson([
            'data' => [
                [
                    'type'       => 'addresses',
                    'id'         => $this->address->id,
                    'attributes' => [
                        'userId'           => $this->user->id,
                        'formattedAddress' => $this->address->formatted_address,
                        'lat'              => $this->address->lat,
                        'lng'              => $this->address->lng,
                        'placeId'          => $this->address->place_id,
                        'postalCode'       => $this->address->postal_code,
                        'unitNumber'       => $this->address->unit_number,
                        'businessName'     => $this->address->business_name,
                        'instructions'     => $this->address->instructions,
                        'selected'         => $this->address->selected,
                    ],
                    'links' => [
                        'self' => 'https://api.runner.test/v3/runner/users/' . $this->user->id . '/addresses/' . $this->address->id,
                    ],
                ],
            ],
        ]);
    }

    /**@test */
    public function test_it_can_create_update_address()
    {
        $updateAddress = [
            'data' => [
                [
                    'type'       => 'addresses',
                    'id'         => $this->address->id,
                    'attributes' => [
                        'userId'           => $this->user->id,
                        'formattedAddress' => 'Test address',
                        'lat'              => '43.7296809',
                        'lng'              => '43.7296809',
                        'selected'         => true,
                    ],
                ],
            ],
        ];
        $response = $this->put($this->baseUrl . '/users/' . $this->user->id . '/addresses/' . $this->address->id, $updateAddress);
        $response->assertStatus(200);
        $this->assertDatabaseHas('addresses', [
            'id'                => $this->address->id,
            'formatted_address' => $this->address->formatted_address,
            'lat'               => $this->address->lat,
            'lng'               => $this->address->lng,
            'place_id'          => $this->address->place_id,
            'postal_code'       => $this->address->postal_code,
            'unit_number'       => $this->address->unit_number,
            'business_name'     => $this->address->business_name,
            'instructions'      => $this->address->instructions,
        ]);
    }
}
