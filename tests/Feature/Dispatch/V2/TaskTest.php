<?php

namespace Tests\Feature\Dispatch\V2;

use App\Task;
use App\User;
use App\Order;
use App\Store;
use Tests\TestCase;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->product = factory(\App\Product::class)->create();
        $this->store   = factory(Store::class)->create();
        $this->content = json_encode([
            [
                'id'                => $this->product->id,
                'title'             => $this->product->title,
                'quantity'          => 3,
                'runner_price'      => $this->product->runner_price,
                'packaging'         => $this->product->packaging,
                'image'             => $this->product->image,
                'producingCountry'  => $this->product->producing_country,
                'isVintage'         => true,
            ],
        ]);
        $this->order = factory(Order::class)->create([
            'content'      => $this->content,
            'deleted_at'   => null,
            'store_id'     => $this->store->id,
            'inventory_id' => null,
            'incentive_id' => null,
            'app_version'  => null,
            'address_id'   => 121,
            'status'       => 1,
        ]);

        $this->courier = factory(\App\User::class)->create([
            'role' => 2,
        ]);
        $this->actingAs($this->courier, 'api');

        $this->task = factory(\App\Task::class)->create([
            'courier_id' => $this->courier->id,
            'order_id'   => $this->order->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_tasks()
    {
        $response = $this->get($this->baseUrl . '/tasks/' . $this->task->id);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_create_task()
    {
        $order = factory(Order::class)->create([
            'content'      => $this->content,
            'deleted_at'   => null,
            'store_id'     => $this->store->id,
            'inventory_id' => null,
            'incentive_id' => null,
            'app_version'  => null,
            'address_id'   => 121,
            'status'       => 1,
        ]);

        $data = [
            'data' => [
                'attributes' => [
                    'containerId' => 1,
                    'courierId'   => $this->courier->id,
                    'index'       => 1,
                    'orderId'     => $order->id,
                ],
            ],
        ];

        $this->mock(NotificationService::class, function ($mock) {
            $mock->allows('sendCourierPushNotification')->once();
            $mock->allows('orderDispatched')->once();
        });

        $response = $this->post($this->baseUrl . '/tasks', $data);

        $this->assertDatabaseHas('tasks', [
            'order_id' => $order->id
        ]);

        $response->assertStatus(201);
    }

    // public function test_creating_task_sends_push_notification()
    // {
    //     $task    = factory(Task::class)->create();
    //     $courier = factory(User::class)->create([
    //         'fcm_token' => 'cG9TPhzseq8:APA91bFVwQAvqsbjDMEJckIjOmHefuSsoDRU4RWz3-OlIwlMO90IjMg5tHZaBNXiGxhAHBQsF9i4s0y5qoeRQE9HHjO7l2yFarQSyzsEcxrqLB38Uv-Jqd64vjfjrMrJgh8hZymTUtVO'
    //     ]);

    //     $notificationService = new NotificationService();
    //     $notificationService->sendCourierPushNotification($task, $courier);
    // }

    /**
     * @test
     */
    // public function test_it_can_update_task()
    // {
    //     Event::fake();

    //     $runner = factory(\App\User::class)->create([
    //         'role' => 2,
    //     ]);
    //     $data = [
    //         'attributes' => [
    //             'containerId' => 1,
    //             'courierId'   => $runner->id,
    //             'index'       => 1,
    //         ],
    //     ];

    //     $response = $this->put($this->baseUrl . '/tasks/' . $this->task->id, $data);

    //     $task = $this->task;

    //     $response->assertStatus(200);
    // }
}
