<?php

namespace Tests\Feature\Api\Dispatch\V2;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/dispatch/v2';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = factory(User::class)->create([
            'role' => 5,
        ]);
        $this->artisan('passport:install');
    }

    /**
     * @test
     */
    public function test_it_can_login_as_dispatch()
    {
        $user = factory(User::class)->create([
            'role' => 1,
        ]);
        $response = $this->post($this->baseUrl . '/login', [
            'email'    => $user->email,
            'password' => 'secret',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_login_as_user()
    {
        $customer = factory(User::class)->create([
            'role' => 3,
        ]);
        $this->actingAs($this->user);
        $response = $this->post($this->baseUrl . '/login/' . $customer->id);

        $this->assertAuthenticated($guard = null);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }
}
