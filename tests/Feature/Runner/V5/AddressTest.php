<?php

namespace Tests\Feature\Runner\V5;

use DB;
use App\User;
use App\Inventory;
use App\Territory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_store_from_postal_code()
    {
        $inventory = factory(Inventory::class)->create([
            'store_id' => 1,
        ]);
        factory(\App\Store::class)->create();
        factory(\App\Store::class)->create([
            'id' => 8,
        ]);
        $territory = Territory::create([
            'title' => 'Test',
        ]);
        DB::table('postal_codes')->insert([
            'postal_code'  => 'M5H',
            'territory_id' => $territory->id
        ]);
        DB::table('inventory_territory')->insert([
            'inventory_id' => $inventory->id,
            'territory_id' => $territory->id,
        ]);
        DB::table('inventory_postal_code')->insert([
            'postal_code'  => 'M5H',
            'inventory_id' => $inventory->id,
        ]);

        $response = $this->get($this->baseUrl . '/address/validate?postalCode=M5H');
        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'title',
                    'store_logo',
                    'sub_domain',
                ],
            ],
        ]);
    }
}
