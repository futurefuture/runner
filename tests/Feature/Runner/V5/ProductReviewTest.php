<?php

namespace Tests\Feature\Runner\V5;

use App\Product;
use App\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductReviewTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_it_can_get_product_reviews()
    {
        $product = factory(Product::class)->create();
        factory(Review::class)->create([
            'product_id'     => $product->id,
        ]);

        $response = $this->get($this->baseUrl . '/products/' . $product->id . '/reviews');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'title',
                    'value',
                    'product_id',
                    'user_id',
                    'first_name',
                    'last_name',
                    'user_name',
                    'avatar_image',
                    'is_approved',
                    'description',
                    'helpful',
                    'unhelpful',
                    'created_at',
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_user_can_create_product_review()
    {
        $user    = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/products/' . $product->id . '/reviews', [
            'type'        => 'product reviews',
            'title'       => 'Test',
            'description' => 'testing',
            'value'       => 5,
            'user_id'     => $user->id,
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'title',
            'value',
            'product_id',
            'user_id',
            'first_name',
            'last_name',
            'user_name',
            'avatar_image',
            'is_approved',
            'description',
            'helpful',
            'unhelpful',
            'created_at',
        ]);
    }

    /**
     * test.
     */
    public function test_user_can_update_a_product_review_with_helpful()
    {
        $user    = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $review  = factory(Review::class)->create([
            'product_id' => $product->id
        ]);
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/products/' . $product->id . '/reviews/' . $review->id, [
            'type'       => 'product reviews',
            'helpful' => 1
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'title',
            'value',
            'product_id',
            'user_id',
            'first_name',
            'last_name',
            'user_name',
            'avatar_image',
            'is_approved',
            'description',
            'helpful',
            'unhelpful',
            'created_at',
        ]);

        $this->assertDatabaseHas('reviews', [
            'id'      => $review->id,
            'helpful' => 1
        ]);
    }

    /**
     * test.
     */
    public function test_user_can_update_a_product_review_with_unhelpful()
    {
        $user    = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $review  = factory(Review::class)->create([
            'product_id' => $product->id
        ]);
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/products/' . $product->id . '/reviews/' . $review->id, [
            'type'       => 'product reviews',
            'unhelpful' => 0
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'title',
            'value',
            'product_id',
            'user_id',
            'first_name',
            'last_name',
            'user_name',
            'avatar_image',
            'is_approved',
            'description',
            'helpful',
            'unhelpful',
            'created_at',
        ]);

        $this->assertDatabaseHas('reviews', [
            'id'        => $review->id,
            'unhelpful' => 1
        ]);
    }
}
