<?php

namespace Tests\Feature\Runner\V5;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        User::unsetEventDispatcher();
    }

    /**
     * @test
     **/
    public function test_user_can_register()
    {
        $user    = factory(User::class)->make();
        $address = [
            'formatted_address' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'               => '43.6406456',
            'lng'               => '-79.38093900000001',
            'place_id'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postal_code'       => 'M5J 2Y2',
        ];

        $response = $this->post($this->baseUrl . '/register', [
            'email'         => $user->email,
            'password'      => $user->password,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'phone_number'  => $user->phone_number,
            'date_of_birth' => $user->date_of_birth,
            'anonymous_id'  => '123123123',
            'is_legal_age'  => true,
            'address'       => $address,
        ]);

        $response->assertJsonStructure([
            'access_token',
            'user' => [
                'id',
                'first_name',
                'last_name',
            ],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
        ]);
        $this->assertDatabaseHas('addresses', [
            'place_id' => $address['place_id'],
        ]);
    }

    // /**
    //  * @test
    //  **/
    // public function test_referred_gets_points_when_registers()
    // {
    //     $referrer = factory(User::class)->create();
    //     $user     = factory(User::class)->make();

    //     DB::table('invite_user')->insert([
    //         'user_id'       => $referrer->id,
    //         'email_sent_to' => $user->email,
    //     ]);

    //     $address = [
    //         'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
    //         'lat'              => '43.6406456',
    //         'lng'              => '-79.38093900000001',
    //         'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
    //         'postalCode'       => 'M5J 2Y2',
    //     ];

    //     $response = $this->post($this->baseUrl . '/register', [
    //         'data' => [
    //             'attributes' => [
    //                 'email'       => $user->email,
    //                 'password'    => $user->password,
    //                 'firstName'   => $user->first_name,
    //                 'lastName'    => $user->last_name,
    //                 'phoneNumber' => $user->phone_number,
    //                 'dateOfBirth' => $user->date_of_birth,
    //                 'anonymousId' => '123123123',
    //                 'isLegalAge'  => true,
    //                 'address'     => $address,
    //             ],
    //         ],
    //     ]);

    //     $updatedUser = User::where('email', $user->email)->first();

    //     $response->assertJsonStructure([
    //         'data' => [
    //             'type',
    //             'attributes' => [
    //                 'accessToken',
    //                 'user' => [
    //                     'id',
    //                     'firstName',
    //                     'lastName',
    //                 ],
    //             ],
    //         ],
    //     ]);
    //     $response->assertStatus(200);
    //     $this->assertDatabaseHas('users', [
    //         'email' => $user->email,
    //     ]);
    //     $this->assertDatabaseHas('addresses', [
    //         'place_id' => $address['placeId'],
    //     ]);
    //     $this->assertDatabaseHas('reward_points', [
    //         'user_id' => $updatedUser->id,
    //         'value'   => 1000,
    //     ]);
    // }

    // /**
    //  * @test
    //  **/
    // public function test_user_cannot_register_with_blacklisted_email()
    // {
    //     $user = factory(User::class)->make([
    //         'email' => 'jarek@qq.com',
    //     ]);
    //     $address = [
    //         'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
    //         'lat'              => '43.6406456',
    //         'lng'              => '-79.38093900000001',
    //         'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
    //         'postalCode'       => 'M5J 2Y2',
    //     ];

    //     $response = $this->post($this->baseUrl . '/register', [
    //         'data' => [
    //             'attributes' => [
    //                 'email'       => $user->email,
    //                 'password'    => $user->password,
    //                 'firstName'   => $user->first_name,
    //                 'lastName'    => $user->last_name,
    //                 'phoneNumber' => $user->phone_number,
    //                 'dateOfBirth' => $user->date_of_birth,
    //                 'anonymousId' => '123123123',
    //                 'isLegalAge'  => true,
    //                 'address'     => $address,
    //             ],
    //         ],
    //     ]);

    //     $response->assertJsonStructure([
    //         'errors' => [
    //             'error',
    //         ],
    //     ]);
    //     $response->assertStatus(401);
    //     $this->assertDatabaseMissing('users', [
    //         'email' => $user->email,
    //     ]);
    // }

    // /**
    //  * @test
    //  **/
    // public function test_user_cannot_register_if_under_age()
    // {
    //     $user = factory(User::class)->make([
    //         'email' => 'jarek@qq.com',
    //     ]);
    //     $address = [
    //         'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
    //         'lat'              => '43.6406456',
    //         'lng'              => '-79.38093900000001',
    //         'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
    //         'postalCode'       => 'M5J 2Y2',
    //     ];

    //     $response = $this->post($this->baseUrl . '/register', [
    //         'data' => [
    //             'attributes' => [
    //                 'email'       => $user->email,
    //                 'password'    => $user->password,
    //                 'firstName'   => $user->first_name,
    //                 'lastName'    => $user->last_name,
    //                 'phoneNumber' => $user->phone_number,
    //                 'dateOfBirth' => $user->date_of_birth,
    //                 'anonymousId' => '123123123',
    //                 'isLegalAge'  => false,
    //                 'address'     => $address,
    //             ],
    //         ],
    //     ]);

    //     $response->assertJsonStructure([
    //         'errors' => [
    //             'error',
    //         ],
    //     ]);
    //     $response->assertStatus(401);
    //     $this->assertDatabaseMissing('users', [
    //         'email' => $user->email,
    //     ]);
    // }

    /**
     * @test
     */
    public function test_user_can_login()
    {
        $user = factory(User::class)->create();

        $response = $this->post($this->baseUrl . '/login', [
            'email'        => $user->email,
            'password'     => 'secret',
            'anonymous_id' => '23423423423',
        ]);

        $response->assertJsonStructure([
            'access_token',
            'user' => [
                'id',
                'first_name',
                'last_name',
                'google_id',
                'facebook_id',
                'apple_id'
            ]
        ]);

        $response->assertJson([
            'user' => [
                'first_name' => $user->first_name
            ]
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_google_login()
    {
        $user = factory(User::class)->create([
            'google_id' => '115398402302837625068',
        ]);

        $response = $this->post($this->baseUrl . '/login', [
            'email'        => $user->email,
            'password'     => 'secret',
            'anonymous_id' => '1231212',
            'google_id'    => $user->google_id
        ]);

        $response->assertJsonStructure([
            'access_token',
            'user' => [
                'id',
                'first_name',
                'last_name',
                'google_id',
                'facebook_id',
                'apple_id'
            ]
        ]);

        $response->assertJson([
            'user' => [
                'google_id' => $user->google_id
            ]
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_logout()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/logout');

        $response->assertStatus(200);
        $this->assertDatabaseMissing('oauth_access_tokens', [
            'user_id' => $user->id,
        ]);
    }

    public function test_null_is_returned_when_user_without_account_tries_logging_in_via_social()
    {
        $user = factory(User::class)->make();

        $response = $this->post($this->baseUrl . '/login', [
            'email'        => $user->email,
            'password'     => 'password',
            'anonymous_id' => '23423423423',
            'google_id'    => 'test',
        ]);

        $response->assertJson([
            'access_token' => null,
            'user'         => [
                'id'          => null,
                'first_name'  => null,
                'last_name'   => null,
                'google_id'   => null,
                'facebook_id' => null,
            ],
        ]);
    }
}
