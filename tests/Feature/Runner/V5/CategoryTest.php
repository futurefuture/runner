<?php

namespace Tests\Feature\Runner\V5;

use DB;
use App\Tag;
use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5/categories';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_categories()
    {
        factory(Category::class)->create();

        $response = $this->get($this->baseUrl);

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'title',
                    'parent_id',
                    'slug',
                    'options',
                    'square_image',
                    'banner_image',
                    'description',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_category()
    {
        $category = factory(Category::class)->create();

        $response = $this->get($this->baseUrl . '/' . $category->id);

        $response->assertJsonStructure([
            'type',
            'id',
            'title',
            'parent_id',
            'slug',
            'options',
            'square_image',
            'banner_image',
            'description',
        ]);
    }
}
