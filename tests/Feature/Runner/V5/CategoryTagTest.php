<?php

namespace Tests\Feature\Runner\V5;

use DB;
use App\Tag;
use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTagTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->category = factory(Category::class)->create();
        $this->tag      = factory(Tag::class)->create();
    }

    /**
     * @test
     */
    public function it_can_get_all_category_tags()
    {
        DB::table('category_tag')->insert([
            'tag_id'        => $this->tag->id,
            'category_id'   => $this->category->id,
        ]);

        $response = $this->get($this->baseUrl . '/category/' . $this->category->id . '/tags');
        $response->assertStatus(200);
    }
}
