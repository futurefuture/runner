<?php

namespace Tests\Feature\Runner\V5;

use App\Product;
use App\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5/products';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product()
    {
        $product = factory(Product::class)->create();

        $response = $this->get($this->baseUrl . '/' . $product->id);

        $response->assertJsonStructure([
            'type',
            'id',
            'sku',
            'upc',
            'is_active',
            'title',
            'long_description',
            'short_description',
            'retail_price',
            'wholesale_price',
            'limited_time_offer_price',
            'limited_time_offer_savings',
            'markup_percentage',
            'markup',
            'runner_price',
            'images',
            'image_thumbnail',
            'packaging',
            'alcohol_content',
            'sugar_content',
            'slug',
            'category_id',
            'style',
            'producing_country',
            'producer',
            'reward_points',
            'quantity',
            'case_deal',
            'incentives',
            'average_rating',
            'favourite_id',
            'reviews_count',
            'is_card',
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product_with_reviews()
    {
        $product = factory(Product::class)->create();
        $user    = factory(User::class)->create();
        factory(Review::class)->create([
            'user_id'    => $user->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get($this->baseUrl . '/' . $product->id . '?include=reviews');

        $response->assertJsonStructure([
            'type',
            'id',
            'sku',
            'upc',
            'is_active',
            'title',
            'long_description',
            'short_description',
            'retail_price',
            'wholesale_price',
            'limited_time_offer_price',
            'limited_time_offer_savings',
            'markup_percentage',
            'markup',
            'runner_price',
            'images',
            'image_thumbnail',
            'packaging',
            'alcohol_content',
            'sugar_content',
            'slug',
            'category_id',
            'style',
            'producing_country',
            'producer',
            'reward_points',
            'quantity',
            'case_deal',
            'incentives',
            'reviews',
            'average_rating',
            'favourite_id',
            'reviews_count',
            'is_card',
        ]);
    }
}
