<?php

namespace Tests\Feature\Runner\V5;

use DB;
use App\User;
use App\Store;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserFavouritesTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $storeOptions = [
            'storeLogo' => 'test',
        ];
        $this->store = factory(Store::class)->create([
            'options' => $storeOptions,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_users_favourites()
    {
        $user    = factory(\App\User::class)->create();
        $product = factory(Product::class)->create();

        DB::table('favourites')->insert([
            'user_id'    => $user->id,
            'product_id' => $product->id,
            'store_id'   => $this->store->id,
        ]);

        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/favourites');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'user_id',
                    'product_id',
                    'store',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_create_a_user_favourite()
    {
        $user    = factory(\App\User::class)->create();
        $product = factory(Product::class)->create();

        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/favourites', [
            'product_id' => $product->id,
            'store_id'   => $this->store->id,
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'user_id',
            'product_id',
            'store',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_user_favourites_by_store()
    {
        $user    = factory(\App\User::class)->create();
        $product = factory(Product::class)->create();
        DB::table('favourites')->insert([
            'user_id'    => $user->id,
            'product_id' => $product->id,
            'store_id'   => $this->store->id,
        ]);

        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/favourites/by-store');

        $response->assertJsonStructure([
            [
                'id',
                'title',
                'store_logo',
                'products',
            ],
        ]);
    }
}
