<?php

namespace Tests\Feature\Runner\V5;

use App\User;
use App\Refund;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefundTest extends TestCase
{
  use RefreshDatabase;

  protected $baseUrl = 'https://api.runner.test/runner/v5';

  protected function setUp(): void
  {
    parent::setUp();
    User::unsetEventDispatcher();
  }

  /**
   *
   * Test to see if refund row can be retrieved.
   *
   */
  public function test_it_can_get_refund()
  {
    $order = factory(\App\Order::class)->create([
      'id' => 123,
    ]);
    $user = factory(\App\User::class)->create([
      'id' => 321,
    ]);
    $this->actingAs($user, 'api');
    $refund = factory(Refund::class)->create([
      'order_id' => $order->id,
      'user_id'  => $user->id,
    ]);

    $response = $this->get($this->baseUrl . '/refund/' . $refund->id);
    $response->assertJsonStructure([
      'id',
      'order_id',
      'user_id',
      'refund_id',
      'charge_id',
      'amount',
      'reason',
    ]);
  }
}
