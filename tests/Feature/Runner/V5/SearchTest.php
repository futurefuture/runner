<?php

namespace Tests\Feature\Runner\V5;

use App\Store;
use App\Product;
use App\Inventory;
use App\PostalCode;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5/search';

    /**
     * test.
     */
    public function test_it_can_get_product_based_on_search_query()
    {
        $product    = factory(Product::class)->create([
            'id'        => 844,
            'title'     => 'Carlsberg',
            'is_active' => 1
        ]);
        $store      = factory(Store::class)->create([
            'options' => [
                'storeLogo' => 'test',
                'aboutText' => 'test'
            ]
        ]);
        $inventory  = factory(Inventory::class)->create([
            'store_id' => $store->id
        ]);
        $postalCode = factory(PostalCode::class)->create();
        $query      = $product->title;

        $inventory->productsAll()->attach($product, [
            'quantity' => 15,
        ]);

        $postalCode->inventories()->attach($inventory);
        $response = $this->get($this->baseUrl . '?q=' . $query . '&postalCode=' . $postalCode->postal_code);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'type',
                    'id',
                    'sku',
                    'upc',
                    'is_active',
                    'title',
                    'long_description',
                    'short_description',
                    'retail_price',
                    'markup_percentage',
                    'markup',
                    'runner_price',
                    'images' => [
                        [
                            'index',
                            'image',
                        ]
                    ],
                    'image_thumbnail',
                    'packaging',
                    'alcohol_content',
                    'sugar_content',
                    'slug',
                    'category_id',
                    'style',
                    'producing_country',
                    'producer',
                    'reward_points',
                    'quantity',
                    'case_deal',
                    'incentives' => [],
                    'average_rating',
                    'favourite_id',
                    'reviewsCount',
                    'stores' => [
                        'type',
                        'id',
                        'title',
                        'store_logo',
                        'sub_domain',
                        'about_text'
                    ]
                ]
            ]
        ]);
    }
}
