<?php

namespace Tests\Feature\Runner\V5;

use App\Address;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_a_user()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id);
        $response->assertJsonStructure([
            'type',
            'id',
            'first_name',
            'last_name',
            'date_of_birth',
            'phone_number',
            'email',
            'invite_code',
            'avatar_image',
            'reward_points_balance',
            'notifications' => [
                'promo_notification',
                'order_notification',
                'cart_notification'    
            ],
            'created_at',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_user_with_addresses_and_favourites()
    {
        $user = factory(User::class)->create();
        factory(Address::class)->create([
            'user_id' => $user->id,
        ]);
        $product = factory(Product::class)->create();
        DB::table('favourites')->insert([
            'user_id'    => $user->id,
            'product_id' => $product->id,
        ]);
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '?include=addresses,favourites');
        $response->assertJsonStructure([
            'type',
            'id',
            'first_name',
            'last_name',
            'date_of_birth',
            'phone_number',
            'email',
            'invite_code',
            'avatar_image',
            'reward_points_balance',
            'notifications' => [
                'promo_notification',
                'order_notification',
                'cart_notification'
            ],
            'created_at',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_a_user()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user->id, [
            'first_name' => 'test',
        ]);

        $this->assertDataBaseHas('users', [
            'first_name' => 'test',
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'first_name',
            'last_name',
            'date_of_birth',
            'phone_number',
            'email',
            'invite_code',
            'avatar_image',
            'reward_points_balance',
            'notifications' => [
                'promo_notification',
                'order_notification',
                'cart_notification'
            ],
            'created_at',
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_with_duplicate_email()
    {
        $user1 = factory(\App\User::class)->create([
            'email' => 'test@gmail.com',
        ]);
        $user2 = factory(\App\User::class)->create();
        $this->actingAs($user2, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user2->id, [
            'first_name' => 'test',
            'email'     => 'test@gmail.com',
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name' => $user2->first_name,
            'email'      => 'test@gmail.com',
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_with_duplicate_phone_number()
    {
        factory(\App\User::class)->create([
            'email' => '4161111111',
        ]);
        $user2 = factory(\App\User::class)->create();
        $this->actingAs($user2, 'api');

        $this->put($this->baseUrl . '/users/' . $user2->id, [
            'first_name'   => 'test',
            'phone_number' => '4161111111',
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name'   => $user2->first_name,
            'phone_number' => '4161111111',
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_password_if_old_password_is_incorrect()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user->id . '/update-password', [
            'old_password' => 'secret',
            'new_password' => bcrypt('test'),
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'first_name',
            'last_name',
            'date_of_birth',
            'phone_number',
            'email',
            'invite_code',
            'avatar_image',
            'reward_points_balance',
            'notifications' => [
                'promo_notification',
                'order_notification',
                'cart_notification'
            ],
            'created_at',
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name'   => $user->first_name,
            'password'     => bcrypt('test'),
        ]);
    }

    /**
     * @test
     */
    public function test_it_refer_a_friend_with_email_address()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/refer-a-friend', [
            'type'        => 'email',
            'contact_info' => 'jarek@wearefuturefuture.com',
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_refer_a_friend_with_a_phone_number()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/refer-a-friend', [
            'type'        => 'phoneNumber',
            'contact_info' => '6477746899',
        ]);

        $response->assertStatus(200);
    }
}
