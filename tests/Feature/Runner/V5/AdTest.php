<?php

namespace Tests\Feature\Runner\V5;

use App\Ad;
use App\Tag;
use App\User;
use App\Store;
use App\Partner;
use App\Product;
use App\Campaign;
use App\Category;
use App\Inventory;
use App\Territory;
use Carbon\Carbon;
use App\PostalCode;
use Tests\TestCase;
use App\StoreLayout;
use App\Services\SegmentService;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_fire_segment_ad_impression_for_ad_type_1()
    {
        $store = factory(Store::class)->create([
            'id' => 8,
        ]);
        $territory  = factory(Territory::class)->create();
        $inventory  = factory(Inventory::class)->create();
        $partner    = factory(Partner::class)->create();
        $campaign   = factory(Campaign::class)->create();
        $category   = factory(Category::class)->create();
        $product    = factory(Product::class)->create([
            'category_id' => $category->id
        ]);
        DB::table('inventory_product')->insert([
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 3
        ]);
        $tag        = factory(Tag::class)->create();
        $postalCode = factory(PostalCode::class)->create([
            'territory_id' => $territory->id
        ]);
        $layout     = [
            [
                'type'       => 'layout patterns',
                'id'         => '1',
                'sort_order' => 2,
                'url'       => 'runner\/v4\/inventories\/4\/categories\/2\/products?filter[tags]=lcbo-featured-beer-cider&limit=5', 'icon' => '',
                'title'     => 'Featured Beer & Ciders',
                'sub_title'  => 'Select Beer & Cider delivered directly to your door in less than 2hrs',
                'link'      => [
                    'title'      => 'Shop All',
                    'sub_title'   => null,
                    'type'       => 'filters',
                    'category_id' => '2',
                    'store_id'    => '1',
                    'url'        => 'runner\/v4\/inventories\/4\/categories\/2\/beer-cider?filter[tags]=lcbo-featured-beer-cider&limit=5',
                    'pretty_url'  => 'category\/beer-cider?filter[tags]=lcbo-featured-beer-cider',
                    'sub_domain'  => 'lcbo',
                    'filters'    => [
                        [
                            'type'       => 'tags',
                            'id'         => '40',
                            'title' => 'Featured Beer & Ciders',
                            'slug'  => 'lcbo-featured-beer-cider',
                            'type'  => 'promotion'
                        ]
                    ]
                ]
            ]
        ];
        factory(StoreLayout::class)->create([
            'store_id'     => $store->id,
            'territory_id' => $territory->id,
            'layout'       => $layout
        ]);

        factory(Ad::class)->create([
            'ad_type_id'  => 1,
            'partner_id'  => $partner->id,
            'campaign_id' => $campaign->id,
            'products'    => '[' . $product->id . ']',
            'tag_id'      => $tag->id,
            'start_date'  => Carbon::now()->subDays(3),
            'end_date'    => Carbon::now()->addDays(3)
        ]);

        $this->mock(SegmentService::class, function ($mock) {
            $mock->allows('trackAdImpression')->once();
        });

        $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products?filter[tags]=' . $tag->slug);
    }
}
