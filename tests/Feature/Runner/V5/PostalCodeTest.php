<?php

namespace Tests\Feature\Runner\V5;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostalCodeTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5/postal-codes';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_store_out_of_bounds_postal_code()
    {
        $postalCode = 'XXX';

        $response = $this->post($this->baseUrl . '/out-of-bounds', [
            'postal_code' => $postalCode,
        ]);

        $this->assertDatabaseHas('out_of_bounds_postal_codes', [
            'postal_code' => $postalCode,
        ]);

        $response->assertJsonStructure([
            'type',
            'id',
            'postal_code',
        ]);
    }
}
