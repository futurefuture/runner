<?php

namespace Tests\Feature\Runner\V5;

use App\Cart;
use App\User;
use App\Product;
use App\Category;
use App\Inventory;
use Tests\TestCase;
use App\Actions\V5\UpdateUserInventoryCartAction;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserInventoryCartTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5/users';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user      = factory(User::class)->create();
        $this->inventory = factory(Inventory::class)->create();
        $this->actingAs($this->user, 'api');
    }

    /**
     * @test
     */
    public function test_it_can_get_a_user_cart_by_inventory()
    {
        $cart = factory(Cart::class)->create([
            'inventory_id' => $this->inventory->id,
            'user_id'      => $this->user->id,
        ]);

        $response = $this->get($this->baseUrl . '/' . $this->user->id . '/inventories/' . $this->inventory->id . '/carts/' . $cart->id);

        $response->assertJsonStructure([
            'type',
            'id',
            'products',
            'coupon',
            'incentive',
            'sub_total',
            'delivery_fee',
            'service_fee',
            'tax',
            'discount',
            'total',
            'is_gift',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_create_a_user_cart_by_inventory()
    {
        $category = factory(Category::class)->create();
        $product  = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);

        $response = $this->post($this->baseUrl . '/' . $this->user->id . '/inventories/' . $this->inventory->id . '/carts/', [
            'type'        => 'carts',
            'anonymousId' => '324',
            'products'    => [
                [
                    'id'          => $product->id,
                    'quantity'    => 1,
                    'allow_sub'   => true,
                ],
            ],
        ]);

        $this->assertDatabaseHas('carts', [
            'user_id'      => $this->user->id,
            'inventory_id' => $this->inventory->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_a_user_cart_by_inventory()
    {
        $cart = factory(Cart::class)->create([
            'inventory_id' => $this->inventory->id,
            'user_id'      => $this->user->id,
        ]);
        $category = factory(Category::class)->create();
        $product  = factory(Product::class)->create([
            'category_id'  => $category->id,
            'runner_price' => 1000
        ]);

        $response = $this->put($this->baseUrl . '/' . $this->user->id . '/inventories/' . $this->inventory->id . '/carts/' . $cart->id, [
            'type'       => 'carts',
            'anonymous_id' => '1231231',
            'products'    => [
                [
                    'id'          => $product->id,
                    'quantity'    => 1,
                    'allow_sub'    => true,
                    'type'        => 'regular',
                ],
            ],
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('carts', [
            'user_id'      => $this->user->id,
            'inventory_id' => $this->inventory->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_gets_proper_sub_total_when_update_cart()
    {
        $cart = factory(Cart::class)->create([
            'inventory_id' => $this->inventory->id,
            'user_id'      => $this->user->id,
        ]);
        $category = factory(Category::class)->create();
        $product  = factory(Product::class)->create([
            'runner_price' => 1000,
            'category_id'  => $category->id
        ]);
        $data = [
            'anonymousId' => '324',
            'products'    => [
                [
                    'id'          => $product->id,
                    'quantity'    => 2,
                    'allow_sub'   => true,
                    'type'        => 'regular',
                ],
            ],
        ];

        $updatedCart = new UpdateUserInventoryCartAction();

        $content          = json_decode(json_decode($updatedCart->execute($this->user, $cart->id, $data))->content);
        $products         = $content->products;
        $productsSubTotal = 0;

        foreach ($products as $p) {
            $productsSubTotal += $p->quantity * $p->runnerPrice;
        }

        $this->assertTrue($productsSubTotal === $content->subTotal);
    }
}
