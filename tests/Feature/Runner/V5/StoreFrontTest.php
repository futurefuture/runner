<?php

namespace Tests\Feature\Runner\V5;

use App\User;
use App\Store;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreFrontTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v5';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_storefront()
    {
        $store = factory(Store::class)->create([
            'id' => 8,
        ]);
        $inventory = factory(Inventory::class)->create([
            'store_id' => $store->id,
        ]);

        $response = $this->get($this->baseUrl . '/store-fronts/' . $inventory->id);

        $response->assertJsonStructure([
            'type',
            'id',
            'inventory' => [
                'id',
                'vendor',
                'address',
                'is_open',
                'notification',
                'hours' => [],
            ],
            'store_logo',
            'location_id',
            'sub_domain',
            'sort_order',
            'tower_image',
            'layouts',
        ]);
    }
}
