<?php

namespace Tests\Feature\Runner\V4;

use App\Ad;
use App\User;
use App\Review;
use App\Product;
use App\Category;
use App\Inventory;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InventoryCategoryProductTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_it_can_get_products()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'upc',
                        'isActive',
                        'title',
                        'longDescription',
                        'shortDescription',
                        'retailPrice',
                        'wholesalePrice',
                        'limitedTimeOfferPrice',
                        'limitedTimeOfferSavings',
                        'markupPercentage',
                        'markup',
                        'runnerPrice',
                        'images',
                        'imageThumbnail',
                        'packaging',
                        'alcoholContent',
                        'sugarContent',
                        'slug',
                        'categoryId',
                        'style',
                        'producingCountry',
                        'producer',
                        'rewardPoints',
                        'quantity',
                        'caseDeal',
                        'incentives',
                        'averageRating',
                        'favouriteId',
                        'reviewsCount',
                        'isCard',
                    ],
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_products_with_reviews()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        $user = factory(User::class)->create();
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);
        factory(Review::class)->create([
            'product_id' => $product->id,
            'user_id'    => $user->id,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products?include=reviews');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'upc',
                        'isActive',
                        'title',
                        'longDescription',
                        'shortDescription',
                        'retailPrice',
                        'wholesalePrice',
                        'limitedTimeOfferPrice',
                        'limitedTimeOfferSavings',
                        'markupPercentage',
                        'markup',
                        'runnerPrice',
                        'images',
                        'imageThumbnail',
                        'packaging',
                        'alcoholContent',
                        'sugarContent',
                        'slug',
                        'categoryId',
                        'style',
                        'producingCountry',
                        'producer',
                        'rewardPoints',
                        'quantity',
                        'caseDeal',
                        'incentives',
                        'averageRating',
                        'favouriteId',
                        'reviewsCount',
                        'reviews',
                        'isCard',
                    ],
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        $ad = factory(Ad::class)->create([
            'product_id' => $product->id
        ]);
        $user = factory(User::class)->create();
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        factory(Review::class)->create([
            'product_id' => $product->id,
            'user_id'    => $user->id,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products/' . $product->id . '?include=reviews');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'sku',
                    'upc',
                    'isActive',
                    'title',
                    'longDescription',
                    'shortDescription',
                    'retailPrice',
                    'wholesalePrice',
                    'limitedTimeOfferPrice',
                    'limitedTimeOfferSavings',
                    'markupPercentage',
                    'markup',
                    'runnerPrice',
                    'images',
                    'imageThumbnail',
                    'packaging',
                    'alcoholContent',
                    'sugarContent',
                    'slug',
                    'categoryId',
                    'style',
                    'producingCountry',
                    'producer',
                    'rewardPoints',
                    'quantity',
                    'caseDeal',
                    'incentives',
                    'reviews',
                    'averageRating',
                    'favouriteId',
                    'reviewsCount',
                    'isCard',
                    'ads'
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product_with_reviews()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products/' . $product->id);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'sku',
                    'upc',
                    'isActive',
                    'title',
                    'longDescription',
                    'shortDescription',
                    'retailPrice',
                    'wholesalePrice',
                    'limitedTimeOfferPrice',
                    'limitedTimeOfferSavings',
                    'markupPercentage',
                    'markup',
                    'runnerPrice',
                    'images',
                    'imageThumbnail',
                    'packaging',
                    'alcoholContent',
                    'sugarContent',
                    'slug',
                    'categoryId',
                    'style',
                    'producingCountry',
                    'producer',
                    'rewardPoints',
                    'quantity',
                    'caseDeal',
                    'incentives',
                    'averageRating',
                    'favouriteId',
                    'reviewsCount',
                    'isCard',
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_related_products_from_a_product()
    {
        $inventory      = factory(Inventory::class)->create();
        $category       = factory(Category::class)->create();
        $product        = factory(Product::class)->create();
        $relatedProduct = factory(Product::class)->create();
        DB::table('category_product')->insert([
            'category_id' => $category->id,
            'product_id'  => $product->id,
        ]);
        DB::table('category_product')->insert([
            'category_id' => $category->id,
            'product_id'  => $relatedProduct->id,
        ]);
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 10,
        ]);
        DB::table('inventory_product')->insert([
            'product_id'   => $relatedProduct->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 10,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);
        DB::table('related_products')->insert([
            'product_id'         => $product->id,
            'related_product_id' => $relatedProduct->id,
            'index'              => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products/' . $product->id . '/related-products');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'upc',
                        'isActive',
                        'title',
                        'longDescription',
                        'shortDescription',
                        'retailPrice',
                        'wholesalePrice',
                        'limitedTimeOfferPrice',
                        'limitedTimeOfferSavings',
                        'markupPercentage',
                        'markup',
                        'runnerPrice',
                        'images',
                        'imageThumbnail',
                        'packaging',
                        'alcoholContent',
                        'sugarContent',
                        'slug',
                        'categoryId',
                        'style',
                        'producingCountry',
                        'producer',
                        'rewardPoints',
                        'quantity',
                        'caseDeal',
                        'incentives',
                        'averageRating',
                        'favouriteId',
                        'reviewsCount',
                        'isCard',
                    ],
                ],
            ],
        ]);
    }

    public function test_a_product_has_an_ad_relationship_when_ad_is_active()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        $ad = factory(Ad::class)->create([
            'id'         => 1234,
            'product_id' => $product->id,
            'ad_type_id' => 1,
            'start_date' => Carbon::now()->subDays(1),
            'end_date'   => Carbon::now()->addDays(1)
        ]);
        $user = factory(User::class)->create();
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        factory(Review::class)->create([
            'product_id' => $product->id,
            'user_id'    => $user->id,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products/' . $product->id . '?include=reviews');

        $response->assertJsonFragment([
            'id'   => $ad->id,
        ]);
    }

    public function test_a_product_does_not_have_an_ad_relationship_when_ad_is_inactive()
    {
        $inventory = factory(Inventory::class)->create();
        $category  = factory(Category::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);
        $ad = factory(Ad::class)->create([
            'id'         => 1234,
            'product_id' => $product->id,
            'start_date' => Carbon::now()->addDays(1),
            'end_date'   => Carbon::now()->addDays(2)
        ]);
        $user = factory(User::class)->create();
        DB::table('inventory_product')->insert([
            'product_id'   => $product->id,
            'inventory_id' => $inventory->id,
            'quantity'     => 0,
        ]);
        factory(Review::class)->create([
            'product_id' => $product->id,
            'user_id'    => $user->id,
        ]);
        DB::table('category_product')->insert([
            'product_id'  => $product->id,
            'category_id' => $category->id,
            'index'       => 0,
        ]);

        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/categories/' . $category->id . '/products/' . $product->id . '?include=reviews');

        $response->assertJsonMissing([
            'id' => $ad->id
        ]);
    }
}
