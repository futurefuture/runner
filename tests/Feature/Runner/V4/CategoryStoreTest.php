<?php

namespace Tests\Feature\Runner\V4;

use DB;
use App\User;
use App\Store;
use App\Product;
use App\Category;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryStoreTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_stores_from_category_and_postal_code()
    {
        $category  = factory(Category::class)->create();
        $store     = factory(Store::class)->create();
        $inventory = factory(Inventory::class)->create();
        $product   = factory(Product::class)->create([
            'category_id' => $category->id,
        ]);

        DB::table('inventory_postal_code')->insert([
            'postal_code'  => 'M5H',
            'inventory_id' => $inventory->id,
        ]);

        DB::table('inventory_product')->insert([
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 10,
        ]);

        $response = $this->get($this->baseUrl . '/categories/' . $category->id . '/stores?postalCode=M5H 3J2');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'title',
                        'storeLogo',
                        'navigationLogo',
                        'hours',
                        'blockImage',
                        'nextDeliveryTime',
                        'aboutText',
                        'subDomain',
                        'inventoryId',
                        'towerImage',
                        'categories',
                    ],
                ],
            ],
        ]);
    }
}
