<?php

namespace Tests\Feature\Runner\V4;

use App\Cart;
use App\User;
use App\Store;
use App\Address;
use App\Product;
use App\Inventory;
use App\Territory;
use Carbon\Carbon;
use App\PostalCode;
use Tests\TestCase;
use App\StoreLayout;
use App\InventoryProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserOrderTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = factory(\App\User::class)->create([
            'stripe_id' => 'cus_E2q7YMxvJWngvl',
        ]);
        $this->actingAs($this->user, 'api');
    }

    /**
     * @test
     */
    public function test_it_can_re_order_last_order_from_specific_inventory()
    {
        $pastOrder = factory(\App\Order::class)->states('new')->create([
            'user_id'      => $this->user->id,
            'inventory_id' => 1,
            'status'       => 3,
        ]);
        $products       = json_decode($pastOrder->content);
        $subtotal       = (int) $products[0]->runnerPrice * $products[0]->quantity;
        $tax            = ((number_format((float) $products[0]->runnerPrice, 2, '.', '') - number_format((float) $products[0]->runnerPrice, 2, '.', '') / 1.12) * 0.13) * $products[0]->quantity;

        $response = $this->post($this->baseUrl . '/users/' . $this->user->id . '/orders/' . $pastOrder->id . '/re-order', [
            'data' => [
                'attributes' => [
                    'inventoryId' => 1,
                ],
            ],
        ]);

        $this->assertEquals($subtotal, json_decode($response->getContent())->data->attributes->subTotal);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'products',
                    'coupon',
                    'subTotal',
                    'deliveryFee',
                    'tax',
                    'discount',
                    'total',
                    'inventoryId',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_get_user_orders()
    {
        $courier = factory(\App\User::class)->create();
        $store   = factory(Store::class)->create();
        $order   = factory(\App\Order::class)->states('new')->create([
            'status'   => 3,
            'user_id'  => $this->user->id,
            'runner_1' => $courier->id,
            'store_id' => $store->id,
        ]);

        $response = $this->get($this->baseUrl . '/users/' . $this->user->id . '/orders');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_get_a_user_order()
    {
        $courier = factory(\App\User::class)->create();
        $store   = factory(\App\Store::class)->create();
        $order   = factory(\App\Order::class)->states('new')->create([
            'status'   => 3,
            'runner_1' => $courier->id,
            'user_id'  => $this->user->id,
            'store_id' => $store->id
        ]);

        $response = $this->get($this->baseUrl . '/users/' . $this->user->id . '/orders/' . $order->id);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'userId',
                    'status',
                    'items',
                    'courier',
                    'device',
                    'customer',
                    'address',
                    'unitNumber',
                    'instructions',
                    'subTotal',
                    'serviceFee',
                    'discount',
                    'discountType',
                    'delivery',
                    'tip',
                    'displayTaxTotal',
                    'total',
                    'signature',
                    'scheduledTime',
                    'deliveredAt',
                    'createdAt',
                    'gift',
                ],
            ],
        ]);
    }

    public function test_it_can_create_a_user_order()
    {
        $this->withoutExceptionHandling();
        $store      = factory(Store::class)->create();
        $territory  = factory(Territory::class)->create();
        $postalCode = factory(PostalCode::class)->create([
            'territory_id' => $territory->id
        ]);
        $address    = factory(Address::class)->create([
            'user_id'     => $this->user->id,
            'postal_code' => $postalCode->postal_code,
            'selected'    => 1
        ]);
        $inventory = factory(Inventory::class)->create([
            'store_id' => $store->id,
        ]);
        DB::table('inventory_territory')
            ->insert([
                'inventory_id' => $inventory->id,
                'territory_id' => $territory->id
            ]);
        DB::table('inventory_postal_code')
            ->insert([
                'postal_code'  => $postalCode->postal_code,
                'inventory_id' => $inventory->id
            ]);
        factory(StoreLayout::class)->create([
            'can_take_payment' => 1,
            'territory_id'     => $territory->id,
            'store_id'         => $store->id
        ]);
        $product          = factory(Product::class)->create();
        $inventoryProduct = InventoryProduct::create([
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 6
        ]);

        $products = [
            [
                'id'          => $product->id,
                'title'       => $product->title,
                'runnerPrice' => (int) $product->runner_price,
                'retailPrice' => (int) $product->retail_price,
                'quantity'    => 4,
                'packaging'   => $product->packaging,
                'allowSub'    => true,
                'type'        => 'regular',
                'subTotal'    => (int) $product->runner_price * 4,
            ],
        ];
        $cartContent = json_encode([
            'products'     => $products,
            'subTotal'     => (int) $product->runner_price * 4,
            'deliveryFee'  => 1000,
            'tax'          => 34,
            'rewardPoints' => 0,
            'discount'     => 0,
            'total'        => ((int) $product->runner_price * 4) + 1000
        ]);
        factory(Cart::class)->create([
            'user_id'      => $this->user->id,
            'inventory_id' => $inventory->id,
            'content'      => $cartContent,
        ]);
        DB::table('config')->insert([
            'key'   => 'close_store',
            'value' => 0,
        ]);
        $now      = Carbon::now()->toDateTimeString();
        $customer = json_encode((object) [
            'firstName'   => $this->user->first_name,
            'lastName'    => $this->user->last_name,
            'phoneNumber' => (string) $this->user->phone_number,
            'email'       => $this->user->email,
        ]);
        $formattedAddress = json_encode((object) [
            'address' => $address->formatted_address,
            'lat'     => (float) $address->lat,
            'lng'     => (float) $address->lng,
        ]);
        $response = $this->post($this->baseUrl . '/users/' . $this->user->id . '/orders/', [
            'data' => [
                'attributes' => [
                    'anonymousId'   => '123',
                    'inventoryId'   => $inventory->id,
                    'device'        => 1,
                    'userId'        => $this->user->id,
                    'appVersion'    => '4.0',
                    'isGift'        => false,
                    'orderDateTime' => $now,
                    'tip'           => '500',
                ],
            ],
        ]);

        $markup = ((int) $product->runner_price * 4) - ((int) $product->retail_price * 4);
        $total  = (int) ((int) $product->runner_price * 4) + 1000 + 500;

        $this->assertDatabaseHas('orders', [
            'user_id'           => $this->user->id,
            'status'            => 7,
            'content'           => json_encode($products),
            'runner_1'          => null,
            'device'            => 1,
            'customer'          => $customer,
            'subtotal'          => (int) $product->runner_price * 4,
            'address'           => $formattedAddress,
            'cogs'              => $product->retail_price * 4,
            'discount'          => 0,
            'delivery'          => 1000,
            'delivery_tax'      => 130,
            'markup'            => $markup,
            'markup_tax'        => (int) ($markup * 0.13),
            'display_tax_total' => (int) ($markup * 0.13) + 130,
            'tip'               => 500,
            'total'             => $total,
            'stripe_fee'        => (int) (($total * 0.023) + 30),
            'refund'            => null,
            'signature'         => null,
            'delivered_at'      => null,
            // 'schedule_time'     => Carbon::now()->toDateTimeString(),
            'additional_info'   => null,
            'first_time'        => 1,
            'service_fee'       => 0,
            'incentive_id'      => null,
            'store_id'          => $store->id,
            'inventory_id'      => $inventory->id,
            'app_version'       => '4.0',
            'address_id'        => $address->id,
            'postal_code'       => $address->postal_code
        ]);

        $this->assertDatabaseHas('inventory_product', [
            'inventory_id' => $inventory->id,
            'product_id'   => $product->id,
            'quantity'     => 2
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'userId',
                    'status',
                    'scheduledTime',
                    'items',
                    'courier',
                    'device',
                    'customer',
                    'address',
                    'unitNumber',
                    'instructions',
                    'subTotal',
                    'discount',
                    'delivery',
                    'displayTaxTotal',
                    'tip',
                    'total',
                    'serviceFee',
                    'createdAt',
                ],
            ],
        ]);
    }

    public function test_it_cannot_create_a_user_order_if_store_layout_closed()
    {
        $user = factory(User::class)->create([
            'stripe_id' => 'cus_E2q7YMxvJWngvl',
        ]);
        $territory  = factory(Territory::class)->create();
        $postalCode = factory(PostalCode::class)->create([
            'territory_id' => $territory->id
        ]);
        $address    = factory(Address::class)->create([
            'user_id'     => $user->id,
            'postal_code' => $postalCode->postal_code,
            'selected'    => 1
        ]);
        $store     = factory(Store::class)->create();
        $inventory = factory(Inventory::class)->create([
            'store_id' => $store->id,
        ]);
        DB::table('inventory_territory')
            ->insert([
                'inventory_id' => $inventory->id,
                'territory_id' => $territory->id
            ]);
        DB::table('inventory_postal_code')
            ->insert([
                'postal_code'  => $postalCode->postal_code,
                'inventory_id' => $inventory->id
            ]);
        factory(StoreLayout::class)->create([
            'can_take_payment' => 0,
            'territory_id'     => $territory->id,
            'store_id'         => $store->id
        ]);
        $product  = factory(Product::class)->create();
        $products = [
            [
                'id'          => $product->id,
                'title'       => $product->title,
                'runnerPrice' => $product->runner_price,
                'quantity'    => 1,
                'packaging'   => $product->packaging,
                'allowSub'    => true,
                'type'        => 'regular',
                'subTotal'    => $product->runner_price,
            ],
        ];
        $cartContent = json_encode([
            'products'     => $products,
            'subTotal'     => 321,
            'deliveryFee'  => 999,
            'tax'          => 34,
            'rewardPoints' => 0,
            'discount'     => 0,
            'total'        => 1354,
        ]);
        factory(Cart::class)->create([
            'user_id'      => $user->id,
            'inventory_id' => $inventory->id,
            'content'      => $cartContent,
        ]);

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/orders/', [
            'data' => [
                'attributes' => [
                    'anonymousId'   => '123',
                    'inventoryId'   => $inventory->id,
                    'device'        => 1,
                    'userId'        => $user->id,
                    'appVersion'    => '4.0',
                    'isGift'        => false,
                    'orderDateTime' => '2019-12-09 12:00:00',
                    'tip'           => '500',
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);
    }

    public function test_it_cannot_create_a_user_order_if_global_payment_closed()
    {
        $user = factory(User::class)->create([
            'stripe_id' => 'cus_E2q7YMxvJWngvl',
        ]);
        $territory  = factory(Territory::class)->create();
        $postalCode = factory(PostalCode::class)->create([
            'territory_id' => $territory->id
        ]);
        factory(Address::class)->create([
            'user_id'     => $user->id,
            'postal_code' => $postalCode->postal_code,
            'selected'    => 1
        ]);
        $store     = factory(Store::class)->create();
        $inventory = factory(Inventory::class)->create([
            'store_id' => $store->id,
        ]);
        DB::table('inventory_territory')
            ->insert([
                'inventory_id' => $inventory->id,
                'territory_id' => $territory->id
            ]);
        DB::table('inventory_postal_code')
            ->insert([
                'postal_code'  => $postalCode->postal_code,
                'inventory_id' => $inventory->id
            ]);
        factory(StoreLayout::class)->create([
            'can_take_payment' => 1,
            'territory_id'     => $territory->id,
            'store_id'         => $store->id
        ]);
        $product  = factory(Product::class)->create();
        $products = [
            [
                'id'          => $product->id,
                'title'       => $product->title,
                'runnerPrice' => $product->runner_price,
                'quantity'    => 1,
                'packaging'   => $product->packaging,
                'allowSub'    => true,
                'type'        => 'regular',
                'subTotal'    => $product->runner_price,
            ],
        ];
        $cartContent = json_encode([
            'products'     => $products,
            'subTotal'     => 321,
            'deliveryFee'  => 999,
            'tax'          => 34,
            'rewardPoints' => 0,
            'discount'     => 0,
            'total'        => 1354,
        ]);
        factory(Cart::class)->create([
            'user_id'      => $user->id,
            'inventory_id' => $inventory->id,
            'content'      => $cartContent,
        ]);
        DB::table('config')->insert([
            'key'   => 'close_store',
            'value' => 'true',
        ]);

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/orders/', [
            'data' => [
                'attributes' => [
                    'anonymousId'   => '123',
                    'inventoryId'   => $inventory->id,
                    'device'        => 1,
                    'userId'        => $user->id,
                    'appVersion'    => '4.0',
                    'isGift'        => false,
                    'orderDateTime' => '2019-12-09 12:00:00',
                    'tip'           => '500',
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);
    }
}
