<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use App\Survey;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SurveyTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_it_can_get_surveys()
    {
        $survey   = factory(Survey::class)->create();
        $response = $this->get($this->baseUrl . '/surveys/');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'title',
                        'startDate',
                        'endDate',
                        'results',
                        'segment',
                        'partnerId',
                        'createdAt',
                        'updatedAt',
                    ],
                ],
            ],
        ]);
    }
}
