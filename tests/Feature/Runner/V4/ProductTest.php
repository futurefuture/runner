<?php

namespace Tests\Feature\Runner\V4;

use App\Product;
use App\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4/products';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product()
    {
        $product = factory(Product::class)->create();

        $response = $this->get($this->baseUrl . '/' . $product->id);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'sku',
                    'upc',
                    'isActive',
                    'title',
                    'longDescription',
                    'shortDescription',
                    'retailPrice',
                    'wholesalePrice',
                    'limitedTimeOfferPrice',
                    'limitedTimeOfferSavings',
                    'markupPercentage',
                    'markup',
                    'runnerPrice',
                    'images',
                    'imageThumbnail',
                    'packaging',
                    'alcoholContent',
                    'sugarContent',
                    'slug',
                    'categoryId',
                    'style',
                    'producingCountry',
                    'producer',
                    'rewardPoints',
                    'quantity',
                    'caseDeal',
                    'incentives',
                    'averageRating',
                    'favouriteId',
                    'reviewsCount',
                    'isCard',
                ],
            ],
        ]);
    }

    /**
     * test.
     */
    public function test_it_can_get_a_product_with_reviews()
    {
        $product = factory(Product::class)->create();
        $user    = factory(User::class)->create();
        factory(Review::class)->create([
            'user_id'    => $user->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get($this->baseUrl . '/' . $product->id . '?include=reviews');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'sku',
                    'upc',
                    'isActive',
                    'title',
                    'longDescription',
                    'shortDescription',
                    'retailPrice',
                    'wholesalePrice',
                    'limitedTimeOfferPrice',
                    'limitedTimeOfferSavings',
                    'markupPercentage',
                    'markup',
                    'runnerPrice',
                    'images',
                    'imageThumbnail',
                    'packaging',
                    'alcoholContent',
                    'sugarContent',
                    'slug',
                    'categoryId',
                    'style',
                    'producingCountry',
                    'producer',
                    'rewardPoints',
                    'quantity',
                    'caseDeal',
                    'incentives',
                    'reviews',
                    'averageRating',
                    'favouriteId',
                    'reviewsCount',
                    'isCard',
                ],
            ],
        ]);
    }
}
