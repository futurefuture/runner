<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UserRewardPointTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4/users';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_user_reward_points()
    {
        $user = factory(User::class)->create();
        DB::table('reward_points')->insert([
            'user_id' => $user->id,
            'value'   => 100,
        ]);
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/' . $user->id . '/reward-points');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'userId',
                        'value',
                        'type',
                        'details',
                        'createdAt',
                        'updatedAt',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_customer()
    {
        $user = factory(User::class)->create([
            'stripe_id' => 'cus_E2q7YMxvJWngvl',
        ]);
        $this->actingAs($user, 'api');
        $response = $this->get($this->baseUrl . '/' . $user->id . '/stripe/customer');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes',
            ],
        ]);
    }
}
