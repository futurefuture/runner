<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use App\Refund;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefundTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     *
     * Test to see if refund row can be retrieved.
     *
     */
    public function test_it_can_get_refund()
    {
        $order = factory(\App\Order::class)->create([
      'id' => 123,
    ]);
        $user = factory(\App\User::class)->create([
      'id' => 321,
    ]);
        $this->actingAs($user, 'api');
        $refund = factory(Refund::class)->create([
      'order_id' => $order->id,
      'user_id'  => $user->id,
    ]);

        $response = $this->get($this->baseUrl . '/refund/' . $refund->id);
        $response->assertJsonStructure([
      'data' => [
        'id',
        'attributes' => [
          'orderId',
          'userId',
          'refundId',
          'chargeId',
          'amount',
          'reason',
        ],
      ],
    ]);
    }

    /**
     *
     * Test to see if refund row can be deleted via soft delete
     *
     */
    public function test_it_can_remove_refund()
    {
        $order = factory(\App\Order::class)->create([
      'id' => 123,
    ]);
        $user = factory(\App\User::class)->create([
      'id' => 321,
    ]);
        $this->actingAs($user, 'api');
        $refund = factory(Refund::class)->create([
      'order_id' => $order->id,
      'user_id'  => $user->id,
    ]);
        $this->delete($this->baseUrl . '/refund/' . $refund->id);
        $this->assertSoftDeleted('refunds', [
      'id' => $refund->id,
    ]);
    }
}
