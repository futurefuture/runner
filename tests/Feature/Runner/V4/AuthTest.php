<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        User::unsetEventDispatcher();
    }

    /**
     * @test
     **/
    public function test_user_can_register()
    {
        $user    = factory(User::class)->make();
        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
        ]);
        $this->assertDatabaseHas('addresses', [
            'place_id' => $address['placeId'],
        ]);
    }

    /**
     * @test
     **/
    public function test_referred_gets_points_when_registers()
    {
        $referrer = factory(User::class)->create();
        $user     = factory(User::class)->make();

        DB::table('invite_user')->insert([
            'user_id'       => $referrer->id,
            'email_sent_to' => $user->email,
        ]);

        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);

        $updatedUser = User::where('email', $user->email)->first();

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
        ]);
        $this->assertDatabaseHas('addresses', [
            'place_id' => $address['placeId'],
        ]);
        $this->assertDatabaseHas('reward_points', [
            'user_id' => $updatedUser->id,
            'value'   => 1000,
        ]);
    }

    /**
     * @test
     **/
    public function test_user_cannot_register_with_blacklisted_email()
    {
        $user = factory(User::class)->make([
            'email' => 'jarek@qq.com',
        ]);
        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email,
        ]);
    }

    /**
     * @test
     **/
    public function test_user_cannot_register_if_under_age()
    {
        $user = factory(User::class)->make([
            'email' => 'jarek@qq.com',
        ]);
        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => false,
                    'address'     => $address,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email,
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_login()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('password'),
        ]);

        $response = $this->post($this->baseUrl . '/login', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => 'password',
                    'anonymousId' => '23423423423',
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_google_login()
    {
        factory(User::class)->create([
            'password'  => bcrypt('password'),
            'google_id' => '115398402302837625068',
        ]);

        $response = $this->post($this->baseUrl . '/login', [
            'data' => [
                'attributes' => [
                    'googleId'    => '115398402302837625068',
                    'anonymousId' => '12312412412',
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_facebook_login()
    {
        factory(User::class)->create([
            'password'    => bcrypt('password'),
            'facebook_id' => '10151070691734967',
        ]);

        $response = $this->post($this->baseUrl . '/login', [
            'data' => [
                'attributes' => [
                    'facebookId'  => '10151070691734967',
                    'anonymousId' => '124453453',
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_logout()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/logout');

        $response->assertStatus(200);
        $this->assertDatabaseMissing('oauth_access_tokens', [
            'user_id' => $user->id,
        ]);
    }

    public function test_null_is_returned_when_user_without_account_tries_logging_in_via_social()
    {
        $user = factory(User::class)->make();

        $response = $this->post($this->baseUrl . '/login', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => 'password',
                    'anonymousId' => '23423423423',
                    'googleId'    => 'test',
                ],
            ],
        ]);

        $response->assertJson([
            'data' => [
                'type'       => 'access tokens',
                'attributes' => [
                    'accessToken' => null,
                    'user'        => [
                        'id'         => null,
                        'firstName'  => null,
                        'lastName'   => null,
                        'googleId'   => null,
                        'facebookId' => null,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     **/
    public function test_user_cannot_register_if_email_already_exist()
    {
        $user = factory(User::class)->make([
            'email' => 'christian@getrunner.io',
        ]);
        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => '123-645-7968',
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);
        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);
        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);

        $response->assertStatus(401);

        $response->assertJson([
            'errors' => [
                'error' => 'The email has already been taken.'
            ]
        ]);
    }

    /**
     * @test
     **/
    public function test_user_cannot_register_if_phone_number_already_exist()
    {
        $user = factory(User::class)->make([
            'email' => '123testQWEtest321@mail.com',
        ]);
        $address = [
            'formattedAddress' => '8 York St, Toronto, ON M5J 2Y2, Canada',
            'lat'              => '43.6406456',
            'lng'              => '-79.38093900000001',
            'placeId'          => 'ChIJD9HcT9U0K4gRRQfCTDh8JAs',
            'postalCode'       => 'M5J 2Y2',
        ];

        $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => 'testmail321321321@mail.com',
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);
        $response = $this->post($this->baseUrl . '/register', [
            'data' => [
                'attributes' => [
                    'email'       => $user->email,
                    'password'    => $user->password,
                    'firstName'   => $user->first_name,
                    'lastName'    => $user->last_name,
                    'phoneNumber' => $user->phone_number,
                    'dateOfBirth' => $user->date_of_birth,
                    'anonymousId' => '123123123',
                    'isLegalAge'  => true,
                    'address'     => $address,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'errors' => [
                'error',
            ],
        ]);

        $response->assertStatus(401);

        $response->assertJson([
            'errors' => [
                'error' => 'The phone number has already been taken.'
            ]
        ]);
    }
}
