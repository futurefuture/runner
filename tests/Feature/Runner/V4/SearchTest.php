<?php

namespace Tests\Feature\Runner\V4;

use App\Store;
use App\Product;
use App\Inventory;
use App\PostalCode;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4/search';

    /**
     * test.
     */
    public function test_it_can_get_product_based_on_search_query()
    {
        $product    = factory(Product::class)->create([
            'id'        => 844,
            'title'     => 'Carlsberg',
            'is_active' => 1
        ]);
        $store      = factory(Store::class)->create([
            'options' => [
                'storeLogo' => 'test',
                'aboutText' => 'test'
            ]
        ]);
        $inventory  = factory(Inventory::class)->create([
            'store_id' => $store->id
        ]);
        $postalCode = factory(PostalCode::class)->create();
        $query      = $product->title;

        $inventory->productsAll()->attach($product, [
            'quantity' => 15,
        ]);

        $postalCode->inventories()->attach($inventory);
        $response = $this->get($this->baseUrl . '?q=' . $query . '&postalCode=' . $postalCode->postal_code);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'upc',
                        'isActive',
                        'title',
                        'longDescription',
                        'shortDescription',
                        'retailPrice',
                        'markupPercentage',
                        'markup',
                        'runnerPrice',
                        'images' => [
                            [
                                'index',
                                'image',
                            ]
                        ],
                        'imageThumbnail',
                        'packaging',
                        'alcoholContent',
                        'sugarContent',
                        'slug',
                        'categoryId',
                        'style',
                        'producingCountry',
                        'producer',
                        'rewardPoints',
                        'quantity',
                        'caseDeal',
                        'incentives' => [],
                        'averageRating',
                        'favouriteId',
                        'reviewsCount',
                        'stores' => [
                            [
                                'type',
                                'id',
                                'attributes' => [
                                    'title',
                                    'storeLogo',
                                    'subDomain',
                                    'aboutText'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }
}
