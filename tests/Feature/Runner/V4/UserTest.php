<?php

namespace Tests\Feature\Runner\V4;

use App\Address;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_a_user()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                    'inviteCode',
                    'avatarImage',
                    'rewardPointsBalance',
                    'notifications' => [
                        'promoNotification',
                        'orderNotification',
                    ],
                    'createdAt',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_user_with_addresses_and_favourites()
    {
        $user = factory(User::class)->create();
        factory(Address::class)->create([
            'user_id' => $user->id,
        ]);
        $product = factory(Product::class)->create();
        DB::table('favourites')->insert([
            'user_id'    => $user->id,
            'product_id' => $product->id,
        ]);
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '?include=addresses,favourites');
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                    'inviteCode',
                    'avatarImage',
                    'rewardPointsBalance',
                    'favourites',
                    'addresses',
                    'notifications' => [
                        'promoNotification',
                        'orderNotification',
                    ],
                    'createdAt',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_a_user()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user->id, [
            'data' => [
                'attributes' => [
                    'firstName' => 'test',
                ],
            ],
        ]);

        $this->assertDataBaseHas('users', [
            'first_name' => 'test',
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                    'inviteCode',
                    'avatarImage',
                    'rewardPointsBalance',
                    'notifications' => [
                        'promoNotification',
                        'orderNotification',
                    ],
                    'createdAt',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_with_duplicate_email()
    {
        $user1 = factory(\App\User::class)->create([
            'email' => 'test@gmail.com',
        ]);
        $user2 = factory(\App\User::class)->create();
        $this->actingAs($user2, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user2->id, [
            'data' => [
                'attributes' => [
                    'firstName' => 'test',
                    'email'     => 'test@gmail.com',
                ],
            ],
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name' => $user2->first_name,
            'email'      => 'test@gmail.com',
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_with_duplicate_phone_number()
    {
        factory(\App\User::class)->create([
            'email' => '4161111111',
        ]);
        $user2 = factory(\App\User::class)->create();
        $this->actingAs($user2, 'api');

        $this->put($this->baseUrl . '/users/' . $user2->id, [
            'data' => [
                'attributes' => [
                    'firstName'   => 'test',
                    'phoneNumber' => '4161111111',
                ],
            ],
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name'   => $user2->first_name,
            'phone_number' => '4161111111',
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_update_a_user_password_if_old_password_is_incorrect()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->put($this->baseUrl . '/users/' . $user->id . '/update-password', [
            'data' => [
                'attributes' => [
                    'oldPassword' => 'secret',
                    'newPassword' => bcrypt('test'),
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                    'inviteCode',
                    'avatarImage',
                    'rewardPointsBalance',
                    'notifications' => [
                        'promoNotification',
                        'orderNotification',
                    ],
                    'createdAt',
                ],
            ],
        ]);

        $this->assertDataBaseMissing('users', [
            'first_name'   => $user->first_name,
            'password'     => bcrypt('test'),
        ]);
    }

    /**
     * @test
     */
    public function test_it_refer_a_friend_with_email_address()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/refer-a-friend', [
            'data' => [
                'attributes' => [
                    'type'        => 'email',
                    'contactInfo' => 'jarek@wearefuturefuture.com',
                ],
            ],
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_refer_a_friend_with_a_phone_number()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/refer-a-friend', [
            'data' => [
                'attributes' => [
                    'type'        => 'phoneNumber',
                    'contactInfo' => '6477746899',
                ],
            ],
        ]);

        $response->assertStatus(200);
    }
}
