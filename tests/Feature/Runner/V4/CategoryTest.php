<?php

namespace Tests\Feature\Runner\V4;

use DB;
use App\Tag;
use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4/categories';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_categories()
    {
        factory(Category::class)->create();

        $response = $this->get($this->baseUrl);

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'attributes' => [
                        'title',
                        'parentId',
                        'slug',
                        'options',
                        'squareImage',
                        'bannerImage',
                        'description',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_a_category()
    {
        $category = factory(Category::class)->create();

        $response = $this->get($this->baseUrl . '/' . $category->id);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'title',
                    'parentId',
                    'slug',
                    'options',
                    'squareImage',
                    'bannerImage',
                    'description',
                ],
            ],
        ]);
    }
}
