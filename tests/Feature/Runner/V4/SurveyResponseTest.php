<?php

namespace Tests\Feature\Runner\V4;

use App\Survey;
use App\SurveyOption;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SurveyResponseTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * test.
     */
    public function test_user_can_create_survey_response()
    {
        $survey       = factory(Survey::class)->create();
        $surveyOption = factory(SurveyOption::class)->create([
            'survey_id' => $survey->id,
        ]);
        $surveyOption2 = factory(SurveyOption::class)->create([
            'survey_id' => $survey->id,
        ]);
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $this->post($this->baseUrl . '/surveys/' . $survey->id . '/responses', [
            'data' => [
                'type'       => 'survey responses',
                'attributes' => [
                    'surveyId'            => $survey->id,
                    'surveyOptionId'      => $surveyOption->id,
                    'userId'              => $user->id,
                ],
            ],
        ]);

        $this->post($this->baseUrl . '/surveys/' . $survey->id . '/responses', [
            'data' => [
                'type'       => 'survey responses',
                'attributes' => [
                    'surveyId'            => $survey->id,
                    'surveyOptionId'      => $surveyOption->id,
                    'userId'              => $user->id,
                ],
            ],
        ]);

        $response = $this->post($this->baseUrl . '/surveys/' . $survey->id . '/responses', [
            'data' => [
                'type'       => 'survey responses',
                'attributes' => [
                    'surveyId'            => $survey->id,
                    'surveyOptionId'      => $surveyOption2->id,
                    'userId'              => $user->id,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    '*' => [
                        'surveyOptionId',
                        'title',
                        'percentage',
                    ],
                ],
            ]
        ]);
    }
}
