<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use App\Services\StripeService;
use App\Services\StripeServiceFake;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserStripeTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4/users';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();

        // swap out for fake stripe service
        $fakeStripeService = new StripeServiceFake;
        $this->swap(StripeService::class, $fakeStripeService);
    }

    /**
     * @test
     */
    public function test_it_can_create_customer_source()
    {
        $stripeId = 'cus_' . substr(md5(rand()), 0, 14);
        $user     = factory(User::class)->create([
            'stripe_id' => $stripeId,
        ]);
        $this->actingAs($user, 'api');
        $response = $this->post($this->baseUrl . '/' . $user->id . '/stripe/customer/sources', [
            'data' => [
                'id' => 'tok_visa'
            ]
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes',
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_customer()
    {
        $user = factory(User::class)->create([
            'stripe_id' => 'test_customer'
        ]);

        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/' . $user->id . '/stripe/customer');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes',
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_delete_customer_source()
    {
        $stripeId   = 'cus_' . substr(md5(rand()), 0, 14);
        $sourceId   = 'card_' . substr(md5(rand()), 0, 24);
        $user       = factory(User::class)->create([
            'stripe_id' => $stripeId,
        ]);
        $this->actingAs($user, 'api');
        $response = $this->delete($this->baseUrl . '/' . $user->id . '/stripe/customer/sources/' . $sourceId);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes',
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_default_customer_source()
    {
        $stripeId = 'cus_' . substr(md5(rand()), 0, 14);
        $user     = factory(User::class)->create([
            'stripe_id' => $stripeId,
        ]);
        $this->actingAs($user, 'api');
        $response = $this->put($this->baseUrl . '/' . $user->id . '/stripe/customer/sources/tok_visa');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes',
            ],
        ]);
    }
}
