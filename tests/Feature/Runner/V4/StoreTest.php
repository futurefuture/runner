<?php

namespace Tests\Feature\Runner\V4;

use App\User;
use App\Store;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/runner/v4';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_store()
    {
        $store = factory(Store::class)->create([
            'id' => 8,
        ]);
        DB::table('store_layouts')->insert([
            'id'           => 2,
            'store_id'     => $store->id,
            'territory_id' => 1,
            'layout'       => 'test',
        ]);

        $response = $this->get($this->baseUrl . '/stores/' . $store->id . '?postalCode=M5H%21Y2');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'title',
                    'options',
                ],
                'links' => [
                    'self',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_show_available_delivery_times_by_inventories()
    {
        $store = factory(Store::class)->create([
            'id' => 1,
        ]);
        $inventory = factory(Inventory::class)->create([
            'id'       => 1,
            'store_id' => $store->id,
        ]);
        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id . '/available-delivery-times/');
        $response->assertJsonStructure([
            'data' => [],
        ]);
    }
}
