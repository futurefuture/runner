<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user = factory(User::class)->create([
            'promo_notification' => 0,
        ]);
        $this->actingAs($this->user, 'api');
    }

    /**
     * @test
     */
    public function test_it_can_show_user()
    {
        $response = $this->get($this->baseUrl . '/users/' . $this->user->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'firstName',
                    'lastName',
                    'dateOfBirth',
                    'phoneNumber',
                    'email',
                    'avatarImage',
                    'inviteCode',
                    'rewardPoints',
                    'promoNotification',
                    'orderNotification',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_user()
    {
        $data = [
            'firstName'         => 'Test Update',
            'lastName'          => 'Test update last',
            'promoNotification' => true,
        ];
        $response = $this->put($this->baseUrl . '/users/' . $this->user->id, [
            'data' => [
                'type'       => 'users',
                'id'         => $this->user->id,
                'attributes' => [
                    'firstName'         => $data['firstName'],
                    'lastName'          => $data['lastName'],
                    'promoNotification' => $data['promoNotification'],
                ],
            ],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id'                 => $this->user->id,
            'first_name'         => $data['firstName'],
            'last_name'          => $data['lastName'],
            'date_of_birth'      => $this->user->date_of_birth,
            'promo_notification' => 1,
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_logout()
    {
        $response = $this->post($this->baseUrl . '/logout');
        $this->assertFalse(session()->hasOldInput($this->user->email));
    }
}
