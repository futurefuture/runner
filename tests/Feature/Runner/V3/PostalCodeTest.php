<?php

namespace Tests\Feature\Runner\V3;

use DB;
use App\User;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostalCodeTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_store_from_postal_code()
    {
        $inventory = factory(Inventory::class)->create([
            'store_id' => 1,
        ]);
        $store = factory(\App\Store::class)->create();
        DB::table('postal_codes')->insert([
            'postal_code' => 'M5H',
        ]);
        DB::table('inventory_postal_code')->insert([
            'postal_code'  => 'M5H',
            'inventory_id' => $inventory->id,
        ]);

        $response = $this->get($this->baseUrl . '/address/validate?postalCode=M5H');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'sortOrder',
                    'attributes' => [
                        'title',
                        'vendor',
                        'address',
                        'isOpen',
                        'notification',
                        'storeLogo',
                        'inventoryId',
                        'locationId',
                        'subDomain',
                        'inventoryHours' => [],
                        'sortOrder',
                        'towerImage',
                        'layout',
                    ],
                ],
            ],
        ]);
    }

    public function test_it_can_store_out_of_bounds_postal_code()
    {
        $response = $this->post($this->baseUrl . '/postal-codes/out-of-bounds', [
            'data' => [
                'attributes' => [
                    'postalCode' => 'P4P',
                ],
            ],
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('out_of_bounds_postal_codes', [
            'postal_code' => 'P4P',
        ]);
    }
}
