<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserOrderTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_re_order_last_order_from_specific_inventory()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $pastOrder = factory(\App\Order::class)->states('new')->create([
            'user_id'      => $user->id,
            'inventory_id' => 1,
            'status'       => 3,
        ]);
        $products = json_decode($pastOrder->content);
        $subtotal = (int) $products[0]->runnerPrice * $products[0]->quantity;
        $tax      = ((number_format((float) $products[0]->runnerPrice, 2, '.', '') - number_format((float) $products[0]->runnerPrice, 2, '.', '') / 1.12) * 0.13) * $products[0]->quantity;
        $total    = round($subtotal + $tax + 999 + (999 * 0.13) - 0);

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/orders/' . $pastOrder->id . '/re-order', [
            'data' => [
                'attributes' => [
                    'inventoryId' => 1,
                ],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'products',
                    'coupon',
                    'subTotal',
                    'deliveryFee',
                    'tax',
                    'discount',
                    'total',
                    'inventoryId',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_cannot_re_order_last_order_from_different_inventory()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $pastOrder = factory(\App\Order::class)->states('new')->create([
            'user_id'      => $user->id,
            'inventory_id' => 1,
            'status'       => 3,
        ]);
        $products = json_decode($pastOrder->content);
        $subtotal = (int) $products[0]->runnerPrice * $products[0]->quantity;
        $tax      = ((number_format((float) $products[0]->runnerPrice, 2, '.', '') - number_format((float) $products[0]->runnerPrice, 2, '.', '') / 1.12) * 0.13) * $products[0]->quantity;
        $total    = round($subtotal + $tax + 999 + (999 * 0.13) - 0);

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/orders/' . $pastOrder->id . '/re-order', [
            'data' => [
                'attributes' => [
                    'inventoryId' => 2,
                ],
            ],
        ]);

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function test_it_get_user_orders()
    {
        $user    = factory(\App\User::class)->create();
        $courier = factory(\App\User::class)->create();
        $order   = factory(\App\Order::class)->states('new')->create([
            'status'   => 3,
            'user_id'  => $user->id,
            'runner_1' => $courier->id,
        ]);
        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/orders/');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_get_a_user_order()
    {
        $user    = factory(\App\User::class)->create();
        $courier = factory(\App\User::class)->create();
        $order   = factory(\App\Order::class)->states('new')->create([
            'status'   => 3,
            'runner_1' => $courier->id,
            'user_id'  => $user->id,
        ]);
        $this->actingAs($user, 'api');
        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/orders/' . $order->id);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'store',
                    'inventoryId',
                    'user_id',
                    'status',
                    'items',
                    'runner_1',
                    'runner_2',
                    'courier' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                    'device',
                    'customer',
                    'address',
                    'unit_number',
                    'coupon',
                    'instructions',
                    'subtotal',
                    'service_fee',
                    'discount',
                    'discountType',
                    'delivery',
                    'tip',
                    'display_tax_total',
                    'total',
                    'signature',
                    'schedule_time',
                    'delivered_at',
                    'created_at',
                    'gift',
                ],
            ],
        ]);
    }
}
