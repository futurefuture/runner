<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use App\Store;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InventoriesTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_inventory()
    {
        $store = factory(Store::class)->create([
            'id' => 1,
        ]);
        $inventory = factory(Inventory::class)->create([
            'id'       => 1,
            'store_id' => $store->id,
        ]);
        $response = $this->get($this->baseUrl . '/inventories/' . $inventory->id);
        $response->assertJsonStructure([
            'data' => [
                    'type',
                    'id',
                    'attributes' => [
                        'title',
                        'isActive',
                        'hours',
                        'locationId',
                        'phoneNumber',
                        'address',
                    ],
                    'links' => [
                        'self',
                    ],
                ],
        ]);
    }
}
