<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use App\Store;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->store = factory(Store::class)->create([
            'id' => 1,
        ]);
        $this->inventory = factory(Inventory::class)->create([
            'id'       => 1,
            'store_id' => $this->store->id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_show_store()
    {
        $response = $this->get($this->baseUrl . '/stores/' . $this->store->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'title',
                    'isActive',
                    'options',
                ],
                'links' => [
                    'self',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_show_store_inventories()
    {
        $this->store->inventories()->attach($this->inventory->id);
        $response = $this->get($this->baseUrl . '/stores/' . $this->store->id . '/inventories/' . $this->inventory->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'title',
                    'isActive',
                    'hours',
                    'locationId',
                    'phoneNumber',
                    'address',
                ],
                'links' => [
                    'self',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_show_available_delivery_times_by_inventories()
    {
        $response = $this->get($this->baseUrl . '/inventories/' . $this->inventory->id . '/available-delivery-times/');
        $response->assertJsonStructure([
            'data' => [],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_show_available_delivery_times()
    {
        $this->withoutExceptionHandling();
        $store    = factory(Store::class)->create();
        $response = $this->get($this->baseUrl . '/stores/' . $store->id . '/available-delivery-times/');

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'id',
                    'regularTimeSlots' => [
                        [
                            'hour',
                            'interval',
                            'rewardPoints',
                            'regularPrice',
                            'salePrice',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
