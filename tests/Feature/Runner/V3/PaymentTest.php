<?php

namespace Tests\Feature\Runner\V3;

use DB;
use App\User;
use App\Product;
use App\Inventory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_create_a_regular_payment()
    {
        // set store closed to false
        DB::table('config')->insert([
            'key'   => 'close_store',
            'value' => 'false',
        ]);

        // create fake store
        factory(\App\Store::class)->create();

        // create fake inventory
        factory(Inventory::class)->create();

        // fake store inentory relationship
        DB::table('inventory_store')->insert([
            'inventory_id' => 1,
            'store_id'     => 1,
        ]);

        // fake user
        $user = factory(\App\User::class)->create([
            'stripe_id' => 'cus_EAW8EwqubLMGme',
        ]);
        $this->actingAs($user, 'api');

        // fake address
        $address = factory(\App\Address::class)->create([
            'user_id' => $user->id,
        ]);

        // fake product
        $product = factory(Product::class)->create();

        // fake cart
        $cart = factory(\App\Cart::class)->create([
            'user_id' => $user->id,
            'content' => json_encode([
                'products' => [
                    [
                        'id'           => $product->id,
                        'title'        => $product->title,
                        'runnerPrice'  => $product->runner_price,
                        'image'        => $product->image,
                        'quantity'     => 1,
                        'packaging'    => $product->packaging,
                        'rewardPoints' => null,
                        'allowSub'     => true,
                        'subTotal'     => $product->runner_price * 1,
                    ],
                ],
                'subTotal'     => 321,
                'deliveryFee'  => 999,
                'tax'          => 34,
                'rewardPoints' => 0,
                'discount'     => 0,
                'total'        => 1423,
            ]),
        ]);

        $response = $this->post('https://api.runner.test/v3/runner/payment/create', [
            'data' => [
                'attributes' => [
                    'userId'      => $user->id,
                    'inventoryId' => 1,
                ],
            ],
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('orders', [
            'user_id'        => $user->id,
            'status'         => 0,
            'content'        => json_encode([
                [
                    'id'           => $product->id,
                    'title'        => $product->title,
                    'runnerPrice'  => $product->runner_price,
                    'image'        => $product->image,
                    'quantity'     => 1,
                    'packaging'    => $product->packaging,
                    'rewardPoints' => null,
                    'allowSub'     => true,
                    'subTotal'     => $product->runner_price,
                ],
            ]),
            'address_id' => $address->id,
        ]);
    }
}
