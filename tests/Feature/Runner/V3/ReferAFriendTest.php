<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReferAFriendTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_send_a_refer_a_friend_email()
    {
        Mail::fake();

        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/refer-a-friend', [
            'data' => [
                'type'       => 'refer-a-friends',
                'attributes' => [
                    'type'          => 'email',
                    'contactInfo'   => 'jarek@getrunner.io',
                ],
            ],
        ]);

        $response->assertStatus(200);
    }
}
