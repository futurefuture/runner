<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoriesTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_categories()
    {
        $categories = factory(Category::class, 10)->create();
        $response   = $this->get($this->baseUrl . '/categories/');
        $response->assertJsonStructure([
            'data' => [[
                'type',
                'id',
                'attributes' => [
                    'title',
                    'parentId',
                    'slug',
                    'image',
                    ],
                ],
            ],
        ]);
    }
}
