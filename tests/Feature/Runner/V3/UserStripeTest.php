<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserStripeTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_customer()
    {
        $user = factory(\App\User::class)->create([
            'stripe_id' => 'cus_B3Rx5cnruKRoBH',
        ]);

        $this->actingAs($user, 'api');

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/stripe/customer');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'id',
                    'object',
                    'account_balance',
                    'created',
                    'currency',
                    'default_source',
                    'description',
                    'discount',
                    'email',
                    'invoice_prefix',
                    'invoice_settings',
                    'livemode',
                    'metadata',
                    'shipping',
                    'sources',
                    'subscriptions',
                    'tax_info',
                    'tax_info_verification',
                ],
            ],
        ]);
    }
}
