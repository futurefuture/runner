<?php

namespace Tests\Feature\Runner\V3;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PageTest extends TestCase
{
    use RefreshDatabase;

    protected $store;

    protected function setUp(): void
    {
        parent::setUp();
        $this->store = factory(\App\Store::class)->create([
            'sub_domain' => 'www',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_search_page()
    {
        $response = $this->get('/search');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_explore_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_checkout_page()
    {
        $response = $this->get('/checkout');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_confirmation_page()
    {
        $order = factory(\App\Order::class)->create();

        $response = $this->get('/confirmation?orderId='.$order->id);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_register_page()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_login_page()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_password_email_page()
    {
        $response = $this->get('/password/email');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_password_reset_page()
    {
        // fake token sent back from password rest request
        $token = rand();

        $response = $this->get('/password/reset/'.$token);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_sitemap_page()
    {
        $response = $this->get('/sitemap.xml');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_rewards_page()
    {
        $response = $this->get('/rewards/');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_facebook_login_page()
    {
        $response = $this->get('/login/facebook');

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function test_it_can_get_google_login_page()
    {
        $response = $this->get('/login/google');

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function test_it_can_get_about_page()
    {
        $response = $this->get('/about');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_terms_conditions_page()
    {
        $response = $this->get('/terms-conditions');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_faq_page()
    {
        $response = $this->get('/faq');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_refer_a_friend_page()
    {
        $response = $this->get('/refer-a-friend');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function test_it_can_get_payment_information_page()
    {
        $response = $this->get('/payment-information');

        $response->assertStatus(200);
    }
}
