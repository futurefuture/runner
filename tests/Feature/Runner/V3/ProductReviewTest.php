<?php

namespace Tests\Feature\Runner\V3;

use App\Product;
use App\Review;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductReviewTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->product = factory(Product::class)->create();
        $this->user    = factory(User::class)->create();
        $this->actingAs($this->user, 'api');
    }

    /**
     * @test
     */
    public function test_it_can_create_product_review()
    {
        $review = factory(Review::class)->make([
            'id'         => 1,
            'product_id' => $this->product->id,
            'user_id'    => $this->user->id,
        ]);
        $response = $this->post($this->baseUrl . '/products/' . $this->product->id . '/reviews', [
            'data' => [
                'type'       => 'reviews',
                'attributes' => [
                    'title'      => $review->title,
                    'value'      => $review->value,
                    'product_id' => $review->product_id,
                    'user_id'    => $review->user_id,
                    'is_approved'=> $review->is_approved,
                    'description'=> $review->description,
                    'helpful'    => $review->helpful,
                    'unhelpful'  => $review->unhelpful,
                ],
            ],
        ]);
        $this->assertDatabaseHas('reviews', [
            'id'              => $review->id,
            'value'           => $review->value,
            'user_id'         => $review->user_id,
            'is_approved'     => 0,
            'description'     => $review->description,
            'helpful'         => 0,
            'unhelpful'       => 0,
            'created_at'      => Carbon::now()->toDateTimeString(),
            'updated_at'      => Carbon::now()->toDateTimeString(),
            'title'           => $review->title,
            'product_id'      => $review->product_id,
        ]);
    }

    /**
     * @test
     */
    public function test_it_get_all_product_review()
    {
        $this->review = factory(Review::class)->create([
            'product_id'    => $this->product->id,
            'user_id'       => $this->user->id,
        ]);
        $response = $this->get($this->baseUrl . '/products/' . $this->product->id . '/reviews');
        $response->assertJsonStructure([
            'data' => [
                    [
                        'type',
                        'id',
                        'attributes' => [
                            'title',
                            'value',
                            'product_id',
                            'user_id',
                            'firstName',
                            'lastName',
                            'userName',
                            'avatarImage',
                            'is_approved',
                            'description',
                            'helpful',
                            'unhelpful',
                        ],
                    ],
                ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_update_review()
    {
        $data      = ['helpful'=> 1];
        $reviewNew = factory(Review::class)->create([
        'product_id'    => $this->product->id,
        'user_id'       => $this->user->id,
        ]);
        $response = $this->put($this->baseUrl . '/products/' . $this->product->id . '/reviews/' . $reviewNew->id, [
        'data' => [
            'attributes' => [
                'helpful'   => $data['helpful'],
            ],
            ],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('reviews', [
            'id'              => $reviewNew->id,
            'value'           => $reviewNew->value,
            'user_id'         => $reviewNew->user_id,
            'is_approved'     => 1,
            'description'     => $reviewNew->description,
            'helpful'         => $reviewNew->helpful + 1,
            'unhelpful'       => 0,
            'created_at'      => Carbon::now()->toDateTimeString(),
            'updated_at'      => Carbon::now()->toDateTimeString(),
            'title'           => $reviewNew->title,
            'product_id'      => $reviewNew->product_id,
        ]);
    }
}
