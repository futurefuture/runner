<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    private function loginAdmin()
    {
        $user = factory(\App\User::class)->create([
            'role' => 1,
        ]);
        $this->actingAs($user, 'api');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        User::unsetEventDispatcher();
    }

    /**
     * @test

    public function test_user_can_register()
    {
        $user = factory(User::class)->make();

        /*$this->instance(StripeService::class, Mockery::mock(StripeService::class, function ($mock) use ($user) {
            $mock->shouldReceive()->storeCustomer(['firstName' => $user->first_name, 'email' => $user->email])->once();
        }));

        $this->instance(MailChimpService::class, ['param' => $user], Mockery::mock(MailChimpService::class, ['param' => $user], function ($mock) {
            $mock->shouldReceive()->addSubscriber()->once();
        }));

        $response = $this->post($this->baseUrl . '/register', [
            'email'             => $user->email,
            'password'          => $user->password,
            'firstName'         => $user->first_name,
            'lastName'          => $user->last_name,
            'phoneNumber'       => $user->phone_number,
            'dateOfBirth'       => $user->date_of_birth,
            'isLegalAge'        => true
        ]);

        /** $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName'
                    ]
                ]
            ]
        ]);

        $response->assertStatus(200);
    }
     */

    /**
     * @test
     */
    public function test_user_can_login()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('password'),
        ]);

        $response = $this->post($this->baseUrl . '/login', [
            'email'    => $user->email,
            'password' => 'password',
        ]);

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'user' => [
                        'id',
                        'firstName',
                        'lastName',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_logout()
    {
        $this->loginAdmin();

        $response = $this->post($this->baseUrl . '/logout');
        $response->assertStatus(200);
    }
}
