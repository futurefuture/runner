<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use App\Product;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_product()
    {
        $category = factory(Category::class)->create();
        $product  = factory(Product::class)->create([
            'id'          => 1,
            'category_id' => $category->id,
        ]);
        $review = factory(\App\Review::class)->create([
            'product_id' => $product->id,
        ]);
        $response = $this->get($this->baseUrl . '/products/' . $product->id);
        $response->assertJsonStructure([
            'data' => [
                    'type',
                    'id',
                    'attributes' => [
                        'sku',
                        'upc',
                        'isActive',
                        'title',
                        'longDescription',
                        'shortDescription',
                        'retailPrice',
                        'wholesalePrice',
                        'limitedTimeOfferPrice',
                        'limitedTimeOfferSavings',
                        'markupPercentage',
                        'markup',
                        'runnerPrice',
                        'image',
                        'imageThumbnail',
                        'packaging',
                        'reviews',
                        'alcoholContent',
                        'sugarContent',
                        'slug',
                        'categoryId',
                        'category',
                        'style',
                        'producingCountry',
                        'producer',
                        'productRating',
                        'rewardPoints',
                        'caseDeal',
                        'quantityDiscount',
                        'inStock',
                    ],
                    'links' => [
                        'self',
                    ],
                ],
        ]);
    }
}
