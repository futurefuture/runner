<?php

namespace Tests\Feature\Runner\V3;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_show_store()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $address = factory(\App\Address::class)->make([
            'id' => 1,
        ]);

        $response = $this->post($this->baseUrl . '/users/' . $user->id . '/addresses', [
            'data' => [
                'type'       => 'addresses',
                'attributes' => [
                    'formattedAddress' => $address->formatted_address,
                    'placeId'          => $address->place_id,
                    'lat'              => $address->lat,
                    'lng'              => $address->lng,
                    'postalCode'       => $address->postal_code,
                    'unitNumber'       => $address->unit_number,
                    'instructions'     => $address->instructions,
                    'businessName'     => $address->business_name,
                    'selected'         => $address->selected,
                ],
            ],
        ]);

        $response->assertJson([
            'data' => [
                [
                    'type'       => 'addresses',
                    'id'         => $address->id,
                    'attributes' => [
                        'userId'           => $user->id,
                        'formattedAddress' => $address->formatted_address,
                        'lat'              => $address->lat,
                        'lng'              => $address->lng,
                        'placeId'          => $address->place_id,
                        'postalCode'       => $address->postal_code,
                        'unitNumber'       => $address->unit_number,
                        'businessName'     => $address->business_name,
                        'instructions'     => $address->instructions,
                        'selected'         => $address->selected,
                    ],
                    'links' => [
                        'self' => $this->baseUrl . '/users/' . $user->id . '/addresses/' . $address->id,
                    ],
                ],
            ],
        ]);

        $this->assertDatabaseHas('addresses', [
            'user_id'           => $user->id,
            'formatted_address' => $address->formatted_address,
            'lat'               => $address->lat,
            'lng'               => $address->lng,
            'place_id'          => $address->place_id,
            'postal_code'       => $address->postal_code,
            'unit_number'       => $address->unit_number,
            'business_name'     => $address->business_name,
            'instructions'      => $address->instructions,
            'selected'          => $address->selected,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_all_addresses()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $addresses = factory(\App\Address::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->get($this->baseUrl . '/users/' . $user->id . '/addresses');

        $response->assertJson([
            'data' => [
                [
                    'type'       => 'addresses',
                    'id'         => $addresses[0]->id,
                    'attributes' => [
                        'userId'           => $user->id,
                        'formattedAddress' => $addresses[0]->formatted_address,
                        'lat'              => $addresses[0]->lat,
                        'lng'              => $addresses[0]->lng,
                        'placeId'          => $addresses[0]->place_id,
                        'postalCode'       => $addresses[0]->postal_code,
                        'unitNumber'       => $addresses[0]->unit_number,
                        'businessName'     => $addresses[0]->business_name,
                        'instructions'     => $addresses[0]->instructions,
                        'selected'         => $addresses[0]->selected,
                    ],
                ],
                [
                    'type'       => 'addresses',
                    'id'         => $addresses[1]->id,
                    'attributes' => [
                        'userId'           => $user->id,
                        'formattedAddress' => $addresses[1]->formatted_address,
                        'lat'              => $addresses[1]->lat,
                        'lng'              => $addresses[1]->lng,
                        'placeId'          => $addresses[1]->place_id,
                        'postalCode'       => $addresses[1]->postal_code,
                        'unitNumber'       => $addresses[1]->unit_number,
                        'businessName'     => $addresses[1]->business_name,
                        'instructions'     => $addresses[1]->instructions,
                        'selected'         => $addresses[1]->selected,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_update_all_attributes_on_address()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $addressOne = factory(\App\Address::class)->create([
            'user_id' => $user->id,
        ]);
        $addressTwo = factory(\App\Address::class)->make([
            'user_id' => $user->id,
        ]);

        $response = $this->put($this->baseUrl . '/users/' . $user->id . '/addresses/' . $addressOne->id, [
            'data' => [
                'type'       => 'addresses',
                'id'         => $addressOne->id,
                'attributes' => [
                    'formattedAddress' => $addressTwo->formatted_address,
                    'lat'              => $addressTwo->lat,
                    'lng'              => $addressTwo->lng,
                    'placeId'          => $addressTwo->place_id,
                    'postalCode'       => $addressTwo->postal_code,
                    'unitNumber'       => $addressTwo->unit_number,
                    'businessName'     => $addressTwo->business_name,
                    'instructions'     => $addressTwo->instructions,
                ],
            ],
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('addresses', [
            'id'                => $addressOne->id,
            'formatted_address' => $addressTwo->formatted_address,
            'lat'               => $addressTwo->lat,
            'lng'               => $addressTwo->lng,
            'place_id'          => $addressTwo->place_id,
            'postal_code'       => $addressTwo->postal_code,
            'unit_number'       => $addressTwo->unit_number,
            'business_name'     => $addressTwo->business_name,
            'instructions'      => $addressTwo->instructions,
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_update_selected_address_and_unselected_other_addresses()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $addressOne = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
            'selected' => 0,
        ]);
        $addressTwo = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
            'selected' => 1,
        ]);

        $response = $this->put($this->baseUrl . '/users/' . $user->id . '/addresses/' . $addressOne->id, [
            'data' => [
                'type'       => 'addresses',
                'id'         => $addressOne->id,
                'attributes' => [
                    'selected' => 1,
                ],
            ],
        ]);

        $response->assertJson([
            'data' => [
                [
                    'type'       => 'addresses',
                    'id'         => $addressOne->id,
                    'attributes' => [
                        'userId'           => $user->id,
                        'formattedAddress' => $addressOne->formatted_address,
                        'lat'              => $addressOne->lat,
                        'lng'              => $addressOne->lng,
                        'placeId'          => $addressOne->place_id,
                        'postalCode'       => $addressOne->postal_code,
                        'unitNumber'       => $addressOne->unit_number,
                        'businessName'     => $addressOne->business_name,
                        'instructions'     => $addressOne->instructions,
                        'selected'         => 1,
                    ],
                ],
                [
                    'type'       => 'addresses',
                    'id'         => $addressTwo->id,
                    'attributes' => [
                        'userId'           => $user->id,
                        'formattedAddress' => $addressTwo->formatted_address,
                        'lat'              => $addressTwo->lat,
                        'lng'              => $addressTwo->lng,
                        'placeId'          => $addressTwo->place_id,
                        'postalCode'       => $addressTwo->postal_code,
                        'unitNumber'       => $addressTwo->unit_number,
                        'businessName'     => $addressTwo->business_name,
                        'instructions'     => $addressTwo->instructions,
                        'selected'         => 0,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_delete_an_address()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        $address = factory(\App\Address::class)->create([
            'user_id'  => $user->id,
        ]);

        $response = $this->delete($this->baseUrl . '/users/' . $user->id . '/addresses/' . $address->id);

        $this->assertSoftDeleted('addresses', [
            'id' => $address->id,
        ]);
    }
}
