<?php

namespace Tests\Feature\Runner\V3;

use App\Cart;
use App\Inventory;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CartTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->user      = factory(User::class)->create(['id' => 11500]);
        $this->inventory = factory(Inventory::class)->create(['id' => 1]);
    }

    /**
     * @test
     */
    public function test_it_can_show_cart_of_user()
    {
        $cart = factory(Cart::class)->create([
            'id'           => 1,
            'user_id'      => $this->user->id,
            'inventory_id' => $this->inventory->id,
            'content'      => '{"coupon":null,"incentive":{"id":1459,"value":500},"products":[{"id":16007,"title":"Felix & Lucie Cabernet-Syrah","runnerPrice":1663,"retailPrice":1494,"image":"https:\/\/www.lcbo.com\/content\/dam\/lcbo\/products\/635920.jpg\/jcr:content\/renditions\/cq5dam.web.1280.1280.jpeg","quantity":3,"packaging":"750mL bottle","rewardPoints":null,"allowSub":true,"type":"regular","subTotal":4989}],"subTotal":4989,"deliveryFee":999,"serviceFee":0,"tax":199,"rewardPointsUsed":0,"isGift":false,"discount":500,"total":5687}',
        ]);
        $response = $this->get($this->baseUrl . '/users/' . $this->user->id . '/inventories/' . $this->inventory->id . '/carts/' . $cart->id);
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'products' => [
                        [
                            'id',
                            'title',
                            'runnerPrice',
                            'retailPrice',
                            'image',
                            'quantity',
                            'packaging',
                            'rewardPoints',
                            'allowSub',
                            'type',
                            'subTotal',
                        ],
                    ],
                    'coupon',
                    'incentive'=> [
                        'id',
                        'value',
                    ],
                    'subTotal',
                    'serviceFee',
                    'deliveryFee',
                    'tax',
                    'discount',
                    'total',
                ],
            ],
        ]);
    }
}
