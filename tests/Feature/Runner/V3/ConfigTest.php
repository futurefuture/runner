<?php

namespace Tests\Feature\Runner\V3;

use DB;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfigTest extends TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'https://api.runner.test/v3/runner';

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
    }

    /**
     * @test
     */
    public function test_it_can_get_global_notification()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        DB::table('config')->insert([
            'key'   => 'globalNotification',
            'value' => 'test',
        ]);

        $response = $this->get($this->baseUrl . '/config/92002993827773499200982773/global-notification');

        $response->assertStatus(200);
        $response->assertJson([
            'id'    => 1,
            'key'   => 'globalNotification',
            'value' => 'test',
        ]);
    }

    /**
     * @test
     */
    public function test_it_can_get_global_configs()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user, 'api');
        DB::table('config')->insert([
            'key'   => 'globalNotification',
            'value' => 'test',
        ]);
        $response = $this->get($this->baseUrl . '/config/92002993827773499200982773');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id'         => 1,
                'attributes' => [
                    'ags'  => '18570a327b9fce163e19ce027f79a3bb',
                      'ai' => [
                        'gifts'    => 'gifts',
                        'runner'   => 'new_products',
                    ],
                      'aid'          => '1250283544',
                      'gmk'          => 'AIzaSyBGp2tDORwlAzcrrdA_eKBffxsZLf_kNdQ',
                      'aai'          => 'EWG2ZDNJEQ',
                      'zac'          => '4eWpbBn0mSlawTtakFCDwFilkuwfGaFH',
                      'ami'          => 'merchant.com.futurefuture.runner',
                      'adk'          => '6LnJkXQVVY2tme76JZJV3b',
                      'notification' => null,
                      'appVersion'   => 'Wersion 4.1.6',
                ],
            ],
        ]);
    }
}
