<?php

namespace Tests\Feature\Api\V3\Runner;

use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use App\Mail\UserAbandonedCart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use App\Mail\UserSignedUpWithNoPurchase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConsoleCommandTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        User::unsetEventDispatcher();
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function test_it_dispatches_email_to_new_user_with_no_purchase()
    {
        Mail::fake();
        $twelveAndAHalfMinutesAgo = Carbon::now()
                                        ->subMinutes(12)
                                        ->subSeconds(30)
                                        ->toDateTimeString();

        $user = factory(User::class)->create([
            'created_at' => $twelveAndAHalfMinutesAgo,
        ]);

        $response = Artisan::call('runner:deuwnp');

        Mail::assertQueued(UserSignedUpWithNoPurchase::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email) &&
                    $mail->hasBcc('service@getrunner.io');
        });
    }

    /**
     * @test
     */
    public function test_it_does_not_dispatch_email_to_new_user_with_purchase()
    {
        Mail::fake();
        $twelveAndAHalfMinutesAgo = Carbon::now()
                                        ->subMinutes(12)
                                        ->subSeconds(30)
                                        ->toDateTimeString();

        $user = factory(\App\User::class)->create([
            'created_at' => $twelveAndAHalfMinutesAgo,
        ]);
        $order = factory(\App\Order::class)->create([
            'status'  => 3,
            'user_id' => $user->id,
        ]);

        $response = Artisan::call('runner:deuwnp');

        Mail::assertNotQueued(UserSignedUpWithNoPurchase::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email) &&
                    $mail->hasBcc('service@getrunner.io');
        });
    }

    /**
     * @test
     */
    public function test_it_does_not_dispatch_email_to_older_user_with_no_purchase()
    {
        Mail::fake();
        $fourteenMinutesAgo = Carbon::now()
                                        ->subMinutes(14)
                                        ->toDateTimeString();

        $user = factory(User::class)->create([
            'created_at' => $fourteenMinutesAgo,
        ]);

        $response = Artisan::call('runner:deuwnp');

        Mail::assertNotQueued(UserSignedUpWithNoPurchase::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email) &&
                    $mail->hasBcc('service@getrunner.io');
        });
    }

    /**
     * @test
     */
    public function test_it_dispatches_email_to_user_with_abandoned_cart()
    {
        Mail::fake();
        $user = factory(User::class)->create();
        $cart = factory(\App\Cart::class)->create([
            'marketing_status' => 0,
        ]);

        $response = Artisan::call('runner:deuwac');

        Mail::assertNotQueued(UserAbandonedCart::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email) &&
                    $mail->hasBcc('service@getrunner.io');
        });
    }

    /**
     * @test
     */
    public function test_it_can_update_past_product_incentives_to_inactive()
    {
        $endDate = Carbon::now()->subDays(1)->toDateString();

        $productIncentive = factory(\App\IncentiveProduct::class)->create([
            'end_date' => $endDate,
        ]);

        $response = Artisan::call('runner:update-product-incentives');

        $this->assertDatabaseHas('incentive_product', [
            'id'        => 1,
            'is_active' => 0,
        ]);
    }
}
