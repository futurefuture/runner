const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
const SentryWebpackPlugin = require('@sentry/webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/runner/app.js', 'public/runner/js/app.js')
  .js('resources/js/dispatchV1/app.js', 'public/dispatchV1/js/app.js')
  .js('resources/js/command/app.js', 'public/command/js/app.js')
  .js('resources/js/dispatchV2/app.js', 'public/dispatchV2/js/app.js')
  .sass('resources/sass/v3/app.scss', 'public/css/v3/app.css')
  .options({
    processCssUrls: false,
    postCss: [
      tailwindcss('./tailwind.js')
    ],
  })
  .sass('resources/sass/bulma/bulma.sass', 'public/css/bulma.css')
  .sass('resources/sass/courier/styles.scss', 'public/css/courier.css')
  .sass('resources/sass/command/app.scss', 'public/css/command.css')
  .sass('resources/sass/dispatch/app.scss', 'public/css/dispatch.css')
  .sass('resources/sass/dispatchv2/app.scss', 'public/css/dispatchv2.css')
  .sass('resources/sass/affiliate/app.scss', 'public/css/affiliate.css')
  .sass('resources/sass/v2/app.scss', 'public/css/v2/app.css');

if (mix.inProduction()) {
  mix.version();
}
